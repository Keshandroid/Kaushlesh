package gun0912.tedimagepicker
import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.ParcelFileDescriptor
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gun0912.tedonactivityresult.model.ActivityResult
import com.tedpark.tedonactivityresult.rx2.TedRxOnActivityResult
import gun0912.tedimagepicker.adapter.AlbumAdapter
import gun0912.tedimagepicker.adapter.GridSpacingItemDecoration
import gun0912.tedimagepicker.adapter.MediaAdapter
import gun0912.tedimagepicker.adapter.SelectedMediaAdapter
import gun0912.tedimagepicker.base.BaseRecyclerViewAdapter
import gun0912.tedimagepicker.builder.TedImagePickerBaseBuilder
import gun0912.tedimagepicker.builder.type.AlbumType
import gun0912.tedimagepicker.builder.type.SelectType
import gun0912.tedimagepicker.databinding.ActivityTedImagePickerBinding
import gun0912.tedimagepicker.extenstion.close
import gun0912.tedimagepicker.extenstion.isOpen
import gun0912.tedimagepicker.extenstion.setLock
import gun0912.tedimagepicker.extenstion.toggle
import gun0912.tedimagepicker.model.Album
import gun0912.tedimagepicker.model.Media
import gun0912.tedimagepicker.util.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList


internal class TedImagePickerActivity : AppCompatActivity(),SelectedMediaAdapter.OnStartDragListener {

    private lateinit var binding: ActivityTedImagePickerBinding
    private val albumAdapter by lazy { AlbumAdapter(builder) }
    private lateinit var mediaAdapter: MediaAdapter
    private lateinit var selectedMediaAdapter: SelectedMediaAdapter

    private lateinit var builder: TedImagePickerBaseBuilder<*>

    private lateinit var disposable: Disposable

    private var selectedPosition = 0
    lateinit var touchHelper: ItemTouchHelper
    var totalsize :Int = 0;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSavedInstanceState(savedInstanceState)
        if (Build.VERSION.SDK_INT != Build.VERSION_CODES.O) {
            requestedOrientation = builder.screenOrientation
        }
        startAnimation()
        binding = DataBindingUtil.setContentView(this, R.layout.activity_ted_image_picker)
        binding.imageCountFormat = builder.imageCountFormat
        setupToolbar()
        setupTitle()
        setupRecyclerView()
        setupListener()
        setupSelectedMediaView()
        setupButton()
        setupAlbumType()
        loadMedia()



    }

    private fun startAnimation() {
        if (builder.startEnterAnim != null && builder.startExitAnim != null) {
            overridePendingTransition(builder.startEnterAnim!!, builder.startExitAnim!!)
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(builder.showTitle)
        builder.backButtonResId.let {
            binding.toolbar.setNavigationIcon(it)
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setupTitle() {
        val title = builder.title ?: getString(builder.titleResId)
        setTitle(title)
    }

    private fun setupButton() {
        with(binding) {
            buttonGravity = builder.buttonGravity
            buttonText = builder.buttonText ?: getString(builder.buttonTextResId)
            buttonTextColor =
                    ContextCompat.getColor(this@TedImagePickerActivity, builder.buttonTextColorResId)
            buttonBackground = builder.buttonBackgroundResId
            buttonDrawableOnly = builder.buttonDrawableOnly
        }

        setupButtonVisibility()
    }

    private fun setupButtonVisibility() {
        binding.showButton = when {
            builder.selectType == SelectType.SINGLE -> false
            else -> mediaAdapter.selectedUriList.isNotEmpty()
        }
    }

    private fun loadMedia(isRefresh: Boolean = false) {
        disposable = GalleryUtil.getMedia(this, builder.mediaType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { albumList: List<Album> ->
                    albumAdapter.replaceAll(albumList)
                    setSelectedAlbum(selectedPosition)
                    if (!isRefresh) {
                        setSelectedUriList(builder.selectedUriList)
                    }
                    binding.layoutContent.rvMedia.visibility = View.VISIBLE

                }
    }

    private fun setSelectedUriList(uriList: List<Uri>?) =
            uriList?.forEach { uri: Uri -> onMultiMediaClick(uri) }

    private fun setSavedInstanceState(savedInstanceState: Bundle?) {

        val bundle: Bundle? = when {
            savedInstanceState != null -> savedInstanceState
            else -> intent.extras
        }

        builder = bundle?.getParcelable(EXTRA_BUILDER)
                ?: TedImagePickerBaseBuilder<TedImagePickerBaseBuilder<*>>()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(EXTRA_BUILDER, builder)
        super.onSaveInstanceState(outState)
    }

    private fun setupRecyclerView() {
        setupAlbumRecyclerView()
        setupMediaRecyclerView()
        setupSelectedMediaRecyclerView()
    }


    private fun setupAlbumRecyclerView() {

        val albumAdapter = albumAdapter.apply {
            onItemClickListener = object : BaseRecyclerViewAdapter.OnItemClickListener<Album> {
                override fun onItemClick(data: Album, itemPosition: Int, layoutPosition: Int) {
                    this@TedImagePickerActivity.setSelectedAlbum(itemPosition)
                    binding.drawerLayout.close()
                    binding.isAlbumOpened = false
                }
            }
        }
        binding.rvAlbum.run {
            adapter = albumAdapter
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    binding.drawerLayout.setLock(newState == RecyclerView.SCROLL_STATE_DRAGGING)
                }
            })
        }

        binding.rvAlbumDropDown.adapter = albumAdapter

    }

    private fun setupMediaRecyclerView() {
        mediaAdapter = MediaAdapter(this, builder).apply {
            onItemClickListener = object : BaseRecyclerViewAdapter.OnItemClickListener<Media> {
                override fun onItemClick(data: Media, itemPosition: Int, layoutPosition: Int) {
                    binding.isAlbumOpened = false
                    this@TedImagePickerActivity.onMediaClick(data.uri)
                }

                override fun onHeaderClick() {

                    if(selectedMediaAdapter.itemCount == 12)
                    {
                        ToastUtil.showToast("you have reached maximum image limit")
                    }
                    else {
                        onCameraTileClick()
                    }
                }
            }

            onMediaAddListener = {
                binding.layoutContent.rvSelectedMedia.smoothScrollToPosition(selectedMediaAdapter.itemCount)
            }

        }

        binding.layoutContent.rvMedia.run {
            layoutManager = GridLayoutManager(this@TedImagePickerActivity, IMAGE_SPAN_COUNT)
            addItemDecoration(GridSpacingItemDecoration(IMAGE_SPAN_COUNT, 8))
            itemAnimator = null
            adapter = mediaAdapter
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)

                    (layoutManager as? LinearLayoutManager)?.let {
                        val firstVisiblePosition = it.findFirstCompletelyVisibleItemPosition()
                        if (firstVisiblePosition <= 0) {
                            return
                        }
                        val media = mediaAdapter.getItem(firstVisiblePosition)
                        val dateString = SimpleDateFormat(
                                builder.scrollIndicatorDateFormat,
                                Locale.getDefault()
                        ).format(Date(TimeUnit.SECONDS.toMillis(media.dateAddedSecond)))
                        binding.layoutContent.fastScroller.setBubbleText(dateString)
                    }
                }
            })
        }

        binding.layoutContent.fastScroller.recyclerView = binding.layoutContent.rvMedia
//        val callback: ItemTouchHelper.Callback = ItemMoveCallbackListener(adapter)
//        touchHelper = ItemTouchHelper(callback)
//        touchHelper.attachToRecyclerView(rvimages)
    }

    public fun setupSelectedMediaRecyclerView() {
        binding.layoutContent.selectType = builder.selectType

        selectedMediaAdapter = SelectedMediaAdapter(this).apply {
            onClearClickListener = {
                uri -> onMultiMediaClick(uri)
            }
        }
        binding.layoutContent.rvSelectedMedia.run {
            layoutManager = LinearLayoutManager(
                    this@TedImagePickerActivity,
                    LinearLayoutManager.HORIZONTAL,
                    false
            )
            adapter = selectedMediaAdapter

            val callback: ItemTouchHelper.Callback = ItemMoveCallbackListener(selectedMediaAdapter)

            touchHelper = ItemTouchHelper(callback)

            touchHelper.attachToRecyclerView(binding.layoutContent.rvSelectedMedia)

        }

    }

    @SuppressLint("CheckResult")
    private fun onCameraTileClick() {
        val (cameraIntent, uri) = MediaUtil.getMediaIntentUri(
                this@TedImagePickerActivity,
                builder.mediaType,
                builder.savedDirectoryName
        )
        TedRxOnActivityResult.with(this@TedImagePickerActivity)
                .startActivityForResult(cameraIntent)
                .subscribe { activityResult: ActivityResult ->
                    if (activityResult.resultCode == Activity.RESULT_OK) {
                        MediaUtil.scanMedia(this, uri)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe {
                                    loadMedia(true)
                                    onMediaClick(uri)
                                }
                    }
                }
    }


    private fun onMediaClick(uri: Uri) {
        when (builder.selectType) {
            SelectType.SINGLE -> onSingleMediaClick(uri)
            SelectType.MULTI -> onMultiMediaClick(uri)
        }
    }

    public fun onMultiMediaClick(uri: Uri) {


        var imagepath = getPathFromGooglePhotosUri(uri)
        //  val f = File(uri.path)
        ///   val filesize = f.length()

        ///   Log.e("filesize===", filesize.toString())
        ////   Log.e("itemssize===", totalsize.toString())
        Log.e("imagepath==", imagepath.toString());

        if (mediaAdapter.selectedUriList.contains(uri)) {

            totalsize=totalsize - printFileSize(imagepath)!!;
            Log.e("totalsize0==", totalsize.toString());
            mediaAdapter.toggleMediaSelect(uri)
            binding.layoutContent.items = mediaAdapter.selectedUriList
            updateSelectedMediaView()
            setupButtonVisibility()
        }
        else {
            totalsize = totalsize + printFileSize(imagepath)!!;
            Log.e("totalsize1==", totalsize.toString());
            // mediaAdapter.toggleMediaSelect(uri)
            if(totalsize>140)
            {

                totalsize=totalsize - printFileSize(imagepath)!!;
                Log.e("totalsize2===", totalsize.toString())
                //Toast.makeText(applicationContext,"Max. Allowable pics size is 150 mb",Toast.LENGTH_SHORT).show();
                Toast.makeText(applicationContext,"You reach maximum allowable uploading limit",Toast.LENGTH_SHORT).show();
            } else {
                Log.e("totalsize3===", totalsize.toString())
                mediaAdapter.toggleMediaSelect(uri)
                binding.layoutContent.items = mediaAdapter.selectedUriList
                updateSelectedMediaView()
                setupButtonVisibility()
            }
        }




    }

    private fun setupSelectedMediaView() {
        binding.layoutContent.viewSelectedMedia.run {
            if (mediaAdapter.selectedUriList.size > 0) {
                layoutParams.height =
                        resources.getDimensionPixelSize(R.dimen.ted_image_picker_selected_view_height)
            } else {
                layoutParams.height = 0
            }
            requestLayout()
        }
    }

    private fun updateSelectedMediaView() {
        Log.d("ted", "mediaAdapter.selectedUriList.size: ${mediaAdapter.selectedUriList.size}")
        binding.layoutContent.viewSelectedMedia.post {
            binding.layoutContent.viewSelectedMedia.run {
                if (mediaAdapter.selectedUriList.size > 0) {
                    slideView(
                            this,
                            layoutParams.height,
                            resources.getDimensionPixelSize(R.dimen.ted_image_picker_selected_view_height)
                    )
                } else if (mediaAdapter.selectedUriList.size == 0) {
                    slideView(this, layoutParams.height, 0)
                }
            }
        }
    }

    private fun slideView(view: View, currentHeight: Int, newHeight: Int) {
        val valueAnimator = ValueAnimator.ofInt(currentHeight, newHeight).apply {
            addUpdateListener {
                view.layoutParams.height = it.animatedValue as Int
                view.requestLayout()
            }
        }

        AnimatorSet().apply {
            interpolator = AccelerateDecelerateInterpolator()
            play(valueAnimator)
        }.start()
    }

    private fun onSingleMediaClick(uri: Uri) {
        val data = Intent().apply {
            putExtra(EXTRA_SELECTED_URI, uri)
        }
        setResult(Activity.RESULT_OK, data)
        finish()
    }

    override fun finish() {
        super.finish()
        finishAnimation()
    }

    private fun finishAnimation() {
        if (builder.finishEnterAnim != null && builder.finishExitAnim != null) {
            overridePendingTransition(builder.finishEnterAnim!!, builder.finishExitAnim!!)
        }
    }
    private fun onMultiMediaDone() {


        val selectedUriList =selectedMediaAdapter.getItemList()

        Log.e("selectedUrilList==", selectedMediaAdapter.getItemList().toString())

        if (selectedUriList.size < builder.minCount) {
            val message = builder.minCountMessage ?: getString(builder.minCountMessageResId)
            ToastUtil.showToast(message)
        } else {

            val data = Intent().apply {
                putParcelableArrayListExtra(
                        EXTRA_SELECTED_URI_LIST,
                        ArrayList(selectedUriList)
                )
            }
            setResult(Activity.RESULT_OK, data)
            finish()
        }

    }
//    private fun onMultiMediaDone() {
//
//        selectedMediaAdapter.getItemList()
//        val selectedUriList = mediaAdapter.selectedUriList
//        if (selectedUriList.size < builder.minCount) {
//            val message = builder.minCountMessage ?: getString(builder.minCountMessageResId)
//            ToastUtil.showToast(message)
//        } else {
//
//            val data = Intent().apply {
//                putParcelableArrayListExtra(
//                    EXTRA_SELECTED_URI_LIST,
//                    ArrayList(selectedUriList)
//                )
//            }
//            setResult(Activity.RESULT_OK, data)
//            finish()
//        }
//
//    }


    private fun setSelectedAlbum(selectedPosition: Int) {
        val album = albumAdapter.getItem(selectedPosition)
        if (this.selectedPosition == selectedPosition && binding.selectedAlbum == album) {
            return
        }

        binding.selectedAlbum = album
        this.selectedPosition = selectedPosition
        albumAdapter.setSelectedAlbum(album)
        mediaAdapter.replaceAll(album.mediaUris)
        binding.layoutContent.rvMedia.layoutManager?.scrollToPosition(0)
    }

    private fun setupListener() {
        binding.viewSelectedAlbum.setOnClickListener {
            binding.drawerLayout.toggle()
        }

        binding.viewDoneTop.root.setOnClickListener {
            onMultiMediaDone()
        }
        binding.viewDoneBottom.root.setOnClickListener {
            onMultiMediaDone()
        }

        binding.viewSelectedAlbumDropDown.setOnClickListener {
            binding.isAlbumOpened = !binding.isAlbumOpened
        }

    }

    private fun setupAlbumType() {
        if (builder.albumType == AlbumType.DRAWER) {
            binding.viewSelectedAlbumDropDown.visibility = View.GONE
        } else {
            binding.viewBottom.visibility = View.GONE
            binding.drawerLayout.setLock(true)
        }
    }


    override fun onBackPressed() {
        if (isAlbumOpened()) {
            closeAlbum()
        } else {
            super.onBackPressed()
        }

    }

    private fun isAlbumOpened(): Boolean =
            if (builder.albumType == AlbumType.DRAWER) {
                binding.drawerLayout.isOpen()
            } else {
                binding.isAlbumOpened
            }

    private fun closeAlbum() {

        if (builder.albumType == AlbumType.DRAWER) {
            binding.drawerLayout.close()
        } else {
            binding.isAlbumOpened = false
        }
    }

    override fun onDestroy() {
        if (!disposable.isDisposed) {
            disposable.dispose()
        }
        super.onDestroy()
    }


    companion object {

        private const val IMAGE_SPAN_COUNT = 3
        private const val EXTRA_BUILDER = "EXTRA_BUILDER"
        private const val EXTRA_SELECTED_URI = "EXTRA_SELECTED_URI"
        private const val EXTRA_SELECTED_URI_LIST = "EXTRA_SELECTED_URI_LIST"

        internal fun getIntent(context: Context, builder: TedImagePickerBaseBuilder<*>) =
                Intent(context, TedImagePickerActivity::class.java)
                        .apply {
                            putExtra(EXTRA_BUILDER, builder)
                        }

        internal fun getSelectedUri(data: Intent): Uri? =
                data.getParcelableExtra(EXTRA_SELECTED_URI)

        internal fun getSelectedUriList(data: Intent): List<Uri>? =
                data.getParcelableArrayListExtra(EXTRA_SELECTED_URI_LIST)
    }

    override fun onStartDrag(viewHolder: SelectedMediaAdapter.MediaViewHolder) {
        touchHelper.startDrag(viewHolder)
    }
    fun printFileSize(fileName: String?): Int? {
        val file = File(fileName)
        Log.e("filename==", file.toString())

        if (file.exists()) {

            // size of a file (in bytes)
            val bytes = file.length()
            val kilobytes = bytes / 1024
            val megabytes = kilobytes / 1024

            Log.e("bytes", bytes.toString())
            Log.e("kilobytes", kilobytes.toString())
            Log.e("megabytes", megabytes.toString())

            //Toast.makeText(applicationContext,"Size=="+ megabytes.toString(),Toast.LENGTH_SHORT).show();

            return megabytes.toInt();
        } else {
            println("File does not exist!")
        }
        return null;
    }
    fun printFileSize1(file: File?): Int? {
        // val file = File(fileName)
        Log.e("filename==", file.toString())

        if (file != null) {
            if (file.exists()) {

                // size of a file (in bytes)
                val bytes = file.length()
                val kilobytes = bytes / 1024
                val megabytes = kilobytes / 1024

                Log.e("bytes", bytes.toString())
                Log.e("kilobytes", kilobytes.toString())
                Log.e("megabytes", megabytes.toString())

                //   Toast.makeText(applicationContext,"Size=="+ megabytes.toString(),Toast.LENGTH_SHORT).show();

                return megabytes.toInt();
            } else {
                println("File does not exist!")
            }
        }
        return null;
    }
    fun getPathFromGooglePhotosUri(uriPhoto: Uri?): String? {
        if (uriPhoto == null) return null
        var input: FileInputStream? = null
        var output: FileOutputStream? = null
        try {
            val pfd: ParcelFileDescriptor? = contentResolver.openFileDescriptor(uriPhoto, "r")
            val fd: FileDescriptor = pfd!!.getFileDescriptor()
            input = FileInputStream(fd)
            val tempFilename: String = getTempFilename(this)
            output = FileOutputStream(tempFilename)
            var read: Int = 0
            val bytes = ByteArray(4096)
            while (input.read(bytes).also({ read = it }) != -1) {
                output.write(bytes, 0, read)
            }

            return tempFilename
        } catch (ignored: IOException) {
            // Nothing we can do
        } finally {
            closeSilently(input)
            closeSilently(output)
        }
        return null
    }

    @Throws(IOException::class)
    private fun getTempFilename(context: Context): String {
        val outputDir = context.cacheDir
        val outputFile = File.createTempFile("image", "tmp", outputDir)
        return outputFile.absolutePath
    }

    fun closeSilently(c: Closeable?) {
        if (c == null) return
        try {
            c.close()
        } catch (t: Throwable) {
            // Do nothing
        }
    }
}

