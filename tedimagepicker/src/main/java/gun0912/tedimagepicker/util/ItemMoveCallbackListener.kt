package gun0912.tedimagepicker.util

import android.os.Handler
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.core.os.HandlerCompat.postDelayed
import gun0912.tedimagepicker.adapter.SelectedMediaAdapter



internal class ItemMoveCallbackListener(val adapter: SelectedMediaAdapter) : ItemTouchHelper.Callback() {
    override fun getMovementFlags(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder
    ): Int {
        val dragFlags = ItemTouchHelper.UP or ItemTouchHelper.DOWN or ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
        return makeMovementFlags(dragFlags, 0)
    }

    override fun isItemViewSwipeEnabled(): Boolean {
        return false
    }

    override fun isLongPressDragEnabled(): Boolean {
        return false
    }

    override fun onMove(
        recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        adapter.onRowMoved(viewHolder.adapterPosition, target.adapterPosition,
            viewHolder as SelectedMediaAdapter.MediaViewHolder)

        return true
    }

    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
        if (actionState != ItemTouchHelper.ACTION_STATE_DRAG) {
            if (viewHolder is SelectedMediaAdapter.MediaViewHolder) {
                adapter.onRowSelected(viewHolder)
            }

            Handler().postDelayed({   adapter.notifyDataSetChanged() }, 1000)

        }
        super.onSelectedChanged(viewHolder, actionState)
    }

    override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
        super.clearView(recyclerView, viewHolder)
        if (viewHolder is SelectedMediaAdapter.MediaViewHolder) {
            adapter.onRowClear(viewHolder)

        }
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
    }

    interface Listener {
        fun onRowMoved(fromPosition: Int, toPosition: Int,itemViewHolder: SelectedMediaAdapter.MediaViewHolder)
        fun onRowSelected(itemViewHolder: SelectedMediaAdapter.MediaViewHolder)
        fun onRowClear(itemViewHolder: SelectedMediaAdapter.MediaViewHolder)
    }
}