package gun0912.tedimagepicker.util

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.widget.Toast
import gun0912.tedimagepicker.R

object ToastUtil {
    lateinit var context: Context
    var progressDialog: ProgressDialog? = null
    var toastAction: ((String) -> Unit)? = null

    fun showToast(text: String) {
        toastAction?.invoke(text) ?: Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
    }

    fun showProgressDialogWithTitle(activity: Activity?, substring: String) {
        progressDialog =  ProgressDialog(activity, R.style.AppCompatAlertDialogStyle)
        progressDialog!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        //Without this user can hide loader by tapping outside screen
        progressDialog!!.setCancelable(false)
        progressDialog!!.setMessage(substring)
        progressDialog!!.show()
    }

}
