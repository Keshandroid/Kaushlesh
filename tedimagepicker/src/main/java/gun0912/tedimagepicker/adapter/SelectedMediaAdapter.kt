package gun0912.tedimagepicker.adapter

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.NO_POSITION
import com.bumptech.glide.Glide
import gun0912.tedimagepicker.R
import gun0912.tedimagepicker.TedImagePickerActivity
import gun0912.tedimagepicker.base.BaseRecyclerViewAdapter
import gun0912.tedimagepicker.base.BaseViewHolder
import gun0912.tedimagepicker.databinding.ItemSelectedMediaBinding
import java.util.*
import gun0912.tedimagepicker.util.*

class SelectedMediaAdapter(private val startDragListener: OnStartDragListener) : BaseRecyclerViewAdapter<Uri, SelectedMediaAdapter.MediaViewHolder>(),ItemMoveCallbackListener.Listener{
    private val TAG : String = SelectedMediaAdapter::class.simpleName!!

    // lateinit var list : MutableList<Uri>= ArrayList()
   // val list = mutableListOf<Uri>()
    var onClearClickListener: ((Uri) -> Unit)? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    var isMoving = false

    override fun getViewHolder(parent: ViewGroup, viewType: ViewType) = MediaViewHolder(parent)



    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        layoutManager = recyclerView.layoutManager

    }


    inner class MediaViewHolder(parent: ViewGroup) : BaseViewHolder<ItemSelectedMediaBinding, Uri>(parent, R.layout.item_selected_media) {
      //  holder=MediaViewHolder

        init {
            binding.ivClear.setOnClickListener {
                val item = getItem(adapterPosition.takeIf { it != NO_POSITION }
                    ?: return@setOnClickListener)
                onClearClickListener?.invoke(item)
            }
            binding.ivImage.setOnTouchListener(View.OnTouchListener { v, event ->
                if (event.action == MotionEvent.ACTION_MOVE) {
                    isMoving = true
                    Log.i("isMoving:", "true")
                }
                false
            })

            binding.ivImage.setOnLongClickListener(View.OnLongClickListener {
                if (isMoving) {
                    startDragListener.onStartDrag(this@MediaViewHolder)
                }
                false
            })

        }

        override fun bind(data: Uri) {
            Log.e(TAG, "bind Called with position : $adapterPosition URI : ${data.toString()}")
            binding.uri = data
            ///list.add(data)

        }

        override fun recycled() {
            if ((itemView.context as? Activity)?.isDestroyed == true) {
                return
            }
            Log.e("itemView==", itemView.toString())
            Glide.with(itemView).clear(binding.ivImage)

        }


    }

    override fun onRowMoved(fromPosition: Int, toPosition: Int,itemViewHolder: MediaViewHolder) {
//        Log.e("onRowMoved", "onRowMoved")
//        Log.e("fromPosition", fromPosition.toString())
//        Log.e("toPosition", toPosition.toString())
//        Log.e("binding.list-size==", list.size.toString())
//        Log.e("Collections1=", list.toString())

        Log.d(TAG,
            "onRowMoved() called with: fromPosition = $fromPosition, toPosition = $toPosition")

//
//
        if (fromPosition < toPosition) {
            for (i in fromPosition until toPosition) {
                Collections.swap(items, i, i + 1)
                //itemViewHolder.binding.uri = list.get(i)
            }
        } else {
            for (i in fromPosition downTo toPosition + 1) {
                Collections.swap(items, i, i - 1)
                ///itemViewHolder.binding.uri = list.get(i)
            }
        }
//        Log.e(TAG,"Swaping done");
//       // notifyDataSetChanged()
      notifyItemMoved(fromPosition, toPosition)

        Log.e("PickerItems","Items : $items")

    }

    override fun onRowSelected(itemViewHolder: MediaViewHolder) {
        Log.e("onRowSelected", "onRowSelected")
        itemViewHolder.binding.ivImage.setBackgroundColor(Color.GRAY);
    }

    override fun onRowClear(itemViewHolder: MediaViewHolder) {
        itemViewHolder.binding.ivImage.setBackgroundColor(Color.WHITE);
    }

    interface OnStartDragListener {
        fun onStartDrag(viewHolder:MediaViewHolder)
    }
}