package com.kaushlesh.view.activity.AdShowcase.Jwellery

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.view.contains
import androidx.recyclerview.widget.LinearLayoutManager
import com.kaushlesh.R
import com.kaushlesh.adapter.AdShowcase.JewellreyFillIndividualCategoryAdapter
import kotlinx.android.synthetic.main.activity_jewellery_individual_category.*
import kotlinx.android.synthetic.main.layout_toolbar.*


class JewelleryIndividualCategory : AppCompatActivity(), View.OnClickListener {
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    lateinit var adapter: JewellreyFillIndividualCategoryAdapter
    var arraylist = ArrayList<View>()
    var arraylist1 = ArrayList<Int>()

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_jewellery_individual_category)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        toolbar = findViewById(R.id.toolbar)
        tvtoolbartitle = toolbar.findViewById(R.id.title)
        tvtoolbartitle.text = resources.getText(R.string.txt_fill_individual_detail)
        arraylist1.add(1)

        addcalll()

        add.setOnClickListener(this)
        btn_back.setOnClickListener(this)
    }

    private fun addcalll() {
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_all_add.layoutManager = layoutManager
         adapter = JewellreyFillIndividualCategoryAdapter(arraylist1, this)
        rv_all_add.adapter = adapter
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btn_back -> {
                onBackPressed()
            }
            R.id.add -> {

                arraylist1.add(2)
                addcalll()

            }
        }
    }
}