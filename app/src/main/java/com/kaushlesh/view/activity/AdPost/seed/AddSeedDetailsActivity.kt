package com.kaushlesh.view.activity.AdPost.seed

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.kaushlesh.R
import com.kaushlesh.bean.AdPost.AdGrainSeedBean
import com.kaushlesh.view.activity.AdPost.SetPriceActivity
import com.kaushlesh.constant.AppConstants.printLog
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.utils.validations.AdPostPropertyValidation
import com.kaushlesh.view.activity.AdPost.UploadPhotoActivity
import kotlinx.android.synthetic.main.activity_add_brand_detail.*
import kotlinx.android.synthetic.main.activity_add_seed_details.*
import kotlinx.android.synthetic.main.activity_add_seed_details.et_ad_title
import kotlinx.android.synthetic.main.activity_add_seed_details.et_other_info
import java.util.ArrayList

class AddSeedDetailsActivity : AppCompatActivity(), View.OnClickListener {
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal lateinit var rltype: RelativeLayout
    internal lateinit var rlstock: RelativeLayout
    internal lateinit var rlprice: RelativeLayout
    internal lateinit var sptype: Spinner
    internal lateinit var spstock: Spinner
    internal lateinit var spprice: Spinner
    internal lateinit var tvtype: TextView
    internal lateinit var tvstock: TextView
    internal lateinit var tvprice: TextView
    internal lateinit var ibtype: ImageButton
    internal lateinit var ibstock: ImageButton
    internal lateinit var ibprice: ImageButton
    internal lateinit var btnnext: Button
    internal lateinit var etadtitle: EditText
    internal lateinit var etotherinfo: EditText
    internal lateinit var tv1: TextView
    internal lateinit var tv2: TextView
    internal lateinit var type_spinner_type: ArrayAdapter<String>
    internal lateinit var stock_spinner_type: ArrayAdapter<String>
    internal lateinit var price_spinner_type: ArrayAdapter<String>
    internal var typelist: MutableList<String> = ArrayList()
    internal var list: MutableList<String> = ArrayList()
    internal lateinit var tvfarmer: TextView
    internal lateinit var tvdealer: TextView
    internal lateinit var tvwholeseller: TextView
    internal var farmer = false
    internal var dealer = false
    internal var wholeseller = false

    lateinit var storeUserData: StoreUserData
    lateinit var adGrainSeedBean: AdGrainSeedBean
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_seed_details)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        storeUserData=StoreUserData(this)
        adGrainSeedBean = AdGrainSeedBean()

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.txt_grain_seed_sell)

        initBindViews()

        Utils.setontouch(et_other_info)
        Utils.setontouch1(et_ad_title)

    }

    private fun initBindViews() {

        rltype = findViewById(R.id.rl_grain_type)
        sptype = findViewById(R.id.spinner_grain_type)
        tvtype = findViewById(R.id.tv_grain_type)
        ibtype = findViewById(R.id.ib_grain_type)
        rlstock = findViewById(R.id.rl_stock_type)
        spstock = findViewById(R.id.spinner_stock_type)
        tvstock = findViewById(R.id.tv_stock_type)
        ibstock = findViewById(R.id.ib_stock_type)
        rlprice = findViewById(R.id.rl_price_type)
        spprice = findViewById(R.id.spinner_price_type)
        tvprice = findViewById(R.id.tv_price_type)
        ibprice = findViewById(R.id.ib_price_type)


        btnnext = findViewById(R.id.btn_next)
        etadtitle = findViewById(R.id.et_ad_title)
        etotherinfo = findViewById(R.id.et_other_info)
        tv1 = findViewById(R.id.tv_1)
        tv2 = findViewById(R.id.tv_2)

        tvfarmer = findViewById(R.id.tvfarmer)
        tvdealer = findViewById(R.id.tvdealer)
        tvwholeseller = findViewById(R.id.tvwholeseller)

        //tvfarmer.setBackgroundResource(R.drawable.bg_edittext_black)

        btnback.setOnClickListener(this)
        btnnext.setOnClickListener(this)
        rltype.setOnClickListener(this)
        ibtype.setOnClickListener(this)
        rlstock.setOnClickListener(this)
        ibstock.setOnClickListener(this)
        rlprice.setOnClickListener(this)
        ibprice.setOnClickListener(this)
        tvfarmer.setOnClickListener(this)
        tvdealer.setOnClickListener(this)
        tvwholeseller.setOnClickListener(this)

        typelist.add("Wheat")
        typelist.add("Rice")

        list.add("Kg")
        list.add("Gm")

        type_spinner_type = ArrayAdapter(
            applicationContext,
            android.R.layout.simple_spinner_dropdown_item,
            typelist
        )

        // Creating adapter for spinner
        val dataAdapter1 =
            ArrayAdapter(applicationContext, android.R.layout.simple_spinner_item, typelist)

        // Drop down layout style - list view with radio button
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        // attaching data adapter to spinner
        sptype.adapter = dataAdapter1

        sptype.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>,
                view: View,
                position: Int,
                l: Long
            ) {
                printLog(TAG, "onItemSelected: ===" + adapterView.selectedItem)

            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        }

        stock_spinner_type =
            ArrayAdapter(applicationContext, android.R.layout.simple_spinner_dropdown_item, list)


        // Creating adapter for spinner
        val dataAdapter11 =
            ArrayAdapter(applicationContext, android.R.layout.simple_spinner_item, list)

        // Drop down layout style - list view with radio button
        dataAdapter11.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        // attaching data adapter to spinner
        spstock.adapter = dataAdapter11

        spstock.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>,
                view: View,
                position: Int,
                l: Long
            ) {
                printLog(TAG, "onItemSelected: ===" + adapterView.selectedItem)

            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        }


        price_spinner_type =
            ArrayAdapter(applicationContext, android.R.layout.simple_spinner_dropdown_item, list)


        // Creating adapter for spinner
        val dataAdapter12 =
            ArrayAdapter(applicationContext, android.R.layout.simple_spinner_item, list)

        // Drop down layout style - list view with radio button
        dataAdapter12.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        // attaching data adapter to spinner
        spprice.adapter = dataAdapter12

        spprice.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>,
                view: View,
                position: Int,
                l: Long
            ) {
                printLog(TAG, "onItemSelected: ===" + adapterView.selectedItem)

            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        }



        etadtitle.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {


                tv1.text = s.length.toString() + "/77"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })

        etotherinfo.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {


                tv2.text = s.length.toString() + "/5000"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_back -> onBackPressed()

            R.id.btn_next -> {
                /*val intent = Intent(applicationContext, UploadPhotoActivity::class.java)
                startActivity(intent)*/
                validations()
            }

            R.id.rl_grain_type -> {

                tvtype.visibility = View.GONE
                sptype.visibility = View.VISIBLE
                sptype.performClick()
            }

            R.id.ib_grain_type -> {

                tvtype.visibility = View.GONE
                sptype.visibility = View.VISIBLE
                rltype.performClick()
            }

            R.id.rl_stock_type -> {

                tvstock.visibility = View.GONE
                spstock.visibility = View.VISIBLE
                spstock.performClick()
            }

            R.id.ib_stock_type -> {

                tvstock.visibility = View.GONE
                spstock.visibility = View.VISIBLE
                rlstock.performClick()
            }

            R.id.rl_price_type -> {

                tvprice.visibility = View.GONE
                spprice.visibility = View.VISIBLE
                spprice.performClick()
            }

            R.id.ib_price_type -> {

                tvprice.visibility = View.GONE
                spprice.visibility = View.VISIBLE
                rlprice.performClick()
            }

            R.id.tvfarmer -> {
                farmer = true
                dealer = false
                wholeseller = false

                tvfarmer.setBackgroundResource(R.drawable.bg_edittext_black)
                tvdealer.setBackgroundResource(R.drawable.bg_edittext)
                tvwholeseller.setBackgroundResource(R.drawable.bg_edittext)

                tvfarmer.setTextColor(resources.getColor(R.color.black))
                tvdealer.setTextColor(resources.getColor(R.color.gray))
                tvwholeseller.setTextColor(resources.getColor(R.color.gray))

                adGrainSeedBean.listed_by= "1"
            }

            R.id.tvdealer -> {
                farmer = false
                dealer = true
                wholeseller = false

                tvfarmer.setBackgroundResource(R.drawable.bg_edittext)
                tvdealer.setBackgroundResource(R.drawable.bg_edittext_black)
                tvwholeseller.setBackgroundResource(R.drawable.bg_edittext)

                tvfarmer.setTextColor(resources.getColor(R.color.gray))
                tvdealer.setTextColor(resources.getColor(R.color.black))
                tvwholeseller.setTextColor(resources.getColor(R.color.gray))

                adGrainSeedBean.listed_by= "2"
            }

            R.id.tvwholeseller -> {
                farmer = false
                dealer = false
                wholeseller = true

                tvfarmer.setBackgroundResource(R.drawable.bg_edittext)
                tvdealer.setBackgroundResource(R.drawable.bg_edittext)
                tvwholeseller.setBackgroundResource(R.drawable.bg_edittext_black)

                tvfarmer.setTextColor(resources.getColor(R.color.gray))
                tvdealer.setTextColor(resources.getColor(R.color.gray))
                tvwholeseller.setTextColor(resources.getColor(R.color.black))

                adGrainSeedBean.listed_by= "3"
            }
        }/* new AlertDialog.Builder(AddSeedDetailsActivity.this)
                        .setAdapter(type_spinner_type, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                String selecteduser = typelist.get(which);
                                AppConstants.printLog(TAG, "====Selected type =====" + selecteduser);
                                tvtype.setText(selecteduser);
                                dialog.dismiss();

                            }
                        }).create().show();*//*    new AlertDialog.Builder(AddSeedDetailsActivity.this)
                        .setAdapter(stock_spinner_type, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                String selecteduser = list.get(which);
                                AppConstants.printLog(TAG, "====Selected type =====" + selecteduser);
                                tvstock.setText(selecteduser);
                                dialog.dismiss();

                            }
                        }).create().show();*//*new AlertDialog.Builder(AddSeedDetailsActivity.this)
                        .setAdapter(price_spinner_type, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                String selecteduser = list.get(which);
                                AppConstants.printLog(TAG, "====Selected type =====" + selecteduser);
                                tvprice.setText(selecteduser);
                                dialog.dismiss();

                            }
                        }).create().show();*/
    }

    private fun validations() {

        val issubmit = AdPostPropertyValidation.checkForGrainSeed(this,adGrainSeedBean,et_stock,et_price,etadtitle,etotherinfo)
        Utils.showLog(TAG,"status" +  issubmit)
        if(issubmit) {
            adGrainSeedBean.category_id = storeUserData.getString(Constants.CATEGORY_ID)
            adGrainSeedBean.sub_category_id = storeUserData.getString(Constants.SUBCATEGORYID)
            adGrainSeedBean.available_stock = et_stock.text.toString()
            adGrainSeedBean.price = et_price.text.toString()
            adGrainSeedBean.title = etadtitle.text.toString()
            adGrainSeedBean.other_information = etotherinfo.text.toString()

            val gson = Gson()
            val json = gson.toJson(adGrainSeedBean)
            storeUserData.setString(Constants.ADDGRAINSEED,json)

            Log.e("save", "adOfficeForSell save data: "+storeUserData.getString(Constants.ADDGRAINSEED))

            val intent = Intent(applicationContext, UploadPhotoActivity::class.java)
            startActivity(intent)
        }
    }

    companion object {

        private val TAG = "AddSeedDetailsActivity"
    }
}
