package com.kaushlesh.view.activity.AdPost.Property

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.ParcelFileDescriptor
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.kaushlesh.R
import com.kaushlesh.bean.AdPost.ApVillsellandRentBean
import com.kaushlesh.bean.AdPost.LabourWareHouseBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.utils.AdPostImages
import com.kaushlesh.utils.validations.AdPostPropertyValidation
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.AdPost.UploadActivity1
import com.kaushlesh.view.activity.AdPost.UploadPhotoActivity
import gun0912.tedimagepicker.builder.TedImagePicker
import kotlinx.android.synthetic.main.activity_ap_villa_sell_and_rent.*
import kotlinx.android.synthetic.main.activity_attention_ad_post.*
import kotlinx.android.synthetic.main.activity_labour_detail.*
import kotlinx.android.synthetic.main.activity_labour_detail.et_ad_title
import kotlinx.android.synthetic.main.activity_labour_detail.et_other_info
import kotlinx.android.synthetic.main.activity_labour_detail.tv_1
import java.io.*

class LabourDetailActivity : AppCompatActivity(), View.OnClickListener {
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal lateinit var btnnext: Button
    internal lateinit var tvwarehouse: TextView
    internal lateinit var tvlabourcamp: TextView
    internal lateinit var tvrent: TextView
    internal lateinit var tvsell: TextView
    internal lateinit var etadtitile: EditText
    internal lateinit var etotherinfo: EditText
    internal lateinit var tvbuilder: TextView
    internal lateinit var tvdealer: TextView
    internal lateinit var tvowner: TextView

    internal var warehouse: Boolean? = false
    internal var labourcamp: Boolean? = false
    internal var rent: Boolean? = false
    internal var sell: Boolean? = false
    internal var builder: Boolean? = false
    internal var dealer: Boolean? = false
    internal var owner: Boolean? = false
    lateinit var labourWareHouseBean: LabourWareHouseBean
    lateinit var storeUserData: StoreUserData
    lateinit var resizebitmapmain: Bitmap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_labour_detail)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        storeUserData = StoreUserData(this)
        labourWareHouseBean = LabourWareHouseBean()

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.txt_include_details)

        initBindViews()
    }

    private fun initBindViews() {
        btnnext = findViewById(R.id.btn_next)

        tvwarehouse = findViewById(R.id.tvwarehouse)
        tvlabourcamp = findViewById(R.id.tvlabourcamp)
        tvrent = findViewById(R.id.tvrent)
        tvsell = findViewById(R.id.tvsell)
        etadtitile = findViewById(R.id.et_ad_title)
        etotherinfo = findViewById(R.id.et_other_info)
        tvbuilder = findViewById(R.id.tvbuilder)
        tvdealer = findViewById(R.id.tvdealer)
        tvowner = findViewById(R.id.tvowner)


        //setbackgroundSelected(R.drawable.bg_edittext_black)

        btnback.setOnClickListener(this)
        btnnext.setOnClickListener(this)

        tvwarehouse.setOnClickListener(this)
        tvlabourcamp.setOnClickListener(this)

        tvrent.setOnClickListener(this)
        tvsell.setOnClickListener(this)


        tvbuilder.setOnClickListener(this)
        tvdealer.setOnClickListener(this)
        tvowner.setOnClickListener(this)

        Utils.setontouch(et_other_info)
        Utils.setontouch1(et_ad_title)

        etadtitile.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {


                tv_1.text = s.length.toString() + "/77"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })

        etotherinfo.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {


                tv_2.text = s.length.toString() + "/5000"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })
        labourWareHouseBean.property_type= ""
        labourWareHouseBean.purpose= ""
        labourWareHouseBean.listed_by =""

    }

    private fun setbackgroundSelected(b: Int) {
        tvwarehouse.setBackgroundResource(b)
        tvrent.setBackgroundResource(b)
        tvowner.setBackgroundResource(b)
    }

    override fun onClick(view: View) {
        when (view.id) {

            R.id.btn_back -> onBackPressed()

            R.id.btn_next -> {
                //val intent = Intent(applicationContext, UploadPhotoActivity::class.java)
                //startActivity(intent)
                validations()

            }

            R.id.tvwarehouse -> {
                setValue(true, false, "type")
                setBottomSelection(
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.color.black,
                    R.color.gray,
                    "type"
                )

                labourWareHouseBean.property_type ="10"
            }

            R.id.tvlabourcamp -> {
                setValue(false, true, "type")
                setBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.black,
                    "type"
                )
                labourWareHouseBean.property_type ="11"
            }

            R.id.tvrent -> {
                setValue(true, false, "purpose")
                setBottomSelection(
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.color.black,
                    R.color.gray,
                    "purpose"
                )
                labourWareHouseBean.purpose ="1"
            }

            R.id.tvsell -> {
                setValue(false, true, "purpose")
                setBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.black,
                    "purpose"
                )
                labourWareHouseBean.purpose ="2"
            }

            R.id.tvowner -> {
                setTypeValue(true, false, false, "listedby")
                setTypeBottomSelection(
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    "listedby"
                )
                labourWareHouseBean.listed_by ="3"
            }

            R.id.tvdealer -> {
                setTypeValue(false, true, false, "listedby")
                setTypeBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    "listedby"
                )
                labourWareHouseBean.listed_by ="2"
            }

            R.id.tvbuilder -> {
                setTypeValue(false, false, true, "listedby")
                setTypeBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    "listedby"
                )
                labourWareHouseBean.listed_by ="1"
            }
        }
    }

    private fun setTypeValue(tb1: Boolean, tb2: Boolean, tb3: Boolean, type: String) {

        if (type.equals("listedby", ignoreCase = true)) {
            owner = tb1
            dealer = tb2
            builder = tb3
        }
    }

    private fun setTypeBottomSelection(
        t1: Int,
        t2: Int,
        t3: Int,
        t4: Int,
        t5: Int,
        t6: Int,
        type: String
    ) {

        if (type.equals("listedby", ignoreCase = true)) {
            tvowner.setBackgroundResource(t1)
            tvdealer.setBackgroundResource(t2)
            tvbuilder.setBackgroundResource(t3)

            tvowner.setTextColor(resources.getColor(t4))
            tvdealer.setTextColor(resources.getColor(t5))
            tvbuilder.setTextColor(resources.getColor(t6))
        }

    }
    private fun setValue(b1: Boolean, b2: Boolean, type: String) {

        if (type.equals("type", ignoreCase = true)) {
            warehouse = b1
            labourcamp = b2
        }

        if (type.equals("purpose", ignoreCase = true)) {
            rent = b1
            sell = b2
        }
    }

    private fun setBottomSelection(b1: Int, b2: Int, b3: Int, b4: Int, type: String) {
        if (type.equals("type", ignoreCase = true)) {

            tvwarehouse.setBackgroundResource(b1)
            tvlabourcamp.setBackgroundResource(b2)

            tvwarehouse.setTextColor(resources.getColor(b3))
            tvlabourcamp.setTextColor(resources.getColor(b4))
        }

        if (type.equals("purpose", ignoreCase = true)) {
            tvrent.setBackgroundResource(b1)
            tvsell.setBackgroundResource(b2)

            tvrent.setTextColor(resources.getColor(b3))
            tvsell.setTextColor(resources.getColor(b4))
        }
    }

    companion object {

        private val TAG = "LabourDetailActivity"
    }

    private fun validations() {

        val issubmit = AdPostPropertyValidation.checkForLabourWarehouse(this,labourWareHouseBean,etadtitile,etotherinfo)
        Utils.showLog(TAG,"status" +  issubmit)
        if(issubmit) {

            labourWareHouseBean.category_id = storeUserData.getString(Constants.CATEGORY_ID)
            labourWareHouseBean.sub_category_id = storeUserData.getString(Constants.SUBCATEGORYID)

            labourWareHouseBean.title= etadtitile.text.toString()
            labourWareHouseBean.other_information = etotherinfo.text.toString()

            val gson = Gson()
            val json = gson.toJson(labourWareHouseBean)
            storeUserData.setString(Constants.ADDLABOURWAREHOUSE,json)

            Log.e("save", "adOfficeForSell save data: "+storeUserData.getString(Constants.ADDLABOURWAREHOUSE))

            //val intent = Intent(applicationContext, UploadPhotoActivity::class.java)
            //startActivity(intent)

            setNormalMultiButton()
        }
    }
    fun setNormalMultiButton() {
        val selectedUriList: List<Uri>? = null

        TedImagePicker.with(this)
                .max(12, "Maximum 12 photos allowed")
                .errorListener { message ->
                    Log.d("ted", "message: $message")
                }
                .selectedUri(selectedUriList)
                .startMultiImage { list: List<Uri> ->
                    Log.e("list===", "listsize: ${list.size}")
                    showMultiImage1(list)
                }

    }

    private fun showMultiImage1(uriList: List<Uri>) {

        Log.d("ted=", "uriList: $uriList")
        Log.e("uriList.get(0)====", "uriList.get(0): $uriList.get(0)")

        Thread(Runnable {
            AppConstants.imageList.clear()
            AppConstants.imageListuri.clear()
            uriList.forEach {
                AppConstants.imageListuri.add(it)
                val imagepath = getPathFromGooglePhotosUri(it)
                resizebitmapmain = decodeImageFromFiles(imagepath, 280, 280)
                Log.e("resizebitmapmain====", "01--: $resizebitmapmain")
                val ei = ExifInterface(imagepath.toString())
                val orientation: Int = ei.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED
                )
                var rotatedBitmap: Bitmap? = null

                when (orientation) {
                    ExifInterface.ORIENTATION_ROTATE_90 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 90F)
                    ExifInterface.ORIENTATION_ROTATE_180 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 180F)
                    ExifInterface.ORIENTATION_ROTATE_270 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 270F)
                    ExifInterface.ORIENTATION_NORMAL -> rotatedBitmap = resizebitmapmain
                    else -> rotatedBitmap = resizebitmapmain
                }

                AppConstants.imageList.add(rotatedBitmap!!)
            }

            Log.e("imageListsize====", "viewSize: ${AppConstants.imageList.size}")

            if(AppConstants.imageList!=null)
            {
                Log.e("AppConstantsimageList==", "viewSize: ${AppConstants.imageList.size}")
                checkAndSaveLobourCamp(this)
            }

        }).start()


        AdPostImages.changeScreentoNext(this)

    }

    private fun checkAndSaveLobourCamp(context: Context) {
        ///  bimglist.clear()
        val storeUserData = StoreUserData(context)
        val gson = Gson()

        if (AppConstants.imageList.size >= 1) {

            Log.e("test", "imagelist: " + AppConstants.imageList.size+"===")

            val json: String = storeUserData.getString(Constants.ADDLABOURWAREHOUSE)

            Log.e("String", "String data: " + json+"====")

            val labourWareHouseBean = gson.fromJson(json, LabourWareHouseBean::class.java)

            Log.e("test", "listsave data: " + labourWareHouseBean.toString()+"====")
            labourWareHouseBean.post_images?.clear()
            labourWareHouseBean.post_images = AppConstants.imageList
            Log.e("test", "post_images: " +  labourWareHouseBean.post_images!!.size+"===")


            Log.e("test", "post_images data: " + labourWareHouseBean.post_images)

            val jsonset = gson.toJson(labourWareHouseBean)

            storeUserData.setString(Constants.ADDLABOURWAREHOUSE, jsonset)

            ////   AdPostImages.changeScreentoLocation(context)

        } else {
            Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
        }
    }

    fun rotateImage(source: Bitmap, angle: Float): Bitmap? {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(
                source, 0, 0, source.width, source.height,
                matrix, true
        )
    }

    fun decodeImageFromFiles(path: String?, width: Int, height: Int): Bitmap {
        val scaleOptions = BitmapFactory.Options()
        scaleOptions.inJustDecodeBounds = true
        BitmapFactory.decodeFile(path, scaleOptions)
        var scale = 1
        while (scaleOptions.outWidth / scale / 2 >= width
                && scaleOptions.outHeight / scale / 2 >= height
        ) {
            scale *= 2
        }
        // decode with the sample size
        val outOptions = BitmapFactory.Options()
        outOptions.inSampleSize = scale
        val bitmap: Bitmap = BitmapFactory.decodeFile(path, outOptions)
        Utils.showLog(
                UploadActivity1.TAG,
                "===BitmapFactory===width" + bitmap.width + "===height" + bitmap.height + "==="
        )

        return bitmap
    }

    fun getPathFromGooglePhotosUri(uriPhoto: Uri?): String? {
        if (uriPhoto == null) return null
        var input: FileInputStream? = null
        var output: FileOutputStream? = null
        try {
            val pfd: ParcelFileDescriptor? = contentResolver.openFileDescriptor(uriPhoto, "r")
            val fd: FileDescriptor = pfd!!.getFileDescriptor()
            input = FileInputStream(fd)
            val tempFilename: String = getTempFilename(this)
            output = FileOutputStream(tempFilename)
            var read: Int = 0
            val bytes = ByteArray(4096)
            while (input.read(bytes).also({ read = it }) != -1) {
                output.write(bytes, 0, read)
            }
            return tempFilename
        } catch (ignored: IOException) {
            // Nothing we can do
        } finally {
            closeSilently(input)
            closeSilently(output)
        }
        return null
    }

    @Throws(IOException::class)
    private fun getTempFilename(context: Context): String {
        val outputDir = context.cacheDir
        val outputFile = File.createTempFile("image", "tmp", outputDir)
        return outputFile.absolutePath
    }

    fun closeSilently(c: Closeable?) {
        if (c == null) return
        try {
            c.close()
        } catch (t: Throwable) {
            // Do nothing
        }
    }

}

