package com.kaushlesh.view.activity.productDetails

import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import android.widget.FrameLayout
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.kaushlesh.R
import com.kaushlesh.adapter.FullScreenImageAdapter
import com.kaushlesh.bean.PostDetails.postDetailsBean

class FullScreenImageViewActivity : AppCompatActivity() {

    private var adapter: FullScreenImageAdapter? = null
    private var viewPager: ViewPager? = null
    lateinit var mSliderItems: MutableList<postDetailsBean.PostImageX>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_screen_image_view)

        viewPager = findViewById<View>(R.id.pager) as? ViewPager
        viewPager?.offscreenPageLimit = 3

        val i = intent
        val position = i.getIntExtra("position", 0)
        mSliderItems = i.getSerializableExtra("list") as ArrayList<postDetailsBean.PostImageX>

        adapter = FullScreenImageAdapter(
            this,
            mSliderItems as ArrayList<postDetailsBean.PostImageX>
        )
        viewPager?.adapter = adapter
      ///  viewPager!!.setAdapter(adapter)

        // displaying selected image first

        // displaying selected image first
        viewPager?.setCurrentItem(position)
    }
}