package com.kaushlesh.view.activity.AdShowcase.Cars

import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.kaushlesh.R
import com.kaushlesh.adapter.AdShowcase.CarFillIndividualCategoryAdapter
import kotlinx.android.synthetic.main.activity_carshowroom_individual_categoery_about_car.*


class CarshowroomIndividualCategoeryAboutCar : AppCompatActivity() {

    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    lateinit var adapter: CarFillIndividualCategoryAdapter
    var fillCategoryListArray = ArrayList<Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_carshowroom_individual_categoery_about_car)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getText(R.string.txt_fill_car_individual_detail)
        fillCategoryListArray.add(1)

        addcalll()

        btnback.setOnClickListener {
            onBackPressed()
        }
        add.setOnClickListener {
            fillCategoryListArray.add(2)
            addcalll()
        }


    }

    private fun addcalll() {
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_all_add.layoutManager = layoutManager
        adapter = CarFillIndividualCategoryAdapter(fillCategoryListArray, this)
        rv_all_add.adapter = adapter
    }


    /*  fun onAddField() {
          val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
          val rowView: View = inflater.inflate(R.layout.item_carshowroom_individual_categoery_about_car, null)
          ll_mainlayout!!.addView(rowView, ll_mainlayout!!.childCount - 1)

          var b = ll_mainlayout!!.childCount - 1

          for (a in 0..b) {

              if (a == b) {

              } else {
                  rowView.rvlayout.isEnabled = false
                  rowView.tv_upload.isEnabled = false
                  rowView.spinner_car_model.isEnabled = false
                  rowView.et_engine.isEnabled = false
                  rowView.et_milage.isEnabled = false
                  rowView.et_showroom_price.isEnabled = false
                  rowView.et_discount_rs.isEnabled = false
                  rowView.et_road_price.isEnabled = false
                  rowView.et_desc_model.isEnabled = false
                  rowView.et_showroom_price.isEnabled = false
              }
          }
      }*/

}