package com.kaushlesh.view.activity.AdPost.Bikes

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.ParcelFileDescriptor
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.R
import com.kaushlesh.bean.AdPost.AdPostBikesBean
import com.kaushlesh.bean.MobileBrandBeans
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.constant.AppConstants.printLog
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.AdPostImages
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.utils.validations.AdPostBikesValidation
import com.kaushlesh.view.activity.AdPost.UploadActivity1
import com.kaushlesh.view.activity.AdPost.UploadPhotoActivity
import gun0912.tedimagepicker.builder.TedImagePicker
import kotlinx.android.synthetic.main.activity_add_vehicle_detail.*
import kotlinx.android.synthetic.main.activity_add_vehicle_detail.et_ad_title
import kotlinx.android.synthetic.main.activity_add_vehicle_detail.et_other_info
import kotlinx.android.synthetic.main.activity_labour_detail.*
import org.json.JSONObject
import java.io.*
import java.util.ArrayList

class AddVehicleDetailActivity : AppCompatActivity(), View.OnClickListener ,ParseControllerListener{
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal lateinit var rltype: RelativeLayout
    internal lateinit var rlkm: RelativeLayout
    internal lateinit var rlyear: RelativeLayout
    internal lateinit var sptype: Spinner
    internal lateinit var tvtype: TextView
    internal lateinit var ibtype: ImageButton
    internal lateinit var btnnext: Button
    internal lateinit var etadtitle: EditText
    internal lateinit var etotherinfo: EditText
    internal lateinit var tv11: TextView
    internal lateinit var tv1: TextView
    internal lateinit var tv2: TextView
    internal lateinit var type_spinner_type: ArrayAdapter<String>
    internal lateinit var tvpetrol: TextView
    internal lateinit var tvelectric: TextView
    internal lateinit var tvdiesel: TextView
    internal var petrol: Boolean? = false
    internal var electric: Boolean? = false
    internal var diesel: Boolean? = false
    internal var typelist: MutableList<String> = ArrayList()
    internal var typeidlist: MutableList<String> = ArrayList()
    lateinit var storeUserData: StoreUserData
    lateinit var adPostBikesBean: AdPostBikesBean
    lateinit var getListingController: GetListingController
    internal var brandlist: ArrayList<MobileBrandBeans.MobileBrandBeansList> = ArrayList()
    lateinit var resizebitmapmain: Bitmap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bike_scooter_detail)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.txt_include_details)

        storeUserData=StoreUserData(this)
        adPostBikesBean= AdPostBikesBean()
        getListingController = GetListingController(this, this)


        initBindViews()

        getListingController.getBrandList()

        Utils.setontouch(et_other_info)
        Utils.setontouch1(et_ad_title)

        ll_disclaimer.visibility = View.VISIBLE
    }

    private fun initBindViews() {


        tvpetrol = findViewById(R.id.tvpetrol)
        tvelectric = findViewById(R.id.tvelectric)
        tvdiesel = findViewById(R.id.tvdiesel)

        rltype = findViewById(R.id.rl_brand_type)
        sptype = findViewById(R.id.spinner_brand_type)
        tvtype = findViewById(R.id.tv_brand_type)
        ibtype = findViewById(R.id.ib_brand_type)

        rlyear = findViewById(R.id.rl_year_type)
        rlkm = findViewById(R.id.rl_km_type)

        btnnext = findViewById(R.id.btn_next)
        etadtitle = findViewById(R.id.et_ad_title)
        etotherinfo = findViewById(R.id.et_other_info)
        tv1 = findViewById(R.id.tv_1)
        tv2 = findViewById(R.id.tv_2)
        tv11 = findViewById(R.id.tv_11)

        //tvpetrol.setBackgroundResource(R.drawable.bg_edittext_black)

        tvpetrol.setOnClickListener(this)
        tvelectric.setOnClickListener(this)
        tvdiesel.setOnClickListener(this)

        btnback.setOnClickListener(this)
        btnnext.setOnClickListener(this)
        rltype.setOnClickListener(this)
        ibtype.setOnClickListener(this)


        type_spinner_type = ArrayAdapter(
            applicationContext,
            android.R.layout.simple_spinner_dropdown_item,
            typelist
        )

        sptype.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>,
                view: View,
                position: Int,
                l: Long
            ) {
                printLog(TAG, "onItemSelected: ===" + adapterView.selectedItem)
                printLog(TAG, "onItemSelected: ===" + adapterView.selectedItem)
                printLog(TAG, "onItemSelected: ===" + typeidlist.get(position))
                adPostBikesBean.brandId = typeidlist.get(position)
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        }


        etadtitle.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {


                tv1.text = s.length.toString() + "/77"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })

        etotherinfo.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {


                tv2.text = s.length.toString() + "/5000"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })

        adPostBikesBean.fuel = ""
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_back -> onBackPressed()

            R.id.btn_next -> {
                //val intent = Intent(applicationContext, UploadPhotoActivity::class.java)
                //startActivity(intent)
                validations()
            }

            R.id.tvpetrol -> {
                if(petrol!!)
                {
                    setValue(false, false, false, "fuel")
                    setFuel()
                }
                else {
                    setValue(true, false, false, "fuel")
                    setBottomSelection(
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.color.black,
                        R.color.gray,
                        R.color.gray,
                        "fuel"
                    )

                    adPostBikesBean.fuel ="1"
                }
            }

            R.id.tvelectric -> {
                if(electric!!)
                {
                    setValue(false, false, false, "fuel")
                    setFuel()
                }
                else {
                    setValue(false, true, false, "fuel")
                    setBottomSelection(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.color.gray,
                        R.color.black,
                        R.color.gray,
                        "fuel")

                    adPostBikesBean.fuel ="2"
                }
            }

            R.id.tvdiesel -> {
                setValue(false, false,true, "fuel")
                setBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    "fuel"
                )
            }

            R.id.rl_brand_type -> {

                tvtype.visibility = View.GONE
                sptype.visibility = View.VISIBLE
                sptype.performClick()
            }

            R.id.ib_brand_type -> {

                tvtype.visibility = View.GONE
                sptype.visibility = View.VISIBLE
                rltype.performClick()
            }
        }
    }

    private fun setFuel() {

        setValue(false, false,false, "fuel")
        setBottomSelection(
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.color.gray,
            R.color.gray,
            R.color.gray,
            "fuel"
        )
        adPostBikesBean.fuel = ""
    }


    private fun setValue(b1: Boolean, b2: Boolean,b3:Boolean, type: String) {

        if (type.equals("fuel", ignoreCase = true)) {
            petrol = b1
            electric = b2
            diesel = b3
        }
    }

    private fun setBottomSelection(b1: Int, b2: Int, b3: Int, b4: Int,b5: Int, b6: Int ,type: String) {
        if (type.equals("fuel", ignoreCase = true)) {

            tvpetrol.setBackgroundResource(b1)
            tvelectric.setBackgroundResource(b2)
            tvdiesel.setBackgroundResource(b3)

            tvpetrol.setTextColor(resources.getColor(b4))
            tvelectric.setTextColor(resources.getColor(b5))
            tvdiesel.setTextColor(resources.getColor(b6))
        }
    }

    companion object {

        private val TAG = "AddVehicleDetailActivity"
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {

        if (method.equals("GetBrands")) {

            brandlist.clear()
            typelist.clear()

            val dataModel = MobileBrandBeans()
            dataModel.MobileBrandBeans(da)

            brandlist = dataModel.mobileBrandList
            Utils.showLog(TAG,"==brand list size==" + brandlist.size)

            for(i in brandlist.indices)
            {
                val name = brandlist.get(i).brandName
                val id = brandlist.get(i).brandId
                typelist.add(name.toString())
                typeidlist.add(id.toString())
            }

            type_spinner_type = ArrayAdapter(
                applicationContext,
                android.R.layout.simple_spinner_dropdown_item,
                typelist
            )
            // Creating adapter for spinner
            val dataAdapter1 =
                ArrayAdapter(applicationContext, android.R.layout.simple_spinner_item, typelist)

            // Drop down layout style - list view with radio button
            dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

            // attaching data adapter to spinner
            sptype.adapter = dataAdapter1

        }
    }

    override fun onFail(msg: String, method: String) {
        AppConstants.printToast(applicationContext,msg)

    }

    private fun validations(){

        val issubmit = AdPostBikesValidation.checkForMotorScooter(this,adPostBikesBean,et_year,et_km,etadtitle,etotherinfo)
        Utils.showLog(TAG,"status" +  issubmit)
        if(issubmit) {
            adPostBikesBean.category_id = storeUserData.getString(Constants.CATEGORY_ID)
            adPostBikesBean.sub_category_id = storeUserData.getString(Constants.SUBCATEGORYID)
            adPostBikesBean.year = et_year.text.toString()
            adPostBikesBean.kmDriven = et_km.text.toString()
            adPostBikesBean.title = etadtitle.text.toString()
            adPostBikesBean.other_information = etotherinfo.text.toString()

            val gson = Gson()
            val json = gson.toJson(adPostBikesBean)
            storeUserData.setString(Constants.ADDBIKES,json)

            Log.e("save", "adOfficeForSell save data: "+storeUserData.getString(Constants.ADDBIKES))

            //val intent = Intent(applicationContext, UploadPhotoActivity::class.java)
            //startActivity(intent)
            setNormalMultiButton()
        }
    }

    fun setNormalMultiButton() {
        val selectedUriList: List<Uri>? = null

        TedImagePicker.with(this)
                .max(12, "Maximum 12 photos allowed")
                .errorListener { message ->
                    Log.d("ted", "message: $message")
                }
                .selectedUri(selectedUriList)
                .startMultiImage { list: List<Uri> ->
                    Log.e("list===", "listsize: ${list.size}")
                    showMultiImage1(list)
                }

    }

    private fun showMultiImage1(uriList: List<Uri>) {

        Log.d("ted=", "uriList: $uriList")
        Log.e("uriList.get(0)====", "uriList.get(0): $uriList.get(0)")

        Thread(Runnable {
            AppConstants.imageList.clear()
            AppConstants.imageListuri.clear()
            uriList.forEach {
                AppConstants.imageListuri.add(it)
                val imagepath = getPathFromGooglePhotosUri(it)
                resizebitmapmain = decodeImageFromFiles(imagepath, 280, 280)
                Log.e("resizebitmapmain====", "01--: $resizebitmapmain")
                val ei = ExifInterface(imagepath.toString())
                val orientation: Int = ei.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED
                )
                var rotatedBitmap: Bitmap? = null

                when (orientation) {
                    ExifInterface.ORIENTATION_ROTATE_90 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 90F)
                    ExifInterface.ORIENTATION_ROTATE_180 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 180F)
                    ExifInterface.ORIENTATION_ROTATE_270 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 270F)
                    ExifInterface.ORIENTATION_NORMAL -> rotatedBitmap = resizebitmapmain
                    else -> rotatedBitmap = resizebitmapmain
                }

                AppConstants.imageList.add(rotatedBitmap!!)
            }

            Log.e("imageListsize====", "viewSize: ${AppConstants.imageList.size}")

            if(AppConstants.imageList!=null)
            {
                Log.e("AppConstantsimageList==", "viewSize: ${AppConstants.imageList.size}")
                checkAndSaveBikes(this)
            }

        }).start()

        AdPostImages.changeScreentoNext(this)

    }

    fun rotateImage(source: Bitmap, angle: Float): Bitmap? {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(
                source, 0, 0, source.width, source.height,
                matrix, true
        )
    }

    fun decodeImageFromFiles(path: String?, width: Int, height: Int): Bitmap {
        val scaleOptions = BitmapFactory.Options()
        scaleOptions.inJustDecodeBounds = true
        BitmapFactory.decodeFile(path, scaleOptions)
        var scale = 1
        while (scaleOptions.outWidth / scale / 2 >= width
                && scaleOptions.outHeight / scale / 2 >= height
        ) {
            scale *= 2
        }
        // decode with the sample size
        val outOptions = BitmapFactory.Options()
        outOptions.inSampleSize = scale
        val bitmap: Bitmap = BitmapFactory.decodeFile(path, outOptions)
        Utils.showLog(
                UploadActivity1.TAG,
                "===BitmapFactory===width" + bitmap.width + "===height" + bitmap.height + "==="
        )

        return bitmap
    }

    fun getPathFromGooglePhotosUri(uriPhoto: Uri?): String? {
        if (uriPhoto == null) return null
        var input: FileInputStream? = null
        var output: FileOutputStream? = null
        try {
            val pfd: ParcelFileDescriptor? = contentResolver.openFileDescriptor(uriPhoto, "r")
            val fd: FileDescriptor = pfd!!.getFileDescriptor()
            input = FileInputStream(fd)
            val tempFilename: String = getTempFilename(this)
            output = FileOutputStream(tempFilename)
            var read: Int = 0
            val bytes = ByteArray(4096)
            while (input.read(bytes).also({ read = it }) != -1) {
                output.write(bytes, 0, read)
            }
            return tempFilename
        } catch (ignored: IOException) {
            // Nothing we can do
        } finally {
            closeSilently(input)
            closeSilently(output)
        }
        return null
    }

    @Throws(IOException::class)
    private fun getTempFilename(context: Context): String {
        val outputDir = context.cacheDir
        val outputFile = File.createTempFile("image", "tmp", outputDir)
        return outputFile.absolutePath
    }

    fun closeSilently(c: Closeable?) {
        if (c == null) return
        try {
            c.close()
        } catch (t: Throwable) {
            // Do nothing
        }
    }


    private fun checkAndSaveBikes(context: Context) {
        //AdPostImages.bimglist.clear()

        val storeUserData = StoreUserData(context)
        val gson = Gson()

        if (AppConstants.imageList.size >= 1) {

            Log.e("test", "imagelist: " + AppConstants.imageList.size+"===")

            val json: String = storeUserData.getString(Constants.ADDBIKES)

            Log.e("String", "String data: " + json+"====")

            val adPostBikesBean = gson.fromJson(json, AdPostBikesBean::class.java)

            Log.e("test", "listsave data: " + adPostBikesBean.toString())
            adPostBikesBean.post_images?.clear()
            adPostBikesBean.post_images = AppConstants.imageList
            Log.e("test", "post_images: " +  adPostBikesBean.post_images!!.size+"===")


            Log.e("test", "post_images data: " + adPostBikesBean.post_images)

            val jsonset = gson.toJson(adPostBikesBean)

            storeUserData.setString(Constants.ADDBIKES, jsonset)

        }
        else{
            Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
        }
    }
}

