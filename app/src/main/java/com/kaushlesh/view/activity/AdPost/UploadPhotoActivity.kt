package com.kaushlesh.view.activity.AdPost

import android.Manifest
import android.app.Activity
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.ImageDecoder
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.kaushlesh.R
import com.kaushlesh.adapter.UploadImagesAdapter
import com.kaushlesh.bean.AdPost.ApVillsellandRentBean
import com.kaushlesh.bean.ImagesBean
import com.kaushlesh.utils.*
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class UploadPhotoActivity : AppCompatActivity(), View.OnClickListener,
    UploadImagesAdapter.OnStartDragListener, UploadImagesAdapter.ItemClickListener {
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal lateinit var ivcamera: ImageView
    internal lateinit var ivgallery: ImageView
    internal lateinit var btnnext: Button
    internal lateinit var rvimages: RecyclerView
    internal var imglist: MutableList<ImagesBean> = ArrayList()
    private var num_images = 0
    internal lateinit var adapter: UploadImagesAdapter
    lateinit var touchHelper: ItemTouchHelper
    internal var category: String? = null
    var countImage: Int = 12
    lateinit var storeUserData: StoreUserData
    lateinit var adVillsellandRentBeanList: ApVillsellandRentBean
    internal var categoryid: String? = null
    private var dateFormatter: SimpleDateFormat? = null
    var photoFile: File? = null
    var mCurrentPhotoPath: String? = null
    internal var bimglist: ArrayList<Bitmap> = ArrayList()
    val orientation: Int = 0
    lateinit var resizebitmapmain :Bitmap
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_photo)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        storeUserData = StoreUserData(this)
        adVillsellandRentBeanList = ApVillsellandRentBean()

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.txt_upload_photo)

        val storeusedata = StoreUserData(applicationContext)
        category = storeusedata.getString(Constants.CATEGORY_NAME)
        categoryid = storeusedata.getString(Constants.CATEGORY_ID)
        Utils.showLog(TAG, "==category name==" + category + "id==" + categoryid)

        if (storeusedata.getString(Constants.CATEGORY_NAME) == getString(R.string.txt_properties) || storeusedata.getString(Constants.CATEGORY_NAME) == getString(R.string.txt_vehicle)) {
            countImage = 12
        } else {
            countImage = 12
        }

        initBindViews()
//
//        dateFormatter = SimpleDateFormat(
//                DATE_FORMAT, Locale.US)

    }

    private fun initBindViews() {
        ivcamera = findViewById(R.id.iv_camera)
        ivgallery = findViewById(R.id.iv_gallery)
        btnnext = findViewById(R.id.btn_next)
        rvimages = findViewById(R.id.rv_imgs)

        btnback.setOnClickListener(this)
        btnnext.setOnClickListener(this)
        ivcamera.setOnClickListener(this)
        ivgallery.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_back -> onBackPressed()

            R.id.btn_next -> {

                AdPostImages.checkCatAnSub(this, imglist, storeUserData.getString(Constants.CATEGORY_ID), storeUserData.getString(Constants.SUBCATEGORYID))
            }

            R.id.iv_camera -> requestMultiplePermissions("camera")

            R.id.iv_gallery -> requestMultiplePermissions("gallery")
        }
    }

    private fun requestMultiplePermissions(type: String) {
        Dexter.withActivity(this).withPermissions(
            Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    // check if all permissions are granted
                    if (report.areAllPermissionsGranted()) {
                        Log.i(TAG, "All permissions are granted")

                        if (type.equals("camera", ignoreCase = true)) {
                            takePhotoFromCamera()
                        }
                        if (type.equals("gallery", ignoreCase = true)) {
                            choosePhotoFromGallary()
                        }
                        // showPictureDialog();
                        /*if (imageList.size() < 6) {

                            showPictureDialog();
                        } else {

                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.image_max_limit), Toast.LENGTH_SHORT).show();
                        }*/
                    }

                    // check for permanent denial of any permission
                    /* if (report.isAnyPermissionPermanentlyDenied) {
                        // show alert dialog navigating to Settings
                        //openSettingsDialog();
                    }*/
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }

            }).withErrorListener { Log.i(TAG, "Some Error!") }
            .onSameThread()
            .check()
    }

    // @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    fun choosePhotoFromGallary() {

        if (imglist.size < countImage) {
            //   val intent = Intent()
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
            ////   intent.putExtra(Intent.Extra, 3);
            startActivityForResult(Intent.createChooser(intent, getString(R.string.txt_select_picture)), 100);
        } else {
            Utils.showToast(this, getString(R.string.txt_image_limit))
        }

    }

    private fun takePhotoFromCamera() {

        if (imglist.size < countImage) {
            /*  val captureImageIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
              if (captureImageIntent.resolveActivity(packageManager) != null) {
                  startActivityForResult(captureImageIntent, 200)
              }*/

            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (takePictureIntent.resolveActivity(packageManager) != null) {
                // Create the File where the photo should go
                try {
                    photoFile = createImageFile()
                    // Continue only if the File was successfully created
                    if (photoFile != null) {
                        val photoURI = FileProvider.getUriForFile(this, applicationContext.packageName.toString() + ".provider", photoFile!!)
                        takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                        takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                        startActivityForResult(takePictureIntent, 200)
                    }
                } catch (ex: Exception) {
                    // Error occurred while creating the File
                    displayMessage(baseContext, ex.message.toString())
                }

            }
        } else {
            Utils.showToast(this, getString(R.string.txt_image_limit))
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File(storageDir, imageFileName + ".png")

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.absolutePath
        return image
    }

    private fun displayMessage(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 100) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                if (data.clipData != null && data.clipData!!.itemCount > 0) {
                    val selectedImageCount = data.clipData!!.itemCount

                    val totalImages = selectedImageCount + imglist.size

                    if (totalImages > countImage) {
                        Utils.showToast(this@UploadPhotoActivity, getString(R.string.txt_image_limit))
                        return
                    } else {

                        val tempContext = this@UploadPhotoActivity
                        BackgroundTask.with(UploadPhotoActivity::class.java.simpleName, object :
                            BackgroundTask.Listener<ArrayList<ImagesBean>> {
                            override fun canStart(): Boolean = true

                            override fun backgroundTask(): ArrayList<ImagesBean> {
                                val imageList = arrayListOf<ImagesBean>()

                                for (i in 0 until data.clipData!!.itemCount) {

                                    val imageUri = data.clipData!!.getItemAt(i).uri
                                    val imagePath = PathUtil.getRealPath(tempContext, imageUri)

                                    if (imagePath != null) {
                                        Utils.showLog(TAG, "===gallery_imgpath===" + imagePath)
                                        //val resizedBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri)


                                        /// Utils.showLog(TAG, "===gallery_orientation===" + orientation+"===")

                                        val tempFile = File(imagePath)
                                        Utils.showLog(TAG, "===gallery_imgfile===" + tempFile.length() / 1024)
                                        ///   val data1 =  File(getPathFromGooglePhotosUri(imageUri));

                                        ///  val data1 = saveBitmapToFile(tempFile)
                                        val data1 = getCompressed(baseContext, imagePath)
                                        Utils.showLog(TAG, "===gallery_getPath===" + data1!!.getPath() + "===")

                                        val filePath: String = data1!!.getPath()
                                        //val resizedBitmap = BitmapFactory.decodeFile(filePath)
                                        Utils.showLog(TAG, "===resizebitmapmain===" +resizebitmapmain+ "===")
                                        val resizedBitmap =resizebitmapmain

                                        var rotatedBitmap: Bitmap? = null
                                        val ei = ExifInterface(imagePath)
                                        val orientation: Int = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                                            ExifInterface.ORIENTATION_UNDEFINED)
                                        when (orientation) {
                                            ExifInterface.ORIENTATION_ROTATE_90 -> rotatedBitmap = rotateImage(resizedBitmap, 90F)
                                            ExifInterface.ORIENTATION_ROTATE_180 -> rotatedBitmap = rotateImage(resizedBitmap, 180F)
                                            ExifInterface.ORIENTATION_ROTATE_270 -> rotatedBitmap = rotateImage(resizedBitmap, 270F)
                                            ExifInterface.ORIENTATION_NORMAL -> rotatedBitmap = resizedBitmap
                                            else -> rotatedBitmap = resizedBitmap
                                        }
                                        imageList.add(ImagesBean(rotatedBitmap))

                                        /* val bmOptions = BitmapFactory.Options()
                                        val resizedBitmap = BitmapFactory.decodeFile(imagePath, bmOptions)*/
                                        /* val resizedBitmap = getResizedBitmap(
                                                BitmapFactory.decodeFile(imagePath),
                                                200,
                                                200
                                        )*/


                                    }
                                }
                                return imageList
                            }

                            override fun taskCompleted(result: ArrayList<ImagesBean>) {
                                try {
                                    imglist.addAll(result)
                                    num_images += result.size
                                    bindRecyclerView()
                                } catch (e: Exception) {
                                    Utils.showToast(this@UploadPhotoActivity, e.message)
                                }
                            }
                        })

                    }
                } else if (data.data != null) {
                    val imageUri = data.data!!
                    val imagePath = PathUtil.getRealPath(this@UploadPhotoActivity, imageUri)
                    if (imagePath != null) {
                        if (imglist.size > countImage) {
                            Utils.showToast(
                                this,
                                getString(R.string.txt_image_limit)
                            )
                        } else {
                            val ei = ExifInterface(imagePath)
                            val orientation: Int = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                                ExifInterface.ORIENTATION_UNDEFINED)

                            BackgroundTask.with(
                                "UploadPhotoActivity",
                                object : BackgroundTask.Listener<ImagesBean> {
                                    override fun canStart(): Boolean = true
                                    override fun backgroundTask(): ImagesBean {
                                        //val resizedBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri)

                                        ///     Utils.showLog(TAG, "===camera_orientation===" + orientation+"==")
                                        Utils.showLog(TAG, "===camera_img path===" + imagePath)

                                        val tempFile = File(imagePath)
                                        Utils.showLog(TAG, "===camera_imgfile===" + tempFile.length() / 1024)

                                        // val data1 = saveBitmapToFile(tempFile)

                                        val data1 = getCompressed(baseContext, imagePath)

                                        Utils.showLog(TAG, "===getPath===" + data1!!.getPath() + "===")
                                        val filePath: String = data1!!.getPath()

                                        ///   val resizedBitmap = BitmapFactory.decodeFile(filePath)
                                        val resizedBitmap = resizebitmapmain
                                        Utils.showLog(TAG, "===resizebitmapmain===" +resizebitmapmain+ "===")
                                        // val resizedBitmap = decodeFile(data1)



                                        var rotatedBitmap: Bitmap? = null

                                        when (orientation) {
                                            ExifInterface.ORIENTATION_ROTATE_90 -> rotatedBitmap = rotateImage(resizedBitmap, 90F)
                                            ExifInterface.ORIENTATION_ROTATE_180 -> rotatedBitmap = rotateImage(resizedBitmap, 180F)
                                            ExifInterface.ORIENTATION_ROTATE_270 -> rotatedBitmap = rotateImage(resizedBitmap, 270F)
                                            ExifInterface.ORIENTATION_NORMAL -> rotatedBitmap = resizedBitmap
                                            else -> rotatedBitmap = resizedBitmap
                                        }

                                        return ImagesBean(rotatedBitmap)
                                        /*   val bmOptions = BitmapFactory.Options()
                                        val resizedBitmap = BitmapFactory.decodeFile(imagePath, bmOptions)*/
                                        /*  val resizedBitmap = getResizedBitmap(
                                                BitmapFactory.decodeFile(imagePath),
                                                200,
                                                200
                                        )*/

                                    }

                                    override fun taskCompleted(result: ImagesBean) {
                                        imglist.add(result)
                                        num_images += 1
                                        bindRecyclerView()
                                    }
                                })
                        }
                    } else {
                        Utils.showToast(this@UploadPhotoActivity, getString(R.string.txt_something_wrong))
                    }
                } else {
                    // Utils.showToast(this@UploadPhotoActivity, getString(R.string.txt_photo_op_cancelled))
                }
            } else {
                //Utils.showToast(this@UploadPhotoActivity, getString(R.string.txt_photo_cancelled))
            }
        } else if (requestCode == 200) {
            if (resultCode == Activity.RESULT_OK) {
                if (imglist.size > countImage) {
                    Utils.showToast(this, getString(R.string.txt_image_limit))
                } else {
                    if (photoFile == null) {
                        Utils.showToast(this, getString(R.string.txt_wrong_with_file))
                        return
                    }

                    Utils.showLog(TAG, "===img path===" + photoFile!!.path)

                    val tempFile = File(photoFile!!.path)
                    val ei = ExifInterface(photoFile!!.path)
                    val orientation: Int = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED)

                    BackgroundTask.with(
                        "UploadPhotoActivity",
                        object : BackgroundTask.Listener<ImagesBean?> {
                            override fun canStart(): Boolean = true
                            override fun backgroundTask(): ImagesBean? {
                                try {
                                    /*   val bmOptions = BitmapFactory.Options()
                                    val resizedBitmap = BitmapFactory.decodeFile(tempFile.path, bmOptions)*/
                                    /// Utils.showLog(TAG, "===img_orientation===" + orientation+"==")

                                    // val resizedBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.fromFile(tempFile))
                                    Utils.showLog(TAG, "===img file===" + tempFile.length() / 1024)
                                    val data1 = getCompressed(baseContext, photoFile!!.path)
                                    //val data1 = saveBitmapToFile(tempFile)
                                    val filePath: String = data1!!.getPath()
                                    val resizedBitmap = resizebitmapmain
                                    Utils.showLog(TAG, "===resizebitmapmain===" +resizebitmapmain+ "===")
                                    ///    val resizedBitmap = BitmapFactory.decodeFile(filePath)

                                    /*    val resizedBitmap = getResizedBitmap(
                                            BitmapFactory.decodeFile(tempFile.path),
                                            200,
                                            200
                                    )
*/
                                    var rotatedBitmap: Bitmap? = null
                                    when (orientation) {
                                        ExifInterface.ORIENTATION_ROTATE_90 -> rotatedBitmap = rotateImage(resizedBitmap, 90F)
                                        ExifInterface.ORIENTATION_ROTATE_180 -> rotatedBitmap = rotateImage(resizedBitmap, 180F)
                                        ExifInterface.ORIENTATION_ROTATE_270 -> rotatedBitmap = rotateImage(resizedBitmap, 270F)
                                        ExifInterface.ORIENTATION_NORMAL -> rotatedBitmap = resizedBitmap
                                        else -> rotatedBitmap = resizedBitmap
                                    }
                                    return ImagesBean(rotatedBitmap)
                                } catch (e: java.lang.Exception) {
                                    return null
                                }
                            }

                            override fun taskCompleted(result: ImagesBean?) {
                                result.let {
                                    imglist.add(result!!)
                                    num_images += 1
                                    bindRecyclerView()
                                }
                            }
                        })
                }
            } else {
                //Utils.showToast(this@UploadPhotoActivity, getString(R.string.txt_camrea_op_cancel));
            }
        }
    }

//    fun saveBitmapToFile(file: File): File? {
//        return try {
//            Utils.showLog(TAG, "===getFile1===" +  file+"===")
//            // BitmapFactory options to downsize the image
//            val o = BitmapFactory.Options()
//            o.inJustDecodeBounds = true
//            o.inSampleSize = 6
//            // factor of downsizing the image
//            var inputStream = FileInputStream(file)
//            //Bitmap selectedBitmap = null;
//            BitmapFactory.decodeStream(inputStream, null, o)
//            inputStream.close()
//
//            // The new size we want to scale to
//            val REQUIRED_SIZE = 75
//
//            // Find the correct scale value. It should be the power of 2.
//            var scale = 1
//            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
//                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
//                scale *= 2
//            }
//            val o2 = BitmapFactory.Options()
//            o2.inSampleSize = scale
//            inputStream = FileInputStream(file)
//            val selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2)
//            inputStream.close()
//
//            // here i override the original image file
//            file.createNewFile()
//            Utils.showLog(TAG, "===getFile2===" +  file+"===")
//            val outputStream = FileOutputStream(file)
//            selectedBitmap!!.compress(Bitmap.CompressFormat.JPEG, 70, outputStream)
//            Utils.showLog(TAG, "===getFile3===" +  file+"===")
//            file
//        } catch (e: java.lang.Exception) {
//            null
//        }
//    }

    fun rotateImage(source: Bitmap, angle: Float): Bitmap? {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(source, 0, 0, source.width, source.height,
            matrix, true)
    }

    /*override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && requestCode == 100 && data!!.getData()!=null) {

                // if multiple images are selected
                if (data.getClipData() != null) {

                    val count = data.clipData?.itemCount
                    if (count!! > countImage) {
                        Utils.showToast(this, "you have exceed the limit of maximum image count")
                        return
                    } else {

                        for (i in 0..count.minus(1)) {
                            if (imglist.size <= countImage) {
                                val imageUri: Uri = data.clipData!!.getItemAt(i).uri

                                val  path = getPathFromURI(imageUri)

                                if (path != null) {
                                    *//* val f: File = File(path)
                                     selectedImageUri = Uri.fromFile(f)*//*

                                    //val imagepath = getPath(this, imageUri)
                                    val myBitmap: Bitmap = getResizedBitmap(BitmapFactory.decodeFile(path), 200, 200)
                                   // val uri = getImageUriFromBitmap(applicationContext, myBitmap)

                                    //imglist.add(ImagesBean(uri))
                                    imglist.add(ImagesBean(myBitmap))
                                    num_images += 1
                                    bindRecyclerView()
                                    adapter.notifyDataSetChanged()
                                }
                                //solution 1 to craete scale bitmap and upload
                               *//* val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, imageUri)

                                val scaled = Bitmap.createScaledBitmap(bitmap, 512, ((bitmap.getHeight() * (512.0 / bitmap.getWidth())).toInt()), true)

                                //val bmp = Bitmap.createScaledBitmap(bitmap, 400, 400, true)
                                val uri = getImageUriFromBitmap(applicationContext, scaled)*//*

                             *//*   //solution 2
                                val imagepath = getPath(this, imageUri)
                                val myBitmap: Bitmap = getResizedBitmap(BitmapFactory.decodeFile(imagepath), 200, 200)
                                //val uri = getImageUriFromBitmap(applicationContext, myBitmap)

                                //imglist.add(ImagesBean(uri))
                                imglist.add(ImagesBean(myBitmap))
                                num_images += 1
                                bindRecyclerView()
                                adapter.notifyDataSetChanged()*//*

                                *//* imglist.add(ImagesBean(imageUri))
                             num_images += 1
                             bindRecyclerView()*//*
                            } else {
                                Utils.showToast(this, "you have exceed the limit of maximum image count")
                            }
                        }
                    }

                } else if (data.getData() != null) {

                    if (imglist.size > countImage) {
                        Utils.showToast(this, "you have exceed the limit of maximum image count")
                    } else {
                        // if single image is selected
                        if (imglist.size <= countImage) {

                            val imageUri: Uri = data.data!!

                            val  path = getPathFromURI(imageUri)

                            if (path != null) {
                               *//* val f: File = File(path)
                                selectedImageUri = Uri.fromFile(f)*//*

                                //val imagepath = getPath(this, imageUri)
                                val myBitmap: Bitmap = getResizedBitmap(BitmapFactory.decodeFile(path), 200, 200)
                                //val uri = getImageUriFromBitmap(applicationContext, myBitmap)

                                //imglist.add(ImagesBean(uri))
                                imglist.add(ImagesBean(myBitmap))
                                num_images += 1
                                bindRecyclerView()
                                adapter.notifyDataSetChanged()
                            }

                          //  val bitmap: Bitmap = loadFromUri(imageUri)!!

                           *//* val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, imageUri)
                            val scaled = Bitmap.createScaledBitmap(bitmap, 512, ((bitmap.getHeight() * (512.0 / bitmap.getWidth())).toInt()), true)
                            val uri = getImageUriFromBitmap(applicationContext, scaled)*//*


                            *//*   imglist.add(ImagesBean(imageUri))
                        num_images += 1
                        bindRecyclerView()
                        adapter.notifyDataSetChanged()*//*
                        } else {
                            Utils.showToast(this, "you have exceed the limit of maximum image count")
                        }
                    }
                }
        }

        if (requestCode == 200 && resultCode == Activity.RESULT_OK && data != null) {

                if (imglist.size > countImage) {
                    Utils.showToast(this, "you have exceed the limit of maximum image count")
                } else {

                    val bitmap = BitmapFactory.decodeStream(photoFile!!.inputStream())

                 *//*   val bitmap: Bitmap
                    if (data.getData() == null) {
                        bitmap = data.extras?.get("data") as Bitmap
                    } else {
                        bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver,data.getData())
                    }*//*
                    //val bitmap = data.extras?.get("data")
                    Log.i(TAG, "eeeeedata==>" + bitmap)


                    *//*  getImageUriFromBitmap(applicationContext, bitmap as Bitmap)

                Log.i(TAG, "eeeeedata uri ==>" + getImageUriFromBitmap(applicationContext, bitmap))

                imglist.add(ImagesBean(getImageUriFromBitmap(applicationContext, bitmap))) // store uri
                num_images += 1
                bindRecyclerView()
                adapter.notifyDataSetChanged()*//*

                    //solution 1
                   *//* val scaled = Bitmap.createScaledBitmap(bitmap as Bitmap, 512, ((bitmap.getHeight() * (512.0 / bitmap.getWidth())).toInt()), true)
                    val uri = getImageUriFromBitmap(applicationContext, scaled)*//*

                    //solution2
                    //val imagepath = getPath(this, getImageUriFromBitmap(applicationContext, bitmap as Bitmap))
                    val myBitmap: Bitmap = getResizedBitmap(BitmapFactory.decodeStream(photoFile!!.inputStream()), 200, 200)
                    //val uri = getImageUriFromBitmap(applicationContext, myBitmap)

                    imglist.add(ImagesBean(myBitmap))
                    //imglist.add(ImagesBean(uri)) // store uri
                    num_images += 1
                    bindRecyclerView()
                    adapter.notifyDataSetChanged()

                    Log.i("TAG", "========images list=========" + imglist + imglist.size)
                }
        }
    }*/

    fun getPathFromURI(contentUri: Uri?): String? {
        Log.e(
            UploadPhotoActivity::class.java.simpleName,
            "getPathFromURI Uri : ${contentUri!!.path}"
        )
        var res: String? = null
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = contentResolver.query(contentUri!!, proj, null, null, null)
        if (cursor!!.moveToFirst()) {
            try {
                val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                res = cursor.getString(column_index)
                if (res.isNullOrEmpty()) {
                    Log.e(UploadPhotoActivity::class.java.simpleName, "Result : $res")
                    return null
                }
            } catch (e: Exception) {
                e.printStackTrace()
                return null
            }
        }
        cursor.close()
        return res
    }

    fun loadFromUri(photoUri: Uri): Bitmap? {
        var image: Bitmap? = null
        try {
            // check version of Android on device
            image = if (Build.VERSION.SDK_INT > 27) {
                // on newer versions of Android, use the new decodeBitmap method
                val source: ImageDecoder.Source =
                    ImageDecoder.createSource(this.contentResolver, photoUri)
                ImageDecoder.decodeBitmap(source)
            } else {
                // support older versions of Android by using getBitmap
                MediaStore.Images.Media.getBitmap(this.contentResolver, photoUri)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return image
    }

    fun getPath(context: Context, uri: Uri): String? {
        val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).toTypedArray()
                val type = split[0]
                if ("primary".equals(type, ignoreCase = true)) {
                    return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                }

                // TODO handle non-primary volumes
            } else if (isDownloadsDocument(uri)) {
                val id = DocumentsContract.getDocumentId(uri)
                val contentUri = ContentUris.withAppendedId(
                    Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id)
                )
                return getDataColumn(context, contentUri, null, null)
            } else if (isMediaDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).toTypedArray()
                val type = split[0]
                var contentUri: Uri? = null
                if ("image" == type) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                } else if ("video" == type) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                } else if ("audio" == type) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }
                val selection = "_id=?"
                val selectionArgs = arrayOf(
                    split[1]
                )
                return getDataColumn(context, contentUri, selection, selectionArgs)
            }
        } else if ("content".equals(uri.scheme, ignoreCase = true)) {

            // Return the remote address
            return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(
                context,
                uri,
                null,
                null
            )
        } else if ("file".equals(uri.scheme, ignoreCase = true)) {
            return uri.path
        }
        return null
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    fun getDataColumn(
        context: Context, uri: Uri?, selection: String?,
        selectionArgs: Array<String>?
    ): String? {
        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(
            column
        )
        try {
            cursor = context.contentResolver.query(
                uri!!, projection, selection, selectionArgs,
                null
            )
            if (cursor != null && cursor.moveToFirst()) {
                val index: Int = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(index)
            }
        } finally {
            if (cursor != null) cursor.close()
        }
        return null
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri.authority
    }

    fun getResizedBitmap(bm: Bitmap, newHeight: Int, newWidth: Int): Bitmap {
        val width = bm.width
        val height = bm.height
        val scaleWidth = newWidth.toFloat() / width
        val scaleHeight = newHeight.toFloat() / height
        // create a matrix for the manipulation
        val matrix = Matrix()
        // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight)
        // recreate the new Bitmap
        return Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false)

        /*  val width = bm.width
          val height = bm.height

          val resizedBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888)

          val scaleX = newWidth / width as Float
          val scaleY = newHeight / height as Float
          val pivotX = 0f
          val pivotY = 0f

          val scaleMatrix = Matrix()
          scaleMatrix.setScale(scaleX, scaleY, pivotX, pivotY)

          val canvas = Canvas(resizedBitmap)
          canvas.setMatrix(scaleMatrix)
          canvas.drawBitmap(bm,width, height, Paint(Paint.FILTER_BITMAP_FLAG))

          val matrix = Matrix()
          // resize the bit map
          matrix.postScale(scaleX, scaleY)
          // recreate the new Bitmap
          return Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false)*/

    }

    private fun bindRecyclerView() {
        val manager = GridLayoutManager(applicationContext, 3)
        rvimages.layoutManager = manager
        adapter = UploadImagesAdapter(imglist, applicationContext, this)

        val callback: ItemTouchHelper.Callback = ItemMoveCallbackListener(adapter)
        touchHelper = ItemTouchHelper(callback)
        touchHelper.attachToRecyclerView(rvimages)
        adapter.setClicklistner(this)
        rvimages.adapter = adapter
        adapter.notifyDataSetChanged()
    }


    private fun getImageUriFromBitmap(context: Context?, bitmap: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        //val path = MediaStore.Images.Media.insertImage(context?.contentResolver, bitmap, "Title", null)
        val path: String = MediaStore.Images.Media.insertImage(
            context?.getContentResolver(),
            bitmap,
            "" + System.currentTimeMillis(),
            null
        )
        Log.e(TAG, "===path===" + path.toString())
        return Uri.parse(path)
    }

    companion object {

        val TAG = "UploadPhotoActivity"
    }

    override fun itemclick(bean: ImagesBean) {

    }

    override fun itemremoveclick(position: Int) {
        imglist.removeAt(position)
        bindRecyclerView()
    }

    override fun onStartDrag(viewHolder: RecyclerView.ViewHolder) {

        touchHelper.startDrag(viewHolder)
        //  bindRecyclerView()
    }

    fun saveBitmapToFile(file: File): File? {
        return try {
            Utils.showLog(TAG, "===getFile1===" + file + "===")
            // BitmapFactory options to downsize the image
            val o = BitmapFactory.Options()
            o.inJustDecodeBounds = true
            o.inSampleSize = 6
            // factor of downsizing the image
            var inputStream = FileInputStream(file)
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o)
            inputStream.close()

            // The new size we want to scale to
            val REQUIRED_SIZE = 75

            // Find the correct scale value. It should be the power of 2.
            var scale = 1
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2
            }
            val o2 = BitmapFactory.Options()
            o2.inSampleSize = scale
            inputStream = FileInputStream(file)
            val selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2)
            inputStream.close()

            // here i override the original image file
            file.createNewFile()
            Utils.showLog(TAG, "===getFile2===" + file + "===")
            val outputStream = FileOutputStream(file)
            selectedBitmap!!.compress(Bitmap.CompressFormat.JPEG, 70, outputStream)
            Utils.showLog(TAG, "===getFile3===" + file + "===")
            file
        } catch (e: java.lang.Exception) {
            null
        }
    }


    fun getCompressed(context: Context?, path: String?): File? {
        val SDF = SimpleDateFormat("yyyymmddhhmmss", Locale.getDefault())
        if (context == null) throw NullPointerException("Context must not be null.")
        //getting device external cache directory, might not be available on some devices,
        // so our code fall back to internal storage cache directory, which is always available but in smaller quantity
        var cacheDir = context.externalCacheDir
        if (cacheDir == null) //fall back
            cacheDir = context.cacheDir
        val rootDir = cacheDir!!.absolutePath.toString() + "/ImageCompressor"
        val root = File(rootDir)

        //Create ImageCompressor folder if it doesnt already exists.
        if (!root.exists()) root.mkdirs()

        //decode and resize the original bitmap from @param path.
         resizebitmapmain= decodeImageFromFiles(path,  /* your desired width*/100,  /*your desired height*/100)
        //resizebitmapmain=  BitmapFactory.decodeFile(path)
        Utils.showLog(TAG, "===resizebitmapmain111===" + resizebitmapmain + "===")
        //create placeholder for the compressed image file
        val compressed = File(root, SDF.format(Date()).toString() + ".jpg")

        //convert the decoded bitmap to stream
        val byteArrayOutputStream = ByteArrayOutputStream()

        /*compress bitmap into byteArrayOutputStream
            Bitmap.compress(Format, Quality, OutputStream)
            Where Quality ranges from 1 - 100.
         */
        resizebitmapmain.compress(Bitmap.CompressFormat.JPEG, 75, byteArrayOutputStream)

        /*
        Right now, we have our bitmap inside byteArrayOutputStream Object, all we need next is to write it to the compressed file we created earlier,
        java.io.FileOutputStream can help us do just That!
         */
        val fileOutputStream = FileOutputStream(compressed)
        fileOutputStream.write(byteArrayOutputStream.toByteArray())
        fileOutputStream.flush()
        fileOutputStream.close()

        Utils.showLog(TAG, "===compressed===" + compressed + "===")

        //File written, return to the caller. Done!
        return compressed
    }

    fun decodeImageFromFiles(path: String?, width: Int, height: Int): Bitmap {
        val scaleOptions = BitmapFactory.Options()
        scaleOptions.inJustDecodeBounds = true
        BitmapFactory.decodeFile(path, scaleOptions)
        var scale = 1
        while (scaleOptions.outWidth / scale / 2 >= width
            && scaleOptions.outHeight / scale / 2 >= height) {
            scale *= 2
        }
        // decode with the sample size
        val outOptions = BitmapFactory.Options()
        outOptions.inSampleSize = scale
        val bitmap: Bitmap = BitmapFactory.decodeFile(path, outOptions)
        Utils.showLog(TAG, "===BitmapFactory===width" + bitmap.width + "===height" + bitmap.height + "===")

        return bitmap
    }
//    fun decodeImageFromFiles(path: String?, width: Int, height: Int): Bitmap {
//        val scaleOptions = BitmapFactory.Options()
//        scaleOptions.inJustDecodeBounds = true
//        BitmapFactory.decodeFile(path, scaleOptions)
//        var scale = 1
//
//        var REQUIRED_SIZE = 85
//
//
//        Utils.showLog(TAG, "===scaleOptions.outWidth===" + scaleOptions.outWidth + "===")
//        Utils.showLog(TAG, "===scaleOptions.outHeight===" + scaleOptions.outHeight + "===")
//        Utils.showLog(TAG, "===width===" + width + "===")
//        Utils.showLog(TAG, "===height===" + height + "===")
//
//        while (scaleOptions.outWidth / scale / 2 >= REQUIRED_SIZE
//                && scaleOptions.outHeight / scale / 2 >= REQUIRED_SIZE) {
//            scale *= 2
//        }
//        Utils.showLog(TAG, "===scale===" + scale + "===")
//
//
//        // decode with the sample size
//        val outOptions = BitmapFactory.Options()
//        outOptions.inSampleSize = scale
//        val bitmap: Bitmap=BitmapFactory.decodeFile(path, outOptions)
//        Utils.showLog(TAG, "===BitmapFactory===width" +bitmap.width + "===height"+bitmap.height+"===")
//
//        return bitmap
//    }

    private fun getDropboxIMGHeightSize(uri: Uri): Int {
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeFile(File(uri.path).absolutePath, options)
        val imageHeight = options.outHeight
        Utils.showLog(TAG, "===imageHeight===" + imageHeight + "===")

        return imageHeight;

    }

    private fun getDropboxIMGWidthSize(uri: Uri): Int {
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeFile(File(uri.path).absolutePath, options)
        val imageWidth = options.outWidth
        Utils.showLog(TAG, "===imageWidth===" + imageWidth + "===")
        return imageWidth;

    }


    fun closeSilently(c: Closeable?) {
        if (c == null) return
        try {
            c.close()
        } catch (t: Throwable) {
            // Do nothing
        }
    }

    @Throws(IOException::class)
    private fun getTempFilename(context: Context): String {
        val outputDir = context.cacheDir
        val outputFile = File.createTempFile("image", "tmp", outputDir)
        return outputFile.absolutePath
    }
}

