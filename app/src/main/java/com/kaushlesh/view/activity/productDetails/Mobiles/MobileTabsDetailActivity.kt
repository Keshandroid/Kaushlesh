package com.kaushlesh.view.activity.productDetails.Mobiles

import android.Manifest
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.CompoundButton
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.GsonBuilder
import com.kaushlesh.Controller.AddFavPostController
import com.kaushlesh.Controller.AddPostViewrController
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.Controller.PhoneCallController
import com.kaushlesh.R
import com.kaushlesh.adapter.RelaventPostAdapter
import com.kaushlesh.adapter.SliderAdapterExample
import com.kaushlesh.bean.AdvertisementBean
import com.kaushlesh.bean.MobileBrandBeans
import com.kaushlesh.bean.PostDetails.MobilesPostDetailsBean
import com.kaushlesh.bean.PostDetails.postDetailsBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.PostDetail.GetDetailsFromValue
import com.kaushlesh.utils.PostDetail.SetPostDetailData
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.ChatDetailActivity
import com.kaushlesh.view.activity.Login.LoginMainActivity
import com.kaushlesh.view.activity.productDetails.ReportAdActivity
import com.kaushlesh.view.fragment.MyAccount.MyNetwork.FollowerProfileDetailActivity
import com.kaushlesh.widgets.CustomButton
import com.smarteist.autoimageslider.SliderView
import kotlinx.android.synthetic.main.activity_grain_fruit_veg_detail.*
import kotlinx.android.synthetic.main.activity_mobile_tabs_detail.*
import kotlinx.android.synthetic.main.common_social_layout.btn_call
import kotlinx.android.synthetic.main.common_social_layout.rlWhatsappCall

import kotlinx.android.synthetic.main.common_social_layout.btn_chat
import kotlinx.android.synthetic.main.activity_mobile_tabs_detail.scroll
import kotlinx.android.synthetic.main.activity_product_detail.*
import kotlinx.android.synthetic.main.comman_product_details_bottom.*
import kotlinx.android.synthetic.main.comman_product_details_pager.*
import kotlinx.android.synthetic.main.toolbar_with_menu.*
import org.json.JSONException
import org.json.JSONObject
import java.util.ArrayList

class MobileTabsDetailActivity : AppCompatActivity(),ParseControllerListener ,View.OnClickListener,RelaventPostAdapter.ItemClickListener{

    var TAG = MobileTabsDetailActivity::class.java.getSimpleName()
    val dataModel = MobilesPostDetailsBean()
    lateinit var storeUserData: StoreUserData
    var sliderView: SliderView? = null
    private var adapter: SliderAdapterExample? = null
    lateinit var post_id: String
    lateinit var subcatid: String
    lateinit var getListingController: GetListingController
    internal var brandlist: ArrayList<MobileBrandBeans.MobileBrandBeansList> = ArrayList()
    var contact: String = ""

    //like/dislike broadcast
    protected var localBroadcastManager: LocalBroadcastManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mobile_tabs_detail)

        localBroadcastManager = LocalBroadcastManager.getInstance(this)


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        storeUserData=StoreUserData(this)

        if (intent != null) {
            post_id = intent.getStringExtra("postid").toString()
            subcatid = intent.getStringExtra("subCatId").toString()
            Utils.showLog("test","==post id==" + post_id + "==sub cat id==" + subcatid)
        }

        getListingController = GetListingController(this, this)

        getListingController.getProductDetail(post_id)

        //bindRecyclerviewFreshRecommendation()

        AddPostViewrController(this,post_id)

        sliderView = findViewById(R.id.imageSlider);

        btn_back.setOnClickListener(this)
        tv_aboutAd.setOnClickListener(this)
        iv_social_share.setOnClickListener(this)
        btn_chat.setOnClickListener(this)
        iv_delete.setOnClickListener(this)
        btn_call.setOnClickListener(this)
        rlWhatsappCall.setOnClickListener(this)
        llAdOwnerProfile.setOnClickListener(this)

        iv_fav.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                //broadcast
                if(buttonView!!.isPressed){
                    setFavUnfav()
                    if(isChecked){
                        notifyHomePostLikeDislike(true,post_id)
                    }else{
                        notifyHomePostLikeDislike(false,post_id)
                    }
                }
            }
        })



    }

    private fun setDetailAdvertisments(adsList: java.util.ArrayList<AdvertisementBean.Advertisement>) {
        cardDetailAds.visibility = View.VISIBLE
        Glide.with(applicationContext)
                .load(adsList.get(0).appbanner_img)
                .error(R.drawable.bg_img_placeholder)
                .placeholder(R.drawable.progress_animated_home_banner)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .skipMemoryCache(true)
                .into(detailAdImage)
        detailAdTitle.setText(adsList.get(0).appbanner_title)

        detailAdImage.setOnClickListener{
            showBottomsheetMoreOptions(this, adsList.get(0))

        }
    }

    private fun showBottomsheetMoreOptions(
            context: Context,
            advertisement: AdvertisementBean.Advertisement
    ) {

        var whatsappAds: String = ""

        val bottomSheetDialog = BottomSheetDialog(context)
        bottomSheetDialog.setContentView(R.layout.bottom_sheet_advertisement)


        val llWebsite: LinearLayout? = bottomSheetDialog.findViewById(R.id.llWebsite)
        val llCall: LinearLayout? = bottomSheetDialog.findViewById(R.id.llCall)
        val llWhatsapp: LinearLayout? = bottomSheetDialog.findViewById(R.id.llWhatsapp)

        if(advertisement.appbanner_link!=null && advertisement.appbanner_link != ""){
            llWebsite!!.visibility = View.VISIBLE
        }else{
            llWebsite!!.visibility = View.GONE
        }

        if(advertisement.whatsapp_no!=null && advertisement.whatsapp_no != ""){
            whatsappAds = advertisement.whatsapp_no.toString()
            llWhatsapp!!.visibility = View.VISIBLE
        }else{
            llWhatsapp!!.visibility = View.GONE
        }

        if(advertisement.mobile_no!=null && advertisement.mobile_no != ""){
            contact = advertisement.mobile_no.toString()
            llCall!!.visibility = View.VISIBLE
        }else{
            llCall!!.visibility = View.GONE
        }


        llWebsite!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {

                val httpIntent = Intent(Intent.ACTION_VIEW)
                httpIntent.data = Uri.parse(advertisement.appbanner_link.toString())
                startActivity(httpIntent)

                bottomSheetDialog.dismiss()

            }

        })

        llCall!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {

                if (AppConstants.checkUserIsLoggedin(this@MobileTabsDetailActivity)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(arrayOf(Manifest.permission.CALL_PHONE), 200)
                    }
                } else {
                    val intent = Intent(this@MobileTabsDetailActivity, LoginMainActivity::class.java)
                    AppConstants.alertLogin(
                            this@MobileTabsDetailActivity,
                            getString(R.string.txt_login_to_call),
                            intent
                    )
                }

                bottomSheetDialog.dismiss()
            }

        })

        llWhatsapp!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {

                var appPackage = ""
                if (SetPostDetailData.isAppInstalled(this@MobileTabsDetailActivity, "com.whatsapp")) {
                    appPackage = "com.whatsapp"
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data =
                            Uri.parse("http://api.whatsapp.com/send?phone=+91" + whatsappAds + "&text=")
                    startActivity(intent)
                } else if (SetPostDetailData.isAppInstalled(this@MobileTabsDetailActivity, "com.whatsapp.w4b")) {
                    appPackage = "com.whatsapp.w4b"
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data =
                            Uri.parse("http://api.whatsapp.com/send?phone=+91" + whatsappAds + "&text=")
                    startActivity(intent)
                } else {
                    Utils.showToast(this@MobileTabsDetailActivity, "Whatsapp not installed on your device")
                }

                bottomSheetDialog.dismiss()
            }

        })

        bottomSheetDialog.show()
    }

    protected fun notifyHomePostLikeDislike(isLike: Boolean, postId: String) {
        val intent: Intent = Intent("NOTIFY_POST_LIKE_DISLIKE")
        intent.putExtra("islikedState", ""+isLike)
        intent.putExtra("postIdLikeDislike",""+postId)
        localBroadcastManager?.sendBroadcast(intent)
    }

    fun setFavUnfav(){
        if(AppConstants.checkUserIsLoggedin(this)) {
            val c = AddFavPostController(this, this, post_id.toString())
            c.onClick(iv_fav)
        }
        else{
            val intent = Intent(applicationContext, LoginMainActivity::class.java)
            AppConstants.alertLogin(this, getString(R.string.txt_login_ad_fav), intent)
        }
    }

    override fun onClick(v: View?) {
        when (v!!.getId()) {

            R.id.btn_back -> onBackPressed()

            R.id.tv_aboutAd -> {
                if(AppConstants.checkUserIsLoggedin(this))
                {
                    val intent = Intent(this, ReportAdActivity::class.java)
                    intent.putExtra("postId",post_id)
                    startActivity(intent)
                }
                else{
                    val intent = Intent(applicationContext, LoginMainActivity::class.java)
                    AppConstants.alertLogin(this, getString(R.string.txt_login_to_report), intent)
                }
            }


            R.id.btn_chat ->
            {
                saftyInfoDialog("chat")

            }

            R.id.iv_delete ->
            {
                Utils.deletePost(this,this,post_id)
            }

            R.id.btn_call ->
            {
                saftyInfoDialog("call")

            }

            R.id.rlWhatsappCall -> {
                saftyInfoDialog("whatsapp")

            }
            R.id.iv_social_share -> {
                Utils.sharePostLink(dataModel.category_id.toString(),
                        dataModel.sub_category_id.toString(),
                        dataModel.post_id.toString(),
                        dataModel.postImages.get(0).postImage.toString(), dataModel.sub_categoryName.toString(),this);
            }

            R.id.llAdOwnerProfile ->
            {
                val intent = Intent(this, FollowerProfileDetailActivity::class.java)
                Utils.openFollowingUserDetails(this,intent,dataModel.userId.toString())
            }
        }
    }

    private fun saftyInfoDialog(strType: String) {

        val dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_safty_info)
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val button_ok = dialog.findViewById<CustomButton>(R.id.button_ok)


        button_ok.setOnClickListener {
            dialog.dismiss()

            if(strType == "call"){
                if (AppConstants.checkUserIsLoggedin(this)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(arrayOf(Manifest.permission.CALL_PHONE), 200)
                    }
                }
                else {
                    val intent = Intent(this, LoginMainActivity::class.java)
                    AppConstants.alertLogin(this, getString(R.string.txt_login_to_call), intent)
                }
            }else if(strType == "whatsapp"){

                if (AppConstants.checkUserIsLoggedin(this)) {
                    var appPackage = ""
                    if (SetPostDetailData.isAppInstalled(this, "com.whatsapp")) {
                        appPackage = "com.whatsapp"
                        val intent = Intent(Intent.ACTION_VIEW)
                        intent.data = Uri.parse("http://api.whatsapp.com/send?phone=+91" + dataModel.whatsapp_no.toString() + "&text=")
                        startActivity(intent)
                    }else if (SetPostDetailData.isAppInstalled(this, "com.whatsapp.w4b")) {
                        appPackage = "com.whatsapp.w4b"
                        val intent = Intent(Intent.ACTION_VIEW)
                        intent.data = Uri.parse("http://api.whatsapp.com/send?phone=+91" + dataModel.whatsapp_no.toString() + "&text=")
                        startActivity(intent)
                    } else {
                        Utils.showToast(this, "Whatsapp not installed on your device")
                    }
                }else{
                    val intent = Intent(this, LoginMainActivity::class.java)
                    AppConstants.alertLogin(this, getString(R.string.txt_login_to_send_whatsapp), intent)
                }


            }else if(strType == "chat"){
                if (AppConstants.checkUserIsLoggedin(this)) {
                    val intent = Intent(this, ChatDetailActivity::class.java)
                    intent.putExtra("userid", dataModel.userId.toString())
                    intent.putExtra("postid", dataModel.post_id.toString())
                    intent.putExtra("sendername",dataModel.cuserName)
                    intent.putExtra("recivername",dataModel.userName)
                    intent.putExtra("reciverImage",dataModel.profilePicture)
                    intent.putExtra("senderImage",dataModel.cuserProfile)
                    intent.putExtra("productImg",dataModel.postImages.get(0).postImage)
                    intent.putExtra("sub_cat_name",dataModel.sub_categoryName)
                    intent.putExtra("chatType","s")
                    intent.putExtra("sub_cat_id",dataModel.sub_category_id.toString())
                    intent.putExtra("cat_id",dataModel.category_id.toString())
                    intent.putExtra("adTitle",dataModel.title.toString())
                    intent.putExtra("price",dataModel.price.toString())
                    intent.putExtra("from","detail")
                    startActivity(intent)
                } else {
                    val intent = Intent(this, LoginMainActivity::class.java)
                    AppConstants.alertLogin(this, getString(R.string.txt_login_to_see_chat), intent)
                }
            }

        }

        dialog.show()
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {

        if (method.equals("postDetailAds")) {
            if (da.length() > 0) {
                Log.e("postDetailAds", "$da==")
                try {
                    val dataModel = AdvertisementBean()
                    dataModel.AdvertisementBean(da)
                    Log.e(
                        "DETAIL_ADS",
                        "" + GsonBuilder().setPrettyPrinting().create().toJson(dataModel)
                    )
                    if(!dataModel.adsList.isEmpty()){
                        setDetailAdvertisments(dataModel.adsList)
                    }else{
                        cardDetailAds.visibility = View.GONE
                    }
                } catch (e: JSONException) {
                    cardDetailAds.visibility = View.GONE
                    e.printStackTrace()
                }
            }
        }

        if (method.equals("GetBrands")) {

            val dataModel1 = MobileBrandBeans()
            dataModel1.MobileBrandBeans(da)

            brandlist = dataModel1.mobileBrandList
            Utils.showLog(TAG,"==brand list size==" + brandlist.size)

            setDataDetailsInViews(dataModel)
        }

        if (method.equals("Get PostDetails")) {

            Utils.dismissProgress()

            scroll.fullScroll(View.FOCUS_UP)

            dataModel.MobilesPostDetailsBean(da)

            getListingController.getBrandList()

            //detail page advertisments
            getListingController.getPostDetailAdvertisements(dataModel.category_id.toString())

           // setDataDetailsInViews(dataModel)
        }

        if (method.equals("adFavPost")) {
            Utils.showToast(this,message)
            //SetPostDetailData.setFav(iv_fav,message.toString()!!)

            Utils.showLog(TAG,"==is favourite ==" + dataModel.isFavourite)

        }

    }

    override fun onFail(msg: String, method: String) {
        if(method.equals("Get PostDetails") || method.equals("adFavPost")) {
            Utils.showToast(this, msg)
        }
    }

    private fun setDataDetailsInViews(dataModel: MobilesPostDetailsBean) {

        contact = dataModel.contactNo.toString()

        if(dataModel.bussinessprofile_verified_status != null){
            if(dataModel.bussinessprofile_verified_status.toString() == "1"){
                rlviewBusinessprfl.visibility = View.VISIBLE
            }else{
                rlviewBusinessprfl.visibility = View.GONE
            }
        }

        tv_title.text= dataModel.title
        tv_post_date.text=getString(R.string.txt_post_date)+ " "+ dataModel.published_date
        tv_price.text="₹ "+dataModel.price.toString()
        GetDetailsFromValue.setAboutInfo(dataModel.aboutInfo.toString(),tv_about_info)

        for(i in brandlist.indices)
        {
            //Utils.showLog(TAG,"==list id==" + brandlist.get(i).brandId + "==id==" + dataModel.brandId)
            if(brandlist.get(i).brandId == dataModel.brandId)
            {
                Utils.showLog(TAG,"==list id==" + brandlist.get(i).brandId + "==id==" + dataModel.brandId)
                tv_brand.text = brandlist.get(i).brandName.toString()
            }
        }

        tv_desc.text=dataModel.other_information
        tv_ad_location.text = dataModel.address

        Glide.with(applicationContext)
            .load(dataModel.profilePicture)
            .placeholder(R.drawable.ic_user_pic)
            .into(owner_profile_image)

        GetDetailsFromValue.setUnderlineOnName(dataModel.userName,tv_owner_name)
        tv_date_act.text=getString(R.string.txt_since)+" "+ dataModel.signUpDate
        tv_add_id.text = getString(R.string.txt_ad_id)+" "+ dataModel.ad_id

        adapter = SliderAdapterExample(this,dataModel.postImages);
        SetPostDetailData.setSlider(this,dataModel.postImages, adapter!!,sliderView!!,tv_count)

        SetPostDetailData.setRetaedArray(this,rv_recom,dataModel.relatedArray,this)

        if(mapView!=null){
            SetPostDetailData.setmapdata(supportFragmentManager,mapView, dataModel.latitude.toString(),dataModel.longitude.toString())
        }

        if(dataModel.isFavourite!!.toInt() == 1) {
            iv_fav.isChecked = true
        } else{
            iv_fav.isChecked = false
        }

        if(dataModel.userId != null && dataModel.userId.toString().length >0 && storeUserData.getString(Constants.USER_ID).length >0  && storeUserData.getString(Constants.IS_LOGGED_IN).equals("true")) {
            SetPostDetailData.SetchatButton(storeUserData.getString(Constants.USER_ID).toInt(), dataModel.userId!!.toInt(), btn_chat)
        }
        else{
            btn_chat.visibility = View.VISIBLE
        }

        if(dataModel.userId != null && dataModel.userId.toString().length >0 && storeUserData.getString(Constants.USER_ID).length >0 && storeUserData.getString(Constants.IS_LOGGED_IN).equals("true")) {
            SetPostDetailData.setcallButton(storeUserData.getString(Constants.USER_ID).toInt(), dataModel.userId!!.toInt(),btn_call,dataModel.isphone!!.toInt())
        }
        else{
            btn_call.visibility = View.VISIBLE
        }

        //new added
        if(dataModel.userId != null && dataModel.userId.toString().length >0 && storeUserData.getString(Constants.USER_ID).length >0 && storeUserData.getString(Constants.IS_LOGGED_IN).equals("true")) {
            SetPostDetailData.setWhatsappButton(storeUserData.getString(Constants.USER_ID).toInt(), dataModel.userId!!.toInt(),rlWhatsappCall,dataModel.whatsapp_inquiry_allow!!.toInt())
        } else{
            rlWhatsappCall.visibility = View.VISIBLE
        }

    }




    fun bindRecyclerviewRelevantPost(RelatedArrayList: ArrayList<postDetailsBean.RelatedArray>) {

        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rv_recom.layoutManager = layoutManager
        val adapter = RelaventPostAdapter(RelatedArrayList, this)
        //adapter.setClicklistner(this)
        rv_recom.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun itemclickRecommendation(bean: postDetailsBean.RelatedArray) {

        dataModel.postImages.clear()
        dataModel.relatedArray.clear()

        getListingController = GetListingController(this, this)

        getListingController.getProductDetail(bean.post_id.toString())

        AddPostViewrController(this,post_id)
    }
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == 200) {
            val result = checkCallingOrSelfPermission(Manifest.permission.CALL_PHONE)
            if (result == PackageManager.PERMISSION_GRANTED) {

                val c = PhoneCallController(this, contact)
                c.makePhoneCall()
                /*  val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:$contact"))
                  startActivity(intent)*/
            } else {
               Utils.showToast(this,resources.getString(R.string.txt_phone_permission))
            }
        }
    }
}
