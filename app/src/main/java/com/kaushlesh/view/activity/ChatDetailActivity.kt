package com.kaushlesh.view.activity

import android.app.Activity
import android.app.TimePickerDialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.SystemClock
import android.text.Editable
import android.text.TextWatcher
import android.text.format.DateFormat
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TimePicker
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.database.*
import com.kaushlesh.Controller.ChatNotificationController
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.Controller.NotificationViewController
import com.kaushlesh.R
import com.kaushlesh.bean.chat.ChatNew
import com.kaushlesh.bean.chat.ChatNewList
import com.kaushlesh.chat.DateUtils.getFormattedTime
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.PostDetail.CheckCategoryForPostDetails
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.fragment.MyAccount.MyNetwork.FollowerProfileDetailActivity
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_business_profile.*
import kotlinx.android.synthetic.main.activity_chat_detail.*
import kotlinx.android.synthetic.main.chat_date.view.*
import kotlinx.android.synthetic.main.chat_from_row.view.*
import kotlinx.android.synthetic.main.chat_to_row.view.*
import kotlinx.android.synthetic.main.layout_toolbar_chat.*
import kotlinx.android.synthetic.main.layout_toolbar_chat.btn_back
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*


class ChatDetailActivity : AppCompatActivity() ,View.OnClickListener,ParseControllerListener{
    val TAG = "ChatNewActivity"
    var counter = 0
    val adapter = GroupAdapter<ViewHolder>()
    var luserid : Int ?= null
    var userid :Int ?= null
    var postid :Int ?= null
    var sender_name :String?= null
    var recivername :String?= null
    var reciverImage :String?= null
    var senderImage :String?= null
    var productImg :String?= null
    var sub_cat_name :String?= null
    var chattype :String?= null
    var from :String?= null
    var chattypes :String?= null
    var chattyper :String?= null
    var sub_cat_id :String?= null
    var cat_id :String?= null
    var adtitle :String?= null
    var price :String?= null
    var deleteDate :String = ""
    var postuserid :Int ?= null
    var chatflag :Boolean = false


    internal var list: ArrayList<ChatNew> = ArrayList()

    lateinit var storeUserData: StoreUserData
    lateinit var getListingController: GetListingController

    private var mLastClickTime: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_detail)

        storeUserData= StoreUserData(this@ChatDetailActivity)
        luserid = storeUserData.getString(Constants.USER_ID).toString().toInt()

        getListingController = GetListingController(this, this)

        if(intent != null) {
             userid = intent.getStringExtra("userid").toString().toInt()
             postid = intent.getStringExtra("postid").toString().toInt()
             sender_name = intent.getStringExtra("sendername").toString()
             recivername = intent.getStringExtra("recivername").toString()
             reciverImage = intent.getStringExtra("reciverImage").toString()
             senderImage = intent.getStringExtra("senderImage").toString()
             productImg = intent.getStringExtra("productImg").toString()
             sub_cat_name = intent.getStringExtra("sub_cat_name").toString()
             chattype = intent.getStringExtra("chatType").toString()
             from = intent.getStringExtra("from").toString()
            sub_cat_id = intent.getStringExtra("sub_cat_id").toString()
            cat_id = intent.getStringExtra("cat_id").toString()
            adtitle = intent.getStringExtra("adTitle").toString()
            price = intent.getStringExtra("price").toString()
            deleteDate = intent.getStringExtra("deleteDate").toString()

            setData()
        }


        Utils.showLog(TAG, "==isfrom==" + from)
        Utils.showLog(TAG, "==from id==" + luserid + "== to id==" + userid)
        Utils.showLog(TAG, "==from name==" + sender_name + "== to name==" + recivername)
        Utils.showLog(TAG, "==sender image==" + senderImage + "==to image==" + reciverImage)
        Utils.showLog(TAG, "==sub cat id==" + sub_cat_id + "==cat id==" + cat_id)
        Utils.showLog(TAG, "==adtitle==" + adtitle + "==price==" + price)
        Utils.showLog(TAG, "==deleteDate==" + deleteDate)

        if(from.equals("detail"))
        {
            chattypes = "s"
            chattyper = "b"
        }
        else{
            postuserid = luserid
            if(chattype.equals("b"))
            {
                chattypes = "s"
                chattyper = "b"
            }
            else{
                chattypes = "b"
                chattyper = "s"
            }
        }

        getListingController.getUserOrPostExistDetail(postid!!, userid.toString())

        recyclerview_chat.adapter = adapter

        if (userid!! < luserid!!) {
            listenForMessages(userid!!, luserid!!)
        }
        else{
            listenForMessages(luserid!!, userid!!)
        }

        send_button_chat_log.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                //getListingController.getUserOrPostExistDetail(postid!!, "")

                //prevent double click on button
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if(Utils.isConnected()){
                    if(chatflag) {
                        if (userid!! < luserid!!) {
                            performSendMessage(userid!!, luserid!!)
                        } else {
                            performSendMessage(luserid!!, userid!!)
                        }
                    }
                    else{
                        Utils.showToast(this@ChatDetailActivity, getString(R.string.txt_product_sold))
                    }
                }
            }
        })

        /*send_button_chat_log.setOnClickListener {

            //getListingController.getUserOrPostExistDetail(postid!!, "")

            //prevent double click on button
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                return@setOnClickListener;
            }

            if(Utils.isConnected()){
                if(chatflag) {
                    if (userid!! < luserid!!) {
                        performSendMessage(userid!!, luserid!!)
                    } else {
                        performSendMessage(luserid!!, userid!!)
                    }
                }
                else{
                    Utils.showToast(this, getString(R.string.txt_product_sold))
                }
            }

        }*/

        btn_back.setOnClickListener {

            //FirebaseDatabase.getInstance().getReference("/List/").child("$luserid").child("$userid-$luserid-$postid").child("counter").setValue("0")
            val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            if (inputMethodManager.isActive) {
                if (getCurrentFocus() != null) {
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus()!!.getWindowToken(), 0)
                }
            }
            onBackPressed()
        }

        rl_img_main.setOnClickListener(this)
        tv_uname.setOnClickListener(this)


        edittext_chat_log.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(str: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(str: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(str: Editable) {
                if (str.toString().trim { it <= ' ' }.length > 0) {
                    send_button_chat_log.setTextColor(resources.getColor(R.color.orange))
                } else {
                    send_button_chat_log.setTextColor(resources.getColor(R.color.black))
                }
            }
        })
    }

    private fun setData() {

        tv_category_name.text = sub_cat_name.toString()
        tv_uname.text = recivername.toString()

        tv_ad_title.text = adtitle.toString()

        if(price.equals("0"))
        {
            tv_price.visibility = View.GONE
        }
        else{
            tv_price.visibility = View.VISIBLE
            tv_price.text = "₹ "+price.toString()
        }


        val requestOptions = RequestOptions().placeholder(R.drawable.ic_user_pic)
        Glide.with(applicationContext)
            .load(reciverImage)
            .apply(requestOptions)
            .into(civ_profile)

        val requestOptions1 = RequestOptions().placeholder(R.drawable.bg_img_placeholder)
        Glide.with(applicationContext)
            .load(productImg)
            .apply(requestOptions1)
            .into(iv_product_img)
    }

    private fun listenForMessages(id1: Int, id2: Int) {
        val ref = FirebaseDatabase.getInstance().getReference("$luserid").child("$id1-$id2").child("$postid")

        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                Log.d(TAG, "database error: " + databaseError.message)
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                Log.d(TAG, "has children: " + dataSnapshot.hasChildren())
                if (!dataSnapshot.hasChildren()) {

                }
            }
        })

        ref.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
            }

            override fun onChildMoved(dataSnapshot: DataSnapshot, previousChildName: String?) {
            }

            override fun onChildChanged(dataSnapshot: DataSnapshot, previousChildName: String?) {
            }

            override fun onChildAdded(dataSnapshot: DataSnapshot, previousChildName: String?) {
                dataSnapshot.getValue(ChatNew::class.java)?.let {
                    Utils.showLog("t1", "==if==" + it.id + storeUserData.getString(Constants.USER_ID))

                    if (it.id.toString().equals(storeUserData.getString(Constants.USER_ID))) {
                        Utils.showLog("t1", "==if==" + it.text)
                        //adapter.add(ChatToItem(it.text, it.time))
                        val timeeee = String.format("%.0f", it.timestamp.toFloat()).toLong()
                        Utils.showLog("t1", "==time==" + timeeee)

                        adapter.add(ChatToItem(it.text, getFormattedTime(timeeee.toLong(), this@ChatDetailActivity)))
                        Utils.showLog("t1", "==time==" + getFormattedTime(timeeee.toLong(), this@ChatDetailActivity))

                    } else {
                        Utils.showLog("t1", "==else==")
                        //val sender_ref = FirebaseDatabase.getInstance().getReference("/List/").child("$userid").child("$userid-$postid").child("counter").setValue("0")
                        //adapter.add(ChatFromItem(it.text, it.time))
                        val timeeee = String.format("%.0f", it.timestamp.toFloat()).toLong()
                        adapter.add(ChatFromItem(it.text, getFormattedTime(timeeee.toLong(), this@ChatDetailActivity)))
                        Utils.showLog("t1", "==time==" + timeeee)
                        Utils.showLog("t1", "==time==" + getFormattedTime(timeeee.toLong(), this@ChatDetailActivity))
                    }
                }
                recyclerview_chat.scrollToPosition(adapter.itemCount - 1)
            }

            override fun onChildRemoved(dataSnapshot: DataSnapshot) {
            }

        })
    }

    override fun onClick(v: View?) {
        when (v!!.getId()) {
            R.id.rl_img_main -> {

                if(chatflag) {
                    CheckCategoryForPostDetails.OpenScreen(this, cat_id!!.toInt(), sub_cat_id!!.toInt(), postid!!.toInt())
                }
                else{
                    Utils.showToast(this, getString(R.string.txt_post_not_found))
                }
            }

            R.id.tv_uname -> {
                val intent = Intent(this, FollowerProfileDetailActivity::class.java)
                Utils.openFollowingUserDetails(this, intent, userid.toString())
            }
        }
    }

    private fun performSendMessage(id1: Int, id2: Int) {

        //getDateAfter35Days()

        val sender_ref1 = FirebaseDatabase.getInstance().getReference("/List/").child("$userid").child("$luserid-$userid-$postid")
        Log.d("TAG", "counter" + sender_ref1)
         val eventListener: ValueEventListener = object : ValueEventListener {
             override fun onDataChange(dataSnapshot: DataSnapshot) {
                 counter = dataSnapshot.child("counter").getValue(String::class.java)!!.toInt()
                 deleteDate = dataSnapshot.child("deleteDate").getValue(String::class.java)!!
                 Log.d("TAG", "counter" + counter + "delete date==" + deleteDate)

             }

             override fun onCancelled(databaseError: DatabaseError) {}
         }
         sender_ref1.addListenerForSingleValueEvent(eventListener)


        val text = edittext_chat_log.text.toString()
        if (text.isEmpty()) {
            Utils.showToast(this, getString(R.string.txt_empty_message))
            return
        }

        val d = Date()
        val date: CharSequence = DateFormat.format("dd/MM/yyyy", d.getTime())

       // val dateFormat = SimpleDateFormat("hh:mm a")
        val dateFormat = SimpleDateFormat("HH:mm")
        val cTime: String = dateFormat.format(Date().time)

        val currentTimestamp = System.currentTimeMillis() / 1000

        val reference = FirebaseDatabase.getInstance().getReference("$luserid").child("$id1-$id2").child("$postid").push()
        val reference1 = FirebaseDatabase.getInstance().getReference("$userid").child("$id1-$id2").child("$postid").push()

        val chat = ChatNew(date.toString(), storeUserData.getString(Constants.USER_ID), sender_name.toString(), text, cTime.toString(), currentTimestamp.toString())

        reference.setValue(chat)
            .addOnSuccessListener {
                Log.d(TAG, "Saved our chat message: ${reference.key}")
               // adapter.add(ChatToItem(text, cTime))
                //adapter.notifyDataSetChanged()
                edittext_chat_log.text.clear()
                recyclerview_chat.smoothScrollToPosition(adapter.itemCount - 1)
            }

        reference1.setValue(chat)

        if(deleteDate.equals("null"))
        {
            deleteDate = getDateAfter35Days()
        }

        val sender_ref = FirebaseDatabase.getInstance().getReference("/List/").child("$luserid").child("$userid-$luserid-$postid")
        val chat_sender = ChatNewList(postid.toString(), chattyper.toString(), "0", date.toString(), userid.toString(), productImg.toString(), recivername.toString(), text, cTime.toString(), sub_cat_name.toString(), reciverImage.toString(), currentTimestamp.toString(), sub_cat_id.toString(), cat_id.toString(), adtitle.toString(), price.toString(), deleteDate)
        sender_ref.setValue(chat_sender)

        val reciever_ref = FirebaseDatabase.getInstance().getReference("/List/").child("$userid").child("$luserid-$userid-$postid")
        val chat_receiver = ChatNewList(postid.toString(), chattypes.toString(), (counter + 1).toString(), date.toString(), luserid.toString(), productImg.toString(), sender_name.toString(), text, cTime.toString(), sub_cat_name.toString(), senderImage.toString(), currentTimestamp.toString(), sub_cat_id.toString(), cat_id.toString(), adtitle.toString(), price.toString(), deleteDate)
        reciever_ref.setValue(chat_receiver)

        //recyclerview_chat_log.scrollToPosition(adapter.itemCount - 1)


        sendChatPushNotification(text)

    }


    private fun sendChatPushNotification(textMessage: String) {
        ChatNotificationController(this,luserid,userid,sender_name,textMessage)
    }






    private fun getDateAfter35Days(): String {
        val date = Date()
        var df = SimpleDateFormat("dd/MM/yyyy")
        val c1: Calendar = Calendar.getInstance()
        val currentDate: String = df.format(date) // get current date here

        c1.add(Calendar.DAY_OF_YEAR, 35)
        df = SimpleDateFormat("dd/MM/yyyy")
        val resultDate: Date = c1.getTime()
        val dueDate: String = df.format(resultDate)

        // print the result
        Utils.showLog("DATE_DATE :-> ", currentDate)
        Utils.showLog("DUE_DATE :-> ", dueDate)

        return dueDate
    }

    class ChatToItem(val text: String, val time: String) : Item<ViewHolder>() {

        override fun bind(viewHolder: ViewHolder, position: Int) {
            viewHolder.itemView.textview_from_row.text = text
            viewHolder.itemView.time_tv.text = time
        }

        override fun getLayout(): Int {
            return R.layout.chat_from_row
        }

    }

    class ChatFromItem(val text: String, val time: String) : Item<ViewHolder>() {

        override fun bind(viewHolder: ViewHolder, position: Int) {

            viewHolder.itemView.textview_to_row.text = text
            viewHolder.itemView.time_tv1.text = time
        }

        override fun getLayout(): Int {
            return R.layout.chat_to_row
        }

    }


    override fun onBackPressed() {
       //FirebaseDatabase.getInstance().getReference("/List/").child("$luserid").child("$userid-$luserid-$postid").child("counter").setValue("0")

        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        if (inputMethodManager.isActive) {
            if (getCurrentFocus() != null) {
                inputMethodManager.hideSoftInputFromWindow(
                        getCurrentFocus()!!.getWindowToken(), 0
                )
            }
        }
        super.onBackPressed()
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        Utils.showLog(TAG, "==data onSuccess==" + da)

        if(method.equals("userPostExist")){
            chatflag = true
        }



       /* if (userid!! < luserid!!) {
            performSendMessage(userid!!, luserid!!)
        } else {
            performSendMessage(luserid!!, userid!!)
        }*/
    }

    override fun onFail(msg: String, method: String) {
        Utils.showLog(TAG, "==data onFail==" + msg)

        if(method.equals("userPostExist")){
            chatflag= false
        }


        //Utils.showToast(this, getString(R.string.txt_product_sold))
    }
}