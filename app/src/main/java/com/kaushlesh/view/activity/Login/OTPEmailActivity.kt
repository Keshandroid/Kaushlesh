package com.kaushlesh.view.activity.Login

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.kaushlesh.R
import com.kaushlesh.view.activity.MainActivity
import kotlinx.android.synthetic.main.activity_otpemail.*

class OTPEmailActivity : AppCompatActivity() {

    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otpemail)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.txt_login)


        btnback.setOnClickListener {
            onBackPressed()
        }

        et_one.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                if (!et_one.getText().toString().equals(""))
                    et_two.requestFocus()
            }
        })
        et_two.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (et_two.getText().toString().length == 0) {
                    et_three.requestFocus()
                }
            }

            override fun afterTextChanged(s: Editable) {
                if (!et_two.getText().toString().equals(""))
                    et_three.requestFocus()
            }
        })
        et_three.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (et_three.getText().toString().length == 0) {
                    et_two.requestFocus()
                }
            }

            override fun afterTextChanged(s: Editable) {
                if (!et_three.getText().toString().equals(""))
                    et_four.requestFocus()
            }
        })
        et_four.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (et_four.getText().toString().length == 0) {
                    et_three.requestFocus()
                }
            }

            override fun afterTextChanged(s: Editable) {


                val finalotp = et_one.getText().toString() + et_two.getText().toString() + et_three.getText().toString() + et_four.getText().toString()

                val intent = Intent(applicationContext, MainActivity::class.java)
                startActivity(intent)
                finishAffinity()
            }
        })
    }
}
