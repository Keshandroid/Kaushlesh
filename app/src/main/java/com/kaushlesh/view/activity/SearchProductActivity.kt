package com.kaushlesh.view.activity

import android.app.Activity
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.FetchPlaceRequest
import com.google.android.libraries.places.api.net.PlacesClient
import com.kaushlesh.AutoCompleteAdapter
import com.kaushlesh.R
import com.kaushlesh.api.API
import com.kaushlesh.bean.ProductDataBean
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.parser.GetRequestParsing
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import kotlinx.android.synthetic.main.activity_search_product.*
import kotlinx.android.synthetic.main.activity_set_location.act_location
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList




class SearchProductActivity : AppCompatActivity(), ParseControllerListener {
    internal lateinit var placesClient: PlacesClient
    internal lateinit var autoCompleteAdapter: AutoCompleteAdapter
    internal var latitude: Double = 0.toDouble()
    internal var longitude: Double = 0.toDouble()
    private val TAG = "SetLocation"
    private var userId:String?=null
    private var userToken:String?=null
    private var serchWord:String?=null
    lateinit var storeUserData: StoreUserData
    lateinit var SerachList:ArrayList<String>

    internal var list: ArrayList<ProductDataBean> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_product)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        storeUserData=StoreUserData(this)

        userId=storeUserData.getString(Constants.USER_ID)
        userToken=storeUserData.getString(Constants.TOKEN)



        val apiKey = getString(R.string.google_place_api_key)
        if (apiKey.isEmpty()) {
            //responseView.setText(getString(R.string.error));
            return
        }

        // Setup Places Client
        if (!Places.isInitialized()) {
            Places.initialize(applicationContext, apiKey)
        }
      /*  tv_word.setOnClickListener{
            val text = tv_word.text.toString()
            tv_word.threshold = 1
            callforDisplayUserData(text)
        }*/

        btn_done.setOnClickListener {
            storeUserData.setString(Constants.LATITUDE, latitude.toString())
            storeUserData.setString(Constants.LONGITUDE, longitude.toString())
            storeUserData.setString(Constants.SEARCHPRODUCT, tv_word.text.toString())
            storeUserData.setString(Constants.SEARCHPRODUCTValue,"1")

            //val pf =  ProductListFragment.newInstance()
            //pf.callforDisplayUserDatawithsearch(latitude.toString(),longitude.toString(),tv_word.text.toString(),storeUserData.getString(Constants.USER_ID),storeUserData.getString(Constants.TOKEN))

            //onBackPressed()
            finish()
        }

        btn_close.setOnClickListener {

            finish()
        }
        placesClient = Places.createClient(applicationContext)
        initAutoCompleteTextView()
        initAutoCompleteTextViewSearch()

        tv_word.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                callforDisplayUserData(s.toString())
            }
        })
    }

    private fun initAutoCompleteTextViewSearch() {

        Utils.showLog(TAG,"==list1==" +list)

        SerachList= arrayListOf()
        tv_word.threshold = 1

        for (i in 0 until list.size) {
            SerachList.add(list[i].title.toString())
        }
        Utils.showLog(TAG,"==list==" +SerachList)

        //SerachList.filter { SerachList.contains(tv_word.text.toString()) }
        val adapter = ArrayAdapter<String>(
            this, // Context
            android.R.layout.simple_dropdown_item_1line, // Layout
            SerachList // Array
        )
        //adapter.getFilter().filter(tv_word.text.toString());
        tv_word.setAdapter(adapter)
        adapter.notifyDataSetChanged()


        tv_word.setOnClickListener{
            val text = tv_word.text
            //adapter.getFilter().filter(text.toString());
            //callforDisplayUserData(text.toString())
            //Toast.makeText(applicationContext,"Inputted : $text",Toast.LENGTH_SHORT).show()
        }
        tv_word.threshold = 1

        tv_word.onItemClickListener = AdapterView.OnItemClickListener{
                parent,view,position,id->
            val selectedItem = parent.getItemAtPosition(position).toString()
            // Display the clicked item using toast

        }

        tv_word.setOnDismissListener {
        }

        tv_word.onFocusChangeListener = View.OnFocusChangeListener{
                view, b ->
            if(b){
                // Display the suggestion dropdown on focus
                tv_word.showDropDown()
            }
        }

    }

    private fun initAutoCompleteTextView() {
        act_location.setThreshold(1)
        act_location.setOnItemClickListener(autocompleteClickListener)
        autoCompleteAdapter = AutoCompleteAdapter(applicationContext, placesClient)
        act_location.setAdapter(autoCompleteAdapter)

    }

    private val autocompleteClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
            try {
                val item = autoCompleteAdapter.getItem(i)
                var placeID: String = ""


                if (item != null) {
                    placeID = item.placeId
                }

                //                To specify which data types to return, pass an array of Place.Fields in your FetchPlaceRequest
                //                Use only those fields which are required.

                val placeFields = Arrays.asList(
                    Place.Field.ID,
                    Place.Field.NAME,
                    Place.Field.ADDRESS,
                    Place.Field.LAT_LNG
                )

                var request: FetchPlaceRequest? = null
                request = FetchPlaceRequest.builder(placeID, placeFields)
                    .build()

                if (request != null) {
                    placesClient.fetchPlace(request).addOnSuccessListener { task ->
                        Log.i(
                            TAG,
                            task.place.name + "\n" + task.place.address + "\n" + task.place.latLng
                        )
                        //String address = task.getPlace().getName() + task.getPlace().getAddress();
                        val address = task.place.address
                        Log.i(TAG, "======== LOCATION=======" + address!!)

                        val latLng = task.place.latLng

                        if (latLng != null) {
                            latitude = latLng.latitude
                            longitude = latLng.longitude
                        }

                        Log.i(
                            TAG,
                            "======== LATITUDE =======$latitude========LONGITUDE=======$longitude"
                        )

                        if (!address.isEmpty()) {
                            /* val intent = Intent(getContext(), ActivitySavedPlacesAddressDetails::class.java)
                             intent.putExtra("isfor", "add")
                             intent.putExtra("address", address)
                             intent.putExtra("latitude", latitude.toString())
                             intent.putExtra("longitude", longitude.toString())
                             startActivity(intent)*/
                        }
                    }.addOnFailureListener { e ->
                        e.printStackTrace()
                        Log.i(TAG, e.message.toString())
                    }
                }

            } catch (e: Exception) {
                e.printStackTrace()
            } catch (e: IllegalArgumentException) {
                e.printStackTrace()
            }
        }



    private fun callforDisplayUserData(text: String) {
        //showProgressDialog(this!!, this.resources.getString(R.string.txt_please_wait))
        val p = GetRequestParsing()
        val url =
            API.GET_SEARCH_POST_DETAILS + "?userId=" + userId + "&userToken=" + userToken + "&serchWord=" + tv_word.text
        p.callApi(this, url, "Get searchlist", this)
    }
    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (method.equals("Get searchlist")) {
            if (da.length() > 0) {
                try {
                    val data = da.getJSONArray("result")

                    for (i in 0 until data.length()) {
                        val bean1 = ProductDataBean()
                        bean1.post_id = data.getJSONObject(i).getString("post_id").toInt()
                       // bean1.address = data.getJSONObject(i).getString("address")
                        bean1.title = data.getJSONObject(i).getString("title")

                        list.add(bean1)

                        Utils.showLog(TAG,"==title=="+ bean1.title)
                        initAutoCompleteTextViewSearch()
                    }
                    dismissProgressDialog()


                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onFail(msg: String, method: String) {
        dismissProgressDialog()
    }

    fun showProgressDialog(activity: Activity, msg: String) {
        if (Utils.progressDialog == null) {
            Utils.showProgress(activity)
        }
    }

    fun dismissProgressDialog() {

        if (Utils.progressDialog != null && Utils.progressDialog!!.isShowing()) {
            Utils.dismissProgress()
        }
    }

}