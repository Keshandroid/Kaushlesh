package com.kaushlesh.view.activity.Login

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.NetworkOnMainThreadException
import android.os.StrictMode
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.facebook.*
import com.facebook.AccessToken
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.kaushlesh.Controller.SocialLoginContoller
import com.kaushlesh.R
import com.kaushlesh.bean.RegisterPhoneBean
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.service.LocalStorage
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.RegisterData
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.MainActivity
import com.kaushlesh.view.fragment.MyAccount.EditProfileActivity
import kotlinx.android.synthetic.main.activity_know_more_detail.*
import kotlinx.android.synthetic.main.activity_login_main.*
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.util.*


class LoginMainActivity : AppCompatActivity(),View.OnClickListener,ParseControllerListener {


    private lateinit var socialLoginController: SocialLoginContoller
    var TAG = LoginMainActivity::class.java.simpleName
    lateinit var mGoogleSignInClient: GoogleSignInClient
    val Req_Code:Int=123
    var firebaseAuth= FirebaseAuth.getInstance()
    private var mCallbackManager: CallbackManager? = null
    private var regType: String? = null
    lateinit var storeusedata: StoreUserData

    //firebase token storage
    lateinit var localStorage: LocalStorage


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //FacebookSdk.sdkInitialize(applicationContext)
        //AppEventsLogger.activateApp(application)
        setContentView(R.layout.activity_login_main)

        getFirebaseToken()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        storeusedata = StoreUserData(applicationContext)

        socialLoginController = SocialLoginContoller(this, this)

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.web_client_id))
            .requestEmail()
            .build()
        // getting the value of gso inside the GoogleSigninClient
        mGoogleSignInClient= GoogleSignIn.getClient(this, gso)


        // initialize the firebaseAuth variable
        firebaseAuth= FirebaseAuth.getInstance()


        try {
            tv_tnc.setAllCaps(false);
            tv_tnc.makeLinks(
                    Pair(getString(R.string.txt_terms_and_conditions), View.OnClickListener {
                        val browserIntent = Intent(Intent.ACTION_VIEW)
                        browserIntent.data = Uri.parse("https://gujaratliving.com/terms-conditions")
                        startActivity(browserIntent)
                    }),
                    Pair(getString(R.string.txt_privacy_policy_know_more), View.OnClickListener {
                        val browserIntent = Intent(Intent.ACTION_VIEW)
                        browserIntent.data = Uri.parse("https://gujaratliving.com/privacy-policy")
                        startActivity(browserIntent)
                    }))
        }catch (e: Exception){
            e.printStackTrace()
        }



        rl_phone.setOnClickListener(this)
        rl_facebook.setOnClickListener(this)
        rl_google.setOnClickListener(this)
        tv_email.setOnClickListener(this)
//        tv_tnc.setOnClickListener(this)
        iv_close.setOnClickListener(this)
    }

    fun TextView.makeLinks(vararg links: Pair<String, View.OnClickListener>) {
        val spannableString = SpannableString(this.text)
        var startIndexOfLink = -1
        for (link in links) {
            val clickableSpan = object : ClickableSpan() {
                override fun updateDrawState(textPaint: TextPaint) {
                    // use this to change the link color
                    // textPaint.color = textPaint.linkColor
                    textPaint.color = resources.getColor(R.color.blue_color)
                    // toggle below value to enable/disable
                    // the underline shown below the clickable text
                    textPaint.isUnderlineText = true
                }

                override fun onClick(view: View) {
                    Selection.setSelection((view as TextView).text as Spannable, 0)
                    view.invalidate()
                    link.second.onClick(view)
                }
            }
            startIndexOfLink = this.text.toString().indexOf(link.first, startIndexOfLink + 1)
//      if(startIndexOfLink == -1) continue // todo if you want to verify your texts contains links text
            spannableString.setSpan(
                clickableSpan, startIndexOfLink, startIndexOfLink + link.first.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
        this.movementMethod = LinkMovementMethod.getInstance() // without LinkMovementMethod, link can not click
        this.setText(spannableString, TextView.BufferType.SPANNABLE)
    }

    private fun getFirebaseToken() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(object : OnCompleteListener<InstanceIdResult?> {
                override fun onComplete(task: Task<InstanceIdResult?>) {
                    if (!task.isSuccessful) {
                        Log.d(TAG, "getInstanceId failed", task.exception)
                        return
                    }

                    // Get new Instance ID token
                    val token = task.result!!.token
                    Log.d(TAG, "firebaseToken success = "+token)
                    localStorage = LocalStorage(applicationContext)
                    localStorage.setToken(token)



                }
            })
    }

    override fun onClick(view: View) {
        when (view.id) {

            R.id.rl_phone -> {
                regType = "phone"
                val intent = Intent(this@LoginMainActivity, AddPhoneNumberActivity::class.java)
                startActivity(intent)
            }

            R.id.rl_facebook -> {
                /*  val intent = Intent(applicationContext, MainActivity::class.java)
                startActivity(intent)
                finish()*/

                Utils.showProgress(this)

                regType = "social"

                //Facebook signout
                LoginManager.getInstance().logOut()
                AccessToken.setCurrentAccessToken(null)


                //Google SignOut
                //New Added start
                /*LoginManager.getInstance().logOut()
                mGoogleSignInClient.signOut()*/
                //New Added end


                /*val loggedIn = AccessToken.getCurrentAccessToken() != null

                if(loggedIn){
                    Log.d("loggedIn"," Logged in : " + loggedIn)
                }else{
                    Log.d("loggedIn"," Not Logged In : " + loggedIn)
                }*/


                /*   if (AccessToken.getCurrentAccessToken() != null) {
                    GraphRequest(
                            AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE,
                            GraphRequest.Callback {
                                AccessToken.setCurrentAccessToken(null)
                                LoginManager.getInstance().logOut()
                            }
                    ).executeAsync()
                }*/

                Fblogin()
            }

            R.id.rl_google -> {
                /* val intent = Intent(applicationContext, MainActivity::class.java)
                startActivity(intent)
                finish()*/

                Utils.showProgress(this)


                regType = "social"

                //Google SignOut
                LoginManager.getInstance().logOut()
                mGoogleSignInClient.signOut()


                //Facebook signout
                //New Added start
                /*LoginManager.getInstance().logOut()
                AccessToken.setCurrentAccessToken(null)*/
                //New Added end

                val signInIntent: Intent = mGoogleSignInClient.signInIntent
                startActivityForResult(signInIntent, Req_Code)
            }

            R.id.tv_email -> {
                val intent = Intent(this@LoginMainActivity, AddEmailActivity::class.java)
                startActivity(intent)
            }


            R.id.iv_close -> {
                regType = "none"
                storeusedata.setString(Constants.REGISTREATION_TYPE, "none")
                val intent = Intent(applicationContext, MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==Req_Code){

            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleResult(task)

        } else{
            mCallbackManager?.onActivityResult(requestCode, resultCode, data)
        }



    }

    //google login
    private fun handleResult(completedTask: Task<GoogleSignInAccount>){
        try {
            val account: GoogleSignInAccount? =completedTask.getResult(ApiException::class.java)
            if (account != null) {
                val credential= GoogleAuthProvider.getCredential(account.idToken, null)
                firebaseAuth.signInWithCredential(credential).addOnCompleteListener { task->
                    if(task.isSuccessful) {

                        val email = account.email.toString()
                        val name = account.displayName.toString()
                        val id = account.id.toString()
                        val token = account.idToken.toString()
                        val personPhoto = account.getPhotoUrl()

                        Log.e(TAG, "==email==" + email)
                        Log.e(TAG, "==name==" + name)
                        Log.e(TAG, "==id==" + id)
                        Log.e(TAG, "==token==" + token)
                        Log.e(TAG, "==image==" + personPhoto)


                        localStorage = LocalStorage(applicationContext)


                        val registerData = RegisterData()

                        registerData.email = email
                        registerData.socialid = id
                        registerData.socialtoken = token
                        registerData.socialtype = "google"
                        registerData.phone = ""
                        registerData.name = name
                        registerData.devicetoken = localStorage.getToken().toString()

                        if (personPhoto != null) {
                            // registerData.imgFilePath = ImageFilePath.getPath(this, personPhoto!!)!!
                            //registerData.imgFilePath = personPhoto.path.toString()
                            val imageURL = URL(personPhoto.toString())
                            val connection = imageURL.openConnection() as HttpURLConnection
                            connection.doInput = true
                            connection.connect()

                            val input = connection.inputStream
                            val myBitmap = BitmapFactory.decodeStream(input)
                            Log.i(TAG, "===google bitmap++$myBitmap")


                            val bao = ByteArrayOutputStream()
                            myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bao)
                            val ba = bao.toByteArray()

                            Log.i(TAG, "===byte array ==== " + ba);

                            registerData.socialimgFilePath =ba
                            registerData.profile = myBitmap

                        }

                        socialLoginController.googleLogin(registerData)
                    }
                }
            }
        } catch (e: ApiException){
            Utils.dismissProgress()

            //AppConstants.printToast(this, e.toString())
        }
    }

    //fb login
    private fun Fblogin() {
        mCallbackManager = CallbackManager.Factory.create()

        // Set permissions
        LoginManager.getInstance().logInWithReadPermissions(
            this, Arrays.asList(
                "email",
                "public_profile"
            )
        )

        LoginManager.getInstance().registerCallback(mCallbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {

                    println("Success")

                    val request = GraphRequest.newMeRequest(
                        loginResult.accessToken
                    ) { `object`, response ->
                        Log.i("TAG", `object`.toString())
                        try {

                            val accessToken = loginResult.accessToken.token

                            //AccessToken accessToken = AccessToken.getCurrentAccessToken();
                            val first_name = `object`.getString("first_name")
                            val last_name = `object`.getString("last_name")
                            val email = `object`.getString("email")
                            val id = `object`.getString("id")
                            val image_url = "https://graph.facebook.com/$id/picture?type=normal"

                            Log.i(
                                TAG,
                                "==============FB Info ========= " + "firstname :" + first_name + " lastname :" + last_name + "email  :" + email + "img : " + image_url
                            )
                            Log.i(
                                TAG,
                                "==============FB ID TOKEN ========= " + "id : " + id + "token " + accessToken
                            )

                            try {
                                val imageURL =
                                    URL("https://graph.facebook.com/$id/picture?type=large")

                                Log.i(TAG, "===fb image URL++$imageURL")

                                val connection = imageURL.openConnection() as HttpURLConnection
                                connection.doInput = true
                                connection.connect()

                                val input = connection.inputStream
                                val myBitmap = BitmapFactory.decodeStream(input)

                                Log.i(TAG, "===fb bitmap++$myBitmap")


                                val bao = ByteArrayOutputStream()
                                myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bao)
                                var ba = bao.toByteArray()

                                Log.i(TAG, "===byte array ==== $ba")

                                var registerData = RegisterData()
                                registerData.email = email
                                registerData.socialid = id
                                registerData.socialtoken = accessToken
                                registerData.socialtype = "facebook"
                                registerData.socialimgFilePath = ba
                                registerData.profile = myBitmap
                                registerData.phone = ""
                                registerData.name = first_name + " " + last_name

                                socialLoginController.facebookLogin(registerData)

                            } catch (e: MalformedURLException) {
                                e.printStackTrace()
                            } catch (e: IOException) {
                                e.printStackTrace()
                            } catch (e: NetworkOnMainThreadException) {
                                e.printStackTrace()
                            }

                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }

                    val parameters = Bundle()
                    parameters.putString("fields", "first_name,last_name,email,id")
                    request.parameters = parameters
                    request.executeAsync()
                }

                override fun onCancel() {
                    Utils.dismissProgress()
                    Log.i(TAG, "On cancel")
                }

                override fun onError(error: FacebookException) {
                    Utils.dismissProgress()
                    Log.i(TAG, error.toString())
                }
            })
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        Utils.dismissProgress()
        val dataModel = RegisterPhoneBean()
        dataModel.RegisterPhoneBean(da)
        Utils.showToast(this, message)
        Utils.showLog(TAG, "==message==" + message + "==response==" + da)
        Log.e(
            "get data",
            "name" + dataModel.name.toString() + "email==" + dataModel.ConatctNo.toString()
        )

        if(dataModel.isRegister?.toInt() == 0) {
            if (dataModel.ConatctNo!!.toString().length == 0 || dataModel.name!!.toString().length == 0) {
                val intent1 = Intent(applicationContext, EditProfileActivity::class.java)
                val bundle = Bundle()
                bundle.putSerializable("verifydata", dataModel)
                intent1.putExtras(bundle)
                startActivity(intent1)
                finishAffinity()
            } else {
                val storeusedata = StoreUserData(this)
                storeusedata.setString(Constants.IS_LOGGED_IN, "true")
                val intent = Intent(applicationContext, MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
        else{
            if (dataModel.ConatctNo!!.toString().length == 0 || dataModel.locationId!!.toInt() == 0) {
                val intent1 = Intent(applicationContext, EditProfileActivity::class.java)
                val bundle = Bundle()
                bundle.putSerializable("verifydata", dataModel)
                intent1.putExtras(bundle)
                startActivity(intent1)
                finishAffinity()
            } else {
                val storeusedata = StoreUserData(this)
                storeusedata.setString(Constants.IS_LOGGED_IN, "true")
                val intent = Intent(applicationContext, MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }

    override fun onFail(msg: String, method: String) {
        Utils.dismissProgress()
        Utils.showLog(TAG, "==message==" + msg)
    }
}
