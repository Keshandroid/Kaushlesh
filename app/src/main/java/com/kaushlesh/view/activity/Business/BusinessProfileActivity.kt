package com.kaushlesh.view.activity.Business

import android.Manifest
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.widget.*
import androidx.annotation.IdRes
import androidx.annotation.Nullable
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.GsonBuilder
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.kaushlesh.Controller.*
import com.kaushlesh.R
import com.kaushlesh.bean.BusinessUserProfileBean
import com.kaushlesh.bean.LocationListBeans
import com.kaushlesh.bean.UserProfileBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.*
import com.kaushlesh.widgets.CustomButton
import com.kaushlesh.widgets.CustomEditText
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.activity_business_profile.*
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_set_location.*
import kotlinx.android.synthetic.main.activity_view_business_profile.*
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import java.util.Calendar.*


class BusinessProfileActivity : AppCompatActivity(), View.OnClickListener, ParseControllerListener {

    val TAG = "BusinessProfileActivity"

    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView

    lateinit var rlInfo: RelativeLayout
    internal lateinit var dialog: Dialog
    lateinit var rlInfoTiming: RelativeLayout

    private var rejected_reason: String=""
    private var isAddProfile: Boolean= false

    //front/back images
    private val REQUEST_CAMERA = 0
    private val GALLERY = 1
    private var mImageBitmap: Bitmap? = null
    private var mCurrentPhotoPath: String? = null
    private var imageType: String=""
    private var isProfileAdded: String=""



    //select Days

    //For Business
    var selectedLanguageBusiness: BooleanArray? = null
    var langListBusiness: ArrayList<Int> = ArrayList()

    //For Whatsapp
    var selectedLanguageWp: BooleanArray? = null
    var langListWp: ArrayList<Int> = ArrayList()

    var langArray = arrayOf(
            "Monday", "Tuesday", "Wednesday", "Thursday",
            "Friday", "Saturday", "Sunday"
    )

    var userModel: UserProfileBean = UserProfileBean()
    lateinit var storeusedata: StoreUserData

    //location list
    lateinit var getListingController: GetListingController
    internal lateinit var placesClient: PlacesClient
    internal lateinit var type_spinner_type: ArrayAdapter<String>
    internal var typelist: MutableList<String> = java.util.ArrayList()
    internal var typeidlist: MutableList<String> = java.util.ArrayList()
    internal var locationlist: ArrayList<LocationListBeans.LocationList> = ArrayList()
    var locationid: String? = ""
    var district: String? = null
    lateinit var storeUserData: StoreUserData

    var whatsappInquiryAllow: Boolean = true
    var sameWhatsappNo: Boolean = false
    var whatsappTnquiryTimingSame = false

    //api and controllers
    private lateinit var addBusinessProfileController: AddBusinessProfileController
    private lateinit var editBusinessProfileController: EditBusinessProfileController
    private lateinit var editBusinessTimingsController: EditBusinessTimingsController



    //call timeing lists
    var businessCallList: ArrayList<CallTiming> = ArrayList()
    var wpCallList: ArrayList<CallTiming> = ArrayList()

    //check and set front/back images

    lateinit var initialFrontBitmap: Bitmap
    lateinit var initialBackBitmap: Bitmap

    var isNoImageFront: Boolean =false
    var isNoImageBack: Boolean =false

    var isFrontImageSet: Boolean =false
    var isBackImageSet: Boolean =false

    var changedFrontImg: Boolean =false
    var changedBackImg: Boolean =false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_business_profile)

        findViews()

        storeUserData = StoreUserData(this)
        storeusedata = StoreUserData(applicationContext)
        userModel.userId = storeusedata.getString(Constants.USER_ID)
        userModel.userToken = storeusedata.getString(Constants.TOKEN)


        addBusinessProfileController = AddBusinessProfileController(this, this)
        editBusinessProfileController = EditBusinessProfileController(this, this)
        editBusinessTimingsController = EditBusinessTimingsController(this, this)


        radioGrpWpInquiry.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup, @IdRes checkedId: Int) {
                val radioButton: RadioButton = group.findViewById(checkedId) as RadioButton
                val rbSelected = radioButton.getText().toString();
                if (rbSelected == "Yes") {
                    whatsappInquiryAllow = true
                    llWhatsappInquiryMain.visibility = View.VISIBLE
                } else {
                    whatsappInquiryAllow = false
                    llWhatsappInquiryMain.visibility = View.GONE
                }
            }
        })

        radioGrpWhatsappNumber.setOnCheckedChangeListener(object :
                RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup, @IdRes checkedId: Int) {
                val radioButton: RadioButton = group.findViewById(checkedId) as RadioButton
                val rbSelected = radioButton.getText().toString();
                if (rbSelected == "Yes") {
                    sameWhatsappNo = true
                    llWhatsappNumber.visibility = View.GONE
                } else {
                    sameWhatsappNo = false
                    llWhatsappNumber.visibility = View.VISIBLE
                }
            }
        })

        //Set whatsapp inquiry timing radio buttons

        /*radioGrpInquiryTiming.setOnCheckedChangeListener(object :
                RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup, @IdRes checkedId: Int) {
                val radioButton: RadioButton = group.findViewById(checkedId) as RadioButton
                val rbSelected = radioButton.getText().toString();
                if (rbSelected == "Yes") {
                    whatsappTnquiryTimingSame = true
                    llWhatsappInquiryTiming.visibility = View.GONE
                } else {
                    whatsappTnquiryTimingSame = false
                    llWhatsappInquiryTiming.visibility = View.VISIBLE
                }
            }
        })*/

        edtOpenTime.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                // TODO Auto-generated method stub
                val mcurrentTime: Calendar = Calendar.getInstance()
                val hour: Int = mcurrentTime.get(Calendar.HOUR_OF_DAY)
                val minute: Int = mcurrentTime.get(Calendar.MINUTE)
                val mTimePicker: TimePickerDialog
                mTimePicker = TimePickerDialog(
                        this@BusinessProfileActivity,
                        object : TimePickerDialog.OnTimeSetListener {
                            override fun onTimeSet(
                                    timePicker: TimePicker?,
                                    selectedHour: Int,
                                    selectedMinute: Int
                            ) {

                                mcurrentTime.set(HOUR_OF_DAY, selectedHour)
                                mcurrentTime.set(MINUTE, selectedMinute)
                                val mSDF = SimpleDateFormat("HH:mm")
                                val time = mSDF.format(mcurrentTime.getTime())

                                edtOpenTime.setText("" + time)

                                if (!isAddProfile) {
                                    showCallAllowDialog()
                                }

                            }
                        },
                        hour,
                        minute,
                        true
                ) //Yes 24 hour time
                mTimePicker.setTitle("Select Time")
                mTimePicker.setCanceledOnTouchOutside(true)
                mTimePicker.show()
            }
        })

        edtCloseTime.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                // TODO Auto-generated method stub
                val mcurrentTime: Calendar = Calendar.getInstance()
                val hour: Int = mcurrentTime.get(Calendar.HOUR_OF_DAY)
                val minute: Int = mcurrentTime.get(Calendar.MINUTE)
                val mTimePicker: TimePickerDialog
                mTimePicker = TimePickerDialog(
                        this@BusinessProfileActivity,
                        object : TimePickerDialog.OnTimeSetListener {
                            override fun onTimeSet(
                                    timePicker: TimePicker?,
                                    selectedHour: Int,
                                    selectedMinute: Int
                            ) {

                                mcurrentTime.set(HOUR_OF_DAY, selectedHour)
                                mcurrentTime.set(MINUTE, selectedMinute)
                                val mSDF = SimpleDateFormat("HH:mm")
                                val time = mSDF.format(mcurrentTime.getTime())

                                edtCloseTime.setText("" + time)
                                if (!isAddProfile) {
                                    showCallAllowDialog()
                                }
                            }
                        },
                        hour,
                        minute,
                        true
                ) //Yes 24 hour time
                mTimePicker.setTitle("Select Time")
                mTimePicker.setCanceledOnTouchOutside(true)
                mTimePicker.show()
            }
        })

        /*businessSwitch.setOnCheckedChangeListener(
            object : CompoundButton.OnCheckedChangeListener {
                override fun onCheckedChanged(compoundButton: CompoundButton?, b: Boolean) {
                    *//*Utils.showToast(this@BusinessProfileActivity,"Switch is " +
                            if (businessSwitch.isChecked()) "On" else "Off")*//*
                    showCallAllowDialog()

                }
            })*/

        businessSwitch.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                if (!isAddProfile) {
                    showCallAllowDialog()
                }
            }

        })


        /*whatsappSwitch.setOnCheckedChangeListener(
                object : CompoundButton.OnCheckedChangeListener {
                    override fun onCheckedChanged(compoundButton: CompoundButton?, b: Boolean) {
                        *//*Utils.showToast(this@BusinessProfileActivity,"Switch is " +
                                if (whatsappSwitch.isChecked()) "On" else "Off")*//*
                        showCallAllowDialog()

                    }
                })*/

        whatsappSwitch.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                if (!isAddProfile) {
                    showCallAllowDialogWhatsapp()
                }
            }

        })

        edtOpenTimeWp.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                // TODO Auto-generated method stub
                val mcurrentTime: Calendar = Calendar.getInstance()
                val hour: Int = mcurrentTime.get(Calendar.HOUR_OF_DAY)
                val minute: Int = mcurrentTime.get(Calendar.MINUTE)
                val mTimePicker: TimePickerDialog
                mTimePicker = TimePickerDialog(
                        this@BusinessProfileActivity,
                        object : TimePickerDialog.OnTimeSetListener {
                            override fun onTimeSet(
                                    timePicker: TimePicker?,
                                    selectedHour: Int,
                                    selectedMinute: Int
                            ) {

                                mcurrentTime.set(HOUR_OF_DAY, selectedHour)
                                mcurrentTime.set(MINUTE, selectedMinute)
                                val mSDF = SimpleDateFormat("HH:mm")
                                val time = mSDF.format(mcurrentTime.getTime())

                                edtOpenTimeWp.setText("" + time)
                                if (!isAddProfile) {
                                    showCallAllowDialogWhatsapp()
                                }
                            }
                        },
                        hour,
                        minute,
                        true
                ) //Yes 24 hour time
                mTimePicker.setTitle("Select Time")
                mTimePicker.setCanceledOnTouchOutside(true)
                mTimePicker.show()
            }
        })

        edtCloseTimeWp.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                // TODO Auto-generated method stub
                val mcurrentTime: Calendar = Calendar.getInstance()
                val hour: Int = mcurrentTime.get(Calendar.HOUR_OF_DAY)
                val minute: Int = mcurrentTime.get(Calendar.MINUTE)
                val mTimePicker: TimePickerDialog
                mTimePicker = TimePickerDialog(
                        this@BusinessProfileActivity,
                        object : TimePickerDialog.OnTimeSetListener {
                            override fun onTimeSet(
                                    timePicker: TimePicker?,
                                    selectedHour: Int,
                                    selectedMinute: Int
                            ) {

                                mcurrentTime.set(HOUR_OF_DAY, selectedHour)
                                mcurrentTime.set(MINUTE, selectedMinute)
                                val mSDF = SimpleDateFormat("HH:mm")
                                val time = mSDF.format(mcurrentTime.getTime())

                                edtCloseTimeWp.setText("" + time)
                                if (!isAddProfile) {
                                    showCallAllowDialogWhatsapp()
                                }
                            }
                        },
                        hour,
                        minute,
                        true
                ) //Yes 24 hour time
                mTimePicker.setTitle("Select Time")
                mTimePicker.setCanceledOnTouchOutside(true)
                mTimePicker.show()
            }
        })

        edt_about_business.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                txt_about_business_counter.text = s.length.toString() + "/177"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })

        edt_about_business.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View, event: MotionEvent): Boolean {
                if (edt_about_business.hasFocus()) {
                    v.parent.requestDisallowInterceptTouchEvent(true)
                    when (event.action and MotionEvent.ACTION_MASK) {
                        MotionEvent.ACTION_SCROLL -> {
                            v.parent.requestDisallowInterceptTouchEvent(false)
                            return true
                        }
                    }
                }
                return false
            }
        })

        edt_about_service.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                txt_about_service.text = s.length.toString() + "/5000"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })

        edt_about_service.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View, event: MotionEvent): Boolean {
                if (edt_about_service.hasFocus()) {
                    v.parent.requestDisallowInterceptTouchEvent(true)
                    when (event.action and MotionEvent.ACTION_MASK) {
                        MotionEvent.ACTION_SCROLL -> {
                            v.parent.requestDisallowInterceptTouchEvent(false)
                            return true
                        }
                    }
                }
                return false
            }
        })


        edt_state_business.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
                if (p1!!.getAction() == MotionEvent.ACTION_UP) {
                    showPopupMenu(edt_state_business)
                    return true;
                }
                return false;
            }
        })

        //weekdays selector

        selectedLanguageBusiness = BooleanArray(langArray.size)
        selectedLanguageWp = BooleanArray(langArray.size)

        //init places api
        getListingController = GetListingController(this, this)
        initPlacesAPI()



        getUserBusinessProfile()


        imgFront.setOnClickListener(this)
        imgBack.setOnClickListener(this)
        rlSelectDay.setOnClickListener(this)
        rlSelectDayWp.setOnClickListener(this)
        btnSaveBusinessProfile.setOnClickListener(this)
        btnEditBusinessProfile.setOnClickListener(this)
        tv_dis_type_business.setOnClickListener(this)
        imgMoreBusiness.setOnClickListener(this)
        txtPendingVerifiation.setOnClickListener(this)
        txtRejectedProfile.setOnClickListener(this)

        //reset timing
        txtResetBusiness.setOnClickListener(this)
        txtResetWp.setOnClickListener(this)


    }

    private fun getUserBusinessProfile(){
        val businessProfileId = storeusedata.getString(Constants.BUSINESS_USER_ID)

        getListingController = GetListingController(this, this)
        getListingController.getUserBusinessProfile(businessProfileId)
    }

    private fun initPlacesAPI() {
        val apiKey = getString(R.string.google_place_api_key)
        if (apiKey.isEmpty()) {
            //responseView.setText(getString(R.string.error));
            return
        }

        // Setup Places Client
        if (!Places.isInitialized()) {
            Places.initialize(applicationContext, apiKey)
        }

        placesClient = Places.createClient(applicationContext)
        getListingController.getLocationList()

    }

    private fun findViews() {
        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)
        //tvtoolbartitle.text = resources.getString(R.string.txt_business_profile)
        rlInfo = findViewById(R.id.rlInfo)
        rlInfoTiming = findViewById(R.id.rlInfoTiming)

        btnback.setOnClickListener(this)
        rlInfo.setOnClickListener(this)
        rlInfoTiming.setOnClickListener(this)

    }

    private fun openInfoDialog() {

        dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_info_business)
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val ivclose = dialog.findViewById<ImageView>(R.id.ivclose)


        ivclose.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun openInfoWhatsappDialog() {

        dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_info_wp_business)
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val ivclose = dialog.findViewById<ImageView>(R.id.ivclose)

        ivclose.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun requestMultiplePermissions() {
        Dexter.withActivity(this)
            .withPermissions(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    // check if all permissions are granted
                    if (report.areAllPermissionsGranted()) {
                        Utils.showLog(TAG, "All permissions are granted")

                        showPictureDialog()
                    }

                    // check for permanent denial of any permission
                    if (report.isAnyPermissionPermanentlyDenied) {
                        // show alert dialog navigating to Settings
                        //openSettingsDialog();
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                        permissions: List<PermissionRequest>,
                        token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            }).withErrorListener {
                Log.i(TAG, "Some Error!") }
            .onSameThread()
            .check()
    }

    private fun showPictureDialog() {
        val pictureDialog =
            AlertDialog.Builder(this)
        pictureDialog.setTitle(getString(R.string.txt_select_action))
        val pictureDialogItems = arrayOf(
                getString(R.string.txt_image_from_gellary),
                getString(R.string.txt_imge_from_camera),
                getString(R.string.txt_remove_img)
        )
        pictureDialog.setItems(
                pictureDialogItems
        ) { dialog, which ->
            when (which) {
                0 -> choosePhotoFromGallary()
                1 -> takePhotoFromCamera()
                2 -> removeImageDrawable()
            }
        }
        pictureDialog.show()
    }

    fun removeImageDrawable(){
        if(imageType == "front"){

            isNoImageFront = true

            txtFront.setVisibility(View.VISIBLE)
            imgFront.setImageDrawable(null);

            /*Glide.with(applicationContext)
                    .load(R.drawable.bg_no_img)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(imgFront)*/
        }else{
            isNoImageBack = true

            txtBack.setVisibility(View.VISIBLE)
            imgBack.setImageDrawable(null);

            /*Glide.with(applicationContext)
                    .load(R.drawable.bg_no_img)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(imgBack)*/
        }
    }

    fun setNullImageFront(){
        txtFront.setVisibility(View.VISIBLE)
        imgFront.setImageDrawable(null);
        /*Glide.with(applicationContext)
                .load(R.drawable.bg_no_img)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(imgFront)


        Glide.with(applicationContext)
                .asBitmap().load(R.drawable.bg_no_img)
                .listener(object : RequestListener<Bitmap?> {
                    override fun onLoadFailed(@Nullable e: GlideException?, o: Any, target: Target<Bitmap?>, b: Boolean): Boolean {
                        return false
                    }

                    override fun onResourceReady(bitmap: Bitmap?, o: Any, target: Target<Bitmap?>, dataSource: DataSource, b: Boolean): Boolean {

                        if (bitmap != null) {
                            initialFrontBitmap = bitmap
                        }
                        return false
                    }
                }
                ).submit()*/
    }

    fun setNullImageBack(){
        txtBack.setVisibility(View.VISIBLE)
        imgBack.setImageDrawable(null);
        /*Glide.with(applicationContext)
                .load(R.drawable.bg_no_img)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(imgBack)

        Glide.with(applicationContext)
                .asBitmap().load(R.drawable.bg_no_img)
                .listener(object : RequestListener<Bitmap?> {
                    override fun onLoadFailed(@Nullable e: GlideException?, o: Any, target: Target<Bitmap?>, b: Boolean): Boolean {
                        return false
                    }

                    override fun onResourceReady(bitmap: Bitmap?, o: Any, target: Target<Bitmap?>, dataSource: DataSource, b: Boolean): Boolean {

                        if (bitmap != null) {
                            initialBackBitmap = bitmap
                        }
                        return false
                    }
                }
                ).submit()*/
    }

    fun choosePhotoFromGallary() {
        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(galleryIntent, GALLERY)
    }

    private fun takePhotoFromCamera() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                Log.i(TAG, "IOException")
            }
            if (photoFile != null) {
                /*cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(cameraIntent, REQUEST_CAMERA);*/
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile))
                } else {
                    val file =
                        File(Uri.fromFile(photoFile).path)
                    val photoUri = FileProvider.getUriForFile(
                            applicationContext, getPackageName().toString() + ".provider",
                            file
                    )
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                }
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(intent, REQUEST_CAMERA)
                }
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File? {
        val timeStamp =
            SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir: File = applicationContext.getExternalFilesDir(
                Environment.DIRECTORY_PICTURES
        )!!
        val image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        )
        mCurrentPhotoPath = "file:" + image.absolutePath
        return image
    }

    private fun startCropImageActivity(imageUri: Uri) {
        CropImage.activity(imageUri)
            .setGuidelines(CropImageView.Guidelines.ON)
            .setMultiTouchEnabled(true)
            .setOutputCompressQuality(50)
            .start(this)
    }

    fun getImageUri(inContext: Context, inImage: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val path = MediaStore.Images.Media.insertImage(
                inContext.contentResolver,
                inImage,
                "" + System.currentTimeMillis(),
                null
        )
        return Uri.parse(path)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_back -> {
                onBackPressed()
            }
            R.id.rlInfo -> {
                openInfoDialog()
            }
            R.id.rlInfoTiming -> {
                openInfoWhatsappDialog()
            }


            R.id.imgFront -> {
                imageType = "front"
                requestMultiplePermissions()
            }
            R.id.imgBack -> {
                imageType = "back"
                requestMultiplePermissions()
            }
            R.id.rlSelectDay -> {
                showDialogForDaysBusiness()
            }
            R.id.rlSelectDayWp -> {
                showDialogForDaysWp()
            }

            R.id.tv_dis_type_business -> {
                spinner_district_business.visibility = View.VISIBLE
                spinner_district_business.performClick()
                tv_dis_type_business.visibility = View.GONE

                spinner_district_business.onItemSelectedListener =
                        object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(
                                    adapterView: AdapterView<*>,
                                    view: View,
                                    position: Int,
                                    l: Long
                            ) {

                                if (position > 0) {
                                    AppConstants.printLog(
                                            TAG,
                                            "onItemSelected: ===" + adapterView.selectedItem
                                    )
                                    AppConstants.printLog(
                                            TAG,
                                            "onItemSelected: ===" + typeidlist.get(position)
                                    )
                                    locationid = typeidlist.get(position)
                                    district = adapterView.selectedItem.toString()

                                    storeUserData.setString(Constants.LOC_ID_PKG, locationid.toString())
                                    storeUserData.setString(
                                            Constants.LOCATION_ID,
                                            locationid.toString()
                                    )
                                    storeUserData.setString(
                                            Constants.LOC_NAME,
                                            adapterView.selectedItem.toString()
                                    )
                                    Utils.showLog(
                                            TAG,
                                            "====stored location id====" + locationid.toString()
                                    )

                                    rl_district_business.setBackgroundResource(R.drawable.bg_edittext_black)

                                    //getListingController.getLatLongOfSelectedStrict(district + "," + resources.getString(R.string.txt_gujarat))


                                    /*if (act_location.text.toString().length > 0) {
                                act_location.setText("")
                            }*/
                                } else {
                                    locationid = "0"
                                }
                            }

                            override fun onNothingSelected(adapterView: AdapterView<*>) {
                                locationid = "0"
                            }
                        }
            }

            R.id.btnSaveBusinessProfile -> {

                validateData("AddProfile")

            }

            R.id.btnEditBusinessProfile -> {
                validateData("EditProfile")
            }


            R.id.imgMoreBusiness -> {
                showBottomsheetMoreOptions(this)
            }

            R.id.txtPendingVerifiation -> {
                showPendingVerificationDialog();
            }

            R.id.txtRejectedProfile -> {
                showRejectedProfileDialog();

            }

            R.id.txtResetBusiness -> {
                editBusinessTimings()
            }

            R.id.txtResetWp -> {
                editWhatsappTimings()
            }

        }
    }

    private fun editBusinessTimings() {

        if(edtOpenTime.text.toString() == ""){
            Utils.showToast(this, "Open Time is required")
            return
        }

        if(edtCloseTime.text.toString() == ""){
            Utils.showToast(this, "Close Time is required")
            return
        }

        if(selectedDaysMobile.text.toString() == ""){
            Utils.showToast(this, "Please set days for business call")
            return
        }

        val businessProfileData = BusinessProfileData()

        businessProfileData.userId = userModel.userId
        businessProfileData.userToken = userModel.userToken.toString()

        //Business Call Timing

        var selectedDaysMobileString = selectedDaysMobile.text.toString()
        Log.d(TAG, "$selectedDaysMobileString")

        val callTiming = CallTiming()
        businessCallList.clear()

        if(selectedDaysMobileString.contains("All")){
            callTiming.dayId = 8
            callTiming.openTime = edtOpenTime.text.toString()
            callTiming.closeTime = edtCloseTime.text.toString()
            businessCallList.add(callTiming)
            businessProfileData.bussiness_call_timing = businessCallList
        }else if(selectedDaysMobileString.contains("No Calls")){
            callTiming.dayId = 9
            callTiming.openTime = edtOpenTime.text.toString()
            callTiming.closeTime = edtCloseTime.text.toString()
            businessCallList.add(callTiming)
            businessProfileData.bussiness_call_timing = businessCallList
        }else{
            try {
                val str = selectedDaysMobileString.filter { !it.isWhitespace() }
                val daysArray = str.split(',');
                Log.d(TAG, "$daysArray")

                for (i in 0 until daysArray.size) {
                    val timings = CallTiming()
                    Log.d(TAG, "${daysArray.get(i)}")

                    if(daysArray.get(i) == "Monday"){
                        timings.dayId = 1
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }else if(daysArray.get(i) == "Tuesday"){
                        timings.dayId = 2
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }else if(daysArray.get(i) == "Wednesday"){
                        timings.dayId = 3
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }else if(daysArray.get(i) == "Thursday"){
                        timings.dayId = 4
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }else if(daysArray.get(i) == "Friday"){
                        timings.dayId = 5
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }else if(daysArray.get(i) == "Saturday"){
                        timings.dayId = 6
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }else if(daysArray.get(i) == "Sunday"){
                        timings.dayId = 7
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }
                }
                businessProfileData.bussiness_call_timing = businessCallList
            }catch (e: Exception){
                e.printStackTrace()
            }
        }

        //whatsapp call timing

        var selectedDaysWpString = selectedDaysWp.text.toString()
        Log.d(TAG, "$selectedDaysWpString")

        val callTimingWp = CallTiming()
        //val businessCallArrayWp = JSONArray()
        wpCallList.clear()

        if(selectedDaysWpString.contains("All")){
            callTimingWp.dayId = 8
            callTimingWp.openTime = edtOpenTimeWp.text.toString()
            callTimingWp.closeTime = edtCloseTimeWp.text.toString()
            wpCallList.add(callTimingWp)
            businessProfileData.whatsapp_inquiry_call_timing = wpCallList
        }else if(selectedDaysWpString.contains("No Calls")){
            callTimingWp.dayId = 9
            callTimingWp.openTime = edtOpenTimeWp.text.toString()
            callTimingWp.closeTime = edtCloseTimeWp.text.toString()
            wpCallList.add(callTimingWp)
            businessProfileData.whatsapp_inquiry_call_timing = wpCallList
        }else{
            try {
                val str = selectedDaysWpString.filter { !it.isWhitespace() }
                val daysArray = str.split(',');
                Log.d(TAG, "$daysArray")

                for (i in 0 until daysArray.size) {
                    val timings = CallTiming()
                    Log.d(TAG, "${daysArray.get(i)}")

                    if(daysArray.get(i) == "Monday"){
                        timings.dayId = 1
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }else if(daysArray.get(i) == "Tuesday"){
                        timings.dayId = 2
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }else if(daysArray.get(i) == "Wednesday"){
                        timings.dayId = 3
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }else if(daysArray.get(i) == "Thursday"){
                        timings.dayId = 4
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }else if(daysArray.get(i) == "Friday"){
                        timings.dayId = 5
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }else if(daysArray.get(i) == "Saturday"){
                        timings.dayId = 6
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }else if(daysArray.get(i) == "Sunday"){
                        timings.dayId = 7
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }
                }
                businessProfileData.whatsapp_inquiry_call_timing = wpCallList
            }catch (e: Exception){
                e.printStackTrace()
            }
        }

        //business call switch
        if(businessSwitch.isChecked){
            businessProfileData.is_calls_allow = 1
        }else{
            businessProfileData.is_calls_allow = 0
        }

        //whatsapp call switch
        if(whatsappSwitch.isChecked){
            businessProfileData.is_whatsapp_calls_allow = 1
        }else{
            businessProfileData.is_whatsapp_calls_allow = 0
        }


        editBusinessTimingsController.editBusinessTimings(
                businessProfileData,
                userModel,
                "businessCall"
        )

    }

    private fun editWhatsappTimings() {

        if(edtOpenTimeWp.text.toString() == ""){
            Utils.showToast(this, "Please enter whatsapp inquiry open time")
            return
        }

        if(edtCloseTimeWp.text.toString() == ""){
            Utils.showToast(this, "Please enter whatsapp inquiry close time")
            return
        }

        if(selectedDaysWp.text.toString() == ""){
            Utils.showToast(this, "set day is required for whatsapp inquiry timing")
            return
        }

        val businessProfileData = BusinessProfileData()

        businessProfileData.userId = userModel.userId
        businessProfileData.userToken = userModel.userToken.toString()

        //Business Call Timing

        var selectedDaysMobileString = selectedDaysMobile.text.toString()
        Log.d(TAG, "$selectedDaysMobileString")

        val callTiming = CallTiming()
        businessCallList.clear()

        if(selectedDaysMobileString.contains("All")){
            callTiming.dayId = 8
            callTiming.openTime = edtOpenTime.text.toString()
            callTiming.closeTime = edtCloseTime.text.toString()
            businessCallList.add(callTiming)
            businessProfileData.bussiness_call_timing = businessCallList
        }else if(selectedDaysMobileString.contains("No Calls")){
            callTiming.dayId = 9
            callTiming.openTime = edtOpenTime.text.toString()
            callTiming.closeTime = edtCloseTime.text.toString()
            businessCallList.add(callTiming)
            businessProfileData.bussiness_call_timing = businessCallList
        }else{
            try {
                val str = selectedDaysMobileString.filter { !it.isWhitespace() }
                val daysArray = str.split(',');
                Log.d(TAG, "$daysArray")

                for (i in 0 until daysArray.size) {
                    val timings = CallTiming()
                    Log.d(TAG, "${daysArray.get(i)}")

                    if(daysArray.get(i) == "Monday"){
                        timings.dayId = 1
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }else if(daysArray.get(i) == "Tuesday"){
                        timings.dayId = 2
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }else if(daysArray.get(i) == "Wednesday"){
                        timings.dayId = 3
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }else if(daysArray.get(i) == "Thursday"){
                        timings.dayId = 4
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }else if(daysArray.get(i) == "Friday"){
                        timings.dayId = 5
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }else if(daysArray.get(i) == "Saturday"){
                        timings.dayId = 6
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }else if(daysArray.get(i) == "Sunday"){
                        timings.dayId = 7
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }
                }
                businessProfileData.bussiness_call_timing = businessCallList
            }catch (e: Exception){
                e.printStackTrace()
            }
        }

        //whatsapp call timing

        var selectedDaysWpString = selectedDaysWp.text.toString()
        Log.d(TAG, "$selectedDaysWpString")

        val callTimingWp = CallTiming()
        //val businessCallArrayWp = JSONArray()
        wpCallList.clear()

        if(selectedDaysWpString.contains("All")){
            callTimingWp.dayId = 8
            callTimingWp.openTime = edtOpenTimeWp.text.toString()
            callTimingWp.closeTime = edtCloseTimeWp.text.toString()
            wpCallList.add(callTimingWp)
            businessProfileData.whatsapp_inquiry_call_timing = wpCallList
        }else if(selectedDaysWpString.contains("No Calls")){
            callTimingWp.dayId = 9
            callTimingWp.openTime = edtOpenTimeWp.text.toString()
            callTimingWp.closeTime = edtCloseTimeWp.text.toString()
            wpCallList.add(callTimingWp)
            businessProfileData.whatsapp_inquiry_call_timing = wpCallList
        }else{
            try {
                val str = selectedDaysWpString.filter { !it.isWhitespace() }
                val daysArray = str.split(',');
                Log.d(TAG, "$daysArray")

                for (i in 0 until daysArray.size) {
                    val timings = CallTiming()
                    Log.d(TAG, "${daysArray.get(i)}")

                    if(daysArray.get(i) == "Monday"){
                        timings.dayId = 1
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }else if(daysArray.get(i) == "Tuesday"){
                        timings.dayId = 2
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }else if(daysArray.get(i) == "Wednesday"){
                        timings.dayId = 3
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }else if(daysArray.get(i) == "Thursday"){
                        timings.dayId = 4
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }else if(daysArray.get(i) == "Friday"){
                        timings.dayId = 5
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }else if(daysArray.get(i) == "Saturday"){
                        timings.dayId = 6
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }else if(daysArray.get(i) == "Sunday"){
                        timings.dayId = 7
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }
                }
                businessProfileData.whatsapp_inquiry_call_timing = wpCallList
            }catch (e: Exception){
                e.printStackTrace()
            }
        }

        //business call switch
        if(businessSwitch.isChecked){
            businessProfileData.is_calls_allow = 1
        }else{
            businessProfileData.is_calls_allow = 0
        }

        //whatsapp call switch
        if(whatsappSwitch.isChecked){
            businessProfileData.is_whatsapp_calls_allow = 1
        }else{
            businessProfileData.is_whatsapp_calls_allow = 0
        }

        editBusinessTimingsController.editBusinessTimings(
                businessProfileData,
                userModel,
                "whatsappCall"
        )

    }

    private fun showRejectedProfileDialog(){
        dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_rejected_profile)
        dialog.setTitle("")
        dialog.setCanceledOnTouchOutside(true)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val btnyes = dialog.findViewById(R.id.btn_yes) as CustomButton
        val reasonText = dialog.findViewById(R.id.reasonText) as TextView

        reasonText.setText("Reason of rejection : " + rejected_reason)

        btnyes.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
        })

        dialog.show()
    }

    private fun showCallAllowDialog(){
        dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_call_allow)
        dialog.setTitle("")
        dialog.setCanceledOnTouchOutside(true)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val btnyes = dialog.findViewById(R.id.btn_yes) as CustomButton


        btnyes.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
        })

        dialog.show()
    }

    private fun showCallAllowDialogWhatsapp(){
        dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_call_allow_whatsapp)
        dialog.setTitle("")
        dialog.setCanceledOnTouchOutside(true)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val btnyes = dialog.findViewById(R.id.btn_yes) as CustomButton


        btnyes.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
        })

        dialog.show()
    }

    private fun showEditProfileDialog(){
        dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_edit_profile)
        dialog.setTitle("")
        dialog.setCanceledOnTouchOutside(true)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val btn_not_now = dialog.findViewById(R.id.btn_not_now) as CustomButton
        val btn_edit = dialog.findViewById(R.id.btn_edit) as CustomButton

        btn_edit.setOnClickListener(View.OnClickListener {
            enableAllViews()
            btnSaveBusinessProfile.visibility = View.GONE
            txtRejectedProfile.visibility = View.GONE
            btnEditBusinessProfile.visibility = View.VISIBLE

            dialog.dismiss()
        })

        btn_not_now.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            finish()
        })

        dialog.show()
    }

    private fun showDeleteProfileDialog(){
        dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_delete_profile)
        dialog.setTitle("")
        dialog.setCanceledOnTouchOutside(true)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))


        val btn_no = dialog.findViewById(R.id.btn_no) as CustomButton
        val btn_yes = dialog.findViewById(R.id.btn_yes) as CustomButton


        btn_yes.setOnClickListener(View.OnClickListener {
            val deleteController = DeleteBusinessProfileController(
                    this,
                    this@BusinessProfileActivity
            )
            deleteController.onClick(btn_yes)
            dialog.dismiss()
        })

        btn_no.setOnClickListener(View.OnClickListener {

            dialog.dismiss()
        })

        dialog.show()
    }


    private fun showPendingVerificationDialog(){

        dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_pending_verification)
        dialog.setTitle("")
        dialog.setCanceledOnTouchOutside(true)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val btnyes = dialog.findViewById(R.id.btn_yes) as CustomButton

        btnyes.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
        })

        dialog.show()
    }

    private fun showPopupMenu(edt_state_business: CustomEditText) {
        val popup = PopupMenu(this, edt_state_business)
        popup.inflate(R.menu.state_popup)

        popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item: MenuItem? ->

            when (item!!.itemId) {
                R.id.state -> {
                    //Toast.makeText(this@BusinessProfileActivity, item.title, Toast.LENGTH_SHORT).show()
                }

            }
            true
        })

        popup.show()
    }

    private fun showBottomsheetMoreOptions(businessProfileActivity: BusinessProfileActivity) {
        val bottomSheetDialog = BottomSheetDialog(this)
        bottomSheetDialog.setContentView(R.layout.bottom_sheet_business_profile)


        val editProfile: LinearLayout? = bottomSheetDialog.findViewById(R.id.llEditProfile)
        val deleteProfile: LinearLayout? = bottomSheetDialog.findViewById(R.id.llDeleteProfile)

        editProfile!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {

                showEditProfileDialog()

                bottomSheetDialog.dismiss()

            }

        })

        deleteProfile!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {

                showDeleteProfileDialog()

                bottomSheetDialog.dismiss()
            }

        })

        bottomSheetDialog.show()
    }

    private fun validateData(profileStatus: String){
        if(edt_business_name.text.toString() == ""){
            Utils.showToast(this, "Business Name is required")
            return
        }

        if(edt_about_business.text.toString() == ""){
            Utils.showToast(this, "About business is required")
            return
        }

        if(edt_about_service.text.toString() == ""){
            Utils.showToast(this, "Service information is required")
            return
        }

        if(etphoneContact.text.toString() == ""){
            Utils.showToast(this, "Mobile no is required")
            return
        }

        if(etphoneContact.text.toString().length != 10){
            Utils.showToast(this, "Please enter valid mobile no")
            return
        }

        if(edtOpenTime.text.toString() == ""){
            Utils.showToast(this, "Open Time is required")
            return
        }

        if(edtCloseTime.text.toString() == ""){
            Utils.showToast(this, "Close Time is required")
            return
        }

        if(selectedDaysMobile.text.toString() == ""){
            Utils.showToast(this, "Please set days for business call")
            return
        }



        //validation for whatsapp inquiry
        if(whatsappInquiryAllow){
            if(!sameWhatsappNo){
                if(edtWhatsappNo.text.toString() == ""){
                    Utils.showToast(this, "Please enter whatsapp no")
                    return
                }

                if(edtWhatsappNo.text.toString().length != 10){
                    Utils.showToast(this, "Please enter valid whatsapp no")
                    return
                }

            }


            //Set whatsapp inquiry timing radio buttons
            /*if(!whatsappTnquiryTimingSame){
                if(edtOpenTimeWp.text.toString() == ""){
                    Utils.showToast(this, "Please enter whatsapp inquiry open time")
                    return
                }

                if(edtCloseTimeWp.text.toString() == ""){
                    Utils.showToast(this, "Please enter whatsapp inquiry close time")
                    return
                }

                if(selectedDaysWp.text.toString() == ""){
                    Utils.showToast(this, "set day is required for whatsapp inquiry timing")
                    return
                }

            }*/

            if(edtOpenTimeWp.text.toString() == ""){
                Utils.showToast(this, "Please enter whatsapp inquiry open time")
                return
            }

            if(edtCloseTimeWp.text.toString() == ""){
                Utils.showToast(this, "Please enter whatsapp inquiry close time")
                return
            }

            if(selectedDaysWp.text.toString() == ""){
                Utils.showToast(this, "set day is required for whatsapp inquiry timing")
                return
            }

        }


        if(edt_email_business.text.toString() == ""){
            Utils.showToast(this, "Please enter email")
            return
        }

        if(!Utils.isValidEmail(edt_email_business.text.toString().trim())){
            Utils.showToast(this, "Email is not valid")
            return
        }

        if(edt_address_line_one.text.toString() == ""){
            Utils.showToast(this, "Please enter Address 1")
            return
        }

        if(edt_address_line_two.text.toString() == ""){
            Utils.showToast(this, "Please enter Address 2")
            return
        }

        if(edt_village_town.text.toString() == ""){
            Utils.showToast(this, "Please enter city")
            return
        }

        if(locationid == "0" || locationid == ""){
            Utils.showToast(this, "Please select district")
            return
        }

        if(edt_state_business.text.toString() == ""){
            Utils.showToast(this, "Please enter state")
            return
        }


        if(profileStatus == "AddProfile"){
            addBusinessProfile()
        }else {
            editBusinessProfile()
        }


    }

    private fun addBusinessProfile() {
        val businessProfileData = BusinessProfileData()
        businessProfileData.location_id = locationid
        businessProfileData.userId = userModel.userId
        businessProfileData.userToken = userModel.userToken.toString()
        businessProfileData.bussiness_name = edt_business_name.text.toString()
        businessProfileData.bussiness_about = edt_about_business.text.toString()
        businessProfileData.service_info = edt_about_service.text.toString()

        businessProfileData.mobile_no = etphoneContact.text.toString()
        if(whatsappInquiryAllow){
            businessProfileData.whatsapp_inquiry_allow = 1
        }else{
            businessProfileData.whatsapp_inquiry_allow = 0
        }

        if(sameWhatsappNo){
            businessProfileData.whatsapp_no = etphoneContact.text.toString()
            businessProfileData.same_whatsapp_no = 1
        }else{
            businessProfileData.whatsapp_no = edtWhatsappNo.text.toString()
            businessProfileData.same_whatsapp_no = 0
        }

        businessProfileData.whatsapp_no = edtWhatsappNo.text.toString()

        //Set whatsapp inquiry timing radio buttons
        /*if(whatsappTnquiryTimingSame){
            businessProfileData.whatsapp_inquiry_timing_same = 1
        }else{
            businessProfileData.whatsapp_inquiry_timing_same = 0
        }*/

        businessProfileData.whatsapp_inquiry_timing_same = 0


        businessProfileData.email = edt_email_business.text.toString().trim()
        businessProfileData.address1 = edt_address_line_one.text.toString()
        businessProfileData.address2 = edt_address_line_two.text.toString()
        businessProfileData.city = edt_village_town.text.toString()
        businessProfileData.state = edt_state_business.text.toString()



        businessProfileData.visitingcard_front = imgFront.drawable
        businessProfileData.visitingcard_back = imgBack.drawable

        /*if(imgFront.drawable!=null){
            Log.d("drawableData","Front Not Null")
            businessProfileData.visitingcard_front = imgFront.drawable
        }else{
            Log.d("drawableData","Front Null")
            businessProfileData.visitingcard_front = resources.getDrawable(R.drawable.bg_no_img)
        }

        if(imgBack.drawable!=null){
            Log.d("drawableData","Back Not Null")
            businessProfileData.visitingcard_back = imgBack.drawable
        }else{
            Log.d("drawableData","Back Null")
            businessProfileData.visitingcard_back = resources.getDrawable(R.drawable.bg_no_img)
        }*/



        //Business Call Timing

        var selectedDaysMobileString = selectedDaysMobile.text.toString()
        Log.d(TAG, "$selectedDaysMobileString")

        val callTiming = CallTiming()
        //val businessCallArray = JSONArray()
        businessCallList.clear()

        if(selectedDaysMobileString.contains("All")){
            callTiming.dayId = 8
            callTiming.openTime = edtOpenTime.text.toString()
            callTiming.closeTime = edtCloseTime.text.toString()
            businessCallList.add(callTiming)
            businessProfileData.bussiness_call_timing = businessCallList
        }else if(selectedDaysMobileString.contains("No Calls")){
            callTiming.dayId = 9
            callTiming.openTime = edtOpenTime.text.toString()
            callTiming.closeTime = edtCloseTime.text.toString()
            businessCallList.add(callTiming)
            businessProfileData.bussiness_call_timing = businessCallList
        }else{
            try {
                val str = selectedDaysMobileString.filter { !it.isWhitespace() }
                val daysArray = str.split(',');
                Log.d(TAG, "$daysArray")

                for (i in 0 until daysArray.size) {
                    val timings = CallTiming()
                    Log.d(TAG, "${daysArray.get(i)}")

                    if(daysArray.get(i) == "Monday"){
                        timings.dayId = 1
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }else if(daysArray.get(i) == "Tuesday"){
                        timings.dayId = 2
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }else if(daysArray.get(i) == "Wednesday"){
                        timings.dayId = 3
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }else if(daysArray.get(i) == "Thursday"){
                        timings.dayId = 4
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }else if(daysArray.get(i) == "Friday"){
                        timings.dayId = 5
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }else if(daysArray.get(i) == "Saturday"){
                        timings.dayId = 6
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }else if(daysArray.get(i) == "Sunday"){
                        timings.dayId = 7
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }
                }
                businessProfileData.bussiness_call_timing = businessCallList
            }catch (e: Exception){
                e.printStackTrace()
            }
        }

        //whatsapp call timing

        var selectedDaysWpString = selectedDaysWp.text.toString()
        Log.d(TAG, "$selectedDaysWpString")

        val callTimingWp = CallTiming()
        //val businessCallArrayWp = JSONArray()
        wpCallList.clear()

        if(selectedDaysWpString.contains("All")){
            callTimingWp.dayId = 8
            callTimingWp.openTime = edtOpenTimeWp.text.toString()
            callTimingWp.closeTime = edtCloseTimeWp.text.toString()
            wpCallList.add(callTimingWp)
            businessProfileData.whatsapp_inquiry_call_timing = wpCallList
        }else if(selectedDaysWpString.contains("No Calls")){
            callTimingWp.dayId = 9
            callTimingWp.openTime = edtOpenTimeWp.text.toString()
            callTimingWp.closeTime = edtCloseTimeWp.text.toString()
            wpCallList.add(callTimingWp)
            businessProfileData.whatsapp_inquiry_call_timing = wpCallList
        }else{
            try {
                val str = selectedDaysWpString.filter { !it.isWhitespace() }
                val daysArray = str.split(',');
                Log.d(TAG, "$daysArray")

                for (i in 0 until daysArray.size) {
                    val timings = CallTiming()
                    Log.d(TAG, "${daysArray.get(i)}")

                    if(daysArray.get(i) == "Monday"){
                        timings.dayId = 1
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }else if(daysArray.get(i) == "Tuesday"){
                        timings.dayId = 2
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }else if(daysArray.get(i) == "Wednesday"){
                        timings.dayId = 3
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }else if(daysArray.get(i) == "Thursday"){
                        timings.dayId = 4
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }else if(daysArray.get(i) == "Friday"){
                        timings.dayId = 5
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }else if(daysArray.get(i) == "Saturday"){
                        timings.dayId = 6
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }else if(daysArray.get(i) == "Sunday"){
                        timings.dayId = 7
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }
                }
                businessProfileData.whatsapp_inquiry_call_timing = wpCallList
            }catch (e: Exception){
                e.printStackTrace()
            }
        }

        //business call switch
        if(businessSwitch.isChecked){
            businessProfileData.is_calls_allow = 1
        }else{
            businessProfileData.is_calls_allow = 0
        }

        //whatsapp call switch
        if(whatsappSwitch.isChecked){
            businessProfileData.is_whatsapp_calls_allow = 1
        }else{
            businessProfileData.is_whatsapp_calls_allow = 0
        }

        Log.d(TAG, "$businessProfileData")


        addBusinessProfileController.addBusinessProfile(businessProfileData, userModel)



    }

    private fun editBusinessProfile() {
        val businessProfileData = BusinessProfileData()

        businessProfileData.location_id = locationid
        businessProfileData.userId = userModel.userId
        businessProfileData.userToken = userModel.userToken.toString()
        businessProfileData.bussiness_name = edt_business_name.text.toString()
        businessProfileData.bussiness_about = edt_about_business.text.toString()
        businessProfileData.service_info = edt_about_service.text.toString()

        businessProfileData.mobile_no = etphoneContact.text.toString()
        if(whatsappInquiryAllow){
            businessProfileData.whatsapp_inquiry_allow = 1
        }else{
            businessProfileData.whatsapp_inquiry_allow = 0
        }

        if(sameWhatsappNo){
            businessProfileData.whatsapp_no = etphoneContact.text.toString()
            businessProfileData.same_whatsapp_no = 1
        }else{
            businessProfileData.whatsapp_no = edtWhatsappNo.text.toString()
            businessProfileData.same_whatsapp_no = 0
        }


        //Set whatsapp inquiry timing radio buttons
        /*if(whatsappTnquiryTimingSame){
            businessProfileData.whatsapp_inquiry_timing_same = 1
        }else{
            businessProfileData.whatsapp_inquiry_timing_same = 0
        }*/
        businessProfileData.whatsapp_inquiry_timing_same = 0

        businessProfileData.email = edt_email_business.text.toString().trim()
        businessProfileData.address1 = edt_address_line_one.text.toString()
        businessProfileData.address2 = edt_address_line_two.text.toString()
        businessProfileData.city = edt_village_town.text.toString()
        businessProfileData.state = edt_state_business.text.toString()
        businessProfileData.visitingcard_front = imgFront.drawable
        businessProfileData.visitingcard_back = imgBack.drawable

        //Business Call Timing

        var selectedDaysMobileString = selectedDaysMobile.text.toString()
        Log.d(TAG, "$selectedDaysMobileString")

        val callTiming = CallTiming()
        //val businessCallArray = JSONArray()
        businessCallList.clear()

        if(selectedDaysMobileString.contains("All")){
            callTiming.dayId = 8
            callTiming.openTime = edtOpenTime.text.toString()
            callTiming.closeTime = edtCloseTime.text.toString()
            businessCallList.add(callTiming)
            businessProfileData.bussiness_call_timing = businessCallList
        }else if(selectedDaysMobileString.contains("No Calls")){
            callTiming.dayId = 9
            callTiming.openTime = edtOpenTime.text.toString()
            callTiming.closeTime = edtCloseTime.text.toString()
            businessCallList.add(callTiming)
            businessProfileData.bussiness_call_timing = businessCallList
        }else{
            try {
                val str = selectedDaysMobileString.filter { !it.isWhitespace() }
                val daysArray = str.split(',');
                Log.d(TAG, "$daysArray")

                for (i in 0 until daysArray.size) {
                    val timings = CallTiming()
                    Log.d(TAG, "${daysArray.get(i)}")

                    if(daysArray.get(i) == "Monday"){
                        timings.dayId = 1
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }else if(daysArray.get(i) == "Tuesday"){
                        timings.dayId = 2
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }else if(daysArray.get(i) == "Wednesday"){
                        timings.dayId = 3
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }else if(daysArray.get(i) == "Thursday"){
                        timings.dayId = 4
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }else if(daysArray.get(i) == "Friday"){
                        timings.dayId = 5
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }else if(daysArray.get(i) == "Saturday"){
                        timings.dayId = 6
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }else if(daysArray.get(i) == "Sunday"){
                        timings.dayId = 7
                        timings.openTime = edtOpenTime.text.toString()
                        timings.closeTime = edtCloseTime.text.toString()
                        businessCallList.add(timings)
                    }
                }
                businessProfileData.bussiness_call_timing = businessCallList
            }catch (e: Exception){
                e.printStackTrace()
            }
        }

        //whatsapp call timing

        var selectedDaysWpString = selectedDaysWp.text.toString()
        Log.d(TAG, "$selectedDaysWpString")

        val callTimingWp = CallTiming()
        //val businessCallArrayWp = JSONArray()
        wpCallList.clear()

        if(selectedDaysWpString.contains("All")){
            callTimingWp.dayId = 8
            callTimingWp.openTime = edtOpenTimeWp.text.toString()
            callTimingWp.closeTime = edtCloseTimeWp.text.toString()
            wpCallList.add(callTimingWp)
            businessProfileData.whatsapp_inquiry_call_timing = wpCallList
        }else if(selectedDaysWpString.contains("No Calls")){
            callTimingWp.dayId = 9
            callTimingWp.openTime = edtOpenTimeWp.text.toString()
            callTimingWp.closeTime = edtCloseTimeWp.text.toString()
            wpCallList.add(callTimingWp)
            businessProfileData.whatsapp_inquiry_call_timing = wpCallList
        }else{
            try {
                val str = selectedDaysWpString.filter { !it.isWhitespace() }
                val daysArray = str.split(',');
                Log.d(TAG, "$daysArray")

                for (i in 0 until daysArray.size) {
                    val timings = CallTiming()
                    Log.d(TAG, "${daysArray.get(i)}")

                    if(daysArray.get(i) == "Monday"){
                        timings.dayId = 1
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }else if(daysArray.get(i) == "Tuesday"){
                        timings.dayId = 2
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }else if(daysArray.get(i) == "Wednesday"){
                        timings.dayId = 3
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }else if(daysArray.get(i) == "Thursday"){
                        timings.dayId = 4
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }else if(daysArray.get(i) == "Friday"){
                        timings.dayId = 5
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }else if(daysArray.get(i) == "Saturday"){
                        timings.dayId = 6
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }else if(daysArray.get(i) == "Sunday"){
                        timings.dayId = 7
                        timings.openTime = edtOpenTimeWp.text.toString()
                        timings.closeTime = edtCloseTimeWp.text.toString()
                        wpCallList.add(timings)
                    }
                }
                businessProfileData.whatsapp_inquiry_call_timing = wpCallList
            }catch (e: Exception){
                e.printStackTrace()
            }
        }

        Log.d(TAG, "$businessProfileData")


        if(imgFront.drawable != null){
            val frontBitmap: Bitmap = (imgFront.getDrawable() as BitmapDrawable).getBitmap()

            if (isFrontImageSet && isNoImageFront){
                Log.d("BITMAP_DATA", "FRONT :  111")
                businessProfileData.is_change_front_img = 1
            }else if(isFrontImageSet && !isNoImageFront){
                if (frontBitmap.sameAs(initialFrontBitmap)) {
                    Log.d("BITMAP_DATA", "FRONT :  222")
                    businessProfileData.is_change_front_img = 0
                } else {
                    Log.d("BITMAP_DATA", "FRONT :  333")
                    businessProfileData.is_change_front_img = 1
                }
            }else if(!isFrontImageSet && !isNoImageFront){
                Log.d("BITMAP_DATA", "FRONT :  777")
                businessProfileData.is_change_front_img = 1
            }
        }else{
            if(isFrontImageSet && isNoImageFront){
                Log.d("BITMAP_DATA", "FRONT : NULL 444")
                businessProfileData.is_change_front_img = 1
            }else if(!isFrontImageSet && !isNoImageFront){
                Log.d("BITMAP_DATA", "FRONT : NULL 555")
                businessProfileData.is_change_front_img = 0
            }else if(!isFrontImageSet && isNoImageFront){
                Log.d("BITMAP_DATA", "FRONT : NULL 666")
                businessProfileData.is_change_front_img = 0
            }
        }


        if(imgBack.drawable != null){
            val backBitmap: Bitmap = (imgBack.getDrawable() as BitmapDrawable).getBitmap()

            if (isBackImageSet && isNoImageBack){
                Log.d("BITMAP_DATA", "BACK :  111")

                businessProfileData.is_change_back_img = 1
            }else if(isBackImageSet && !isNoImageBack){
                if (backBitmap.sameAs(initialBackBitmap)) {
                    Log.d("BITMAP_DATA", "BACK :  222")
                    businessProfileData.is_change_back_img = 0
                } else {
                    Log.d("BITMAP_DATA", "BACK :  333")
                    businessProfileData.is_change_back_img = 1
                }
            }else if(!isBackImageSet && !isNoImageBack){
                Log.d("BITMAP_DATA", "BACK :  777")
                businessProfileData.is_change_back_img = 1
            }
        }else{

            if(isBackImageSet && isNoImageBack){
                Log.d("BITMAP_DATA", "BACK : NULL 444")

                businessProfileData.is_change_back_img = 1
            }else if(!isBackImageSet && !isNoImageBack){
                Log.d("BITMAP_DATA", "BACK : NULL 555")

                businessProfileData.is_change_back_img = 0
            }else if(!isBackImageSet && isNoImageBack){
                Log.d("BITMAP_DATA", "BACK : NULL 666")

                businessProfileData.is_change_back_img = 0
            }

        }




        //original
        /*if(imgFront.drawable != null && imgBack.drawable != null){
            val frontBitmap: Bitmap = (imgFront.getDrawable() as BitmapDrawable).getBitmap()
            val backBitmap: Bitmap = (imgBack.getDrawable() as BitmapDrawable).getBitmap()


            if (frontBitmap.sameAs(initialFrontBitmap)) {
                Log.d("BITMAP_DATA", "FRONT : TRUE....")
                businessProfileData.is_change_front_img = 0
            } else {
                Log.d("BITMAP_DATA", "FRONT : FALSE....")
                businessProfileData.is_change_front_img = 1
            }

            if (backBitmap.sameAs(initialBackBitmap)) {
                Log.d("BITMAP_DATA", "BACK : TRUE....")
                businessProfileData.is_change_back_img = 0

            } else {
                Log.d("BITMAP_DATA", "BACK : FALSE....")
                businessProfileData.is_change_back_img = 1
            }
        }*/



        editBusinessProfileController.editBusinessProfile(businessProfileData, userModel)



    }

    private fun showDialogForDaysBusiness() {
        val builder = AlertDialog.Builder(this@BusinessProfileActivity)

        // set title
        builder.setTitle("Select Days")

        // set dialog non cancelable
        builder.setCancelable(false)
        builder.setMultiChoiceItems(
                langArray,
                selectedLanguageBusiness,
                object : DialogInterface.OnMultiChoiceClickListener {
                    override fun onClick(dialogInterface: DialogInterface?, i: Int, b: Boolean) {
                        // check condition
                        if (b) {

                            Log.d("SELECTED_DAY", "" + i)


                            // when checkbox selected
                            // Add position  in lang list
                            langListBusiness.add(i)
                            // Sort array list
                            Collections.sort(langListBusiness)


                        } else {
                            // when checkbox unselected
                            // Remove position from langList
                            langListBusiness.remove(Integer.valueOf(i))
                        }
                    }
                })
        builder.setPositiveButton("OK", object : DialogInterface.OnClickListener {
            override fun onClick(dialogInterface: DialogInterface?, i: Int) {

                if (!isAddProfile) {
                    showCallAllowDialog()
                }


                val stringBuilder = StringBuilder()
                for (j in langListBusiness.indices) {
                    // concat array value
                    stringBuilder.append(langArray[langListBusiness[j]])
                    // check condition
                    if (j != langListBusiness.size - 1) {
                        // When j value  not equal
                        // to lang list size - 1
                        // add comma
                        stringBuilder.append(", ")
                    }
                }
                // set text on textView

                if (stringBuilder.toString().contains("All")) {
                    selectedDaysMobile.setText("All Days")
                } else {
                    selectedDaysMobile.setText(stringBuilder.toString())
                }


            }
        })
        builder.setNegativeButton("Cancel", object : DialogInterface.OnClickListener {
            override fun onClick(dialogInterface: DialogInterface, i: Int) {
                // dismiss dialog
                dialogInterface.dismiss()
            }
        })

        //For All Days
        builder.setNeutralButton("All Days", object : DialogInterface.OnClickListener {
            override fun onClick(dialogInterface: DialogInterface?, i: Int) {
                // use for loop
                for (j in 0 until selectedLanguageBusiness!!.size) {
                    // remove all selection
                    selectedLanguageBusiness!![j] = false
                    // clear language list
                    langListBusiness.clear()
                    // clear text view value
                    selectedDaysMobile.setText("All Days")

                }
                if (!isAddProfile) {
                    showCallAllowDialog()
                }
            }
        })

        //for No Calls
        /*builder.setNeutralButton("No Calls", object : DialogInterface.OnClickListener {
            override fun onClick(dialogInterface: DialogInterface?, i: Int) {
                // use for loop
                for (j in 0 until selectedLanguageBusiness!!.size) {
                    // remove all selection
                    selectedLanguageBusiness!![j] = false
                    // clear language list
                    langListBusiness.clear()
                    // clear text view value
                    selectedDaysMobile.setText("No Calls")
                }
            }
        })*/

        // show dialog
        builder.show()
    }

    private fun showDialogForDaysWp() {
        val builder = AlertDialog.Builder(this@BusinessProfileActivity)

        // set title
        builder.setTitle("Select Days")

        // set dialog non cancelable
        builder.setCancelable(false)
        builder.setMultiChoiceItems(
                langArray,
                selectedLanguageWp,
                object : DialogInterface.OnMultiChoiceClickListener {
                    override fun onClick(dialogInterface: DialogInterface?, i: Int, b: Boolean) {
                        // check condition
                        if (b) {

                            Log.d("SELECTED_DAY", "" + i)


                            // when checkbox selected
                            // Add position  in lang list
                            langListWp.add(i)
                            // Sort array list
                            Collections.sort(langListWp)


                        } else {
                            // when checkbox unselected
                            // Remove position from langList
                            langListWp.remove(Integer.valueOf(i))
                        }
                    }
                })
        builder.setPositiveButton("OK", object : DialogInterface.OnClickListener {
            override fun onClick(dialogInterface: DialogInterface?, i: Int) {

                if (!isAddProfile) {
                    showCallAllowDialogWhatsapp()
                }

                val stringBuilder = StringBuilder()
                for (j in langListWp.indices) {
                    stringBuilder.append(langArray[langListWp[j]])
                    if (j != langListWp.size - 1) {
                        stringBuilder.append(", ")
                    }
                }
                // set text on textView

                if (stringBuilder.toString().contains("All")) {
                    selectedDaysWp.setText("All Days")
                } else {
                    selectedDaysWp.setText(stringBuilder.toString())
                }


            }
        })
        builder.setNegativeButton("Cancel", object : DialogInterface.OnClickListener {
            override fun onClick(dialogInterface: DialogInterface, i: Int) {
                // dismiss dialog
                dialogInterface.dismiss()
            }
        })

        //For All Days
        builder.setNeutralButton("All Days", object : DialogInterface.OnClickListener {
            override fun onClick(dialogInterface: DialogInterface?, i: Int) {
                // use for loop
                for (j in 0 until selectedLanguageWp!!.size) {
                    // remove all selection
                    selectedLanguageWp!![j] = false
                    // clear language list
                    langListWp.clear()
                    // clear text view value
                    selectedDaysWp.setText("All Days")

                }
                if (!isAddProfile) {
                    showCallAllowDialogWhatsapp()
                }
            }
        })

        //For No Calls
        /*builder.setNeutralButton("No Calls", object : DialogInterface.OnClickListener {
            override fun onClick(dialogInterface: DialogInterface?, i: Int) {
                // use for loop
                for (j in 0 until selectedLanguageWp!!.size) {
                    // remove all selection
                    selectedLanguageWp!![j] = false
                    // clear language list
                    langListWp.clear()
                    // clear text view value
                    selectedDaysWp.setText("No Calls")
                }
            }
        })*/


        // show dialog
        builder.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == GALLERY) {
            if (data != null) {
                val selectedImage = data.data
                startCropImageActivity(selectedImage!!)

            }
        } else if (requestCode == REQUEST_CAMERA) {

            /* if(data != null){
                onCaptureImageResult(data);
            }*/
            //Bitmap photo = (Bitmap) data.getExtras().get("data");
            // imgFront.setImageBitmap(photo);
            try {
                mImageBitmap = MediaStore.Images.Media.getBitmap(
                        getContentResolver(), Uri.parse(
                        mCurrentPhotoPath
                )
                )

            } catch (e: IOException) {
                e.printStackTrace()
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }

            if (mImageBitmap != null) {
                startCropImageActivity(getImageUri(applicationContext, mImageBitmap!!))
            }

        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            if(data!=null){
                val result: CropImage.ActivityResult = CropImage.getActivityResult(data)
                if (resultCode == RESULT_OK) {

                    BitmapOptimizer.optimizeFile(
                            result.getUri().getPath(),
                            200000L,
                            object : BitmapOptimizer.CompleteListener {
                                override fun onComplete(
                                        bitmap: Bitmap?,
                                        encodedImage: ByteArray?
                                ) {
                                    if (imageType == "front") {
                                        changedFrontImg = true
                                        txtFront.setVisibility(View.GONE)
                                        imgFront.setImageBitmap(bitmap)
                                        imgFront.setVisibility(View.VISIBLE)
                                        Glide.with(imgFront.getContext())
                                                .load(result.getUri())
                                                .into(imgFront)
                                    } else {
                                        changedBackImg = true
                                        txtBack.setVisibility(View.GONE)
                                        imgBack.setImageBitmap(bitmap)
                                        imgBack.setVisibility(View.VISIBLE)
                                        Glide.with(imgBack.getContext())
                                                .load(result.getUri())
                                                .into(imgBack)
                                    }
                                }

                                override fun onFail(message: String?) {
                                    Log.d(TAG, "onFail: == $message")
                                    Utils.showToast(this@BusinessProfileActivity,"image size is bigger than maximum limit")

                                }

                            })
                } else if(resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    //Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
                }
            }

        }
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (method.equals("GetLocation")) {
            locationlist.clear()
            typelist.clear()

            val dataModel = LocationListBeans()
            dataModel.LocationListBeans(da)

            locationlist = dataModel.locationList
            Utils.showLog(TAG, "==location list size==" + locationlist.size)
            typelist.add(resources.getText(R.string.txt_select_district_hint).toString())   //  In
            typeidlist.add("0")
            for (i in locationlist.indices) {
                val name = locationlist.get(i).locName
                val id = locationlist.get(i).locId
                typelist.add(name.toString())
                typeidlist.add(id.toString())
            }

            type_spinner_type = ArrayAdapter(
                    applicationContext,
                    android.R.layout.simple_spinner_dropdown_item,
                    typelist
            )
            val dataAdapter1 = ArrayAdapter(
                    applicationContext,
                    android.R.layout.simple_spinner_item,
                    typelist
            )
            dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

            spinner_district_business.adapter = dataAdapter1
            tv_dis_type_business.visibility = View.VISIBLE
            tv_dis_type_business.text = resources.getText(R.string.txt_select_district_hint)
            spinner_district_business.visibility = View.GONE
            rl_district_business.setBackgroundResource(R.drawable.bg_edittext)
        }

        if(method.equals("addBusinessProfile")){
            //Log.d(TAG,"SUCCESS_RESPONSE : $da" )
            val bussinessprofile_id = da.getString("bussinessprofile_id")

            storeUserData.setString(Constants.BUSINESS_USER_ID, "" + bussinessprofile_id)

            Utils.showToast(this, "Business Profile Added Successfully")
            finish()
        }

        if(method.equals("editBusinessProfile")){
            //Log.d(TAG,"SUCCESS_RESPONSE : $da" )
            Utils.showToast(this, "Business Profile Updated Successfully")
            finish()
        }


        if(method.equals("deleteBusinessProfile")){
            Utils.showToast(this, "Business Profile Deleted Successfully")
            storeUserData.setString(Constants.BUSINESS_USER_ID, "")

            finish()
        }

        if(method.equals("businessCall")){
            Utils.showToast(this, "Business call timing updated successfully")
            finish()
        }

        if(method.equals("whatsappCall")){
            Utils.showToast(this, "Whatsapp timing updated successfully")
            finish()
        }


        if(method.equals("getBusinessProfile")){
            isAddProfile = false

            val userModel = BusinessUserProfileBean()
            userModel.businessUserProfileBean(da)

            Log.d(
                    "getBusinessProfile",
                    "response : " + GsonBuilder().setPrettyPrinting().create().toJson(
                            userModel
                    )
            )



            setEditProfileData(userModel)

            //enable/disable EDIT & DELETE feature
            if(userModel.is_verified!=null){
                if(userModel.is_verified == "0"){
                    showPendingVerificationDialog()
                    imgMoreBusiness.visibility = View.GONE
                    txtPendingVerifiation.visibility = View.VISIBLE
                    btnSaveBusinessProfile.visibility = View.GONE
                    txtRejectedProfile.visibility =View.GONE
                    disableAllViews()

                }else if(userModel.is_verified == "1"){
                    imgMoreBusiness.visibility = View.VISIBLE
                    txtPendingVerifiation.visibility =View.GONE
                    btnSaveBusinessProfile.visibility = View.GONE
                    txtRejectedProfile.visibility =View.GONE
                    disableSomeViews()
                    showViews()
                }else{
                    showRejectedProfileDialog()
                    imgMoreBusiness.visibility = View.VISIBLE
                    txtPendingVerifiation.visibility =View.GONE
                    btnSaveBusinessProfile.visibility = View.GONE
                    txtRejectedProfile.visibility =View.VISIBLE
                    disableSomeViews()
                    showViews()

                }
            }




        }


    }




    private fun setEditProfileData(userModel: BusinessUserProfileBean) {


        if(userModel.is_calls_allow!=null){
            if(userModel.is_calls_allow.toString() == "0"){
                businessSwitch.isChecked = false
            }else{
                businessSwitch.isChecked = true
            }

        }

        if(userModel.is_whatsapp_calls_allow!=null){

            if(userModel.is_whatsapp_calls_allow.toString() == "0"){
                whatsappSwitch.isChecked = false
            }else{
                whatsappSwitch.isChecked = true
            }


        }

        if(userModel.rejected_reason!=null){
            if(!userModel.rejected_reason.toString().isEmpty()){
                rejected_reason = userModel.rejected_reason.toString()
            }
        }

        if(userModel.bussiness_name!=null){
            edt_business_name.setText(userModel.bussiness_name)
            tvtoolbartitle.text = userModel.bussiness_name +"'s Business Profile"
        }

        if(userModel.profileId!=null){
            titleProfileID.setText("Profile ID : " + userModel.profileId)
        }

        if(userModel.bussiness_about!=null){
            edt_about_business.setText(userModel.bussiness_about)
        }

        if(userModel.service_info!=null){
            edt_about_service.setText(userModel.service_info)
        }

        if(userModel.visitingcard_front!=null){
            if(!userModel.visitingcard_front!!.isEmpty()){

                Log.d("isImageSet"," : FRONT set: " )
                isFrontImageSet = true
                txtFront.setVisibility(View.GONE)

                Glide.with(applicationContext)
                        .load(userModel.visitingcard_front)
                        .placeholder(R.drawable.progress_animated)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(imgFront)


                Glide.with(applicationContext)
                        .asBitmap().load(userModel.visitingcard_front)
                        .listener(object : RequestListener<Bitmap?> {
                            override fun onLoadFailed(@Nullable e: GlideException?, o: Any, target: Target<Bitmap?>, b: Boolean): Boolean {
                                return false
                            }

                            override fun onResourceReady(bitmap: Bitmap?, o: Any, target: Target<Bitmap?>, dataSource: DataSource, b: Boolean): Boolean {

                                if (bitmap != null) {
                                    initialFrontBitmap = bitmap
                                }
                                return false
                            }
                        }
                        ).submit()



            }else{
                isFrontImageSet = false
                Log.d("isImageSet"," : FRONT Not set: " )
                setNullImageFront()

            }

        }

        if(userModel.visitingcard_back!=null){
            if(!userModel.visitingcard_back!!.isEmpty()){
                isBackImageSet = true
                Log.d("isImageSet"," : Back set: " )

                txtBack.setVisibility(View.GONE)

                Glide.with(applicationContext)
                        .load(userModel.visitingcard_back)
                        .placeholder(R.drawable.progress_animated)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(imgBack)


                Glide.with(applicationContext)
                        .asBitmap().load(userModel.visitingcard_back)
                        .listener(object : RequestListener<Bitmap?> {
                            override fun onLoadFailed(@Nullable e: GlideException?, o: Any, target: Target<Bitmap?>, b: Boolean): Boolean {
                                return false
                            }

                            override fun onResourceReady(bitmap: Bitmap?, o: Any, target: Target<Bitmap?>, dataSource: DataSource, b: Boolean): Boolean {

                                if (bitmap != null) {
                                    initialBackBitmap = bitmap
                                }
                                return false
                            }
                        }
                        ).submit()


            }else{
                isBackImageSet = false
                Log.d("isImageSet"," : Back not set: " )

                setNullImageBack()

            }

        }

        if(userModel.mobile_no!=null){
            etphoneContact.setText(userModel.mobile_no)
        }

        if(!userModel.bussiness_call_timing.isEmpty()){
            //set selected hours
            edtOpenTime.setText(userModel.bussiness_call_timing.get(0).openTime)
            edtCloseTime.setText(userModel.bussiness_call_timing.get(0).closeTime)


            //set selected days in dialog and textfield
            for (i in 0 until userModel.bussiness_call_timing.size) {
                for (j in 1 until langArray.size + 1){
                    if(userModel.bussiness_call_timing.get(i).dayId == j){
                        langListBusiness.add(j - 1)

                        selectedLanguageBusiness!![j - 1] = true // display selected days in dialog from api


                    }
                }
            }

            val stringBuilder = StringBuilder()
            for (j in langListBusiness.indices) {
                stringBuilder.append(langArray[langListBusiness[j]])
                if (j != langListBusiness.size - 1) {
                    stringBuilder.append(", ")
                }
            }

            Log.d("stringBuilder", "" + stringBuilder.toString())

            selectedDaysMobile.setText(stringBuilder.toString()) //for weekdays

            //For No Calls
            /*for (i in 0 until userModel.bussiness_call_timing.size) {
                if(userModel.bussiness_call_timing.get(i).dayId == 9){
                    selectedDaysMobile.setText("No Calls") //for No Calls
                }
            }*/

            //For All Days
            for (i in 0 until userModel.bussiness_call_timing.size) {
                if(userModel.bussiness_call_timing.get(i).dayId == 8){
                    selectedDaysMobile.setText("All Days") //for All Days
                }
            }

        }


        //whatsapp Inquiry start =============================
        if(userModel.whatsapp_inquiry_allow.toString() == "1"){
            radioYes.isChecked = true
            whatsappInquiryAllow = true
            llWhatsappInquiryMain.visibility = View.VISIBLE


            //same whatsapp no
            if(userModel.same_whatsapp_no.toString() == "1"){
                radioNumberYes.isChecked = true
                sameWhatsappNo = true
                llWhatsappNumber.visibility = View.GONE
            }else{
                radioNumberNo.isChecked = true
                sameWhatsappNo = false
                llWhatsappNumber.visibility = View.VISIBLE


                //whatsap no
                if(userModel.whatsapp_no!=null){
                    edtWhatsappNo.setText(userModel.whatsapp_no)
                }

            }

            //whatsapp mobile timing
            if(userModel.whatsapp_inquiry_timing_same!=null && userModel.whatsapp_inquiry_timing_same.toString() == "1"){
                //radioTimingYes.isChecked = true        //Set whatsapp inquiry timing radio buttons
                whatsappTnquiryTimingSame = true
                llWhatsappInquiryTiming.visibility = View.GONE
            }else{

                //radioTimingNo.isChecked = true         //Set whatsapp inquiry timing radio buttons
                whatsappTnquiryTimingSame = false
                llWhatsappInquiryTiming.visibility = View.VISIBLE

                //whatsapp inquiry call timing
                if(!userModel.whatsapp_inquiry_call_timing.isEmpty()){
                    //set selected hours
                    edtOpenTimeWp.setText(userModel.whatsapp_inquiry_call_timing.get(0).openTime)
                    edtCloseTimeWp.setText(userModel.whatsapp_inquiry_call_timing.get(0).closeTime)


                    //set selected days in dialog and textfield
                    for (i in 0 until userModel.whatsapp_inquiry_call_timing.size) {
                        for (j in 1 until langArray.size + 1){
                            if(userModel.whatsapp_inquiry_call_timing.get(i).dayId == j){
                                langListWp.add(j - 1)

                                selectedLanguageWp!![j - 1] = true // display selected days in dialog from api


                            }
                        }
                    }

                    val stringBuilder = StringBuilder()
                    for (j in langListWp.indices) {
                        stringBuilder.append(langArray[langListWp[j]])
                        if (j != langListWp.size - 1) {
                            stringBuilder.append(", ")
                        }
                    }

                    selectedDaysWp.setText(stringBuilder.toString()) //for weekdays

                    //For No Calls
                    /*for (i in 0 until userModel.whatsapp_inquiry_call_timing.size) {
                        if(userModel.whatsapp_inquiry_call_timing.get(i).dayId == 9){
                            selectedDaysWp.setText("No Calls") //for No Calls
                        }
                    }*/

                    //For All Days
                    for (i in 0 until userModel.whatsapp_inquiry_call_timing.size) {
                        if(userModel.whatsapp_inquiry_call_timing.get(i).dayId == 8){
                            selectedDaysWp.setText("All Days") //for All Days
                        }
                    }

                }


            }

        }else{
            radioNo.isChecked = true
            whatsappInquiryAllow = false
            llWhatsappInquiryMain.visibility = View.GONE
        }
        //whatsapp Inquiry end =============================

        if(userModel.email!=null){
            edt_email_business.setText(userModel.email)
        }

        if(userModel.address1!=null){
            edt_address_line_one.setText(userModel.address1)
        }

        if(userModel.address2!=null){
            edt_address_line_two.setText(userModel.address2)
        }

        if(userModel.city!=null){
            edt_village_town.setText(userModel.city)
        }

        if(userModel.location_id != null && userModel.location_id.toString().length >0) {
            locationid = userModel.location_id
            tv_dis_type_business.visibility = View.VISIBLE
            tv_dis_type_business.text = userModel.location_name
            spinner_district_business.visibility = View.GONE
        }


    }



    private fun disableSomeViews() {
        edt_business_name.isEnabled = false
        edt_about_business.isEnabled = false
        edt_about_service.isEnabled = false
        imgFront.isEnabled = false
        imgBack.isEnabled = false
        etphoneContact.isEnabled = false
        edtWhatsappNo.isEnabled = false
        edt_email_business.isEnabled = false
        edt_address_line_one.isEnabled = false
        edt_address_line_two.isEnabled = false
        edt_village_town.isEnabled = false
        tv_dis_type_business.isEnabled = false

        //disbale radio buttons
        //Set whatsapp inquiry timing radio buttons
/*
        radioTimingYes.isEnabled = false
        radioTimingNo.isEnabled = false
*/

        radioYes.isEnabled = false
        radioNo.isEnabled = false

        radioNumberYes.isEnabled = false
        radioNumberNo.isEnabled = false

        //disable business call view
        /*txtResetBusiness.isEnabled = false
        rlInfo.isEnabled = false
        edtOpenTime.isEnabled = false
        edtCloseTime.isEnabled = false
        businessSwitch.isEnabled = false
        rlSelectDay.isEnabled = false*/



        //disable whatsapp call view
        /*txtResetWp.isEnabled = false
        rlInfoTiming.isEnabled = false
        edtOpenTimeWp.isEnabled = false
        edtCloseTimeWp.isEnabled = false
        whatsappSwitch.isEnabled = false
        rlSelectDayWp.isEnabled = false*/

        //Gray text in state/district
        edt_state_business.setTextColor(ContextCompat.getColor(this, R.color.gray));
        tv_dis_type_business.setTextColor(ContextCompat.getColor(this, R.color.gray));

    }

    private fun disableAllViews() {
        edt_business_name.isEnabled = false
        edt_about_business.isEnabled = false
        edt_about_service.isEnabled = false
        imgFront.isEnabled = false
        imgBack.isEnabled = false
        etphoneContact.isEnabled = false
        edtWhatsappNo.isEnabled = false
        edt_email_business.isEnabled = false
        edt_address_line_one.isEnabled = false
        edt_address_line_two.isEnabled = false
        edt_village_town.isEnabled = false
        tv_dis_type_business.isEnabled = false

        //disbale radio buttons
        //Set whatsapp inquiry timing radio buttons

        /*radioTimingYes.isEnabled = false
        radioTimingNo.isEnabled = false*/

        radioYes.isEnabled = false
        radioNo.isEnabled = false

        radioNumberYes.isEnabled = false
        radioNumberNo.isEnabled = false

        //disable business call view
        txtResetBusiness.isEnabled = false
        rlInfo.isEnabled = false
        edtOpenTime.isEnabled = false
        edtCloseTime.isEnabled = false
        businessSwitch.isEnabled = false
        rlSelectDay.isEnabled = false



        //disable whatsapp call view
        txtResetWp.isEnabled = false
        rlInfoTiming.isEnabled = false
        edtOpenTimeWp.isEnabled = false
        edtCloseTimeWp.isEnabled = false
        whatsappSwitch.isEnabled = false
        rlSelectDayWp.isEnabled = false


        //set disable/gray background of BUSINESS CALL
        edtOpenTime.setBackgroundResource(R.drawable.round_border_gray_btn)
        edtCloseTime.setBackgroundResource(R.drawable.round_border_gray_btn)
        rlSelectDay.setBackgroundResource(R.drawable.round_border_gray_btn)
        edtOpenTime.setTextColor(ContextCompat.getColor(this, R.color.gray));
        edtCloseTime.setTextColor(ContextCompat.getColor(this, R.color.gray));
        tvSetDay.setTextColor(ContextCompat.getColor(this, R.color.gray));
        selectedDaysMobile.setTextColor(ContextCompat.getColor(this, R.color.gray));

        businessSwitch.getTrackDrawable().setColorFilter(ContextCompat.getColor(
                this, R.color.gray), PorterDuff.Mode.SRC_IN);



        //set disable/gray background of WHATSAPP CALL
        edtOpenTimeWp.setBackgroundResource(R.drawable.round_border_gray_btn)
        edtCloseTimeWp.setBackgroundResource(R.drawable.round_border_gray_btn)
        rlSelectDayWp.setBackgroundResource(R.drawable.round_border_gray_btn)
        edtOpenTimeWp.setTextColor(ContextCompat.getColor(this, R.color.gray));
        edtCloseTimeWp.setTextColor(ContextCompat.getColor(this, R.color.gray));
        tvSetDayWp.setTextColor(ContextCompat.getColor(this, R.color.gray));
        selectedDaysWp.setTextColor(ContextCompat.getColor(this, R.color.gray));

        whatsappSwitch.getTrackDrawable().setColorFilter(ContextCompat.getColor(
                this, R.color.gray), PorterDuff.Mode.SRC_IN);


        //gray text in state/district
        edt_state_business.setTextColor(ContextCompat.getColor(this, R.color.gray));
        tv_dis_type_business.setTextColor(ContextCompat.getColor(this, R.color.gray));

    }

    private fun enableAllViews() {
        edt_business_name.isEnabled = true
        edt_about_business.isEnabled = true
        edt_about_service.isEnabled = true
        imgFront.isEnabled = true
        imgBack.isEnabled = true
        etphoneContact.isEnabled = true
        edtWhatsappNo.isEnabled = true
        edt_email_business.isEnabled = true
        edt_address_line_one.isEnabled = true
        edt_address_line_two.isEnabled = true
        edt_village_town.isEnabled = true
        tv_dis_type_business.isEnabled = true

        //Enable radio buttons
        //Set whatsapp inquiry timing radio buttons
/*
        radioTimingYes.isEnabled = true
        radioTimingNo.isEnabled = true
*/

        radioYes.isEnabled = true
        radioNo.isEnabled = true

        radioNumberYes.isEnabled = true
        radioNumberNo.isEnabled = true

        //enable business call view
        txtResetBusiness.isEnabled = true
        rlInfo.isEnabled = true
        edtOpenTime.isEnabled = true
        edtCloseTime.isEnabled = true
        businessSwitch.isEnabled = true
        rlSelectDay.isEnabled = true

        //enable whatsapp call view
        txtResetWp.isEnabled = true
        rlInfoTiming.isEnabled = true
        edtOpenTimeWp.isEnabled = true
        edtCloseTimeWp.isEnabled = true
        whatsappSwitch.isEnabled = true
        rlSelectDayWp.isEnabled = true

        //Black text in state/district
        edt_state_business.setTextColor(ContextCompat.getColor(this, R.color.black));
        tv_dis_type_business.setTextColor(ContextCompat.getColor(this, R.color.black));

    }

    private fun showViews() {
        txtResetBusiness.visibility = View.VISIBLE
        rlInfo.visibility = View.VISIBLE

        txtResetWp.visibility = View.VISIBLE
        rlInfoTiming.visibility = View.VISIBLE
    }


    override fun onFail(msg: String, method: String) {

        if(method.equals("getBusinessProfile")){
            Log.d(TAG, "ERROR_RESPONSE getBusinessProfile: " + msg)
            isAddProfile = true
        }

        if(method.equals("addBusinessProfile")){
            Log.d(TAG, "ERROR_RESPONSE : " + msg)

            Utils.showToast(this, "error : $msg")
        }

        if(method.equals("deleteBusinessProfile")){
            Log.d(TAG, "ERROR_RESPONSE : " + msg)

            Utils.showToast(this, "error : $msg")
        }

        if(method.equals("editBusinessProfile")){
            Log.d(TAG, "ERROR_RESPONSE : " + msg)
            Utils.showToast(this, "" + msg)
            finish()

        }

    }

}