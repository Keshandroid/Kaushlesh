package com.kaushlesh.view.activity.productDetails

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import com.kaushlesh.Controller.AddContaintReportPostController
import com.kaushlesh.R
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Utils
import kotlinx.android.synthetic.main.activity_report_ad.*
import org.json.JSONObject

class ReportAdActivity : AppCompatActivity(), View.OnClickListener ,ParseControllerListener{


    val TAG  ="ReportAdActivity"
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView

    var Name: String = "";
    lateinit var post_id: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report_ad)

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = getString(R.string.ad_this_repoer)

        btnback.setOnClickListener(this)
        btn_send.setOnClickListener(this)

        if (intent != null) {

            post_id = intent.getStringExtra("postId").toString()
            //   Toast.makeText(this, post_id, Toast.LENGTH_SHORT).show()
            Utils.showLog(TAG,"==post id==" + post_id)

        }

        btn_send.setOnClickListener(this)

        chkfauduser.setOnClickListener() {
            // chkfauduser.isChecked!=chkfauduser.isChecked
            if (chkfauduser.isChecked) {
                Name = chkfauduser.text.toString()
            } else {
                Name = ""
            }
            chkOffensivecontent.isChecked = false
            chkduplicate.isChecked = false
            chkother.isChecked = false

        }

        chkOffensivecontent.setOnClickListener() {
            if (chkOffensivecontent.isChecked) {
                Name = chkOffensivecontent.text.toString()
            } else {
                Name = ""
            }
            chkfauduser.isChecked = false
            chkduplicate.isChecked = false
            chkother.isChecked = false
        }

        chkduplicate.setOnClickListener() {
            if (chkduplicate.isChecked) {
                Name = chkduplicate.text.toString()
            } else {
                Name = ""
            }
            chkfauduser.isChecked = false
            chkOffensivecontent.isChecked = false
            chkother.isChecked = false

        }

        chkother.setOnClickListener() {
            if (chkother.isChecked) {
                Name = chkother.text.toString()
            } else {
                Name = ""
            }
            chkfauduser.isChecked = false
            chkOffensivecontent.isChecked = false
            chkduplicate.isChecked = false
        }
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_back -> onBackPressed()

            R.id.btn_send -> {
                if (Name.length > 3) {
                    var a = et_comment.text.toString()
                    if (a != "") {
                    } else {
                        a = "-"
                    }
                    val c = AddContaintReportPostController(
                        this,
                        this,
                        post_id.toString(),
                        Name,
                        a

                    )
                    c.onClick(btn_send)
                } else {
                    Utils.showToast(this,getString(R.string.txt_select_any_option))
                }
            }
        }
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        Utils.showToast(this,message)
        onBackPressed()
    }

    override fun onFail(msg: String, method: String) {

        Utils.showToast(this,msg)
    }
}