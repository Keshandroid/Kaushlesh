package com.kaushlesh.view.activity.Login

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.kaushlesh.Controller.EditPhoneNumberController
import com.kaushlesh.Controller.RegisterPhoneController
import com.kaushlesh.R
import com.kaushlesh.bean.RegisterPhoneBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.DBHelper
import com.kaushlesh.utils.Utils
import kotlinx.android.synthetic.main.activity_add_phone_number.*
import org.json.JSONException
import org.json.JSONObject


class AddPhoneNumberActivity : AppCompatActivity() ,View.OnClickListener,ParseControllerListener{

    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal lateinit var activity: Activity

    var model: RegisterPhoneBean = RegisterPhoneBean()
    var userId: String? = null;
    var userToken: String? = null;
    var Otp: String? = null;
    var isEdit: Boolean = false
    var dataModel = RegisterPhoneBean()
    var isphoneuse : Boolean = true

    internal lateinit var dbHelper: DBHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_phone_number)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        val extras = intent.extras
        if (extras != null) {
            model =
                (intent.getSerializableExtra("editnumber") as RegisterPhoneBean?)!! //Obtaining data
            userId = model.userid
            userToken = model.userToken
            Otp = model.OTP.toString()
            isEdit = true
            etphone.setText(model.ConatctNo)

        }
        dbHelper = DBHelper.getInstance(this@AddPhoneNumberActivity)

        activity = this

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.txt_login)

        btnback.setOnClickListener(this)
        btn_next.setOnClickListener(this)
        iv_myes_unselect.setOnClickListener(this)
        iv_mno_unselect.setOnClickListener(this)

        //grantAllPermissions()

    }

    private fun grantAllPermissions() {


        Dexter.withActivity(this)
            .withPermissions(
                /*Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CALL_PHONE,
                Manifest.permission.ACCESS_FINE_LOCATION*/

                arrayListOf(Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.CALL_PHONE,
                    Manifest.permission.ACCESS_FINE_LOCATION)

            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    // check if all permissions are granted
                    if (report.areAllPermissionsGranted()) {
                        // do you work now
                    }

                    // check for permanent denial of any permission
                    if (report.isAnyPermissionPermanentlyDenied) {
                        // permission is denied permenantly, navigate user to app settings
                        //grantAllPermissions()
                        showSettingsDialog()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>, token: PermissionToken) {
                    token.continuePermissionRequest()
                }
            })
            .withErrorListener { error ->
                Toast.makeText(
                    applicationContext,
                    "Error occurred! $error",
                    Toast.LENGTH_SHORT
                ).show()
            }
            .onSameThread()
            .check()
    }

    private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Need Permissions")
        builder.setMessage("This app needs permission to use this application. You can grant them in app settings.")
        builder.setPositiveButton(
            "GOTO SETTINGS"
        ) { dialog, which ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton(
            "Cancel"
        ) { dialog, which -> dialog.cancel() }
        builder.show()
    }

    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", getPackageName(), null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_back -> onBackPressed()

            R.id.btn_next -> {
                /* val intent = Intent(applicationContext, OTPMobileActivity::class.java)
                startActivity(intent)*/

                Utils.showProgress(this)

                if (etphone.text.length != 10) {
                    Utils.dismissProgress()
                    etphone.setError(getString(R.string.txt_min_ten_digit))
                } else {
                    Utils.setButtonEnabled(btn_next, false)
                    if (isEdit == true) {

                        callForEditPhone()
                    } else {
                        callForRegisterPhone()
                    }
                }
            }
            R.id.iv_myes_unselect -> {
                isphoneuse = true
                iv_myes_select.visibility = View.VISIBLE
                iv_mno_select.visibility = View.GONE
            }

            R.id.iv_mno_unselect -> {
                isphoneuse = false
                iv_myes_select.visibility = View.GONE
                iv_mno_select.visibility = View.VISIBLE
            }
        }
    }

    private fun callForEditPhone() {

        Log.e("phone", "callForEditPhone: " + etphone.text)
        val c = EditPhoneNumberController(
            this, activity,
            userId.toString().trim({ it <= ' ' }),
            etphone.text.toString().trim({ it <= ' ' }),
            userToken.toString().trim({ it <= ' ' })
        )
        c.onClick(btn_next)
    }

    private fun callForRegisterPhone() {
        val puse: Int
        if(isphoneuse)
        {
            puse = 1
        }
        else{
            puse = 0
        }
        val c = RegisterPhoneController(
            this,
            activity,
            etphone.getText().toString().trim({ it <= ' ' }),
            puse
        )
        c.onClick(btn_next)
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {

        AppConstants.printLog("TAG", "==method==" + method)
        AppConstants.printToast(this, message)
        // progress_spinner?.dismiss()
        Utils.dismissProgress()

        if (method.equals("registerphone")) {
            Log.e("get profile Result-", "$da==")
            try {
                dataModel.RegisterPhoneBean(da)

                val intent = Intent(applicationContext, OTPMobileActivity::class.java)
                val bundle = Bundle()
                bundle.putSerializable("value", dataModel)
                intent.putExtras(bundle)
                startActivity(intent)
                finish()
               /* if (message.equals("User already Registered with this phone")) {
                    //dbUpdatedata()
                    val intent = Intent(applicationContext, MainActivity::class.java)
                    val bundle = Bundle()
                    bundle.putSerializable("value", dataModel)
                    intent.putExtras(bundle)
                    startActivity(intent)
                    finish()
                } else {
                    /// dbAddData()
                    val intent = Intent(applicationContext, OTPMobileActivity::class.java)
                    val bundle = Bundle()
                    bundle.putSerializable("value", dataModel)
                    intent.putExtras(bundle)
                    startActivity(intent)
                    finish()
                }*/

            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }

        if (method.equals("EditNumber")) {
            if (da.length() > 0) {
                Log.e("get profile Result-", "$da==")
                try {

                    dataModel.RegisterPhoneBean(da)

                    val intent = Intent(applicationContext, OTPMobileActivity::class.java)
                    val bundle = Bundle()
                    bundle.putSerializable("value", dataModel)
                    intent.putExtra("phone", etphone.text.toString())
                    intent.putExtra("isedit", true)
                    intent.putExtras(bundle)
                    startActivity(intent)
                    finish()

                   /* if (message.equals(getString(R.string.txt_user_already_registered))) {
                        //dbUpdatedata()
                        val intent = Intent(applicationContext, MainActivity::class.java)
                        val bundle = Bundle()
                        bundle.putSerializable("value", dataModel)
                        intent.putExtras(bundle)
                        startActivity(intent)
                        finish()

                    } else {
                        //  dbAddData()
                        val intent = Intent(applicationContext, OTPMobileActivity::class.java)
                        val bundle = Bundle()
                        bundle.putSerializable("value", dataModel)
                        intent.putExtras(bundle)
                        startActivity(intent)
                        finish()
                    }*/
                    // }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            } else {
                AppConstants.printToast(activity, message)
            }


        }

    }

    override fun onFail(msg: String, method: String) {
        AppConstants.printToast(this, msg)
        Log.e("errror", "onFail: " + msg)
        Utils.dismissProgress()

    }

    override fun onResume() {
        Utils.setButtonEnabled(btn_next, true)
        super.onResume()
    }
}
