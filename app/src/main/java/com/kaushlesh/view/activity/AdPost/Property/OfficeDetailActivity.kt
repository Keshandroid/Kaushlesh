package com.kaushlesh.view.activity.AdPost.Property

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.ParcelFileDescriptor
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.kaushlesh.R
import com.kaushlesh.bean.AdPost.AdOfficeForSellRentBean
import com.kaushlesh.bean.AdPost.ApVillsellandRentBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.utils.AdPostImages
import com.kaushlesh.utils.validations.AdPostPropertyValidation
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.AdPost.UploadActivity1
import com.kaushlesh.view.activity.AdPost.UploadPhotoActivity
import gun0912.tedimagepicker.builder.TedImagePicker
import kotlinx.android.synthetic.main.activity_ap_villa_sell_and_rent.*
import kotlinx.android.synthetic.main.activity_office_detail.*
import kotlinx.android.synthetic.main.activity_office_detail.et_ad_title
import kotlinx.android.synthetic.main.activity_office_detail.et_other_info
import kotlinx.android.synthetic.main.activity_office_detail.tv_1
import java.io.*

class OfficeDetailActivity : AppCompatActivity(), View.OnClickListener {
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal lateinit var btnnext: Button
    internal lateinit var tvbasement: TextView
    internal lateinit var tvgroundfloor: TextView
    internal lateinit var tvfloor1: TextView
    internal lateinit var tvfloor2: TextView
    internal lateinit var tvnewlaunch: TextView
    internal lateinit var tvreadyshift: TextView
    internal lateinit var tvundercons: TextView
    internal lateinit var tvfurnished: TextView
    internal lateinit var tvsemifurnished: TextView
    internal lateinit var tvunfurnished: TextView
    internal lateinit var tvbuilder: TextView
    internal lateinit var tvdealer: TextView
    internal lateinit var tvowner: TextView
    internal lateinit var etcarpetarea: EditText
    internal lateinit var etmaintenance: EditText
    internal lateinit var etwashroom: EditText
    internal lateinit var etpropertyname: EditText
    internal lateinit var etadtitle: EditText
    internal lateinit var etotherinfo: EditText
    internal lateinit var tvcp1: TextView
    internal lateinit var tvcp2: TextView
    internal lateinit var tvcp3: TextView
    internal lateinit var tvcp4: TextView
    internal lateinit var tvcp5: TextView
    internal lateinit var tvnorth: TextView
    internal lateinit var tvsouth: TextView
    internal lateinit var tveast: TextView
    internal lateinit var tvwest: TextView

    internal var basement: Boolean? = false
    internal var groundfloor: Boolean? = false
    internal var floor1: Boolean? = false
    internal var floor2: Boolean? = false
    internal var newlaunc: Boolean? = false
    internal var readyshift: Boolean? = false
    internal var undercons: Boolean? = false
    internal var furnished: Boolean? = false
    internal var semifurnished: Boolean? = false
    internal var unfurnished: Boolean? = false
    internal var builder: Boolean? = false
    internal var dealer: Boolean? = false
    internal var owner: Boolean? = false
    internal var cp1: Boolean? = false
    internal var cp2: Boolean? = false
    internal var cp3: Boolean? = false
    internal var cp4: Boolean? = false
    internal var cp5: Boolean? = false
    internal var north: Boolean? = false
    internal var south: Boolean? = false
    internal var east: Boolean? = false
    internal var west: Boolean? = false

    lateinit var storeUserData: StoreUserData
    lateinit var adOfficeForSellRent: AdOfficeForSellRentBean
    lateinit var resizebitmapmain: Bitmap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_office_detail)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        storeUserData=StoreUserData(this)
        adOfficeForSellRent = AdOfficeForSellRentBean()

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.txt_include_details)

        initBindViews()
    }

    private fun initBindViews() {
        btnnext = findViewById(R.id.btn_next)

        tvbasement = findViewById(R.id.tvbasement)
        tvgroundfloor = findViewById(R.id.tvgroundfloor)
        tvfloor1 = findViewById(R.id.tvf1)
        tvfloor2 = findViewById(R.id.tvf2)
        tvnewlaunch = findViewById(R.id.tvnewlaunch)
        tvreadyshift = findViewById(R.id.tvredyshift)
        tvundercons = findViewById(R.id.tvundercon)
        tvfurnished = findViewById(R.id.tvfurnished)
        tvsemifurnished = findViewById(R.id.tvsemifurnished)
        tvunfurnished = findViewById(R.id.tvunfurnished)
        tvbuilder = findViewById(R.id.tvbuilder)
        tvdealer = findViewById(R.id.tvdealer)
        tvowner = findViewById(R.id.tvowner)
        etcarpetarea = findViewById(R.id.et_carpet_area)
        etmaintenance = findViewById(R.id.et_maintenance)
        etwashroom = findViewById(R.id.et_washroom)
        etpropertyname = findViewById(R.id.et_pname)
        etadtitle = findViewById(R.id.et_ad_title)
        etotherinfo = findViewById(R.id.et_other_info)
        tvcp1 = findViewById(R.id.tvcar0)
        tvcp2 = findViewById(R.id.tvcar1)
        tvcp3 = findViewById(R.id.tvcar2)
        tvcp4 = findViewById(R.id.tvcar3)
        tvcp5 = findViewById(R.id.tvcar4)
        tvnorth = findViewById(R.id.tvnorth)
        tvsouth = findViewById(R.id.tvsouth)
        tveast = findViewById(R.id.tveast)
        tvwest = findViewById(R.id.tvwest)

        //setbackgroundSelected(R.drawable.bg_edittext_black)

        tvbasement.setOnClickListener(this)
        tvgroundfloor.setOnClickListener(this)
        tvfloor1.setOnClickListener(this)
        tvfloor2.setOnClickListener(this)

        btnback.setOnClickListener(this)
        btnnext.setOnClickListener(this)

        tvnewlaunch.setOnClickListener(this)
        tvreadyshift.setOnClickListener(this)
        tvundercons.setOnClickListener(this)

        tvfurnished.setOnClickListener(this)
        tvsemifurnished.setOnClickListener(this)
        tvunfurnished.setOnClickListener(this)

        tvbuilder.setOnClickListener(this)
        tvdealer.setOnClickListener(this)
        tvowner.setOnClickListener(this)

        tvcp1.setOnClickListener(this)
        tvcp2.setOnClickListener(this)
        tvcp3.setOnClickListener(this)
        tvcp4.setOnClickListener(this)
        tvcp5.setOnClickListener(this)

        tvnorth.setOnClickListener(this)
        tvsouth.setOnClickListener(this)
        tveast.setOnClickListener(this)
        tvwest.setOnClickListener(this)

        Utils.setontouch(et_other_info)
        Utils.setontouch1(et_ad_title)

        etadtitle.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {


                tv_1.text = s.length.toString() + "/77"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })

        etotherinfo.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {


                tv_2.text = s.length.toString() + "/5000"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })

        adOfficeForSellRent.listed_by= ""
        adOfficeForSellRent.construction_status= ""
        adOfficeForSellRent.furnishing= ""
        adOfficeForSellRent.car_parking= "0"//change
        adOfficeForSellRent.facing= ""
    }

    private fun setbackgroundSelected(b: Int) {
        tvbasement.setBackgroundResource(b)
        tvnorth.setBackgroundResource(b)
        tvfurnished.setBackgroundResource(b)
        tvnewlaunch.setBackgroundResource(b)
        tvowner.setBackgroundResource(b)
        tvcp1.setBackgroundResource(b)
    }


    override fun onClick(view: View) {
        when (view.id) {

            R.id.btn_back -> onBackPressed()

            R.id.btn_next -> {

                validations()

            }

            R.id.tvbasement -> {
                setTypeFourValue(true, false, false, false, "floor")
                setTypeFourBottomSelection(
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    R.color.gray,
                    "floor"
                )
            }

            R.id.tvgroundfloor -> {
                setTypeFourValue(false, true, false, false, "floor")
                setTypeFourBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    "floor"
                )
            }

            R.id.tvf1 -> {
                setTypeFourValue(false, false, true, false, "floor")
                setTypeFourBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    "floor"
                )
            }

            R.id.tvf2 -> {
                setTypeFourValue(false, false, false, true, "floor")
                setTypeFourBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    "floor"
                )
            }

            R.id.tvnewlaunch -> {
                if(newlaunc!!)
                {
                    setTypeValue(false, false, false, "construction")
                    setConstruction()
                }
                else {
                    setTypeValue(true, false, false, "construction")
                    setTypeBottomSelection(
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.color.black,
                        R.color.gray,
                        R.color.gray,
                        "construction"
                    )
                    adOfficeForSellRent.construction_status = "1"
                }
            }

            R.id.tvredyshift -> {

                if(readyshift!!)
                {
                    setTypeValue(false, false, false, "construction")
                    setConstruction()
                }
                else {
                    setTypeValue(false, true, false, "construction")
                    setTypeBottomSelection(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.color.gray,
                        R.color.black,
                        R.color.gray,
                        "construction"
                    )
                    adOfficeForSellRent.construction_status = "2"
                }
            }

            R.id.tvundercon -> {
                if(undercons!!)
                {
                    setTypeValue(false, false, false, "construction")
                    setConstruction()
                }
                else {
                    setTypeValue(false, false, true, "construction")
                    setTypeBottomSelection(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.color.gray,
                        R.color.gray,
                        R.color.black,
                        "construction"
                    )
                    adOfficeForSellRent.construction_status = "3"
                }
            }

            R.id.tvfurnished -> {
                setTypeValue(true, false, false, "furnish")
                setTypeBottomSelection(
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    "furnish"
                )

                adOfficeForSellRent.furnishing= "1"
            }

            R.id.tvsemifurnished -> {
                setTypeValue(false, true, false, "furnish")
                setTypeBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    "furnish"
                )

                adOfficeForSellRent.furnishing="2"
            }

            R.id.tvunfurnished -> {
                setTypeValue(false, false, true, "furnish")
                setTypeBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    "furnish"
                )

                adOfficeForSellRent.furnishing="3"
            }

            R.id.tvowner -> {
                setTypeValue(true, false, false, "listedby")
                setTypeBottomSelection(
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    "listedby"
                )
                adOfficeForSellRent.listed_by= "3"
            }

            R.id.tvdealer -> {
                setTypeValue(false, true, false, "listedby")
                setTypeBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    "listedby"
                )

                adOfficeForSellRent.listed_by="2"
            }

            R.id.tvbuilder -> {
                setTypeValue(false, false, true, "listedby")
                setTypeBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    "listedby"
                )

                adOfficeForSellRent.listed_by="1"
            }

            R.id.tvcar0 -> {
                if(cp1!!)
                {
                    setValue(false, false, false, false, false, "car")
                    setcar()
                }
                else {
                    setValue(true, false, false, false, false, "car")
                    setBottomSelection(
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.color.black,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        "car"
                    )
//                    adOfficeForSellRent.car_parking = "0"
                    adOfficeForSellRent.car_parking = "1"
                }
            }

            R.id.tvcar1 -> {
                if(cp2!!)
                {
                    setValue(false, false, false, false, false, "car")
                    setcar()
                }
                else {
                    setValue(false, true, false, false, false, "car")
                    setBottomSelection(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.color.gray,
                        R.color.black,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        "car"
                    )
                    //adOfficeForSellRent.car_parking = "1"
                    adOfficeForSellRent.car_parking = "2"
                }
            }

            R.id.tvcar2 -> {
                if(cp3!!)
                {
                    setValue(false, false, false, false, false, "car")
                    setcar()
                }
                else {
                    setValue(false, false, true, false, false, "car")
                    setBottomSelection(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.color.gray,
                        R.color.gray,
                        R.color.black,
                        R.color.gray,
                        R.color.gray,
                        "car"
                    )
                    //adOfficeForSellRent.car_parking = "2"
                    adOfficeForSellRent.car_parking = "3"
                }
            }

            R.id.tvcar3 -> {
                if(cp4!!)
                {
                    setValue(false, false, false, false, false, "car")
                    setcar()
                }
                else {
                    setValue(false, false, false, true, false, "car")
                    setBottomSelection(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        R.color.black,
                        R.color.gray,
                        "car"
                    )
                    //adOfficeForSellRent.car_parking = "3"
                    adOfficeForSellRent.car_parking = "4"
                }
            }

            R.id.tvcar4 -> {
                if(cp5!!)
                {
                    setValue(false, false, false, false, false, "car")
                    setcar()
                }
                else{
                setValue(false, false, false, false, true, "car")
                setBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.gray,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    "car"
                )
                //adOfficeForSellRent.car_parking= "4"
                    adOfficeForSellRent.car_parking= "5"
                }
            }

            R.id.tvnorth -> {

                if(north!!)
                {
                    setTypeFourValue(false, false, false, false, "facing")
                    setfacing()
                }
                else {
                    setTypeFourValue(true, false, false, false, "facing")
                    setTypeFourBottomSelection(
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.color.black,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        "facing"
                    )
                    adOfficeForSellRent.facing = "1"
                }
            }

            R.id.tvsouth -> {
                if(south!!)
                {
                    setTypeFourValue(false, false, false, false, "facing")
                    setfacing()
                }
                else {
                    setTypeFourValue(false, true, false, false, "facing")
                    setTypeFourBottomSelection(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.color.gray,
                        R.color.black,
                        R.color.gray,
                        R.color.gray,
                        "facing"
                    )

                    adOfficeForSellRent.facing = "2"
                }
            }

            R.id.tveast -> {

                if(east!!)
                {
                    setTypeFourValue(false, false, false, false, "facing")
                    setfacing()
                }
                else {
                    setTypeFourValue(false, false, true, false, "facing")
                    setTypeFourBottomSelection(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.color.gray,
                        R.color.gray,
                        R.color.black,
                        R.color.gray,
                        "facing"
                    )

                    adOfficeForSellRent.facing = "3"
                }
            }

            R.id.tvwest -> {

                if(west!!)
                {
                    setTypeFourValue(false, false, false, false, "facing")
                    setfacing()
                }
                else {
                    setTypeFourValue(false, false, false, true, "facing")
                    setTypeFourBottomSelection(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        R.color.black,
                        "facing"
                    )

                    adOfficeForSellRent.facing = "4"
                }
            }
        }
    }

    private fun setConstruction() {

        setTypeBottomSelection(
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.color.gray,
            R.color.gray,
            R.color.gray,
            "construction"
        )
        adOfficeForSellRent.construction_status= ""
    }

    private fun setfacing() {

        setTypeFourBottomSelection(
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.color.gray,
            R.color.gray,
            R.color.gray,
            R.color.gray,
            "facing"
        )
        adOfficeForSellRent.facing= ""
    }

    private fun setcar() {

        setBottomSelection(
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.color.gray,
            R.color.gray,
            R.color.gray,
            R.color.gray,
            R.color.gray,
            "car"
        )
        adOfficeForSellRent.car_parking= "0"
    }

    private fun setTypeFourValue(
        tb1: Boolean,
        tb2: Boolean,
        tb3: Boolean,
        tb4: Boolean,
        type: String
    ) {

        if (type.equals("floor", ignoreCase = true)) {
            basement = tb1
            groundfloor = tb2
            floor1 = tb3
            floor2 = tb4
        }

        if (type.equals("facing", ignoreCase = true)) {
            north = tb1
            south = tb2
            east = tb3
            west = tb4
        }


    }

    private fun setTypeFourBottomSelection(
        t1: Int,
        t2: Int,
        t3: Int,
        t4: Int,
        t5: Int,
        t6: Int,
        t7: Int,
        t8: Int,
        type: String
    ) {

        if (type.equals("floor", ignoreCase = true)) {
            tvbasement.setBackgroundResource(t1)
            tvgroundfloor.setBackgroundResource(t2)
            tvfloor1.setBackgroundResource(t3)
            tvfloor2.setBackgroundResource(t4)

            tvbasement.setTextColor(resources.getColor(t5))
            tvgroundfloor.setTextColor(resources.getColor(t6))
            tvfloor1.setTextColor(resources.getColor(t7))
            tvfloor2.setTextColor(resources.getColor(t8))
        }

        if (type.equals("facing", ignoreCase = true)) {
            tvnorth.setBackgroundResource(t1)
            tvsouth.setBackgroundResource(t2)
            tveast.setBackgroundResource(t3)
            tvwest.setBackgroundResource(t4)

            tvnorth.setTextColor(resources.getColor(t5))
            tvsouth.setTextColor(resources.getColor(t6))
            tveast.setTextColor(resources.getColor(t7))
            tvwest.setTextColor(resources.getColor(t8))
        }

    }

    private fun setTypeValue(tb1: Boolean, tb2: Boolean, tb3: Boolean, type: String) {


        if (type.equals("furnish", ignoreCase = true)) {
            furnished = tb1
            semifurnished = tb2
            unfurnished = tb3
        }

        if (type.equals("construction", ignoreCase = true)) {
            newlaunc = tb1
            readyshift = tb2
            undercons = tb3
        }

        if (type.equals("listedby", ignoreCase = true)) {
            owner = tb1
            dealer = tb2
            builder = tb3
        }
    }

    private fun setTypeBottomSelection(
        t1: Int,
        t2: Int,
        t3: Int,
        t4: Int,
        t5: Int,
        t6: Int,
        type: String
    ) {

        if (type.equals("furnish", ignoreCase = true)) {
            tvfurnished.setBackgroundResource(t1)
            tvsemifurnished.setBackgroundResource(t2)
            tvunfurnished.setBackgroundResource(t3)

            tvfurnished.setTextColor(resources.getColor(t4))
            tvsemifurnished.setTextColor(resources.getColor(t5))
            tvunfurnished.setTextColor(resources.getColor(t6))
        }

        if (type.equals("construction", ignoreCase = true)) {
            tvnewlaunch.setBackgroundResource(t1)
            tvreadyshift.setBackgroundResource(t2)
            tvundercons.setBackgroundResource(t3)

            tvnewlaunch.setTextColor(resources.getColor(t4))
            tvreadyshift.setTextColor(resources.getColor(t5))
            tvundercons.setTextColor(resources.getColor(t6))
        }

        if (type.equals("listedby", ignoreCase = true)) {
            tvowner.setBackgroundResource(t1)
            tvdealer.setBackgroundResource(t2)
            tvbuilder.setBackgroundResource(t3)

            tvowner.setTextColor(resources.getColor(t4))
            tvdealer.setTextColor(resources.getColor(t5))
            tvbuilder.setTextColor(resources.getColor(t6))
        }
    }

    private fun setBottomSelection(
        bd1: Int,
        bd2: Int,
        bd3: Int,
        bd4: Int,
        bd5: Int,
        bd6: Int,
        bd7: Int,
        bd8: Int,
        bd9: Int,
        bd10: Int,
        type: String
    ) {

        if (type.equals("car", ignoreCase = true)) {
            tvcp1.setBackgroundResource(bd1)
            tvcp2.setBackgroundResource(bd2)
            tvcp3.setBackgroundResource(bd3)
            tvcp4.setBackgroundResource(bd4)
            tvcp5.setBackgroundResource(bd5)

            tvcp1.setTextColor(resources.getColor(bd6))
            tvcp2.setTextColor(resources.getColor(bd7))
            tvcp3.setTextColor(resources.getColor(bd8))
            tvcp4.setTextColor(resources.getColor(bd9))
            tvcp5.setTextColor(resources.getColor(bd10))
        }
    }

    private fun setValue(
        b1: Boolean,
        b2: Boolean,
        b3: Boolean,
        b4: Boolean,
        b5: Boolean,
        type: String
    ) {

        if (type.equals("car", ignoreCase = true)) {
            cp1 = b1
            cp2 = b2
            cp3 = b3
            cp4 = b4
            cp5 = b5
        }
    }

    companion object {

        private val TAG = "OfficeDetailActivity"
    }

    private fun validations(){

        val issubmit = AdPostPropertyValidation.checkForOffuce(this,adOfficeForSellRent,et_floor,etcarpetarea,etadtitle,etotherinfo)
        Utils.showLog(TAG,"status" +  issubmit)
        if(issubmit) {
            adOfficeForSellRent.category_id = storeUserData.getString(Constants.CATEGORY_ID)
            adOfficeForSellRent.sub_category_id = storeUserData.getString(Constants.SUBCATEGORYID)
            adOfficeForSellRent.floor_no = et_floor.text.toString()
            adOfficeForSellRent.carpet_area = etcarpetarea.text.toString()
            adOfficeForSellRent.maintenance= etmaintenance.text.toString()
            adOfficeForSellRent.project_name= etpropertyname.text.toString()
            adOfficeForSellRent.wash_room= etwashroom.text.toString()
            adOfficeForSellRent.title = etadtitle.text.toString()
            adOfficeForSellRent.other_information = etotherinfo.text.toString()

            val gson = Gson()
            val json = gson.toJson(adOfficeForSellRent)
            storeUserData.setString(Constants.ADDOFFICESELLANDRENT,json)

            Log.e("save", "adOfficeForSell save data: "+storeUserData.getString(Constants.ADDOFFICESELLANDRENT))

            //val intent = Intent(applicationContext, UploadPhotoActivity::class.java)
            //startActivity(intent)

            setNormalMultiButton()
        }
    }
    fun setNormalMultiButton() {
        val selectedUriList: List<Uri>? = null

        TedImagePicker.with(this)
                .max(12, "Maximum 12 photos allowed")
                .errorListener { message ->
                    Log.d("ted", "message: $message")
                }
                .selectedUri(selectedUriList)
                .startMultiImage { list: List<Uri> ->
                    Log.e("list===", "listsize: ${list.size}")
                    showMultiImage1(list)
                }

    }

    private fun showMultiImage1(uriList: List<Uri>) {

        Log.d("ted=", "uriList: $uriList")
        Log.e("uriList.get(0)====", "uriList.get(0): $uriList.get(0)")

        Thread(Runnable {
            AppConstants.imageList.clear()
            AppConstants.imageListuri.clear()
            uriList.forEach {
                AppConstants.imageListuri.add(it)
                val imagepath = getPathFromGooglePhotosUri(it)
                resizebitmapmain = decodeImageFromFiles(imagepath, 280, 280)
                Log.e("resizebitmapmain====", "01--: $resizebitmapmain")
                val ei = ExifInterface(imagepath.toString())
                val orientation: Int = ei.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED
                )
                var rotatedBitmap: Bitmap? = null

                when (orientation) {
                    ExifInterface.ORIENTATION_ROTATE_90 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 90F)
                    ExifInterface.ORIENTATION_ROTATE_180 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 180F)
                    ExifInterface.ORIENTATION_ROTATE_270 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 270F)
                    ExifInterface.ORIENTATION_NORMAL -> rotatedBitmap = resizebitmapmain
                    else -> rotatedBitmap = resizebitmapmain
                }

                AppConstants.imageList.add(rotatedBitmap!!)
            }

            Log.e("imageListsize====", "viewSize: ${AppConstants.imageList.size}")

            if(AppConstants.imageList!=null)
            {
                Log.e("AppConstantsimageList==", "viewSize: ${AppConstants.imageList.size}")
                checkAndSaveOffice(this)
            }

        }).start()

        AdPostImages.changeScreentoNext(this)

    }

    private fun checkAndSaveOffice(context: Context) {
        ///  bimglist.clear()
        val storeUserData = StoreUserData(context)
        val gson = Gson()

        if (AppConstants.imageList.size >= 1) {

            Log.e("test", "imagelist: " + AppConstants.imageList.size+"===")

            val json: String = storeUserData.getString(Constants.ADDOFFICESELLANDRENT)

            Log.e("String", "String data: " + json+"====")

            val adOfficeForSellBeanList = gson.fromJson(json, AdOfficeForSellRentBean::class.java)

            Log.e("test", "listsave data: " + adOfficeForSellBeanList.toString()+"====")
            adOfficeForSellBeanList.post_images?.clear()
            adOfficeForSellBeanList.post_images = AppConstants.imageList
            Log.e("test", "post_images: " +  adOfficeForSellBeanList.post_images!!.size+"===")


            Log.e("test", "post_images data: " + adOfficeForSellBeanList.post_images)

            val jsonset = gson.toJson(adOfficeForSellBeanList)

            storeUserData.setString(Constants.ADDOFFICESELLANDRENT, jsonset)

            ////   AdPostImages.changeScreentoLocation(context)

        } else {
            Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
        }
    }

    fun rotateImage(source: Bitmap, angle: Float): Bitmap? {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(
                source, 0, 0, source.width, source.height,
                matrix, true
        )
    }

    fun decodeImageFromFiles(path: String?, width: Int, height: Int): Bitmap {
        val scaleOptions = BitmapFactory.Options()
        scaleOptions.inJustDecodeBounds = true
        BitmapFactory.decodeFile(path, scaleOptions)
        var scale = 1
        while (scaleOptions.outWidth / scale / 2 >= width
                && scaleOptions.outHeight / scale / 2 >= height
        ) {
            scale *= 2
        }
        // decode with the sample size
        val outOptions = BitmapFactory.Options()
        outOptions.inSampleSize = scale
        val bitmap: Bitmap = BitmapFactory.decodeFile(path, outOptions)
        Utils.showLog(
                UploadActivity1.TAG,
                "===BitmapFactory===width" + bitmap.width + "===height" + bitmap.height + "==="
        )

        return bitmap
    }

    fun getPathFromGooglePhotosUri(uriPhoto: Uri?): String? {
        if (uriPhoto == null) return null
        var input: FileInputStream? = null
        var output: FileOutputStream? = null
        try {
            val pfd: ParcelFileDescriptor? = contentResolver.openFileDescriptor(uriPhoto, "r")
            val fd: FileDescriptor = pfd!!.getFileDescriptor()
            input = FileInputStream(fd)
            val tempFilename: String = getTempFilename(this)
            output = FileOutputStream(tempFilename)
            var read: Int = 0
            val bytes = ByteArray(4096)
            while (input.read(bytes).also({ read = it }) != -1) {
                output.write(bytes, 0, read)
            }
            return tempFilename
        } catch (ignored: IOException) {
            // Nothing we can do
        } finally {
            closeSilently(input)
            closeSilently(output)
        }
        return null
    }

    @Throws(IOException::class)
    private fun getTempFilename(context: Context): String {
        val outputDir = context.cacheDir
        val outputFile = File.createTempFile("image", "tmp", outputDir)
        return outputFile.absolutePath
    }

    fun closeSilently(c: Closeable?) {
        if (c == null) return
        try {
            c.close()
        } catch (t: Throwable) {
            // Do nothing
        }
    }
}

