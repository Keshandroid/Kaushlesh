package com.kaushlesh.view.activity

import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.gson.GsonBuilder
import com.kaushlesh.BuildConfig
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.R
import com.kaushlesh.bean.TutorialBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Identity
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.Login.LoginMainActivity
import org.json.JSONException
import org.json.JSONObject
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.util.*


class WelcomeActivity: AppCompatActivity(), ParseControllerListener {

    private var viewPager: ViewPager? = null
    private var myViewPagerAdapter: MyViewPagerAdapter? = null
    private var dotsLayout: LinearLayout? = null
    private lateinit var dots: Array<TextView?>
    //lateinit var layouts: IntArray
    private var btnSkip: Button? = null
    private  var btnNext:Button? = null

    lateinit var getListingController: GetListingController
    var mTutorialList: MutableList<TutorialBean.Tutorial> = ArrayList()


    //version check
    var currentVersion: String? = null
    var latestVersion: String? = null
    var forceUpdate: String? = ""
    private var popup: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        }

        setContentView(R.layout.activity_welcome)

        viewPager = findViewById<View>(R.id.view_pager) as ViewPager
        dotsLayout = findViewById<View>(R.id.layoutDots) as LinearLayout
        btnSkip = findViewById<View>(R.id.btn_skip) as Button
        btnNext = findViewById<View>(R.id.btn_next) as Button


        getListingController = GetListingController(this, this)
        getTutorialDetails()

        //Maintanance mode
        getListingController.getMaintananceMode()

        /*layouts = intArrayOf(
                R.layout.welcome_slide1,
                R.layout.welcome_slide2,
                R.layout.welcome_slide3)*/


        changeStatusBarColor()


        btnSkip!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                launchLoginScreen()
            }
        })

        btnNext!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {

                if (!mTutorialList.isEmpty()) {
                    // checking for last page
                    // if last page home screen will be launched
                    val current: Int = getItem(+1)
                    if (current < mTutorialList.size) {
                        // move to next screen
                        viewPager!!.setCurrentItem(current)
                    } else {
                        launchLoginScreen()
                    }
                }
            }
        })

    }


    private fun addBottomDots(currentPage: Int) {

        if(!mTutorialList.isEmpty()){
            dots = arrayOfNulls(mTutorialList.size)
            val colorsActive = resources.getIntArray(R.array.array_dot_active)
            val colorsInactive = resources.getIntArray(R.array.array_dot_inactive)
            dotsLayout!!.removeAllViews()
            for (i in 0 until dots.size) {
                dots[i] = TextView(this)
                dots[i]!!.setText(Html.fromHtml("&#8226;"))
                dots[i]!!.setTextSize(35F)
                dots[i]!!.setTextColor(colorsInactive[currentPage])
                dotsLayout!!.addView(dots[i])
            }
            if (dots.size > 0) dots[currentPage]!!.setTextColor(colorsActive[currentPage])
        }
    }

    private fun getItem(i: Int): Int {
        return viewPager!!.currentItem + i
    }

    private fun getTutorialDetails(){
        getListingController = GetListingController(this, this)
        getListingController.getTutorialData()
    }

    private fun launchLoginScreen() {
//        prefManager.setFirstTimeLaunch(false)
        val intent: Intent?
        intent = Intent(this@WelcomeActivity, LoginMainActivity::class.java)
        startActivity(intent)
        finish()
    }

    //  viewpager change listener
    var viewPagerPageChangeListener: ViewPager.OnPageChangeListener = object : ViewPager.OnPageChangeListener {
        override fun onPageSelected(position: Int) {
            addBottomDots(position)

            // changing the next button text 'NEXT' / 'GOT IT'
            if (position == mTutorialList.size - 1) {
                // last page. make button text to GOT IT
                btnNext!!.setText("GOT IT")
                btnSkip!!.visibility = View.GONE
            } else {
                // still pages are left
                btnNext!!.setText("NEXT")
                btnSkip!!.visibility = View.VISIBLE
            }
        }

        override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {}
        override fun onPageScrollStateChanged(arg0: Int) {}
    }

    /**
     * Making notification bar transparent
     */
    private fun changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window: Window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.setStatusBarColor(Color.TRANSPARENT)
        }
    }

    /**
     * View pager adapter
     */
    inner class MyViewPagerAdapter(private val _activity: Activity,
                                   private val _tutorialList: ArrayList<TutorialBean.Tutorial>) : PagerAdapter() {
        private var layoutInflater: LayoutInflater? = null
        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val imgTutorial: ImageView

            layoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater?

            /*val view: View = layoutInflater!!.inflate(layouts.get(position), container, false)
            container.addView(view)
            return view*/

            val viewLayout = LayoutInflater.from(container.context)
                    .inflate(R.layout.welcome_slide1, container, false)

            imgTutorial = viewLayout.findViewById<View>(R.id.imgTutorial) as ImageView
            Glide.with(applicationContext)
                    .load(_tutorialList[position].tutorialImage)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgTutorial)


            (container as ViewPager).addView(viewLayout)
            return viewLayout

        }

        override fun getCount(): Int {
            return _tutorialList.size
        }

        override fun isViewFromObject(view: View, obj: Any): Boolean {
            return view === obj
        }


        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            val view = `object` as View?
            container.removeView(view)
        }

        /*fun destroyItem(container: ViewGroup, position: Int, `object`: Any?) {
            val view = `object` as View?
            container.removeView(view)
        }*/
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if(method.equals("getTutorial")){
            if (da.length() > 0) {

                Log.e("get getTutorial Result-", "$da==")
                try {

                    val dataModel = TutorialBean()
                    dataModel.TutorialBean(da)

                    Log.e(
                            "TUTORIALS",
                            "" + GsonBuilder().setPrettyPrinting().create().toJson(dataModel)
                    )

                    if(!dataModel.tutorialList.isEmpty()){

                        bindViewpager(dataModel.tutorialList)

                    }else{
                        //llHomeAds.visibility = View.GONE
                    }

                } catch (e: JSONException) {
                    //llHomeAds.visibility = View.GONE
                    e.printStackTrace()
                }
            }


        }


        if(method.equals("maintananceMode")) {
            if (da.length() > 0) {
                Log.e("maintananceMode", "$da==")
                try {
                    var maintananceObject: JSONObject? = null
                    if (da.has("result") && da.get("result") is JSONObject) {
                        maintananceObject = da.getJSONObject("result")
                    }
                    if (maintananceObject != null) {
                        if(maintananceObject.has("maintananceMode")){
                            val maintananceMode = maintananceObject.get("maintananceMode").toString()
                            if(maintananceMode.toString() == "1"){
                                if(maintananceObject.has("maintananceModeMessage")){
                                    val intent = Intent(this@WelcomeActivity, MaintananceActivity::class.java)
                                    intent.putExtra("maintananceModeMessage", maintananceObject.get("maintananceModeMessage").toString())
                                    startActivity(intent)
                                    finish()
                                }else{
                                    val intent = Intent(this@WelcomeActivity, MaintananceActivity::class.java)
                                    startActivity(intent)
                                    finish()
                                }
                            }

                        }

                        if(maintananceObject.has("androidForceUpdate")){
                            val androidForceUpdate = maintananceObject.get("androidForceUpdate").toString()
                            if(androidForceUpdate.toString() == "1"){
                                //Force UPDATE
                                forceUpdate = "true"
                            }else{
                                forceUpdate = "false"
                            }
                            getCurrentVersion()
                        }

                    }




                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }

    }

    private fun bindViewpager(tutorialList: ArrayList<TutorialBean.Tutorial>) {
        this.mTutorialList  = tutorialList

        myViewPagerAdapter = MyViewPagerAdapter(this, tutorialList)
        viewPager!!.setAdapter(myViewPagerAdapter)
        viewPager!!.addOnPageChangeListener(viewPagerPageChangeListener)

        addBottomDots(0)


    }

    override fun onFail(msg: String, method: String) {

    }

    private fun getCurrentVersion() {
        val pm: PackageManager = this.packageManager
        var pInfo: PackageInfo? = null
        try {
            pInfo = pm.getPackageInfo(this.packageName, 0)
        } catch (e1: PackageManager.NameNotFoundException) {
            // TODO Auto-generated catch block
            e1.printStackTrace()
        }
        currentVersion = pInfo!!.versionName

        Log.d("VERSION_CHECK", "=== Current version===$currentVersion")

        GetLatestVersion().execute()
    }

    inner class GetLatestVersion : AsyncTask<String?, String?, JSONObject>() {
        private val progressDialog: ProgressDialog? = null
        override fun onPreExecute() {
            super.onPreExecute()
        }

        override fun onPostExecute(jsonObject: JSONObject) {
            if (latestVersion != null) {
                if (!currentVersion.equals(latestVersion)) {
                    if (!isFinishing()) { //This would help to prevent Error : BinderProxy@45d459c0 is not valid; is your activity running? error
                        Log.d("VERSION_CHECK", "UPDATE THE APP")

                        if(!Identity.getUpdateDialog(this@WelcomeActivity).equals("update_later")){
                            showUpdateDialog()
                        }
                    }
                } else {
                    Log.d("VERSION_CHECK", "CONTINUE OPEN APP")
                }
            } else{
                //background.start();
                super.onPostExecute(jsonObject)
            }

        }

        override fun doInBackground(vararg p0: String?): JSONObject {
            try {
                //It retrieves the latest version by scraping the content of current version from play store at runtime

                val doc: Document = Jsoup.connect("https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}&hl=en_IN&gl=US").get()
//                val doc: Document = Jsoup.connect("https://play.google.com/store/apps/details?id=com.cutnbrush&hl=en_IN&gl=US").get()


                latestVersion = doc.getElementsByClass("htlgb").get(6).text()
                Log.d("VERSION_CHECK", "=== latest version===$latestVersion")
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return JSONObject()
        }
    }

    fun showUpdateDialog() {
        if (!isFinishing) {
            popup = Dialog(this, R.style.DialogCustom)
            popup!!.setContentView(R.layout.dialog_update_app)
            popup!!.setCanceledOnTouchOutside(false)
            popup!!.setCancelable(false)
            popup!!.show()
            val noThanks: TextView = popup!!.findViewById(R.id.txtNoThanks)
            val updateButton: Button = popup!!.findViewById(R.id.btnUpdate)

            if(forceUpdate.equals("true")){
                noThanks.visibility = View.GONE
            }else{
                noThanks.visibility = View.VISIBLE
            }

            noThanks.setOnClickListener {
                Identity.setUpdateDialog(this, "update_later")
                popup!!.dismiss()
            }

            updateButton.setOnClickListener{
                popup!!.dismiss()

                try {
                    val viewIntent = Intent(
                        "android.intent.action.VIEW",
                        Uri.parse("https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}")
//                        Uri.parse("https://play.google.com/store/apps/details?id=com.cutnbrush")

                    )
                    startActivity(viewIntent)
                } catch (e: java.lang.Exception) {
                    Utils.showToast(this, "Unable to Connect Try Again...")
                    e.printStackTrace()
                }

            }

        }
    }

}