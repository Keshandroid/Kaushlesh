package com.kaushlesh.view.activity

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.*
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.GsonBuilder
import com.kaushlesh.Controller.*
import com.kaushlesh.R
import com.kaushlesh.adapter.NotificationsAdapter
import com.kaushlesh.bean.NotificationBean
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.PostDetail.CheckCategoryForPostDetails
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.widgets.CustomButton
import kotlinx.android.synthetic.main.activity_business_profile.*
import org.json.JSONObject
import java.util.*

class NotificationsActivity : AppCompatActivity(),NotificationsAdapter.ItemClickListener, ParseControllerListener {

    internal lateinit var rv_notifications: RecyclerView
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal lateinit var btndelete: ImageView
    internal lateinit var dialog: Dialog
    internal lateinit var rl_no_notifications: RelativeLayout


    private lateinit var getListingController: GetListingController
    val listOrder = ArrayList<NotificationBean.NotificationList>()

    val MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 123
    var result = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notifications)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        btndelete = toolbar.findViewById(R.id.btn_delete)
        tvtoolbartitle = toolbar.findViewById(R.id.title)
        rl_no_notifications = findViewById(R.id.rl_no_notifications)

        //tvtoolbartitle.text = resources.getString(R.string.txt_invoices)
        tvtoolbartitle.text = "Notifications"

        rv_notifications = findViewById(R.id.rv_notifications)

        getNotificationList()

        btnback.setOnClickListener {
          onBackPressed()
        }

        btndelete.setOnClickListener{
            showDialog()
        }

    }

    private fun notificationViewCount(stringBuilder: StringBuilder) {
        NotificationViewController(this,stringBuilder)
    }

    private fun notificationDelete(stringBuilder: StringBuilder) {

        val deleteController = DeleteNotificationsController(
                this,
                this@NotificationsActivity,
                stringBuilder
        )
        deleteController.onClick()
    }

    private fun notificationDeleteALL() {

        val deleteController = DeleteAllNotificationsController(
                this,
                this@NotificationsActivity
        )
        deleteController.onClick()
    }

    private fun getNotificationList() {
        Utils.showProgress(this)
        getListingController = GetListingController(this, this)
        getListingController.getNotificationList()
    }

    private fun showDialog(){
        dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_delete_notifications)
        dialog.setTitle("")
        dialog.setCanceledOnTouchOutside(true)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val btn_no = dialog.findViewById(R.id.btn_no) as CustomButton
        val btn_yes = dialog.findViewById(R.id.btn_yes) as CustomButton

        btn_no.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
        })

        btn_yes.setOnClickListener(View.OnClickListener {

            Utils.showProgress(this)

            //get unread Ids
            /*val stringBuilder = StringBuilder()
            for (i in listOrder.indices) {
                var notificationList: NotificationBean.NotificationList
                notificationList = listOrder.get(i)

                if(notificationList.notification_id!=null){
                    if(i==listOrder.size-1){
                        stringBuilder.append(notificationList.notification_id)
                    }else{
                        stringBuilder.append(notificationList.notification_id,",")
                    }
                }

            }*/
            //call api to delete all notifications

            notificationDeleteALL()


            dialog.dismiss()
        })

        dialog.show()
    }


    /*fun showProgressDialog(activity: Activity, msg: String) {
        if (Utils.progressDialog==null){
            Utils.showProgress(activity)
        }
    }

    fun dismissProgressDialog() {

        if (Utils.progressDialog != null && Utils.progressDialog!!.isShowing()) {
            Utils.dismissProgress()
        }
    }*/


    private fun bindRecyclerview() {
        val recyclerLayoutManager = LinearLayoutManager(applicationContext)

        rv_notifications.layoutManager = recyclerLayoutManager
        rv_notifications.itemAnimator = DefaultItemAnimator()
        val adapter = NotificationsAdapter(listOrder, applicationContext!!)
        adapter.setClicklistner(this)
        rv_notifications.adapter = adapter
        adapter.notifyDataSetChanged()



        //get unread Ids
        val stringBuilder = StringBuilder()
        for (i in listOrder.indices) {
            var notificationList: NotificationBean.NotificationList
            notificationList = listOrder.get(i)

            if(notificationList.is_read!=null){
                if(notificationList.notification_id!=null){
                    if(notificationList.is_read == "0"){
                        if(i==listOrder.size-1){
                            stringBuilder.append(notificationList.notification_id)
                        }else{
                            stringBuilder.append(notificationList.notification_id,",")
                        }
                    }
                }
            }

        }


        //call api to read all notifications
        Log.d("unread_count", "STRING : " +stringBuilder)
        notificationViewCount(stringBuilder)



    }



    private fun checkResponse() {
        try {
            if (listOrder.size > 0) {
                rl_no_notifications.visibility = View.GONE
                rv_notifications.visibility = View.VISIBLE
                btndelete.visibility = View.VISIBLE
                bindRecyclerview()
            }else{
                btndelete.visibility = View.GONE
                rl_no_notifications.visibility = View.VISIBLE
                rv_notifications.visibility = View.GONE
            }
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (method.equals("GetNotification")) {

            Log.d("notiList",GsonBuilder().setPrettyPrinting().create().toJson(da))

            listOrder.clear()
            val dataModel = NotificationBean()
            dataModel.NotificationBean(da)
            listOrder.addAll(dataModel.notificationList)
            Utils.dismissProgress()



            checkResponse()

        }

        if (method.equals("deleteNotifications")) {
            Utils.dismissProgress()
            getNotificationList()

        }

        if (method.equals("deleteAllNotifications")) {
            Utils.dismissProgress()
            getNotificationList()
        }

    }

    override fun onFail(msg: String, method: String) {
        Utils.dismissProgress()

        if (method.equals("GetNotification")) {
            btndelete.visibility = View.GONE
            rl_no_notifications.visibility = View.VISIBLE
            rv_notifications.visibility = View.GONE
        }

    }



    override fun itemclick(bean: NotificationBean.NotificationList) {

        if(bean.is_live!=null){
            if(bean.is_live.equals("1")){
                val storeusedataPost = StoreUserData(this@NotificationsActivity)
                storeusedataPost.setString(Constants.OPEN_POST_DETAIL, "product_list")
                storeusedataPost.setString(Constants.CATEGORY_ID, bean.category_id.toString())
                storeusedataPost.setString(Constants.SUBCATEGORYID, bean.sub_category_id.toString())
                storeusedataPost.setString(Constants.FROM_DETAIL, "true")
                CheckCategoryForPostDetails.OpenScreen(
                        this@NotificationsActivity,
                        bean.category_id!!.toInt(),
                        bean.sub_category_id!!.toInt(),
                        bean.post_id!!.toInt()
                )
            }else{
                Utils.showToast(this, "Post Not Found")
            }
        }




    }

    override fun itemDeleteClick(bean: NotificationBean.NotificationList) {
        Utils.showProgress(this)

        val stringBuilder = StringBuilder()
        stringBuilder.append(bean.notification_id)
        notificationDelete(stringBuilder)
    }



}
