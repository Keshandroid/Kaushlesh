package com.kaushlesh.view.activity.Business

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.ablanco.zoomy.Zoomy
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.kaushlesh.R
import kotlinx.android.synthetic.main.activity_full_screen_image_view.*
import kotlinx.android.synthetic.main.activity_view_business_profile.*
import kotlinx.android.synthetic.main.activity_view_full_screen_img.*
import java.util.*

class ViewFullScreenImgActivity: AppCompatActivity() {

    private var image : String= ""

    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_full_screen_img)

        image = intent.getStringExtra("image").toString()


        Glide.with(applicationContext)
            .load(image)
            .placeholder(R.drawable.progress_animated)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .into(fullScreenImage)

        Zoomy.Builder(this)
            .target(fullScreenImage)
            .enableImmersiveMode(false)
            .register()

        imgCloseFullScreen.setOnClickListener{
            finish()
        }


    }



}