package com.kaushlesh.view.activity

import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import com.kaushlesh.Controller.ScreenRedirectionController
import com.kaushlesh.R
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.fragment.Chat.AllChatFragment
import com.kaushlesh.view.fragment.Chat.BuyingChatFragment
import com.kaushlesh.view.fragment.Chat.SellingChatFragment
import kotlinx.android.synthetic.main.activity_chat.*

class ChatActivity : AppCompatActivity(), View.OnClickListener {
    private var mFragmentManager: FragmentManager? = null
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal lateinit var tvall: TextView
    internal lateinit var tvbuying: TextView
    internal lateinit var tvselling: TextView
    internal lateinit var v12: View
    internal lateinit var v22: View
    internal lateinit var v32: View
    var mActivity = MainActivity()
    var selectedtab = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        mFragmentManager = supportFragmentManager

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.txt_chat)

        tvall = findViewById(R.id.tvall)
        tvbuying = findViewById(R.id.tvbuying)
        tvselling = findViewById(R.id.tvselling)
        v12 = findViewById(R.id.v12)
        v22 = findViewById(R.id.v22)
        v32 = findViewById(R.id.v32)

        btnback.setOnClickListener(this)
        tvall.setOnClickListener(this)
        tvbuying.setOnClickListener(this)
        tvselling.setOnClickListener(this)
        tv_edit.setOnClickListener(this)
        tv_search.setOnClickListener(this)

        setAllTabData()
    }

    private fun setAllTabData(): Boolean {
        setBottomIconSelected(
            View.VISIBLE,
            R.color.orange,
            View.GONE,
            R.color.black,
            View.GONE,
            R.color.black
        )

        setAllTab()


        searchview.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                if (newText.length == 0) {
                    /* tv_search.setText("Search")
                    searchview.visibility = View.GONE*/

                    setSearchDatalear(selectedtab)

                }
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {

                performSearch(selectedtab, query.toString())

                return false
            }

        })

        return true
    }

    private fun performSearch(selectedtab: Int, query: String) {
        if(selectedtab == 1)
        {
            val fm = supportFragmentManager
            val fragment: AllChatFragment? = fm.findFragmentById(R.id.fl_main) as AllChatFragment?
            fragment!!.performSearch(query.toString())
        }
        if(selectedtab == 2)
        {
            val fm = supportFragmentManager
            val fragment: BuyingChatFragment? = fm.findFragmentById(R.id.fl_main) as BuyingChatFragment?
            fragment!!.performSearch(query.toString())
        }
        if(selectedtab == 3)
        {
            val fm = supportFragmentManager
            val fragment: SellingChatFragment? = fm.findFragmentById(R.id.fl_main) as SellingChatFragment?
            fragment!!.performSearch(query.toString())
        }

    }

    private fun setSearchDatalear(selectedtab: Int) {
        if(selectedtab == 1)
        {
            val fm = supportFragmentManager
            val fragment: AllChatFragment? = fm.findFragmentById(R.id.fl_main) as AllChatFragment?
            fragment!!.performSearch("clear")
        }
        if(selectedtab == 2)
        {
            val fm = supportFragmentManager
            val fragment: BuyingChatFragment? = fm.findFragmentById(R.id.fl_main) as BuyingChatFragment?
            fragment!!.performSearch("clear")
        }
        if(selectedtab == 3)
        {
            val fm = supportFragmentManager
            val fragment: SellingChatFragment? = fm.findFragmentById(R.id.fl_main) as SellingChatFragment?
            fragment!!.performSearch("clear")
        }

    }


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_back -> onBackPressed()

            R.id.tv_edit -> {
                Utils.showLog(TAG, "===selected tab===" + selectedtab)

                setTab(selectedtab)

            }

            R.id.tv_search -> {
                Utils.showLog(TAG, "===selected tab===" + selectedtab)
                if (tv_search.text.equals(getString(R.string.txt_search))) {
                    tv_search.setText(getString(R.string.txt_cancel))
                    searchview.visibility = View.VISIBLE
                } else {
                    tv_search.setText(getString(R.string.txt_search))
                    searchview.visibility = View.GONE
                }
            }

            R.id.tvall -> {
                tv_edit.setText(getString(R.string.txt_edit))
                setBottomIconSelected(
                    View.VISIBLE,
                    R.color.orange,
                    View.GONE,
                    R.color.black,
                    View.GONE,
                    R.color.black
                )

                setAllTab()
            }

            R.id.tvbuying -> {
                selectedtab = 2
                tv_edit.setText(getString(R.string.txt_edit))
                setBottomIconSelected(
                    View.GONE,
                    R.color.black,
                    View.VISIBLE,
                    R.color.orange,
                    View.GONE,
                    R.color.black
                )

                setBuyingTab()
            }

            R.id.tvselling -> {
                selectedtab = 3
                tv_edit.setText(getString(R.string.txt_edit))
                setBottomIconSelected(
                    View.GONE,
                    R.color.black,
                    View.GONE,
                    R.color.black,
                    View.VISIBLE,
                    R.color.orange
                )

                setSellingTab()
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun setTab(selectedtab: Int) {

        if(selectedtab ==1)
        {
            if(tv_edit.text.equals(getString(R.string.txt_edit))) {
                tv_edit.setText(getString(R.string.txt_cancel))
                val fm = supportFragmentManager
                val fragment: AllChatFragment? = fm.findFragmentById(R.id.fl_main) as AllChatFragment?
                fragment!!.performEdit(2)
            }
            else{
                tv_edit.setText(getString(R.string.txt_edit))
                val fm = supportFragmentManager
                val fragment: AllChatFragment? = fm.findFragmentById(R.id.fl_main) as AllChatFragment?
                fragment!!.performEdit(1)
            }
        }

        if(selectedtab ==2)
        {
            if(tv_edit.text.equals(getString(R.string.txt_edit))) {
                tv_edit.setText(getString(R.string.txt_cancel))
                val fm = supportFragmentManager
                val fragment: BuyingChatFragment? = fm.findFragmentById(R.id.fl_main) as BuyingChatFragment?
                fragment!!.performEdit(2)
            }
            else{
                tv_edit.setText(getString(R.string.txt_edit))
                val fm = supportFragmentManager
                val fragment: BuyingChatFragment? = fm.findFragmentById(R.id.fl_main) as BuyingChatFragment?
                fragment!!.performEdit(1)
            }
        }

        if(selectedtab ==3)
        {
            if(tv_edit.text.equals(getString(R.string.txt_edit))) {
                tv_edit.setText(getString(R.string.txt_cancel))
                val fm = supportFragmentManager
                val fragment: SellingChatFragment? = fm.findFragmentById(R.id.fl_main) as SellingChatFragment?
                fragment!!.performEdit(2)
            }
            else{
                tv_edit.setText(getString(R.string.txt_edit))
                val fm = supportFragmentManager
                val fragment: SellingChatFragment? = fm.findFragmentById(R.id.fl_main) as SellingChatFragment?
                fragment!!.performEdit(1)
            }
        }
    }

    private fun setBottomIconSelected(p1: Int, p2: Int, p3: Int, p4: Int, p5: Int, p6: Int) {
        v12.visibility = p1
        tvall.setTextColor(resources.getColor(p2))

        v22.visibility = p3
        tvbuying.setTextColor(resources.getColor(p4))

        v32.visibility = p5
        tvselling.setTextColor(resources.getColor(p6))
    }

    private fun setAllTab() {

        selectedtab = 1

//        val fragment = mFragmentManager.findFragmentByTag(AllChatFragment.TAG)
       /* val transaction = mFragmentManager?.beginTransaction()
        transaction?.replace(R.id.fl_main, AllChatFragment.newInstance(), AllChatFragment.newInstance().tag)
        //transaction?.addToBackStack(AllChatFragment.newInstance().tag)
        mFragmentManager?.popBackStack(null, 0)
        transaction?.commit()*/

        val transaction = mFragmentManager?.beginTransaction()
        transaction?.replace(
            R.id.fl_main,
            AllChatFragment.newInstance(),
            AllChatFragment.newInstance().tag
        )
        //transaction?.addToBackStack(AllChatFragment.newInstance().tag)
        mFragmentManager?.popBackStack(null, 0)
        transaction?.commit()
    }

    private fun setBuyingTab() {

        selectedtab = 2

        val transaction = mFragmentManager!!.beginTransaction()
        transaction.replace(
            R.id.fl_main,
            BuyingChatFragment.newInstance(),
            BuyingChatFragment.newInstance().tag
        )
        transaction.addToBackStack(BuyingChatFragment.newInstance().tag)
        mFragmentManager!!.popBackStack(null, 0)
        transaction.commit()
    }

    private fun setSellingTab() {

        selectedtab = 3

        val transaction = mFragmentManager!!.beginTransaction()
        transaction.replace(
            R.id.fl_main,
            SellingChatFragment.newInstance(),
            SellingChatFragment.newInstance().tag
        )
        transaction.addToBackStack(SellingChatFragment.newInstance().tag)
        mFragmentManager!!.popBackStack(null, 0)
        transaction.commit()
    }

    fun changeEditButtonText() {

        tv_edit.text = getString(R.string.txt_edit)
    }

    fun loadAllTabAgain() {
        tv_edit.text = getString(R.string.txt_edit)
        setAllTab()
    }

    fun loadSellingTabAgain() {
        tv_edit.text = getString(R.string.txt_edit)
        setSellingTab()

    }

    fun loadBuyingTabAgain() {
        tv_edit.text = getString(R.string.txt_edit)
        setBuyingTab()
    }

    fun displayHomeTab() {

        val screenRedirectionController = ScreenRedirectionController(this)
        screenRedirectionController.openHomeTab()
    }

    companion object {

        private val TAG = "ChatActivity"
    }

}

