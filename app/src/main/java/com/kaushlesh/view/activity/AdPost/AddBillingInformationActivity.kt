package com.kaushlesh.view.activity.AdPost

import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.*
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.databinding.adapters.TextViewBindingAdapter
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.gson.GsonBuilder
import com.kaushlesh.Controller.AddInvoiceController
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.R
import com.kaushlesh.bean.*
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.constant.AppConstants.printLog
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import kotlinx.android.synthetic.main.activity_business_profile.*
import org.json.JSONObject
import java.util.ArrayList

class AddBillingInformationActivity : AppCompatActivity(), View.OnClickListener, ParseControllerListener {
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal lateinit var rltype: RelativeLayout
    internal lateinit var rlstate: RelativeLayout
    internal lateinit var rlcity: RelativeLayout
    internal lateinit var sptype: Spinner
    internal lateinit var spstate: Spinner
    internal lateinit var spcity: Spinner
    internal lateinit var tvtype: TextView
    internal lateinit var tvstate: TextView
    internal lateinit var tvcity: TextView
    internal lateinit var ibtype: ImageButton
    internal lateinit var ibstate: ImageButton
    internal lateinit var ibcity: ImageButton
    internal lateinit var etemail: EditText
    internal lateinit var etcustomername: EditText
    internal lateinit var etbusinessname: EditText
    internal lateinit var etgstno: EditText
    internal lateinit var etadd1: EditText
    internal lateinit var etadd2: EditText
    internal lateinit var edt_village_town: EditText
    internal lateinit var btnsave: Button
    internal lateinit var llgstno: LinearLayout
    //internal lateinit var type_spinner_type: ArrayAdapter<String>
    internal lateinit var state_spinner_type: ArrayAdapter<String>
    internal lateinit var city_spinner_type: ArrayAdapter<String>
    internal var typelistGST: MutableList<String> = ArrayList()
    internal var statelist: MutableList<String> = ArrayList()
    internal var citylist: MutableList<String> = ArrayList()

    internal lateinit var txt_address_counter: TextView

    //city locations
    lateinit var getListingController: GetListingController
    internal lateinit var placesClient: PlacesClient
    internal lateinit var type_spinner_type: ArrayAdapter<String>
    internal var typelist: MutableList<String> = java.util.ArrayList()
    internal var typeidlist: MutableList<String> = java.util.ArrayList()
    internal var locationlist: ArrayList<LocationListBeans.LocationList> = ArrayList()
    var locationid: String? = ""
    var district: String? = null
    lateinit var storeUserData: StoreUserData

    private lateinit var addInvoiceController: AddInvoiceController
    var userModel: UserProfileBean = UserProfileBean()
    lateinit var storeusedata: StoreUserData


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_billing_information)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.txt_billing_information)

        initBindViews()

        storeUserData = StoreUserData(this)
        storeusedata = StoreUserData(applicationContext)

        userModel.userId = storeusedata.getString(Constants.USER_ID)
        userModel.userToken = storeusedata.getString(Constants.TOKEN)
        addInvoiceController = AddInvoiceController(this, this)

        etadd1.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                txt_address_counter.text = s.length.toString() + "/66"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })

        //init places api
        getListingController = GetListingController(this, this)
        initPlacesAPI()

        getInvoiceDetails()
        setOnclick()
    }

    private fun setOnclick() {

    }

    private fun initPlacesAPI() {
        val apiKey = getString(R.string.google_place_api_key)
        if (apiKey.isEmpty()) {
            //responseView.setText(getString(R.string.error));
            return
        }

        // Setup Places Client
        if (!Places.isInitialized()) {
            Places.initialize(applicationContext, apiKey)
        }

        placesClient = Places.createClient(applicationContext)
        getListingController.getLocationList()

    }

    private fun initBindViews() {
        rltype = findViewById(R.id.rl_type)
        rlstate = findViewById(R.id.rl_state_type)
        rlcity = findViewById(R.id.rl_city_type)
        sptype = findViewById(R.id.spinner_type)
        spstate = findViewById(R.id.spinner_state_type)

        spcity = findViewById(R.id.spinner_district_invoice)
        tvcity = findViewById(R.id.tv_district_type_invoice)

        tvtype = findViewById(R.id.tv_type)
        tvstate = findViewById(R.id.tv_state_type)
        ibtype = findViewById(R.id.ib_type)
        ibstate = findViewById(R.id.ib_state_type)
        ibcity = findViewById(R.id.ib_city_type)
        etemail = findViewById(R.id.et_email)
        etcustomername = findViewById(R.id.et_name)
        etbusinessname = findViewById(R.id.et_business_name)
        etgstno = findViewById(R.id.et_gst_no)
        etadd1 = findViewById(R.id.et_add_lineone)
        txt_address_counter = findViewById(R.id.txt_address_counter)
        //etadd2 = findViewById(R.id.et_add_linetwo)
        btnsave = findViewById(R.id.btn_save)
        llgstno = findViewById(R.id.ll_gst_no)
        edt_village_town = findViewById(R.id.edt_village_town)

        rltype.setBackgroundResource(R.drawable.bg_edittext_black)

        rltype.setOnClickListener(this)
        ibtype.setOnClickListener(this)
        rlstate.setOnClickListener(this)
        ibstate.setOnClickListener(this)
        rlcity.setOnClickListener(this)
        ibcity.setOnClickListener(this)
        btnsave.setOnClickListener(this)
        btnback.setOnClickListener(this)

        tvcity.setOnClickListener(this)

        typelistGST.add("Yes")
        typelistGST.add("No")

        statelist.add("Gujarat")

        type_spinner_type = ArrayAdapter(
            applicationContext,
            android.R.layout.simple_spinner_dropdown_item,
                typelistGST
        )


        // Creating adapter for spinner
        val dataAdapter1 =
            ArrayAdapter(applicationContext, android.R.layout.simple_spinner_item, typelistGST)

        // Drop down layout style - list view with radio button
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        // attaching data adapter to spinner
        sptype.adapter = dataAdapter1

        state_spinner_type = ArrayAdapter(
            applicationContext,
            android.R.layout.simple_spinner_dropdown_item,
            statelist
        )


        // Creating adapter for spinner
        val dataAdapter11 =
            ArrayAdapter(applicationContext, android.R.layout.simple_spinner_item, statelist)

        // Drop down layout style - list view with radio button
        dataAdapter11.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        // attaching data adapter to spinner
        spstate.adapter = dataAdapter11


        /* citylist.add("Rajkot");
        citylist.add("Ahmedabad");
        citylist.add("Vadodara");*/


        sptype.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>,
                view: View,
                position: Int,
                l: Long
            ) {
                tvtype.text = adapterView.selectedItem.toString() + ""
                printLog(TAG, "onItemSelected: ===" + adapterView.selectedItem)

                if (adapterView.selectedItem.toString().equals("Yes", ignoreCase = true)) {
                    llgstno.visibility = View.VISIBLE
                } else {
                    llgstno.visibility = View.GONE
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        }

        /*spcity.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>,
                view: View,
                position: Int,
                l: Long
            ) {
                printLog(TAG, "onItemSelected: ===" + adapterView.selectedItem)

            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        }*/

        spstate.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>,
                view: View,
                position: Int,
                l: Long
            ) {

                printLog(TAG, "onItemSelected: ===" + adapterView.selectedItem)

                if (adapterView.selectedItem.toString().equals("Gujarat", ignoreCase = true)) {

                    citylist.clear()

                    citylist.add("Rajkot")
                    citylist.add("Ahmedabad")
                    citylist.add("Vadodara")

                    city_spinner_type = ArrayAdapter(
                        applicationContext,
                        android.R.layout.simple_spinner_dropdown_item,
                        citylist
                    )

                    // Creating adapter for spinner
                    val dataAdapter111 = ArrayAdapter(
                        applicationContext,
                        android.R.layout.simple_spinner_item,
                        citylist
                    )

                    // Drop down layout style - list view with radio button
                    dataAdapter111.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                    // attaching data adapter to spinner
                    spcity.adapter = dataAdapter111
                } else {

                    citylist.clear()

                    citylist.add("Jaipur")
                    citylist.add("Udaipur")
                    citylist.add("Jodhpur")
                    citylist.add("Ajmer")

                    city_spinner_type = ArrayAdapter(
                        applicationContext,
                        android.R.layout.simple_spinner_dropdown_item,
                        citylist
                    )

                    // Creating adapter for spinner
                    val dataAdapter111 = ArrayAdapter(
                        applicationContext,
                        android.R.layout.simple_spinner_item,
                        citylist
                    )

                    // Drop down layout style - list view with radio button
                    dataAdapter111.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                    // attaching data adapter to spinner
                    spcity.adapter = dataAdapter111
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        }

    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_back -> onBackPressed()

            R.id.btn_save -> {
                saveInvoice()

            }

            R.id.rl_type -> {

                tvtype.visibility = View.GONE
                sptype.visibility = View.VISIBLE
                sptype.performClick()
            }

            R.id.ib_type -> {

                tvtype.visibility = View.GONE
                sptype.visibility = View.VISIBLE
                rltype.performClick()
            }
            R.id.rl_state_type -> {

                tvstate.visibility = View.GONE
                spstate.visibility = View.VISIBLE
                spstate.performClick()
            }

            R.id.ib_state_type -> {

                tvstate.visibility = View.GONE
                spstate.visibility = View.VISIBLE
                rlstate.performClick()
            }


            /*R.id.rl_city_type -> {

                tvcity.visibility = View.GONE
                spcity.visibility = View.VISIBLE
                spcity.performClick()
            }

            R.id.ib_city_type -> {

                tvcity.visibility = View.GONE
                spcity.visibility = View.VISIBLE
                rlcity.performClick()
            }*/


            R.id.tv_district_type_invoice -> {
                tvcity.visibility = View.GONE
                spcity.visibility = View.VISIBLE
                spcity.performClick()

                spcity.onItemSelectedListener =
                        object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(
                                    adapterView: AdapterView<*>,
                                    view: View,
                                    position: Int,
                                    l: Long
                            ) {

                                if (position > 0) {
                                    AppConstants.printLog(
                                            TAG,
                                            "onItemSelected: ===" + adapterView.selectedItem
                                    )
                                    AppConstants.printLog(
                                            TAG,
                                            "onItemSelected: ===" + typeidlist.get(position)
                                    )
                                    locationid = typeidlist.get(position)
                                    district = adapterView.selectedItem.toString()

                                    storeUserData.setString(Constants.LOC_ID_PKG, locationid.toString())
                                    storeUserData.setString(
                                            Constants.LOCATION_ID,
                                            locationid.toString()
                                    )
                                    storeUserData.setString(
                                            Constants.LOC_NAME,
                                            adapterView.selectedItem.toString()
                                    )
                                    Utils.showLog(
                                            TAG,
                                            "====stored location id====" + locationid.toString()
                                    )

                                    rlcity.setBackgroundResource(R.drawable.bg_edittext_black)

                                    //getListingController.getLatLongOfSelectedStrict(district + "," + resources.getString(R.string.txt_gujarat))


                                    /*if (act_location.text.toString().length > 0) {
                                act_location.setText("")
                            }*/
                                } else {
                                    locationid = "0"
                                }
                            }

                            override fun onNothingSelected(adapterView: AdapterView<*>) {
                                locationid = "0"
                            }
                        }
            }




        }/* new AlertDialog.Builder(AddBillingInformationActivity.this)
                        .setAdapter(type_spinner_type, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                String selecteduser = typelist.get(which);
                                printLog(TAG, "====Selected type =====" + selecteduser);
                                tvtype.setText(selecteduser);

                                if(selecteduser.equalsIgnoreCase("Yes"))
                                {
                                    llgstno.setVisibility(View.VISIBLE);
                                }
                                else {
                                    llgstno.setVisibility(View.GONE);
                                }
                                dialog.dismiss();

                            }
                        }).create().show();
*//*new AlertDialog.Builder(AddBillingInformationActivity.this)
                        .setAdapter(state_spinner_type, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                if(tvcity.getText().toString() != null && tvcity.getText().toString().length() > 0)
                                {
                                    tvcity.setText("");
                                    //tvcity.setVisibility(View.GONE);
                                    //spcity.setVisibility(View.GONE);
                                }
                                String selecteduser = statelist.get(which);
                                printLog(TAG, "====Selected user =====" + selecteduser);
                                tvstate.setText(selecteduser);
                                dialog.dismiss();

                            }
                        }).create().show();*//* new AlertDialog.Builder(AddBillingInformationActivity.this)
                        .setAdapter(city_spinner_type, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                String selecteduser = citylist.get(which);
                                printLog(TAG, "====Selected user =====" + selecteduser);
                                tvcity.setText(selecteduser);
                                dialog.dismiss();

                            }
                        }).create().show();*/
    }

    private fun saveInvoice() {
        val invoiceRequest = InvoiceRequest()

        invoiceRequest.userId = userModel.userId
        invoiceRequest.userToken = userModel.userToken.toString()

        invoiceRequest.billing_bussiness_name = etbusinessname.text.toString()
        invoiceRequest.billing_name = etbusinessname.text.toString()
        invoiceRequest.billing_email = ""
        invoiceRequest.billing_address1 = etadd1.text.toString()
        invoiceRequest.billing_address2 = etadd1.text.toString()
        invoiceRequest.billing_state = "Gujarat"
        invoiceRequest.billing_city = edt_village_town.text.toString()
        invoiceRequest.billing_district = locationid

        if(tvtype.text.toString().equals("yes",ignoreCase = true) ){
            invoiceRequest.is_gst = 1
            invoiceRequest.billing_gst_no = etgstno.text.toString()
        }else{
            invoiceRequest.is_gst = 0
            invoiceRequest.billing_gst_no = etgstno.text.toString()
        }

        Log.d(TAG, "$invoiceRequest")

        addInvoiceController.addInvoice(invoiceRequest, userModel)




    }

    private fun getInvoiceDetails(){
        getListingController = GetListingController(this, this)
        getListingController.getInvoiceDetailData()
    }

    fun setInvoiceData(invoiceModel: InvoiceBean) {

        if(invoiceModel.billing_name!=null){
            etbusinessname.setText(invoiceModel.billing_name)
        }

        if(invoiceModel.is_gst!=null){
            if(invoiceModel.is_gst.toString() == "0"){
                sptype.setSelection(1)
            }else{
                sptype.setSelection(0)
            }
        }


        if(invoiceModel.billing_gst_no!=null){
            etgstno.setText(invoiceModel.billing_gst_no)
        }

        if(invoiceModel.billing_address1!=null){
            etadd1.setText(invoiceModel.billing_address1)
        }

        /*if(invoiceModel.billing_address2!=null){
            etadd2.setText(invoiceModel.billing_address2)
        }*/

        if(invoiceModel.billing_city!=null){
            edt_village_town.setText(invoiceModel.billing_city)
        }

        if(invoiceModel.billing_district != null && invoiceModel.billing_district.toString().length >0) {
            locationid = invoiceModel.billing_district
            tvcity.visibility = View.VISIBLE
            tvcity.text = invoiceModel.locationName
            spcity.visibility = View.GONE
        }


    }

    companion object {

        private val TAG = "AddBillingInformationActivity"
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (method.equals("GetLocation")) {
            locationlist.clear()
            typelist.clear()

            val dataModel = LocationListBeans()
            dataModel.LocationListBeans(da)

            locationlist = dataModel.locationList
            Utils.showLog(TAG, "==location list size==" + locationlist.size)
            typelist.add(resources.getText(R.string.txt_select_district_hint).toString())   //  In
            typeidlist.add("0")
            for (i in locationlist.indices) {
                val name = locationlist.get(i).locName
                val id = locationlist.get(i).locId
                typelist.add(name.toString())
                typeidlist.add(id.toString())
            }

            type_spinner_type = ArrayAdapter(
                    applicationContext,
                    android.R.layout.simple_spinner_dropdown_item,
                    typelist
            )
            val dataAdapter1 = ArrayAdapter(
                    applicationContext,
                    android.R.layout.simple_spinner_item,
                    typelist
            )
            dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

            spcity.adapter = dataAdapter1
            tvcity.visibility = View.VISIBLE
            tvcity.text = resources.getText(R.string.txt_select_district_hint)
            spcity.visibility = View.GONE
            rlcity.setBackgroundResource(R.drawable.bg_edittext)
        }

        if(method.equals("addInvoice")){
            //Log.d(TAG,"SUCCESS_RESPONSE : $da" )
            //val bussinessprofile_id = da.getString("bussinessprofile_id")
            //storeUserData.setString(Constants.BUSINESS_USER_ID, "" + bussinessprofile_id)

            Utils.showToast(this, "Invoice detail updated successfully")
        }

        if(method.equals("getInvoice")){
            val invoiceModel = InvoiceBean()
            invoiceModel.invoiceBean(da)

            Log.d(
                    "getInvoice",
                    "response : " + GsonBuilder().setPrettyPrinting().create().toJson(
                            invoiceModel
                    )
            )

            setInvoiceData(invoiceModel)


        }

    }


    override fun onFail(msg: String, method: String) {
        TODO("Not yet implemented")
    }
}
