package com.kaushlesh.view.activity

import android.Manifest
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.content.IntentSender
import android.content.SharedPreferences
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.snackbar.Snackbar
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.kaushlesh.BuildConfig
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.R
import com.kaushlesh.bean.UserProfileBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.constant.AppConstants.printLog
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.reusable.GPSTracker
import com.kaushlesh.utils.*
import com.kaushlesh.utils.PostDetail.CheckCategoryForPostDetails
import com.kaushlesh.view.activity.Business.ViewBusinessProfileActivity
import com.kaushlesh.view.activity.Login.LoginMainActivity
import com.kaushlesh.view.fragment.*
import com.kaushlesh.view.fragment.MyAccount.MyNetwork.FollowerProfileDetailActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONException
import org.json.JSONObject
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.util.*


class MainActivity : AppCompatActivity() , View.OnClickListener,ParseControllerListener{

    private var mFragmentManager: FragmentManager? = null
    var TAG = MainActivity::class.java.getSimpleName()
    lateinit var storeUserData: StoreUserData
    lateinit var getListingController: GetListingController
    lateinit var userId: String
    lateinit var timer : Timer

    //from notification
    var category_id :String?=null
    var sub_category_id :String?=null
    var post_id : String?=null

    //from deeplink
    var deepLinkUserId : String?=null
    var deepLinkBusinessProfileID : String? = null

    //version check
    var currentVersion: String? = null
    var latestVersion: String? = null
    var forceUpdate: String? = ""
    private var popup: Dialog? = null

    //in-app update
    private lateinit var appUpdateManager: AppUpdateManager
    private val IMMEDIATE_APP_UPDATE_REQ_CODE = 124
    private val FLEXIBLE_APP_UPDATE_REQ_CODE = 125

    private var installStateUpdatedListener: InstallStateUpdatedListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        //check if user is redirect from push notification
        if(intent != null){

            //redirect to chat screen when chat-push notification clicked
            if(intent.getStringExtra("openScreen") != null){
                if(intent.getStringExtra("openScreen").equals("chat")){
                    val intent1 = Intent(this@MainActivity, ChatActivity::class.java)
                    startActivity(intent1)
                }
            }




            //post redirection deeplink/notifications both

            if(intent.getStringExtra("category_id") != null &&
                    intent.getStringExtra("sub_category_id") != null &&
                    intent.getStringExtra("post_id") != null){
                category_id = intent.getStringExtra("category_id")
                sub_category_id = intent.getStringExtra("sub_category_id")
                post_id = intent.getStringExtra("post_id")

                val storeusedataPost = StoreUserData(this@MainActivity)
                storeusedataPost.setString(Constants.OPEN_POST_DETAIL, "product_list")
                storeusedataPost.setString(Constants.CATEGORY_ID, category_id.toString())
                storeusedataPost.setString(Constants.SUBCATEGORYID, sub_category_id.toString())
                storeusedataPost.setString(Constants.FROM_DETAIL, "true")
                CheckCategoryForPostDetails.OpenScreen(
                        this@MainActivity,
                        category_id!!.toInt(),
                        sub_category_id!!.toInt(),
                        post_id!!.toInt()
                )
            }

            //profile redirection deeplink
            if(intent.getStringExtra("linkType")!=null){

                if(intent.getStringExtra("linkType") == "profile"){
                    deepLinkUserId = intent.getStringExtra("userId")
                    val intent = Intent(this, FollowerProfileDetailActivity::class.java)
                    Utils.openFollowingUserDetails(this, intent, deepLinkUserId.toString())
                }

                if(intent.getStringExtra("linkType") == "businessprofile"){
                    deepLinkBusinessProfileID = intent.getStringExtra("businessProfileId")

                    val intent = Intent(this@MainActivity, ViewBusinessProfileActivity::class.java)
                    intent.putExtra("businessProfileID", deepLinkBusinessProfileID)
                    startActivity(intent)

                }


            }


        }





        storeUserData=StoreUserData(this)
        userId = storeUserData.getString(Constants.USER_ID)
        Utils.showLog(TAG, "====userid====" + userId)
        timer = Timer()
        //startRepeatingJob(1000L)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        mFragmentManager = supportFragmentManager

        initBindViews()

        getListingController = GetListingController(this, this)



        /*if(!Identity.getPermssionDialog(this@MainActivity).equals("permission_added")){
            showPermissionDialog()
        }*/







        //in-app update
        appUpdateManager = AppUpdateManagerFactory.create(applicationContext)
        installStateUpdatedListener = InstallStateUpdatedListener { state ->
            if (state.installStatus() === InstallStatus.DOWNLOADED) {
                /*Toast.makeText(
                        applicationContext,
                        "DOWNLOADEDDDD: state: " + state.installStatus(),
                        Toast.LENGTH_LONG
                ).show()*/
                popupSnackBarForCompleteUpdate()
            } else if (state.installStatus() === InstallStatus.INSTALLED) {
                /*Toast.makeText(
                        applicationContext,
                        "INSTALLEDDDD: state: " + state.installStatus(),
                        Toast.LENGTH_LONG
                ).show()*/
                removeInstallStateUpdateListener()
            } else {
                /*Toast.makeText(
                        applicationContext,
                        "InstallStateUpdatedListener: state: " + state.installStatus(),
                        Toast.LENGTH_LONG
                ).show()*/
            }
        }
        appUpdateManager.registerListener(installStateUpdatedListener)
        //checkUpdate()



        val settings: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        val lastTimeStarted: Int = settings.getInt("last_time_started", -1)
        val calendar = Calendar.getInstance()
        val today = calendar[Calendar.DAY_OF_YEAR]

        if(Identity.getRateUsDialog(this@MainActivity) != "rated"){
            if (today != lastTimeStarted) {
                showAutoPlayPopup()// show dialog once a day

                val editor: SharedPreferences.Editor = settings.edit()
                editor.putInt("last_time_started", today)
                editor.commit()
            }
        }



    }

    private fun showAutoPlayPopup() {
        Handler().postDelayed({ showRateUsDialog() }, (1000 * 60).toLong())
    }

    fun showRateUsDialog() {

        try {

            if (!isFinishing()) {
                popup = Dialog(this@MainActivity, R.style.DialogCustom)
                popup!!.setContentView(R.layout.dialog_rate_us)
                popup!!.setCanceledOnTouchOutside(false)
                popup!!.setCancelable(false)
                popup!!.show()
                val imgCloseDialog: ImageView = popup!!.findViewById(R.id.imgCloseDialog)
                val btnRateUs: Button = popup!!.findViewById(R.id.btnRateUs)


                imgCloseDialog.setOnClickListener {
                    Identity.setRateUsDialog(this@MainActivity,"not_rated");
                    popup!!.dismiss()
                }

                btnRateUs.setOnClickListener{
                    Identity.setRateUsDialog(this@MainActivity,"rated");

                    popup!!.dismiss()


                    try {
                        val viewIntent = Intent(
                                "android.intent.action.VIEW",
                                Uri.parse("https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}")
                        )
                        startActivity(viewIntent)
                    } catch (e: java.lang.Exception) {
                        //Utils.showToast(context, "Unable to Connect Try Again...")
                        e.printStackTrace()
                    }

                }
            }

        }catch (e: java.lang.Exception){
            e.printStackTrace()
        }

    }

    //immediate update
    private fun checkUpdateImmediate() {
        val appUpdateInfoTask = appUpdateManager.appUpdateInfo
        appUpdateInfoTask.addOnSuccessListener { appUpdateInfo: AppUpdateInfo ->
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)
            ) {
                startUpdateFlowImmediate(appUpdateInfo)
            } else if (appUpdateInfo.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                startUpdateFlowImmediate(appUpdateInfo)
            }
        }
    }

    //flexible update
    private fun checkUpdateFlexible() {
        val appUpdateInfoTask = appUpdateManager.appUpdateInfo
        appUpdateInfoTask.addOnSuccessListener { appUpdateInfo: AppUpdateInfo ->
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)
            ) {
                Log.d("update_type", "flexible")
                startUpdateFlowFlexible(appUpdateInfo)
            } else if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                Log.d("update_type", "downloaded")
                popupSnackBarForCompleteUpdate()
            } else {
                Log.d("update_type", "other")
            }
        }
    }


    private fun popupSnackBarForCompleteUpdate() {


        //new
        Snackbar.make(
                findViewById<View>(android.R.id.content).rootView,
                "New app is ready!",
                Snackbar.LENGTH_INDEFINITE
        )
            .setAction("Install") { view: View? ->
                appUpdateManager.completeUpdate()
            }
            .setActionTextColor(resources.getColor(R.color.red))
            .show()

        //original
        /*Snackbar.make(
            findViewById(android.R.id.content).getRootView(),
            "New app is ready!",
            Snackbar.LENGTH_INDEFINITE
        )
            .setAction("Install") { view: View? ->
                appUpdateManager.completeUpdate()
            }
            .setActionTextColor(resources.getColor(R.color.blue_color))
            .show()*/
    }

    //flexible flow
    private fun startUpdateFlowFlexible(appUpdateInfo: AppUpdateInfo) {
        try {
            appUpdateManager.startUpdateFlowForResult(
                    appUpdateInfo,
                    AppUpdateType.FLEXIBLE,
                    this,
                    FLEXIBLE_APP_UPDATE_REQ_CODE
            )
        } catch (e: IntentSender.SendIntentException) {
            Log.d("update_type", "error : " + e.localizedMessage)
            e.printStackTrace()
        }
    }

    //immediate flow
    private fun startUpdateFlowImmediate(appUpdateInfo: AppUpdateInfo) {
        try {
            appUpdateManager.startUpdateFlowForResult(
                    appUpdateInfo,
                    AppUpdateType.IMMEDIATE,
                    this,
                    IMMEDIATE_APP_UPDATE_REQ_CODE
            )
        } catch (e: IntentSender.SendIntentException) {
            e.printStackTrace()
        }
    }

    private fun removeInstallStateUpdateListener() {
        appUpdateManager.unregisterListener(installStateUpdatedListener)
    }



    private fun initBindViews() {

        ll_home.setOnClickListener(this)
        ll_adshowcase.setOnClickListener(this)
        ll_postad.setOnClickListener(this)
        iv_postadd.setOnClickListener(this)
        ll_talentzone.setOnClickListener(this)
        ll_account.setOnClickListener(this)
        ll_myads.setOnClickListener(this)
        ll_mynetwork.setOnClickListener(this)

        homeTabDisplay()

        if(intent.extras != null)
        {
            val code = intent.getIntExtra("code", 0)
            setdata(code)
        }
    }

    override fun onClick(view: View?) {
        when (view!!.getId()) {
            R.id.ll_home -> {
                //if (tv_home.getCurrentTextColor() != resources.getColor(R.color.orangedark))
                setHomeTab()
            }

            R.id.ll_myads -> {
                printLog(TAG, "===my ads clicked===")
                if (AppConstants.checkUserIsLoggedin(this)) {
                    if (tv_myads.getCurrentTextColor() != resources.getColor(R.color.orangedark))
                        setMyAds()
                } else {
                    val intent = Intent(applicationContext, LoginMainActivity::class.java)
                    AppConstants.alertLogin(this, getString(R.string.txt_myads_login), intent)
                }
            }
            R.id.ll_adshowcase -> {

                //setAdShowcaseTab()
                if (AppConstants.checkUserIsLoggedin(this)) {
                    if (tv_addshowcase.getCurrentTextColor() != resources.getColor(R.color.orangedark))
                        setAdShowcaseTab()
                } else {
                    val intent = Intent(applicationContext, LoginMainActivity::class.java)
                    AppConstants.alertLogin(this, getString(R.string.txt_showcase_login), intent)
                }
            }

            R.id.iv_postadd -> {

                //setPostAdTab()

                if (AppConstants.checkUserIsLoggedin(this)) {
                    if (tv_postadd.getCurrentTextColor() != resources.getColor(R.color.orangedark))
                        setPostAdTab()
                } else {
                    val intent = Intent(applicationContext, LoginMainActivity::class.java)
                    AppConstants.alertLogin(this, getString(R.string.txt_ad_post_login), intent)
                }
            }

            R.id.ll_postad -> {

                if (AppConstants.checkUserIsLoggedin(this)) {
                    if (tv_postadd.getCurrentTextColor() != resources.getColor(R.color.orangedark))
                        setPostAdTab()
                } else {
                    val intent = Intent(applicationContext, LoginMainActivity::class.java)
                    AppConstants.alertLogin(this, getString(R.string.txt_ad_post_login), intent)
                }
                //setPostAdTab()
            }

            R.id.ll_talentzone -> {
                if (tv_talentzone.getCurrentTextColor() != resources.getColor(R.color.orangedark))
                    setTalentZoneTab()
            }

            R.id.ll_account -> {

                if (AppConstants.checkUserIsLoggedin(this)) {
                    if (tv_account.getCurrentTextColor() != resources.getColor(R.color.orangedark))
                        setAccountTab()
                } else {
                    val intent = Intent(applicationContext, LoginMainActivity::class.java)
                    AppConstants.alertLogin(
                            this,
                            getString(R.string.txt_login_access_account),
                            intent
                    )
                }
                //setAccountTab()
            }

            R.id.ll_mynetwork -> {

                if (AppConstants.checkUserIsLoggedin(this)) {
                    if (tv_mynetwork.getCurrentTextColor() != resources.getColor(R.color.orangedark))
                        setMyNetwork()
                } else {
                    val intent = Intent(applicationContext, LoginMainActivity::class.java)
                    AppConstants.alertLogin(this, getString(R.string.txt_mynetwork_login), intent)
                }
            }
        }
    }


    fun homeTabDisplay() {
        setBottomIconSelected(
                R.drawable.ic_home_select,
                R.color.orangedark,
                /* R.drawable.ic_sjowcase_unselect,*/
                R.drawable.ic_ads_grey,
                R.color.black,
                R.drawable.ic_addpost,
                R.color.black,
                R.drawable.ic_talentzone_unselect,
                R.color.black,
                R.drawable.ic_account_unselect,
                R.color.black
        )

        setHomeTab()
    }

    private fun setBottomIconSelected(
            p: Int,
            p2: Int,
            p3: Int,
            p4: Int,
            p5: Int,
            p6: Int,
            p7: Int,
            p8: Int,
            p9: Int,
            p10: Int
    ) {
        iv_home.setImageResource(p)
        tv_home.setTextColor(resources.getColor(p2))

        /*iv_addshowcase.setImageResource(p3)
        tv_addshowcase.setTextColor(resources.getColor(p4))*/
        iv_myads.setImageResource(p3)
        tv_myads.setTextColor(resources.getColor(p4))

        iv_postadd.setImageResource(p5)
        tv_postadd.setTextColor(resources.getColor(p6))

       /* iv_talentzone.setImageResource(p7)
        tv_talentzone.setTextColor(resources.getColor(p8))*/
        iv_mynetwork.setImageResource(p7)
        tv_mynetwork.setTextColor(resources.getColor(p8))

        iv_account.setImageResource(p9)
        tv_account.setTextColor(resources.getColor(p10))
    }

    public fun changeFragment(fragment: Fragment, tag: String?, args: Bundle?, add: Boolean) {

        val ft: FragmentTransaction
        ft = if (add) {
            supportFragmentManager.beginTransaction().addToBackStack(tag)
        } else supportFragmentManager.beginTransaction()
        if (args != null) fragment.arguments = args
        ft.replace(R.id.fl_main, fragment, tag).commitAllowingStateLoss()

    }

    private fun setHomeTab() {
        val storeusedata = StoreUserData(this)
        storeusedata.setString(Constants.FROM_DETAIL, "false")
        setBottomIconSelected(
                R.drawable.ic_home_select,
                R.color.orangedark,
                /* R.drawable.ic_sjowcase_unselect,*/
                R.drawable.ic_ads_grey,
                R.color.black,
                R.drawable.ic_addpost,
                R.color.black,
                /*   R.drawable.ic_talentzone_unselect,*/
                R.drawable.ic_mynetwork_grey,
                R.color.black,
                R.drawable.ic_account_unselect,
                R.color.black
        )

        changeFragment(HomeFragmentNew.newInstance(), HomeFragmentNew.TAG, null, false)
    }

    fun takepermission() {

        Dexter.withContext(this)
                .withPermissions(
                        Manifest.permission.INTERNET,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                ).withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(multiplePermissionsReport: MultiplePermissionsReport) {
                        if (multiplePermissionsReport.areAllPermissionsGranted()) {
                            Utils.showLog(HomeFragmentNew.TAG, "===permission checked====")
                            //getUserLocation()
                            // checkLocation()
                            return
                        }
                        if (multiplePermissionsReport.isAnyPermissionPermanentlyDenied) {
                            //showSettingsDialog()
                            val gpsTracker = GPSTracker(this@MainActivity)
                            gpsTracker.showSettingsAlert()
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(
                            list: List<PermissionRequest>,
                            permissionToken: PermissionToken
                    ) {
                    }
                }).check()
    }

    private fun setMyAds() {
        setBottomIconSelected(
                R.drawable.ic_home_unselect,
                R.color.black,
                R.drawable.ic_ads,
                R.color.orangedark,
                R.drawable.ic_addpost,
                R.color.black,
                /*  R.drawable.ic_talentzone_unselect,*/
                R.drawable.ic_mynetwork_grey,
                R.color.black,
                R.drawable.ic_account_unselect,
                R.color.black
        )


        val bundle = Bundle()
        bundle.putString("from", "myads")
        changeFragment(MyAdsFragment.newInstance(), MyAdsFragment.TAG, bundle, true)
    }


    private fun setMyNetwork() {

        setBottomIconSelected(
                R.drawable.ic_home_unselect,
                R.color.black,
                R.drawable.ic_ads_grey,
                R.color.black,
                R.drawable.ic_addpost,
                R.color.black,
                R.drawable.ic_mynetwork,
                R.color.orangedark,
                R.drawable.ic_account_unselect,
                R.color.black
        )


        //val bundle = Bundle()
        //bundle.putString("from", "myads")
        changeFragment(MyNetworkFragment.newInstance(), MyNetworkFragment.TAG, null, true)
    }


    fun setAdShowcaseTab() {

        setBottomIconSelected(
                R.drawable.ic_home_unselect,
                R.color.black,
                R.drawable.ic_showcase_select,
                R.color.orangedark,
                R.drawable.ic_addpost,
                R.color.black,
                /* R.drawable.ic_talentzone_unselect,*/
                R.drawable.ic_mynetwork_grey,
                R.color.black,
                R.drawable.ic_account_unselect,
                R.color.black
        )


        val bundle = Bundle()
        bundle.putString("from", "adshowcase")
        changeFragment(AdShowcaseFragment.newInstance(), AdShowcaseFragment.TAG, null, true)
    }

    private fun setTalentZoneTab() {

        setBottomIconSelected(
                R.drawable.ic_home_unselect,
                R.color.black,
                /* R.drawable.ic_sjowcase_unselect,*/
                R.drawable.ic_ads_grey,
                R.color.black,
                R.drawable.ic_addpost,
                R.color.black,
                /* R.drawable.ic_talentzone_select,*/
                R.drawable.ic_mynetwork,
                R.color.orangedark,
                R.drawable.ic_account_unselect,
                R.color.black
        )

        changeFragment(TalentZoneFragment.newInstance(), TalentZoneFragment.TAG, null, true)

    }

    private fun setAccountTab() {

        setBottomIconSelected(
                R.drawable.ic_home_unselect,
                R.color.black,
                /* R.drawable.ic_sjowcase_unselect,*/
                R.drawable.ic_ads_grey,
                R.color.black,
                R.drawable.ic_addpost,
                R.color.black,
                /*  R.drawable.ic_talentzone_unselect,*/
                R.drawable.ic_mynetwork_grey,
                R.color.black,
                R.drawable.ic_account_select,
                R.color.orangedark
        )

        changeFragment(MyAccountFragment.newInstance(), MyAccountFragment.TAG, null, true)

    }


    fun setPostAdTab() {

        setBottomIconSelected(
                R.drawable.ic_home_unselect,
                R.color.black,
                /* R.drawable.ic_sjowcase_unselect,*/
                R.drawable.ic_ads_grey,
                R.color.black,
                R.drawable.ic_addpost,
                R.color.orangedark,
                /* R.drawable.ic_talentzone_unselect,*/
                R.drawable.ic_mynetwork_grey,
                R.color.black,
                R.drawable.ic_account_unselect,
                R.color.black
        )

        val bundle = Bundle()
        bundle.putString("from", "adpost")
        changeFragment(AllCategoryFragment.newInstance(), AllCategoryFragment.TAG, bundle, true)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        printLog(TAG, "==result code==" + resultCode)

        if (requestCode == IMMEDIATE_APP_UPDATE_REQ_CODE || requestCode == FLEXIBLE_APP_UPDATE_REQ_CODE) {
            if (resultCode == RESULT_CANCELED) {
                //Toast.makeText(getApplicationContext(), "Update canceled by user! Result Code: " + resultCode, Toast.LENGTH_LONG).show();
                //finishAffinity()
            } else if (resultCode == RESULT_OK) {
                //Toast.makeText(getApplicationContext(),"Update success! Result Code: " + resultCode, Toast.LENGTH_LONG).show();
            } else {
                //Toast.makeText(getApplicationContext(), "Update Failed! Result Code: " + resultCode, Toast.LENGTH_LONG).show();

                if(forceUpdate.equals("true")){
                    checkUpdateImmediate()
                }else{
                    checkUpdateFlexible()
                }
            }
        }

    }

    private fun setdata(code: Int) {
        printLog(TAG, "==result code==" + code)

        try {
            if (code == 9) {
                setAdShowcaseTab()
            }

            if (code == 8) {
                setPostAdTab()
            }

            if (code == 7) {
                setHomeTab()
            }
        }
        catch (e: NullPointerException)
        {
            e.printStackTrace()
        }
    }

    override fun onResume() {
        super.onResume()
        if (AppConstants.checkUserIsLoggedin(this)) {
            val getListingController = GetListingController(this, this)
            getListingController.getUserProfile()


            //Maintanance mode
            getListingController.getMaintananceMode()

        }

    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if(method.equals("profile")) {
            val userModel = UserProfileBean()
            userModel.UserProfileBean(da)
            Utils.showLog(TAG, "=====block status====" + userModel.isBlock.toString())
            storeUserData.setString(Constants.USER_BLOCK, userModel.isBlock.toString())

            storeUserData.setString(Constants.BUSINESS_USER_ID, "" + userModel.bussinessprofile_id)

            storeUserData.setString(Constants.NAME, "" + userModel.fname)
            storeUserData.setString(Constants.CONTACT_NO, "" + userModel.phone)
            storeUserData.setString(Constants.EMAIL, "" + userModel.email)



            if(userModel.isBlock.equals("1"))
            {
                val intent = Intent(this, LoginMainActivity::class.java)
                AppConstants.alertBlock(
                        this,
                        getString(R.string.txt_account_blocked),
                        intent
                )

                storeUserData.setString(Constants.USER_ID, "")
                storeUserData.setString(Constants.TOKEN, "")
                storeUserData.setString(Constants.IS_LOGGED_IN, "")
                storeUserData.setString(Constants.IS_OTP_VERIFED, "")
                storeUserData.setString(Constants.IS_ACTIVE, "")
                storeUserData.setString(Constants.DEVICE_TOKEN, "")
                storeUserData.setString(Constants.DEVICE_TYPE, "")
                storeUserData.setString(Constants.REGISTREATION_TYPE, "")



            }
        }

        if(method.equals("block"))
        {
            Utils.showLog(TAG, "==success==" + message)
        }

        if(method.equals("maintananceMode")) {
            if (da.length() > 0) {
                Log.e("maintananceMode", "$da==")
                try {

                    var maintananceObject: JSONObject? = null
                    if (da.has("result") && da.get("result") is JSONObject) {
                        maintananceObject = da.getJSONObject("result")
                    }
                    if (maintananceObject != null) {
                        if(maintananceObject.has("maintananceMode")){
                            val maintananceMode = maintananceObject.get("maintananceMode").toString()

                            if(maintananceMode.toString() == "1"){
                                if(maintananceObject.has("maintananceModeMessage")){
                                    val intent = Intent(
                                            this@MainActivity,
                                            MaintananceActivity::class.java
                                    )
                                    intent.putExtra(
                                            "maintananceModeMessage",
                                            maintananceObject.get("maintananceModeMessage").toString()
                                    )
                                    startActivity(intent)
                                    finish()
                                }else{
                                    val intent = Intent(
                                            this@MainActivity,
                                            MaintananceActivity::class.java
                                    )
                                    startActivity(intent)
                                    finish()
                                }
                            }
                        }

                        if(maintananceObject.has("androidForceUpdate")){
                            val androidForceUpdate = maintananceObject.get("androidForceUpdate").toString()
                            if(androidForceUpdate.toString() == "1"){
                                //Force UPDATE
                                forceUpdate = "true"
                            }else{
                                forceUpdate = "false"
                            }

                            //new way to update the app using In-App Update
                            if(forceUpdate.equals("true")){
                                checkUpdateImmediate()
                            }else{
                                checkUpdateFlexible()
                            }



                            //old way to update the app
                            //getCurrentVersion()

                        }

                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }

    }

    override fun onFail(msg: String, method: String) {
        Utils.showLog(TAG, "==fail==" + msg)

        timer.cancel()

        val intent = Intent(this, LoginMainActivity::class.java)
        AppConstants.alertBlock(this, getString(R.string.txt_account_blocked), intent)
    }

    /* @InternalCoroutinesApi
     private fun startRepeatingJob(timeInterval: Long): Job {
         return CoroutineScope(Dispatchers.Default).launch {
             while (NonCancellable.isActive) {
                 // add your task here
                 Utils.showLog(TAG,"=====call api========")
                 delay(timeInterval)
             }
         }
     }*/




    private fun getCurrentVersion() {
        val pm: PackageManager = this.packageManager
        var pInfo: PackageInfo? = null
        try {
            pInfo = pm.getPackageInfo(this.packageName, 0)
        } catch (e1: PackageManager.NameNotFoundException) {
            // TODO Auto-generated catch block
            e1.printStackTrace()
        }
        currentVersion = pInfo!!.versionName

        Log.d("VERSION_CHECK", "=== Current version===$currentVersion")


        GetLatestVersion().execute()
    }

    inner class GetLatestVersion : AsyncTask<String?, String?, JSONObject>() {
        private val progressDialog: ProgressDialog? = null
        override fun onPreExecute() {
            super.onPreExecute()
        }

        override fun onPostExecute(jsonObject: JSONObject) {
            if (latestVersion != null) {
                if (!currentVersion.equals(latestVersion)) {
                    if (!isFinishing()) { //This would help to prevent Error : BinderProxy@45d459c0 is not valid; is your activity running? error
                        Log.d("VERSION_CHECK", "UPDATE THE APP")

                        if(!Identity.getUpdateDialog(this@MainActivity).equals("update_later")){
                            showUpdateDialog()
                        }


                    }
                } else {
                    Log.d("VERSION_CHECK", "CONTINUE OPEN APP")
                }
            } else{
                //background.start();
                super.onPostExecute(jsonObject)
            }

        }

        override fun doInBackground(vararg p0: String?): JSONObject {
            try {
                //It retrieves the latest version by scraping the content of current version from play store at runtime

                val doc: Document = Jsoup.connect("https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}&hl=en_IN&gl=US").get()
//                val doc: Document = Jsoup.connect("https://play.google.com/store/apps/details?id=com.cutnbrush&hl=en_IN&gl=US").get()




                latestVersion = doc.getElementsByClass("htlgb").get(6).text()
                Log.d("VERSION_CHECK", "=== latest version===$latestVersion")
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return JSONObject()
        }
    }


















    fun showUpdateDialog() {
        if (!isFinishing) {
            popup = Dialog(this, R.style.DialogCustom)
            popup!!.setContentView(R.layout.dialog_update_app)
            popup!!.setCanceledOnTouchOutside(false)
            popup!!.setCancelable(false)
            popup!!.show()

            val noThanks: TextView = popup!!.findViewById(R.id.txtNoThanks)
            val updateButton: Button = popup!!.findViewById(R.id.btnUpdate)

            if(forceUpdate.equals("true")){
                noThanks.visibility = View.GONE
            }else{
                noThanks.visibility = View.VISIBLE
            }


            noThanks.setOnClickListener {
                Identity.setUpdateDialog(this, "update_later")
                popup!!.dismiss()
            }

            updateButton.setOnClickListener{
                popup!!.dismiss()

                try {
                    val viewIntent = Intent(
                            "android.intent.action.VIEW",
                            Uri.parse("https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}")
//                        Uri.parse("https://play.google.com/store/apps/details?id=com.cutnbrush")


                    )
                    startActivity(viewIntent)
                } catch (e: java.lang.Exception) {
                    Utils.showToast(this, "Unable to Connect Try Again...")
                    e.printStackTrace()
                }

            }

        }
    }


    fun showPermissionDialog() {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_location_permission)
        dialog.setTitle("")
        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val btn_no = dialog.findViewById<TextView>(R.id.btn_no)
        val btn_yes = dialog.findViewById<Button>(R.id.btn_yes)


        btn_no.setOnClickListener(View.OnClickListener {
            dialog.dismiss()

        })

        btn_yes.setOnClickListener(View.OnClickListener {
            Identity.setPermssionDialog(this, "permission_added")
            dialog.dismiss()

        })

        dialog.show()
    }



}
