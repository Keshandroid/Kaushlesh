package com.kaushlesh.view.activity.productDetails.Property

import android.Manifest
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.CompoundButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.GsonBuilder
import com.kaushlesh.Controller.AddFavPostController
import com.kaushlesh.Controller.AddPostViewrController
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.Controller.PhoneCallController
import com.kaushlesh.R
import com.kaushlesh.adapter.RelaventPostAdapter
import com.kaushlesh.adapter.SliderAdapterExample
import com.kaushlesh.bean.AdvertisementBean
import com.kaushlesh.bean.PostDetails.postDetailsBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.PostDetail.GetDetailsFromValue
import com.kaushlesh.utils.PostDetail.SetPostDetailData
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.ChatDetailActivity
import com.kaushlesh.view.activity.Login.LoginMainActivity
import com.kaushlesh.view.activity.productDetails.ReportAdActivity
import com.kaushlesh.view.fragment.MyAccount.MyNetwork.FollowerProfileDetailActivity
import com.kaushlesh.widgets.CustomButton
import com.smarteist.autoimageslider.SliderView
import kotlinx.android.synthetic.main.activity_officeand_shop_sell_details.*
import kotlinx.android.synthetic.main.activity_party_plot_details.*
import kotlinx.android.synthetic.main.common_social_layout.btn_call
import kotlinx.android.synthetic.main.common_social_layout.rlWhatsappCall

import kotlinx.android.synthetic.main.common_social_layout.btn_chat
import kotlinx.android.synthetic.main.activity_party_plot_details.scroll
import kotlinx.android.synthetic.main.activity_party_plot_details.tv_land_area
import kotlinx.android.synthetic.main.activity_party_plot_details.tv_langth
import kotlinx.android.synthetic.main.activity_party_plot_details.tv_listby
import kotlinx.android.synthetic.main.activity_party_plot_details.tv_purpose
import kotlinx.android.synthetic.main.activity_party_plot_details.tv_washroom
import kotlinx.android.synthetic.main.activity_party_plot_details.tv_wide
import kotlinx.android.synthetic.main.activity_product_detail.*
import kotlinx.android.synthetic.main.comman_product_details_bottom.*
import kotlinx.android.synthetic.main.comman_product_details_pager.*
import kotlinx.android.synthetic.main.toolbar_with_menu.*
import org.json.JSONException
import org.json.JSONObject
import java.util.ArrayList

class PartyPlotDetailsActivity : AppCompatActivity(), RelaventPostAdapter.ItemClickListener,
    ParseControllerListener, View.OnClickListener {

    val TAG = "PartyPlotDetailsActivity"
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    var sliderView: SliderView? = null
    private var adapter: SliderAdapterExample? = null
    lateinit var post_id: String
    lateinit var subcatid: String
    lateinit var getListingController: GetListingController
    val dataModel = postDetailsBean()
    var previewType: String = ""
    lateinit var storeUserData: StoreUserData
    var contact: String = ""

    //like/dislike broadcast
    protected var localBroadcastManager: LocalBroadcastManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_party_plot_details)

        localBroadcastManager = LocalBroadcastManager.getInstance(this)


        storeUserData=StoreUserData(this)
        previewType = storeUserData.getString(Constants.OPEN_POST_DETAIL)
        Utils.showLog(TAG,"==preview Type==" + previewType)

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)
        //  tvtoolbartitle.text = resources.getText(R.string.txt_general_project_detail)
        btnback.setOnClickListener(this)
        tv_aboutAd.setOnClickListener(this)

        iv_delete.setOnClickListener(this)
        btn_call.setOnClickListener(this)
        rlWhatsappCall.setOnClickListener(this)
        llAdOwnerProfile.setOnClickListener(this)
        btn_chat.setOnClickListener(this)
        iv_social_share.setOnClickListener(this)

        if (intent != null) {
            post_id = intent.getStringExtra("postid").toString()
            subcatid = intent.getStringExtra("subCatId").toString()
            Utils.showLog("test","==post id==" + post_id + "==sub cat id==" + subcatid)
        }

        getListingController = GetListingController(this, this)

        getListingController.getProductDetail(post_id)

        AddPostViewrController(this,post_id)

        sliderView = findViewById(R.id.imageSlider);
        iv_fav.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                //broadcast
                if(buttonView!!.isPressed){
                    setFavUnfav()
                    if(isChecked){
                        notifyHomePostLikeDislike(true,post_id)
                    }else{
                        notifyHomePostLikeDislike(false,post_id)
                    }
                }
            }
        })



    }

    private fun setDetailAdvertisments(adsList: java.util.ArrayList<AdvertisementBean.Advertisement>) {
        cardDetailAds.visibility = View.VISIBLE
        Glide.with(applicationContext)
                .load(adsList.get(0).appbanner_img)
                .error(R.drawable.bg_img_placeholder)
                .placeholder(R.drawable.progress_animated_home_banner)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .skipMemoryCache(true)
                .into(detailAdImage)
        detailAdTitle.setText(adsList.get(0).appbanner_title)

        detailAdImage.setOnClickListener{
            showBottomsheetMoreOptions(this, adsList.get(0))

        }
    }

    private fun showBottomsheetMoreOptions(
            context: Context,
            advertisement: AdvertisementBean.Advertisement
    ) {

        var whatsappAds: String = ""

        val bottomSheetDialog = BottomSheetDialog(context)
        bottomSheetDialog.setContentView(R.layout.bottom_sheet_advertisement)


        val llWebsite: LinearLayout? = bottomSheetDialog.findViewById(R.id.llWebsite)
        val llCall: LinearLayout? = bottomSheetDialog.findViewById(R.id.llCall)
        val llWhatsapp: LinearLayout? = bottomSheetDialog.findViewById(R.id.llWhatsapp)

        if(advertisement.appbanner_link!=null && advertisement.appbanner_link != ""){
            llWebsite!!.visibility = View.VISIBLE
        }else{
            llWebsite!!.visibility = View.GONE
        }

        if(advertisement.whatsapp_no!=null && advertisement.whatsapp_no != ""){
            whatsappAds = advertisement.whatsapp_no.toString()
            llWhatsapp!!.visibility = View.VISIBLE
        }else{
            llWhatsapp!!.visibility = View.GONE
        }

        if(advertisement.mobile_no!=null && advertisement.mobile_no != ""){
            contact = advertisement.mobile_no.toString()
            llCall!!.visibility = View.VISIBLE
        }else{
            llCall!!.visibility = View.GONE
        }


        llWebsite!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {

                val httpIntent = Intent(Intent.ACTION_VIEW)
                httpIntent.data = Uri.parse(advertisement.appbanner_link.toString())
                startActivity(httpIntent)

                bottomSheetDialog.dismiss()

            }

        })

        llCall!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {

                if (AppConstants.checkUserIsLoggedin(this@PartyPlotDetailsActivity)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(arrayOf(Manifest.permission.CALL_PHONE), 200)
                    }
                } else {
                    val intent = Intent(this@PartyPlotDetailsActivity, LoginMainActivity::class.java)
                    AppConstants.alertLogin(
                            this@PartyPlotDetailsActivity,
                            getString(R.string.txt_login_to_call),
                            intent
                    )
                }

                bottomSheetDialog.dismiss()
            }

        })

        llWhatsapp!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {

                var appPackage = ""
                if (SetPostDetailData.isAppInstalled(this@PartyPlotDetailsActivity, "com.whatsapp")) {
                    appPackage = "com.whatsapp"
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data =
                            Uri.parse("http://api.whatsapp.com/send?phone=+91" + whatsappAds + "&text=")
                    startActivity(intent)
                } else if (SetPostDetailData.isAppInstalled(this@PartyPlotDetailsActivity, "com.whatsapp.w4b")) {
                    appPackage = "com.whatsapp.w4b"
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data =
                            Uri.parse("http://api.whatsapp.com/send?phone=+91" + whatsappAds + "&text=")
                    startActivity(intent)
                } else {
                    Utils.showToast(this@PartyPlotDetailsActivity, "Whatsapp not installed on your device")
                }

                bottomSheetDialog.dismiss()
            }

        })

        bottomSheetDialog.show()
    }

    protected fun notifyHomePostLikeDislike(isLike: Boolean, postId: String) {
        val intent: Intent = Intent("NOTIFY_POST_LIKE_DISLIKE")
        intent.putExtra("islikedState", ""+isLike)
        intent.putExtra("postIdLikeDislike",""+postId)
        localBroadcastManager?.sendBroadcast(intent)
    }

    fun setFavUnfav(){
        if(AppConstants.checkUserIsLoggedin(this)) {
            val c = AddFavPostController(this, this, post_id.toString())
            c.onClick(iv_fav)
        }
        else{
            val intent = Intent(applicationContext, LoginMainActivity::class.java)
            AppConstants.alertLogin(this, getString(R.string.txt_login_ad_fav), intent)
        }
    }


    override fun onSuccess(da: JSONObject, message: String, method: String) {

        if (method.equals("postDetailAds")) {
            if (da.length() > 0) {
                Log.e("postDetailAds", "$da==")
                try {
                    val dataModel = AdvertisementBean()
                    dataModel.AdvertisementBean(da)
                    Log.e(
                        "DETAIL_ADS",
                        "" + GsonBuilder().setPrettyPrinting().create().toJson(dataModel)
                    )
                    if(!dataModel.adsList.isEmpty()){
                        setDetailAdvertisments(dataModel.adsList)
                    }else{
                        cardDetailAds.visibility = View.GONE
                    }
                } catch (e: JSONException) {
                    cardDetailAds.visibility = View.GONE
                    e.printStackTrace()
                }
            }
        }

        if (method.equals("Get PostDetails")) {

            Utils.dismissProgress()

            scroll.fullScroll(View.FOCUS_UP)

            dataModel.postDetailsBean(da)

            Adddata(dataModel)

            //detail page advertisments
            getListingController.getPostDetailAdvertisements(dataModel.category_id.toString())

        }

        if (method.equals("adFavPost")) {
            Utils.showToast(this,message)
            //SetPostDetailData.setFav(iv_fav,message.toString()!!)

            Utils.showLog(TAG,"==is favourite ==" + dataModel.isFavourite)


        }

        if(method.equals("deleteMyPost"))
        {
            Utils.showToast(this,message)
            onBackPressed()
        }

    }

    override fun onFail(msg: String, method: String) {
        if(method.equals("Get PostDetails") || method.equals("adFavPost") || method.equals("deleteMyPost")) {
            Utils.showToast(this, msg)
        }
    }

    private fun Adddata(dataModel: postDetailsBean) {

        contact = dataModel.contactNo.toString()

        if(dataModel.bussinessprofile_verified_status != null){
            if(dataModel.bussinessprofile_verified_status.toString() == "1"){
                rlviewBusinessprfl.visibility = View.VISIBLE
            }else{
                rlviewBusinessprfl.visibility = View.GONE
            }
        }

        tv_title.text=dataModel.title
        tv_post_date.text=getString(R.string.txt_post_date)+ " "+ dataModel.published_date
        tv_price.text="₹ "+dataModel.price.toString()
        GetDetailsFromValue.setAboutInfo(dataModel.aboutInfo.toString(),tv_about_info)

        tv_plot_name.text = dataModel.party_plot_name.toString()
        tv_purpose.text = GetDetailsFromValue.getPurpose(dataModel.purpose.toString().toInt())
        tv_guest_capacity.text = GetDetailsFromValue.checkValue(dataModel.guest_capacity.toString().toInt())
        tv_listby.text=GetDetailsFromValue.getListedBy(dataModel.listed_by.toString().toInt())
        tv_land_area.text=dataModel.plot_area.toString()
        tv_langth.text=GetDetailsFromValue.checkValue(dataModel.length.toString().toInt())
        //legth ??
        tv_wide.text = GetDetailsFromValue.checkValue(dataModel.width.toString().toInt())
        tv_washroom.text = GetDetailsFromValue.getYesNo(dataModel.wash_room_available.toString().toInt())
        tv_kitchen.text = GetDetailsFromValue.getYesNo(dataModel.kitchen_available.toString().toInt())
        tv_desc.text=dataModel.other_information.toString()
        tv_guestroom_available.text = GetDetailsFromValue.getYesNo(dataModel.guest_room_available.toString().toInt())

        tv_ad_location.text = dataModel.address

        if(dataModel.guest_room_available.toString().toInt() == 1)
        {
            ll_no_of_guestroom.visibility = View.VISIBLE
            view_guest.visibility = View.VISIBLE
            tv_room.text = GetDetailsFromValue.checkValue(dataModel.no_of_guest_room.toString().toInt())
        }
        else{
            ll_no_of_guestroom.visibility = View.GONE
            view_guest.visibility = View.GONE
        }

        Glide.with(applicationContext)
            .load(dataModel.profilePicture)
            .placeholder(R.drawable.ic_user_pic)
            .into(owner_profile_image)

        //tv_owner_name.text= dataModel.userName
        GetDetailsFromValue.setUnderlineOnName(dataModel.userName,tv_owner_name)

        tv_date_act.text=getString(R.string.txt_since)+" "+ dataModel.signUpDate
        tv_add_id.text = getString(R.string.txt_ad_id)+" "+ dataModel.ad_id

        adapter = SliderAdapterExample(this,dataModel.postImages);
        SetPostDetailData.setSlider(this,dataModel.postImages, adapter!!,sliderView!!,tv_count)

        SetPostDetailData.setRetaedArray(this,rv_recom,dataModel.relatedArray,this)

        if(mapView!=null){
            SetPostDetailData.setmapdata(supportFragmentManager,mapView, dataModel.latitude.toString(),dataModel.longitude.toString())
        }

        if(dataModel.isFavourite!!.toInt() == 1) {
            iv_fav.isChecked = true
        } else{
            iv_fav.isChecked = false
        }

        if(dataModel.userId != null && dataModel.userId.toString().length >0 && storeUserData.getString(Constants.USER_ID).length >0  && storeUserData.getString(Constants.IS_LOGGED_IN).equals("true")) {
            SetPostDetailData.SetchatButton(storeUserData.getString(Constants.USER_ID).toInt(), dataModel.userId!!.toInt(), btn_chat)
        }
        else{
            btn_chat.visibility = View.VISIBLE
        }

        if(dataModel.userId != null && dataModel.userId.toString().length >0 && storeUserData.getString(Constants.USER_ID).length >0 && storeUserData.getString(Constants.IS_LOGGED_IN).equals("true")) {
            SetPostDetailData.setcallButton(storeUserData.getString(Constants.USER_ID).toInt(), dataModel.userId!!.toInt(),btn_call,dataModel.isphone!!.toInt())
        }
        else{
            btn_call.visibility = View.VISIBLE
        }

        //new added
        if(dataModel.userId != null && dataModel.userId.toString().length >0 && storeUserData.getString(Constants.USER_ID).length >0 && storeUserData.getString(Constants.IS_LOGGED_IN).equals("true")) {
            SetPostDetailData.setWhatsappButton(storeUserData.getString(Constants.USER_ID).toInt(), dataModel.userId!!.toInt(),rlWhatsappCall,dataModel.whatsapp_inquiry_allow!!.toInt())
        } else{
            rlWhatsappCall.visibility = View.VISIBLE
        }

        // SetPostDetailData.setToolBar(storeUserData.getString(Constants.USER_ID).toInt(), dataModel.userId!!.toInt(),btn_chat,previewType,iv_delete)
    }
    fun bindRecyclerviewRelevantPost(RelatedArrayList: ArrayList<postDetailsBean.RelatedArray>) {

        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rv_recom.layoutManager = layoutManager
        val adapter = RelaventPostAdapter(RelatedArrayList, this)
        adapter.setClicklistner(this)
        rv_recom.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun onClick(v: View?) {
        when (v!!.getId()) {
            R.id.tv_aboutAd -> {
                if(AppConstants.checkUserIsLoggedin(this))
                {
                    val intent = Intent(this, ReportAdActivity::class.java)
                    intent.putExtra("postId",post_id)
                    startActivity(intent)
                }
                else{
                    val intent = Intent(applicationContext, LoginMainActivity::class.java)
                    AppConstants.alertLogin(this, getString(R.string.txt_login_to_report), intent)
                }
            }

            R.id.btn_back ->
            {
                onBackPressed()
            }



            R.id.iv_delete ->
            {
                Utils.deletePost(this,this,post_id)
            }
            R.id.btn_call -> {
                saftyInfoDialog("call")

            }

            R.id.rlWhatsappCall -> {
                saftyInfoDialog("whatsapp")

            }
            R.id.iv_social_share -> {
                Utils.sharePostLink(dataModel.category_id.toString(),
                        dataModel.sub_category_id.toString(),
                        dataModel.post_id.toString(),
                        dataModel.postImages.get(0).postImage.toString(), dataModel.sub_categoryName.toString(),this);
            }

            R.id.btn_chat ->
            {
                saftyInfoDialog("chat")

            }

            R.id.llAdOwnerProfile ->
            {
                val intent = Intent(this, FollowerProfileDetailActivity::class.java)
                Utils.openFollowingUserDetails(this,intent,dataModel.userId.toString())
            }
        }
    }

    private fun saftyInfoDialog(strType: String) {

        val dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_safty_info)
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val button_ok = dialog.findViewById<CustomButton>(R.id.button_ok)


        button_ok.setOnClickListener {
            dialog.dismiss()

            if(strType == "call"){
                if (AppConstants.checkUserIsLoggedin(this)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(arrayOf(Manifest.permission.CALL_PHONE), 200)
                    }
                }
                else {
                    val intent = Intent(this, LoginMainActivity::class.java)
                    AppConstants.alertLogin(this, getString(R.string.txt_login_to_call), intent)
                }
            }else if(strType == "whatsapp"){

                if (AppConstants.checkUserIsLoggedin(this)) {
                    var appPackage = ""
                    if (SetPostDetailData.isAppInstalled(this, "com.whatsapp")) {
                        appPackage = "com.whatsapp"
                        val intent = Intent(Intent.ACTION_VIEW)
                        intent.data = Uri.parse("http://api.whatsapp.com/send?phone=+91" + dataModel.whatsapp_no.toString() + "&text=")
                        startActivity(intent)
                    }else if (SetPostDetailData.isAppInstalled(this, "com.whatsapp.w4b")) {
                        appPackage = "com.whatsapp.w4b"
                        val intent = Intent(Intent.ACTION_VIEW)
                        intent.data = Uri.parse("http://api.whatsapp.com/send?phone=+91" + dataModel.whatsapp_no.toString() + "&text=")
                        startActivity(intent)
                    } else {
                        Utils.showToast(this, "Whatsapp not installed on your device")
                    }
                }else{
                    val intent = Intent(this, LoginMainActivity::class.java)
                    AppConstants.alertLogin(this, getString(R.string.txt_login_to_send_whatsapp), intent)
                }


            }else if(strType == "chat"){
                if (AppConstants.checkUserIsLoggedin(this)) {
                    val intent = Intent(this, ChatDetailActivity::class.java)
                    intent.putExtra("userid", dataModel.userId.toString())
                    intent.putExtra("postid", dataModel.post_id.toString())
                    intent.putExtra("sendername",dataModel.cuserName)
                    intent.putExtra("recivername",dataModel.userName)
                    intent.putExtra("reciverImage",dataModel.profilePicture)
                    intent.putExtra("senderImage",dataModel.cuserProfile)
                    intent.putExtra("productImg",dataModel.postImages.get(0).postImage)
                    intent.putExtra("sub_cat_name",dataModel.sub_categoryName)
                    intent.putExtra("chatType","s")
                    intent.putExtra("sub_cat_id",dataModel.sub_category_id.toString())
                    intent.putExtra("cat_id",dataModel.category_id.toString())
                    intent.putExtra("adTitle",dataModel.title.toString())
                    intent.putExtra("price",dataModel.price.toString())
                    intent.putExtra("from","detail")
                    startActivity(intent)
                } else {
                    val intent = Intent(this, LoginMainActivity::class.java)
                    AppConstants.alertLogin(this, getString(R.string.txt_login_to_see_chat), intent)
                }
            }

        }

        dialog.show()
    }

    override fun itemclickRecommendation(bean: postDetailsBean.RelatedArray) {

        dataModel.postImages.clear()
        dataModel.relatedArray.clear()

        getListingController = GetListingController(this, this)

        getListingController.getProductDetail(bean.post_id.toString())

        AddPostViewrController(this,post_id)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == 200) {
            val result = checkCallingOrSelfPermission(Manifest.permission.CALL_PHONE)
            if (result == PackageManager.PERMISSION_GRANTED) {

                val c = PhoneCallController(this, contact)
                c.makePhoneCall()
            } else {
                Utils.showToast(this,resources.getString(R.string.txt_phone_permission))
            }
        }
    }

}