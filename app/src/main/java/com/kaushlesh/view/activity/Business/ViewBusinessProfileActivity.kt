package com.kaushlesh.view.activity.Business

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.ablanco.zoomy.TapListener
import com.ablanco.zoomy.Zoomy
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.GsonBuilder
import com.kaushlesh.Controller.*
import com.kaushlesh.R
import com.kaushlesh.bean.BusinessUserProfileBean
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.CallTiming
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.widgets.CustomButton
import kotlinx.android.synthetic.main.activity_business_profile.*
import kotlinx.android.synthetic.main.activity_follower_profile_detail.*
import kotlinx.android.synthetic.main.activity_view_business_profile.*
import kotlinx.android.synthetic.main.fragment_followers.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ViewBusinessProfileActivity : AppCompatActivity(), View.OnClickListener, ParseControllerListener {

    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal lateinit var imgMore: ImageView
    internal lateinit var dialog: Dialog

    private lateinit var getListingController: GetListingController

    var langArray = arrayOf(
        "Monday", "Tuesday", "Wednesday", "Thursday",
        "Friday", "Saturday", "Sunday", "All Days"
    )

    //For Business
    var selectedLanguageBusiness: BooleanArray? = null
    var langListBusiness: ArrayList<Int> = ArrayList()
    var langListBusinessCall: java.util.ArrayList<Int> = java.util.ArrayList()


    //For Whatsapp
    var selectedLanguageWp: BooleanArray? = null
    var langListWp: java.util.ArrayList<Int> = java.util.ArrayList()
    var langListWpCall: java.util.ArrayList<Int> = java.util.ArrayList()

    var visitedBusinessProfileID = ""

    lateinit var storeUserData: StoreUserData
    var userIdBusiness = ""
    var userId=""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_business_profile)

        findViews()

        storeUserData=StoreUserData(this)
        userId = storeUserData.getString(Constants.USER_ID)


        selectedLanguageBusiness = BooleanArray(langArray.size)
        selectedLanguageWp = BooleanArray(langArray.size)

        if(intent!=null){
            visitedBusinessProfileID = intent.getStringExtra("businessProfileID").toString()
        }

        getUserBusinessProfile()
        //businessProfileViewCount()



    }

    private fun businessProfileViewCount() {
        BusinessProfileCountController(this,visitedBusinessProfileID)
    }

    private fun getUserBusinessProfile(){
        getListingController = GetListingController(this, this)
        getListingController.getUserBusinessProfile(visitedBusinessProfileID)
    }

    private fun findViews() {
        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)
        //tvtoolbartitle.text = resources.getString(R.string.txt_business_profile)
        imgMore = toolbar.findViewById(R.id.imgMore)

        btnback.setOnClickListener(this)
        imgMore.setOnClickListener(this)


    }

    override fun onClick(view: View?) {
        if (view != null) {
            when (view.id) {
                R.id.btn_back -> onBackPressed()
                R.id.imgMore -> {
                    showBottomsheetMoreOptions()
                }
            }
        }
    }

    private fun showBottomsheetMoreOptions() {
        val bottomSheetDialog = BottomSheetDialog(this)
        bottomSheetDialog.setContentView(R.layout.bottom_sheet_profile)

        val share: LinearLayout? = bottomSheetDialog.findViewById(R.id.llShareProfile)
        val report: LinearLayout? = bottomSheetDialog.findViewById(R.id.llReportProfile)

        report?.setOnClickListener {
            bottomSheetDialog.dismiss()
            openReportProfileDialog()
        }

        share?.setOnClickListener{

            if(userId != null && userId.length >0) {
                if (userId.toInt() == userIdBusiness.toInt()) {
                    Utils.shareMyBusinessLink("business-profile",visitedBusinessProfileID, this);
                } else {
                    Utils.shareOtherUserBusinessLink("business-profile",visitedBusinessProfileID, this);

                }
            }


        }

        bottomSheetDialog.show()
    }

    private fun openReportProfileDialog() {
        var reportValue = ""

        dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_report_profile)
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val btnNo = dialog.findViewById<CustomButton>(R.id.btn_no_report)
        val btnYes = dialog.findViewById<CustomButton>(R.id.btn_yes_report)
        val radioGrpReport = dialog.findViewById<RadioGroup>(R.id.radioGrpReport)


        radioGrpReport.setOnCheckedChangeListener(object :
                RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup, @IdRes checkedId: Int) {
                val radioButton: RadioButton = group.findViewById(checkedId) as RadioButton
                val rbSelected = radioButton.getText().toString();
                if (rbSelected == "Fraud User") {
                    reportValue = "Fraud User"
                } else if(rbSelected == "Offensive Profile"){
                    reportValue = "Offensive Profile"
                }else if(rbSelected == "In-active Profile"){
                    reportValue = "In-active Profile"
                }
            }
        })

        btnNo.setOnClickListener {
            dialog.dismiss()
        }

        btnYes.setOnClickListener {

            if(reportValue!=""){
                val reportController = ReportBusinessProfileController(
                        this,
                        this@ViewBusinessProfileActivity,
                        reportValue,
                        visitedBusinessProfileID
                )
                reportController.onClick(btnYes)
                dialog.dismiss()
            }else{
                Utils.showToast(this,"Please select reason")
            }
        }

        dialog.show()
    }

    private fun setBusinessData(userModel: BusinessUserProfileBean) {

        if(userModel.profileId!=null){
            titleProfileIDView.setText("Profile ID : " + userModel.profileId.toString())
        }

        if(userModel.bussinessprofile_id!=null){
            visitedBusinessProfileID = userModel.bussinessprofile_id.toString()
        }

        if(userModel.user_id!=null){
            userIdBusiness = userModel.user_id.toString()
        }

        if(userModel.bussiness_name!=null){
            txtBusinessName.setText(userModel.bussiness_name)
            tvtoolbartitle.text = userModel.bussiness_name +"'s Business Profile"

        }

        if(userModel.bussiness_about!=null){
            txtAboutBusiness.setText(userModel.bussiness_about)
        }

        if(userModel.service_info!=null){
            txtAboutServices.setText(userModel.service_info)
        }

        if(userModel.mobile_no!=null){
            txtMobileNo.setText(userModel.mobile_no)
        }

        if(userModel.whatsapp_no!=null){
            txtWpNumber.setText(userModel.whatsapp_no)
        }

        if(userModel.email!=null){
            txtEmailId.setText(userModel.email)
        }

        if(userModel.address1!=null && userModel.address2!=null && userModel.city!=null && userModel.state!=null){
            txtAddressLines.setText(userModel.address1 + " " + userModel.address2 +
                    " ," +userModel.city + " ," + userModel.state)
        }

        //set business call timing and hours
        if(!userModel.bussiness_call_timing.isEmpty()){
            txtCallHours.setText(userModel.bussiness_call_timing.get(0).openTime + " to "+
                    userModel.bussiness_call_timing.get(0).closeTime)


            //set selected days in dialog and textfield
            for (i in 0 until userModel.bussiness_call_timing.size) {
                for (j in 1 until langArray.size + 1){
                    if(userModel.bussiness_call_timing.get(i).dayId == j){
                        langListBusiness.add(j - 1)

                        selectedLanguageBusiness!![j - 1] = true // display selected days in dialog from api


                    }
                }
            }

            val stringBuilder = StringBuilder()
            for (j in langListBusiness.indices) {
                stringBuilder.append(langArray[langListBusiness[j]])
                if (j != langListBusiness.size - 1) {
                    stringBuilder.append(", ")
                }
            }

            Log.d("stringBuilder", "" + stringBuilder.toString())

            txtCallTiming.setText(stringBuilder.toString()) //for weekdays

            for (i in 0 until userModel.bussiness_call_timing.size) {
                if(userModel.bussiness_call_timing.get(i).dayId == 9){
                    txtCallTiming.setText("No Calls") //for No Calls
                }
            }
        }

        //set whatsapp call timing and hours
        //whatsapp inquiry call timing
        if(!userModel.whatsapp_inquiry_call_timing.isEmpty()){
            //set selected hours
            txtWPCallHours.setText(userModel.whatsapp_inquiry_call_timing.get(0).openTime +" to "+
                    userModel.whatsapp_inquiry_call_timing.get(0).closeTime)


            //set selected days in dialog and textfield
            for (i in 0 until userModel.whatsapp_inquiry_call_timing.size) {
                for (j in 1 until langArray.size + 1){
                    if(userModel.whatsapp_inquiry_call_timing.get(i).dayId == j){
                        langListWp.add(j - 1)

                        selectedLanguageWp!![j - 1] = true // display selected days in dialog from api


                    }
                }
            }

            val stringBuilder = StringBuilder()
            for (j in langListWp.indices) {
                stringBuilder.append(langArray[langListWp[j]])
                if (j != langListWp.size - 1) {
                    stringBuilder.append(", ")
                }
            }

            txtWPCallTiming.setText(stringBuilder.toString()) //for weekdays

            for (i in 0 until userModel.whatsapp_inquiry_call_timing.size) {
                if(userModel.whatsapp_inquiry_call_timing.get(i).dayId == 9){
                    txtWPCallTiming.setText("No Calls") //for No Calls
                }
            }
        }



        if(userModel.visitingcard_front!=null){
            if(!userModel.visitingcard_front!!.isEmpty()){
                Glide.with(applicationContext)
                        .load(userModel.visitingcard_front)
                        .placeholder(R.drawable.bg_img_placeholder)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(imgVisitingCardFront)
            }else{
                Glide.with(applicationContext)
                        .load(R.drawable.bg_no_img)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(imgVisitingCardFront)
            }

        }


        Zoomy.Builder(this)
            .target(imgVisitingCardFront)
            .enableImmersiveMode(false)
            .tapListener(TapListener { v: View? ->

                if(userModel.visitingcard_front!=null) {
                    if (!userModel.visitingcard_front!!.isEmpty()) {
                        startActivity(
                                Intent(this@ViewBusinessProfileActivity, ViewFullScreenImgActivity::class.java)
                                        .putExtra("image", ""+userModel.visitingcard_front)
                        )
                    }
                }

            })
            .register()


        if(userModel.visitingcard_back!=null){
            if(!userModel.visitingcard_back!!.isEmpty()){
                Glide.with(applicationContext)
                        .load(userModel.visitingcard_back)
                        .placeholder(R.drawable.progress_animated)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(imgVisitingCardBack)
            }else{
                Glide.with(applicationContext)
                        .load(R.drawable.bg_no_img)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(imgVisitingCardBack)
            }

        }


        Zoomy.Builder(this)
            .target(imgVisitingCardBack)
            .enableImmersiveMode(false)
            .tapListener(TapListener { v: View? ->

                if(userModel.visitingcard_back!=null) {
                    if (!userModel.visitingcard_back!!.isEmpty()) {
                        startActivity(
                                Intent(this@ViewBusinessProfileActivity, ViewFullScreenImgActivity::class.java)
                                        .putExtra("image", ""+userModel.visitingcard_back)
                        )
                    }
                }
            })
            .register()

        //Business call
        if(userModel.is_calls_allow.toString() == "1"){

            //Business call
            if(!userModel.bussiness_call_timing.isEmpty()){
                compareDatesBusiness(userModel.bussiness_call_timing)
            }

        }else{
            llBusinesCallView.visibility = View.GONE
            rlNoCallBusiness.visibility = View.VISIBLE
        }

        //Whatsapp call
        if(userModel.is_whatsapp_calls_allow.toString() == "1"){

            //Whatsapp call
            if(!userModel.whatsapp_inquiry_call_timing.isEmpty()){
                compareDatesWhatsapp(userModel.whatsapp_inquiry_call_timing)
            }

        }else{
            llWhatsappView.visibility = View.GONE
            rlNoCallWp.visibility = View.VISIBLE
        }

    }

    private fun compareDatesBusiness(bussinessCallTiming: java.util.ArrayList<CallTiming>) {

        langListBusinessCall.clear()

        val businessOpenTime = bussinessCallTiming.get(0).openTime
        val businessCloseTime = bussinessCallTiming.get(0).closeTime


        val now: Calendar = Calendar.getInstance()
        val hour: Int = now.get(Calendar.HOUR_OF_DAY)
        val minute: Int = now.get(Calendar.MINUTE)

        var dateCompareOne: Date? = null
        var dateCompareTwo: Date? = null

        //current weekday
        var date: Date? = now.getTime()
        val weekday: String = SimpleDateFormat("EEEE", Locale.ENGLISH).format(date!!.time)


        for (i in 0 until bussinessCallTiming.size) {
            for (j in 1 until langArray.size + 1){
                if(bussinessCallTiming.get(i).dayId == j){
                    langListBusinessCall.add(j - 1)
                }
            }
        }


        val stringBuilder = StringBuilder()
        for (j in langListBusinessCall.indices) {

            stringBuilder.append(langArray[langListBusinessCall[j]])

            if (j != langListBusinessCall.size - 1) {
                stringBuilder.append(", ")
            }
        }

        Log.d("ComparedDate", "STRING : " +stringBuilder)


        if(stringBuilder.toString().contains(weekday)){
            try {
                date = Utils.parseDate("$hour:$minute")
                dateCompareOne = Utils.parseDate(businessOpenTime)
                dateCompareTwo = Utils.parseDate(businessCloseTime)

                Log.d("ComparedDate", "OPEN : " +businessOpenTime + "CLOSE : " +businessCloseTime )
                Log.d("ComparedDate", "ONE : " +dateCompareOne + "TWO : " +dateCompareTwo )



                if (dateCompareOne!!.before(date) && dateCompareTwo!!.after(date)) {
                    Log.d("ComparedDate", "Time Matched")




                }else{
                    Log.d("ComparedDate", "Time Not Matched")
                    llBusinesCallView.visibility = View.GONE
                    rlNoCallBusiness.visibility = View.VISIBLE

                }

            }catch (e: Exception){
                e.printStackTrace()
            }
        }else{

            for (i in 0 until bussinessCallTiming.size) {
                Log.d("ComparedDate", ""+bussinessCallTiming.get(i).dayId)
                if(bussinessCallTiming.get(i).dayId.toString() == "8"){
                    Log.d("ComparedDate", "All Days")
                    try {
                        date = Utils.parseDate("$hour:$minute")
                        dateCompareOne = Utils.parseDate(businessOpenTime)
                        dateCompareTwo = Utils.parseDate(businessCloseTime)
                        if (dateCompareOne!!.before(date) && dateCompareTwo!!.after(date)) {

                            Log.d("ComparedDate", "Time Matched")

                        }else{
                            Log.d("ComparedDate", "Time Not Matched")
                            llBusinesCallView.visibility = View.GONE
                            rlNoCallBusiness.visibility = View.VISIBLE                        }

                    }catch (e: Exception){
                        e.printStackTrace()
                    }
                }else{
                    Log.d("ComparedDate", "ELSE....")

                    llBusinesCallView.visibility = View.GONE
                    rlNoCallBusiness.visibility = View.VISIBLE
                }
            }

        }


    }

    private fun compareDatesWhatsapp(whatsappCallTiming: java.util.ArrayList<CallTiming>) {

        langListWpCall.clear()

        val whatsappOpenTime = whatsappCallTiming.get(0).openTime
        val whatsappCloseTime = whatsappCallTiming.get(0).closeTime


        val now: Calendar = Calendar.getInstance()
        val hour: Int = now.get(Calendar.HOUR_OF_DAY)
        val minute: Int = now.get(Calendar.MINUTE)

        var dateCompareOne: Date? = null
        var dateCompareTwo: Date? = null

        //current weekday
        var date: Date? = now.getTime()
        val weekday: String = SimpleDateFormat("EEEE", Locale.ENGLISH).format(date!!.time)


        for (i in 0 until whatsappCallTiming.size) {
            for (j in 1 until langArray.size + 1){
                if(whatsappCallTiming.get(i).dayId == j){
                    langListWpCall.add(j - 1)
                }
            }
        }


        val stringBuilder = StringBuilder()
        for (j in langListWpCall.indices) {

            stringBuilder.append(langArray[langListWpCall[j]])

            if (j != langListWpCall.size - 1) {
                stringBuilder.append(", ")
            }
        }

        Log.d("ComparedDate", "STRING : " +stringBuilder)


        if(stringBuilder.toString().contains(weekday)){
            try {
                date = Utils.parseDate("$hour:$minute")
                dateCompareOne = Utils.parseDate(whatsappOpenTime)
                dateCompareTwo = Utils.parseDate(whatsappCloseTime)

                Log.d("ComparedDate", "OPEN : " +whatsappOpenTime + "CLOSE : " +whatsappCloseTime )
                Log.d("ComparedDate", "ONE : " +dateCompareOne + "TWO : " +dateCompareTwo )

                if (dateCompareOne!!.before(date) && dateCompareTwo!!.after(date)) {

                    Log.d("ComparedDate", "Time Matched")


                }else{

                    Log.d("ComparedDate", "Time Not Matched")
                    llWhatsappView.visibility = View.GONE
                    rlNoCallWp.visibility = View.VISIBLE

                }

            }catch (e: Exception){
                e.printStackTrace()
            }
        }else{

            for (i in 0 until whatsappCallTiming.size) {
                Log.d("ComparedDate", ""+whatsappCallTiming.get(i).dayId)
                if(whatsappCallTiming.get(i).dayId.toString() == "8"){
                    Log.d("ComparedDate", "All Days")
                    try {
                        date = Utils.parseDate("$hour:$minute")
                        dateCompareOne = Utils.parseDate(whatsappOpenTime)
                        dateCompareTwo = Utils.parseDate(whatsappCloseTime)
                        if (dateCompareOne!!.before(date) && dateCompareTwo!!.after(date)) {

                            Log.d("ComparedDate", "Time Matched")

                        }else{

                            Log.d("ComparedDate", "Time Not Matched")
                            llWhatsappView.visibility = View.GONE
                            rlNoCallWp.visibility = View.VISIBLE



                        }

                    }catch (e: Exception){
                        e.printStackTrace()
                    }
                }else{
                    Log.d("ComparedDate", "ELSE....")

                    llWhatsappView.visibility = View.GONE
                    rlNoCallWp.visibility = View.VISIBLE
                }
            }

        }


    }


    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if(method.equals("getBusinessProfile")){

            val userModel = BusinessUserProfileBean()
            userModel.businessUserProfileBean(da)

            Log.d(
                "getBusinessProfile",
                "response : " + GsonBuilder().setPrettyPrinting().create().toJson(
                    userModel
                )
            )

            setBusinessData(userModel)
        }

        if(method.equals("reportBusinessProfile")){
            Utils.showToast(this, "" + message)
        }

    }

    override fun onFail(msg: String, method: String) {
        if(method.equals("reportBusinessProfile")){
            Utils.showToast(this, "" + msg)
        }
    }

}