package com.kaushlesh.view.activity.AdPost

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.*
import android.provider.Settings
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.gson.GsonBuilder
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.kaushlesh.AutoCompleteAdapter
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.Controller.PurchasePackageController
import com.kaushlesh.R
import com.kaushlesh.adapter.AdPostSelectPackageAdapter
import com.kaushlesh.bean.*
import com.kaushlesh.bean.AdPost.AdPostResponseBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.constant.AppConstants.printLog
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.reusable.LocationAddress
import com.kaushlesh.utils.AdPost.FromLocation.CheckCategoryAndApiCall
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.fragment.HomeFragmentNew
import com.kaushlesh.view.fragment.MyAccount.PurchasePackage.AdPostPackagesActivity
import com.kaushlesh.widgets.CustomButton
import com.razorpay.Checkout
import com.razorpay.PaymentResultListener
import gun0912.tedimagepicker.builder.TedImagePicker
import kotlinx.android.synthetic.main.activity_ad_post_packages.*
import kotlinx.android.synthetic.main.activity_attention_ad_post.*
import kotlinx.android.synthetic.main.activity_set_location.*
import kotlinx.android.synthetic.main.activity_set_location.act_location
import kotlinx.android.synthetic.main.activity_set_location.rl_dis_type
import kotlinx.android.synthetic.main.activity_set_location.spinner_dis_type
import kotlinx.android.synthetic.main.activity_set_location.tv_dis_type
import kotlinx.android.synthetic.main.layout_ad_post_packages.*
import kotlinx.android.synthetic.main.layout_add_area.*
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList


class SetLocationActivity : AppCompatActivity(), View.OnClickListener, ParseControllerListener,
        PaymentResultListener, AdPostSelectPackageAdapter.ItemClickListener, AdapterView.OnItemClickListener  {

    private var mLastClickTime: Long = 0

    private lateinit var type: String
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal lateinit var etlocation: EditText
    internal lateinit var cbCurrentlocation: CheckBox
    internal lateinit var cbLocation: CheckBox
    internal lateinit var btnnext: CustomButton
    internal lateinit var llLocationDetail: LinearLayout
    internal lateinit var placesClient: PlacesClient
    internal lateinit var autoCompleteAdapter: AutoCompleteAdapter
    internal var latitude: Double = 0.toDouble()
    internal var longitude: Double = 0.toDouble()
    internal var dislatitude: Double = 0.toDouble()
    internal var dislongitude: Double = 0.toDouble()
    internal var curlatitude: Double = 0.toDouble()
    internal var curlongitude: Double = 0.toDouble()
    internal var addlatitude: Double = 0.toDouble()
    internal var addlongitude: Double = 0.toDouble()
    internal var packageid: String? = ""
    internal var userpostpackageid: String? = ""

    lateinit var storeUserData: StoreUserData
    private var imageList = ArrayList<Bitmap>()
    var bitmapImage: Bitmap? = null
    var url: String? = null
    var price: String? = ""
    internal var pkglist: ArrayList<GetMyPackagesBean.Packages> = ArrayList()
    internal var pkglist1: ArrayList<GetMyPackagesBean.Packages> = ArrayList()
    internal var newpkglist: ArrayList<GetMyPackagesBean.Packages> = ArrayList()
    lateinit var checkout: Checkout
    internal var payamount: String? = ""
    internal var sugplaceList: MutableList<LocationSuggestionBean.Lacation> = java.util.ArrayList()
    internal var placeList: MutableList<String> = java.util.ArrayList()
    internal var typelist: MutableList<String> = java.util.ArrayList()
    internal var typeidlist: MutableList<String> = java.util.ArrayList()
    internal var locationlist: ArrayList<LocationListBeans.LocationList> = ArrayList()

    lateinit var getListingController: GetListingController
    lateinit var purchasePackageController: PurchasePackageController
    internal lateinit var type_spinner_type: ArrayAdapter<String>
    var locationid: String? = ""
    var district: String? = null
    private lateinit var posttype: String
    internal lateinit var dialog: Dialog
    var locationName: String? = ""
    lateinit var ivRefresh: ImageView

    var isPackageSelected: Boolean = false

    internal var AUTOCOMPLETE_REQUEST_CODE_SOURCE = 110

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_set_location)

        Checkout.preload(applicationContext)

        checkout = Checkout()
        checkout.setKeyID(getString(R.string.key_razorpay))

        rl_main.visibility = View.VISIBLE
        rl_attention.visibility = View.GONE
        rl_pkgs.visibility = View.GONE

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
        }

        storeUserData = StoreUserData(this)

        pkglist.clear()
        pkglist1.clear()
        newpkglist.clear()

        getListingController = GetListingController(this, this)

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.txt_set_location)

        val apiKey = getString(R.string.google_place_api_key)
        if (apiKey.isEmpty()) {
            //responseView.setText(getString(R.string.error));
            return
        }

        // Setup Places Client
        if (!Places.isInitialized()) {
            Places.initialize(applicationContext, apiKey)
        }

        placesClient = Places.createClient(applicationContext)

        if (intent != null) {
            price = intent.getStringExtra("price")
            Utils.showLog("loc", "==price==" + price)

        }

        initBindViews()

        getListingController.getLocationList()

        //initAutoCompleteTextView()

        //takepermission()

        takepermissionNew()

    }

    private fun initBindViews() {
        etlocation = findViewById(R.id.et_location)
        btnnext = findViewById(R.id.btn_next)
        llLocationDetail = findViewById(R.id.ll_location_detail)
        cbCurrentlocation = findViewById(R.id.cb_current_location)
        cbLocation = findViewById(R.id.cb_location)
        ivRefresh = findViewById(R.id.iv_refresh)

        btnnext.setOnClickListener(this)
        btnback.setOnClickListener(this)
        tv_pkg.setOnClickListener(this)
        tvsellall.setOnClickListener(this)
        btn_ad_post.setOnClickListener(this)
        tvsellalllist.setOnClickListener(this)
        btn_back_att.setOnClickListener(this)
        btn_back_ad_post_package.setOnClickListener(this)
        tv_dis_type.setOnClickListener(this)
        ivRefresh.setOnClickListener(this)

        cbCurrentlocation.setOnClickListener {
            if (cbCurrentlocation.isChecked) {
                //takepermission()
                takepermissionNew()
                cbLocation.setChecked(false); //to uncheck
                act_location.setBackgroundResource(R.drawable.bg_edittext)
                act_location.setText("")
                act_location.setPadding(35, 0, 35, 0)
                act_location.setError(null)
                //hideeKeyboard(act_location)

                closeKeyboard()

            } else {
                cbCurrentlocation.setChecked(true) //to check
                cbLocation.setChecked(false) //to uncheck
                //hideeKeyboard(act_location)
                closeKeyboard()
            }
        }

        cbLocation.setOnClickListener {
            if (cbLocation.isChecked) {
                cbCurrentlocation.setChecked(false); //to uncheck
                act_location.setBackgroundResource(R.drawable.bg_edittext_black)
                act_location.setPadding(35, 0, 35, 0)
                //act_location.requestFocus()
                showKeyboard(act_location)

            } else {
                cbCurrentlocation.setChecked(false) //to check
                cbLocation.setChecked(true) //to uncheck
            }
        }

        act_location.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(paramView: View?, paramMotionEvent: MotionEvent?): Boolean {
                cbCurrentlocation.setChecked(false); //to uncheck
                cbLocation.setChecked(true) //to uncheck
                act_location.setBackgroundResource(R.drawable.bg_edittext_black)
                act_location.setPadding(35, 0, 35, 0)
                return false
            }
        })


        act_location.setOnClickListener {
            act_location.setBackgroundResource(R.drawable.bg_edittext_black)
            cbCurrentlocation.setChecked(false); //to uncheck
            cbLocation.setChecked(true) //to uncheck
        }

        act_location.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                getListingController.callforGetLocationList(s.toString(), dislatitude, dislongitude, district.toString())
            }

            override fun afterTextChanged(s: Editable?) {}
        })


        // Set a focus change listener for auto complete text view
        act_location.onFocusChangeListener = View.OnFocusChangeListener { view, b ->
            if (b) {
                // Display the suggestion dropdown on focus
                act_location.showDropDown()
            }
        }

        /* // Set an item click listener for auto complete text view
         act_location.onItemClickListener = AdapterView.OnItemClickListener{
             parent,view,position,id->
             val selectedItem = parent.getItemAtPosition(position).toString()
             // Display the clicked item using toast
             Toast.makeText(applicationContext,"Selected : $selectedItem",Toast.LENGTH_SHORT).show()
         }
 */
    }

    fun closeKeyboard() {
        val activity = SetLocationActivity

        val view = currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
            imm!!.hideSoftInputFromWindow(view.getWindowToken(), 0)
        }
    }


    private fun takepermissionNew() {

        val permissionList = arrayListOf<String>()
        permissionList.add(Manifest.permission.INTERNET)
        permissionList.add(Manifest.permission.ACCESS_COARSE_LOCATION)
        permissionList.add(Manifest.permission.ACCESS_FINE_LOCATION)

        //original added
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            permissionList.add(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
        }*/
        Utils.showLog(HomeFragmentNew.TAG, "===take permission====")

        //Dexter.withContext(requireContext()).withPermissions(permissionList).withListener(this).check()

        Dexter.withContext(this)
                .withPermissions(permissionList).withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(multiplePermissionsReport: MultiplePermissionsReport) {
                        if (multiplePermissionsReport.areAllPermissionsGranted()) {
                            Utils.showLog(TAG, "===permission checked====")

                            checkLocationNew()
                            return
                        }
                        if (multiplePermissionsReport.isAnyPermissionPermanentlyDenied) {
                            Utils.showLog(TAG, "===permission denied====")
//                            showSettingsDialog() //Original added here
                        }

                    }

                    override fun onPermissionRationaleShouldBeShown(
                            list: List<PermissionRequest>,
                            permissionToken: PermissionToken
                    ) {
                        Utils.showLog(HomeFragmentNew.TAG, "===permission continue====")
                        permissionToken.continuePermissionRequest()
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//                            showSettingsDialog() //Original added here
                        }
                    }
                })
                .onSameThread().check()
    }

    @SuppressLint("MissingPermission")
    private fun checkLocationNew() {

        Log.e(HomeFragmentNew.TAG, "checkLocation called!")
        if (!isGPSEnabled(this)) {
            checkGPSEnabled()
            Log.e(TAG, "ohShit, GPS provider is not enabled ")
        } else {
            try {
                val serviceString = Context.LOCATION_SERVICE
                locationManager = getSystemService(serviceString) as LocationManager?

                val location = locationManager!!.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                Log.e(TAG, "last location" + location)
                if (location != null) {
                    // latitude = location.getLatitude()
                    // longitude = location.getLongitude()
                    curlatitude = location.getLatitude()
                    curlongitude = location.getLongitude()
                }
                Log.e(TAG, "Lat1 : $curlatitude  Long 1: $curlongitude")
                val locationAddress = LocationAddress()
                locationAddress.getAddressFromLocation(curlatitude, curlongitude, this, GeoCodeHandler())

                myLocationListener = object : LocationListener {
                    override fun onLocationChanged(locationListener: Location) {
                        Log.e(TAG, "onLocationChanged invoked")
                        if (locationListener != null) {
                            //latitude = locationListener.latitude
                            //longitude = locationListener.longitude
                            curlatitude = locationListener.latitude
                            curlongitude = locationListener.longitude
                            Log.e(TAG, "Lat : $curlatitude  Long : $curlongitude")

                            if (location != null) {
                                if (location.getLatitude() != curlatitude && location.getLongitude() != curlongitude) {
                                    Log.e(TAG, "if")
                                    val locationAddress1 = LocationAddress()
                                    locationAddress1.getAddressFromLocation(curlatitude, curlongitude, this@SetLocationActivity, GeoCodeHandler())
                                }

                            } else {
                                Log.e(TAG, "else")
                                try {
                                    if (location != null) {
                                        if (location!!.getLatitude() != curlatitude && location.getLongitude() != curlongitude) {
                                            Log.e(TAG, "if")
                                            val locationAddress1 = LocationAddress()
                                            locationAddress1.getAddressFromLocation(curlatitude, curlongitude, this@SetLocationActivity, GeoCodeHandler())
                                        }
                                    }
                                } catch (e: NullPointerException) {
                                    e.printStackTrace()
                                }
                            }
                            /*  if (location == null || location.toString().length == 0) {
                                  val locationAddress = LocationAddress()
                                  locationAddress.getAddressFromLocation(latitude, longitude, context!!, GeoCodeHandler())
                              }*/
                        }
                    }

                    override fun onProviderDisabled(provider: String) {}
                    override fun onProviderEnabled(provider: String) {}
                    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
                }

                locationManager!!.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000 * 60 * 5, 0f, myLocationListener!!)
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }
        }
    }

    private fun checkGPSEnabled() {
        val manager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER).not()) {
            turnOnGPS()
        }
    }

    private fun turnOnGPS() {
        val request = LocationRequest.create().apply {
            interval = 2000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
        val builder = LocationSettingsRequest.Builder().addLocationRequest(request)
        val client: SettingsClient = LocationServices.getSettingsClient(this)
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
        task.addOnFailureListener {
            if (it is ResolvableApiException) {
                try {
                    it.startResolutionForResult(this, 12345)
                } catch (sendEx: IntentSender.SendIntentException) {
                }
            }
        }.addOnSuccessListener {
            //here GPS is On
            checkLocation()
        }
    }

    private fun hideeKeyboard(actLocation: AutoCompleteTextView) {
        actLocation.clearFocus()
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
    }

    private fun showKeyboard(actLocation: AutoCompleteTextView) {
        actLocation.requestFocus()
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }


    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        locationName = parent?.getItemAtPosition(position).toString()
        //call api to get location latlng
        Utils.showLog(TAG, "=====address lat long api call=======" + locationName)
        getListingController.getLatLongOfSelectedAddress(locationName.toString())
    }

    private fun initAutoCompleteTextViewSearch() {
        val adapter = ArrayAdapter<String>(this, // Context
                android.R.layout.simple_spinner_dropdown_item, // Layout
                placeList // Array
        )

        // Set the AutoCompleteTextView adapter
        act_location.setAdapter(adapter)
        act_location.setOnItemClickListener(this)
        // Auto complete threshold
        // The minimum number of characters to type to show the drop down
        act_location.threshold = 1
        adapter.notifyDataSetChanged()
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_back -> onBackPressed()

            R.id.btn_back_ad_post_package -> {
                rl_main.visibility = View.VISIBLE
                rl_attention.visibility = View.GONE
                rl_pkgs.visibility = View.GONE

                if(pkglist!=null){
                    pkglist.clear()
                }
                if(pkglist1!=null){
                    pkglist1.clear()
                }
                if(newpkglist!=null){
                    newpkglist.clear()
                }
                Utils.setButtonEnabled(btnnext, true)
            }

            R.id.btn_back_att -> {
                rl_main.visibility = View.VISIBLE
                rl_attention.visibility = View.GONE
                rl_pkgs.visibility = View.GONE

                Utils.setButtonEnabled(btnnext, true)
            }

            R.id.tv_dis_type -> {
                spinner_dis_type.visibility = View.VISIBLE
                spinner_dis_type.performClick()
                tv_dis_type.visibility = View.GONE

                spinner_dis_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {

                        if (position > 0) {
                            printLog(TAG, "onItemSelected: ===" + adapterView.selectedItem)
                            printLog(TAG, "onItemSelected: ===" + typeidlist.get(position))
                            locationid = typeidlist.get(position)
                            district = adapterView.selectedItem.toString()

                            storeUserData.setString(Constants.LOC_ID_PKG, locationid.toString())
                            storeUserData.setString(Constants.LOCATION_ID, locationid.toString())
                            storeUserData.setString(Constants.LOC_NAME, adapterView.selectedItem.toString())
                            Utils.showLog(TAG, "====stored location id====" + locationid.toString())

                            rl_dis_type.setBackgroundResource(R.drawable.bg_edittext_black)

                            getListingController.getLatLongOfSelectedStrict(district + "," + resources.getString(R.string.txt_gujarat))

                            llLocationDetail.visibility = View.VISIBLE

                            //initAutoCompleteTextViewSearch()

                            if (act_location.text.toString().length > 0) {
                                act_location.setText("")
                            }
                        } else {
                            llLocationDetail.visibility = View.GONE
                            locationid = "0"
                        }
                    }

                    override fun onNothingSelected(adapterView: AdapterView<*>) {
                        llLocationDetail.visibility = View.GONE
                        locationid = "0"
                    }
                }
            }

            R.id.btn_next -> {

                //storeUserData.setString(Constants.LOCATION_ID, locationid.toString())
                Utils.showLog(TAG, "====current checkbox====" + cbCurrentlocation.isChecked)
                Utils.showLog(TAG, "====location checkbox====" + cbLocation.isChecked)
                Utils.showLog(TAG, "==== cuurent lat====" + curlatitude)
                Utils.showLog(TAG, "==== current long====" + curlongitude)
                Utils.showLog(TAG, "==== location lat====" + addlatitude)
                Utils.showLog(TAG, "==== location long====" + addlongitude)

                if (locationid.equals("0") || locationid!!.length == 0) {
                    Utils.showToast(this, resources.getString(R.string.txt_select_district_toast))
                } else {
                    if (cbCurrentlocation.isChecked) {
                        /*   if (tv_current_loc.text.toString().contains(district.toString()))*/
                        if (curlatitude == 0.0 && curlongitude == 0.0) {
                            Utils.showLocationAlert(this, resources.getString(R.string.txt_check_location_current_not_found))
                        } else {
                            latitude = curlatitude
                            longitude = curlongitude
                            if (tv_current_loc.text.toString().contains(resources.getString(R.string.txt_gujarat), ignoreCase = true) || tv_current_loc.text.toString().contains("GJ", ignoreCase = true) || tv_current_loc.text.toString().contains("Guj", ignoreCase = true)) {
                                //openConfirmationDialog()
                                openConfirmationDialogForLiveLocation()
                            } else {
                                //for district
                                //Utils.showLocationAlert(this, resources.getString(R.string.txt_check_location))

                                //for state
                                Utils.showLocationAlert(this, resources.getString(R.string.txt_check_location_state))
                            }
                        }
                    } else {
                        if (act_location.text.length > 0) {

                            if (act_location.text.toString().equals(locationName)) {
                                //Utils.setButtonEnabled(btnnext,false)

                                if (addlatitude == 0.0 && addlongitude == 0.0) {
                                    Utils.showLocationAlert(this, resources.getString(R.string.txt_check_location_refer))
                                } else {
                                    latitude = addlatitude
                                    longitude = addlongitude
                                    openConfirmationDialog()
                                }

                                //getListingController.getMyPackgesList(storeUserData.getString(Constants.CATEGORY_ID))
                            } else {
                                Utils.showLocationAlert(this, resources.getString(R.string.txt_check_location_refer))
                            }

                        } else {
                            act_location.setError(resources.getString(R.string.txt_required_filed))
                        }
                    }
                }
            }

            R.id.tv_pkg -> {
                if (payamount != null && payamount.toString().length > 0) {
                    Utils.setTextviewEnabled(tv_pkg, false)

                    val total = payamount!!.toInt() * 100

                    if(Utils.isConnected()){
                        startpay(total.toString())
                    }else{
                        Utils.showAlertConnection(this)
                    }
                }
            }

            R.id.tvsellall -> {
                val intent = Intent(applicationContext, AdPostPackagesActivity::class.java)
                intent.putExtra("isPremium", "no")
                intent.putExtra("address", act_location.text.toString())
                intent.putExtra("catId", storeUserData.getString(Constants.CATEGORY_ID))
                intent.putExtra("lat", latitude.toString())
                intent.putExtra("long", longitude.toString())
                intent.putExtra("price", price.toString())
                intent.putExtra("postaddress", locationName.toString())
                startActivity(intent)
            }

            R.id.tvsellalllist -> {
                val intent = Intent(applicationContext, AdPostPackagesActivity::class.java)
                intent.putExtra("isPremium", "no")
                intent.putExtra("address", act_location.text.toString())
                intent.putExtra("catId", storeUserData.getString(Constants.CATEGORY_ID))
                intent.putExtra("lat", latitude.toString())
                intent.putExtra("long", longitude.toString())
                intent.putExtra("price", price.toString())
                intent.putExtra("postaddress", locationName.toString())
                startActivity(intent)
            }

            R.id.btn_ad_post -> {

                //prevent double click on button
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    return;
                }

                mLastClickTime = SystemClock.elapsedRealtime();

                Utils.showLog(TAG, "==package id===" + packageid)

                if(!isPackageSelected){
                    Utils.showToast(this, getString(R.string.txt_select_pkg))
                    return;
                }

                if (packageid != null && packageid.toString().length == 0) {
                    Utils.showToast(this, getString(R.string.txt_select_pkg))
                } else {
                    type = "normal"
                    posttype = "pkglist"
                    storeUserData.setString(Constants.USER_PKG_ID, userpostpackageid.toString())

                    Utils.setButtonEnabled(btn_ad_post, false)
                    if (cbCurrentlocation.isChecked) {
                        locationName = tv_current_loc.text.toString()
                    } else {
                        locationName = act_location.text.toString()
                    }
                    CheckCategoryAndApiCall.checkforCategory(this,
                            storeUserData.getString(Constants.CATEGORY_ID).toInt(),
                            storeUserData.getString(Constants.SUBCATEGORYID).toInt(),
                            locationName.toString(),
                            latitude.toString(),
                            longitude.toString(),
                            price.toString(),
                            packageid!!
                    )
                }
            }

            R.id.iv_refresh -> {
                takepermissionNew()

                val anim = RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, .5f, Animation.RELATIVE_TO_SELF, .5f)
                anim.setInterpolator(LinearInterpolator())
                anim.setRepeatCount(1)
                anim.setDuration(1000)
                ivRefresh.setAnimation(anim)
                ivRefresh.startAnimation(anim)
            }
        }
    }

    private fun openConfirmationDialogForLiveLocation() {

            dialog = Dialog(this)
            // Include dialog.xml file
            dialog.setContentView(R.layout.dialog_logout)
            // Set dialog title
            dialog.setTitle("")
            dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            // set values for custom dialog components - text, image and button
            val tv_title = dialog.findViewById<TextView>(R.id.tv_title)
            val btnno = dialog.findViewById<Button>(R.id.btn_no)
            val btnyes = dialog.findViewById<Button>(R.id.btn_yes)

            tv_title.text = resources.getString(R.string.txt_ad_post_confirmation_location)
            btnno.text = resources.getString(R.string.txt_review)
            btnyes.text = resources.getString(R.string.txt_ok)
            btnno.setTextColor(resources.getColor(R.color.black))
            btnyes.setTextColor(resources.getColor(R.color.orange))

            btnno.setOnClickListener {
                Utils.setButtonEnabled(btnnext, true)
                dialog.dismiss()
            }

            btnyes.setOnClickListener {
                dialog.dismiss()
                openConfirmationDialog()
            }

            dialog.show()
    }

    private fun openConfirmationDialog() {

        dialog = Dialog(this)
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_logout)
        // Set dialog title
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set values for custom dialog components - text, image and button
        val tv_title = dialog.findViewById<TextView>(R.id.tv_title)
        val btnno = dialog.findViewById<Button>(R.id.btn_no)
        val btnyes = dialog.findViewById<Button>(R.id.btn_yes)

        tv_title.text = resources.getString(R.string.txt_ad_post_confirmation)
        btnno.text = resources.getString(R.string.txt_review)
        btnyes.text = resources.getString(R.string.txt_post_now)
        btnno.setTextColor(resources.getColor(R.color.black))
        btnyes.setTextColor(resources.getColor(R.color.orange))

        btnno.setOnClickListener {
            Utils.setButtonEnabled(btnnext, true)
            dialog.dismiss()
        }

        btnyes.setOnClickListener {
            Utils.setButtonEnabled(btnnext, false)
            getListingController.getMyPackgesList(storeUserData.getString(Constants.CATEGORY_ID))
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun initAutoCompleteTextView() {

        //val type = Typeface.createFromAsset(getContext()!!.getAssets(), "fonts/" + "SFRegular.otf")
        //atvaddress.setTypeface(type)
        act_location.setThreshold(1)
        //act_location.setOnItemClickListener(autocompleteClickListener)
        autoCompleteAdapter = AutoCompleteAdapter(applicationContext, placesClient)
        act_location.setAdapter(autoCompleteAdapter)

    }
/*
    private val autocompleteClickListener =
        AdapterView.OnItemClickListener { adapterView, view, i, l ->
            try {
                val item = autoCompleteAdapter.getItem(i)
                var placeID: String = ""

                if (item != null) {
                    placeID = item.placeId
                }

                //                To specify which data types to return, pass an array of Place.Fields in your FetchPlaceRequest
                //                Use only those fields which are required.

                val placeFields = Arrays.asList(
                        Place.Field.ID,
                        Place.Field.NAME,
                        Place.Field.ADDRESS,
                        Place.Field.LAT_LNG
                )

                var request: FetchPlaceRequest? = null
                request = FetchPlaceRequest.builder(placeID, placeFields)
                    .build()

                if (request != null) {
                    placesClient.fetchPlace(request).addOnSuccessListener { task ->
                        Log.i(
                                TAG,
                                task.place.name + "\n" + task.place.address + "\n" + task.place.latLng
                        )

                        //String address = task.getPlace().getName() + task.getPlace().getAddress();
                        val address = task.place.address
                        Log.i(TAG, "======== LOCATION=======" + address!!)

                        val latLng = task.place.latLng

                        if (latLng != null) {
                            latitude = latLng.latitude
                            longitude = latLng.longitude
                        }

                        Log.i(
                                TAG,
                                "======== LATITUDE =======$latitude========LONGITUDE=======$longitude"
                        )

                        if (!address.isEmpty()) {

                            *//* val intent = Intent(getContext(), ActivitySavedPlacesAddressDetails::class.java)
                            intent.putExtra("isfor", "add")
                            intent.putExtra("address", address)
                            intent.putExtra("latitude", latitude.toString())
                            intent.putExtra("longitude", longitude.toString())
                            startActivity(intent)*//*

                        }
                    }.addOnFailureListener { e ->
                        e.printStackTrace()
                        Log.i(TAG, e.message.toString())
                    }
                }

            } catch (e: Exception) {
                e.printStackTrace()
            } catch (e: IllegalArgumentException) {
                e.printStackTrace()
            }
        }*/

    companion object {
        private val TAG = "SetLocationActivity"
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {

        if (method.equals("GetMyPackages")) {
            val dataModel = GetMyPackagesBean()
            dataModel.GetMyPackagesBean(da)

            pkglist = dataModel.freepkglist
            Utils.showLog(TAG, "==my pckg list size==" + pkglist.size)

            Utils.showLog(TAG, "==pkg list==" + GsonBuilder().setPrettyPrinting().create().toJson(pkglist))

            if (pkglist.size > 0) {
                for (i in pkglist.indices) {
                    if (pkglist.get(i).remaiingpost!!.toInt() > 0) {
                        pkglist1.add(pkglist.get(i))
                    }
                }

                if (pkglist1.size > 0) {
                    pkglist1.sortWith(Comparator { o1, o2 -> o1.pkgvalidity!!.compareTo(o2.pkgvalidity!!) })

                    // val sortedModel= Collections.sort(pkglist, comparator)
                    Utils.showLog(TAG, "==sorted list==" + GsonBuilder().setPrettyPrinting().create().toJson(pkglist1))

                    for (m in pkglist1.indices) {
                        if (pkglist1.get(m).packagetype.equals("Free")) {
                            Utils.showLog(
                                    TAG,
                                    "==ad post free===" + GsonBuilder().setPrettyPrinting().create().toJson(
                                            pkglist1.get(m)
                                    )
                            )

                            rl_main.visibility = View.VISIBLE
                            rl_attention.visibility = View.GONE
                            rl_pkgs.visibility = View.GONE

                            packageid = pkglist1.get(m).packageid.toString()
                            userpostpackageid = pkglist1.get(m).userpostpackageid.toString()
                            storeUserData.setString(Constants.USER_PKG_ID, userpostpackageid.toString())
                            type = "normal"
                            posttype = "free"

                            if (cbCurrentlocation.isChecked) {
                                locationName = tv_current_loc.text.toString()
                            } else {
                                locationName = act_location.text.toString()
                            }

                            CheckCategoryAndApiCall.checkforCategory(
                                    this,
                                    storeUserData.getString(Constants.CATEGORY_ID).toInt(),
                                    storeUserData.getString(Constants.SUBCATEGORYID).toInt(),
                                    locationName.toString(),
                                    latitude.toString(),
                                    longitude.toString(),
                                    price.toString(),
                                    packageid!!
                            )
                            break
                        } else {

                            newpkglist.clear()
                            Utils.showLog(
                                    TAG,
                                    "==else=" + GsonBuilder().setPrettyPrinting().create().toJson(
                                            pkglist1
                                    )
                            )
                            //val newpkglist: ArrayList<GetMyPackagesBean.Packages> = ArrayList()

                            for (k in pkglist1.indices) {
                                if (pkglist1.get(k).packagetype.equals("More")) {
                                    newpkglist.add(pkglist1.get(k))
                                }
                            }

                            Utils.showLog(
                                    TAG,
                                    "==new list==" + GsonBuilder().setPrettyPrinting().create().toJson(
                                            newpkglist
                                    )
                            )
                            if (newpkglist.size > 0) {
                                rl_main.visibility = View.GONE
                                rl_attention.visibility = View.GONE
                                rl_pkgs.visibility = View.VISIBLE

                                bindRecyclerViewAdPostPackage(newpkglist, 0)
                            }

                            /* if(pkglist1.get(m).packagetype.equals("More"))
                            {
                                Utils.showLog(TAG,"==ad post More===" + GsonBuilder().setPrettyPrinting().create().toJson(pkglist1.get(m)))

                                packageid = pkglist1.get(m).packageid.toString()
                                type ="normal"
                                CheckCategoryAndApiCall.checkforCategory(this,storeUserData.getString(Constants.CATEGORY_ID).toInt(),storeUserData.getString(Constants.SUBCATEGORYID).toInt(),act_location.text.toString(),latitude.toString(),longitude.toString(),price.toString(), packageid!!)
                                break
                            }*/
                        }
                    }
                } else {
                    openAttentionScreen(
                            storeUserData.getString(Constants.CATEGORY_ID).toInt(),
                            storeUserData.getString(Constants.SUBCATEGORYID).toInt(),
                            act_location.text.toString(),
                            latitude.toString(),
                            longitude.toString(),
                            price.toString()
                    )
                }
            } else {
                openAttentionScreen(
                        storeUserData.getString(Constants.CATEGORY_ID).toInt(),
                        storeUserData.getString(Constants.SUBCATEGORYID).toInt(),
                        act_location.text.toString(),
                        latitude.toString(),
                        longitude.toString(),
                        price.toString()
                )
            }


        } else if (method.equals("GetExtraAd")) {
            val dataModel = ExtraAdPackageBean()
            dataModel.ExtraAdPackageBean(da)

            Log.d("GetExtraAd","setLocation : "+GsonBuilder().setPrettyPrinting().create().toJson(dataModel))


            packageid = dataModel.packageId.toString()
            userpostpackageid = dataModel.userpostpackageId.toString()
            storeUserData.setString(Constants.USER_PKG_ID, userpostpackageid.toString())

            Utils.showLog(TAG, "===pkg id==" + packageid + "==" + userpostpackageid)
            payamount = dataModel.price.toString()

            Utils.showLog(TAG, "===total price =" + payamount)

            tv_pkg_name.setText(dataModel.name)

            val ss = "<b>" + dataModel.freeAdRenewAfter + "</b>"
            tv_renew_day.text = Html.fromHtml(resources.getString(R.string.txt_due_to_free_ad) + " " + ss + " days.")
            tv_price.text = "₹ " + dataModel.price.toString()
            tv_pkg.text = resources.getString(R.string.txt_pay_rupee) + " " + dataModel.price.toString()

        } else if (method.equals("purchasePkg")) {
            Utils.showToast(this, message)

            userpostpackageid = da.getString("userpostpurchasepackage_id")
            storeUserData.setString(Constants.USER_PKG_ID, userpostpackageid.toString())
            type = "attention"
            posttype = "rpay"
            if (cbCurrentlocation.isChecked) {
                locationName = tv_current_loc.text.toString()
            } else {
                locationName = act_location.text.toString()
            }
            CheckCategoryAndApiCall.checkforCategory(this, storeUserData.getString(Constants.CATEGORY_ID).toInt(), storeUserData.getString(Constants.SUBCATEGORYID).toInt(), locationName.toString(), latitude.toString(), longitude.toString(), price.toString(), packageid!!)
        } else if (method.equals("GetLocation")) {
            locationlist.clear()
            typelist.clear()

            val dataModel = LocationListBeans()
            dataModel.LocationListBeans(da)

            locationlist = dataModel.locationList
            Utils.showLog(TAG, "==location list size==" + locationlist.size)
            typelist.add(resources.getText(R.string.txt_select_district_hint).toString())   //  In
            typeidlist.add("0")
            for (i in locationlist.indices) {
                val name = locationlist.get(i).locName
                val id = locationlist.get(i).locId
                typelist.add(name.toString())
                typeidlist.add(id.toString())
            }

            type_spinner_type = ArrayAdapter(
                    applicationContext,
                    android.R.layout.simple_spinner_dropdown_item,
                    typelist
            )
            // Creating adapter for spinner
            val dataAdapter1 = ArrayAdapter(applicationContext, android.R.layout.simple_spinner_item, typelist)

            // Drop down layout style - list view with radio button
            dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

            // attaching data adapter to spinner
            spinner_dis_type.adapter = dataAdapter1


            tv_dis_type.visibility = View.VISIBLE
            tv_dis_type.text = resources.getText(R.string.txt_select_district_hint)
            spinner_dis_type.visibility = View.GONE
            rl_dis_type.setBackgroundResource(R.drawable.bg_edittext)
        } else if (method.equals("latLong")) {
            val dataModel = LatLongBean()
            dataModel.LatLongBean(da)

//            Utils.showLog(TAG, "---lat--" + dataModel.latLngList.get(0).latitude)
//            Utils.showLog(TAG, "---long--" + dataModel.latLngList.get(0).longitude)

            dislatitude = dataModel.latLngList.get(0).latitude!!.toDouble()
            dislongitude = dataModel.latLngList.get(0).longitude!!.toDouble()
        } else if (method.equals("addresslatLong")) {
            val dataModel = LatLongBean()
            dataModel.LatLongBean(da)

//            Utils.showLog(TAG, "---add lat--" + dataModel.latLngList.get(0).latitude)
//            Utils.showLog(TAG, "---add long--" + dataModel.latLngList.get(0).longitude)



            //This if condition is newly added (old code doesn't have conditions)
            if(!dataModel.latLngList.isEmpty()){
                addlatitude = dataModel.latLngList.get(0).latitude!!.toDouble()
                addlongitude = dataModel.latLngList.get(0).longitude!!.toDouble()
            }else{
                act_location.setText("")
                Utils.showToast(this@SetLocationActivity,"Address not found kindly use another nearby location")
            }


        } else if (method.equals("suggestList")) {
            placeList.clear()
            val dataModel = LocationSuggestionBean()
            dataModel.LocationSuggestionBean(da)

            Utils.showLog(TAG, "---list size--" + dataModel.locationsList.size)

            sugplaceList = dataModel.locationsList
            for (i in sugplaceList.indices) {

                //old code
                /*if (sugplaceList.get(i).locationName.toString().contains(resources.getString(R.string.txt_gujarat))) {
                    placeList.add(sugplaceList.get(i).locationName.toString())
                }*/

                //new condition for gujarati and english language of phone settings
                if (sugplaceList.get(i).locationName.toString().contains("Gujarat") ||
                        sugplaceList.get(i).locationName.toString().contains("ગુજરાત")) {
                    placeList.add(sugplaceList.get(i).locationName.toString())
                }

            }

            initAutoCompleteTextViewSearch()
        } else {
            //AppConstants.printToast(applicationContext, message)

            val dataModel = AdPostResponseBean()
            dataModel.AdPostResponseBean(da)

            Utils.showLog(TAG, "===data===" + dataModel)
            val data = dataModel.pkglist.get(0)

            redirectToCongratulation(data)

        }
    }

    private fun redirectToCongratulation(data: AdPostResponseBean.PkgInfo) {
        storeUserData.setString(Constants.LOCATION_ID, "")
        if (posttype.equals("free")) {
            Utils.showToast(this, "Your Ad Posted Successfully")
        }
        /* if(posttype.equals("rpay"))
         {
             Utils.showToast(this,"Your Ad Posted Successfully")
         }*/
        if (posttype.equals("pkglist")) {
            Utils.showToast(this, "Package Applied Successfully")
        }




        storeUserData.setString(Constants.USER_PKG_ID, "")
        storeUserData.setString(Constants.LOC_ID_PKG, "")
        val intent = Intent(this@SetLocationActivity, CongratulationAdpostActivity::class.java)
        intent.putExtra("data", data)
        intent.putExtra("type", posttype)
        startActivity(intent)
        finishAffinity()


        /*
        Handler().postDelayed({
            if (!isFinishing) {

                storeUserData.setString(Constants.USER_PKG_ID, "")
                storeUserData.setString(Constants.LOC_ID_PKG, "")
                val intent = Intent(this@SetLocationActivity, CongratulationAdpostActivity::class.java)
                intent.putExtra("data", data)
                intent.putExtra("type", posttype)
                startActivity(intent)
                finishAffinity()
            }
        }, 1000)*/
    }

    private fun bindRecyclerViewAdPostPackage(newpkglist: ArrayList<GetMyPackagesBean.Packages>, pos: Int) {
        val recyclerLayoutManager = LinearLayoutManager(this)
        rv_Package.layoutManager = recyclerLayoutManager
        val adapter = AdPostSelectPackageAdapter(newpkglist, this)
        adapter.setClicklistner(this)
        rv_Package.adapter = adapter
        adapter.notifyDataSetChanged()
        rv_Package.scrollToPosition(pos)
    }

    private fun openAttentionScreen(catId: Int, subCatId: Int, address: String, latitude: String, longitude: String, price: String) {

        getListingController.getExtraAdPackage(storeUserData.getString(Constants.CATEGORY_ID))

        rl_main.visibility = View.GONE
        rl_attention.visibility = View.VISIBLE
        rl_pkgs.visibility = View.GONE
    }

    override fun onFail(msg: String, method: String) {
        if (method.equals("GetMyPackages")) {
            /*  val intent = Intent(applicationContext, AttentionAdPostActivity::class.java)
            startActivity(intent)
            finish()*/
            openAttentionScreen(
                    storeUserData.getString(Constants.CATEGORY_ID).toInt(),
                    storeUserData.getString(Constants.SUBCATEGORYID).toInt(),
                    act_location.text.toString(),
                    latitude.toString(),
                    longitude.toString(),
                    price.toString()
            )
        }
    }

    fun findMin(list: ArrayList<Int>): Int? {
        return list.min()
    }

    private fun startpay(price: String) {

        val activity: Activity = this
        val co = Checkout()
        co.setKeyID(activity.getString(R.string.key_razorpay))

        try {
            val options = JSONObject()

            if(storeUserData.getString(Constants.NAME).equals("")){
                options.put("name", "Gujarat Living User")
            }else{
                options.put("name", ""+storeUserData.getString(Constants.NAME))
            }


            options.put("description", "Amount")
            options.put("theme.color", "#000000");
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png")
            options.put("currency", "INR")
            options.put("amount", price)

            val prefill = JSONObject()
            prefill.put("email", ""+storeUserData.getString(Constants.EMAIL))
            prefill.put("contact", ""+storeUserData.getString(Constants.CONTACT_NO))
            //prefill.put("contact", storeUserData.getString(Constants.PHONE_NUMBER))

            options.put("prefill", prefill)
            co.open(activity, options)
        } catch (e: Exception) {
            Utils.showLog(TAG, "=============" + e.message)
            //Toast.makeText(activity, "Error in payment: " + e.message, Toast.LENGTH_LONG).show()
            e.printStackTrace()
        }
    }

    override fun onPaymentError(errocode: Int, response: String?) {
        try {
            //AppConstants.printToast(applicationContext, "Payment failed: " + errocode.toString() + " " + response)
            printLog("test", response + "Exception in onPaymentSuccess" + errocode)
            Utils.showAlert(this, getString(R.string.txt_payment_canceled_user))

        } catch (e: java.lang.Exception) {
            printLog("test", "Exception in onPaymentError" + e)
        }

        //listener.onFail(errocode.toString(),response.toString())
    }

    override fun onPaymentSuccess(razorpayPaymentId: String?) {
        try {
            //AppConstants.printToast(applicationContext, "Payment Successful: $razorpayPaymentId")

            Utils.showLog(TAG, "==purchase pkg id==" + packageid)

            if (packageid != null && packageid.toString().length > 0) {


                val c = PurchasePackageController(this, packageid!!, razorpayPaymentId.toString(),"", this)
                c.purchasePackage()
            }

        } catch (e: java.lang.Exception) {
            printLog("test", "Exception in onPaymentSuccess" + e)
        }
    }

    override fun itemClicked(pos: Int, selected: Boolean) {
        Log.d("ExtraAd0"," selected : "+ selected)

        if (selected) {
            //addButtonView(premiumpkglist[pos].price)

            isPackageSelected = true

            packageid = newpkglist[pos].packageid!!

            Log.d("ExtraAd0"," selected : "+ packageid)

            userpostpackageid = newpkglist[pos].userpostpackageid
            storeUserData.setString(Constants.USER_PKG_ID, userpostpackageid.toString())
        }else{
            isPackageSelected = false

        }
        for (i in 0 until newpkglist.size) {
            if (pos == i && selected == true) {
                newpkglist[i].selected = true
            } else {
                newpkglist[i].selected = false
            }
        }
        bindRecyclerViewAdPostPackage(newpkglist, pos)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == AUTOCOMPLETE_REQUEST_CODE_SOURCE) {
            if (resultCode == Activity.RESULT_OK) {
                val place = Autocomplete.getPlaceFromIntent(data!!)
                Log.i(TAG, "Place: " + place.name + ", " + place.id)

                val selection = place.address
                Log.e("selection_source", selection!! + "===")

                val pick_up_address = place.name.toString() + ", " + selection

//                latitude = place.latLng!!.latitude
//                longitude = place.latLng!!.longitude
                addlatitude = place.latLng!!.latitude
                addlongitude = place.latLng!!.longitude

                act_location.setText(place.name + ", " + place.address)


            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                val status = Autocomplete.getStatusFromIntent(data!!)
                Log.i(TAG, status.statusMessage!!)
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

        when (requestCode) {
            101 -> {
                checkLocationNew()
            }
        }
    }

    override fun onResume() {
        Utils.setButtonEnabled(btnnext, true)
        Utils.setButtonEnabled(btn_ad_post, true)
        Utils.setTextviewEnabled(tv_pkg, true)
        super.onResume()
    }

    private fun takepermission() {

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.INTERNET,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                ).withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(multiplePermissionsReport: MultiplePermissionsReport) {
                        if (multiplePermissionsReport.areAllPermissionsGranted()) {
                            Utils.showLog(HomeFragmentNew.TAG, "===permission checked====")
                            //getUserLocation()
                            //checkLocation()
                            checkLocationNew()
                            return
                        }
                        if (multiplePermissionsReport.isAnyPermissionPermanentlyDenied) {
                            showSettingsDialog()
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(
                            list: List<PermissionRequest>,
                            permissionToken: PermissionToken
                    ) {
                    }
                }).check()
    }

    private var locationManager: LocationManager? = null
    private var myLocationListener: LocationListener? = null

    fun isGPSEnabled(mContext: Context): Boolean {
        val locationManager = mContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    fun checkLocation() {
        try {
            val serviceString = Context.LOCATION_SERVICE
            locationManager = getSystemService(serviceString) as LocationManager?
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this@SetLocationActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Utils.showLog(TAG, "===if granted====")
                return
            }
            myLocationListener = object : LocationListener {
                override fun onLocationChanged(locationListener: Location) {
                    if (isGPSEnabled(this@SetLocationActivity)) {
                        if (locationListener != null) {
                            if (ActivityCompat.checkSelfPermission(this@SetLocationActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this@SetLocationActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                Utils.showLog(TAG, "===if if granted====")
                                return
                            }
                            if (locationManager != null) {
                                val location = locationManager!!.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                                if (location != null) {
//                                    latitude = location.getLatitude()
//                                    longitude = location.getLongitude()
                                    curlatitude = location.getLatitude()
                                    curlongitude = location.getLongitude()
                                    Log.i(TAG, "--- LATITUDE ---$curlatitude---LONGITUDE---$curlongitude")
                                    val locationAddress = LocationAddress()
                                    locationAddress.getAddressFromLocation(curlatitude, curlongitude, this@SetLocationActivity, GeoCodeHandler())
                                }
                            }
                        }
                    } else if (isInternetConnected(this@SetLocationActivity)) {
                        if (locationManager != null) {
                            val location = locationManager!!
                                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                            if (location != null) {
//                                latitude = location.getLatitude()
//                                longitude = location.getLongitude()
                                curlatitude = location.getLatitude()
                                curlongitude = location.getLongitude()
                                Log.i(TAG, "--- LATITUDE ---$curlatitude---LONGITUDE---$curlongitude")
                                val locationAddress = LocationAddress()
                                locationAddress.getAddressFromLocation(curlatitude, curlongitude, this@SetLocationActivity, GeoCodeHandler())
                            }
                        }
                    }
                }

                override fun onProviderDisabled(provider: String) {}
                override fun onProviderEnabled(provider: String) {}
                override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
            }
            locationManager!!.requestLocationUpdates(LocationManager.GPS_PROVIDER, 300000, 1f, myLocationListener as LocationListener)
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    internal inner class GeoCodeHandler : Handler() {
        override fun handleMessage(message: Message) {
            val locationAddress: String
            locationAddress = when (message.what) {
                1 -> {
                    val bundle = message.data
                    bundle.getString("address").toString()
                }
                /*else -> null.toString()*/
                else -> "Gujarat"
            }
            Utils.showLog(TAG, "---- address---" + locationAddress)
            //tv_current_loc.setText(locationAddress)

            if (locationAddress.equals("null") || locationAddress.isEmpty()) {
                tv_current_loc.setText("Searching Location")
            } else {
                tv_current_loc.setText(locationAddress)
            }
            Log.i(TAG, "======= CURRENT LOCATION =========" + locationAddress + "Lattitude : " + curlatitude + "Longitude : " + curlongitude)

        }
    }

    fun isInternetConnected(ctx: Context): Boolean {
        val connectivityMgr = ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val wifi = connectivityMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
        val mobile = connectivityMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
        // Check if wifi or mobile network is available or not. If any of them is
        // available or connected then it will return true, otherwise false;
        if (wifi != null) {
            if (wifi.isConnected) {
                return true
            }
        }
        if (mobile != null) {
            if (mobile.isConnected) {
                return true
            }
        }
        return false
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Need Permissions")
        builder.setMessage("This app needs permission to use this application. You can grant them in app settings.")
        builder.setPositiveButton(
                "GOTO SETTINGS"
        ) { dialog, which ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton(
                "Cancel"
        ) { dialog, which -> dialog.cancel() }
        builder.show()



    }

    // navigating user to app settings
    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", getPackageName(), null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }

    override fun onBackPressed() {
        super.onBackPressed()

        if (storeUserData.getString(Constants.CATEGORY_ID).toInt() == 7 || storeUserData.getString(Constants.CATEGORY_ID).toInt() == 10 || storeUserData.getString(Constants.CATEGORY_ID).toInt() == 11 || storeUserData.getString(Constants.CATEGORY_ID).toInt() == 12) {

            if (AppConstants.imageListuri != null && AppConstants.imageListuri.size > 0) {
                Log.e("imageListuri2===", AppConstants.imageListuri.toString())

                TedImagePicker.with(this)
                        .max(12, "Maximum 12 photos allowed")
                        .errorListener { message ->
                            Log.e("ted", "message: $message")
                        }
                        .selectedUri(AppConstants.imageListuri)
                        .startMultiImage { list: List<Uri> ->
                            Log.e("list===", "listsize: ${list.size}")
                            AppConstants.showMultiImage1(list, applicationContext, storeUserData.getString(Constants.CATEGORY_ID).toInt(),
                                    storeUserData.getString(Constants.SUBCATEGORYID).toInt())
                        }
            }
        }
    }

}

