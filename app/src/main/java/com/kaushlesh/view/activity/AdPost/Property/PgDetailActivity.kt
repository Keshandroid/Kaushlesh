package com.kaushlesh.view.activity.AdPost.Property

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.ParcelFileDescriptor
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.kaushlesh.R
import com.kaushlesh.bean.AdPost.ApVillsellandRentBean
import com.kaushlesh.bean.AdPost.PgGuestHouseBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.utils.AdPostImages
import com.kaushlesh.utils.validations.AdPostPropertyValidation
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.AdPost.UploadActivity1
import com.kaushlesh.view.activity.AdPost.UploadPhotoActivity
import gun0912.tedimagepicker.builder.TedImagePicker
import kotlinx.android.synthetic.main.activity_ap_villa_sell_and_rent.*
import kotlinx.android.synthetic.main.activity_pg_detail.*
import kotlinx.android.synthetic.main.activity_pg_detail.et_ad_title
import kotlinx.android.synthetic.main.activity_pg_detail.et_other_info
import kotlinx.android.synthetic.main.activity_pg_detail.tv_1
import java.io.*

class PgDetailActivity : AppCompatActivity(), View.OnClickListener {
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal lateinit var btnnext: Button
    internal lateinit var etname: EditText
    internal lateinit var tvfurnished: TextView
    internal lateinit var tvsemifurnished: TextView
    internal lateinit var tvunfurnished: TextView
    internal lateinit var tvbuilder: TextView
    internal lateinit var tvdealer: TextView
    internal lateinit var tvowner: TextView
    internal lateinit var tvguesthouse: TextView
    internal lateinit var tvpg: TextView
    internal lateinit var tvroommate: TextView
    internal lateinit var tvcp1: TextView
    internal lateinit var tvcp2: TextView
    internal lateinit var tvcp3: TextView
    internal lateinit var tvcp4: TextView
    internal lateinit var tvcp5: TextView
    internal lateinit var ivmyselect: ImageView
    internal lateinit var ivmyunselect: ImageView
    internal lateinit var ivmnselect: ImageView
    internal lateinit var ivmnunselect: ImageView
    internal lateinit var ivacyselect: ImageView
    internal lateinit var ivacyunselect: ImageView
    internal lateinit var ivacnselect: ImageView
    internal lateinit var ivacnunselect: ImageView
    internal lateinit var etadtitle: EditText
    internal lateinit var etotherinfo: EditText

    internal var furnished: Boolean? = false
    internal var semifurnished: Boolean? = false
    internal var unfurnished: Boolean? = false
    internal var builder: Boolean? = false
    internal var dealer: Boolean? = false
    internal var owner: Boolean? = false
    internal var cp1: Boolean? = false
    internal var cp2: Boolean? = false
    internal var cp3: Boolean? = false
    internal var cp4: Boolean? = false
    internal var cp5: Boolean? = false
    internal var meal_yes: Boolean? = true
    internal var meal_no: Boolean? = false
    internal var ac_yes: Boolean? = true
    internal var ac_no: Boolean? = false
    internal var guesthouse: Boolean? = false
    internal var pg: Boolean? = false
    internal var roommmate: Boolean? = false

    lateinit var adGuestHouseList: PgGuestHouseBean
    lateinit var storeUserData: StoreUserData
    var isallow : Boolean = false
    lateinit var resizebitmapmain: Bitmap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pg_detail)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        storeUserData = StoreUserData(this)
        adGuestHouseList = PgGuestHouseBean()

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.txt_include_details)

        initBindViews()
    }

    private fun initBindViews() {
        btnnext = findViewById(R.id.btn_next)

        etname = findViewById(R.id.et_name)
        tvfurnished = findViewById(R.id.tvfurnished)
        tvsemifurnished = findViewById(R.id.tvsemifurnished)
        tvunfurnished = findViewById(R.id.tvunfurnished)
        tvbuilder = findViewById(R.id.tvbuilder)
        tvdealer = findViewById(R.id.tvdealer)
        tvowner = findViewById(R.id.tvowner)

        tvguesthouse = findViewById(R.id.tvguesthouse)
        tvpg = findViewById(R.id.tvpg)
        tvroommate = findViewById(R.id.tvroommate)

        tvcp1 = findViewById(R.id.tvcar0)
        tvcp2 = findViewById(R.id.tvcar1)
        tvcp3 = findViewById(R.id.tvcar2)
        tvcp4 = findViewById(R.id.tvcar3)
        tvcp5 = findViewById(R.id.tvcar4)
        ivmyselect = findViewById(R.id.iv_myes_select)
        ivmyunselect = findViewById(R.id.iv_myes_unselect)
        ivmnselect = findViewById(R.id.iv_mno_select)
        ivmnunselect = findViewById(R.id.iv_mno_unselect)
        ivacyselect = findViewById(R.id.iv_acyes_select)
        ivacyunselect = findViewById(R.id.iv_acyes_unselect)
        ivacnselect = findViewById(R.id.iv_acno_select)
        ivacnunselect = findViewById(R.id.iv_acno_unselect)
        etadtitle = findViewById(R.id.et_ad_title)
        etotherinfo = findViewById(R.id.et_other_info)

        btnback.setOnClickListener(this)
        btnnext.setOnClickListener(this)

        tvfurnished.setOnClickListener(this)
        tvsemifurnished.setOnClickListener(this)
        tvunfurnished.setOnClickListener(this)

        tvbuilder.setOnClickListener(this)
        tvdealer.setOnClickListener(this)
        tvowner.setOnClickListener(this)

        tvguesthouse.setOnClickListener(this)
        tvpg.setOnClickListener(this)
        tvroommate.setOnClickListener(this)

        tvcp1.setOnClickListener(this)
        tvcp2.setOnClickListener(this)
        tvcp3.setOnClickListener(this)
        tvcp4.setOnClickListener(this)
        tvcp5.setOnClickListener(this)

        ivmyunselect.setOnClickListener(this)
        ivmnunselect.setOnClickListener(this)

        ivacyunselect.setOnClickListener(this)
        ivacnunselect.setOnClickListener(this)

        //setbackgroundSelected(R.drawable.bg_edittext_black)

        Utils.setontouch(et_other_info)
        Utils.setontouch1(et_ad_title)

        etadtitle.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {


                tv_1.text = s.length.toString() + "/77"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })

        etotherinfo.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {


                tv_2.text = s.length.toString() + "/5000"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })

        adGuestHouseList.property_type= ""
        adGuestHouseList.furnishing = ""
        adGuestHouseList.listed_by= ""
        adGuestHouseList.car_parking ="0"
        adGuestHouseList.meals_included= "1"
        adGuestHouseList.ac= "1"
    }

    private fun setbackgroundSelected(b: Int) {
        tvfurnished.setBackgroundResource(b)
        tvguesthouse.setBackgroundResource(b)
        tvowner.setBackgroundResource(b)
        tvcp1.setBackgroundResource(b)
    }

    override fun onClick(view: View) {
        when (view.id) {

            R.id.btn_back -> onBackPressed()

            R.id.btn_next -> {
                //val intent = Intent(applicationContext, UploadPhotoActivity::class.java)
                //startActivity(intent)

                validations()

            }

            R.id.tvfurnished -> {
                setTypeValue(true, false, false, "furnish")
                setTypeBottomSelection(
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    "furnish"
                )
                adGuestHouseList.furnishing = "1"
            }

            R.id.tvsemifurnished -> {
                setTypeValue(false, true, false, "furnish")
                setTypeBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    "furnish"
                )
                adGuestHouseList.furnishing = "2"
            }

            R.id.tvunfurnished -> {
                setTypeValue(false, false, true, "furnish")
                setTypeBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    "furnish"
                )
                adGuestHouseList.furnishing = "3"
            }

            R.id.tvowner -> {
                setTypeValue(true, false, false, "listedby")
                setTypeBottomSelection(
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    "listedby"
                )
                adGuestHouseList.listed_by= "3"
            }

            R.id.tvdealer -> {
                setTypeValue(false, true, false, "listedby")
                setTypeBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    "listedby"
                )
                adGuestHouseList.listed_by= "2"
            }

            R.id.tvbuilder -> {
                setTypeValue(false, false, true, "listedby")
                setTypeBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    "listedby"
                )
                adGuestHouseList.listed_by= "1"
            }

            R.id.tvguesthouse -> {
                setTypeValue(true, false, false, "type")
                setTypeBottomSelection(
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    "type"
                )
                adGuestHouseList.property_type= "12"
            }

            R.id.tvpg -> {
                setTypeValue(false, true, false, "type")
                setTypeBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    "type"
                )
                adGuestHouseList.property_type= "13"
            }

            R.id.tvroommate -> {
                setTypeValue(false, false, true, "type")
                setTypeBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    "type"
                )
                adGuestHouseList.property_type= "14"
            }

            R.id.tvcar0 -> {

                if(cp1!!)
                {
                    setBedroomValue(false, false, false, false, false, "car")
                    setcar()
                }
                else {
                    setBedroomValue(true, false, false, false, false, "car")
                    setBedroomBottomSelection(
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.color.black,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        "car"
                    )
                    //adGuestHouseList.car_parking = "0"
                    adGuestHouseList.car_parking = "1"
                }
            }

            R.id.tvcar1 -> {

                if(cp2!!)
                {
                    setBedroomValue(false, false, false, false, false, "car")
                    setcar()
                }
                else {
                    setBedroomValue(false, true, false, false, false, "car")
                    setBedroomBottomSelection(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.color.gray,
                        R.color.black,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        "car"
                    )
                    //adGuestHouseList.car_parking = "1"
                    adGuestHouseList.car_parking = "2"
                }
            }

            R.id.tvcar2 -> {
                if(cp3!!)
                {
                    setBedroomValue(false, false, false, false, false, "car")
                    setcar()
                }
                else {
                    setBedroomValue(false, false, true, false, false, "car")
                    setBedroomBottomSelection(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.color.gray,
                        R.color.gray,
                        R.color.black,
                        R.color.gray,
                        R.color.gray,
                        "car"
                    )
                    //adGuestHouseList.car_parking = "2"
                    adGuestHouseList.car_parking = "3"
                }
            }

            R.id.tvcar3 -> {

                if(cp4!!)
                {
                    setBedroomValue(false, false, false, false, false, "car")
                    setcar()
                }
                else {
                    setBedroomValue(false, false, false, true, false, "car")
                    setBedroomBottomSelection(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        R.color.black,
                        R.color.gray,
                        "car"
                    )
                    //adGuestHouseList.car_parking = "3"
                    adGuestHouseList.car_parking = "4"
                }
            }

            R.id.tvcar4 -> {
                if(cp5!!)
                {
                    setBedroomValue(false, false, false, false, false, "car")
                    setcar()
                }
                else {
                    setBedroomValue(false, false, false, false, true, "car")
                    setBedroomBottomSelection(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        R.color.black,
                        "car"
                    )
                    //adGuestHouseList.car_parking = "4"
                    adGuestHouseList.car_parking = "5"
                }
            }

            R.id.iv_myes_unselect -> {
                meal_yes = true
                meal_no = false
                ivmyselect.visibility = View.VISIBLE
                ivmnselect.visibility = View.GONE
                adGuestHouseList.meals_included = "1"
            }

            R.id.iv_mno_unselect -> {
                meal_yes = false
                meal_no = true
                ivmyselect.visibility = View.GONE
                ivmnselect.visibility = View.VISIBLE
                adGuestHouseList.meals_included = "2"
            }

            R.id.iv_acyes_unselect -> {
                ac_yes = true
                ac_no = false
                ivacyselect.visibility = View.VISIBLE
                ivacnselect.visibility = View.GONE
                adGuestHouseList.ac = "1"
            }

            R.id.iv_acno_unselect -> {
                ac_yes = false
                ac_no = true
                ivacyselect.visibility = View.GONE
                ivacnselect.visibility = View.VISIBLE
                adGuestHouseList.ac = "2"
            }
        }
    }

    private fun setcar() {

        setBedroomBottomSelection(
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.color.gray,
            R.color.gray,
            R.color.gray,
            R.color.gray,
            R.color.gray,
            "car"
        )
        adGuestHouseList.car_parking= "0"
    }

    private fun setTypeValue(tb1: Boolean, tb2: Boolean, tb3: Boolean, type: String) {

        if (type.equals("furnish", ignoreCase = true)) {
            furnished = tb1
            semifurnished = tb2
            unfurnished = tb3
        }

        if (type.equals("listedby", ignoreCase = true)) {
            owner = tb1
            dealer = tb2
            builder = tb3
        }

        if (type.equals("type", ignoreCase = true)) {
            guesthouse = tb1
            pg = tb2
            roommmate = tb3
        }
    }

    private fun setTypeBottomSelection(
        t1: Int,
        t2: Int,
        t3: Int,
        t4: Int,
        t5: Int,
        t6: Int,
        type: String
    ) {

        if (type.equals("furnish", ignoreCase = true)) {
            tvfurnished.setBackgroundResource(t1)
            tvsemifurnished.setBackgroundResource(t2)
            tvunfurnished.setBackgroundResource(t3)

            tvfurnished.setTextColor(resources.getColor(t4))
            tvsemifurnished.setTextColor(resources.getColor(t5))
            tvunfurnished.setTextColor(resources.getColor(t6))
        }

        if (type.equals("listedby", ignoreCase = true)) {
            tvowner.setBackgroundResource(t1)
            tvdealer.setBackgroundResource(t2)
            tvbuilder.setBackgroundResource(t3)

            tvowner.setTextColor(resources.getColor(t4))
            tvdealer.setTextColor(resources.getColor(t5))
            tvbuilder.setTextColor(resources.getColor(t6))
        }

        if (type.equals("type", ignoreCase = true)) {
            tvguesthouse.setBackgroundResource(t1)
            tvpg.setBackgroundResource(t2)
            tvroommate.setBackgroundResource(t3)

            tvguesthouse.setTextColor(resources.getColor(t4))
            tvpg.setTextColor(resources.getColor(t5))
            tvroommate.setTextColor(resources.getColor(t6))
        }
    }

    private fun setBedroomBottomSelection(
        bd1: Int,
        bd2: Int,
        bd3: Int,
        bd4: Int,
        bd5: Int,
        bd6: Int,
        bd7: Int,
        bd8: Int,
        bd9: Int,
        bd10: Int,
        type: String
    ) {

        if (type.equals("car", ignoreCase = true)) {
            tvcp1.setBackgroundResource(bd1)
            tvcp2.setBackgroundResource(bd2)
            tvcp3.setBackgroundResource(bd3)
            tvcp4.setBackgroundResource(bd4)
            tvcp5.setBackgroundResource(bd5)

            tvcp1.setTextColor(resources.getColor(bd6))
            tvcp2.setTextColor(resources.getColor(bd7))
            tvcp3.setTextColor(resources.getColor(bd8))
            tvcp4.setTextColor(resources.getColor(bd9))
            tvcp5.setTextColor(resources.getColor(bd10))
        }
    }

    private fun setBedroomValue(
        b1: Boolean,
        b2: Boolean,
        b3: Boolean,
        b4: Boolean,
        b5: Boolean,
        type: String
    ) {

        if (type.equals("car", ignoreCase = true)) {
            cp1 = b1
            cp2 = b2
            cp3 = b3
            cp4 = b4
            cp5 = b5
        }
    }

    companion object {

        private val TAG = "PgDetailActivity"
    }

    private fun validations(){

        val issubmit = AdPostPropertyValidation.checkForPgGuestHouse(this,adGuestHouseList,etadtitle,etotherinfo)
        Utils.showLog(TAG,"status" +  issubmit)
        if(issubmit) {
            adGuestHouseList.category_id = storeUserData.getString(Constants.CATEGORY_ID)
            adGuestHouseList.sub_category_id = storeUserData.getString(Constants.SUBCATEGORYID)

            adGuestHouseList.title= etadtitle.text.toString()
            adGuestHouseList.other_information = etotherinfo.text.toString()

            val gson = Gson()
            val json = gson.toJson(adGuestHouseList)
            storeUserData.setString(Constants.ADDPGGUSTHOUSE,json)

            Log.e("save", "adOfficeForSell save data: "+storeUserData.getString(Constants.ADDPGGUSTHOUSE))

            //val intent = Intent(applicationContext, UploadPhotoActivity::class.java)
            //startActivity(intent)

            setNormalMultiButton()
        }
    }

    fun setNormalMultiButton() {
        val selectedUriList: List<Uri>? = null

        TedImagePicker.with(this)
                .max(12, "Maximum 12 photos allowed")
                .errorListener { message ->
                    Log.d("ted", "message: $message")
                }
                .selectedUri(selectedUriList)
                .startMultiImage { list: List<Uri> ->
                    Log.e("list===", "listsize: ${list.size}")
                    showMultiImage1(list)
                }

    }

    private fun showMultiImage1(uriList: List<Uri>) {

        Log.d("ted=", "uriList: $uriList")
        Log.e("uriList.get(0)====", "uriList.get(0): $uriList.get(0)")

        Thread(Runnable {
            AppConstants.imageList.clear()
            AppConstants.imageListuri.clear()
            uriList.forEach {
                AppConstants.imageListuri.add(it)
                val imagepath = getPathFromGooglePhotosUri(it)
                resizebitmapmain = decodeImageFromFiles(imagepath, 280, 280)
                Log.e("resizebitmapmain====", "01--: $resizebitmapmain")
                val ei = ExifInterface(imagepath.toString())
                val orientation: Int = ei.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED
                )
                var rotatedBitmap: Bitmap? = null

                when (orientation) {
                    ExifInterface.ORIENTATION_ROTATE_90 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 90F)
                    ExifInterface.ORIENTATION_ROTATE_180 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 180F)
                    ExifInterface.ORIENTATION_ROTATE_270 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 270F)
                    ExifInterface.ORIENTATION_NORMAL -> rotatedBitmap = resizebitmapmain
                    else -> rotatedBitmap = resizebitmapmain
                }

                AppConstants.imageList.add(rotatedBitmap!!)
            }

            Log.e("imageListsize====", "viewSize: ${AppConstants.imageList.size}")

            if(AppConstants.imageList!=null)
            {
                Log.e("AppConstantsimageList==", "viewSize: ${AppConstants.imageList.size}")
                checkAndSavePg(this)
            }

        }).start()

        AdPostImages.changeScreentoNext(this)

    }

    private fun checkAndSavePg(context: Context) {
        ///  bimglist.clear()
        val storeUserData = StoreUserData(context)
        val gson = Gson()

        if (AppConstants.imageList.size >= 1) {

            Log.e("test", "imagelist: " + AppConstants.imageList.size+"===")

            val json: String = storeUserData.getString(Constants.ADDPGGUSTHOUSE)

            Log.e("String", "String data: " + json+"====")

            val pgGuestHouseBean = gson.fromJson(json, PgGuestHouseBean::class.java)

            Log.e("test", "listsave data: " + pgGuestHouseBean.toString()+"====")
            pgGuestHouseBean.post_images?.clear()
            pgGuestHouseBean.post_images = AppConstants.imageList
            Log.e("test", "post_images: " +  pgGuestHouseBean.post_images!!.size+"===")


            Log.e("test", "post_images data: " + pgGuestHouseBean.post_images)

            val jsonset = gson.toJson(pgGuestHouseBean)

            storeUserData.setString(Constants.ADDPGGUSTHOUSE, jsonset)

            ////   AdPostImages.changeScreentoLocation(context)

        } else {
            Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
        }
    }

    fun rotateImage(source: Bitmap, angle: Float): Bitmap? {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(
                source, 0, 0, source.width, source.height,
                matrix, true
        )
    }

    fun decodeImageFromFiles(path: String?, width: Int, height: Int): Bitmap {
        val scaleOptions = BitmapFactory.Options()
        scaleOptions.inJustDecodeBounds = true
        BitmapFactory.decodeFile(path, scaleOptions)
        var scale = 1
        while (scaleOptions.outWidth / scale / 2 >= width
                && scaleOptions.outHeight / scale / 2 >= height
        ) {
            scale *= 2
        }
        // decode with the sample size
        val outOptions = BitmapFactory.Options()
        outOptions.inSampleSize = scale
        val bitmap: Bitmap = BitmapFactory.decodeFile(path, outOptions)
        Utils.showLog(
                UploadActivity1.TAG,
                "===BitmapFactory===width" + bitmap.width + "===height" + bitmap.height + "==="
        )

        return bitmap
    }

    fun getPathFromGooglePhotosUri(uriPhoto: Uri?): String? {
        if (uriPhoto == null) return null
        var input: FileInputStream? = null
        var output: FileOutputStream? = null
        try {
            val pfd: ParcelFileDescriptor? = contentResolver.openFileDescriptor(uriPhoto, "r")
            val fd: FileDescriptor = pfd!!.getFileDescriptor()
            input = FileInputStream(fd)
            val tempFilename: String = getTempFilename(this)
            output = FileOutputStream(tempFilename)
            var read: Int = 0
            val bytes = ByteArray(4096)
            while (input.read(bytes).also({ read = it }) != -1) {
                output.write(bytes, 0, read)
            }
            return tempFilename
        } catch (ignored: IOException) {
            // Nothing we can do
        } finally {
            closeSilently(input)
            closeSilently(output)
        }
        return null
    }

    @Throws(IOException::class)
    private fun getTempFilename(context: Context): String {
        val outputDir = context.cacheDir
        val outputFile = File.createTempFile("image", "tmp", outputDir)
        return outputFile.absolutePath
    }

    fun closeSilently(c: Closeable?) {
        if (c == null) return
        try {
            c.close()
        } catch (t: Throwable) {
            // Do nothing
        }
    }
}

