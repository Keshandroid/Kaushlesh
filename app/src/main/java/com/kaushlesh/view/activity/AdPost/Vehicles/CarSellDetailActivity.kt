package com.kaushlesh.view.activity.AdPost.Vehicles

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.ParcelFileDescriptor
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.R
import com.kaushlesh.bean.AdPost.AdPostVehiclesBean
import com.kaushlesh.bean.MobileBrandBeans
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.constant.AppConstants.printLog
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.reusable.DateTextFormatter
import com.kaushlesh.utils.AdPostImages
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.utils.validations.AdPostVehiclesValidation
import com.kaushlesh.view.activity.AdPost.UploadActivity1
import com.kaushlesh.view.activity.AdPost.UploadPhotoActivity
import gun0912.tedimagepicker.builder.TedImagePicker
import kotlinx.android.synthetic.main.activity_add_brand_detail.*
import org.json.JSONObject
import java.io.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class CarSellDetailActivity : AppCompatActivity(), View.OnClickListener , ParseControllerListener {
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal lateinit var rltype: RelativeLayout
    internal lateinit var sptype: Spinner
    internal lateinit var tvtype: TextView
    internal lateinit var ibtype: ImageButton
    internal lateinit var btnnext: Button
    internal lateinit var etadtitle: EditText
    internal lateinit var etotherinfo: EditText
    internal lateinit var tv1: TextView
    internal lateinit var tv2: TextView
    internal lateinit var etyear: EditText
    internal lateinit var etkm: EditText
    internal lateinit var etinsurance: EditText
    internal lateinit var type_spinner_type: ArrayAdapter<String>
    internal lateinit var tvcng: TextView
    internal lateinit var tvdeisel: TextView
    internal lateinit var tvlpg: TextView
    internal lateinit var tvpetrol: TextView
    internal lateinit var tvelectric: TextView
    internal lateinit var tvautomatic: TextView
    internal lateinit var tvmaual: TextView
    internal lateinit var tvfirst: TextView
    internal lateinit var tvsecond: TextView
    internal lateinit var tvthird: TextView
    internal lateinit var tvfourth: TextView
    internal lateinit var tvfourthp: TextView
    internal lateinit var tvcomp: TextView
    internal lateinit var tvthirdparty: TextView
    internal lateinit var tvzerodep: TextView

    internal var lpg: Boolean? = false
    internal var petrol: Boolean? = false
    internal var cng: Boolean? = false
    internal var diesel: Boolean? = false
    internal var electric: Boolean? = false
    internal var comp: Boolean? = false
    internal var thirdparty: Boolean? = false
    internal var zerodep: Boolean? = false
    internal var automatic: Boolean? = false
    internal var manual: Boolean? = false
    internal var first: Boolean? = false
    internal var second: Boolean? = false
    internal var third: Boolean? = false
    internal var fourth: Boolean? = false
    internal var fourthp: Boolean? = false

    internal var typelist: MutableList<String> = ArrayList()
    internal var typeidlist: MutableList<String> = ArrayList()

    lateinit var storeUserData: StoreUserData
    lateinit var adPostVehiclesBean: AdPostVehiclesBean
    lateinit var getListingController: GetListingController
    internal var brandlist: ArrayList<MobileBrandBeans.MobileBrandBeansList> = ArrayList()
    internal lateinit var date: DatePickerDialog.OnDateSetListener
    private val myCalendar = Calendar.getInstance()
    lateinit var resizebitmapmain: Bitmap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_car_sell_detail)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.txt_include_details)

        storeUserData=StoreUserData(this)
        adPostVehiclesBean= AdPostVehiclesBean()
        getListingController = GetListingController(this, this)

        initBindViews()

        Utils.setontouch(et_other_info)
        Utils.setontouch1(et_ad_title)

        getListingController.getBrandList()


        date = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            view.maxDate = myCalendar.timeInMillis
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            updateLabelsdate()
        }
    }

    private fun initBindViews() {

        rltype = findViewById(R.id.rl_brand_type)
        sptype = findViewById(R.id.spinner_brand_type)
        tvtype = findViewById(R.id.tv_brand_type)
        ibtype = findViewById(R.id.ib_brand_type)

        btnnext = findViewById(R.id.btn_next)
        etadtitle = findViewById(R.id.et_ad_title)
        etotherinfo = findViewById(R.id.et_other_info)
        tv1 = findViewById(R.id.tv_1)
        tv2 = findViewById(R.id.tv_2)
        etyear = findViewById(R.id.et_year)
        etkm = findViewById(R.id.et_km_driven)
        etinsurance = findViewById(R.id.et_insurance_validity)

        tvcng = findViewById(R.id.tvcng)
        tvlpg = findViewById(R.id.tvlpg)
        tvpetrol = findViewById(R.id.tvpetrol)
        tvdeisel = findViewById(R.id.tvdiesel)
        tvelectric = findViewById(R.id.tvelectric)
        tvautomatic = findViewById(R.id.tvautomatic)
        tvmaual = findViewById(R.id.tvmanual)
        tvfirst = findViewById(R.id.tvfirst)
        tvsecond = findViewById(R.id.tvsecond)
        tvthird = findViewById(R.id.tvthird)
        tvfourth = findViewById(R.id.tvfourth)
        tvfourthp = findViewById(R.id.tvfourthp)
        tvcomp = findViewById(R.id.tvcomprehensive)
        tvthirdparty = findViewById(R.id.tvthirdparty)
        tvzerodep = findViewById(R.id.tvzerodep)

        //setbackgroundSelected(R.drawable.bg_edittext_black)

        btnback.setOnClickListener(this)
        btnnext.setOnClickListener(this)
        rltype.setOnClickListener(this)
        ibtype.setOnClickListener(this)

        tvcng.setOnClickListener(this)
        tvlpg.setOnClickListener(this)
        tvpetrol.setOnClickListener(this)
        tvdeisel.setOnClickListener(this)
        tvelectric.setOnClickListener(this)

        tvcomp.setOnClickListener(this)
        tvthirdparty.setOnClickListener(this)
        tvzerodep.setOnClickListener(this)

        tvautomatic.setOnClickListener(this)
        tvmaual.setOnClickListener(this)

        tvfirst.setOnClickListener(this)
        tvsecond.setOnClickListener(this)
        tvthird.setOnClickListener(this)
        tvfourth.setOnClickListener(this)
        tvfourthp.setOnClickListener(this)

        etinsurance.addTextChangedListener(DateTextFormatter(etinsurance, "##.##.##"))

        etinsurance.setOnClickListener(this)

        //typelist.add("Brand 1")
        //typelist.add("Brand 2")

      /*  type_spinner_type = ArrayAdapter(
            applicationContext,
            android.R.layout.simple_spinner_dropdown_item,
            typelist
        )


        // Creating adapter for spinner
        val dataAdapter1 =
            ArrayAdapter(applicationContext, android.R.layout.simple_spinner_item, typelist)

        // Drop down layout style - list view with radio button
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        // attaching data adapter to spinner
        sptype.adapter = dataAdapter1*/

        sptype.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                printLog(TAG, "onItemSelected: ===" + adapterView.selectedItem)
                printLog(TAG, "onItemSelected: ===" + typeidlist.get(position))
                adPostVehiclesBean.brandId = typeidlist.get(position)
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        }

        etadtitle.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {


                tv1.text = s.length.toString() + "/77"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })

        etotherinfo.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {


                tv2.text = s.length.toString() + "/5000"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })


    }

    private fun setbackgroundSelected(b: Int) {

        tvcng.setBackgroundResource(b)
        tvautomatic.setBackgroundResource(b)
        tvfirst.setBackgroundResource(b)
        tvcomp.setBackgroundResource(b)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_back -> onBackPressed()

            R.id.et_insurance_validity ->
            {
                val datePickerDialog = DatePickerDialog(this, R.style.DatePickerDialogTheme, date,
                    myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH))
                datePickerDialog.datePicker.minDate = System.currentTimeMillis() - 1000
                datePickerDialog.show()
            }

            R.id.btn_next -> {

                //Utils.showLog(TAG,"==date==" +isValidFormat("dd.MM.yy",etinsurance.text.toString()))

               /* val c = Calendar.getInstance()
                val sdf1 = SimpleDateFormat("dd.MM.yy")
                val getCurrentDateTime = sdf1.format(c.time)

                Utils.showLog(TAG,"==date==" +CheckDates(getCurrentDateTime,etinsurance.text.toString()))*/

                //val intent = Intent(applicationContext, UploadPhotoActivity::class.java)
                //startActivity(intent)

                validations()
            }

            R.id.rl_type -> {

                tvtype.visibility = View.GONE
                sptype.visibility = View.VISIBLE
                sptype.performClick()
            }

            R.id.ib_type -> {

                tvtype.visibility = View.GONE
                sptype.visibility = View.VISIBLE
                rltype.performClick()
            }

            R.id.tvcng -> {

                /*if(cng!!)
                {
                    setTypeValueFive(false, false, false, false,false,"fuel")
                    setFuel()
                }
                else {*/
                    setTypeValueFive(true, false, false, false, false, "fuel")
                    setTypeBottomSelectionFive(
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.color.black,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        "fuel"
                    )
                    adPostVehiclesBean.fuel = "1"
               // }
            }

            R.id.tvpetrol -> {

               /* if(petrol!!)
                {
                    setTypeValueFive(false, false, false, false,false,"fuel")
                    setFuel()
                }
                else {*/
                    setTypeValueFive(false, true, false, false, false, "fuel")
                    setTypeBottomSelectionFive(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.color.gray,
                        R.color.black,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        "fuel"
                    )

                    adPostVehiclesBean.fuel = "2"
               // }
            }

            R.id.tvdiesel -> {

              /*  if(diesel!!)
                {
                    setTypeValueFive(false, false, false, false,false,"fuel")
                    setFuel()
                }
                else {*/
                    setTypeValueFive(false, false, true, false, false, "fuel")
                    setTypeBottomSelectionFive(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.color.gray,
                        R.color.gray,
                        R.color.black,
                        R.color.gray,
                        R.color.gray,
                        "fuel"
                    )
                    adPostVehiclesBean.fuel = "3"
               // }
            }

            R.id.tvelectric -> {

              /*  if(electric!!)
                {
                    setTypeValueFive(false, false, false, false,false,"fuel")
                    setFuel()
                }
                else {*/
                    setTypeValueFive(false, false, false, true, false, "fuel")
                    setTypeBottomSelectionFive(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        R.color.black,
                        R.color.gray,
                        "fuel"
                    )
                    adPostVehiclesBean.fuel = "5"
               // }
            }

            R.id.tvlpg -> {

               /* if(lpg!!)
                {
                    setTypeValueFive(false, false, false, false,false,"fuel")
                    setFuel()
                }
                else {*/
                    setTypeValueFive(false, false, false, false, true, "fuel")
                    setTypeBottomSelectionFive(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        R.color.black,
                        "fuel"
                    )
                    adPostVehiclesBean.fuel = "4"
               // }
            }

            R.id.tvcomprehensive -> {

                if(comp!!)
                {
                    setType()
                }
                else {
                    setTypeValue(true, false, false, "type")
                    setTypeBottomSelection(
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.color.black,
                        R.color.gray,
                        R.color.gray,
                        "type"
                    )
                    adPostVehiclesBean.insuranceType = "1"
                }
            }

            R.id.tvthirdparty -> {

                if(thirdparty!!)
                {
                    setType()
                }
                else {
                    setTypeValue(false, true, false, "type")
                    setTypeBottomSelection(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.color.gray,
                        R.color.black,
                        R.color.gray,
                        "type"
                    )
                    adPostVehiclesBean.insuranceType = "2"
                }
            }

            R.id.tvzerodep -> {

                if(zerodep!!)
                {
                    setType()
                }
                else {
                    setTypeValue(false, false, true, "type")
                    setTypeBottomSelection(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.color.gray,
                        R.color.gray,
                        R.color.black,
                        "type"
                    )
                    adPostVehiclesBean.insuranceType = "3"
                }
            }

            R.id.tvautomatic -> {
                setValue(true, false, "type")
                setBottomSelection(
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.color.black,
                    R.color.gray,
                    "type"
                )
                adPostVehiclesBean.transmission = "1"
            }

            R.id.tvmanual -> {
                setValue(false, true, "type")
                setBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.black,
                    "type"
                )
                adPostVehiclesBean.transmission = "2"
            }

            R.id.tvfirst -> {
                setTypeValueFive(true, false, false, false, false,"facing")
                setTypeBottomSelectionFive(
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    R.color.gray,
                    R.color.gray,
                    "facing"
                )
                adPostVehiclesBean.noOfowners = "1"
            }

            R.id.tvsecond -> {
                setTypeValueFive(false, true, false, false, false,"facing")
                setTypeBottomSelectionFive(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    R.color.gray,
                    "facing"
                )
                adPostVehiclesBean.noOfowners = "2"
            }

            R.id.tvthird -> {
                setTypeValueFive(false, false, true, false,false, "facing")
                setTypeBottomSelectionFive(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    "facing"
                )
                adPostVehiclesBean.noOfowners = "3"
            }

            R.id.tvfourth -> {
                setTypeValueFive(false, false, false, true,false, "facing")
                setTypeBottomSelectionFive(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    "facing"
                )
                adPostVehiclesBean.noOfowners = "4"
            }

            R.id.tvfourthp -> {
                setTypeValueFive(false, false, false, false,true, "facing")
                setTypeBottomSelectionFive(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.gray,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    "facing"
                )
                adPostVehiclesBean.noOfowners = "5"
            }
        }
    }

    private fun setType() {

        setTypeValue(false, false, false, "type")
        setTypeBottomSelection(
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.color.gray,
            R.color.gray,
            R.color.gray,
            "type"
        )
        adPostVehiclesBean.insuranceType = ""
    }

    private fun setFuel() {

        setTypeBottomSelectionFive(
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.color.gray,
            R.color.gray,
            R.color.gray,
            R.color.gray,
            R.color.gray,
            "fuel"
        )

        adPostVehiclesBean.fuel= ""
    }

    private fun setTypeBottomSelectionFive(
        t1: Int,
        t2: Int,
        t3: Int,
        t4: Int,
        t5: Int,
        t6: Int,
        t7: Int,
        t8: Int,
        t9: Int,
        t10: Int,
        type: String
    ) {
        if (type.equals("fuel", ignoreCase = true)) {
            tvcng.setBackgroundResource(t1)
            tvpetrol.setBackgroundResource(t2)
            tvdeisel.setBackgroundResource(t3)
            tvelectric.setBackgroundResource(t4)
            tvlpg.setBackgroundResource(t5)

            tvcng.setTextColor(resources.getColor(t6))
            tvpetrol.setTextColor(resources.getColor(t7))
            tvdeisel.setTextColor(resources.getColor(t8))
            tvelectric.setTextColor(resources.getColor(t9))
            tvlpg.setTextColor(resources.getColor(t10))
        }

        if (type.equals("facing", ignoreCase = true)) {
            tvfirst.setBackgroundResource(t1)
            tvsecond.setBackgroundResource(t2)
            tvthird.setBackgroundResource(t3)
            tvfourth.setBackgroundResource(t4)
            tvfourthp.setBackgroundResource(t5)

            tvfirst.setTextColor(resources.getColor(t6))
            tvsecond.setTextColor(resources.getColor(t7))
            tvthird.setTextColor(resources.getColor(t8))
            tvfourth.setTextColor(resources.getColor(t9))
            tvfourthp.setTextColor(resources.getColor(t10))
        }

    }

    private fun setTypeValueFive(
        b: Boolean,
        b1: Boolean,
        b2: Boolean,
        b3: Boolean,
        b4: Boolean,
        type: String
    ) {
        if (type.equals("fuel", ignoreCase = true)) {
            cng = b
            petrol = b1
            diesel = b2
            electric = b3
            lpg = b4
        }

        if (type.equals("facing", ignoreCase = true)) {
            first = b
            second = b1
            third = b2
            fourth = b3
            fourthp = b4
        }
    }

    private fun setValue(b1: Boolean, b2: Boolean, type: String) {

        if (type.equals("type", ignoreCase = true)) {
            automatic = b1
            manual = b2
        }
    }

    private fun setBottomSelection(b1: Int, b2: Int, b3: Int, b4: Int, type: String) {
        if (type.equals("type", ignoreCase = true)) {

            tvautomatic.setBackgroundResource(b1)
            tvmaual.setBackgroundResource(b2)

            tvautomatic.setTextColor(resources.getColor(b3))
            tvmaual.setTextColor(resources.getColor(b4))
        }
    }

    private fun setTypeValue(tb1: Boolean, tb2: Boolean, tb3: Boolean, type: String) {

        if (type.equals("fuel", ignoreCase = true)) {
            cng = tb1
            diesel = tb2
            electric = tb3
        }

        if (type.equals("type", ignoreCase = true)) {
            comp = tb1
            thirdparty = tb2
            zerodep = tb3
        }
    }

    private fun setTypeBottomSelection(
        t1: Int,
        t2: Int,
        t3: Int,
        t4: Int,
        t5: Int,
        t6: Int,
        type: String
    ) {

        if (type.equals("fuel", ignoreCase = true)) {
            tvcng.setBackgroundResource(t1)
            tvdeisel.setBackgroundResource(t2)
            tvelectric.setBackgroundResource(t3)

            tvcng.setTextColor(resources.getColor(t4))
            tvdeisel.setTextColor(resources.getColor(t5))
            tvelectric.setTextColor(resources.getColor(t6))
        }

        if (type.equals("type", ignoreCase = true)) {
            tvcomp.setBackgroundResource(t1)
            tvthirdparty.setBackgroundResource(t2)
            tvzerodep.setBackgroundResource(t3)

            tvcomp.setTextColor(resources.getColor(t4))
            tvthirdparty.setTextColor(resources.getColor(t5))
            tvzerodep.setTextColor(resources.getColor(t6))
        }

    }

    private fun setTypeFourValue(
        tb1: Boolean,
        tb2: Boolean,
        tb3: Boolean,
        tb4: Boolean,
        type: String
    ) {

        if (type.equals("facing", ignoreCase = true)) {
            first = tb1
            second = tb2
            third = tb3
            fourth = tb4
        }
    }

    private fun setTypeFourBottomSelection(
        t1: Int,
        t2: Int,
        t3: Int,
        t4: Int,
        t5: Int,
        t6: Int,
        t7: Int,
        t8: Int,
        type: String
    ) {

        if (type.equals("facing", ignoreCase = true)) {
            tvfirst.setBackgroundResource(t1)
            tvsecond.setBackgroundResource(t2)
            tvthird.setBackgroundResource(t3)
            tvfourth.setBackgroundResource(t4)

            tvfirst.setTextColor(resources.getColor(t5))
            tvsecond.setTextColor(resources.getColor(t6))
            tvthird.setTextColor(resources.getColor(t7))
            tvfourth.setTextColor(resources.getColor(t8))
        }

    }

    companion object {

        private val TAG = "CarSellDetailActivity"
    }

    fun isValidFormat(format: String, value: String): Boolean {
        var date: Date? = null
        try {
            val sdf = SimpleDateFormat(format)
            date = sdf.parse(value)
            if (value != sdf.format(date)) {
                date = null
            }
        } catch (ex: ParseException) {
            ex.printStackTrace()
        }

        return date != null
    }

    fun CheckDates(startDate: String, endDate: String): Int {

        val dfDate = SimpleDateFormat("dd.MM.yy")

        var b = 0

        try {
            if (dfDate.parse(startDate).before(dfDate.parse(endDate))) {
                b = 0  // If start date is before end date.
            } else if (dfDate.parse(startDate) == dfDate.parse(endDate)) {
                b = 1  // If two dates are equal.
            } else {
                b = 2 // If start date is after the end date.
            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return b
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {

        if (method.equals("GetBrands")) {

            brandlist.clear()
            typelist.clear()

            val dataModel = MobileBrandBeans()
            dataModel.MobileBrandBeans(da)

            brandlist = dataModel.mobileBrandList
            Utils.showLog(TAG,"==brand list size==" + brandlist.size)

            for(i in brandlist.indices)
            {
                val name = brandlist.get(i).brandName
                val id = brandlist.get(i).brandId
                typelist.add(name.toString())
                typeidlist.add(id.toString())
            }

            type_spinner_type = ArrayAdapter(
                applicationContext,
                android.R.layout.simple_spinner_dropdown_item,
                typelist
            )
            // Creating adapter for spinner
            val dataAdapter1 =
                ArrayAdapter(applicationContext, android.R.layout.simple_spinner_item, typelist)

            // Drop down layout style - list view with radio button
            dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

            // attaching data adapter to spinner
            sptype.adapter = dataAdapter1

        }
    }

    override fun onFail(msg: String, method: String) {
        AppConstants.printToast(applicationContext,msg)

    }

    private fun updateLabelsdate() {
        val myFormat = "dd.MM.yy" //In which you need put here
        val sdf = SimpleDateFormat(myFormat, Locale.US)

        etinsurance.setText(sdf.format(myCalendar.time))
    }

    private fun validations(){

        val issubmit = AdPostVehiclesValidation.checkForCarForSell(this,adPostVehiclesBean,etyear,etkm,etadtitle,etotherinfo)
        Utils.showLog(TAG,"status" +  issubmit)
        if(issubmit) {
            adPostVehiclesBean.category_id = storeUserData.getString(Constants.CATEGORY_ID)
            adPostVehiclesBean.sub_category_id = storeUserData.getString(Constants.SUBCATEGORYID)
            adPostVehiclesBean.year = etyear.text.toString()
            adPostVehiclesBean.kmDriven = etkm.text.toString()
            adPostVehiclesBean.insuranceValidity= etinsurance.text.toString()
            adPostVehiclesBean.title = etadtitle.text.toString()
            adPostVehiclesBean.other_information = etotherinfo.text.toString()

            val gson = Gson()
            val json = gson.toJson(adPostVehiclesBean)
            storeUserData.setString(Constants.ADDVEHICLES,json)

            Log.e("save", "adOfficeForSell save data: "+storeUserData.getString(Constants.ADDVEHICLES))

            //val intent = Intent(applicationContext, UploadPhotoActivity::class.java)
            //startActivity(intent)

            setNormalMultiButton()
        }
    }

    fun setNormalMultiButton() {
        val selectedUriList: List<Uri>? = null

        TedImagePicker.with(this)
                .max(12, "Maximum 12 photos allowed")
                .errorListener { message ->
                    Log.d("ted", "message: $message")
                }
                .selectedUri(selectedUriList)
                .startMultiImage { list: List<Uri> ->
                    Log.e("list===", "listsize: ${list.size}")
                    showMultiImage1(list)
                }

    }

    private fun showMultiImage1(uriList: List<Uri>) {

        Log.d("ted=", "uriList: $uriList")
        Log.e("uriList.get(0)====", "uriList.get(0): $uriList.get(0)")

        Thread(Runnable {
            AppConstants.imageList.clear()
            AppConstants.imageListuri.clear()
            uriList.forEach {
                AppConstants.imageListuri.add(it)
                val imagepath = getPathFromGooglePhotosUri(it)
                resizebitmapmain = decodeImageFromFiles(imagepath, 280, 280)
                Log.e("resizebitmapmain====", "01--: $resizebitmapmain")
                val ei = ExifInterface(imagepath.toString())
                val orientation: Int = ei.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED
                )
                var rotatedBitmap: Bitmap? = null

                when (orientation) {
                    ExifInterface.ORIENTATION_ROTATE_90 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 90F)
                    ExifInterface.ORIENTATION_ROTATE_180 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 180F)
                    ExifInterface.ORIENTATION_ROTATE_270 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 270F)
                    ExifInterface.ORIENTATION_NORMAL -> rotatedBitmap = resizebitmapmain
                    else -> rotatedBitmap = resizebitmapmain
                }

                AppConstants.imageList.add(rotatedBitmap!!)
            }

            Log.e("imageListsize====", "viewSize: ${AppConstants.imageList.size}")

            if(AppConstants.imageList!=null)
            {
                Log.e("AppConstantsimageList==", "viewSize: ${AppConstants.imageList.size}")
                checkAndSaveVehicles(this)
            }

        }).start()

        AdPostImages.changeScreentoNext(this)
    }

    fun rotateImage(source: Bitmap, angle: Float): Bitmap? {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(
                source, 0, 0, source.width, source.height,
                matrix, true
        )
    }

    fun decodeImageFromFiles(path: String?, width: Int, height: Int): Bitmap {
        val scaleOptions = BitmapFactory.Options()
        scaleOptions.inJustDecodeBounds = true
        BitmapFactory.decodeFile(path, scaleOptions)
        var scale = 1
        while (scaleOptions.outWidth / scale / 2 >= width
                && scaleOptions.outHeight / scale / 2 >= height
        ) {
            scale *= 2
        }
        // decode with the sample size
        val outOptions = BitmapFactory.Options()
        outOptions.inSampleSize = scale
        val bitmap: Bitmap = BitmapFactory.decodeFile(path, outOptions)
        Utils.showLog(
                UploadActivity1.TAG,
                "===BitmapFactory===width" + bitmap.width + "===height" + bitmap.height + "==="
        )

        return bitmap
    }

    fun getPathFromGooglePhotosUri(uriPhoto: Uri?): String? {
        if (uriPhoto == null) return null
        var input: FileInputStream? = null
        var output: FileOutputStream? = null
        try {
            val pfd: ParcelFileDescriptor? = contentResolver.openFileDescriptor(uriPhoto, "r")
            val fd: FileDescriptor = pfd!!.getFileDescriptor()
            input = FileInputStream(fd)
            val tempFilename: String = getTempFilename(this)
            output = FileOutputStream(tempFilename)
            var read: Int = 0
            val bytes = ByteArray(4096)
            while (input.read(bytes).also({ read = it }) != -1) {
                output.write(bytes, 0, read)
            }
            return tempFilename
        } catch (ignored: IOException) {
            // Nothing we can do
        } finally {
            closeSilently(input)
            closeSilently(output)
        }
        return null
    }

    @Throws(IOException::class)
    private fun getTempFilename(context: Context): String {
        val outputDir = context.cacheDir
        val outputFile = File.createTempFile("image", "tmp", outputDir)
        return outputFile.absolutePath
    }

    fun closeSilently(c: Closeable?) {
        if (c == null) return
        try {
            c.close()
        } catch (t: Throwable) {
            // Do nothing
        }
    }

    private fun checkAndSaveVehicles(context: Context) {
        val storeUserData = StoreUserData(context)
        val gson = Gson()

        if (AppConstants.imageList.size >= 1) {

            Log.e("test", "imagelist: " + AppConstants.imageList.size+"===")

            val json: String = storeUserData.getString(Constants.ADDVEHICLES)

            Log.e("String", "String data: " + json+"====")

            val adPostVehiclesBean = gson.fromJson(json, AdPostVehiclesBean::class.java)

            Log.e("test", "listsave data: " + adPostVehiclesBean.toString())
            adPostVehiclesBean.post_images?.clear()
            adPostVehiclesBean.post_images = AppConstants.imageList
            Log.e("test", "post_images: " +  adPostVehiclesBean.post_images!!.size+"===")

            Log.e("test", "post_images data: " + adPostVehiclesBean.post_images)
            val jsonset = gson.toJson(adPostVehiclesBean)

            storeUserData.setString(Constants.ADDVEHICLES, jsonset)
        }
        else{
            Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
        }
    }

}

