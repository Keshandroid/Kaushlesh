package com.kaushlesh.view.activity.AdPost.Vehicles

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.ParcelFileDescriptor
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.R
import com.kaushlesh.bean.AdPost.AdPostBikesBean
import com.kaushlesh.bean.AdPost.AdPostMobilesBean
import com.kaushlesh.bean.AdPost.AdPostVehiclesBean
import com.kaushlesh.bean.MobileBrandBeans
import com.kaushlesh.bean.VehicleCarDecorTypesBeans
import com.kaushlesh.bean.VehicleTypesBeans
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.constant.AppConstants.printLog
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.AdPostImages
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.utils.validations.AdPostVehiclesValidation
import com.kaushlesh.view.activity.AdPost.UploadActivity1
import com.kaushlesh.view.activity.AdPost.UploadPhotoActivity
import gun0912.tedimagepicker.builder.TedImagePicker
import kotlinx.android.synthetic.main.activity_add_brand_detail.*
import org.json.JSONObject
import java.io.*
import java.util.ArrayList

class AddBrandDetailActivity : AppCompatActivity(), View.OnClickListener , ParseControllerListener {
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal lateinit var rltype: RelativeLayout
    internal lateinit var sptype: Spinner
    internal lateinit var tvtype: TextView
    internal lateinit var ibtype: ImageButton
    internal lateinit var btnnext: Button
    internal lateinit var etadtitle: EditText
    internal lateinit var etotherinfo: EditText
    internal lateinit var tv1: TextView
    internal lateinit var tv2: TextView
    internal lateinit var type_spinner_type: ArrayAdapter<String>

    internal var typelist: MutableList<String> = ArrayList()
    internal var typeidlist: MutableList<String> = ArrayList()

    lateinit var storeUserData: StoreUserData
    lateinit var adPostVehiclesBean: AdPostVehiclesBean
    lateinit var getListingController: GetListingController
    //internal var brandlist: ArrayList<VehicleCarDecorTypesBeans.VehicleTypeBeansList> = ArrayList()
    internal var brandlist: ArrayList<MobileBrandBeans.MobileBrandBeansList> = ArrayList()
    lateinit var resizebitmapmain: Bitmap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_brand_detail)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.txt_include_details)

        storeUserData=StoreUserData(this)
        adPostVehiclesBean= AdPostVehiclesBean()
        getListingController = GetListingController(this, this)

        initBindViews()

        Utils.setontouch(et_other_info)
        Utils.setontouch1(et_ad_title)

        //getListingController.getVehicleCarDecorTypeList()
        getListingController.getBrandList()
    }

    private fun initBindViews() {


        rltype = findViewById(R.id.rl_brand_type)
        sptype = findViewById(R.id.spinner_brand_type)
        tvtype = findViewById(R.id.tv_brand_type)
        ibtype = findViewById(R.id.ib_brand_type)

        btnnext = findViewById(R.id.btn_next)
        etadtitle = findViewById(R.id.et_ad_title)
        etotherinfo = findViewById(R.id.et_other_info)
        tv1 = findViewById(R.id.tv_1)
        tv2 = findViewById(R.id.tv_2)

        btnback.setOnClickListener(this)
        btnnext.setOnClickListener(this)
        rltype.setOnClickListener(this)
        ibtype.setOnClickListener(this)


        sptype.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                printLog(TAG, "onItemSelected: ===" + adapterView.selectedItem)
                printLog(TAG, "onItemSelected: ===" + typeidlist.get(position))
                adPostVehiclesBean.vehicleType = typeidlist.get(position)
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        }

        etadtitle.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {


                tv1.text = s.length.toString() + "/77"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })

        etotherinfo.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {


                tv2.text = s.length.toString() + "/5000"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_back -> onBackPressed()

            R.id.btn_next -> {
                //val intent = Intent(applicationContext, UploadPhotoActivity::class.java)
                //startActivity(intent)

                validations()
            }

            R.id.rl_type -> {

                tvtype.visibility = View.GONE
                sptype.visibility = View.VISIBLE
                sptype.performClick()
            }

            R.id.ib_type -> {

                tvtype.visibility = View.GONE
                sptype.visibility = View.VISIBLE
                rltype.performClick()
            }
        }
    }

    companion object {

        private val TAG = "AddBrandDetailActivity"
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {

      /*  if (method.equals("GetVehicleCarType"))*/
        if (method.equals("GetBrands")) {

            brandlist.clear()
            typelist.clear()


            val dataModel = MobileBrandBeans()
            dataModel.MobileBrandBeans(da)

         /*   val dataModel = VehicleCarDecorTypesBeans()
            dataModel.VehicleCarDecorTypesBeans(da)

            brandlist = dataModel.vehicleTypeList
            Utils.showLog(TAG,"==vehicle type list size==" + brandlist.size)

            for(i in brandlist.indices)
            {
                val name = brandlist.get(i).typeName
                val id = brandlist.get(i).typeId
                typelist.add(name.toString())
                typeidlist.add(id.toString())
            }*/
            brandlist = dataModel.mobileBrandList
            Utils.showLog(TAG,"==brand list size==" + brandlist.size)

            for(i in brandlist.indices)
            {
                val name = brandlist.get(i).brandName
                val id = brandlist.get(i).brandId
                typelist.add(name.toString())
                typeidlist.add(id.toString())
            }

            type_spinner_type = ArrayAdapter(applicationContext, android.R.layout.simple_spinner_dropdown_item, typelist)
            // Creating adapter for spinner
            val dataAdapter1 = ArrayAdapter(applicationContext, android.R.layout.simple_spinner_item, typelist)

            // Drop down layout style - list view with radio button
            dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

            // attaching data adapter to spinner
            sptype.adapter = dataAdapter1

        }
    }

    override fun onFail(msg: String, method: String) {
        AppConstants.printToast(applicationContext,msg)

    }

    private fun validations(){

        val issubmit = AdPostVehiclesValidation.checkForCarDecorAutoParts(this,adPostVehiclesBean,etadtitle,etotherinfo)
        Utils.showLog(TAG,"status" +  issubmit)
        if(issubmit) {
            adPostVehiclesBean.category_id = storeUserData.getString(Constants.CATEGORY_ID)
            adPostVehiclesBean.sub_category_id = storeUserData.getString(Constants.SUBCATEGORYID)
            adPostVehiclesBean.title = etadtitle.text.toString()
            adPostVehiclesBean.other_information = etotherinfo.text.toString()

            val gson = Gson()
            val json = gson.toJson(adPostVehiclesBean)
            storeUserData.setString(Constants.ADDVEHICLES,json)

            Log.e("save", "adOfficeForSell save data: "+storeUserData.getString(Constants.ADDVEHICLES))

            //val intent = Intent(applicationContext, UploadPhotoActivity::class.java)
            //startActivity(intent)

            setNormalMultiButton()
        }
    }

    fun setNormalMultiButton() {
        val selectedUriList: List<Uri>? = null

        TedImagePicker.with(this)
                .max(12, "Maximum 12 photos allowed")
                .errorListener { message ->
                    Log.d("ted", "message: $message")
                }
                .selectedUri(selectedUriList)
                .startMultiImage { list: List<Uri> ->
                    Log.e("list===", "listsize: ${list.size}")
                    showMultiImage1(list)
                }

    }

    private fun showMultiImage1(uriList: List<Uri>) {

        Log.d("ted=", "uriList: $uriList")
        Log.e("uriList.get(0)====", "uriList.get(0): $uriList.get(0)")

        Thread(Runnable {
            AppConstants.imageList.clear()
            AppConstants.imageListuri.clear()
            uriList.forEach {
                AppConstants.imageListuri.add(it)
                val imagepath = getPathFromGooglePhotosUri(it)
                resizebitmapmain = decodeImageFromFiles(imagepath, 280, 280)
                Log.e("resizebitmapmain====", "01--: $resizebitmapmain")
                val ei = ExifInterface(imagepath.toString())
                val orientation: Int = ei.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED
                )
                var rotatedBitmap: Bitmap? = null

                when (orientation) {
                    ExifInterface.ORIENTATION_ROTATE_90 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 90F)
                    ExifInterface.ORIENTATION_ROTATE_180 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 180F)
                    ExifInterface.ORIENTATION_ROTATE_270 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 270F)
                    ExifInterface.ORIENTATION_NORMAL -> rotatedBitmap = resizebitmapmain
                    else -> rotatedBitmap = resizebitmapmain
                }

                AppConstants.imageList.add(rotatedBitmap!!)
            }

            Log.e("imageListsize====", "viewSize: ${AppConstants.imageList.size}")

            if(AppConstants.imageList!=null)
            {
                Log.e("AppConstantsimageList==", "viewSize: ${AppConstants.imageList.size}")
                checkAndSaveVehicles(this)
            }

        }).start()

        AdPostImages.changeScreentoNext(this)
    }

    private fun checkAndSaveVehicles(context: Context) {
        val storeUserData = StoreUserData(context)
        val gson = Gson()

        if (AppConstants.imageList.size >= 1) {

            Log.e("test", "imagelist: " + AppConstants.imageList.size+"===")

            val json: String = storeUserData.getString(Constants.ADDVEHICLES)

            Log.e("String", "String data: " + json+"====")

            val adPostVehiclesBean = gson.fromJson(json, AdPostVehiclesBean::class.java)

            Log.e("test", "listsave data: " + adPostVehiclesBean.toString())
            adPostVehiclesBean.post_images?.clear()
            adPostVehiclesBean.post_images = AppConstants.imageList
            Log.e("test", "post_images: " +  adPostVehiclesBean.post_images!!.size+"===")

            Log.e("test", "post_images data: " + adPostVehiclesBean.post_images)
            val jsonset = gson.toJson(adPostVehiclesBean)

            storeUserData.setString(Constants.ADDVEHICLES, jsonset)
        }
        else{
            Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
        }
    }

    fun rotateImage(source: Bitmap, angle: Float): Bitmap? {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(
                source, 0, 0, source.width, source.height,
                matrix, true
        )
    }

    fun decodeImageFromFiles(path: String?, width: Int, height: Int): Bitmap {
        val scaleOptions = BitmapFactory.Options()
        scaleOptions.inJustDecodeBounds = true
        BitmapFactory.decodeFile(path, scaleOptions)
        var scale = 1
        while (scaleOptions.outWidth / scale / 2 >= width
                && scaleOptions.outHeight / scale / 2 >= height
        ) {
            scale *= 2
        }
        // decode with the sample size
        val outOptions = BitmapFactory.Options()
        outOptions.inSampleSize = scale
        val bitmap: Bitmap = BitmapFactory.decodeFile(path, outOptions)
        Utils.showLog(
                UploadActivity1.TAG,
                "===BitmapFactory===width" + bitmap.width + "===height" + bitmap.height + "==="
        )

        return bitmap
    }

    fun getPathFromGooglePhotosUri(uriPhoto: Uri?): String? {
        if (uriPhoto == null) return null
        var input: FileInputStream? = null
        var output: FileOutputStream? = null
        try {
            val pfd: ParcelFileDescriptor? = contentResolver.openFileDescriptor(uriPhoto, "r")
            val fd: FileDescriptor = pfd!!.getFileDescriptor()
            input = FileInputStream(fd)
            val tempFilename: String = getTempFilename(this)
            output = FileOutputStream(tempFilename)
            var read: Int = 0
            val bytes = ByteArray(4096)
            while (input.read(bytes).also({ read = it }) != -1) {
                output.write(bytes, 0, read)
            }
            return tempFilename
        } catch (ignored: IOException) {
            // Nothing we can do
        } finally {
            closeSilently(input)
            closeSilently(output)
        }
        return null
    }

    @Throws(IOException::class)
    private fun getTempFilename(context: Context): String {
        val outputDir = context.cacheDir
        val outputFile = File.createTempFile("image", "tmp", outputDir)
        return outputFile.absolutePath
    }

    fun closeSilently(c: Closeable?) {
        if (c == null) return
        try {
            c.close()
        } catch (t: Throwable) {
            // Do nothing
        }
    }

}

