package com.kaushlesh.view.activity.AdPost.Property

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.ParcelFileDescriptor
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.kaushlesh.R
import com.kaushlesh.bean.AdPost.AdPostGeneralBean
import com.kaushlesh.bean.AdPost.ApVillsellandRentBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.constant.AppConstants.printLog
import com.kaushlesh.utils.AdPostImages
import com.kaushlesh.utils.validations.AdPostPropertyValidation
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.AdPost.UploadActivity1
import com.kaushlesh.view.activity.AdPost.UploadPhotoActivity
import gun0912.tedimagepicker.builder.TedImagePicker
import kotlinx.android.synthetic.main.activity_ap_villa_sell_and_rent.*
import kotlinx.android.synthetic.main.activity_edit_profile.*
import java.io.*
import java.util.ArrayList

class  ApVillaSellAndRentActivity : AppCompatActivity(), View.OnClickListener {
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal lateinit var tvaprtment: TextView
    internal lateinit var tvbuilderfloor: TextView
    internal lateinit var tvfarmhouse: TextView
    internal lateinit var tvhousevilla: TextView
    internal lateinit var tvbed1: TextView
    internal lateinit var tvbed2: TextView
    internal lateinit var tvbed3: TextView
    internal lateinit var tvbed4: TextView
    internal lateinit var tvbed5: TextView
    internal lateinit var tvbth1: TextView
    internal lateinit var tvbth2: TextView
    internal lateinit var tvbth3: TextView
    internal lateinit var tvbth4: TextView
    internal lateinit var tvbth5: TextView
    internal lateinit var tvfurnished: TextView
    internal lateinit var tvsemifurnished: TextView
    internal lateinit var tvunfurnished: TextView
    internal lateinit var tvnewlaunch: TextView
    internal lateinit var tvreadyshift: TextView
    internal lateinit var tvundercons: TextView
    internal lateinit var tvbuilder: TextView
    internal lateinit var tvdealer: TextView
    internal lateinit var tvowner: TextView
    internal lateinit var etsuperbuiltarea: EditText
    internal lateinit var etcarpetarea: EditText
    internal lateinit var etmaintenance: EditText
    internal lateinit var etfloors: EditText
    internal lateinit var etfloorno: EditText
    internal lateinit var etotherinfo: EditText
    internal lateinit var etadtitle: EditText
    internal lateinit var tvcp1: TextView
    internal lateinit var tvcp2: TextView
    internal lateinit var tvcp3: TextView
    internal lateinit var tvcp4: TextView
    internal lateinit var tvcp5: TextView
    internal lateinit var ivyselect: ImageView
    internal lateinit var ivyunselect: ImageView
    internal lateinit var ivnselect: ImageView
    internal lateinit var ivnunselect: ImageView

    internal lateinit var btnnext: Button
    internal lateinit var llbachlors: LinearLayout
    internal lateinit var tvnorth: TextView
    internal lateinit var tvsouth: TextView
    internal lateinit var tveast: TextView
    internal lateinit var tvwest: TextView
    internal var isfor: String? = null
    internal var appartment: Boolean? = false
    internal var builderfloor: Boolean? = false
    internal var farmhouse: Boolean? = false
    internal var housevilla: Boolean? = false
    internal var furnished: Boolean? = false
    internal var semifurnished: Boolean? = false
    internal var unfurnished: Boolean? = false
    internal var newlaunc: Boolean? = false
    internal var readyshift: Boolean? = false
    internal var undercons: Boolean? = false
    internal var builder: Boolean? = false
    internal var dealer: Boolean? = false
    internal var owner: Boolean? = false
    internal var bed1: Boolean? = false
    internal var bed2: Boolean? = false
    internal var bed3: Boolean? = false
    internal var bed4: Boolean? = false
    internal var bed5: Boolean? = false
    internal var bath1: Boolean? = false
    internal var bath2: Boolean? = false
    internal var bath3: Boolean? = false
    internal var bath4: Boolean? = false
    internal var bath5: Boolean? = false
    internal var cp1: Boolean? = false
    internal var cp2: Boolean? = false
    internal var cp3: Boolean? = false
    internal var cp4: Boolean? = false
    internal var cp5: Boolean? = false
    internal var north: Boolean? = false
    internal var south: Boolean? = false
    internal var east: Boolean? = false
    internal var west: Boolean? = false
    internal var bach_allowed_yes: Boolean? = true
    internal var bach_allowed_no: Boolean? = false

    lateinit var storeUserData: StoreUserData
    lateinit var adVillsellandRentBeanList: ApVillsellandRentBean
    lateinit var resizebitmapmain: Bitmap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ap_villa_sell_and_rent)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        storeUserData=StoreUserData(this)
        adVillsellandRentBeanList= ApVillsellandRentBean()

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.txt_include_details)

        if (intent != null) {
            isfor = intent.getStringExtra("for")
            AppConstants.printLog(TAG, "==isfor==" + isfor!!)
        }

        initBindViews()
    }

    private fun initBindViews() {

        btnnext = findViewById(R.id.btn_next)
        llbachlors = findViewById(R.id.ll_bachelors)
        tvaprtment = findViewById(R.id.tvappartment)
        tvbuilderfloor = findViewById(R.id.tvbuilderfloor)
        tvfarmhouse = findViewById(R.id.tvfarmhouse)
        tvhousevilla = findViewById(R.id.tvhousevilla)
        tvbed1 = findViewById(R.id.tvb1)
        tvbed2 = findViewById(R.id.tvb2)
        tvbed3 = findViewById(R.id.tvb3)
        tvbed4 = findViewById(R.id.tvb4)
        tvbed5 = findViewById(R.id.tvb5)
        tvbth1 = findViewById(R.id.tvbth1)
        tvbth2 = findViewById(R.id.tvbth2)
        tvbth3 = findViewById(R.id.tvbth3)
        tvbth4 = findViewById(R.id.tvbth4)
        tvbth5 = findViewById(R.id.tvbth5)
        tvfurnished = findViewById(R.id.tvfurnished)
        tvsemifurnished = findViewById(R.id.tvsemifurnished)
        tvunfurnished = findViewById(R.id.tvunfurnished)
        tvnewlaunch = findViewById(R.id.tvnewlaunch)
        tvreadyshift = findViewById(R.id.tvredyshift)
        tvundercons = findViewById(R.id.tvundercon)
        tvbuilder = findViewById(R.id.tvbuilder)
        tvdealer = findViewById(R.id.tvdealer)
        tvowner = findViewById(R.id.tvowner)
        etsuperbuiltarea = findViewById(R.id.et_builtup_area)
        etcarpetarea = findViewById(R.id.et_carpet_area)
        etmaintenance = findViewById(R.id.et_maintenance)
        etfloors = findViewById(R.id.et_floors)
        etfloorno = findViewById(R.id.et_floor_no)
        etadtitle = findViewById(R.id.et_ad_title)
        etotherinfo = findViewById(R.id.et_other_info)
        tvcp1 = findViewById(R.id.tvcar0)
        tvcp2 = findViewById(R.id.tvcar1)
        tvcp3 = findViewById(R.id.tvcar2)
        tvcp4 = findViewById(R.id.tvcar3)
        tvcp5 = findViewById(R.id.tvcar4)
        ivyselect = findViewById(R.id.iv_yes_select)
        ivyunselect = findViewById(R.id.iv_yes_unselect)
        ivnselect = findViewById(R.id.iv_no_select)
        ivnunselect = findViewById(R.id.iv_no_unselect)

        tvnorth = findViewById(R.id.tvnorth)
        tvsouth = findViewById(R.id.tvsouth)
        tveast = findViewById(R.id.tveast)
        tvwest = findViewById(R.id.tvwest)

        Utils.setontouch(et_other_info)
        Utils.setontouch1(et_ad_title)


        if (isfor!!.equals("rent", ignoreCase = true)) {
            llbachlors.visibility = View.VISIBLE
            storeUserData.setString(Constants.APVILLARENTORSELL,"rent")
            adVillsellandRentBeanList.bachelors_allowed= "1"

        } else {
            llbachlors.visibility = View.GONE
            storeUserData.setString(Constants.APVILLARENTORSELL,"sell")
            adVillsellandRentBeanList.bachelors_allowed= ""
        }

        //setbackgroundSelected(R.drawable.bg_edittext_black)

        btnback.setOnClickListener(this)
        btnnext.setOnClickListener(this)

        tvaprtment.setOnClickListener(this)
        tvbuilderfloor.setOnClickListener(this)
        tvfarmhouse.setOnClickListener(this)
        tvhousevilla.setOnClickListener(this)

        tvfurnished.setOnClickListener(this)
        tvsemifurnished.setOnClickListener(this)
        tvunfurnished.setOnClickListener(this)

        tvnewlaunch.setOnClickListener(this)
        tvreadyshift.setOnClickListener(this)
        tvundercons.setOnClickListener(this)

        tvbuilder.setOnClickListener(this)
        tvdealer.setOnClickListener(this)
        tvowner.setOnClickListener(this)

        tvbed1.setOnClickListener(this)
        tvbed2.setOnClickListener(this)
        tvbed3.setOnClickListener(this)
        tvbed4.setOnClickListener(this)
        tvbed5.setOnClickListener(this)

        tvbth1.setOnClickListener(this)
        tvbth2.setOnClickListener(this)
        tvbth3.setOnClickListener(this)
        tvbth4.setOnClickListener(this)
        tvbth5.setOnClickListener(this)

        tvcp1.setOnClickListener(this)
        tvcp2.setOnClickListener(this)
        tvcp3.setOnClickListener(this)
        tvcp4.setOnClickListener(this)
        tvcp5.setOnClickListener(this)

        ivyunselect.setOnClickListener(this)
        ivnunselect.setOnClickListener(this)

        tvnorth.setOnClickListener(this)
        tvsouth.setOnClickListener(this)
        tveast.setOnClickListener(this)
        tvwest.setOnClickListener(this)


        etadtitle.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {


                tv_1.text = s.length.toString() + "/77"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })

        etotherinfo.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {


                tv_otherinfo.text = s.length.toString() + "/5000"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })

        adVillsellandRentBeanList.property_type= ""
        adVillsellandRentBeanList.listed_by=""
        adVillsellandRentBeanList.bedrooms=""
        adVillsellandRentBeanList.bathrooms= ""
        adVillsellandRentBeanList.construction_status= ""
        adVillsellandRentBeanList.furnishing= ""
        adVillsellandRentBeanList.car_parking= "0"
        adVillsellandRentBeanList.facing= ""
        adVillsellandRentBeanList.bachelors_allowed ="1"
    }

    private fun setbackgroundSelected(b: Int) {
        tvaprtment.setBackgroundResource(b)
        tvbed1.setBackgroundResource(b)
        tvbth1.setBackgroundResource(b)
        tvfurnished.setBackgroundResource(b)
        tvnewlaunch.setBackgroundResource(b)
        tvowner.setBackgroundResource(b)
        tvcp1.setBackgroundResource(b)
    }

    override fun onClick(view: View) {
        when (view.id) {

            R.id.btn_back -> onBackPressed()

            R.id.btn_next -> {
                /*val intent = Intent(applicationContext, UploadPhotoActivity::class.java)
                startActivity(intent)*/

                validations()

            }

            R.id.tvappartment -> {
                setTypeFourValue(true, false, false, false, "type")
                setTypeFourBottomSelection(
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    R.color.gray,
                    "type"
                )

                //adVillsellandRentBeanList.property_type= tvappartment.text.toString()
                adVillsellandRentBeanList.property_type ="1"
            }

            R.id.tvbuilderfloor -> {
                setTypeFourValue(false, true, false, false, "type")
                setTypeFourBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    "type"
                )

                //adVillsellandRentBeanList.property_type= tvbuilderfloor.text.toString()
                adVillsellandRentBeanList.property_type ="2"
            }

            R.id.tvfarmhouse -> {
                setTypeFourValue(false, false, true, false, "type")
                setTypeFourBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    "type"
                )

               //adVillsellandRentBeanList.property_type= tvfarmhouse.text.toString()
                adVillsellandRentBeanList.property_type ="3"
            }

            R.id.tvhousevilla -> {
                setTypeFourValue(false, false, false, true, "type")
                setTypeFourBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    "type"
                )

                //adVillsellandRentBeanList.property_type= tvhousevilla.text.toString()
                adVillsellandRentBeanList.property_type ="4"
            }

            R.id.tvfurnished -> {
                setTypeValue(true, false, false, "furnish")
                setTypeBottomSelection(
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    "furnish"
                )

                //adVillsellandRentBeanList.furnishing= tvfurnished.text.toString()
                adVillsellandRentBeanList.furnishing= "1"
            }

            R.id.tvsemifurnished -> {
                setTypeValue(false, true, false, "furnish")
                setTypeBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    "furnish"
                )

                //adVillsellandRentBeanList.furnishing= tvsemifurnished.text.toString()
                adVillsellandRentBeanList.furnishing="2"

            }

            R.id.tvunfurnished -> {
                setTypeValue(false, false, true, "furnish")
                setTypeBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    "furnish"
                )

                //adVillsellandRentBeanList.furnishing= tvunfurnished.text.toString()
                adVillsellandRentBeanList.furnishing="3"
            }

            R.id.tvnewlaunch -> {
                setTypeValue(true, false, false, "construction")
                setTypeBottomSelection(
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    "construction"
                )
              //  adVillsellandRentBeanList.construction_status= tvnewlaunch.text.toString()
                adVillsellandRentBeanList.construction_status= "1"

            }

            R.id.tvredyshift -> {
                setTypeValue(false, true, false, "construction")
                setTypeBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    "construction"
                )

                //adVillsellandRentBeanList.construction_status= tvredyshift.text.toString()
                adVillsellandRentBeanList.construction_status= "2"

            }

            R.id.tvundercon -> {
                setTypeValue(false, false, true, "construction")
                setTypeBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    "construction"
                )

                //adVillsellandRentBeanList.construction_status= tvundercon.text.toString()
                adVillsellandRentBeanList.construction_status= "3"

            }

            R.id.tvowner -> {
                setTypeValue(true, false, false, "listedby")
                setTypeBottomSelection(
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    "listedby"
                )

                //adVillsellandRentBeanList.listed_by= tvowner.text.toString()
                adVillsellandRentBeanList.listed_by= "3"
            }

            R.id.tvdealer -> {
                setTypeValue(false, true, false, "listedby")
                setTypeBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    "listedby"
                )

                //adVillsellandRentBeanList.listed_by= tvdealer.text.toString()
                adVillsellandRentBeanList.listed_by="2"
            }

            R.id.tvbuilder -> {
                setTypeValue(false, false, true, "listedby")
                setTypeBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    "listedby"
                )

                //adVillsellandRentBeanList.listed_by= tvbuilder.text.toString()
                adVillsellandRentBeanList.listed_by="1"
            }

            R.id.tvb1 -> {
                setBedroomValue(true, false, false, false, false, "bed")
                setBedroomBottomSelection(
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    R.color.gray,
                    R.color.gray,
                    "bed"
                )
               // adVillsellandRentBeanList.bedrooms= tvb1.text.toString()
                adVillsellandRentBeanList.bedrooms= "1"
            }

            R.id.tvb2 -> {
                setBedroomValue(false, true, false, false, false, "bed")
                setBedroomBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    R.color.gray,
                    "bed"
                )
               // adVillsellandRentBeanList.bedrooms= tvb2.text.toString()
                adVillsellandRentBeanList.bedrooms= "2"
            }

            R.id.tvb3 -> {
                setBedroomValue(false, false, true, false, false, "bed")
                setBedroomBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    "bed"
                )

                //adVillsellandRentBeanList.bedrooms= tvb3.text.toString()
                adVillsellandRentBeanList.bedrooms= "3"
            }

            R.id.tvb4 -> {
                setBedroomValue(false, false, false, true, false, "bed")
                setBedroomBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    "bed"
                )
               // adVillsellandRentBeanList.bedrooms= tvb4.text.toString()
                adVillsellandRentBeanList.bedrooms= "4"
            }

            R.id.tvb5 -> {
                setBedroomValue(false, false, false, false, true, "bed")
                setBedroomBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.gray,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    "bed"
                )

                //adVillsellandRentBeanList.bedrooms= tvb5.text.toString()
                adVillsellandRentBeanList.bedrooms= "5"
            }

            R.id.tvbth1 -> {
                setBedroomValue(true, false, false, false, false, "bath")
                setBedroomBottomSelection(
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    R.color.gray,
                    R.color.gray,
                    "bath"
                )

                //adVillsellandRentBeanList.bathrooms= tvbth1.text.toString()
                adVillsellandRentBeanList.bathrooms= "1"

            }

            R.id.tvbth2 -> {
                setBedroomValue(false, true, false, false, false, "bath")
                setBedroomBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    R.color.gray,
                    "bath"
                )

                //adVillsellandRentBeanList.bathrooms= tvbth2.text.toString()
                adVillsellandRentBeanList.bathrooms= "2"
            }

            R.id.tvbth3 -> {
                setBedroomValue(false, false, true, false, false, "bath")
                setBedroomBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    "bath"
                )

                //adVillsellandRentBeanList.bathrooms= tvbth3.text.toString()
                adVillsellandRentBeanList.bathrooms= "3"
            }

            R.id.tvbth4 -> {
                setBedroomValue(false, false, false, true, false, "bath")
                setBedroomBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    "bath"
                )
               // adVillsellandRentBeanList.bathrooms= tvbth4.text.toString()
                adVillsellandRentBeanList.bathrooms= "4"
            }

            R.id.tvbth5 -> {
                setBedroomValue(false, false, false, false, true, "bath")
                setBedroomBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.gray,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    "bath"
                )
                //adVillsellandRentBeanList.bathrooms= tvbth5.text.toString()
                adVillsellandRentBeanList.bathrooms= "5"
            }

            R.id.tvcar0 -> {

                if(cp1!!)
                {
                    setBedroomValue(false, false, false, false, false, "car")
                    setcar()
                }
                else {
                    setBedroomValue(true, false, false, false, false, "car")
                    setBedroomBottomSelection(
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.color.black,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        "car"
                    )
                    // adVillsellandRentBeanList.car_parking= tvcar0.text.toString()

                    //adVillsellandRentBeanList.car_parking = "0"
                    adVillsellandRentBeanList.car_parking = "1"
                }
            }

            R.id.tvcar1 -> {

                if(cp2!!)
                {
                    setBedroomValue(false, false, false, false, false, "car")
                    setcar()
                }
                else{
                setBedroomValue(false, true, false, false, false, "car")
                setBedroomBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    R.color.gray,
                    "car"
                )
                //adVillsellandRentBeanList.car_parking= tvcar1.text.toString()

                //adVillsellandRentBeanList.car_parking= "1"
                    adVillsellandRentBeanList.car_parking= "2"
                }
            }

            R.id.tvcar2 -> {

                if(cp3!!)
                {
                    setBedroomValue(false, false, false, false, false, "car")
                    setcar()
                }

                else {
                    setBedroomValue(false, false, true, false, false, "car")
                    setBedroomBottomSelection(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.color.gray,
                        R.color.gray,
                        R.color.black,
                        R.color.gray,
                        R.color.gray,
                        "car"
                    )
                    // adVillsellandRentBeanList.car_parking= tvcar2.text.toString()

                    //adVillsellandRentBeanList.car_parking = "2"
                    adVillsellandRentBeanList.car_parking = "3"
                }
            }

            R.id.tvcar3 -> {
                if(cp4!!)
                {
                    setBedroomValue(false, false, false, false, false, "car")
                    setcar()
                }
                else {
                    setBedroomValue(false, false, false, true, false, "car")
                    setBedroomBottomSelection(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        R.color.black,
                        R.color.gray,
                        "car"
                    )
                    //adVillsellandRentBeanList.car_parking= tvcar3.text.toString()

                    //adVillsellandRentBeanList.car_parking = "3"
                    adVillsellandRentBeanList.car_parking = "4"
                }
            }

            R.id.tvcar4 -> {

                if(cp5!!)
                {
                    setBedroomValue(false, false, false, false, false, "car")
                    setcar()
                }
                else {
                    setBedroomValue(false, false, false, false, true, "car")
                    setBedroomBottomSelection(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        R.color.black,
                        "car"
                    )

                    //adVillsellandRentBeanList.car_parking= tvcar4.text.toString()

                    //adVillsellandRentBeanList.car_parking = "4"
                    adVillsellandRentBeanList.car_parking = "5"
                }
            }

            R.id.iv_yes_unselect -> {
                bach_allowed_yes = true
                bach_allowed_no = false
                ivyselect.visibility = View.VISIBLE
                ivnselect.visibility = View.GONE

                //adVillsellandRentBeanList.bachelors_allowed= "yes"
                adVillsellandRentBeanList.bachelors_allowed= "1"
            }

            R.id.iv_no_unselect -> {
                bach_allowed_yes = false
                bach_allowed_no = true
                ivyselect.visibility = View.GONE
                ivnselect.visibility = View.VISIBLE

               // adVillsellandRentBeanList.bachelors_allowed= "No"
                adVillsellandRentBeanList.bachelors_allowed= "2"
            }

            R.id.tvnorth -> {

                if(north!!)
                {
                    setTypeFourValue(false, false, false, false, "facing")
                    setfacing()
                }
                else {
                    setTypeFourValue(true, false, false, false, "facing")
                    setTypeFourBottomSelection(
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.color.black,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        "facing"
                    )
                    adVillsellandRentBeanList.facing= "1"
                }

                //adVillsellandRentBeanList.facing= tvnorth.text.toString()

            }

            R.id.tvsouth -> {

                if(south!!)
                {
                    setTypeFourValue(false, false, false, false, "facing")
                    setfacing()
                }
                else {
                    setTypeFourValue(false, true, false, false, "facing")
                    setTypeFourBottomSelection(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.color.gray,
                        R.color.black,
                        R.color.gray,
                        R.color.gray,
                        "facing"
                    )

                    //adVillsellandRentBeanList.facing= tvsouth.text.toString()
                    adVillsellandRentBeanList.facing = "2"
                }
            }

            R.id.tveast -> {

                if(east!!)
                {
                    setTypeFourValue(false, false, false, false, "facing")
                    setfacing()
                }
                else {
                    setTypeFourValue(false, false, true, false, "facing")
                    setTypeFourBottomSelection(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.color.gray,
                        R.color.gray,
                        R.color.black,
                        R.color.gray,
                        "facing"
                    )

                    //adVillsellandRentBeanList.facing= tveast.text.toString()
                    adVillsellandRentBeanList.facing = "3"
                }
            }

            R.id.tvwest -> {

                if(west!!)
                {
                    setTypeFourValue(false, false, false, false, "facing")
                    setfacing()
                }
                else {
                    setTypeFourValue(false, false, false, true, "facing")
                    setTypeFourBottomSelection(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        R.color.black,
                        "facing"
                    )

                    // adVillsellandRentBeanList.facing= tvwest.text.toString()
                    adVillsellandRentBeanList.facing = "4"
                }
            }
        }
    }

    private fun setcar() {

        setBedroomBottomSelection(
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.color.gray,
            R.color.gray,
            R.color.gray,
            R.color.gray,
            R.color.gray,
            "car"
        )
        // adVillsellandRentBeanList.car_parking= tvcar0.text.toString()
        adVillsellandRentBeanList.car_parking= "0"
    }

    private fun setfacing() {

        setTypeFourBottomSelection(
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.color.gray,
            R.color.gray,
            R.color.gray,
            R.color.gray,
            "facing"
        )
        adVillsellandRentBeanList.facing= ""
    }

    private fun validations(){

        val issubmit = AdPostPropertyValidation.checkForApVillaFarm(this,adVillsellandRentBeanList,etsuperbuiltarea,etcarpetarea,etadtitle,etotherinfo)
        Utils.showLog(TAG,"status" +  issubmit)
        if(issubmit)
        {
            adVillsellandRentBeanList.category_id = storeUserData.getString(Constants.CATEGORY_ID)
            adVillsellandRentBeanList.sub_category_id = storeUserData.getString(Constants.SUBCATEGORYID)
            adVillsellandRentBeanList.super_builtup_area = etsuperbuiltarea.text.toString()
            adVillsellandRentBeanList.carpet_area = etcarpetarea.text.toString()
            adVillsellandRentBeanList.maintenance = etmaintenance.text.toString()
            adVillsellandRentBeanList.total_floors = etfloors.text.toString()
            adVillsellandRentBeanList.floor_no = etfloorno.text.toString()
            adVillsellandRentBeanList.project_name = et_property_name.text.toString()
            adVillsellandRentBeanList.title = etadtitle.text.toString()
            adVillsellandRentBeanList.other_information = etotherinfo.text.toString()

            val gson = Gson()
            val json = gson.toJson(adVillsellandRentBeanList)
            storeUserData.setString(Constants.APVILLSELLONE, json)
            Log.e("save", "save data: " + storeUserData.getString(Constants.APVILLSELLONE))

            //val intent = Intent(applicationContext, UploadPhotoActivity::class.java)
            //startActivity(intent)

            setNormalMultiButton()
        }

    }

    fun setNormalMultiButton() {
        val selectedUriList: List<Uri>? = null

        TedImagePicker.with(this)
                .max(12, "Maximum 12 photos allowed")
                .errorListener { message ->
                    Log.d("ted", "message: $message")
                }
                .selectedUri(selectedUriList)
                .startMultiImage { list: List<Uri> ->
                    Log.e("list===", "listsize: ${list.size}")
                    showMultiImage1(list)
                }

    }

    private fun showMultiImage1(uriList: List<Uri>) {

        Log.d("ted=", "uriList: $uriList")
        Log.e("uriList.get(0)====", "uriList.get(0): $uriList.get(0)")

        Thread(Runnable {
            AppConstants.imageList.clear()
            AppConstants.imageListuri.clear()
            uriList.forEach {
                AppConstants.imageListuri.add(it)
                val imagepath = getPathFromGooglePhotosUri(it)
                resizebitmapmain = decodeImageFromFiles(imagepath, 280, 280)
                Log.e("resizebitmapmain====", "01--: $resizebitmapmain")
                val ei = ExifInterface(imagepath.toString())
                val orientation: Int = ei.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED
                )
                var rotatedBitmap: Bitmap? = null

                when (orientation) {
                    ExifInterface.ORIENTATION_ROTATE_90 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 90F)
                    ExifInterface.ORIENTATION_ROTATE_180 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 180F)
                    ExifInterface.ORIENTATION_ROTATE_270 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 270F)
                    ExifInterface.ORIENTATION_NORMAL -> rotatedBitmap = resizebitmapmain
                    else -> rotatedBitmap = resizebitmapmain
                }

                AppConstants.imageList.add(rotatedBitmap!!)
            }

            Log.e("imageListsize====", "viewSize: ${AppConstants.imageList.size}")

            if(AppConstants.imageList!=null)
            {
                Log.e("AppConstantsimageList==", "viewSize: ${AppConstants.imageList.size}")
                checkAndSaveAppVilla(this)
            }

        }).start()


        AdPostImages.changeScreentoNext(this)

    }

    private fun checkAndSaveAppVilla(context: Context) {
        ///  bimglist.clear()
        val storeUserData = StoreUserData(context)
        val gson = Gson()

        if (AppConstants.imageList.size >= 1) {

            Log.e("test", "imagelist: " + AppConstants.imageList.size+"===")

            val json: String = storeUserData.getString(Constants.APVILLSELLONE)

            Log.e("String", "String data: " + json+"====")

            val adVillsellandRentBeanList = gson.fromJson(json, ApVillsellandRentBean::class.java)

            Log.e("test", "listsave data: " + adVillsellandRentBeanList.toString()+"====")
            adVillsellandRentBeanList.post_images?.clear()
            adVillsellandRentBeanList.post_images = AppConstants.imageList
            Log.e("test", "post_images: " +  adVillsellandRentBeanList.post_images!!.size+"===")


            Log.e("test", "post_images data: " + adVillsellandRentBeanList.post_images)

            val jsonset = gson.toJson(adVillsellandRentBeanList)

            storeUserData.setString(Constants.APVILLSELLONE, jsonset)

            ////   AdPostImages.changeScreentoLocation(context)

        } else {
            Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
        }
    }

    fun rotateImage(source: Bitmap, angle: Float): Bitmap? {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(
                source, 0, 0, source.width, source.height,
                matrix, true
        )
    }

    fun decodeImageFromFiles(path: String?, width: Int, height: Int): Bitmap {
        val scaleOptions = BitmapFactory.Options()
        scaleOptions.inJustDecodeBounds = true
        BitmapFactory.decodeFile(path, scaleOptions)
        var scale = 1
        while (scaleOptions.outWidth / scale / 2 >= width
                && scaleOptions.outHeight / scale / 2 >= height
        ) {
            scale *= 2
        }
        // decode with the sample size
        val outOptions = BitmapFactory.Options()
        outOptions.inSampleSize = scale
        val bitmap: Bitmap = BitmapFactory.decodeFile(path, outOptions)
        Utils.showLog(
                UploadActivity1.TAG,
                "===BitmapFactory===width" + bitmap.width + "===height" + bitmap.height + "==="
        )

        return bitmap
    }

    fun getPathFromGooglePhotosUri(uriPhoto: Uri?): String? {
        if (uriPhoto == null) return null
        var input: FileInputStream? = null
        var output: FileOutputStream? = null
        try {
            val pfd: ParcelFileDescriptor? = contentResolver.openFileDescriptor(uriPhoto, "r")
            val fd: FileDescriptor = pfd!!.getFileDescriptor()
            input = FileInputStream(fd)
            val tempFilename: String = getTempFilename(this)
            output = FileOutputStream(tempFilename)
            var read: Int = 0
            val bytes = ByteArray(4096)
            while (input.read(bytes).also({ read = it }) != -1) {
                output.write(bytes, 0, read)
            }
            return tempFilename
        } catch (ignored: IOException) {
            // Nothing we can do
        } finally {
            closeSilently(input)
            closeSilently(output)
        }
        return null
    }

    @Throws(IOException::class)
    private fun getTempFilename(context: Context): String {
        val outputDir = context.cacheDir
        val outputFile = File.createTempFile("image", "tmp", outputDir)
        return outputFile.absolutePath
    }

    fun closeSilently(c: Closeable?) {
        if (c == null) return
        try {
            c.close()
        } catch (t: Throwable) {
            // Do nothing
        }
    }

    private fun setError(editText: EditText) {
        editText.setError("Required Field")
    }

    private fun setBedroomBottomSelection(
        bd1: Int,
        bd2: Int,
        bd3: Int,
        bd4: Int,
        bd5: Int,
        bd6: Int,
        bd7: Int,
        bd8: Int,
        bd9: Int,
        bd10: Int,
        type: String
    ) {
        if (type.equals("bed", ignoreCase = true)) {

            tvbed1.setBackgroundResource(bd1)
            tvbed2.setBackgroundResource(bd2)
            tvbed3.setBackgroundResource(bd3)
            tvbed4.setBackgroundResource(bd4)
            tvbed5.setBackgroundResource(bd5)

            tvbed1.setTextColor(resources.getColor(bd6))
            tvbed2.setTextColor(resources.getColor(bd7))
            tvbed3.setTextColor(resources.getColor(bd8))
            tvbed4.setTextColor(resources.getColor(bd9))
            tvbed5.setTextColor(resources.getColor(bd10))
        }

        if (type.equals("bath", ignoreCase = true)) {
            tvbth1.setBackgroundResource(bd1)
            tvbth2.setBackgroundResource(bd2)
            tvbth3.setBackgroundResource(bd3)
            tvbth4.setBackgroundResource(bd4)
            tvbth5.setBackgroundResource(bd5)

            tvbth1.setTextColor(resources.getColor(bd6))
            tvbth2.setTextColor(resources.getColor(bd7))
            tvbth3.setTextColor(resources.getColor(bd8))
            tvbth4.setTextColor(resources.getColor(bd9))
            tvbth5.setTextColor(resources.getColor(bd10))
        }

        if (type.equals("car", ignoreCase = true)) {
            tvcp1.setBackgroundResource(bd1)
            tvcp2.setBackgroundResource(bd2)
            tvcp3.setBackgroundResource(bd3)
            tvcp4.setBackgroundResource(bd4)
            tvcp5.setBackgroundResource(bd5)

            tvcp1.setTextColor(resources.getColor(bd6))
            tvcp2.setTextColor(resources.getColor(bd7))
            tvcp3.setTextColor(resources.getColor(bd8))
            tvcp4.setTextColor(resources.getColor(bd9))
            tvcp5.setTextColor(resources.getColor(bd10))
        }
    }

    private fun setBedroomValue(
        b1: Boolean,
        b2: Boolean,
        b3: Boolean,
        b4: Boolean,
        b5: Boolean,
        type: String
    ) {

        if (type.equals("bed", ignoreCase = true)) {
            bed1 = b1
            bed2 = b2
            bed3 = b3
            bed4 = b4
            bed5 = b5
        }
        if (type.equals("bath", ignoreCase = true)) {
            bath1 = b1
            bath2 = b2
            bath3 = b3
            bath4 = b4
            bath5 = b5
        }
        if (type.equals("car", ignoreCase = true)) {
            cp1 = b1
            cp2 = b2
            cp3 = b3
            cp4 = b4
            cp5 = b5
        }
    }

    private fun setTypeValue(tb1: Boolean, tb2: Boolean, tb3: Boolean, type: String) {

        if (type.equals("type", ignoreCase = true)) {
            appartment = tb1
            builderfloor = tb2
            farmhouse = tb3
        }

        if (type.equals("furnish", ignoreCase = true)) {
            furnished = tb1
            semifurnished = tb2
            unfurnished = tb3
        }

        if (type.equals("construction", ignoreCase = true)) {
            newlaunc = tb1
            readyshift = tb2
            undercons = tb3
        }

        if (type.equals("listedby", ignoreCase = true)) {
            owner = tb1
            dealer = tb2
            builder = tb3
        }
    }

    private fun setTypeBottomSelection(
        t1: Int,
        t2: Int,
        t3: Int,
        t4: Int,
        t5: Int,
        t6: Int,
        type: String
    ) {

        if (type.equals("type", ignoreCase = true)) {
            tvaprtment.setBackgroundResource(t1)
            tvbuilderfloor.setBackgroundResource(t2)
            tvfarmhouse.setBackgroundResource(t3)

            tvaprtment.setTextColor(resources.getColor(t4))
            tvbuilderfloor.setTextColor(resources.getColor(t5))
            tvfarmhouse.setTextColor(resources.getColor(t6))
        }

        if (type.equals("furnish", ignoreCase = true)) {
            tvfurnished.setBackgroundResource(t1)
            tvsemifurnished.setBackgroundResource(t2)
            tvunfurnished.setBackgroundResource(t3)

            tvfurnished.setTextColor(resources.getColor(t4))
            tvsemifurnished.setTextColor(resources.getColor(t5))
            tvunfurnished.setTextColor(resources.getColor(t6))
        }

        if (type.equals("construction", ignoreCase = true)) {
            tvnewlaunch.setBackgroundResource(t1)
            tvreadyshift.setBackgroundResource(t2)
            tvundercons.setBackgroundResource(t3)

            tvnewlaunch.setTextColor(resources.getColor(t4))
            tvreadyshift.setTextColor(resources.getColor(t5))
            tvundercons.setTextColor(resources.getColor(t6))
        }

        if (type.equals("listedby", ignoreCase = true)) {
            tvowner.setBackgroundResource(t1)
            tvdealer.setBackgroundResource(t2)
            tvbuilder.setBackgroundResource(t3)

            tvowner.setTextColor(resources.getColor(t4))
            tvdealer.setTextColor(resources.getColor(t5))
            tvbuilder.setTextColor(resources.getColor(t6))
        }
    }

    private fun setTypeFourValue(
        tb1: Boolean,
        tb2: Boolean,
        tb3: Boolean,
        tb4: Boolean,
        type: String
    ) {

        if (type.equals("facing", ignoreCase = true)) {
            north = tb1
            south = tb2
            east = tb3
            west = tb4
        }

        if (type.equals("type", ignoreCase = true)) {
            appartment = tb1
            builderfloor = tb2
            farmhouse = tb3
            housevilla = tb4
        }
    }

    private fun setTypeFourBottomSelection(
        t1: Int,
        t2: Int,
        t3: Int,
        t4: Int,
        t5: Int,
        t6: Int,
        t7: Int,
        t8: Int,
        type: String
    ) {

        if (type.equals("facing", ignoreCase = true)) {
            tvnorth.setBackgroundResource(t1)
            tvsouth.setBackgroundResource(t2)
            tveast.setBackgroundResource(t3)
            tvwest.setBackgroundResource(t4)

            tvnorth.setTextColor(resources.getColor(t5))
            tvsouth.setTextColor(resources.getColor(t6))
            tveast.setTextColor(resources.getColor(t7))
            tvwest.setTextColor(resources.getColor(t8))
        }

        if (type.equals("type", ignoreCase = true)) {
            tvaprtment.setBackgroundResource(t1)
            tvbuilderfloor.setBackgroundResource(t2)
            tvfarmhouse.setBackgroundResource(t3)
            tvhousevilla.setBackgroundResource(t4)

            tvaprtment.setTextColor(resources.getColor(t5))
            tvbuilderfloor.setTextColor(resources.getColor(t6))
            tvfarmhouse.setTextColor(resources.getColor(t7))
            tvhousevilla.setTextColor(resources.getColor(t8))
        }

    }
    companion object {
        private val TAG = "ApVillaSellAndRentActivity"
    }

}

