package com.kaushlesh.view.activity

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.kaushlesh.R

class MaintananceActivity : AppCompatActivity() {

    lateinit var txtMaintenanceMessage: TextView
    lateinit var btnExit: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maintanance)

        txtMaintenanceMessage = findViewById(R.id.txtMaintenanceMessage)
        btnExit = findViewById(R.id.btnExit)

        if(intent.extras != null) {
            if(intent.getStringExtra("maintananceModeMessage") != null){
                val data = intent.getStringExtra("maintananceModeMessage").toString()
                txtMaintenanceMessage.setText(data)
            }
        }

        btnExit.setOnClickListener(object : View.OnClickListener{
            override fun onClick(p0: View?) {
                finishAffinity()
            }
        })

    }

}