package com.kaushlesh.view.activity.Login

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.kaushlesh.Controller.EditPhoneOtp
import com.kaushlesh.Controller.PhoneOtpVerificationController
import com.kaushlesh.Controller.ResendOtpController
import com.kaushlesh.R
import com.kaushlesh.bean.RegisterPhoneBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.reusable.GenericTextWatcher
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.MainActivity
import com.kaushlesh.view.fragment.MyAccount.EditProfileActivity
import kotlinx.android.synthetic.main.activity_otpmobile.*
import org.json.JSONException
import org.json.JSONObject


class OTPMobileActivity : AppCompatActivity() ,ParseControllerListener{

    val TAG = "OTPMobileActivity"
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal lateinit var activity: Activity
    var userId:String?=null;
    var userToken:String?=null;
    var phoneNo:String?=null;
    var Otp:String?=null;
    var isEditNumber:Boolean?=false
    var model: RegisterPhoneBean = RegisterPhoneBean();
    var dataModel = RegisterPhoneBean()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otpmobile)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }


        activity=this

        val extras = intent.extras
        if (extras != null) {
            dataModel = (intent.getSerializableExtra("value") as RegisterPhoneBean) //Obtaining data
            // dataModel=(intent.getSerializableExtra("value") as RegisterPhoneBean)
            //Obtaining data

            phoneNo = intent.getStringExtra("phone")

            if(phoneNo != null && phoneNo.toString().length > 0)
            {
                tv2.text = " +91 " + phoneNo
                isEditNumber =  intent.getBooleanExtra("isedit", true)
            }
            else{
                phoneNo = dataModel.ConatctNo
                tv2.text = " +91 " + phoneNo
                isEditNumber = dataModel.isEditNumber
            }

            userId = dataModel.userid
            userToken = dataModel.userToken
            Otp = dataModel.OTP.toString()


            tv_code.text = Otp
        }


        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.txt_login)

        tv_resend_code.setOnClickListener {
            ResendOtp()
        }
        iv_edit.setOnClickListener {
            editNumber()
        }

        btnback.setOnClickListener {
            onBackPressed()
        }

        tv_code.setOnClickListener {

            val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)

            et_one.setBackgroundResource(R.drawable.bg_edit_text_focused)
            et_one.setText(Otp!!.get(0).toString())
            et_two.setText(Otp!!.get(1).toString())
            et_three.setText(Otp!!.get(2).toString())
            et_four.setText(Otp!!.get(3).toString())
            et_five.setText(Otp!!.get(4).toString())
            et_six.setText(Otp!!.get(5).toString())



        }

        setOtpEditTextHandler();

       /* val edit = arrayOf<EditText>(et_one, et_two, et_three, et_four, et_five, et_six)
        et_one.addTextChangedListener(GenericTextWatcher(et_one, edit))
        et_two.addTextChangedListener(GenericTextWatcher(et_two, edit))
        et_three.addTextChangedListener(GenericTextWatcher(et_three, edit))
        et_four.addTextChangedListener(GenericTextWatcher(et_four, edit))
        et_five.addTextChangedListener(GenericTextWatcher(et_five, edit))
        et_six.addTextChangedListener(GenericTextWatcher(et_six, edit))*/

     /*   et_one.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                if (!et_one.getText().toString().equals("")) {
                    //et_two.requestFocus()
                }
            }
        })
        et_two.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (et_two.getText().toString().length == 0) {
                    //et_one.requestFocus()
                }
            }

            override fun afterTextChanged(s: Editable) {
                if (!et_two.getText().toString().equals("")) {
                    // et_three.requestFocus()
                }
            }
        })
        et_three.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (et_three.getText().toString().length == 0) {
                   // et_two.requestFocus()
                }
            }

            override fun afterTextChanged(s: Editable) {
                if (!et_three.getText().toString().equals("")) {
                    //  et_four.requestFocus()
                }
            }
        })
        et_four.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (et_four.getText().toString().length == 0) {
                   // et_three.requestFocus()
                }
            }

            override fun afterTextChanged(s: Editable) {
                if (!et_four.getText().toString().equals("")) {
                    // et_five.requestFocus()
                }
            }
        })
        et_five.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (et_five.getText().toString().length == 0) {
                   // et_four.requestFocus()
                }
            }

            override fun afterTextChanged(s: Editable) {
                if (!et_five.getText().toString().equals("")) {
                    //et_six.requestFocus()
                }
            }
        })
        et_six.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (et_six.getText().toString().length == 0) {
                   // et_five.requestFocus()
                }
            }

            override fun afterTextChanged(s: Editable) {

                *//* val intent = Intent(applicationContext, MainActivity::class.java)
                startActivity(intent)
                finishAffinity()*//*

                val finalotp =
                        et_one.getText().toString() + et_two.getText().toString() + et_three.getText()
                                .toString() + et_four.getText().toString() + et_five.getText()
                                .toString() + et_six.getText().toString()

                if (Otp == finalotp) {
                    if (isEditNumber == true) {
                        callForEditNumberOtp(finalotp)
                    } else {
                        callForRegisterPhone(finalotp)
                    }
                } else {
                    callForRegisterPhone(finalotp)
                }
            }
        })*/


    }

    private fun setOtpEditTextHandler() { //This is the function to be called

        val otpEt: Array<EditText?> = arrayOfNulls<EditText>(6)
        otpEt[0] = findViewById<View>(R.id.et_one) as EditText
        otpEt[1] = findViewById<View>(R.id.et_two) as EditText
        otpEt[2] = findViewById<View>(R.id.et_three) as EditText
        otpEt[3] = findViewById<View>(R.id.et_four) as EditText
        otpEt[4] = findViewById<View>(R.id.et_five) as EditText
        otpEt[5] = findViewById<View>(R.id.et_six) as EditText


        for (i in 0..5) { //Its designed for 6 digit OTP
            otpEt.get(i)!!.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
                override fun afterTextChanged(s: Editable) {
                    if (i == 5 && !otpEt.get(i)!!.getText().toString().isEmpty()) {
                        otpEt.get(i)!!.clearFocus() //Clears focus when you have entered the last digit of the OTP.

                        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
                        val finalotp = et_one.getText().toString() + et_two.getText().toString() + et_three.getText()
                                        .toString() + et_four.getText().toString() + et_five.getText()
                                        .toString() + et_six.getText().toString()

                        if (Otp == finalotp) {
                            if (isEditNumber == true) {
                                callForEditNumberOtp(finalotp)
                            } else {
                                callForRegisterPhone(finalotp)
                            }
                        } else {
                            callForRegisterPhone(finalotp)
                        }

                    } else if (!otpEt.get(i)!!.getText().toString().isEmpty()) {
                        otpEt.get(i + 1)!!.requestFocus() //focuses on the next edittext after a digit is entered.
                        //showKeyboard(otpEt.get(i + 1)!!)
                    }
                }
            })
            otpEt.get(i)!!.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
                if (event.action !== KeyEvent.ACTION_DOWN) {
                    return@OnKeyListener false //Dont get confused by this, it is because onKeyListener is called twice and this condition is to avoid it.
                }
                if (keyCode == KeyEvent.KEYCODE_DEL &&
                        otpEt.get(i)!!.getText().toString().isEmpty() && i != 0) {
//this condition is to handel the delete input by users.
                    otpEt.get(i - 1)!!.setText("") //Deletes the digit of OTP
                    otpEt.get(i - 1)!!.requestFocus() //and sets the focus on previous digit
                }
                false
            })
        }
    }

    private fun showKeyboard(get: EditText) {

        val imm: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(get, InputMethodManager.SHOW_IMPLICIT)

    }

    private fun editNumber() {
        val intent = Intent(applicationContext, AddPhoneNumberActivity::class.java)
        val bundle = Bundle()
        bundle.putSerializable("editnumber", dataModel)
        intent.putExtras(bundle)
        startActivity(intent)
        finish()
    }

    private fun ResendOtp() {
        callForResendOtp()
    }

    private fun callForResendOtp() {
        val c = ResendOtpController(this, activity, phoneNo.toString().trim({ it <= ' ' }))
        c.onClick(tv_resend_code)
    }


    private fun callForRegisterPhone(finalotp: String) {

        val c = PhoneOtpVerificationController(
                this,
                activity,
                userId.toString().trim { it <= ' ' },
                finalotp.toString().trim { it <= ' ' },
                userToken.toString().trim { it <= ' ' })
        c.addTextChangedListener(et_six)

    }

    private fun callForEditNumberOtp(finalotp: String) {
        val c = EditPhoneOtp(
                this,
                activity,
                userId.toString().trim { it <= ' ' },
                finalotp.toString().trim { it <= ' ' },
                userToken.toString().trim { it <= ' ' })
        c.addTextChangedListener(et_six)
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {

        AppConstants.printLog("TAG", "==method==" + method)
        // progress_spinner?.dismiss()
        Utils.showToast(this, message)

        if (method.equals("otp")) {

            dataModel.RegisterPhoneBean(da)
            Log.e("get data", "name" + dataModel.name.toString() + "email==" + dataModel.email.toString())

            if(dataModel.isRegister!!.toInt() == 0) {
                if (dataModel.name.toString().length > 0 && dataModel.email.toString().length > 0) {
                    val storeusedata = StoreUserData(activity)
                    storeusedata.setString(Constants.IS_LOGGED_IN, "true")
                    val intent = Intent(applicationContext, MainActivity::class.java)
                    val bundle = Bundle()
                    bundle.putSerializable("verifydata", dataModel)
                    intent.putExtras(bundle)
                    startActivity(intent)
                    finishAffinity()
                } else {
                    val intent1 = Intent(applicationContext, EditProfileActivity::class.java)
                    val bundle = Bundle()
                    bundle.putSerializable("verifydata", dataModel)
                    intent1.putExtras(bundle)
                    startActivity(intent1)
                    finishAffinity()
                }
            }
            else{
                if (dataModel.ConatctNo!!.toString().length == 0 || dataModel.locationId!!.toInt() == 0) {

                    val intent1 = Intent(applicationContext, EditProfileActivity::class.java)
                    val bundle = Bundle()
                    bundle.putSerializable("verifydata", dataModel)
                    intent1.putExtras(bundle)
                    startActivity(intent1)
                    finishAffinity()
                } else {
                    val storeusedata = StoreUserData(activity)
                    storeusedata.setString(Constants.IS_LOGGED_IN, "true")
                    val intent = Intent(applicationContext, MainActivity::class.java)
                    val bundle = Bundle()
                    bundle.putSerializable("verifydata", dataModel)
                    intent.putExtras(bundle)
                    startActivity(intent)
                    finishAffinity()
                }
            }

           /* if (dataModel.isActive == "1") {
                val intent = Intent(applicationContext, MainActivity::class.java)
                val bundle = Bundle()
                bundle.putSerializable("verifydata", dataModel)
                intent.putExtras(bundle)
                startActivity(intent)
                finish()

            } else {

            }*/

        }
        if(method.equals("otpResend")) {
            if (da.length() > 0) {
                Log.e("get Resend Otp", "$da==")
                try {
                    Otp=da.getString("otp")

                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            } else {
                AppConstants.printToast(activity, message)
            }

        }

        if(method.equals("editotp")) {
            if (da.length() > 0) {
                Log.e("Edit Otp-", "$da==")
                try {
                    //if (da.has("result")) {
                    // var data = da.getJSONObject("result")
                    /*        var faa: PhoneOtpVerificationBean = PhoneOtpVerificationBean();

                            faa.userToken = da.getString("userToken")
                            faa.userid = da.getString("userid")
                            faa.ConatctNo = da.getString("ConatctNo")
                            faa.isOTPVerified = da.getInt("isOTPVerified")
                            faa.isActive = da.getString("isActive")*/

                    val data = RegisterPhoneBean()
                    data.RegisterPhoneBean(da)
                    dataModel.RegisterPhoneBean(da)

                    if (dataModel.isActive=="1"){

                        val intent = Intent(applicationContext, MainActivity::class.java)
                        val bundle = Bundle()
                        bundle.putSerializable("verifydata", dataModel)
                        intent.putExtras(bundle)
                        startActivity(intent)
                        finish()

                    }else
                    {
                        val intent1 = Intent(applicationContext, EditProfileActivity::class.java)
                        val bundle = Bundle()
                        bundle.putSerializable("verifydata", dataModel)
                        intent1.putExtras(bundle)
                        startActivity(intent1)
                        finish()
                    }

                    // }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            } else {
                AppConstants.printToast(activity, message)
            }
        }
    }

    override fun onFail(msg: String, method: String) {
        AppConstants.printToast(this, msg)
        Log.e("errror", "onFail: " + msg)
    }

}
