package com.kaushlesh.view.activity.AdShowcase.Commercial

import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.kaushlesh.R
import kotlinx.android.synthetic.main.activity_comercial_fill_individual_category.*
import kotlinx.android.synthetic.main.item_jewellery_individual_category.view.*


class ComercialFillIndividualCategory : AppCompatActivity() {

    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comercial_fill_individual_category)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getText(R.string.txt_include_individual_details)

        btnback.setOnClickListener {
            onBackPressed()
        }
        add.setOnClickListener {
            onAddField()
        }
        ll_mainlayout!!.ll_delete.setOnClickListener {
            onDelete(ll_mainlayout)
        }
    }

    fun onDelete(view: View) {
        ll_mainlayout!!.removeView(view.parent as View)
    }


    fun onAddField() {
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val rowView: View = inflater.inflate(R.layout.item_jewellery_individual_category, null)
        ll_mainlayout!!.addView(rowView, ll_mainlayout!!.childCount - 1)

        var b = ll_mainlayout!!.childCount - 1

        for (a in 0..b) {

            if (a == b) {
                /* rowView.ll_fist.isEnabled = true
                 rowView.ll_fist.isClickable = true
                 rowView.et_super_builtup_area.isEnabled = true
                 rowView.tv_brand_type.isEnabled = true
                 rowView.spinner_about_project.isEnabled = true
                 rowView.tv_upload.isEnabled = true
                 rowView.et_carpet_area.isEnabled = true
                 rowView.et_washroom.isEnabled = true
                 rowView.et_price.isEnabled = true*/
            } else {
                rowView.ll_fist.isEnabled = false
                rowView.ll_fist.isClickable = false
                rowView.et_super_builtup_area.isEnabled = false
                rowView.tv_brand_type.isEnabled = false
                rowView.spinner_about_project.isEnabled = false
                rowView.tv_upload.isEnabled = false
                rowView.et_carpet_area.isEnabled = false
                rowView.et_washroom.isEnabled = false
                rowView.et_price.isEnabled = false
            }
        }
    }

}