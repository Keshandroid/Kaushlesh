package com.kaushlesh.view.activity

import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.location.LocationManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.util.Log
import android.view.MotionEvent
import android.view.Window
import android.view.WindowManager
import android.widget.TextView
import android.widget.VideoView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.location.*
import com.google.gson.GsonBuilder
import com.kaushlesh.R
import com.kaushlesh.reusable.GPSTracker
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.Identity
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import org.json.JSONObject
import java.util.*


class SplashActivity : AppCompatActivity()/* ,LocationListener*/{

    var TAG = SplashActivity::class.java.simpleName
    private lateinit var locationManager: LocationManager
    private lateinit var txtAppVersion: TextView

    private lateinit var videoView: VideoView

    private val locationPermissionCode = 2

    //notifications


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.requestWindowFeature(Window.FEATURE_NO_TITLE) // Removes title bar
        window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_splash)

        //show update dialog when re-open the app
        Identity.setUpdateDialog(this, "")


        txtAppVersion = findViewById(R.id.txtAppVersion)
        videoView = findViewById(R.id.videoView)

        getCurrentVersion()

        //notifications
        val intent1 = intent
        if (intent1 != null && intent1.extras != null) {
            val extras = intent1.extras

            if (extras != null) {

                var jsonObject: JSONObject? = null
                try {


//                    Log.e("FIREBSE_111", "<<Intent>> description : " + extras.getString("description"))
//                    Log.e("FIREBSE_111", "<<Intent>> post_id : " + extras.getString("post_id"))
//                    Log.e("FIREBSE_111", "<<Intent>> title : " + extras.getString("title"))
//                    Log.e("FIREBSE_111", "<<Intent>> sub_category_id : " + extras.getString("sub_category_id"))
//                    Log.e("FIREBSE_111", "<<Intent>> postImgUrl : " + extras.getString("postImgUrl"))
//                    Log.e("FIREBSE_111", "<<Intent>> category_id : " + extras.getString("category_id"))
//                    Log.e("FIREBSE_111", "<<Intent>> is_read : " + extras.getString("is_read"))
//                    Log.e("FIREBSE_111", "All Data: splash  " + GsonBuilder().setPrettyPrinting().create().toJson(extras))
//                    Log.e("FIREBSE_111", "<<Intent>> title : " + extras.getString("title"))
                    Log.e("PUSH_NOTI_111", "<<Intent>> title : " + extras.getString("title"))


                    val description = extras.getString("description")
                    val post_id = extras.getString("post_id")
                    val title = extras.getString("title")
                    val sub_category_id = extras.getString("sub_category_id")
                    val postImgUrl = extras.getString("postImgUrl")
                    val category_id = extras.getString("category_id")
                    val is_read = extras.getString("is_read")

                    val chatPushTitle = extras.getString("title")

                    val storeusedata = StoreUserData(applicationContext)
                    val isloggedin = storeusedata.getString(Constants.IS_LOGGED_IN)
                    val isactive = storeusedata.getString(Constants.IS_ACTIVE)
                    Utils.showLog(TAG, "==is logged in==" + isloggedin + "== is active ==" + isactive)

                    if (isloggedin.equals("true")) {

                        if(post_id!= null && post_id.toString() != "0"){
                            val intent = Intent(this@SplashActivity, MainActivity::class.java)
                            intent.putExtra("category_id", category_id.toString())
                            intent.putExtra("sub_category_id", sub_category_id.toString())
                            intent.putExtra("post_id", post_id.toString())
                            startActivity(intent)
                            finish()
                        }else if (title!=null && title.toString().equals("New Chat Message")){
                            Log.e("PUSH_NOTI_111", "000")
                            val intent = Intent(this@SplashActivity, MainActivity::class.java)
                            intent.putExtra("openScreen", "chat")
                            startActivity(intent)
                            finish()
                        }else{
                            openMainApp()
                        }

                        //old condition before the chat-push notification implemented
                        /*if(post_id!= null && post_id.toString() != "0"){
                            val intent = Intent(this@SplashActivity, MainActivity::class.java)
                            intent.putExtra("category_id", category_id.toString())
                            intent.putExtra("sub_category_id", sub_category_id.toString())
                            intent.putExtra("post_id", post_id.toString())
                            startActivity(intent)
                            finish()
                        }else{
                            openMainApp()
                        }*/
                    }else{
                        openMainApp()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }else{
                openMainApp()
            }
        }else{
            openMainApp()
        }







        //original here
       /* val storeusedata = StoreUserData(applicationContext)
        val isloggedin = storeusedata.getString(Constants.IS_LOGGED_IN)
        val isactive = storeusedata.getString(Constants.IS_ACTIVE)
        Utils.showLog(TAG, "==is logged in==" + isloggedin + "== is active ==" + isactive)


        Handler().postDelayed({
            if (!isFinishing) {
                val intent: Intent?

                if (isloggedin.equals("true")) {
                    intent = Intent(this@SplashActivity, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {

                    //new added for welcome slider screen
                    intent = Intent(this@SplashActivity, WelcomeActivity::class.java)
                    startActivity(intent)
                    finish()

                    //original
                    *//*intent = Intent(this@SplashActivity, LoginMainActivity::class.java)
                    startActivity(intent)
                    finish()*//*

                }
            }
        }, 3000)*/
    }

    private fun playAnimation(){
        try {
            //val videoHolder = VideoView(this)
            //setContentView(videoView)
            val video = Uri.parse("android.resource://" + packageName + "/" + R.raw.comp)
            videoView.setVideoURI(video)
            videoView.setZOrderOnTop(true);
            videoView.setOnCompletionListener(object : MediaPlayer.OnCompletionListener {
                override fun onCompletion(mp: MediaPlayer?) {
                    jump()
                }
            })
            videoView.start()
        } catch (ex: java.lang.Exception) {
            jump()
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        jump()
        return true
    }

    private fun jump() {
        if (isFinishing) return
        /*startActivity(Intent(this, MainActivity::class.java))
        finish()*/

        val storeusedata = StoreUserData(applicationContext)
        val isloggedin = storeusedata.getString(Constants.IS_LOGGED_IN)

        val intent: Intent?

        if (isloggedin.equals("true")) {
            intent = Intent(this@SplashActivity, MainActivity::class.java)
            startActivity(intent)
            finish()
        } else {

            //new added for welcome slider screen
            intent = Intent(this@SplashActivity, WelcomeActivity::class.java)
            startActivity(intent)
            finish()

            //original
            /*intent = Intent(this@SplashActivity, LoginMainActivity::class.java)
            startActivity(intent)
            finish()*/

        }

    }

    private fun getCurrentVersion() {
        val pm: PackageManager = this.packageManager
        var pInfo: PackageInfo? = null
        try {
            pInfo = pm.getPackageInfo(this.packageName, 0)
        } catch (e1: PackageManager.NameNotFoundException) {
            // TODO Auto-generated catch block
            e1.printStackTrace()
        }
        //currentVersion = pInfo!!.versionName

        txtAppVersion.setText("APP version : " + pInfo!!.versionName)


    }

    fun openMainApp(){
        val storeusedata = StoreUserData(applicationContext)
        val isloggedin = storeusedata.getString(Constants.IS_LOGGED_IN)
        val isactive = storeusedata.getString(Constants.IS_ACTIVE)


        //Deep Linking redirection
        val uri = intent.data
        if (uri != null) {

            if (isloggedin.equals("true")) {

                val parameters = uri.pathSegments // all parameters


                val linkType = parameters[0] // link type parameter
//                Log.d("DEEP_LINKING", "<<linkType>> " + linkType)

                if (linkType.toString().equals("post")){
                    val categoryId = parameters[1]
                    val subCategoryId = parameters[2]
                    val postId = parameters[3]


//                    Log.d("DEEP_LINKING", "<<categoryId>> " + categoryId)
//                    Log.d("DEEP_LINKING", "<<subCategoryId>> " + subCategoryId)
//                    Log.d("DEEP_LINKING", "<<postId>> " + postId)

                    val intent = Intent(this@SplashActivity, MainActivity::class.java)
                    intent.putExtra("category_id", categoryId.toString())
                    intent.putExtra("sub_category_id", subCategoryId.toString())
                    intent.putExtra("post_id", postId.toString())
                    startActivity(intent)
                    finish()
                }else if(linkType.toString().equals("profile")){

                    val userId = parameters[1]

//                    Log.d("DEEP_LINKING", "<<userId>> " + userId)


                    val intent = Intent(this@SplashActivity, MainActivity::class.java)
                    intent.putExtra("linkType", "profile")
                    intent.putExtra("userId", userId.toString())
                    startActivity(intent)
                    finish()

                }else if(linkType.toString().equals("business-profile")){
                    val businessProfileId = parameters[1]

//                    Log.d("DEEP_LINKING", "<<businessProfileId>> " + businessProfileId)

                    val intent = Intent(this@SplashActivity, MainActivity::class.java)
                    intent.putExtra("linkType", "businessprofile")
                    intent.putExtra("businessProfileId", businessProfileId.toString())
                    startActivity(intent)
                    finish()
                }


            }else{
                playAnimation()
            }
        }else{

            playAnimation()

            /*Handler().postDelayed({
                if (!isFinishing) {
                    val intent: Intent?

                    if (isloggedin.equals("true")) {
                        intent = Intent(this@SplashActivity, MainActivity::class.java)
                        startActivity(intent)
                        finish()
                    } else {

                        //new added for welcome slider screen
                        intent = Intent(this@SplashActivity, WelcomeActivity::class.java)
                        startActivity(intent)
                        finish()

                        //original
                        *//*intent = Intent(this@SplashActivity, LoginMainActivity::class.java)
                        startActivity(intent)
                        finish()*//*

                    }
                }
            }, 3000)*/



        }


    }

    fun getLocation() {
        val gpsTracker = GPSTracker(this)
        if (gpsTracker.isGPSEnabled) {
            val latitude: Double = gpsTracker.getLatitude()
            val longitude: Double = gpsTracker.getLongitude()
//            Log.i(TAG, "--- LATITUDE ---${latitude}---LONGITUDE---${longitude}")
        } else {
            gpsTracker.showSettingsAlert()
        }
    }

    private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Need Permissions")
        builder.setMessage("This app needs permission to use this application. You can grant them in app settings.")
        builder.setPositiveButton(
                "GOTO SETTINGS"
        ) { dialog, which ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton(
                "Cancel"
        ) { dialog, which -> dialog.cancel() }
        builder.show()
    }

    // navigating user to app settings
    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", getPackageName(), null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }

}
