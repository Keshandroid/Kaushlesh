package com.kaushlesh.view.activity.AdPost

import android.app.Application
import android.content.DialogInterface
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.google.gson.GsonBuilder
import com.kaushlesh.R
import com.kaushlesh.application.KaushleshApplication
import com.kaushlesh.bean.AdPost.AdPostResponseBean
import com.kaushlesh.constant.AppConstants.printLog
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.MainActivity
import com.kaushlesh.view.fragment.MyAccount.MyAds.MyAdsActivity
import com.kaushlesh.view.fragment.MyAccount.MyAds.MyPostActivity
import kotlinx.android.synthetic.main.activity_congratulation_adpost.*

class CongratulationAdpostActivity : AppCompatActivity(),View.OnClickListener,
    DialogInterface.OnClickListener {

    val TAG = "CongratulationAdpostActivity"
    var pkgdata : AdPostResponseBean.PkgInfo ?= null
    var posttype : String ?= null

    lateinit var storeUserData: StoreUserData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_congratulation_adpost)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        storeUserData = StoreUserData(this)
        setlistners()

        if(intent.extras != null)
        {
            pkgdata = intent.getSerializableExtra("data") as AdPostResponseBean.PkgInfo?
            posttype = intent.getStringExtra("type")

            if(pkgdata != null) {
                setdata()
            }
        }

        Utils.showLog(TAG,"==ad type==" + posttype)
        Utils.showLog(TAG, "==ad post data==" + GsonBuilder().setPrettyPrinting().create().toJson(pkgdata))

    }

    private fun setdata() {

        if(pkgdata!!.pkgtype.equals("Free")) {

            ll_pkg_old.visibility = View.VISIBLE
            ll_pkg_new.visibility = View.GONE

            val catname = Constants.getCategoryFromId(pkgdata?.catid?.toInt()!!,this)
            tv_category.text = catname
            tv_free_allowed.text = pkgdata!!.noofad.toString()
            tv_validity.text = pkgdata!!.pkgvalidity.toString()
            tv_free_ad_used.text =
                (pkgdata!!.noofad!!.toInt() - pkgdata!!.remainingpost!!.toInt()).toString()
            tv_pending_free_ad.text = pkgdata!!.remainingpost.toString()

            tv_free_ad_renew.text = pkgdata!!.freeAdRenewAfter.toString() + " Days"
            tv_total_extra_ad.text = pkgdata!!.totalextraAd.toString()
            tv_used_extra_ad.text = pkgdata!!.usedExtraAd.toString()
            tv_pending_extra_ad.text = pkgdata!!.availExtraAd.toString()
        }
        else {

            ll_pkg_old.visibility = View.GONE
            ll_pkg_new.visibility = View.VISIBLE
            //new screen data

            //tv_details.text = pkgdata!!.noofad + " Ads for " + pkgdata!!.duration + " days"
            tv_details.text = pkgdata!!.name
            tvpuchase.text = pkgdata!!.noofad
            tvused.text = pkgdata!!.remainingpost
            tvavailable.text =
                (pkgdata!!.noofad!!.toInt() - pkgdata!!.remainingpost!!.toInt()).toString()

            val catname1 = Constants.getCategoryFromId(pkgdata!!.catid!!.toInt(),this)
            tv_category1.text = catname1

            val date_after = Utils.formateDateFromstring("dd-MM-yyyy", "dd/MM/yyyy", pkgdata!!.expdate.toString())
            printLog("TAG", "==FORMATED DATE===$date_after")

            if(posttype.equals("pkglist")) {
                tv_date_exp.text = date_after
            }
            else{
                tv_date_exp.text = "-"
            }
        }
    }

    private fun setlistners() {
        tv_sell_urgent_now.setOnClickListener(this)
        tv_ad_preview.setOnClickListener(this)
        btn_back1.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {

            R.id.btn_back1 -> {
                Utils.showLog("TAG","===clicked===")
                storeUserData.setString(Constants.FROM_DETAIL, "false")
                val intent = Intent(applicationContext, MyPostActivity::class.java)
                intent.putExtra("from","attention")
                startActivity(intent)
                finish()
            }

            R.id.tv_sell_urgent_now -> {
                /*val intent = Intent(applicationContext, MainActivity::class.java)
                startActivity(intent)
                finish()*/

                Utils.showAlert(this,resources.getString(R.string.txt_ad_post_sell_urgent_message))
            }

            R.id.tv_ad_preview -> {
                val intent = Intent(applicationContext, MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }

    override fun onClick(p0: DialogInterface?, p1: Int) {
        val intent = Intent(applicationContext, MainActivity::class.java)
        startActivity(intent)
        finishAffinity()
    }

    override fun onBackPressed() {

        storeUserData.setString(Constants.FROM_DETAIL, "false")
        val intent = Intent(applicationContext, MyPostActivity::class.java)
        intent.putExtra("from","attention")
        startActivity(intent)
        finish()
        super.onBackPressed()
    }
}
