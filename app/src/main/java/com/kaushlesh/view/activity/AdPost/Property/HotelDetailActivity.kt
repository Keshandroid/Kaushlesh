package com.kaushlesh.view.activity.AdPost.Property

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.ParcelFileDescriptor
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.kaushlesh.R
import com.kaushlesh.bean.AdPost.ApVillsellandRentBean
import com.kaushlesh.bean.AdPost.HotelandResortBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.utils.AdPostImages
import com.kaushlesh.utils.validations.AdPostPropertyValidation
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.AdPost.UploadActivity1
import com.kaushlesh.view.activity.AdPost.UploadPhotoActivity
import gun0912.tedimagepicker.builder.TedImagePicker
import kotlinx.android.synthetic.main.activity_ap_villa_sell_and_rent.*
import kotlinx.android.synthetic.main.activity_hotel_detail.*
import kotlinx.android.synthetic.main.activity_hotel_detail.et_ad_title
import kotlinx.android.synthetic.main.activity_hotel_detail.et_other_info
import kotlinx.android.synthetic.main.activity_hotel_detail.tv_1
import kotlinx.android.synthetic.main.activity_hotel_detail.tv_2
import java.io.*

class HotelDetailActivity : AppCompatActivity(), View.OnClickListener {
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal lateinit var btnnext: Button
    internal lateinit var tvhotel: TextView
    internal lateinit var tvresort: TextView
    internal lateinit var tvbooking: TextView
    internal lateinit var tvsell: TextView
    internal lateinit var etpropertyname: EditText
    internal lateinit var ettotalrooms: EditText
    internal lateinit var tvbuilder: TextView
    internal lateinit var tvdealer: TextView
    internal lateinit var tvowner: TextView
    internal lateinit var tvdelux: TextView
    internal lateinit var tvsuperdelux: TextView
    internal lateinit var tvsuit: TextView
    internal lateinit var tvall: TextView

    internal lateinit var etadtitile: EditText
    internal lateinit var etotherinfo: EditText

    internal var hotel: Boolean? = false
    internal var resort: Boolean? = false
    internal var booking: Boolean? = false
    internal var sell: Boolean? = false
    internal var builder: Boolean? = false
    internal var dealer: Boolean? = false
    internal var owner: Boolean? = false
    internal var delux: Boolean? = false
    internal var superdelux: Boolean? = false
    internal var suit: Boolean? = false
    internal var all: Boolean? = false
    lateinit var adHotelandResortList: HotelandResortBean
    lateinit var storeUserData: StoreUserData
    var facilitylist: ArrayList<Int> = ArrayList()
    var typeofroom: ArrayList<Int> = ArrayList()
    lateinit var resizebitmapmain: Bitmap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel_detail)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.txt_include_details)

        storeUserData = StoreUserData(this)
        adHotelandResortList = HotelandResortBean()

        initBindViews()

    }

    private fun initBindViews() {
        btnnext = findViewById(R.id.btn_next)

        tvhotel = findViewById(R.id.tvhotel)
        tvresort = findViewById(R.id.tvresort)
        tvbooking = findViewById(R.id.tvbooking)
        tvsell = findViewById(R.id.tvsell)
        etpropertyname = findViewById(R.id.et_pname)
        ettotalrooms = findViewById(R.id.et_rooms)
        tvbuilder = findViewById(R.id.tvbuilder)
        tvdealer = findViewById(R.id.tvdealer)
        tvowner = findViewById(R.id.tvowner)
        tvdelux = findViewById(R.id.tvdelux)
        tvsuperdelux = findViewById(R.id.tvsdelux)
        tvsuit = findViewById(R.id.tvsuit)
        tvall = findViewById(R.id.tvall)

        etadtitile = findViewById(R.id.et_ad_title)
        etotherinfo = findViewById(R.id.et_other_info)

        //setbackgroundSelected(R.drawable.bg_edittext_black)

        btnback.setOnClickListener(this)
        btnnext.setOnClickListener(this)

        tvhotel.setOnClickListener(this)
        tvresort.setOnClickListener(this)

        tvbooking.setOnClickListener(this)
        tvsell.setOnClickListener(this)

        tvbuilder.setOnClickListener(this)
        tvdealer.setOnClickListener(this)
        tvowner.setOnClickListener(this)

        tvdelux.setOnClickListener(this)
        tvsuperdelux.setOnClickListener(this)
        tvsuit.setOnClickListener(this)
        tvall.setOnClickListener(this)

        rl_food.setOnClickListener(this)
        rl_gym.setOnClickListener(this)
        rl_card_payment.setOnClickListener(this)
        rl_wifi.setOnClickListener(this)
        rl_power.setOnClickListener(this)
        rl_security.setOnClickListener(this)
        rl_swimming.setOnClickListener(this)
        rl_fire.setOnClickListener(this)
        rl_room.setOnClickListener(this)

        Utils.setontouch(et_other_info)
        Utils.setontouch1(et_ad_title)

        etadtitile.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {


                tv_1.text = s.length.toString() + "/77"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })

        etotherinfo.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {


                tv_2.text = s.length.toString() + "/5000"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })

        adHotelandResortList.property_type = ""
        adHotelandResortList.purpose = ""
        adHotelandResortList.listed_by =""
        adHotelandResortList.type_of_rooms = ""
        adHotelandResortList.hotel_service_facility = ""

    }

    private fun setbackgroundSelected(b: Int) {
        tvhotel.setBackgroundResource(b)
        tvbooking.setBackgroundResource(b)
        tvowner.setBackgroundResource(b)
        tvdelux.setBackgroundResource(b)
    }


    override fun onClick(view: View) {
        when (view.id) {

            R.id.btn_back -> onBackPressed()

            R.id.btn_next -> {
                //val intent = Intent(applicationContext, UploadPhotoActivity::class.java)
                //startActivity(intent)
                validations()

            }

            R.id.tvhotel -> {
                setValue(true, false, "type")
                setBottomSelection(
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.color.black,
                    R.color.gray,
                    "type"
                )

                adHotelandResortList.property_type = "8"
            }

            R.id.tvresort -> {
                setValue(false, true, "type")
                setBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.black,
                    "type"
                )
                adHotelandResortList.property_type = "9"
            }

            R.id.tvbooking -> {
                setValue(true, false, "purpose")
                setBottomSelection(
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.color.black,
                    R.color.gray,
                    "purpose"
                )

                adHotelandResortList.purpose = "3"
            }

            R.id.tvsell -> {
                setValue(false, true, "purpose")
                setBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.black,
                    "purpose"
                )

                adHotelandResortList.purpose = "2"
            }

            R.id.tvowner -> {
                setTypeValue(true, false, false, "listedby")
                setTypeBottomSelection(
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    "listedby"
                )

                adHotelandResortList.listed_by = "3"
            }

            R.id.tvdealer -> {
                setTypeValue(false, true, false, "listedby")
                setTypeBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    "listedby"
                )
                adHotelandResortList.listed_by = "2"
            }

            R.id.tvbuilder -> {
                setTypeValue(false, false, true, "listedby")
                setTypeBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    "listedby"
                )
                adHotelandResortList.listed_by = "1"
            }

            R.id.tvdelux -> {

                setRoomValue(1, "room",1)

               /* if(delux!!)
                {
                    setTypeFourValue(false, false, false, false, "room")
                    setRoom()
                }
                else {
                    setTypeFourValue(true, false, false, false, "room")
                    setTypeFourBottomSelection(
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.color.black,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        "room"
                    )

                    adHotelandResortList.type_of_rooms = "1"
                }*/
            }

            R.id.tvsdelux -> {

                setRoomValue(2, "room",2)
                /*if(superdelux!!)
                {
                    setTypeFourValue(false, false, false, false, "room")
                    setRoom()
                }
                else {
                    setTypeFourValue(false, true, false, false, "room")
                    setTypeFourBottomSelection(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.color.gray,
                        R.color.black,
                        R.color.gray,
                        R.color.gray,
                        "room"
                    )

                    adHotelandResortList.type_of_rooms = "2"
                }*/
            }

            R.id.tvsuit -> {

                setRoomValue(3, "room",3)

               /* if(suit!!)
                {
                    setTypeFourValue(false, false, false, false, "room")
                    setRoom()
                }
                else {
                    setTypeFourValue(false, false, true, false, "room")
                    setTypeFourBottomSelection(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.color.gray,
                        R.color.gray,
                        R.color.black,
                        R.color.gray,
                        "room"
                    )

                    adHotelandResortList.type_of_rooms = "3"
                }*/
            }

            R.id.tvall -> {
                setTypeFourValue(false, false, false,true, "room")
                setTypeFourBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    "room"
                )
            }

            R.id.rl_food -> {
                setTypetenValue(1, "facility",1)
                //adHotelandResortList.hotel_service_facility = "1"

            }
            R.id.rl_room -> {
                setTypetenValue(2, "facility",9)
                //adHotelandResortList.hotel_service_facility = "9"
            }
            R.id.rl_gym -> {
                setTypetenValue(3, "facility",2)
                //adHotelandResortList.hotel_service_facility = "2"
            }
            R.id.rl_card_payment -> {
                setTypetenValue(4, "facility",3)
                //adHotelandResortList.hotel_service_facility = "3"
            }
            R.id.rl_wifi -> {
                setTypetenValue(5, "facility",4)
                //adHotelandResortList.hotel_service_facility ="4"
            }
            R.id.rl_power -> {
                setTypetenValue(6, "facility",11)
                //adHotelandResortList.hotel_service_facility = "11"
            }
            R.id.rl_security -> {
                setTypetenValue(7, "facility",7)
                //adHotelandResortList.hotel_service_facility = "7"
            }

            R.id.rl_swimming -> {
                setTypetenValue(8, "facility",6)
                //adHotelandResortList.hotel_service_facility = "6"
            }
            R.id.rl_fire -> {
                setTypetenValue(9, "facility",8)
                //adHotelandResortList.hotel_service_facility = "8"
            }
        }
    }

    private fun setRoomValue(i: Int, type: String, id: Int) {
        if(i == 1)
        {
            if(tvdelux.background.getConstantState()?.equals(tvdelux.getContext().getDrawable(R.drawable.bg_edittext_black)?.getConstantState())!!)
            {
                tvdelux.setTextColor(resources.getColor(R.color.gray))
                tvdelux.setBackgroundResource(R.drawable.bg_edittext)
                typeofroom.remove(1)
            }
            else{
                tvdelux.setTextColor(resources.getColor(R.color.black))
                tvdelux.setBackgroundResource(R.drawable.bg_edittext_black)
                typeofroom.add(1)
            }
        }

        if(i == 2)
        {
            if(tvsuperdelux.background.getConstantState()?.equals(tvsuperdelux.getContext().getDrawable(R.drawable.bg_edittext_black)?.getConstantState())!!)
            {
                tvsuperdelux.setTextColor(resources.getColor(R.color.gray))
                tvsuperdelux.setBackgroundResource(R.drawable.bg_edittext)
                typeofroom.remove(2)
            }
            else{
                tvsuperdelux.setTextColor(resources.getColor(R.color.black))
                tvsuperdelux.setBackgroundResource(R.drawable.bg_edittext_black)
                typeofroom.add(2)
            }
        }

        if(i == 3)
        {
            if(tvsuit.background.getConstantState()?.equals(tvsuit.getContext().getDrawable(R.drawable.bg_edittext_black)?.getConstantState())!!)
            {
                tvsuit.setTextColor(resources.getColor(R.color.gray))
                tvsuit.setBackgroundResource(R.drawable.bg_edittext)
                typeofroom.remove(3)
            }
            else{
                tvsuit.setTextColor(resources.getColor(R.color.black))
                tvsuit.setBackgroundResource(R.drawable.bg_edittext_black)
                typeofroom.add(3)
            }
        }
    }

    private fun setRoom() {

        setTypeFourBottomSelection(
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.color.gray,
            R.color.gray,
            R.color.gray,
            R.color.gray,
            "room"
        )

        adHotelandResortList.type_of_rooms = ""
    }

    private fun setTypetenValue(tb1: Int, type: String,id: Int) {

        if(tb1 == 1)
        {
            if(rl_food.background.getConstantState()?.equals(rl_food.getContext().getDrawable(R.drawable.bg_edittext_black)?.getConstantState())!!)
            {
                tv_food.setTextColor(resources.getColor(R.color.gray))
                rl_food.setBackgroundResource(R.drawable.bg_edittext)
                iv_food.setBackgroundResource(R.drawable.ic_food_grey)
                facilitylist.remove(1)
            }
            else{
                tv_food.setTextColor(resources.getColor(R.color.black))
                rl_food.setBackgroundResource(R.drawable.bg_edittext_black)
                iv_food.setBackgroundResource(R.drawable.ic_food_black)
                facilitylist.add(1)
            }
        }


        if (tb1 == 2) {

            if(rl_room.background.getConstantState()?.equals(rl_room.getContext().getDrawable(R.drawable.bg_edittext_black)?.getConstantState())!!)
            {
                tv_room_service.setTextColor(resources.getColor(R.color.gray))
                rl_room.setBackgroundResource(R.drawable.bg_edittext)
                iv_room.setBackgroundResource(R.drawable.ic_room_grey)
                facilitylist.remove(9)
            }
            else{
                tv_room_service.setTextColor(resources.getColor(R.color.black))
                rl_room.setBackgroundResource(R.drawable.bg_edittext_black)
                iv_room.setBackgroundResource(R.drawable.ic_room_black)
                facilitylist.add(9)
            }

        }

        if (tb1 == 3) {

            if(rl_gym.background.getConstantState()?.equals(rl_gym.getContext().getDrawable(R.drawable.bg_edittext_black)?.getConstantState())!!)
            {
                tv_gym.setTextColor(resources.getColor(R.color.gray))
                rl_gym.setBackgroundResource(R.drawable.bg_edittext)
                iv_gym.setBackgroundResource(R.drawable.ic_gym_grey)
                facilitylist.remove(2)
            }
            else{
                tv_gym.setTextColor(resources.getColor(R.color.black))
                rl_gym.setBackgroundResource(R.drawable.bg_edittext_black)
                iv_gym.setBackgroundResource(R.drawable.ic_gym_black)
                facilitylist.add(2)
            }
        }
        if (tb1 == 4) {

            if(rl_card_payment.background.getConstantState()?.equals(rl_card_payment.getContext().getDrawable(R.drawable.bg_edittext_black)?.getConstantState())!!)
            {
                tv_card_payment.setTextColor(resources.getColor(R.color.gray))
                rl_card_payment.setBackgroundResource(R.drawable.bg_edittext)
                iv_card_payment.setBackgroundResource(R.drawable.ic_card_grey)

                facilitylist.remove(3)
            }
            else{
                tv_card_payment.setTextColor(resources.getColor(R.color.black))
                rl_card_payment.setBackgroundResource(R.drawable.bg_edittext_black)
                iv_card_payment.setBackgroundResource(R.drawable.ic_card_black)
                facilitylist.add(3)
            }

        }
        if (tb1 == 5) {

            if(rl_wifi.background.getConstantState()?.equals(rl_wifi.getContext().getDrawable(R.drawable.bg_edittext_black)?.getConstantState())!!)
            {
                tv_wifi.setTextColor(resources.getColor(R.color.gray))
                rl_wifi.setBackgroundResource(R.drawable.bg_edittext)
                iv_wifi.setBackgroundResource(R.drawable.ic_wifi_grey)

                facilitylist.remove(4)
            }
            else{
                tv_wifi.setTextColor(resources.getColor(R.color.black))
                rl_wifi.setBackgroundResource(R.drawable.bg_edittext_black)
                iv_wifi.setBackgroundResource(R.drawable.ic_wifi_black)
                facilitylist.add(4)
            }


        }
        if (tb1 == 6) {

            if(rl_power.background.getConstantState()?.equals(rl_power.getContext().getDrawable(R.drawable.bg_edittext_black)?.getConstantState())!!)
            {
                tv_power.setTextColor(resources.getColor(R.color.gray))
                rl_power.setBackgroundResource(R.drawable.bg_edittext)
                iv_power.setBackgroundResource(R.drawable.ic_powerback_grey)
                facilitylist.remove(11)
            }
            else{
                tv_power.setTextColor(resources.getColor(R.color.black))
                rl_power.setBackgroundResource(R.drawable.bg_edittext_black)
                iv_power.setBackgroundResource(R.drawable.ic_powerback_black)
                facilitylist.add(11)
            }

        }
        if (tb1 == 7) {

            if(rl_security.background.getConstantState()?.equals(rl_security.getContext().getDrawable(R.drawable.bg_edittext_black)?.getConstantState())!!)
            {
                tv_security.setTextColor(resources.getColor(R.color.gray))
                rl_security.setBackgroundResource(R.drawable.bg_edittext)
                iv_security.setBackgroundResource(R.drawable.ic_security_grey)
                facilitylist.remove(7)
            }
            else{
                tv_security.setTextColor(resources.getColor(R.color.black))
                rl_security.setBackgroundResource(R.drawable.bg_edittext_black)
                iv_security.setBackgroundResource(R.drawable.ic_security_black)

                facilitylist.add(7)
            }
        }
        if (tb1 == 8) {

            if(rl_swimming.background.getConstantState()?.equals(rl_swimming.getContext().getDrawable(R.drawable.bg_edittext_black)?.getConstantState())!!)
            {
                tv_swimming.setTextColor(resources.getColor(R.color.gray))
                rl_swimming.setBackgroundResource(R.drawable.bg_edittext)
                iv_swimming.setBackgroundResource(R.drawable.ic_swimming_grey)
                facilitylist.remove(6)
            }
            else{
                tv_swimming.setTextColor(resources.getColor(R.color.black))
                rl_swimming.setBackgroundResource(R.drawable.bg_edittext_black)
                iv_swimming.setBackgroundResource(R.drawable.ic_swimming_black)
                facilitylist.add(6)
            }

        }
        if (tb1 == 9) {

            if(rl_fire.background.getConstantState()?.equals(rl_fire.getContext().getDrawable(R.drawable.bg_edittext_black)?.getConstantState())!!)
            {
                tv_fire.setTextColor(resources.getColor(R.color.gray))
                rl_fire.setBackgroundResource(R.drawable.bg_edittext)
                iv_fire.setBackgroundResource(R.drawable.ic_fire_grey)
                facilitylist.remove(8)
            }
            else{
                tv_fire.setTextColor(resources.getColor(R.color.black))
                rl_fire.setBackgroundResource(R.drawable.bg_edittext_black)
                iv_fire.setBackgroundResource(R.drawable.ic_fire_black)
                facilitylist.add(8)
            }

        }

    }

    private fun setTypeFourValue(
        tb1: Boolean,
        tb2: Boolean,
        tb3: Boolean,
        tb4: Boolean,
        type: String
    ) {

        if (type.equals("room", ignoreCase = true)) {
            delux = tb1
            superdelux = tb2
            suit = tb3
            all = tb4
        }
    }

    private fun setTypeFourBottomSelection(
        t1: Int,
        t2: Int,
        t3: Int,
        t4: Int,
        t5: Int,
        t6: Int,
        t7: Int,
        t8: Int,
        type: String
    ) {

        if (type.equals("room", ignoreCase = true)) {
            tvdelux.setBackgroundResource(t1)
            tvsuperdelux.setBackgroundResource(t2)
            tvsuit.setBackgroundResource(t3)
            tvall.setBackgroundResource(t4)

            tvdelux.setTextColor(resources.getColor(t5))
            tvsuperdelux.setTextColor(resources.getColor(t6))
            tvsuit.setTextColor(resources.getColor(t7))
            tvall.setTextColor(resources.getColor(t8))

        }

    }

    private fun setValue(b1: Boolean, b2: Boolean, type: String) {

        if (type.equals("type", ignoreCase = true)) {
            hotel = b1
            resort = b2
        }

        if (type.equals("purpose", ignoreCase = true)) {
            booking = b1
            sell = b2
        }
    }

    private fun setBottomSelection(b1: Int, b2: Int, b3: Int, b4: Int, type: String) {
        if (type.equals("type", ignoreCase = true)) {

            tvhotel.setBackgroundResource(b1)
            tvresort.setBackgroundResource(b2)

            tvhotel.setTextColor(resources.getColor(b3))
            tvresort.setTextColor(resources.getColor(b4))
        }

        if (type.equals("purpose", ignoreCase = true)) {
            tvbooking.setBackgroundResource(b1)
            tvsell.setBackgroundResource(b2)

            tvbooking.setTextColor(resources.getColor(b3))
            tvsell.setTextColor(resources.getColor(b4))
        }
    }

    private fun setTypeValue(tb1: Boolean, tb2: Boolean, tb3: Boolean, type: String) {

        if (type.equals("listedby", ignoreCase = true)) {
            owner = tb1
            dealer = tb2
            builder = tb3
        }

        if (type.equals("room", ignoreCase = true)) {
            delux = tb1
            superdelux = tb2
            suit = tb3
        }

    }

    private fun setTypeBottomSelection(
        t1: Int,
        t2: Int,
        t3: Int,
        t4: Int,
        t5: Int,
        t6: Int,
        type: String
    ) {

        if (type.equals("listedby", ignoreCase = true)) {
            tvowner.setBackgroundResource(t1)
            tvdealer.setBackgroundResource(t2)
            tvbuilder.setBackgroundResource(t3)

            tvowner.setTextColor(resources.getColor(t4))
            tvdealer.setTextColor(resources.getColor(t5))
            tvbuilder.setTextColor(resources.getColor(t6))
        }

        if (type.equals("room", ignoreCase = true)) {
            tvdelux.setBackgroundResource(t1)
            tvsuperdelux.setBackgroundResource(t2)
            tvsuit.setBackgroundResource(t3)

            tvdelux.setTextColor(resources.getColor(t4))
            tvsuperdelux.setTextColor(resources.getColor(t5))
            tvsuit.setTextColor(resources.getColor(t6))
        }
    }

    companion object {

        private val TAG = "HotelDetailActivity"
    }

    private fun validations() {

        val issubmit = AdPostPropertyValidation.checkForHotelResort(this,adHotelandResortList,etadtitile,etotherinfo,etpropertyname)
        Utils.showLog(TAG,"status" +  issubmit)
        if(issubmit) {

            adHotelandResortList.category_id = storeUserData.getString(Constants.CATEGORY_ID)
            adHotelandResortList.sub_category_id = storeUserData.getString(Constants.SUBCATEGORYID)

            adHotelandResortList.property_name = etpropertyname.text.toString()
            adHotelandResortList.total_rooms = ettotalrooms.text.toString()
            adHotelandResortList.title = etadtitile.text.toString()
            adHotelandResortList.other_information = etotherinfo.text.toString()

            Utils.showLog(TAG,"==facility list==" + facilitylist)
            val facility = TextUtils.join(",",facilitylist)
            Utils.showLog(TAG,"==facility==" + facility)
            adHotelandResortList.hotel_service_facility = facility

            Utils.showLog(TAG,"==type of room list==" + typeofroom)
            val typeroom = TextUtils.join(",",typeofroom)
            Utils.showLog(TAG,"==type room==" + typeroom)
            adHotelandResortList.type_of_rooms = typeroom

            val gson = Gson()
            val json = gson.toJson(adHotelandResortList)
            storeUserData.setString(Constants.ADDHOTELANDRESORT,json)

            Log.e("save", "adOfficeForSell save data: "+storeUserData.getString(Constants.ADDHOTELANDRESORT))

            //val intent = Intent(applicationContext, UploadPhotoActivity::class.java)
            //startActivity(intent)

            setNormalMultiButton()
        }
    }
    fun setNormalMultiButton() {
        val selectedUriList: List<Uri>? = null

        TedImagePicker.with(this)
                .max(12, "Maximum 12 photos allowed")
                .errorListener { message ->
                    Log.d("ted", "message: $message")
                }
                .selectedUri(selectedUriList)
                .startMultiImage { list: List<Uri> ->
                    Log.e("list===", "listsize: ${list.size}")
                    showMultiImage1(list)
                }

    }

    private fun showMultiImage1(uriList: List<Uri>) {

        Log.d("ted=", "uriList: $uriList")
        Log.e("uriList.get(0)====", "uriList.get(0): $uriList.get(0)")

        Thread(Runnable {
            AppConstants.imageList.clear()
            AppConstants.imageListuri.clear()
            uriList.forEach {
                AppConstants.imageListuri.add(it)
                val imagepath = getPathFromGooglePhotosUri(it)
                resizebitmapmain = decodeImageFromFiles(imagepath, 280, 280)
                Log.e("resizebitmapmain====", "01--: $resizebitmapmain")
                val ei = ExifInterface(imagepath.toString())
                val orientation: Int = ei.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED
                )
                var rotatedBitmap: Bitmap? = null

                when (orientation) {
                    ExifInterface.ORIENTATION_ROTATE_90 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 90F)
                    ExifInterface.ORIENTATION_ROTATE_180 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 180F)
                    ExifInterface.ORIENTATION_ROTATE_270 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 270F)
                    ExifInterface.ORIENTATION_NORMAL -> rotatedBitmap = resizebitmapmain
                    else -> rotatedBitmap = resizebitmapmain
                }

                AppConstants.imageList.add(rotatedBitmap!!)
            }

            Log.e("imageListsize====", "viewSize: ${AppConstants.imageList.size}")

            if(AppConstants.imageList!=null)
            {
                Log.e("AppConstantsimageList==", "viewSize: ${AppConstants.imageList.size}")
                checkAndSaveHotelResort(this)
            }

        }).start()


        AdPostImages.changeScreentoNext(this)

    }

    private fun checkAndSaveHotelResort(context: Context) {
        ///  bimglist.clear()
        val storeUserData = StoreUserData(context)
        val gson = Gson()

        if (AppConstants.imageList.size >= 1) {

            Log.e("test", "imagelist: " + AppConstants.imageList.size+"===")

            val json: String = storeUserData.getString(Constants.ADDHOTELANDRESORT)

            Log.e("String", "String data: " + json+"====")

            val hotelandResortBean = gson.fromJson(json, HotelandResortBean::class.java)

            Log.e("test", "listsave data: " + hotelandResortBean.toString()+"====")
            hotelandResortBean.post_images?.clear()
            hotelandResortBean.post_images = AppConstants.imageList
            Log.e("test", "post_images: " +  hotelandResortBean.post_images!!.size+"===")


            Log.e("test", "post_images data: " + hotelandResortBean.post_images)

            val jsonset = gson.toJson(hotelandResortBean)

            storeUserData.setString(Constants.ADDHOTELANDRESORT, jsonset)

            ////   AdPostImages.changeScreentoLocation(context)

        } else {
            Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
        }
    }

    fun rotateImage(source: Bitmap, angle: Float): Bitmap? {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(
                source, 0, 0, source.width, source.height,
                matrix, true
        )
    }

    fun decodeImageFromFiles(path: String?, width: Int, height: Int): Bitmap {
        val scaleOptions = BitmapFactory.Options()
        scaleOptions.inJustDecodeBounds = true
        BitmapFactory.decodeFile(path, scaleOptions)
        var scale = 1
        while (scaleOptions.outWidth / scale / 2 >= width
                && scaleOptions.outHeight / scale / 2 >= height
        ) {
            scale *= 2
        }
        // decode with the sample size
        val outOptions = BitmapFactory.Options()
        outOptions.inSampleSize = scale
        val bitmap: Bitmap = BitmapFactory.decodeFile(path, outOptions)
        Utils.showLog(
                UploadActivity1.TAG,
                "===BitmapFactory===width" + bitmap.width + "===height" + bitmap.height + "==="
        )

        return bitmap
    }

    fun getPathFromGooglePhotosUri(uriPhoto: Uri?): String? {
        if (uriPhoto == null) return null
        var input: FileInputStream? = null
        var output: FileOutputStream? = null
        try {
            val pfd: ParcelFileDescriptor? = contentResolver.openFileDescriptor(uriPhoto, "r")
            val fd: FileDescriptor = pfd!!.getFileDescriptor()
            input = FileInputStream(fd)
            val tempFilename: String = getTempFilename(this)
            output = FileOutputStream(tempFilename)
            var read: Int = 0
            val bytes = ByteArray(4096)
            while (input.read(bytes).also({ read = it }) != -1) {
                output.write(bytes, 0, read)
            }
            return tempFilename
        } catch (ignored: IOException) {
            // Nothing we can do
        } finally {
            closeSilently(input)
            closeSilently(output)
        }
        return null
    }

    @Throws(IOException::class)
    private fun getTempFilename(context: Context): String {
        val outputDir = context.cacheDir
        val outputFile = File.createTempFile("image", "tmp", outputDir)
        return outputFile.absolutePath
    }

    fun closeSilently(c: Closeable?) {
        if (c == null) return
        try {
            c.close()
        } catch (t: Throwable) {
            // Do nothing
        }
    }
}

