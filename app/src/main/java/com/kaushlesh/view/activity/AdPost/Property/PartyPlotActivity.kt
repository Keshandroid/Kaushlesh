package com.kaushlesh.view.activity.AdPost.Property

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.ParcelFileDescriptor
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.kaushlesh.R
import com.kaushlesh.bean.AdPost.AdPartyPlotBean
import com.kaushlesh.bean.AdPost.ApVillsellandRentBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.utils.AdPostImages
import com.kaushlesh.utils.validations.AdPostPropertyValidation
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.AdPost.UploadActivity1
import com.kaushlesh.view.activity.AdPost.UploadPhotoActivity
import gun0912.tedimagepicker.builder.TedImagePicker
import kotlinx.android.synthetic.main.activity_ap_villa_sell_and_rent.*
import kotlinx.android.synthetic.main.activity_party_plot.*
import kotlinx.android.synthetic.main.activity_party_plot.et_ad_title
import kotlinx.android.synthetic.main.activity_party_plot.et_other_info
import kotlinx.android.synthetic.main.activity_party_plot.tv_1
import java.io.*

class PartyPlotActivity : AppCompatActivity(), View.OnClickListener {
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal lateinit var btnnext: Button
    internal lateinit var etpartyplotname: EditText
    internal lateinit var etguestcapacity: EditText
    internal lateinit var etnoofrooms: EditText
    internal lateinit var tvbooking: TextView
    internal lateinit var tvsell: TextView
    internal lateinit var ivguestyselect: ImageView
    internal lateinit var ivguestyunselect: ImageView
    internal lateinit var ivguestnselect: ImageView
    internal lateinit var ivguestnunselect: ImageView
    internal lateinit var ivkichyselect: ImageView
    internal lateinit var ivkichyunselect: ImageView
    internal lateinit var ivkichnselect: ImageView
    internal lateinit var ivkichnunselect: ImageView
    internal lateinit var ivwashyselect: ImageView
    internal lateinit var ivwashyunselect: ImageView
    internal lateinit var ivwashnselect: ImageView
    internal lateinit var ivwashnunselect: ImageView
    internal lateinit var etadtitile: EditText
    internal lateinit var etotherinfo: EditText

    internal lateinit var tvbuilder: TextView
    internal lateinit var tvdealer: TextView
    internal lateinit var tvowner: TextView

    internal var booking: Boolean? = false
    internal var sell: Boolean? = false
    internal var groom_avail_yes: Boolean? = true
    internal var groom_avail_no: Boolean? = false
    internal var kich_yes: Boolean? = true
    internal var kich_no: Boolean? = false
    internal var wash_yes: Boolean? = true
    internal var wash_no: Boolean? = false

    internal var builder: Boolean? = false
    internal var dealer: Boolean? = false
    internal var owner: Boolean? = false

    lateinit var adPartyPlotList: AdPartyPlotBean
    lateinit var storeUserData: StoreUserData
    var isallow : Boolean = false
    lateinit var resizebitmapmain: Bitmap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_party_plot)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        storeUserData = StoreUserData(this)
        adPartyPlotList = AdPartyPlotBean()

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.txt_include_details)

        initBindViews()
    }

    private fun initBindViews() {
        btnnext = findViewById(R.id.btn_next)

        etpartyplotname = findViewById(R.id.et_partyplotname)
        etguestcapacity = findViewById(R.id.et_guest_capacity)
        etnoofrooms = findViewById(R.id.et_guestroom_no)
        tvbooking = findViewById(R.id.tvbooking)
        tvsell = findViewById(R.id.tvsell)
        ivguestyselect = findViewById(R.id.iv_yes_select)
        ivguestyunselect = findViewById(R.id.iv_yes_unselect)
        ivguestnselect = findViewById(R.id.iv_no_select)
        ivguestnunselect = findViewById(R.id.iv_no_unselect)
        ivkichyselect = findViewById(R.id.iv_ky_select)
        ivkichyunselect = findViewById(R.id.iv_ky_unselect)
        ivkichnselect = findViewById(R.id.iv_kno_select)
        ivkichnunselect = findViewById(R.id.iv_kno_unselect)
        ivwashyselect = findViewById(R.id.iv_wy_select)
        ivwashyunselect = findViewById(R.id.iv_wy_unselect)
        ivwashnselect = findViewById(R.id.iv_wno_select)
        ivwashnunselect = findViewById(R.id.iv_wno_unselect)
        etadtitile = findViewById(R.id.et_ad_title)
        etotherinfo = findViewById(R.id.et_other_info)

        tvbuilder = findViewById(R.id.tvbuilder)
        tvdealer = findViewById(R.id.tvdealer)
        tvowner = findViewById(R.id.tvowner)

        //tvbooking.setBackgroundResource(R.drawable.bg_edittext_black)
        //tvowner.setBackgroundResource(R.drawable.bg_edittext_black)

        btnback.setOnClickListener(this)
        btnnext.setOnClickListener(this)

        tvbooking.setOnClickListener(this)
        tvsell.setOnClickListener(this)

        tvbuilder.setOnClickListener(this)
        tvdealer.setOnClickListener(this)
        tvowner.setOnClickListener(this)

        ivguestyunselect.setOnClickListener(this)
        ivguestnunselect.setOnClickListener(this)

        ivkichyunselect.setOnClickListener(this)
        ivkichnunselect.setOnClickListener(this)

        ivwashyunselect.setOnClickListener(this)
        ivwashnunselect.setOnClickListener(this)

        Utils.setontouch(et_other_info)
        Utils.setontouch1(et_ad_title)

        etadtitile.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {


                tv_1.text = s.length.toString() + "/77"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })

        etotherinfo.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {


                tv_2.text = s.length.toString() + "/5000"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })

        adPartyPlotList.purpose = ""
        adPartyPlotList.listed_by = ""
        adPartyPlotList.guest_room_available = "1"
        adPartyPlotList.kitchen_available = "1"
        adPartyPlotList.wash_room_available = "1"

        if(groom_avail_yes!!){
            ll_no_of_guestromm.visibility = View.VISIBLE
        }
        else
        {
            ll_no_of_guestromm.visibility = View.GONE
        }
    }

    override fun onClick(view: View) {
        when (view.id) {

            R.id.btn_back -> onBackPressed()

            R.id.btn_next -> {
                //val intent = Intent(applicationContext, UploadPhotoActivity::class.java)
                //startActivity(intent)
                validations()

            }

            R.id.tvbooking -> {
                setValue(true, false, "purpose")
                setBottomSelection(
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.color.black,
                    R.color.gray,
                    "purpose"
                )

                adPartyPlotList.purpose = "3"
            }

            R.id.tvsell -> {
                setValue(false, true, "purpose")
                setBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.black,
                    "purpose"
                )
                adPartyPlotList.purpose = "2"
            }

            R.id.tvowner -> {
                setTypeValue(true, false, false, "listedby")
                setTypeBottomSelection(
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    "listedby"
                )

                adPartyPlotList.listed_by = "3"
            }

            R.id.tvdealer -> {
                setTypeValue(false, true, false, "listedby")
                setTypeBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    "listedby"
                )

                adPartyPlotList.listed_by = "2"
            }

            R.id.tvbuilder -> {
                setTypeValue(false, false, true, "listedby")
                setTypeBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    "listedby"
                )

                adPartyPlotList.listed_by = "1"
            }


            R.id.iv_yes_unselect -> {
                groom_avail_yes = true
                groom_avail_no = false
                ivguestyselect.visibility = View.VISIBLE
                ivguestnselect.visibility = View.GONE

                adPartyPlotList.guest_room_available = "1"

                ll_no_of_guestromm.visibility = View.VISIBLE
            }

            R.id.iv_no_unselect -> {
                groom_avail_yes = false
                groom_avail_no = true
                ivguestyselect.visibility = View.GONE
                ivguestnselect.visibility = View.VISIBLE

                adPartyPlotList.guest_room_available = "2"

                ll_no_of_guestromm.visibility = View.GONE
            }

            R.id.iv_ky_unselect -> {
                kich_yes = true
                kich_no = false
                ivkichyselect.visibility = View.VISIBLE
                ivkichnselect.visibility = View.GONE

                adPartyPlotList.kitchen_available = "1"
            }

            R.id.iv_kno_unselect -> {
                kich_yes = false
                kich_no = true
                ivkichyselect.visibility = View.GONE
                ivkichnselect.visibility = View.VISIBLE

                adPartyPlotList.kitchen_available = "2"
            }

            R.id.iv_wy_unselect -> {
                wash_yes = true
                wash_no = false
                ivwashyselect.visibility = View.VISIBLE
                ivwashnselect.visibility = View.GONE

                adPartyPlotList.wash_room_available = "1"
            }

            R.id.iv_wno_unselect -> {
                wash_yes = false
                wash_no = true
                ivwashyselect.visibility = View.GONE
                ivwashnselect.visibility = View.VISIBLE

                adPartyPlotList.wash_room_available = "2"
            }
        }
    }

    private fun validations() {

        val issubmit = AdPostPropertyValidation.checkForPartyPlot(this,adPartyPlotList,etpartyplotname,etguestcapacity,etnoofrooms,etadtitile,etotherinfo,groom_avail_yes!!)
        Utils.showLog(TAG,"status" +  issubmit)
        if(issubmit) {
            adPartyPlotList.category_id = storeUserData.getString(Constants.CATEGORY_ID)
            adPartyPlotList.sub_category_id = storeUserData.getString(Constants.SUBCATEGORYID)

            adPartyPlotList.party_plot_name= etpartyplotname.text.toString()
            adPartyPlotList.guest_capacity = etguestcapacity.text.toString()
            adPartyPlotList.no_of_guest_room = etnoofrooms.text.toString()
            adPartyPlotList.title= etadtitile.text.toString()
            adPartyPlotList.other_information = etotherinfo.text.toString()

            val gson = Gson()
            val json = gson.toJson(adPartyPlotList)
            storeUserData.setString(Constants.ADDPARTYPLOT,json)

            Log.e("save", "adOfficeForSell save data: "+storeUserData.getString(Constants.ADDPARTYPLOT))

            //al intent = Intent(applicationContext, UploadPhotoActivity::class.java)
            //startActivity(intent)

            setNormalMultiButton()
        }

    }

    fun setNormalMultiButton() {
        val selectedUriList: List<Uri>? = null

        TedImagePicker.with(this)
                .max(12, "Maximum 12 photos allowed")
                .errorListener { message ->
                    Log.d("ted", "message: $message")
                }
                .selectedUri(selectedUriList)
                .startMultiImage { list: List<Uri> ->
                    Log.e("list===", "listsize: ${list.size}")
                    showMultiImage1(list)
                }

    }

    private fun showMultiImage1(uriList: List<Uri>) {

        Log.d("ted=", "uriList: $uriList")
        Log.e("uriList.get(0)====", "uriList.get(0): $uriList.get(0)")

        Thread(Runnable {
            AppConstants.imageList.clear()
            AppConstants.imageListuri.clear()
            uriList.forEach {
                AppConstants.imageListuri.add(it)
                val imagepath = getPathFromGooglePhotosUri(it)
                resizebitmapmain = decodeImageFromFiles(imagepath, 280, 280)
                Log.e("resizebitmapmain====", "01--: $resizebitmapmain")
                val ei = ExifInterface(imagepath.toString())
                val orientation: Int = ei.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED
                )
                var rotatedBitmap: Bitmap? = null

                when (orientation) {
                    ExifInterface.ORIENTATION_ROTATE_90 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 90F)
                    ExifInterface.ORIENTATION_ROTATE_180 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 180F)
                    ExifInterface.ORIENTATION_ROTATE_270 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 270F)
                    ExifInterface.ORIENTATION_NORMAL -> rotatedBitmap = resizebitmapmain
                    else -> rotatedBitmap = resizebitmapmain
                }

                AppConstants.imageList.add(rotatedBitmap!!)
            }

            Log.e("imageListsize====", "viewSize: ${AppConstants.imageList.size}")

            if(AppConstants.imageList!=null)
            {
                Log.e("AppConstantsimageList==", "viewSize: ${AppConstants.imageList.size}")
                checkAndSavePartyPlot(this)
            }

        }).start()

        AdPostImages.changeScreentoNext(this)

    }

    private fun checkAndSavePartyPlot(context: Context) {
        ///  bimglist.clear()
        val storeUserData = StoreUserData(context)
        val gson = Gson()

        if (AppConstants.imageList.size >= 1) {

            Log.e("test", "imagelist: " + AppConstants.imageList.size+"===")

            val json: String = storeUserData.getString(Constants.ADDPARTYPLOT)

            Log.e("String", "String data: " + json+"====")

            val adPartyPlotBean = gson.fromJson(json, AdPartyPlotBean::class.java)

            Log.e("test", "listsave data: " + adPartyPlotBean.toString()+"====")
            adPartyPlotBean.post_images?.clear()
            adPartyPlotBean.post_images = AppConstants.imageList
            Log.e("test", "post_images: " +  adPartyPlotBean.post_images!!.size+"===")


            Log.e("test", "post_images data: " + adPartyPlotBean.post_images)

            val jsonset = gson.toJson(adPartyPlotBean)

            storeUserData.setString(Constants.ADDPARTYPLOT, jsonset)

            ////   AdPostImages.changeScreentoLocation(context)

        } else {
            Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
        }
    }

    fun rotateImage(source: Bitmap, angle: Float): Bitmap? {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(
                source, 0, 0, source.width, source.height,
                matrix, true
        )
    }

    fun decodeImageFromFiles(path: String?, width: Int, height: Int): Bitmap {
        val scaleOptions = BitmapFactory.Options()
        scaleOptions.inJustDecodeBounds = true
        BitmapFactory.decodeFile(path, scaleOptions)
        var scale = 1
        while (scaleOptions.outWidth / scale / 2 >= width
                && scaleOptions.outHeight / scale / 2 >= height
        ) {
            scale *= 2
        }
        // decode with the sample size
        val outOptions = BitmapFactory.Options()
        outOptions.inSampleSize = scale
        val bitmap: Bitmap = BitmapFactory.decodeFile(path, outOptions)
        Utils.showLog(
                UploadActivity1.TAG,
                "===BitmapFactory===width" + bitmap.width + "===height" + bitmap.height + "==="
        )

        return bitmap
    }

    fun getPathFromGooglePhotosUri(uriPhoto: Uri?): String? {
        if (uriPhoto == null) return null
        var input: FileInputStream? = null
        var output: FileOutputStream? = null
        try {
            val pfd: ParcelFileDescriptor? = contentResolver.openFileDescriptor(uriPhoto, "r")
            val fd: FileDescriptor = pfd!!.getFileDescriptor()
            input = FileInputStream(fd)
            val tempFilename: String = getTempFilename(this)
            output = FileOutputStream(tempFilename)
            var read: Int = 0
            val bytes = ByteArray(4096)
            while (input.read(bytes).also({ read = it }) != -1) {
                output.write(bytes, 0, read)
            }
            return tempFilename
        } catch (ignored: IOException) {
            // Nothing we can do
        } finally {
            closeSilently(input)
            closeSilently(output)
        }
        return null
    }

    @Throws(IOException::class)
    private fun getTempFilename(context: Context): String {
        val outputDir = context.cacheDir
        val outputFile = File.createTempFile("image", "tmp", outputDir)
        return outputFile.absolutePath
    }

    fun closeSilently(c: Closeable?) {
        if (c == null) return
        try {
            c.close()
        } catch (t: Throwable) {
            // Do nothing
        }
    }

    private fun setValue(b1: Boolean, b2: Boolean, type: String) {

        if (type.equals("purpose", ignoreCase = true)) {
            booking = b1
            sell = b2
        }
    }

    private fun setBottomSelection(b1: Int, b2: Int, b3: Int, b4: Int, type: String) {

        if (type.equals("purpose", ignoreCase = true)) {
            tvbooking.setBackgroundResource(b1)
            tvsell.setBackgroundResource(b2)

            tvbooking.setTextColor(resources.getColor(b3))
            tvsell.setTextColor(resources.getColor(b4))
        }
    }

    private fun setTypeValue(tb1: Boolean, tb2: Boolean, tb3: Boolean, type: String) {

        if (type.equals("listedby", ignoreCase = true)) {
            owner = tb1
            dealer = tb2
            builder = tb3
        }

    }

    private fun setTypeBottomSelection(
        t1: Int,
        t2: Int,
        t3: Int,
        t4: Int,
        t5: Int,
        t6: Int,
        type: String
    ) {

        if (type.equals("listedby", ignoreCase = true)) {
            tvowner.setBackgroundResource(t1)
            tvdealer.setBackgroundResource(t2)
            tvbuilder.setBackgroundResource(t3)

            tvowner.setTextColor(resources.getColor(t4))
            tvdealer.setTextColor(resources.getColor(t5))
            tvbuilder.setTextColor(resources.getColor(t6))
        }
    }

    companion object {

        private val TAG = "PartyPlotActivity"
    }
}

