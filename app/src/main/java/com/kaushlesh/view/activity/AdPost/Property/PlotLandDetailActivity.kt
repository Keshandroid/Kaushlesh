package com.kaushlesh.view.activity.AdPost.Property

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.ParcelFileDescriptor
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.kaushlesh.R
import com.kaushlesh.bean.AdPost.AdPlotAndLandBean
import com.kaushlesh.bean.AdPost.ApVillsellandRentBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.utils.AdPostImages
import com.kaushlesh.utils.validations.AdPostPropertyValidation
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.AdPost.UploadActivity1
import com.kaushlesh.view.activity.AdPost.UploadPhotoActivity
import gun0912.tedimagepicker.builder.TedImagePicker
import kotlinx.android.synthetic.main.activity_ap_villa_sell_and_rent.*
import kotlinx.android.synthetic.main.activity_plot_land_detail.*
import kotlinx.android.synthetic.main.activity_plot_land_detail.et_ad_title
import kotlinx.android.synthetic.main.activity_plot_land_detail.et_other_info
import kotlinx.android.synthetic.main.activity_plot_land_detail.tv_1
import java.io.*

class PlotLandDetailActivity : AppCompatActivity(), View.OnClickListener {
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal lateinit var btnnext: Button
    internal lateinit var etname: EditText
    internal lateinit var etplotarea: EditText
    internal lateinit var etlength: EditText
    internal lateinit var etwidth: EditText
    internal lateinit var etadtitle: EditText
    internal lateinit var etotherinfo: EditText
    internal lateinit var tvrent: TextView
    internal lateinit var tvsell: TextView
    internal lateinit var tvnorth: TextView
    internal lateinit var tvsouth: TextView
    internal lateinit var tveast: TextView
    internal lateinit var tvwest: TextView
    internal lateinit var tvbuilder: TextView
    internal lateinit var tvdealer: TextView
    internal lateinit var tvowner: TextView

    internal var agriculture: Boolean? = false
    internal var commercial: Boolean? = false
    internal var residential: Boolean? = false

    internal var rent: Boolean? = false
    internal var sell: Boolean? = false
    internal var north: Boolean? = false
    internal var south: Boolean? = false
    internal var east: Boolean? = false
    internal var west: Boolean? = false
    internal var builder: Boolean? = false
    internal var dealer: Boolean? = false
    internal var owner: Boolean? = false

    lateinit var storeUserData: StoreUserData
    lateinit var adPlotAndLandList: AdPlotAndLandBean
    var isallow : Boolean = false
    lateinit var resizebitmapmain: Bitmap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_plot_land_detail)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        storeUserData = StoreUserData(this)
        adPlotAndLandList = AdPlotAndLandBean()

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.txt_include_details)

        initBindViews()
    }

    private fun initBindViews() {
        btnnext = findViewById(R.id.btn_next)

        etname = findViewById(R.id.et_name)
        etplotarea = findViewById(R.id.et_plot_area)
        etlength = findViewById(R.id.et_length)
        etwidth = findViewById(R.id.et_width)
        etadtitle = findViewById(R.id.et_ad_title)
        etotherinfo = findViewById(R.id.et_other_info)
        tvrent = findViewById(R.id.tvrent)
        tvsell = findViewById(R.id.tvsell)
        tvnorth = findViewById(R.id.tvnorth)
        tvsouth = findViewById(R.id.tvsouth)
        tveast = findViewById(R.id.tveast)
        tvwest = findViewById(R.id.tvwest)
        tvbuilder = findViewById(R.id.tvbuilder)
        tvdealer = findViewById(R.id.tvdealer)
        tvowner = findViewById(R.id.tvowner)

        //setbackgroundSelected(R.drawable.bg_edittext_black)


        btnback.setOnClickListener(this)
        btnnext.setOnClickListener(this)

        tvrent.setOnClickListener(this)
        tvsell.setOnClickListener(this)

        tvbuilder.setOnClickListener(this)
        tvdealer.setOnClickListener(this)
        tvowner.setOnClickListener(this)


        tvnorth.setOnClickListener(this)
        tvsouth.setOnClickListener(this)
        tveast.setOnClickListener(this)
        tvwest.setOnClickListener(this)

        tvagri.setOnClickListener(this)
        tvcommercial.setOnClickListener(this)
        tvresidential.setOnClickListener(this)

        Utils.setontouch(et_other_info)
        Utils.setontouch1(et_ad_title)

        etadtitle.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {


                tv_1.text = s.length.toString() + "/77"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })

        etotherinfo.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {


                tv_2.text = s.length.toString() + "/5000"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })

        adPlotAndLandList.property_type= ""
        adPlotAndLandList.purpose= ""
        adPlotAndLandList.listed_by = ""
        adPlotAndLandList.facing = ""
    }

    private fun setbackgroundSelected(b: Int) {
        tvrent.setBackgroundResource(b)
        tvnorth.setBackgroundResource(b)
        tvagri.setBackgroundResource(b)
        tvowner.setBackgroundResource(b)
    }

    override fun onClick(view: View) {
        when (view.id) {

            R.id.btn_back -> onBackPressed()

            R.id.btn_next -> {
//                val intent = Intent(applicationContext, UploadPhotoActivity::class.java)
//                startActivity(intent)

                validations()

            }

            R.id.tvrent -> {
                setValue(true, false, "purpose")
                setBottomSelection(
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.color.black,
                    R.color.gray,
                    "purpose"
                )
                adPlotAndLandList.purpose = "1"
            }

            R.id.tvsell -> {
                setValue(false, true, "purpose")
                setBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.black,
                    "purpose"
                )
                adPlotAndLandList.purpose = "2"
            }

            R.id.tvnorth -> {
                if(north!!)
                {
                    setTypeFourValue(false, false, false, false, "facing")
                    setfacing()
                }
                else {

                    setTypeFourValue(true, false, false, false, "facing")
                    setTypeFourBottomSelection(
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.color.black,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        "facing"
                    )
                    adPlotAndLandList.facing = "1"
                }
            }

            R.id.tvsouth -> {

                if(south!!)
                {
                    setTypeFourValue(false, false, false, false, "facing")
                    setfacing()
                }
                else {
                    setTypeFourValue(false, true, false, false, "facing")
                    setTypeFourBottomSelection(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.color.gray,
                        R.color.black,
                        R.color.gray,
                        R.color.gray,
                        "facing"
                    )
                    adPlotAndLandList.facing = "2"
                }
            }

            R.id.tveast -> {
                if(east!!)
                {
                    setTypeFourValue(false, false, false, false, "facing")
                    setfacing()
                }
                else {
                    setTypeFourValue(false, false, true, false, "facing")
                    setTypeFourBottomSelection(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.drawable.bg_edittext,
                        R.color.gray,
                        R.color.gray,
                        R.color.black,
                        R.color.gray,
                        "facing"
                    )
                    adPlotAndLandList.facing = "3"
                }
            }

            R.id.tvwest -> {
                if(west!!)
                {
                    setTypeFourValue(false, false, false, false, "facing")
                    setfacing()
                }
                else {
                    setTypeFourValue(false, false, false, true, "facing")
                    setTypeFourBottomSelection(
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext,
                        R.drawable.bg_edittext_black,
                        R.color.gray,
                        R.color.gray,
                        R.color.gray,
                        R.color.black,
                        "facing"
                    )
                    adPlotAndLandList.facing = "4"
                }
            }

            R.id.tvagri -> {
                setTypeValue(true, false, false, "type")
                setTypeBottomSelection(
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    "type"
                )
                adPlotAndLandList.property_type = "5"

            }

            R.id.tvcommercial -> {
                setTypeValue(false, true, false, "type")
                setTypeBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    "type"
                )
                adPlotAndLandList.property_type = "6"
            }

            R.id.tvresidential -> {
                setTypeValue(false, false, true, "type")
                setTypeBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    "type"
                )
                adPlotAndLandList.property_type = "7"
            }

            R.id.tvowner -> {
                setTypeValue(true, false, false, "listedby")
                setTypeBottomSelection(
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.color.black,
                    R.color.gray,
                    R.color.gray,
                    "listedby"
                )
                adPlotAndLandList.listed_by = "3"
            }

            R.id.tvdealer -> {
                setTypeValue(false, true, false, "listedby")
                setTypeBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.drawable.bg_edittext,
                    R.color.gray,
                    R.color.black,
                    R.color.gray,
                    "listedby"
                )
                adPlotAndLandList.listed_by = "2"
            }

            R.id.tvbuilder -> {
                setTypeValue(false, false, true, "listedby")
                setTypeBottomSelection(
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext,
                    R.drawable.bg_edittext_black,
                    R.color.gray,
                    R.color.gray,
                    R.color.black,
                    "listedby"
                )
                adPlotAndLandList.listed_by = "1"
            }

        }
    }


    private fun setfacing() {

        setTypeFourBottomSelection(
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.drawable.bg_edittext,
            R.color.gray,
            R.color.gray,
            R.color.gray,
            R.color.gray,
            "facing"
        )
        adPlotAndLandList.facing= ""
    }

    private fun validations(){
        val issubmit = AdPostPropertyValidation.checkForPlotLand(this,adPlotAndLandList,etplotarea,etadtitle,etotherinfo)
        Utils.showLog(TAG,"status" +  issubmit)
        if(issubmit)
        {
            adPlotAndLandList.category_id = storeUserData.getString(Constants.CATEGORY_ID)
            adPlotAndLandList.sub_category_id = storeUserData.getString(Constants.SUBCATEGORYID)

            adPlotAndLandList.project_name= etname.text.toString()
            adPlotAndLandList.plot_area = etplotarea.text.toString()
            adPlotAndLandList.length = etlength.text.toString()
            adPlotAndLandList.width= etwidth.text.toString()
            adPlotAndLandList.title = etadtitle.text.toString()
            adPlotAndLandList.other_information = etotherinfo.text.toString()

            val gson = Gson()
            val json = gson.toJson(adPlotAndLandList)
            storeUserData.setString(Constants.ADDPLOTANDLAND,json)

            Log.e("save", "adOfficeForSell save data: "+storeUserData.getString(Constants.ADDPLOTANDLAND))

            //val intent = Intent(applicationContext, UploadPhotoActivity::class.java)
            //startActivity(intent)

            setNormalMultiButton()
        }

    }

    fun setNormalMultiButton() {
        val selectedUriList: List<Uri>? = null

        TedImagePicker.with(this)
                .max(12, "Maximum 12 photos allowed")
                .errorListener { message ->
                    Log.d("ted", "message: $message")
                }
                .selectedUri(selectedUriList)
                .startMultiImage { list: List<Uri> ->
                    Log.e("list===", "listsize: ${list.size}")
                    showMultiImage1(list)
                }

    }

    private fun showMultiImage1(uriList: List<Uri>) {

        Log.d("ted=", "uriList: $uriList")
        Log.e("uriList.get(0)====", "uriList.get(0): $uriList.get(0)")

        Thread(Runnable {
            AppConstants.imageList.clear()
            AppConstants.imageListuri.clear()
            uriList.forEach {
                AppConstants.imageListuri.add(it)
                val imagepath = getPathFromGooglePhotosUri(it)
                resizebitmapmain = decodeImageFromFiles(imagepath, 280, 280)
                Log.e("resizebitmapmain====", "01--: $resizebitmapmain")
                val ei = ExifInterface(imagepath.toString())
                val orientation: Int = ei.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED
                )
                var rotatedBitmap: Bitmap? = null

                when (orientation) {
                    ExifInterface.ORIENTATION_ROTATE_90 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 90F)
                    ExifInterface.ORIENTATION_ROTATE_180 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 180F)
                    ExifInterface.ORIENTATION_ROTATE_270 -> rotatedBitmap =
                            rotateImage(resizebitmapmain, 270F)
                    ExifInterface.ORIENTATION_NORMAL -> rotatedBitmap = resizebitmapmain
                    else -> rotatedBitmap = resizebitmapmain
                }

                AppConstants.imageList.add(rotatedBitmap!!)
            }

            Log.e("imageListsize====", "viewSize: ${AppConstants.imageList.size}")

            if(AppConstants.imageList!=null)
            {
                Log.e("AppConstantsimageList==", "viewSize: ${AppConstants.imageList.size}")
                checkAndSavePlotLand(this)
            }

        }).start()

        AdPostImages.changeScreentoNext(this)

    }

    private fun checkAndSavePlotLand(context: Context) {
        ///  bimglist.clear()
        val storeUserData = StoreUserData(context)
        val gson = Gson()

        if (AppConstants.imageList.size >= 1) {

            Log.e("test", "imagelist: " + AppConstants.imageList.size+"===")

            val json: String = storeUserData.getString(Constants.ADDPLOTANDLAND)

            Log.e("String", "String data: " + json+"====")

            val adPlotAndLandBean = gson.fromJson(json, AdPlotAndLandBean::class.java)

            Log.e("test", "listsave data: " + adPlotAndLandBean.toString()+"====")
            adPlotAndLandBean.post_images?.clear()
            adPlotAndLandBean.post_images = AppConstants.imageList
            Log.e("test", "post_images: " +  adPlotAndLandBean.post_images!!.size+"===")


            Log.e("test", "post_images data: " + adPlotAndLandBean.post_images)

            val jsonset = gson.toJson(adPlotAndLandBean)

            storeUserData.setString(Constants.ADDPLOTANDLAND, jsonset)

            ////   AdPostImages.changeScreentoLocation(context)

        } else {
            Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
        }
    }

    fun rotateImage(source: Bitmap, angle: Float): Bitmap? {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(
                source, 0, 0, source.width, source.height,
                matrix, true
        )
    }

    fun decodeImageFromFiles(path: String?, width: Int, height: Int): Bitmap {
        val scaleOptions = BitmapFactory.Options()
        scaleOptions.inJustDecodeBounds = true
        BitmapFactory.decodeFile(path, scaleOptions)
        var scale = 1
        while (scaleOptions.outWidth / scale / 2 >= width
                && scaleOptions.outHeight / scale / 2 >= height
        ) {
            scale *= 2
        }
        // decode with the sample size
        val outOptions = BitmapFactory.Options()
        outOptions.inSampleSize = scale
        val bitmap: Bitmap = BitmapFactory.decodeFile(path, outOptions)
        Utils.showLog(
                UploadActivity1.TAG,
                "===BitmapFactory===width" + bitmap.width + "===height" + bitmap.height + "==="
        )

        return bitmap
    }

    fun getPathFromGooglePhotosUri(uriPhoto: Uri?): String? {
        if (uriPhoto == null) return null
        var input: FileInputStream? = null
        var output: FileOutputStream? = null
        try {
            val pfd: ParcelFileDescriptor? = contentResolver.openFileDescriptor(uriPhoto, "r")
            val fd: FileDescriptor = pfd!!.getFileDescriptor()
            input = FileInputStream(fd)
            val tempFilename: String = getTempFilename(this)
            output = FileOutputStream(tempFilename)
            var read: Int = 0
            val bytes = ByteArray(4096)
            while (input.read(bytes).also({ read = it }) != -1) {
                output.write(bytes, 0, read)
            }
            return tempFilename
        } catch (ignored: IOException) {
            // Nothing we can do
        } finally {
            closeSilently(input)
            closeSilently(output)
        }
        return null
    }

    @Throws(IOException::class)
    private fun getTempFilename(context: Context): String {
        val outputDir = context.cacheDir
        val outputFile = File.createTempFile("image", "tmp", outputDir)
        return outputFile.absolutePath
    }

    fun closeSilently(c: Closeable?) {
        if (c == null) return
        try {
            c.close()
        } catch (t: Throwable) {
            // Do nothing
        }
    }

    private fun setTypeValue(tb1: Boolean, tb2: Boolean, tb3: Boolean, type: String) {

        if (type.equals("listedby", ignoreCase = true)) {
            owner = tb1
            dealer = tb2
            builder = tb3
        }

        if (type.equals("type", ignoreCase = true)) {
            agriculture = tb1
            commercial = tb2
            residential = tb3
        }
    }

    private fun setTypeBottomSelection(
        t1: Int,
        t2: Int,
        t3: Int,
        t4: Int,
        t5: Int,
        t6: Int,
        type: String
    ) {

        if (type.equals("type", ignoreCase = true)) {
            tvagri.setBackgroundResource(t1)
            tvcommercial.setBackgroundResource(t2)
            tvresidential.setBackgroundResource(t3)

            tvagri.setTextColor(resources.getColor(t4))
            tvcommercial.setTextColor(resources.getColor(t5))
            tvresidential.setTextColor(resources.getColor(t6))
        }


        if (type.equals("listedby", ignoreCase = true)) {
            tvowner.setBackgroundResource(t1)
            tvdealer.setBackgroundResource(t2)
            tvbuilder.setBackgroundResource(t3)

            tvowner.setTextColor(resources.getColor(t4))
            tvdealer.setTextColor(resources.getColor(t5))
            tvbuilder.setTextColor(resources.getColor(t6))
        }
    }

    private fun setValue(b1: Boolean, b2: Boolean, type: String) {

        if (type.equals("purpose", ignoreCase = true)) {
            rent = b1
            sell = b2
        }
    }

    private fun setBottomSelection(b1: Int, b2: Int, b3: Int, b4: Int, type: String) {

        if (type.equals("purpose", ignoreCase = true)) {
            tvrent.setBackgroundResource(b1)
            tvsell.setBackgroundResource(b2)

            tvrent.setTextColor(resources.getColor(b3))
            tvsell.setTextColor(resources.getColor(b4))
        }
    }

    private fun setTypeFourValue(
        tb1: Boolean,
        tb2: Boolean,
        tb3: Boolean,
        tb4: Boolean,
        type: String
    ) {

        if (type.equals("facing", ignoreCase = true)) {
            north = tb1
            south = tb2
            east = tb3
            west = tb4
        }


    }

    private fun setTypeFourBottomSelection(
        t1: Int,
        t2: Int,
        t3: Int,
        t4: Int,
        t5: Int,
        t6: Int,
        t7: Int,
        t8: Int,
        type: String
    ) {

        if (type.equals("facing", ignoreCase = true)) {
            tvnorth.setBackgroundResource(t1)
            tvsouth.setBackgroundResource(t2)
            tveast.setBackgroundResource(t3)
            tvwest.setBackgroundResource(t4)

            tvnorth.setTextColor(resources.getColor(t5))
            tvsouth.setTextColor(resources.getColor(t6))
            tveast.setTextColor(resources.getColor(t7))
            tvwest.setTextColor(resources.getColor(t8))
        }

    }

    companion object {

        private val TAG = "PlotLandDetailActivity"
    }
}

