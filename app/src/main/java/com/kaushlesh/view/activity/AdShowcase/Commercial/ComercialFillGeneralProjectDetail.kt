package com.kaushlesh.view.activity.AdShowcase.Commercial

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.kaushlesh.R
import com.kaushlesh.view.activity.AdShowcase.Jwellery.JewelleryIndividualCategory
import kotlinx.android.synthetic.main.activity_comercial_fill_general_project_detail.*


class ComercialFillGeneralProjectDetail : AppCompatActivity() {

    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comercial_fill_general_project_detail)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getText(R.string.txt_general_project_detail)
        btnback.setOnClickListener {
            onBackPressed()
        }

        btn_next.setOnClickListener {
            val intent = Intent(applicationContext, JewelleryIndividualCategory::class.java)
            startActivity(intent)
        }
    }
}