package com.kaushlesh.view.activity.AdPost

import android.content.Intent
import android.icu.number.NumberFormatter
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.kaushlesh.R
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import gun0912.tedimagepicker.builder.TedImagePicker
import java.text.NumberFormat
import java.util.*

class SetPriceActivity : AppCompatActivity(), View.OnClickListener {
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal lateinit var etprice: EditText
    internal lateinit var btnnext: Button
    lateinit var storeUserData: StoreUserData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_set_price)
        storeUserData = StoreUserData(this)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.txt_set_price)

        initBindViews()
    }

    private fun initBindViews() {

        etprice = findViewById(R.id.et_price)
        btnnext = findViewById(R.id.btn_next)

        btnnext.setOnClickListener(this)
        btnback.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_back -> onBackPressed()

            R.id.btn_next -> {
               /* val intent = Intent(applicationContext, SetLocationActivity::class.java)
                startActivity(intent)*/
                if (etprice.text.length > 0) {

                    val price = Utils.GujaratiToEnglish(etprice.text.toString())
                    Log.e(TAG,"===price====" + Utils.GujaratiToEnglish(price))
                    if(price.startsWith("0"))
                    {
                        Utils.showToast(this,getString(R.string.txt_valid_price))
                    }
                    else {
                        val intent = Intent(applicationContext, SetLocationActivity::class.java)
                        //intent.putExtra("price", etprice.text.toString())
                        intent.putExtra("price", price.toString())
                        startActivity(intent)
                    }
                }else
                {
                    etprice.setError(getString(R.string.txt_required_filed))
                }
            }
        }
    }

    companion object {

        private val TAG = "SetPriceActivity"
    }

    override fun onBackPressed() {
        super.onBackPressed()

        if (AppConstants.imageListuri != null && AppConstants.imageListuri.size > 0) {
            Log.e("imageListuri2===", AppConstants.imageListuri.toString())

            TedImagePicker.with(this)
                    .max(12, "Maximum 12 photos allowed")
                    .errorListener { message ->
                        Log.e("ted", "message: $message")
                    }
                    .selectedUri(AppConstants.imageListuri)
                    .startMultiImage { list: List<Uri> ->
                        Log.e("list===", "listsize: ${list.size}")
                        AppConstants.showMultiImage1(list, applicationContext, storeUserData.getString(Constants.CATEGORY_ID).toInt(), storeUserData.getString(Constants.SUBCATEGORYID).toInt())
                    }
        }
    }

}

