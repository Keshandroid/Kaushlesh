package com.kaushlesh.view.fragment.MyAccount.MyAds

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.kaushlesh.Controller.ScreenRedirectionController
import com.kaushlesh.R
import com.kaushlesh.adapter.MyAdsPagerAdapter
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.MainActivity


class MyPostActivity : AppCompatActivity() ,View.OnClickListener{

    val TAG = "MyPostActivity"
    var tabLayout: TabLayout? = null
    var viewPager: ViewPager? = null
    var radioGroup: RadioGroup? = null
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    var isfrom : String = ""

    private lateinit var screenRedirectionController: ScreenRedirectionController
    lateinit var storeUserData: StoreUserData
    internal lateinit var btndelete: ImageView
    internal lateinit var dialog: Dialog
    lateinit var fragment: MyPostAdFragment
    var pos = 0
    lateinit var adapter :MyAdsPagerAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_post)

        fragment= MyPostAdFragment()

        screenRedirectionController = ScreenRedirectionController(this)

        storeUserData= StoreUserData(this)
        storeUserData.setString(Constants.POST_FROM, "activity")
        storeUserData.setString(Constants.FROM_DETAIL, "false")

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        btndelete = toolbar.findViewById(R.id.btn_delete)
        tvtoolbartitle = toolbar.findViewById(R.id.title)


        tvtoolbartitle.text = getString(R.string.my_post)

        if(intent != null)
        {
            isfrom = intent.getStringExtra("from").toString()
        }

        btnback.setOnClickListener(this)
        btndelete.setOnClickListener(this)

        tabLayout = findViewById(R.id.tablaout)
        viewPager = findViewById(R.id.pager)

        tabLayout!!.addTab(tabLayout!!.newTab().setText(getString(R.string.txt_my_ads)))
        tabLayout!!.addTab(tabLayout!!.newTab().setText(getString(R.string.txt_favourite)))
        tabLayout!!.tabGravity = TabLayout.GRAVITY_FILL

        adapter = MyAdsPagerAdapter(
            applicationContext,
            supportFragmentManager,
            tabLayout!!.tabCount
        )
        viewPager!!.adapter = adapter
        //viewPager!!.offscreenPageLimit = 1


        viewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))

        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager!!.currentItem = tab.position
                Utils.showLog(TAG, "current tab==> " + tab.position.toString())
                pos = tab.position
                if (tab.position == 1) {
                    btndelete.visibility = View.GONE
                } else {
                    btndelete.visibility = View.VISIBLE
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

    }

    fun setAdPost() {
        screenRedirectionController.openAdPostTab()
    }

    fun setHomeScreen() {
        screenRedirectionController.openHomeTab()
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_back -> {
                if (isfrom.equals("attention")) {
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    onBackPressed()
                }
            }

            R.id.btn_delete -> {
                openConfirmationDialog()
            }
        }
    }

    private fun openConfirmationDialog() {

        dialog = Dialog(this)
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_delete_ads)
        // Set dialog title
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        //dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_white);
        // set values for custom dialog components - text, image and button
        radioGroup = dialog.findViewById(R.id.radioGroup)

        val cbexpired = dialog.findViewById(R.id.cbexpired) as CheckBox
        val cbrejected = dialog.findViewById(R.id.cbrejected) as CheckBox
        val btnyes = dialog.findViewById(R.id.btn_yes) as TextView
        val btnno = dialog.findViewById(R.id.btn_no) as TextView

        btnyes.setOnClickListener(View.OnClickListener {
            //checkbox
            if(cbexpired.isChecked && cbrejected.isChecked)
            {
                MyPostAdFragment.getInstance()?.callApiToRefreshDeletedPost("2,3")
                dialog.dismiss()
            }
            else {
                if (cbexpired.isChecked || cbrejected.isChecked) {
                    if (cbexpired.isChecked) {
                        MyPostAdFragment.getInstance()?.callApiToRefreshDeletedPost("3")
                    } else if (cbrejected.isChecked) {
                        MyPostAdFragment.getInstance()?.callApiToRefreshDeletedPost("2")
                    }
                    dialog.dismiss()
                } else {
                    Toast.makeText(this, "Please select one option", Toast.LENGTH_SHORT).show()
                }
            }



            //radio buttoon
           /* val selectedId: Int = radioGroup!!.checkedRadioButtonId

            val radioButton = dialog.findViewById<RadioButton>(selectedId)
            Log.e(TAG, "===selected radio Button===$selectedId")
            if (selectedId == -1) {
                Toast.makeText(this, "Please select one option", Toast.LENGTH_SHORT).show()
            } else {

                if(radioButton.getText().toString().equals(resources.getString(R.string.txt_delete_expired_post)))
                {
                    MyPostAdFragment.getInstance()?.callApiToRefreshDeletedPost(1)
                }
                else{
                    MyPostAdFragment.getInstance()?.callApiToRefreshDeletedPost(2)
                }

                dialog.dismiss()
            }*/
        })

        btnno.setOnClickListener(View.OnClickListener { dialog.dismiss() })

        dialog.show()
    }

    override fun onBackPressed() {
        if(isfrom.equals("attention"))
        {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
        else{
            super.onBackPressed()
        }
    }

}