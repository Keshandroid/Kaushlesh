package com.kaushlesh.view.fragment.MyAccount.Communication

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.R
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.view.activity.MainActivity
import com.kaushlesh.view.fragment.MyAccount.PurchasePackage.PurchasePackageFragment
import com.kaushlesh.view.fragment.ProductListFragment
import kotlinx.android.synthetic.main.fragment_purchase_package.*
import org.json.JSONException
import org.json.JSONObject


class ContactUsFragment : Fragment(), View.OnClickListener, ParseControllerListener {
    var cname: String = ""
    var category: String = ""
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    lateinit var txtContactNumber: TextView
    lateinit var contactUsEmail: TextView

    lateinit var getListingController: GetListingController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_contact_us, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getListingController = GetListingController(activity!!, this)


        toolbar = view.findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)
        contactUsEmail = view.findViewById(R.id.contactUsEmail)
        txtContactNumber = view.findViewById(R.id.txtContactNumber)

        if (arguments != null) {
            tvtoolbartitle.text = arguments!!.getString("name").toString()
            AppConstants.printLog(
                ProductListFragment.TAG,
                "==title==$cname" + "==category==$category"
            )
        }
        btnback.setOnClickListener {
            if (activity != null) {
                activity!!.onBackPressed()
            }
        }

        getListingController.getContactUs(2)


        addOnclicked()
    }

    private fun addOnclicked() {


    }

    override fun onClick(v: View?) {
        when (v?.id) {

        }

    }

    companion object {
        val TAG = "ContactUsFragment"
        fun newInstance(): ContactUsFragment {
            return ContactUsFragment()
        }
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (method.equals("contactUs")) {
            if (da.length() > 0) {
                Log.e("get contactUs", "$da==")
                try {

                    if(da.has("result")) {
                        val data = da.getJSONObject("result")
                        val staticEmail = data.getString("staticEmail")
                        val staticNumber = data.getString("staticNumber")

                        txtContactNumber.setText(""+staticNumber)
                        contactUsEmail.setText(""+staticEmail)


                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onFail(msg: String, method: String) {
    }

}