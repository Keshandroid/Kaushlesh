package com.kaushlesh.view.fragment.MyAccount

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.SystemClock
import android.provider.MediaStore
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.*
import androidx.annotation.IdRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.FragmentManager
import com.bumptech.glide.Glide
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.kaushlesh.Controller.EditProfileController
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.R
import com.kaushlesh.api.API
import com.kaushlesh.bean.LocationListBeans
import com.kaushlesh.bean.RegisterPhoneBean
import com.kaushlesh.bean.UserProfileBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.parser.GetRequestParsing
import com.kaushlesh.service.LocalStorage
import com.kaushlesh.utils.*
import com.kaushlesh.view.activity.Login.AddPhoneNumberActivity
import com.kaushlesh.view.activity.MainActivity
import com.kaushlesh.widgets.CustomEditText
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.activity_add_details.*
import kotlinx.android.synthetic.main.activity_business_profile.*
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_edit_profile.etphone
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class EditProfileActivity : AppCompatActivity(),View.OnClickListener ,ParseControllerListener{

    val TAG = "EditProfileActivity"
    private var mFragmentManager: FragmentManager? = null
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView

    internal lateinit var activity: Activity
    lateinit var userId: String
    lateinit var userToken: String
    lateinit var userProfile: String
    lateinit var isActive: String
    lateinit var isOTPVerified: String
    lateinit var device_token: String
    lateinit var device_type: String
    lateinit var registrationType: String
    lateinit var device_id: String

    var fname: String? = "1"
    var lname: String? = "1"
    var email: String? = "1"
    var address_street: String? = "1"
    var address_apt: String? = "1"
    var city: String? = "1"
    var state: String? = "1"
    var zip: String? = "1"
    var model: RegisterPhoneBean = RegisterPhoneBean()
    var userModel: UserProfileBean = UserProfileBean()
    var dataModel = RegisterPhoneBean()
    internal lateinit var dialog: Dialog
    internal lateinit var dialog1: Dialog
    private val REQUEST_CAMERA = 0
    private val GALLERY = 1
    private var mImageBitmap: Bitmap? = null
    private var mCurrentPhotoPath: String? = null

    lateinit var storeusedata: StoreUserData
    private lateinit var editProfileController: EditProfileController
    var isphoneuse : Boolean = true
    var locationid: String? = null
    lateinit var getListingController: GetListingController
    internal var typelist: MutableList<String> = java.util.ArrayList()
    internal var typeidlist: MutableList<String> = java.util.ArrayList()
    internal var locationlist: ArrayList<LocationListBeans.LocationList> = ArrayList()
    internal lateinit var type_spinner_type: ArrayAdapter<String>
    internal lateinit var tv2: TextView

    var whatsappInquiryAllow: Boolean = true
    var sameWhatsappNo: Boolean = false

    private var mLastClickTime: Long = 0

    lateinit var localStorage: LocalStorage


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        tv2 = findViewById(R.id.tv_2)

        getListingController = GetListingController(this, this)
        getListingController.getLocationList()

        editProfileController = EditProfileController(this, this)
        activity = this
        mFragmentManager = supportFragmentManager
        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)
        tvtoolbartitle.text = resources.getString(R.string.txt_edit_profile)
        btnback.setOnClickListener(this)

        btn_save.setOnClickListener(this)
        ivmobile.setOnClickListener(this)
        ll_info.setOnClickListener(this)
        ivedit.setOnClickListener(this)
        etphone.setOnClickListener(this)
        tv_dis_type.setOnClickListener(this)
        ll_info1.setOnClickListener(this)

        device_id = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID)

        storeusedata = StoreUserData(applicationContext)

        userModel.userId = storeusedata.getString(Constants.USER_ID)
        userModel.userProfile = storeusedata.getString(Constants.USER_ID)
        userModel.userToken =  storeusedata.getString(Constants.TOKEN)
        userModel.isOTPVerified = storeusedata.getString(Constants.IS_OTP_VERIFED)
        userModel.isActive =  storeusedata.getString(Constants.IS_ACTIVE)
        userModel.device_token =  storeusedata.getString(Constants.DEVICE_TOKEN)
        userModel.device_type =  storeusedata.getString(Constants.DEVICE_TYPE)
        userModel.registrationType =  storeusedata.getString(Constants.REGISTREATION_TYPE)

        Utils.showLog(TAG,"====reg type==" + userModel.registrationType)

        //for otp verify
        if (userModel.isOTPVerified == "1") {
            ivverify.setImageResource(R.drawable.verify)
        } else {
            ivverify.setImageResource(R.drawable.warning)
        }

        callforDisplayUserData()

        et_about_you.setOnTouchListener(View.OnTouchListener { v, event ->
            if (v.id == R.id.et_about_you) {
                v.parent.requestDisallowInterceptTouchEvent(true)
                when (event.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_UP -> v.parent
                            .requestDisallowInterceptTouchEvent(false)
                }
            }
            false
        })

        et_about_you.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {

                tv2.text = s.length.toString() + "/100"
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })

        radioGrpWpInquiry2.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup, @IdRes checkedId: Int) {
                val radioButton: RadioButton = group.findViewById(checkedId) as RadioButton
                val rbSelected = radioButton.getText().toString();
                if (rbSelected == "Yes") {
                    whatsappInquiryAllow = true
                    llWhatsappInquiryMain2.visibility = View.VISIBLE
                } else {
                    whatsappInquiryAllow = false
                    llWhatsappInquiryMain2.visibility = View.GONE
                }
            }
        })

        radioGrpWhatsappNumber2.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup, @IdRes checkedId: Int) {
                val radioButton: RadioButton = group.findViewById(checkedId) as RadioButton
                val rbSelected = radioButton.getText().toString();
                if (rbSelected == "Yes") {
                    sameWhatsappNo = true
                    llWhatsappNumber2.visibility = View.GONE

                } else {
                    sameWhatsappNo = false
                    llWhatsappNumber2.visibility = View.VISIBLE
                }
            }
        })

        radioGrpMobile.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup, @IdRes checkedId: Int) {
                val radioButton: RadioButton = group.findViewById(checkedId) as RadioButton
                val rbSelected = radioButton.getText().toString();
                if (rbSelected == "Yes") {
                    isphoneuse = true
                } else {
                    isphoneuse = false
                }
            }
        })

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_back -> {
                Utils.showLog(TAG,"==location id===" + tv_dis_type.text.toString() + locationid)
                val regtype =storeusedata.getString(Constants.REGISTREATION_TYPE)
                if(regtype != null && regtype.length > 0)
                {
                    if(regtype.equals("phone"))
                    {
                        if(locationid ==null || locationid.toString().length == 0) {
                            Utils.showToast(this, getString(R.string.txt_select_district_hint))
                        }
                        else{
                            if(userModel.phone != null && userModel.phone.toString().length >0 && userModel.locationId != null && userModel.locationId.toString().length >0)
                            {
                                //callEditData()
                                backtoMainscreen()
                            }
                            else{
                                openConfirmationDialog()
                            }
                        }
                       /* else if (et_name.text.length == 0 || et_email.text.length == 0) {
                            openNameEmailDialog()
                        } else {
                            backtoMainscreen()
                        }*/
                    }
                    else{
                        backtoMainscreen()
                      /*  if(etphone.text.length == 0 || locationid ==null || locationid.toString().length == 0) {
                            openMobileWarningDialog()
                        }
                        else if (et_name.text.length == 0 || et_email.text.length == 0) {
                            openNameEmailDialog()
                        } else {
                            backtoMainscreen()
                        }*/
                    }
                }
                else{
                    backtoMainscreen()
                }
               /* if(locationid ==null || locationid.toString().length == 0) {
                    Utils.showToast(this, getString(R.string.txt_select_district_hint))
                }
                else if(etphone.text.length == 0 || locationid ==null || locationid.toString().length == 0) {
                    openMobileWarningDialog()
                }
                else if (et_name.text.length == 0 || et_email.text.length == 0) {
                    openNameEmailDialog()
                } else {
                    backtoMainscreen()
                }*/
            }
            R.id.tv_dis_type ->
            {
                spinner_dis_type.visibility = View.VISIBLE
                spinner_dis_type.performClick()
                tv_dis_type.visibility = View.GONE
                spinner_dis_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                            adapterView: AdapterView<*>,
                            view: View,
                            position: Int,
                            l: Long
                    ) {
                        AppConstants.printLog(TAG, "onItemSelected: ===" + adapterView.selectedItem)
                        AppConstants.printLog(TAG, "onItemSelected: ===" + adapterView.selectedItem)
                        AppConstants.printLog(TAG, "onItemSelected: ===" + typeidlist.get(position))
                        locationid = typeidlist.get(position)
                        rl_dis_type.setBackgroundResource(R.drawable.bg_edittext_black)
                    }

                    override fun onNothingSelected(adapterView: AdapterView<*>) {

                    }
                }

            }
            R.id.btn_save -> {

                //prevent double click on button
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    return;
                }

                mLastClickTime = SystemClock.elapsedRealtime();

                if (etphone.text.length != 10) {
                    etphone.setError(getString(R.string.txt_min_ten_digit))
                    Utils.showToast(this, getString(R.string.txt_min_ten_digit))
                } else if(locationid ==null || locationid.toString().length == 0) {
                    Utils.showToast(this, getString(R.string.txt_select_district_hint))
                } else {

                    if(userModel.phone != null && userModel.phone.toString().length >0 && userModel.locationId != null && userModel.locationId.toString().length >0)
                    {
                        if (et_name.text.length == 0 || et_email.text.length == 0) {
                            openNameEmailDialog()
                        } else {


                            if(et_email.text.toString().trim().length > 0){
                                if(!Utils.isValidEmail(et_email.text.toString().trim())){
                                    Utils.showToast(this, "Email is not valid")
                                }else{
                                    callEditData()
                                }
                            }else{
                                callEditData()
                            }

                            /*if(!Utils.isValidEmail(et_email.text.toString().trim())){
                                Utils.showToast(this, "Email is not valid")
                                return
                            }

                            callEditData()*/
                        }
                    }
                    else{
                        openConfirmationDialog()
                    }
                }
            }
            R.id.ivmobile -> {
                dataModel.userid = userModel.userId
                dataModel.userToken = userModel.userToken

                val intent = Intent(applicationContext, AddPhoneNumberActivity::class.java)
                val bundle = Bundle()
                bundle.putSerializable("editnumber", dataModel)
                intent.putExtras(bundle)
                startActivity(intent)
            }

            R.id.etphone -> {
                if (etphone.text.toString().length > 0) {

                    //openInfoDialog()
                }
            }
            R.id.ll_info -> {
                openInfoDialog()
            }

            R.id.ll_info1 -> {
                openInfoDialog()
            }

            R.id.ivedit -> {
                requestMultiplePermissions()
            }

        }
    }


    private fun openConfirmationDialog() {

        dialog = Dialog(this)
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_logout)
        // Set dialog title
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set values for custom dialog components - text, image and button
        val tv_title = dialog.findViewById<TextView>(R.id.tv_title)
        val btnno = dialog.findViewById<Button>(R.id.btn_no)
        val btnyes = dialog.findViewById<Button>(R.id.btn_yes)

       // tv_title.text = resources.getString(R.string.txt_edit_profile_confirmation)
        tv_title.text = resources.getString(R.string.txt_edit_profile_confirmation_final)
        btnno.text = resources.getString(R.string.txt_review)
        btnyes.text = resources.getString(R.string.txt_save)
        btnno.setTextColor(resources.getColor(R.color.black))
        btnyes.setTextColor(resources.getColor(R.color.orange))

        btnno.setOnClickListener {
            dialog.dismiss()
        }

        btnyes.setOnClickListener {


            if(et_email.text.toString().trim().length > 0){
                if(!Utils.isValidEmail(et_email.text.toString().trim())){
                    Utils.showToast(this, "Email is not valid")
                }else{
                    callEditData()
                }
            }else{
                callEditData()
            }


            //old
            /*if(!Utils.isValidEmail(et_email.text.toString().trim())){
                Utils.showToast(this, "Email is not valid")
            }else{
                callEditData()
            }*/

            dialog.dismiss()
        }

        dialog.show()
    }

    private fun openMobileWarningDialog() {
        dialog = Dialog(this)
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_logout)
        // Set dialog title
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        //dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_white);
        // set values for custom dialog components - text, image and button

        val tvtitle = dialog.findViewById(R.id.tv_title) as TextView
        tvtitle.setText(resources.getText(R.string.txt_mobile_warning))
        val btnyes = dialog.findViewById(R.id.btn_yes) as TextView
        val btnno = dialog.findViewById(R.id.btn_no) as TextView

        btnyes.text =resources.getString(R.string.txt_ok)
        btnno.isAllCaps = false
        btnno.text = resources.getString(R.string.txt_register_mobile)

        btnno.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
        })

        btnyes.setOnClickListener(View.OnClickListener {
            backtoMainscreen()
            dialog.dismiss()
        })

        dialog.show()

    }

    private fun backtoMainscreen() {

        if(intent != null)
        {
            if(intent.getStringExtra("from").equals("profile")) {
                onBackPressed()
            }
            else {
                val intent1 = Intent(applicationContext, MainActivity::class.java)
                startActivity(intent1)
                finish()
            }
        }
    }

    private fun openNameEmailDialog() {

        dialog1 = Dialog(this)
        // Include dialog.xml file
        dialog1.setContentView(R.layout.dialog_logout)
        // Set dialog title
        dialog1.setTitle("")
        dialog1.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        //dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_white);
        // set values for custom dialog components - text, image and button

        val tvtitle = dialog1.findViewById(R.id.tv_title) as TextView
        tvtitle.setText(resources.getText(R.string.txt_name_email_warning))
        val btnyes = dialog1.findViewById(R.id.btn_yes) as TextView
        val btnno = dialog1.findViewById(R.id.btn_no) as TextView
        btnyes.text = resources.getString(R.string.txt_ok)
        btnno.text = resources.getString(R.string.txt_not_now)

        btnno.setOnClickListener(View.OnClickListener {
            if(userModel.phone != null && userModel.phone.toString().length >0 && userModel.locationId != null && userModel.locationId.toString().length >0)
            {

                if(et_email.text.toString().trim().length > 0){
                    if(!Utils.isValidEmail(et_email.text.toString().trim())){
                        Utils.showToast(this, "Email is not valid")
                    }else{
                        callEditData()
                    }
                }else{
                    callEditData()
                }

                //old
                /*if(!Utils.isValidEmail(et_email.text.toString().trim())){
                    Utils.showToast(this, "Email is not valid")
                }else{
                    callEditData()
                }*/


            } else{
                dialog1.dismiss()
                opendialog()
            }
            //callEditData()
            dialog1.dismiss()
        })

        btnyes.setOnClickListener(View.OnClickListener { dialog1.dismiss() })

        dialog1.show()
    }

    private fun opendialog() {
        openConfirmationDialog()
    }

    private fun callforDisplayUserData() {
        if (userModel.userId != null) {
            Log.e("TAG", "callforDisplayUserData: " + userModel.userId)
            val p = GetRequestParsing()
            val url = API.USER_PROFILE + "?userId=" + userModel.userId + "&userToken=" + userModel.userToken + "&profileId=" + userModel.userProfile
            p.callApi(this.activity, url, "ViewProfile", this)
            Log.e("TAG", "get profile: " + url)
        }
    }

    private fun callEditData() {

        //validation for whatsapp inquiry
        if(whatsappInquiryAllow) {
            if (!sameWhatsappNo) {
                if (edtWhatsappNo2.text.toString() == "") {
                    Utils.showToast(this, "Please enter whatsapp no")
                    return
                }

                if (edtWhatsappNo2.text.toString().length != 10) {
                    Utils.showToast(this, "Please enter valid whatsapp no")
                    return
                }

            }
        }

        localStorage = LocalStorage(applicationContext)


        val registerData = RegisterData()

        registerData.name =  et_name.text.toString()
        registerData.email = et_email.text.toString()
        registerData.phone = etphone.text.toString()
        registerData.state = et_state.text.toString()
        registerData.deviceid = device_id.toString()
        registerData.devicetoken = localStorage.getToken().toString()
        registerData.image = civ_profile.drawable
        registerData.aboutyou = et_about_you.text.toString()
        registerData.locationid = locationid.toString()
        if(isphoneuse) {
            registerData.isPhone = 1
        } else{
            registerData.isPhone = 0
        }

        //whatsapp fields start
        if(whatsappInquiryAllow){
            registerData.whatsapp_inquiry_allow = 1
        }else{
            registerData.whatsapp_inquiry_allow = 0
        }

        if(sameWhatsappNo){
            registerData.same_whatsapp_no = 1
            registerData.whatsapp_no = etphone.text.toString()

        }else{
            registerData.same_whatsapp_no = 0
            registerData.whatsapp_no = edtWhatsappNo2.text.toString()

        }

        //whatsapp fields end



        editProfileController.editProfille(registerData, userModel)

    }



    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (method.equals("ViewProfile")) {

            if (da.length() > 0) {

                Log.e("get profile Result-", "$da==")
                try {

                    userModel.UserProfileBean(da)

                    setProfileData(userModel)

                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            } else {
                AppConstants.printToast(activity, message)
            }
        }
        if (method.equals("EditProfile")) {
            if (da.length() > 0) {
                Log.e("Edit profile Result-", "$da==")
                Log.e("Edit_profile", "$da==")

                AppConstants.printToast(activity, message)
                try {

                    storeusedata.setString(Constants.IS_LOGGED_IN, "true")

                    storeusedata.setString(Constants.NAME,da.getString("name"))
                    storeusedata.setString(Constants.EMAIL,da.getString("email"))
                    storeusedata.setString(Constants.CONTACT_NO,da.getString("ConatctNo"))

                    backtoMainscreen()

                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            } else {
                AppConstants.printToast(activity, message)
            }
        }
        if (method.equals("GetLocation")) {
            locationlist.clear()
            typelist.clear()

            val dataModel = LocationListBeans()
            dataModel.LocationListBeans(da)
            locationlist = dataModel.locationList
            Utils.showLog(TAG, "==location list size==" + locationlist.size)

            for (i in locationlist.indices) {
                val name = locationlist.get(i).locName
                val id = locationlist.get(i).locId
                typelist.add(name.toString())
                typeidlist.add(id.toString())
            }

            type_spinner_type = ArrayAdapter(
                    applicationContext,
                    android.R.layout.simple_spinner_dropdown_item,
                    typelist
            )
            // Creating adapter for spinner
            val dataAdapter1 =
                    ArrayAdapter(applicationContext, android.R.layout.simple_spinner_item, typelist)

            // Drop down layout style - list view with radio button
            dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

            // attaching data adapter to spinner
            spinner_dis_type.adapter = dataAdapter1


            tv_dis_type.visibility = View.VISIBLE
            tv_dis_type.text = resources.getText(R.string.txt_select_district_hint)
            spinner_dis_type.visibility = View.GONE
            rl_dis_type.setBackgroundResource(R.drawable.bg_edittext)
        }
    }

    private fun setProfileData(userModel: UserProfileBean) {
        if(userModel.fname != null && userModel.fname.toString().length >0) {
            storeusedata.setString(Constants.USER_NAME, userModel.fname.toString())
            et_name.setText(userModel.fname)
            et_name.setBackgroundResource(R.drawable.bg_edittext_black)
            et_name.setPadding(20, 0, 0, 0)
        }
        else{
            setBackGroundGrey(et_name)
            storeusedata.setString(Constants.USER_NAME, "GL User")
        }

        if(userModel.phone != null && userModel.phone.toString().length >0) {

            //storeusedata.setString(Constants.IS_LOGGED_IN, "true")
            etphone.setText(userModel.phone)
            etphone.setBackgroundResource(R.drawable.bg_edittext_black)
            etphone.setPadding(20, 0, 0, 0)
            etphone.isFocusable = false
            //etphone.isClickable = true
        }
        else{
            setBackGroundGrey(etphone)
            etphone.isEnabled = true
        }

        if(userModel.registrationType != null && userModel.registrationType.toString().length > 0)
        {
            if(!userModel.registrationType.equals("phone"))
            {
                et_email.isFocusable = false
                iv_edit.visibility = View.GONE
            }
        }

        if(userModel.email != null && userModel.email.toString().length >0) {
            et_email.setText(userModel.email)
            et_email.setBackgroundResource(R.drawable.bg_edittext_black)
            et_email.setPadding(20, 0, 0, 0)
        }
        else{
            setBackGroundGrey(et_email)
        }

        if(userModel.aboutyou != null && userModel.aboutyou.toString().length >0) {
            et_about_you.setText(userModel.aboutyou)
            et_about_you.setBackgroundResource(R.drawable.bg_edittext_black)
            et_about_you.setPadding(20, 0, 0, 0)
        }
        else{
            setBackGroundGrey(et_about_you)
        }

        if(userModel.userProfile != null && userModel.userProfile.toString().length >0)
        {
            storeusedata.setString(Constants.PROFILE, userModel.userProfile.toString())
            Glide.with(applicationContext)
                .load(userModel.userProfile).placeholder(R.drawable.ic_user_pic)
                .into(civ_profile)
        }

        if(userModel.isphone!!.toInt() == 1)
        {
            radioMobileYes.isChecked = true
            isphoneuse = true
        }
        else{
            radioMobileNo.isChecked = true
            isphoneuse = false
        }

        if(userModel.locationId != null && userModel.locationId.toString().length >0)
        //if(userModel.locationName != null && userModel.locationName.toString().length >0)
        {
            locationid = userModel.locationId
            tv_dis_type.isEnabled = false
            rl_dis_type.setBackgroundResource(R.drawable.bg_edittext_black)
            ib_dis_type.visibility = View.GONE

            try {
                //spinner_dis_type.setSelection(type_spinner_type.getPosition(userModel.locationName))
                tv_dis_type.text = userModel.locationName
            }
            catch (e: UninitializedPropertyAccessException)
            {
                e.printStackTrace()
            }
        }
        if(userModel.phone != null && userModel.phone.toString().length >0 && userModel.locationId != null && userModel.locationId.toString().length >0)
        {
            storeusedata.setString(Constants.IS_LOGGED_IN, "true")
            ll_info.visibility = View.VISIBLE
            ll_info1.visibility = View.VISIBLE
        }


        //whatsapp data
        if(userModel.whatsapp_inquiry_allow.toString() == "1") {
            radioYes2.isChecked = true
            whatsappInquiryAllow = true
            llWhatsappInquiryMain2.visibility = View.VISIBLE


            //same whatsapp no
            if (userModel.same_whatsapp_no.toString() == "1") {
                radioNumberYes2.isChecked = true
                sameWhatsappNo = true
                llWhatsappNumber2.visibility = View.GONE

                //whatsap no
                if (userModel.whatsapp_no != null) {
                    edtWhatsappNo2.setText(userModel.whatsapp_no)
                }

            } else {
                radioNumberNo2.isChecked = true
                sameWhatsappNo = false
                llWhatsappNumber2.visibility = View.VISIBLE


                //whatsap no
                if (userModel.whatsapp_no != null) {
                    edtWhatsappNo2.setText(userModel.whatsapp_no)
                }

            }
        }else{
            radioNo2.isChecked = true
            whatsappInquiryAllow = false
            llWhatsappInquiryMain2.visibility = View.GONE
        }

    }

    private fun setBackGroundGrey(editText: CustomEditText) {
        editText.setText("")
        editText.setBackgroundResource(R.drawable.bg_edittext)
        editText.setPadding(20, 0, 0, 0)
    }


    override fun onFail(msg: String, method: String) {
        Utils.showLog(TAG, msg)
        Utils.showToast(this, msg)
    }

    private fun openInfoDialog() {

        dialog = Dialog(this)
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_edit_profile_info)
        // Set dialog title
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.getWindow()!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        // set values for custom dialog components - text, image and button
        val ivclose = dialog.findViewById<ImageView>(R.id.ivclose)
        val txtContactUsDialog = dialog.findViewById<TextView>(R.id.txtContactUsDialog)
        ivclose.setOnClickListener {
            dialog.dismiss()
        }

        txtContactUsDialog.setText("Call : "+Identity.getContactUsNumber(this))

        dialog.show()
    }

    private fun requestMultiplePermissions() {
        Dexter.withActivity(this)
            .withPermissions(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    // check if all permissions are granted
                    if (report.areAllPermissionsGranted()) {
                        Utils.showLog(TAG, "All permissions are granted")

                        showPictureDialog()
                    }

                    // check for permanent denial of any permission
                    if (report.isAnyPermissionPermanentlyDenied) {
                        // show alert dialog navigating to Settings
                        //openSettingsDialog();
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                        permissions: List<PermissionRequest>,
                        token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            }).withErrorListener {
                Log.i(TAG, "Some Error!") }
            .onSameThread()
            .check()
    }

    private fun showPictureDialog() {
        val pictureDialog =
            AlertDialog.Builder(this)
        pictureDialog.setTitle(getString(R.string.txt_select_action))
        val pictureDialogItems = arrayOf(
                getString(R.string.txt_image_from_gellary),
                getString(R.string.txt_imge_from_camera),
                getString(R.string.txt_remove_img)
        )
        pictureDialog.setItems(
                pictureDialogItems
        ) { dialog, which ->
            when (which) {
                0 -> choosePhotoFromGallary()
                1 -> takePhotoFromCamera()
                2 -> civ_profile.setImageDrawable(null);
            }
        }
        pictureDialog.show()
    }

    fun choosePhotoFromGallary() {
        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(galleryIntent, GALLERY)
    }

    private fun takePhotoFromCamera() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                Log.i(TAG, "IOException")
            }
            if (photoFile != null) {
                /*cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(cameraIntent, REQUEST_CAMERA);*/
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile))
                } else {
                    val file =
                        File(Uri.fromFile(photoFile).path)
                    val photoUri = FileProvider.getUriForFile(
                            applicationContext, getPackageName().toString() + ".provider",
                            file
                    )
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                }
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(intent, REQUEST_CAMERA)
                }
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File? {
        val timeStamp =
            SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir: File = applicationContext.getExternalFilesDir(
                Environment.DIRECTORY_PICTURES
        )!!
        val image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        )
        mCurrentPhotoPath = "file:" + image.absolutePath
        return image
    }

    fun getImageUri(inContext: Context, inImage: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val path = MediaStore.Images.Media.insertImage(
                inContext.contentResolver,
                inImage,
                "" + System.currentTimeMillis(),
                null
        )
        return Uri.parse(path)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == GALLERY) {
            if (data != null) {
                val selectedImage = data.data
                startCropImageActivity(selectedImage!!)

            }
        } else if (requestCode == REQUEST_CAMERA) {

            /* if(data != null){
                onCaptureImageResult(data);
            }*/
            //Bitmap photo = (Bitmap) data.getExtras().get("data");
            // civ_profile.setImageBitmap(photo);
            try {
                mImageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.parse(mCurrentPhotoPath))

            } catch (e: IOException) {
                e.printStackTrace()
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }

            if (mImageBitmap != null) {
                startCropImageActivity(getImageUri(applicationContext, mImageBitmap!!))
            }

        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (data != null) {
                val result: CropImage.ActivityResult = CropImage.getActivityResult(data)
                if (resultCode == RESULT_OK) {

                    BitmapOptimizer.optimizeFile(
                            result.getUri().getPath(),
                            200000L,
                            object : BitmapOptimizer.CompleteListener {
                                override fun onComplete(
                                        bitmap: Bitmap?,
                                        encodedImage: ByteArray?
                                ) {
                                    civ_profile.setImageBitmap(bitmap)
                                    civ_profile.setVisibility(View.VISIBLE)
                                    Glide.with(civ_profile.getContext())
                                            .load(result.getUri())
                                            .into(civ_profile)

                                }

                                override fun onFail(message: String?) {
                                    Log.d(TAG, "onFail: == $message")
                                }

                            })
                } else if(resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    //Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private fun startCropImageActivity(imageUri: Uri) {
        CropImage.activity(imageUri)
            .setGuidelines(CropImageView.Guidelines.ON)
            .setMultiTouchEnabled(true)
            .setOutputCompressQuality(50)
            .start(this)
    }

    override fun onBackPressed() {
        if(userModel.phone != null && userModel.phone.toString().length >0 && userModel.locationId != null && userModel.locationId.toString().length >0)
        {
            storeusedata.setString(Constants.IS_LOGGED_IN, "true")
        }
        else{
            storeusedata.setString(Constants.IS_LOGGED_IN, "false")
        }
        super.onBackPressed()
    }
    /*override fun onBackPressed() {
       // super.onBackPressed()
        if(etphone.text.length == 0)
        {
            openMobileWarningDialog()
        }
        else{
            backtoMainscreen()
        }
    }*/
}