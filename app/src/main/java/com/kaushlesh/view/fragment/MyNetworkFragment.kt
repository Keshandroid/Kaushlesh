package com.kaushlesh.view.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.kaushlesh.R
import com.kaushlesh.adapter.FollowFollowingPagerAdapter

class MyNetworkFragment : Fragment() ,View.OnClickListener {

    var tabLayout: TabLayout? = null
    var viewPager: ViewPager? = null

    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_network, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar = view.findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        btnback.visibility = View.GONE
        tvtoolbartitle.text = getString(R.string.txt_my_network)

        btnback.setOnClickListener(this)
        tabLayout = view.findViewById(R.id.tablaout)
        viewPager = view.findViewById(R.id.pager)

        tabLayout!!.addTab(tabLayout!!.newTab().setText(getString(R.string.txt_followers)))
        tabLayout!!.addTab(tabLayout!!.newTab().setText(getString(R.string.txt_followings)))
        tabLayout!!.tabGravity = TabLayout.GRAVITY_FILL

        val adapter = FollowFollowingPagerAdapter(context!!, childFragmentManager, tabLayout!!.tabCount)
        viewPager!!.adapter = adapter

        viewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))

        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager!!.currentItem = tab.position
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {

            }
            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

    }
    companion object {
        val TAG = "MyNetworkFragment"

        fun newInstance(): MyNetworkFragment {
            return MyNetworkFragment()
        }
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_back -> {
                activity?.onBackPressed()
            }
        }
    }
}