package com.kaushlesh.view.fragment.MyAccount.PurchasePackage

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.kaushlesh.R
import kotlinx.android.synthetic.main.activity_choose_package.*

class ChoosePackageActivity : AppCompatActivity(), View.OnClickListener {
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_package)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.txt_choose_package)
        btnback.setOnClickListener(this)
        ll_post_package.setOnClickListener(this)
        ll_brand_showcase_package.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_back -> onBackPressed()

            R.id.ll_post_package ->
            {
                val intent = Intent(this, AdPostPackagesActivity::class.java)
                startActivity(intent)
            }

            R.id.ll_brand_showcase_package ->
            {
                val intent = Intent(this, BrandShowcasePackagesActivity::class.java)
                startActivity(intent)
            }

        }
    }

}