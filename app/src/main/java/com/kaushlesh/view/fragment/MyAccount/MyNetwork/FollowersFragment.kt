package com.kaushlesh.view.fragment.MyAccount.MyNetwork

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.GsonBuilder
import com.kaushlesh.BuildConfig
import com.kaushlesh.Controller.FollowUnfollowController
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.R
import com.kaushlesh.adapter.FollowerAdapter
import com.kaushlesh.bean.BusinessUserProfileBean
import com.kaushlesh.bean.FollowFollowingBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.Business.BusinessProfileActivity
import com.kaushlesh.view.activity.Login.LoginMainActivity
import com.kaushlesh.widgets.CustomButton
import com.kaushlesh.widgets.CustomTextView
import kotlinx.android.synthetic.main.fragment_followers.*
import kotlinx.android.synthetic.main.fragment_followers.tv_no_follow
import kotlinx.android.synthetic.main.fragment_following.*
import org.json.JSONObject
import java.lang.Exception

class FollowersFragment : Fragment(),FollowerAdapter.ItemClickListener,ParseControllerListener {

    val TAG = "FollowersFragment"
    private lateinit var getListingController: GetListingController
    val list = ArrayList<FollowFollowingBean.FFlist>()
    lateinit var storeusedata: StoreUserData

    lateinit var storeUseData: StoreUserData
    var userId: String = ""

    lateinit var btn_refer: CustomButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_followers, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_refer = view.findViewById(R.id.btn_refer)

        rlBusinessProfileFollower?.setOnClickListener{
            val intent = Intent(context, BusinessProfileActivity::class.java)
            startActivity(intent)
        }

        rlAddBusinessProfile?.setOnClickListener{
            val intent = Intent(context, BusinessProfileActivity::class.java)
            startActivity(intent)
        }

        storeUseData = StoreUserData(context!!)
        userId = storeUseData.getString(Constants.USER_ID)

        llShareMyProfileFollowers?.setOnClickListener{
            Utils.shareMyProfileLink("profile",userId, context!!);
        }

        btn_refer.setOnClickListener{
            try {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.setType("text/plain")
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Gujarat Living")
                var shareMessage = "*Gujarat Living* \nGujarat's largest market place for Buy and Sell all type of products\n\n"
                shareMessage = """
                    ${shareMessage}https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}
                    """.trimIndent()
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                startActivity(Intent.createChooser(shareIntent, "choose one"))
            } catch (e: Exception) {
                //e.toString();
            }
        }

        bindRecyclerView()
    }

    private fun bindRecyclerView() {

        val recyclerLayoutManager = LinearLayoutManager(context)
        rv_follow.layoutManager = recyclerLayoutManager
        rv_follow.itemAnimator = DefaultItemAnimator()
        val adapter = FollowerAdapter(list, context!!,1)
        adapter.setClicklistner(this)
        rv_follow.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun itemclick(bean: FollowFollowingBean.FFlist) {
        val intent = Intent(activity, FollowerProfileDetailActivity::class.java)
        Utils.openFollowingUserDetails(context!!,intent,bean.userId.toString())
    }

    override fun itemclickaddToFollowing(bean: FollowFollowingBean.FFlist) {
        if(bean.isFollowing == 1)
        {
            Utils.showToast(activity,"You are already following this user.")
        }
        else {
            if (AppConstants.checkUserIsLoggedin(activity!!)) {

                val c = FollowUnfollowController(activity!!, bean.userId.toString(), this)
                c.addToFollow()

            } else {
                val intent = Intent(context, LoginMainActivity::class.java)
                AppConstants.alertLogin(activity!!, getString(R.string.txt_login_to_follow), intent)
            }
        }
    }

    override fun onResume() {
        Utils.showLog(TAG,"==on resume===")
        list.clear()
        getFollowersListCall()
        getUserBusinessProfile() // temp comment
        super.onResume()
    }

    private fun getUserBusinessProfile(){
        storeusedata = StoreUserData(context!!)

        val businessProfileId = storeusedata.getString(Constants.BUSINESS_USER_ID)

        getListingController = GetListingController(activity!!, this)
        getListingController.getUserBusinessProfile(businessProfileId)
    }

    private fun getFollowersListCall() {
        getListingController = GetListingController(activity!!, this)
        getListingController.getFollowerList()
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (method.equals("GetFollowerList")) {
            list.clear()
            val dataModel = FollowFollowingBean()
            dataModel.FollowFollowingBean(da)
            list.addAll(dataModel.list)
            Utils.dismissProgress()
            checkResponse()
        }
        if(method.equals("addFollow"))
        {
            Utils.showToast(activity, message)
            list.clear()
            getFollowersListCall()
        }

        if(method.equals("getBusinessProfile")){

            val userModel = BusinessUserProfileBean()
            userModel.businessUserProfileBean(da)

            Log.d("getBusinessProfile", "response : "+GsonBuilder().setPrettyPrinting().create().toJson(userModel))

            setBusinessData(userModel)


        }

    }

    private fun setBusinessData(userModel: BusinessUserProfileBean) {
        Log.d("BusinessUser","success...")

        rlBusinessProfileFollower?.visibility = View.VISIBLE
        rlAddBusinessProfile?.visibility  = View.GONE



        if(userModel.is_verified!=null){

            if(userModel.is_verified == "0"){
                tv_status_verify_follow?.setText("Pending")
                imgVerifyStatus?.setImageResource(R.drawable.img_pending)
            }else if(userModel.is_verified == "1"){
                tv_status_verify_follow?.setText("Verified")
                imgVerifyStatus?.setImageResource(R.drawable.verify)
            }else if(userModel.is_verified == "2"){
                tv_status_verify_follow?.setText("Rejected")
                imgVerifyStatus?.setImageResource(R.drawable.img_rejected)
            }

        }

        if(userModel.profile_counter!=null){
            profile_counter_txt?.setText(userModel.profile_counter.toString())
        }

        if(userModel.followersCount!=null){
            txtMyFollowers?.setText(userModel.followersCount.toString())
        }


    }

    override fun onFail(msg: String, method: String) {
        Log.d("BusinessUser","Failed...")

       /* try {
            if(method.equals("addFollow")) {
                Utils.showToast(activity, msg)
            } else {
                rv_follow.visibility = View.GONE
                tv_no_follow.visibility = View.VISIBLE
            }
        }catch (e: Exception){
            e.printStackTrace()
        }
*/

        if(method.equals("getBusinessProfile")) {
            if(msg == "Bussiness Profile not Found"){
                rlBusinessProfileFollower?.visibility  = View.GONE
                rlAddBusinessProfile?.visibility = View.VISIBLE
            }
        }

        if (method.equals("GetFollowerList")) {
            try {
                rv_follow.visibility = View.GONE
                tv_no_follow.visibility = View.VISIBLE
            }catch (e: Exception){

            }
        }

        if(method.equals("addFollow")) {
            Utils.showToast(activity, msg)
        }

    }

    private fun checkResponse() {
        if(list.size > 0) {
            tv_no_follow.visibility = View.GONE
            rv_follow.visibility = View.VISIBLE
            bindRecyclerView()
        } else{
            tv_no_follow.visibility = View.VISIBLE
            rv_follow.visibility = View.GONE
        }
    }

   /* override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            // Refresh your fragment here
            fragmentManager!!.beginTransaction().detach(this).attach(this).commit()
        }
    }*/
}