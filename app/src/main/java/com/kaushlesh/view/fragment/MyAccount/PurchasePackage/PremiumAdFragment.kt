package com.kaushlesh.view.fragment.MyAccount.PurchasePackage

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.GsonBuilder
import com.kaushlesh.R
import com.kaushlesh.adapter.AdPostPackageAdapter
import com.kaushlesh.bean.GetCategoryPackagesBean
import com.kaushlesh.utils.Utils
import kotlinx.android.synthetic.main.fragment_extraad.*


class PremiumAdFragment : Fragment(), AdPostPackageAdapter.ItemClickListener  {

    internal var premiumpkglist: ArrayList<GetCategoryPackagesBean.Packages> = ArrayList()
    var mCallback: OnHeadlineSelectedListener? = null
    var templist  : ArrayList<GetCategoryPackagesBean.Packages> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_extraad, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //bindRecyclerviewFuel()
        instance = this
        premiumpkglist = (activity as AdPostPackagesActivity).getDataPremiumaAd()
        Utils.showLog(TAG,"==premium addd===" + premiumpkglist.size)

        setupdata(premiumpkglist)

    }

    private fun setupdata(premiumpkglist: java.util.ArrayList<GetCategoryPackagesBean.Packages>) {
        if(premiumpkglist.size >0)
        {

            premiumpkglist.sortByDescending{ it.noofAd!!.toInt()}

            Utils.showLog(ExtraAdFragment.TAG, "==list duration==" + GsonBuilder().setPrettyPrinting().create().toJson(premiumpkglist))

            templist = premiumpkglist

            templist.sortByDescending{ it.duration!!.toInt()}

            Utils.showLog(ExtraAdFragment.TAG, "==list ad==" + GsonBuilder().setPrettyPrinting().create().toJson(templist))

            templist.reverse()

            //extraadlist.sortedWith(compareBy({ it.duration!!.toInt() }, { it.noofAd?.toInt() }))

            Utils.showLog(ExtraAdFragment.TAG, "==list ad==" + GsonBuilder().setPrettyPrinting().create().toJson(templist))

            bindRecyclerviewFuel(premiumpkglist)
        }
    }

    private fun bindRecyclerviewFuel(premiumpkglist: ArrayList<GetCategoryPackagesBean.Packages>) {
        val recyclerLayoutManager = GridLayoutManager(context, 2)
        rv_extra.layoutManager = recyclerLayoutManager
        rv_extra.setHasFixedSize(true);
        val adapter = AdPostPackageAdapter(context!!,"premiumAd", premiumpkglist)
        rv_extra.adapter = adapter
        adapter.setClicklistner(this)
        adapter.notifyDataSetChanged()
    }
    companion object {
        val TAG = "PremiumAdFragment"
        fun newInstance(): PremiumAdFragment {
            return PremiumAdFragment()
        }

        private var instance: PremiumAdFragment? = null

        @Synchronized
        fun getInstance(): PremiumAdFragment? {
            return instance
        }
    }

    override fun itemclickProduct(bean: GetCategoryPackagesBean.Packages, type: String) {

        Utils.showLog(TAG,"===type===" + type + "id===" + bean.packageid + "==price==" + bean.price)

        mCallback?.onArticleSelected(type,bean)

    }

    interface OnHeadlineSelectedListener {
        fun onArticleSelected(
            position: String,
            bean: GetCategoryPackagesBean.Packages
        )
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            mCallback = context as OnHeadlineSelectedListener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString() + " must implement FragmentToActivity")
        }

    }

    fun refershData(premiumpkglist: java.util.ArrayList<GetCategoryPackagesBean.Packages>) {

        setupdata(premiumpkglist)
    }

}