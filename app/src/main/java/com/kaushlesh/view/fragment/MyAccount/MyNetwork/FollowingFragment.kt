package com.kaushlesh.view.fragment.MyAccount.MyNetwork

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.GsonBuilder
import com.kaushlesh.BuildConfig
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.R
import com.kaushlesh.adapter.FollowingAdapter
import com.kaushlesh.bean.BusinessUserProfileBean
import com.kaushlesh.bean.FollowFollowingBean
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.Business.BusinessProfileActivity
import com.kaushlesh.widgets.CustomButton
import com.kaushlesh.widgets.CustomTextView
import kotlinx.android.synthetic.main.fragment_following.*
import org.json.JSONObject


class FollowingFragment : Fragment() ,ParseControllerListener,FollowingAdapter.ItemClickListener{


    val TAG = "FollowingFragment"
    private lateinit var getListingController: GetListingController
    val list = ArrayList<FollowFollowingBean.FFlist>()
    lateinit var storeusedata: StoreUserData

    lateinit var storeUseData: StoreUserData
    var userId: String = ""

    lateinit var btn_refer: CustomButton


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_following, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_refer = view.findViewById(R.id.btn_refer)


        rlBusinessProfileFollowing?.setOnClickListener{
            val intent = Intent(context, BusinessProfileActivity::class.java)
            startActivity(intent)
        }

        rlAddBusinessProfileFollowing?.setOnClickListener{
            val intent = Intent(context, BusinessProfileActivity::class.java)
            startActivity(intent)
        }

        storeUseData = StoreUserData(context!!)
        userId = storeUseData.getString(Constants.USER_ID)

        llShareMyProfileFollowing?.setOnClickListener{
            Utils.shareMyProfileLink("profile",userId, context!!);
        }

        btn_refer.setOnClickListener{
            try {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.setType("text/plain")
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Gujarat Living")
                var shareMessage = "*Gujarat Living* \nGujarat's largest market place for Buy and Sell all type of products\n\n"
                shareMessage = """
                    ${shareMessage}https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}
                    """.trimIndent()
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                startActivity(Intent.createChooser(shareIntent, "choose one"))
            } catch (e: Exception) {
                //e.toString();
            }
        }

        //bindRecyclerView()
    }


    private fun bindRecyclerView() {

        val recyclerLayoutManager = LinearLayoutManager(context)
        rv_following.layoutManager = recyclerLayoutManager
        rv_following.itemAnimator = DefaultItemAnimator()
        val adapter = FollowingAdapter(list, context!!, 2)
        //val adapter = FollowerAdapter(ArrayList(), context!!,2)
        adapter.setClicklistner(this)
        rv_following.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun itemclick(bean: FollowFollowingBean.FFlist) {
        val intent = Intent(activity, FollowerProfileDetailActivity::class.java)
        Utils.openFollowingUserDetails(context!!, intent, bean.userId.toString())
    }


    override fun onResume() {
        Utils.showLog(TAG, "==on resume===")
        list.clear()
        getFollowingListCall()
        getUserBusinessProfile() // temp comment
        super.onResume()
    }

    private fun getUserBusinessProfile(){
        storeusedata = StoreUserData(context!!)

        val businessProfileId = storeusedata.getString(Constants.BUSINESS_USER_ID)

        getListingController = GetListingController(activity!!, this)
        getListingController.getUserBusinessProfile(businessProfileId)
    }

    private fun getFollowingListCall() {
        getListingController = GetListingController(activity!!, this)
        getListingController.getFollowingList()
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {



        if (method.equals("GetFollowingList")) {
            list.clear()
            val dataModel = FollowFollowingBean()
            dataModel.FollowFollowingBean(da)
            list.addAll(dataModel.list)
            Utils.dismissProgress()
            checkResponse()
        }

        if(method.equals("getBusinessProfile")){

            val userModel = BusinessUserProfileBean()
            userModel.businessUserProfileBean(da)

            Log.d("getBusinessProfile", "response : " + GsonBuilder().setPrettyPrinting().create().toJson(userModel))

            setBusinessData(userModel)


        }

    }

    override fun onFail(msg: String, method: String) {

        if(method.equals("getBusinessProfile")) {
            Log.d(TAG, "ERROR_RESPONSE FOLLOWING: " + msg)

            if(msg == "Bussiness Profile not Found"){

                try {
                    rlBusinessProfileFollowing?.visibility  = View.GONE
                    rlAddBusinessProfileFollowing?.visibility  = View.VISIBLE
                }catch (e: Exception){
                    e.printStackTrace()
                }


            }
        }

        if(method.equals("GetFollowingList")){
            Log.d(TAG, "ERROR_RESPONSE GetFollowingList: " + msg)
            try {
                rv_following.visibility = View.GONE
                tv_no_follow.visibility = View.VISIBLE
            }catch (e: Exception){

            }
        }

    }

    private fun setBusinessData(userModel: BusinessUserProfileBean) {

        rlBusinessProfileFollowing?.visibility   = View.VISIBLE
        rlAddBusinessProfileFollowing?.visibility  = View.GONE

        if(userModel.is_verified!=null){

            if(userModel.is_verified == "0"){
                tv_status_verify_following?.setText("Pending")
                imgVerifyStatusFollowing?.setImageResource(R.drawable.img_pending)
            }else if(userModel.is_verified == "1"){
                tv_status_verify_following?.setText("Verified")
                imgVerifyStatusFollowing?.setImageResource(R.drawable.verify)
            }else if(userModel.is_verified == "2"){
                tv_status_verify_following?.setText("Rejected")
                imgVerifyStatusFollowing?.setImageResource(R.drawable.img_rejected)
            }
        }

        if(userModel.profile_counter!=null){
            txtProfileVisit?.setText(userModel.profile_counter.toString())
        }

        if(userModel.followingCount!=null){
            txtMyFollowings?.setText(userModel.followingCount.toString())
        }






    }

    private fun checkResponse() {
        if(list.size > 0) {
            if(tv_no_follow!=null && rv_following!=null){
                tv_no_follow.visibility = View.GONE
                rv_following.visibility = View.VISIBLE
                bindRecyclerView()
            }
        } else{
            if(tv_no_follow!=null && rv_following!=null){
                tv_no_follow.visibility = View.VISIBLE
                rv_following.visibility = View.GONE
            }
        }
    }

   /* override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            // Refresh your fragment here
            fragmentManager!!.beginTransaction().detach(this).attach(this).commit()
        }
    }*/
}