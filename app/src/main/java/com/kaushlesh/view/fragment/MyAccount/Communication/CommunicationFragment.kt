package com.kaushlesh.view.fragment.MyAccount.Communication

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.kaushlesh.BuildConfig
import com.kaushlesh.R
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.MainActivity
import com.kaushlesh.view.fragment.MyAccount.PurchasePackage.AdPostPackagesActivity
import com.kaushlesh.view.fragment.MyAccount.PurchasePackage.PurchasePackageFragment
import com.kaushlesh.view.fragment.ProductListFragment
import kotlinx.android.synthetic.main.fragment_communication.*
import kotlinx.android.synthetic.main.fragment_purchase_package.*


class CommunicationFragment : Fragment(), View.OnClickListener {
    var cname: String = ""
    var category: String = ""
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    lateinit var ll_rate_us: LinearLayout

    private var popup: Dialog? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_communication, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar = view.findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)
        ll_rate_us = view.findViewById(R.id.ll_rate_us)

        tvtoolbartitle.text = resources.getString(R.string.txt_communication)

        btnback.setOnClickListener {
            if (activity != null) {
                activity!!.onBackPressed()
            }
        }

        ll_rate_us.setOnClickListener {
            showRateUsDialog()
        }

        addOnclicked()
    }

    fun showRateUsDialog() {

        popup = Dialog(context!!, R.style.DialogCustom)
        popup!!.setContentView(R.layout.dialog_rate_us)
        popup!!.setCanceledOnTouchOutside(false)
        popup!!.setCancelable(false)
        popup!!.show()
        val imgCloseDialog: ImageView = popup!!.findViewById(R.id.imgCloseDialog)
        val btnRateUs: Button = popup!!.findViewById(R.id.btnRateUs)



        imgCloseDialog.setOnClickListener {
            popup!!.dismiss()
        }

        btnRateUs.setOnClickListener{
            popup!!.dismiss()

            try {
                val viewIntent = Intent(
                        "android.intent.action.VIEW",
                        Uri.parse("https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}")
                )
                startActivity(viewIntent)
            } catch (e: java.lang.Exception) {
                //Utils.showToast(context, "Unable to Connect Try Again...")
                e.printStackTrace()
            }

        }
    }

    private fun addOnclicked() {
        ll_support.setOnClickListener(this)
        ll_invite_frnd.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.ll_support -> {
                val bundle = Bundle()
                bundle.putString("name", getString(R.string.str_support))
                //  bundle.putString("catName",getString(R.string.txt_purchase_package))
                (activity as MainActivity).changeFragment(
                    SupportFragment.newInstance(),
                    SupportFragment.TAG,
                    bundle,
                    true
                )
            }

            R.id.ll_invite_frnd -> {
                val intent = Intent(activity, ReferFriendActivity::class.java)
                startActivity(intent)
            }
        }
    }

    companion object {
        val TAG = "CommunicationFragment"
        fun newInstance(): CommunicationFragment {
            return CommunicationFragment()
        }
    }
}