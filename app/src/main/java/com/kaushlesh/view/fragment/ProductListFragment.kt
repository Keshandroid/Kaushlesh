package com.kaushlesh.view.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.*
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.*
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import com.google.gson.Gson
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.kaushlesh.Controller.AddFavPostController
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.R
import com.kaushlesh.adapter.ProductListAdapter
import com.kaushlesh.bean.Filter.FilterPropertyBean
import com.kaushlesh.bean.HomeFilterLocationBean
import com.kaushlesh.bean.ProductDataBean
import com.kaushlesh.bean.ProductListHouseBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.parser.GetRequestParsing
import com.kaushlesh.reusable.EndlessRecyclerOnScrollListenerList
import com.kaushlesh.reusable.LocationAddress
import com.kaushlesh.utils.*
import com.kaushlesh.utils.PostDetail.CheckCategoryForPostDetails
import com.kaushlesh.view.activity.Login.LoginMainActivity
import com.kaushlesh.view.filters.*
import kotlinx.android.synthetic.main.fragment_home_new.*
import kotlinx.android.synthetic.main.fragment_my_post.*
import kotlinx.android.synthetic.main.fragment_product_detail.*
import kotlinx.android.synthetic.main.fragment_product_detail.rl_no_post
import kotlinx.android.synthetic.main.item_row_fresh_recommendation.*
import org.json.JSONException
import org.json.JSONObject


class ProductListFragment : Fragment() ,ProductListAdapter.ItemClickListener,
    FilterViewVehicleComman.BottomSheetListener,FilterViewPropertyComman.BottomSheetListener,FilterViewGeneral.BottomSheetListener,
    FilterViewMobileComman.BottomSheetListener,FilterViewBikesComman.BottomSheetListener,FilterViewComman.BottomSheetListener,
    FilterViewGrainSeedComman.BottomSheetListener,FilterViewKhedutComman.BottomSheetListener,ParseControllerListener,View.OnClickListener,DistrictListDialogActivity.BottomSheetListener{
    private var scrollStart: Boolean = false
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var ivfilter: ImageView
    internal lateinit var ivmenu: ImageView
    internal lateinit var ivmenu2: ImageView
    internal lateinit var ivmenu3: ImageView
    var cname : String = ""
    var category : String = ""
    var categoryId: Int? = null
    var subCategoryId: Int? = null
    var url: String? = null
    lateinit var getListingController: GetListingController
    internal var list: ArrayList<ProductDataBean> = ArrayList()
    internal var plist: ArrayList<ProductListHouseBean.ProductListHouse> = ArrayList()
    lateinit var storeUserData: StoreUserData
    var searchword: String = ""
    var lati: String = ""
    var long: String = ""
    private lateinit var filterBeanDataModel: FilterPropertyBean
    private var fusedLocationClient: FusedLocationProviderClient? = null
    internal var latitude: Double = 0.toDouble()
    internal var longitude: Double = 0.toDouble()
    internal var curlatitude: Double = 0.toDouble()
    internal var curlongitude: Double = 0.toDouble()
    internal var addlatitude: Double = 0.toDouble()
    internal var addlongitude: Double = 0.toDouble()
    var locationName : String = ""
    var loc_id : String = ""
    lateinit var etLocation : EditText
    lateinit var ivRefresh : ImageView
    lateinit var filterbean : HomeFilterLocationBean
    var k: Int = 0

    private var featureRcvScrollListener: EndlessRecyclerOnScrollListenerList? = null
    lateinit var rv_products : RecyclerView

    private lateinit var rl_list: RelativeLayout
    private lateinit var rl_no_post: RelativeLayout

    //private var scrollListener: EndlessScrollRecyclListener? = null
    var current_page = 1
    private var progressStatus = 0
    private var handler: Handler = Handler()
    lateinit var progressBar: ProgressBar
    lateinit var adapter : ProductListAdapter
    var pageNo: Int = 0
    var radius: Int = 70

    //broadcast
    protected var localBroadcastManager: LocalBroadcastManager? = null

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_product_detail, container, false)
    }

    private val mLikeDislikePostReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.getStringExtra("islikedState") != null && intent.getStringExtra("postIdLikeDislike") != null) {
                val likeState = intent.getStringExtra("islikedState")
                val postIdLikeDislike = intent.getStringExtra("postIdLikeDislike")

                Log.d("CLICKED_ITEM", " Updated : POST_ID - " + postIdLikeDislike + " - notifyStatus - " + likeState)

                updateLikeDislikeStatus(likeState, postIdLikeDislike)

            }
        }
    }

    fun updateLikeDislikeStatus(likeState: String?, postIdLikeDislike: String?) {
        try {
            if(!adapter.getAllItems().isEmpty()){
                val items = adapter.getAllItems()
                if(!items.isEmpty()){
                    adapter.updateLikeDislikePost(likeState, postIdLikeDislike)
                }
            }
        }catch (e: Exception){
            e.printStackTrace()
        }
    }




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        localBroadcastManager = LocalBroadcastManager.getInstance(activity!!)
        localBroadcastManager!!.registerReceiver(mLikeDislikePostReceiver,
                IntentFilter("NOTIFY_POST_LIKE_DISLIKE"))
    }

    override fun onDestroy() {
        super.onDestroy()
        localBroadcastManager!!.unregisterReceiver(mLikeDislikePostReceiver)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getListingController = GetListingController(activity!!, this)
        filterbean = HomeFilterLocationBean()

        if (arguments != null) {
            cname = arguments!!.getString("name").toString()
            category  = arguments!!.getString("catName").toString()
            AppConstants.printLog(TAG, "==title==$cname" + "==category==$category")

            categoryId = arguments!!.getString("cateid")!!.toInt()
            subCategoryId = arguments!!.getString("subcateid")!!.toInt()
            url = arguments!!.getString("url").toString()

            AppConstants.printLog(TAG, "==catId==$categoryId" + "==subCatId==$subCategoryId")
        }
        storeUserData = StoreUserData(context!!)
        storeUserData.setString(Constants.SEARCHPRODUCTValue, "0")

        //Focus Location Client for user location
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity!!)

        filterBeanDataModel = FilterPropertyBean()

        toolbar = view.findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        ivfilter = toolbar.findViewById(R.id.iv_filter)
        ivmenu = toolbar.findViewById(R.id.iv_menu)
        ivmenu2 = toolbar.findViewById(R.id.iv_menu2)
        ivmenu3 = toolbar.findViewById(R.id.iv_menu3)
        etLocation = view.findViewById(R.id.et_find)
        ivRefresh = view.findViewById(R.id.iv_refresh)
        rv_products = view.findViewById(R.id.rv_products)
        progressBar = view.findViewById(R.id.preogressbar)
        rl_list = view.findViewById(R.id.rl_list)
        rl_no_post = view.findViewById(R.id.rl_no_post)


        ivmenu.visibility = View.VISIBLE
        ivmenu2.visibility = View.GONE
        ivmenu3.visibility = View.GONE

        rv_products.itemAnimator = null
        rv_products.getRecycledViewPool().clear()
        Utils.showLog(TAG, "==method call==" + plist.size)
        val layoutManager = GridLayoutManager(context, 2)
        rv_products.layoutManager = layoutManager
        //val adapter = ProductListAdapter(productList, context!!,1)
        adapter = ProductListAdapter(arrayListOf(), context!!, 1, categoryId, subCategoryId)
        adapter.setClicklistner(this)
        rv_products.adapter = adapter



        featureRcvScrollListener = object : EndlessRecyclerOnScrollListenerList()
        {
                    override fun onLoadMore(page: Int, totalItemsCount: Int) {

                        try {
                            Log.e("Pagination", "onLoadMore Invocke : ${page}")
                            val marginLayoutParams = RelativeLayout.LayoutParams(rv_products.getLayoutParams())
                            //val marginLayoutParams = ViewGroup.MarginLayoutParams(rcvListView.getLayoutParams())
                            marginLayoutParams.setMargins(0, 0, 0, 130)
                            rv_products.setLayoutParams(marginLayoutParams)
                            progressBar.setVisibility(View.VISIBLE)

                            Thread {
                                while (progressStatus < 100) {
                                    progressStatus += 20
                                    handler.post(Runnable {
                                        progressBar.progress = progressStatus
                                    })
                                    try {
                                        // Sleep for 200 milliseconds.
                                        Thread.sleep(100)
                                    } catch (e: InterruptedException) {
                                        e.printStackTrace()
                                    }
                                }
                            }.start()

                            pageNo = page * 20
                            radius = (page + 1) * 70
                            Utils.showLog(TAG, "====page===" + page)
                            Utils.showLog(TAG, "=== page multiple===" + page * 20)
                            if (loc_id.length == 0) {
                                callApitoGetLocationWiseList(locationName.toString(), loc_id, curlatitude.toString(), curlongitude.toString(), searchword, page * 20, radius)

                            } else {
                                callApitoGetLocationWiseList(locationName.toString(), loc_id, addlatitude.toString(), addlongitude.toString(), searchword, page * 20, radius)

                            }

                            //getListingController.getHomePostList(locationName.toString(), loc_id, latitude.toString(), longitude.toString(), searchword, page * 20)
                        }
                        catch (e: Exception)
                        {
                            e.printStackTrace()
                            Utils.showLog(TAG, e.printStackTrace().toString())
                        }
                    }

                }

        rv_products.addOnScrollListener(featureRcvScrollListener!!)


        //getProductApiCall("Product")

       /* getListingController = GetListingController(activity!!, this)

        getListingController.getProductList(categoryId.toString(),subCategoryId.toString(),url)*/

        btnback.setOnClickListener(this)
        rlSearch.setOnClickListener(this)
        et_find.setOnClickListener(this)
        ivfilter.setOnClickListener(this)
        ivmenu.setOnClickListener(this)
        ivmenu2.setOnClickListener(this)
        ivmenu3.setOnClickListener(this)
        ivRefresh.setOnClickListener(this)
    }

    private fun getProductListApiCall(i: Int) {


    }

    override fun onClick(v: View?) {
        when (v!!.getId()) {

            R.id.btn_back -> {
                if (activity != null) {
                    activity!!.onBackPressed()
                }
            }

            R.id.rlSearch -> {
                /*  val intent = Intent(context, SearchProductActivity::class.java)
                context?.startActivity(intent)*/

                DistrictListDialogActivity.display(childFragmentManager, this)
            }

            R.id.et_find -> {
                rlSearch.performClick()
            }

            R.id.iv_filter -> {
                //openBottomSheet(category)
                val comman = CommanPropertyTypeListFilter()
                comman.openBottomSheet(categoryId!!.toInt(), subCategoryId!!.toInt(), category, cname, this, fragmentManager!!, url.toString())
                /* val myBottomSheet = FilterViewVehicleComman.newInstance(this,cname)
                 myBottomSheet.show(fragmentManager!!, myBottomSheet.getTag())*/

                /*val mBottomSheetDialog = RoundedBottomSheetDialog(context!!)
                val sheetView = layoutInflater.inflate(R.layout.filter_comman_view, null)
                mBottomSheetDialog.setContentView(sheetView)
                mBottomSheetDialog.show()
                */
            }

            R.id.iv_menu -> {
                ivmenu.visibility = View.GONE
                ivmenu2.visibility = View.VISIBLE
                ivmenu3.visibility = View.GONE

                /* val layoutManager = LinearLayoutManager(context)
                rv_products.layoutManager = layoutManager
                val adapter = ProductListAdapter(plist, context!!,2,categoryId,subCategoryId)
                adapter.setClicklistner(this)
                rv_products.adapter = adapter
                adapter.notifyDataSetChanged()*/

                Utils.showLog(TAG, "==method call==" + plist.size)
                val layoutManager = LinearLayoutManager(context)
                rv_products.layoutManager = layoutManager
                //val adapter = ProductListAdapter(productList, context!!,1)
                adapter = ProductListAdapter(arrayListOf(), context!!, 2, categoryId, subCategoryId)
                adapter.setClicklistner(this)
                rv_products.adapter = adapter
                //adapter.notifyDataSetChanged()
                checkLocationData()
            }

            R.id.iv_menu2 -> {
                ivmenu.visibility = View.GONE
                ivmenu2.visibility = View.GONE
                ivmenu3.visibility = View.VISIBLE

                /* val layoutManager = GridLayoutManager(context,1)
                rv_products.layoutManager = layoutManager
                val adapter = ProductListAdapter(plist, context!!,3,categoryId,subCategoryId)
                adapter.setClicklistner(this)
                rv_products.adapter = adapter
                adapter.notifyDataSetChanged()*/


                Utils.showLog(TAG, "==method call==" + plist.size)
                val layoutManager = GridLayoutManager(context, 1)
                rv_products.layoutManager = layoutManager
                //val adapter = ProductListAdapter(productList, context!!,1)
                adapter = ProductListAdapter(arrayListOf(), context!!, 3, categoryId, subCategoryId)
                adapter.setClicklistner(this)
                rv_products.adapter = adapter
                //adapter.notifyDataSetChanged()

                checkLocationData()
            }

            R.id.iv_menu3 -> {
                ivmenu.visibility = View.VISIBLE
                ivmenu2.visibility = View.GONE
                ivmenu3.visibility = View.GONE

                /*val layoutManager = GridLayoutManager(context, 2)
                rv_products.layoutManager = layoutManager
                val adapter = ProductListAdapter(plist, context!!,1,categoryId,subCategoryId)
                adapter.setClicklistner(this)
                rv_products.adapter = adapter
                adapter.notifyDataSetChanged()*/


                Utils.showLog(TAG, "==method call==" + plist.size)
                val layoutManager = GridLayoutManager(context, 2)
                rv_products.layoutManager = layoutManager
                //val adapter = ProductListAdapter(productList, context!!,1)
                adapter = ProductListAdapter(arrayListOf(), context!!, 1, categoryId, subCategoryId)
                adapter.setClicklistner(this)
                rv_products.adapter = adapter
                //adapter.notifyDataSetChanged()

                checkLocationData()
            }

            R.id.iv_refresh -> {

                plist.clear()
                adapter.removeItems()
                takepermissionNew()


                val anim = RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, .5f, Animation.RELATIVE_TO_SELF, .5f)
                anim.setInterpolator(LinearInterpolator())
                anim.setRepeatCount(1)
                anim.setDuration(1000)
                ivRefresh.setAnimation(anim)
                ivRefresh.startAnimation(anim)
            }
        }
    }
    private fun getProductApiCall(apiCalltype: String) {

        if (apiCalltype == "Product") {
            getListingController = GetListingController(activity!!, this)
            getListingController.getProductList(
                    categoryId.toString(),
                    subCategoryId.toString(),
                    url
            )
        } else if (apiCalltype == "FilterProduct") {
            getListingController = GetListingController(activity!!, this)
            getListingController.callforDisplayfilterData(filterBeanDataModel, url)

        }
        storeUserData.setString(Constants.SEARCHPRODUCT, "")
        storeUserData.setString(Constants.LATITUDE, "")
        storeUserData.setString(Constants.LONGITUDE, "")
    }

    private fun bindRecyclerviewProducts(productlist: ArrayList<ProductListHouseBean.ProductListHouse>) {

        ivmenu.visibility = View.VISIBLE
        ivmenu2.visibility = View.GONE
        ivmenu3.visibility = View.GONE

        Utils.showLog(TAG, "==method call==" + productlist.size)
        val layoutManager = GridLayoutManager(context, 2)
        rv_products.layoutManager = layoutManager
        //val adapter = ProductListAdapter(productList, context!!,1)
        val adapter = ProductListAdapter(productlist, context!!, 1, categoryId, subCategoryId)
        rv_products.adapter = adapter
        adapter.setClicklistner(this)
        adapter.notifyDataSetChanged()
    }

    companion object {
        val TAG = "ProductListFragment"

        fun newInstance(): ProductListFragment {
            return ProductListFragment()
        }
    }

    override fun itemclickProduct(bean: ProductListHouseBean.ProductListHouse) {

        val storeusedata = StoreUserData(context!!)
        storeusedata.setString(Constants.OPEN_POST_DETAIL, "product_list")
        storeusedata.setString(Constants.CATEGORY_ID, bean.category_id.toString())
        storeusedata.setString(Constants.SUBCATEGORYID, bean.sub_category_id.toString())
        storeusedata.setString(Constants.FROM_DETAIL, "true")
        CheckCategoryForPostDetails.OpenScreen(context!!, bean.category_id!!.toInt(), bean.sub_category_id!!.toInt(), bean.post_id!!.toInt())
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {

        dismissProgressDialog()
        if (method.equals("Get Product")) {

            progressBar.setVisibility(View.GONE)
            progressStatus = 0
            val marginLayoutParams = RelativeLayout.LayoutParams(rv_products.getLayoutParams())
            marginLayoutParams.setMargins(0, 0, 0, 0)
            rv_products.setLayoutParams(marginLayoutParams)

            if (da.length() > 0) {

                Log.e(TAG, "fresh Product Result-" + "$da==")
                try {

                    plist.clear()
                    val dataModel = ProductListHouseBean()
                    dataModel.ProductListHouseBean(da)

                    rl_list.visibility = View.VISIBLE
                    rl_no_post.visibility = View.GONE
                    rv_products.visibility = View.VISIBLE


                   // plist = dataModel.productlist
                    Log.e(TAG, "Response Item Size: ${dataModel.productlist.size}")
                    plist.addAll(dataModel.productlist)
                    if(!plist.isEmpty() && plist.size >0)
                    {
                        Utils.showLog(TAG, "====scroll start====" + scrollStart)
                        if(scrollStart) {
                            adapter.removeItems()
                            rv_products.scrollToPosition(0)
                        }
                        Handler(Looper.getMainLooper()).postDelayed({
                            val distinct = plist.distinctBy { it.post_id }
                            //rcvListViewAdapter.setHomeItems(freshrecomlist)
                            adapter.addItems(distinct as java.util.ArrayList<ProductListHouseBean.ProductListHouse>)
                            rv_products.setItemViewCacheSize(adapter.itemCount)
                            scrollStart = false
                        }, 100)

                        Log.e(TAG, "Item : ${dataModel.productlist.size} RcvItemCount : ${adapter.itemCount}")
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                catch (e : NullPointerException)
                {
                    e.printStackTrace()
                }
            }

         /*   plist.clear()

            val dataModel = ProductListHouseBean()
            dataModel.ProductListHouseBean(da)

            plist = dataModel.productlist
            Utils.showLog(TAG,"==product list size==" + plist.size)

            if(plist.size >0) {

                try {
                    rl_list.visibility = View.VISIBLE
                    rl_no_post.visibility = View.GONE
                    rv_products.visibility = View.VISIBLE
                    bindRecyclerviewProducts(plist)
                }
                catch (e : IllegalStateException)
                {
                    e.printStackTrace()
                }
            }*/
        }

        if (method.equals("adFavPost")) {
            Utils.showToast(activity, message)
        }
    }

    override fun onFail(msg: String, method: String) {
        dismissProgressDialog()
       /* try {
            rv_products.visibility = View.GONE
            rl_list.visibility = View.GONE
            rl_no_post.visibility = View.VISIBLE
            //AppConstants.printToast(context!!,msg)
        } catch (e : IllegalStateException)
        {
            e.printStackTrace()
        }*/
        Utils.showLog(TAG, "====onfail list===" + plist.size)

        if(plist.size > 0)
        {
            progressBar.visibility = View.GONE
        }
        else{
            Utils.showToast(activity, msg)
            progressBar.visibility = View.GONE
            rv_products.visibility = View.GONE
            rl_list.visibility = View.GONE
            rl_no_post.visibility = View.VISIBLE
            //rlnopost.visibility = View.VISIBLE
            adapter.removeItems()
        }
    }

    override fun onResume() {
        super.onResume()
       /* Utils.showLog(TAG, "==on resume called ==" + k++)
        if(storeUserData.getString(Constants.SEARCHPRODUCTValue).equals("1")) {
            searchword = storeUserData.getString(Constants.SEARCHPRODUCT)
            lati = storeUserData.getString(Constants.LATITUDE)
            long = storeUserData.getString(Constants.LONGITUDE)
            showProgressDialog(this.activity!!, this.resources.getString(R.string.txt_please_wait))
            callforDisplayUserDatawithsearch(lati,long,searchword,storeUserData.getString(Constants.USER_ID),storeUserData.getString(Constants.TOKEN))
        }
        else{
            getProductApiCall("Product")
        }*/

        checkLocationData()
    }

    private fun checkLocationData() {

        plist.clear()

        val locData = storeUserData.getBoolean(Constants.GET_LOCA_DATA)

        if(locData) {
            Utils.showLog(TAG, "===check location data if===")
            val gson = Gson()
            val json: String = storeUserData.getString(Constants.HOME_LOC_FILTER)
            Utils.showLog(TAG, "===location saved data===" + json)

            if (!json.isEmpty() || json != null) {
                filterbean = gson.fromJson(json, HomeFilterLocationBean::class.java)
                Utils.showLog(TAG, "===location saved data===" + filterbean)

                if(filterbean.locationName.isEmpty() || filterbean.locationName.length ==0) {
                    etLocation.setText("Searching Location")
                    locationName = ""
                }
                else {
                    locationName = filterbean.locationName
                    etLocation.setText(locationName)
                }
                loc_id = filterbean.loc_id
                latitude = filterbean.latitude.toDouble()
                longitude = filterbean.longitude.toDouble()
                searchword = filterbean.searchword

                if(loc_id.length == 0)
                {
                    curlatitude = filterbean.latitude.toDouble()
                    curlongitude = filterbean.longitude.toDouble()
                    //checkLocation()
                    //ivRefresh.visibility = View.VISIBLE
                    //callApitoGetHomeFreshPost(locationName.toString(), loc_id, latitude.toString(), longitude.toString(), searchword, 0)
                }
                else {
                    addlatitude =filterbean.latitude.toDouble()
                    addlongitude = filterbean.longitude.toDouble()
                    ivRefresh.visibility = View.GONE
                    //callApitoGetHomeFreshPost(locationName.toString(), loc_id, latitude.toString(), longitude.toString(), searchword, pageNo)
                }
                val storeusedata = StoreUserData(context!!)
                val isfrom = storeusedata.getString(Constants.FROM_DETAIL)
                Utils.showLog(TAG, "====screen is from detail ===" + isfrom + pageNo)
                if(isfrom.equals("true"))
                {
                    storeusedata.setString(Constants.FROM_DETAIL, "false");
                }
                else {
                    featureRcvScrollListener?.reset()
                    //resetAdditionalFilterData()
                    adapter.removeItems()
                    pageNo = 0
                    radius = 70
                    callApitoGetLocationWiseList(locationName.toString(), loc_id, latitude.toString(), longitude.toString(), searchword, pageNo, radius)
                }
            }
        }
        else{
            Utils.showLog(TAG, "===check location data else===")
            takepermissionNew()
        }
    }

    private fun resetAdditionalFilterData() {
        plist.clear()
        filterBeanDataModel = FilterPropertyBean()
        filterBeanDataModel.categoryId = categoryId.toString()
        filterBeanDataModel.subCategoryId = subCategoryId.toString()

        FiltersSaveRemoveData.cleardata(categoryId.toString(), subCategoryId.toString())

    }

    fun showProgressDialog(activity: Activity, msg: String) {
        try {
            if (Utils.progressDialog == null) {
                Utils.showProgress(activity)
            }
        }
        catch (e: NullPointerException)
        {
            e.printStackTrace()
        }
    }

    fun dismissProgressDialog() {

        if (Utils.progressDialog != null && Utils.progressDialog!!.isShowing()) {
            Utils.dismissProgress()
        }
    }

    fun callforDisplayUserDatawithsearch(lat: String, long: String, searchword: String, userid: String, token: String) {

        list.clear()
        val p = GetRequestParsing()
        val url = url + "?userId=" + userid + "&userToken=" + token + "&category_id=" + categoryId +
                    "&sub_category_id=" + subCategoryId +
                    "&serchWord=" + searchword +
                    "&latitude=" + lat +
                    "&longitude=" + long

        p.callApi(activity!!, url, "Get Product", this)
    }


    override fun onButtonClicked(text: String, type: String, list: ArrayList<ProductListHouseBean.ProductListHouse>) {
        plist.clear()

        if(list.size >0) {

            plist = list

            rl_list.visibility = View.VISIBLE
            rl_no_post.visibility = View.GONE
            rv_products.visibility = View.VISIBLE
            bindRecyclerviewProducts(plist)
        }

        else{
            rv_products.visibility = View.GONE
            rl_list.visibility = View.GONE
            rl_no_post.visibility = View.VISIBLE
            AppConstants.printToast(context!!, type)
        }
    }

    override fun callApiWithFilter(datamodel: FilterPropertyBean, url1: String) {
        //storeUserData.setString(Constants.FROM_DETAIL, "true")
        Utils.showLog(TAG, "===FILETR BEAN====" + datamodel)
        Utils.showLog(TAG, "===FILETR BEAN p1====" + datamodel.minPrice)
        Utils.showLog(TAG, "===FILETR BEAN p2====" + datamodel.maxPrice)
        pageNo = 0
        scrollStart = true
        filterBeanDataModel = datamodel
        url = url1
        checkLocationData()

    }

    override fun itemFavUnfavourite(dataModel: ProductListHouseBean.ProductListHouse, position: Int) {
        if(AppConstants.checkUserIsLoggedin(activity!!))
        {
            val c = AddFavPostController(this, activity!!, dataModel.post_id.toString())
            c.onClick(iv_like)
        }
        else{
            val intent = Intent(context, LoginMainActivity::class.java)
            AppConstants.alertLogin(activity!!, getString(R.string.txt_login_ad_fav), intent)
        }
    }

    private fun takepermissionNew() {

        val permissionList = arrayListOf<String>()
        permissionList.add(Manifest.permission.INTERNET)
        permissionList.add(Manifest.permission.ACCESS_COARSE_LOCATION)
        permissionList.add(Manifest.permission.ACCESS_FINE_LOCATION)

        //original added
        /*if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
        {
            permissionList.add(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
        }*/
        Utils.showLog(HomeFragmentNew.TAG, "===take permission====")

        //Dexter.withContext(requireContext()).withPermissions(permissionList).withListener(this).check()

        Dexter.withContext(context)
                .withPermissions(permissionList).withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(multiplePermissionsReport: MultiplePermissionsReport) {
                        if (multiplePermissionsReport.areAllPermissionsGranted()) {
                            Utils.showLog(HomeFragmentNew.TAG, "===permission checked====")
                            //getUserLocation()
                            checkLocation()
                            return
                        }
                        if (multiplePermissionsReport.isAnyPermissionPermanentlyDenied) {
                            Utils.showLog(HomeFragmentNew.TAG, "===permission denied====")
                            //showSettingsDialog() // originally its added
                        }

                    }

                    override fun onPermissionRationaleShouldBeShown(
                            list: List<PermissionRequest>,
                            permissionToken: PermissionToken
                    ) {
                        Utils.showLog(HomeFragmentNew.TAG, "===permission continue====")
                        permissionToken.continuePermissionRequest()
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                            //showSettingsDialog()// originally its added
                        }
                        //showSettingsDialog()
                    }
                })
                .onSameThread().check()
    }

    private fun getPermissionList(): ArrayList<String> {
        val permissionList = arrayListOf<String>()
        permissionList.add(Manifest.permission.INTERNET)
        permissionList.add(Manifest.permission.ACCESS_COARSE_LOCATION)
        permissionList.add(Manifest.permission.ACCESS_FINE_LOCATION)

        //original added
        /*if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
        {
            permissionList.add(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
        }*/
        val deniedPermissionList = arrayListOf<String>()
        permissionList.forEach {
            if(ContextCompat.checkSelfPermission(requireContext(), it) != PackageManager.PERMISSION_GRANTED)
            {
                Log.d(HomeFragmentNew.TAG, "Permission Not Grandted : $it")
                deniedPermissionList.add(it)
            }
        }

        return deniedPermissionList
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {

        when (requestCode) {
            2012 -> {
                Log.e(HomeFragmentNew.TAG, "=====ONPERMISSION RESULY===")
                getPermissionList()
                for (grantResult in grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        return
                    }
                }
                checkLocation()
            }

            1200 -> {
                if (grantResults.size <= 0) {
                    // If user interaction was interrupted, the permission request is cancelled and you
                    // receive empty arrays.
                    Log.i("WoosmapGeofencing", "User interaction was cancelled.")
                } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i("WoosmapGeofencing", "Permission granted, updates requested, starting location updates")
                } else {
                    // Permission denied.

                    // Notify the user via a SnackBar that they have rejected a core permission for the
                    // app, which makes the Activity useless. In a real app, core permissions would
                    // typically be best requested during a welcome-screen flow.

                    // Additionally, it is important to remember that a permission might have been
                    // rejected without asking the user for permission (device policy or "Never ask
                    // again" prompts). Therefore, a user interface affordance is typically implemented
                    // when permissions are denied. Otherwise, your app could appear unresponsive to
                    // touches or interactions which have required permissions.
                    showSettingsDialog()
                    /* showSnackbar(android.R.string.permission_denied_explanation, android.R.string.settings,
                                View.OnClickListener { // Build intent that displays the App settings screen.
                                    val intent = Intent()
                                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                    val uri = Uri.fromParts("package",
                                            BuildConfig.APPLICATION_ID, null)
                                    intent.data = uri
                                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                    startActivity(intent)
                                })*/
                }
            }
        }
    }

    override fun selectedLocation(district: String, locId: String, name: String, lat: String, long: String) {
        Utils.showLog(HomeFragmentNew.TAG, "---district---" + district + "--- id ---" + locId)
        Utils.showLog(HomeFragmentNew.TAG, "---name---" + name + "--- lat ---" + lat + "--- long ---" + long)
        featureRcvScrollListener?.reset()
        storeUserData.setBoolean(Constants.GET_LOCA_DATA, false)
        loc_id = locId
        //latitude = lat.toDouble()
        //longitude = long.toDouble()
        addlatitude = lat.toDouble()
        addlongitude = long.toDouble()
        if(!name.isEmpty() && name.toString().length > 0)
        {
            etLocation!!.setText(name)
            locationName = name
        }
        else {
            etLocation!!.setText(district)
            locationName = district
        }
        if(loc_id.length >0) {
            scrollStart = true
        }
        ivRefresh.visibility = View.GONE


        resetAdditionalFilterData()
        callApitoGetLocationWiseList(locationName.toString(), loc_id, addlatitude.toString(), addlongitude.toString(), searchword, 0, 70)
        //getListingController.getHomePostList(locationName.toString(), loc_id, latitude.toString(), longitude.toString(), searchword, 0)
    }

    private fun callApitoGetLocationWiseList(locationName: String, loc_id: String, latitude: String, longitude: String, searchword: String, page: Int, radius: Int) {
        //showProgressDialog(this.activity!!, this.resources.getString(R.string.txt_please_wait))

        when(categoryId)
        {
            1 -> {
                getListingController.callforDisplayfilterVehicalDataNew(locationName.replace(" ", ""), loc_id, latitude, longitude, searchword, page, radius, filterBeanDataModel, url, categoryId.toString(), subCategoryId.toString())

            }
            2 -> {
                getListingController.callforDisplayfilterDataNew(locationName.replace(" ", ""), loc_id, latitude, longitude, searchword, page, radius, filterBeanDataModel, url, categoryId.toString(), subCategoryId.toString())

            }
            3 -> {
                getListingController.callforDisplayfilterMobileDataNew(locationName.replace(" ", ""), loc_id, latitude, longitude, searchword, page, radius, filterBeanDataModel, url, categoryId.toString(), subCategoryId.toString())
            }
            4 -> {
                getListingController.callforDisplayfilterBikeDataNew(locationName.replace(" ", ""), loc_id, latitude, longitude, searchword, page, radius, filterBeanDataModel, url, categoryId.toString(), subCategoryId.toString())

            }
            5 -> {
                getListingController.callforDisplayfilterGenralDataNew(locationName.replace(" ", ""), loc_id, latitude, longitude, searchword, page, radius, filterBeanDataModel, url, categoryId.toString(), subCategoryId.toString())

            }
            6 -> {
                getListingController.callforDisplayfilterGenralDataNew(locationName.replace(" ", ""), loc_id, latitude, longitude, searchword, page, radius, filterBeanDataModel, url, categoryId.toString(), subCategoryId.toString())
            }
            7 -> {
                getListingController.callforDisplayfilterShopCityDataNew(locationName.replace(" ", ""), loc_id, latitude, longitude, searchword, page, radius, filterBeanDataModel, url, categoryId.toString(), subCategoryId.toString())
            }
            8 -> {
                getListingController.callforDisplayfilterGenralDataNew(locationName.replace(" ", ""), loc_id, latitude, longitude, searchword, page, radius, filterBeanDataModel, url, categoryId.toString(), subCategoryId.toString())
            }
            9 -> {
                getListingController.callforDisplayfilterGenralDataNew(locationName.replace(" ", ""), loc_id, latitude, longitude, searchword, page, radius, filterBeanDataModel, url, categoryId.toString(), subCategoryId.toString())
            }
            10 -> {
                getListingController.callforDisplayfilterShopCityDataNew(locationName.replace(" ", ""), loc_id, latitude, longitude, searchword, page, radius, filterBeanDataModel, url, categoryId.toString(), subCategoryId.toString())
            }
            11 -> {
                getListingController.callforDisplayfilterShopCityDataNew(locationName.replace(" ", ""), loc_id, latitude, longitude, searchword, page, radius, filterBeanDataModel, url, categoryId.toString(), subCategoryId.toString())
            }
            12 -> {
                getListingController.callforDisplayfilterGainSeedsNew(locationName.replace(" ", ""), loc_id, latitude, longitude, searchword, page, radius, filterBeanDataModel, url, categoryId.toString(), subCategoryId.toString())
            }
        }

        //callforDisplayUserDatawithsearch(latitude,longitude,searchword,storeUserData.getString(Constants.USER_ID),storeUserData.getString(Constants.TOKEN))

        filterbean.locationName = locationName
        filterbean.loc_id = loc_id
        filterbean.latitude = latitude
        filterbean.longitude= longitude
        filterbean.searchword = searchword

        val gson = Gson()
        val json = gson.toJson(filterbean)
        storeUserData.setString(Constants.HOME_LOC_FILTER, json)
        storeUserData.setBoolean(Constants.GET_LOCA_DATA, true)
    }

    override fun currentLocation() {
        // takepermission()
        featureRcvScrollListener?.reset()
        loc_id = ""
        storeUserData.setBoolean(Constants.GET_LOCA_DATA, false)
        takepermissionNew()
        ivRefresh.visibility = View.VISIBLE

        resetAdditionalFilterData()
    }

    fun isGPSEnabled(mContext: Context): Boolean {
        val locationManager = mContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    private var locationManager: LocationManager? = null
    private var myLocationListener: LocationListener? = null

    @SuppressLint("MissingPermission")
    fun checkLocation() {
        Log.e(TAG, "checkLocation called!")
        if(!isGPSEnabled(requireContext()))
        {
            checkGPSEnabled()
            Log.e(HomeFragmentNew.TAG, "GPS provider is not enabled ")
        }
        else {

            ivRefresh.visibility = View.VISIBLE
            try {
                val serviceString = Context.LOCATION_SERVICE
                locationManager = context?.getSystemService(serviceString) as LocationManager?

                handler = Handler()
                val location = locationManager!!.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                Log.e(TAG, "last location" + location)
                if (location != null) {
                    //latitude = location.getLatitude()
                    //longitude = location.getLongitude()
                    curlatitude = location.getLatitude()
                    curlongitude = location.getLongitude()
                }
                Log.e(TAG, "Lat1 : $curlatitude  Long 1: $curlongitude")
                val locationAddress = LocationAddress()
                locationAddress.getAddressFromLocation(curlatitude, curlongitude, context!!, GeoCodeHandler())

                myLocationListener = object : LocationListener {
                    override fun onLocationChanged(locationListener: Location) {
                        Log.e(TAG, "onLocationChanged invoked")

                        if (locationListener != null) {
                            //latitude = locationListener.latitude
                            //longitude = locationListener.longitude
                            curlatitude = locationListener.latitude
                            curlongitude = locationListener.longitude

                            Log.e(TAG, "Lat : $curlatitude  Long : $curlongitude")

                            val locData = storeUserData.getBoolean(Constants.GET_LOCA_DATA)
                            if (locData) {
                                val gson = Gson()
                                val json: String = storeUserData.getString(Constants.HOME_LOC_FILTER)
                                Utils.showLog(TAG, "===location saved data===" + json)

                                if (!json.isEmpty() || json != null) {
                                    filterbean = gson.fromJson(json, HomeFilterLocationBean::class.java)
                                    Utils.showLog(TAG, "===location saved data===" + filterbean)
                                    loc_id = filterbean.loc_id

                                    if (loc_id.length == 0 && location != null) {
                                        /* if (location.getLatitude() != latitude && location.getLongitude() != longitude)*/
                                        if (location.getLatitude() != curlatitude && location.getLongitude() != curlongitude){
                                            Log.e(TAG, "if")
                                            //val locationAddress1 = LocationAddress()
                                            //locationAddress1.getAddressFromLocation(curlatitude, curlongitude, context!!, GeoCodeHandler())

                                            if(adapter.itemCount <= 0) {
                                                plist.clear()
                                                featureRcvScrollListener?.reset()
                                                adapter.removeItems()
                                                val locationAddress1 = LocationAddress()
                                                locationAddress1.getAddressFromLocation(curlatitude, curlongitude, context!!, GeoCodeHandler())
                                            }
                                        }
                                        else{
                                            curlatitude = locationListener.latitude
                                            curlongitude = locationListener.longitude
                                            Log.e(TAG, "if else")
                                            if(adapter.itemCount <= 0) {
                                                plist.clear()
                                                featureRcvScrollListener?.reset()
                                                adapter.removeItems()
                                                val locationAddress1 = LocationAddress()
                                                locationAddress1.getAddressFromLocation(locationListener.latitude, locationListener.longitude, context!!, GeoCodeHandler())
                                            }
                                        }
                                    }
                                }
                            } else {
                                Log.e(TAG, "else")
                                try {
                                    if (location!!.getLatitude() != curlatitude && location.getLongitude() != curlongitude) {
                                        Log.e(TAG, "if")
                                        //val locationAddress1 = LocationAddress()
                                        //locationAddress1.getAddressFromLocation(curlatitude, curlongitude, context!!, GeoCodeHandler())

                                        if (adapter.itemCount <= 0)
                                        {
                                            plist.clear()
                                            featureRcvScrollListener?.reset()
                                            adapter.removeItems()
                                            val locationAddress1 = LocationAddress()
                                            locationAddress1.getAddressFromLocation(curlatitude, curlongitude, context!!, GeoCodeHandler())
                                        }
                                    }
                                    else{
                                        curlatitude = locationListener.latitude
                                        curlongitude = locationListener.longitude
                                        Log.e(TAG, "else if")

                                        if(adapter.itemCount <= 0) {
                                            plist.clear()
                                            featureRcvScrollListener?.reset()
                                            adapter.removeItems()
                                            val locationAddress1 = LocationAddress()
                                            locationAddress1.getAddressFromLocation(locationListener.latitude, locationListener.longitude, context!!, GeoCodeHandler())
                                        }
                                    }
                                } catch (e: NullPointerException) {
                                    e.printStackTrace()
                                }
                            }

                            /*  if (location == null || location.toString().length == 0) {
                            val locationAddress = LocationAddress()
                            locationAddress.getAddressFromLocation(latitude, longitude, context!!, GeoCodeHandler())
                        }*/
                        }

                    }

                    override fun onProviderDisabled(provider: String) {}
                    override fun onProviderEnabled(provider: String) {}
                    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
                }

                locationManager!!.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000 * 60 * 5, 0f, myLocationListener!!)

            } catch (e: NullPointerException) {
                e.printStackTrace()
            }
        }
    }

    private fun checkGPSEnabled() {
        val manager = requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER).not()) {
            turnOnGPS()
        }
    }

    private fun turnOnGPS() {
        val request = LocationRequest.create().apply {
            interval = 1000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
        val builder = LocationSettingsRequest.Builder().addLocationRequest(request)
        val client: SettingsClient = LocationServices.getSettingsClient(requireActivity())
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
        task.addOnFailureListener {
            if (it is ResolvableApiException) {
                try {
                    it.startResolutionForResult(requireActivity(), 12345)
                } catch (sendEx: IntentSender.SendIntentException) {
                }
            }
        }.addOnSuccessListener {
            //here GPS is On
            checkLocation()
        }
    }

    internal inner class GeoCodeHandler : Handler() {
        override fun handleMessage(message: Message) {
            val locationAddress: String
            locationAddress = when (message.what) {
                1 -> {
                    val bundle = message.data
                    bundle.getString("address").toString()
                }
                /*else -> null.toString()*/
                else -> "Gujarat"
                /*else -> "Searching Location"*/
            }
            Utils.showLog(HomeFragmentNew.TAG, "---- address---" + locationAddress)
            if(locationAddress.equals("null") || locationAddress.isEmpty())
            {
                etLocation.setText("Searching Location")
                progressBar.visibility = View.VISIBLE
            }
            else {
                etLocation.setText(locationAddress)
            }

            locationName = locationAddress
            plist.clear()
            scrollStart = true
            Log.i(HomeFragmentNew.TAG, "======= CURRENT LOCATION =========" + locationAddress + "Lattitude : " + curlatitude + "Longitude : " + curlongitude)

            featureRcvScrollListener?.reset()
            callApitoGetLocationWiseList(locationAddress.replace(" ", ""), "", curlatitude.toString(), curlongitude.toString(), searchword, 0, 70)
            //getListingController.getHomePostList(locationAddress.replace(" ", ""), "", latitude.toString(), longitude.toString(), searchword, 0)
        }
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle("Need Location Permissions")
        builder.setMessage("This app needs location permission to use this application. You can grant them in app settings.")
        builder.setPositiveButton(
                "GOTO SETTINGS"
        ) { dialog, which ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton(
                "Cancel"
        ) { dialog, which -> dialog.cancel() }
        builder.show()
    }

    // navigating user to app settings
    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", context?.getPackageName(), null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            101 -> {
                checkLocation()
            }
        }
    }

}
