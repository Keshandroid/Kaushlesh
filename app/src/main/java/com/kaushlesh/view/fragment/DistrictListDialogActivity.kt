package com.kaushlesh.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.R
import com.kaushlesh.adapter.LocationListAdapter
import com.kaushlesh.bean.LocationListBeans
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Utils
import kotlinx.android.synthetic.main.layout_district_list.*
import org.json.JSONObject

class DistrictListDialogActivity: DialogFragment(),View.OnClickListener,ParseControllerListener ,LocationListAdapter.ItemClickListener,AddAreaDialogActivity.BottomSheetListener {

    lateinit var rvdis : RecyclerView
    lateinit var rlclose : RelativeLayout
    lateinit var llCurrentLocation : LinearLayout
    lateinit var getListingController: GetListingController
    internal var locationlist: ArrayList<LocationListBeans.LocationList> = ArrayList()
    var bottomSheetListener: BottomSheetListener? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppTheme_FullScreenDialog)
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window!!.setLayout(width, height)
            //dialog.window!!.setWindowAnimations(R.style.AppTheme_Slide)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)
        val view: View = inflater.inflate(R.layout.layout_district_list, container, false)
        rvdis  = view.findViewById(R.id.rv_district)
        rlclose  = view.findViewById(R.id.rl_close)
        llCurrentLocation = view.findViewById(R.id.ll_current_location)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //bindRecyclerviewFoodTypes()

        getListingController = GetListingController(activity!! ,this)

        rlclose.setOnClickListener(this)
        tv_guj.setOnClickListener(this)
        llCurrentLocation.setOnClickListener(this)

        getListingController.getLocationList()

    }

    companion object {
        const val TAG = "DistrictListDialogActivity"
        fun display(fragmentManager: FragmentManager, listener: BottomSheetListener): DistrictListDialogActivity {
            val exampleDialog = DistrictListDialogActivity()
            exampleDialog.show(fragmentManager, TAG)
            exampleDialog.bottomSheetListener = listener
            return exampleDialog
        }

       /* fun newInstance(listener: BottomSheetListener): DistrictListDialogActivity {
            val fragment = DistrictListDialogActivity()
            fragment.bottomSheetListener = listener
            return fragment
        }*/
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.rl_close -> {
                dismiss()
            }

            R.id.tv_guj -> {
                bottomSheetListener!!.selectedLocation("Gujarat", "0","","0.0","0.0")
                dismiss()
            }

            R.id.ll_current_location -> {
                bottomSheetListener!!.currentLocation()
                dismiss()
            }
        }
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        locationlist.clear()

        val dataModel = LocationListBeans()
        dataModel.LocationListBeans(da)

        locationlist = dataModel.locationList
        Utils.showLog(TAG, "==location list size==" + locationlist.size)

        bindRecyclerview(locationlist)
    }

    private fun bindRecyclerview(locationlist: java.util.ArrayList<LocationListBeans.LocationList>) {
        try {
            val layoutManager = LinearLayoutManager(context)
            rvdis.layoutManager = layoutManager
            val adapter = LocationListAdapter(locationlist, context!!)
            adapter.setClicklistner(this)
            rvdis.adapter = adapter
            adapter.notifyDataSetChanged()
        }
        catch (e : NullPointerException)
        {
            e.printStackTrace()
        }
    }

    override fun onFail(msg: String, method: String) {
       Utils.showToast(activity,method)
    }

    override fun itemclickSubCategory(bean: LocationListBeans.LocationList) {
        //bottomSheetListener!!.selectedLocation(bean.locName.toString(), bean.locId.toString(),"","","")
        val bundle = Bundle()
        bundle.putString("district",bean.locName)
        bundle.putString("id",bean.locId.toString())
        AddAreaDialogActivity.display(childFragmentManager,this,bundle)
        //dismiss()
    }

    interface BottomSheetListener {
        fun selectedLocation(district: String,locId: String,name : String, lat : String,long: String)
        fun currentLocation()
    }

    override fun selectedLocation(district: String, locId: String, name: String, lat: String, long: String) {
        bottomSheetListener!!.selectedLocation(district.toString(), locId.toString(),name,lat,long)
        dismiss()
    }

}