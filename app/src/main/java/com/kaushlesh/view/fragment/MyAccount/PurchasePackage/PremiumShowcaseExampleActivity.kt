package com.kaushlesh.view.fragment.MyAccount.PurchasePackage

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.GridLayoutManager
import com.kaushlesh.R
import com.kaushlesh.adapter.PremiumAdExampleAdapter
import com.kaushlesh.bean.GetMyPostBean
import com.kaushlesh.bean.ProductDataBean
import kotlinx.android.synthetic.main.activity_premium_ad_example.*

class PremiumShowcaseExampleActivity : AppCompatActivity() ,PremiumAdExampleAdapter.ItemClickListener{

    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_premium_ad_example)

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = getString(R.string.txt_premium_showcase_example)

        btnback.setOnClickListener {
            this.onBackPressed()
        }

        bindRecyclerview()
    }

    private fun bindRecyclerview() {

        val layoutManager = GridLayoutManager(applicationContext, 2)
        rv_premium_ads.layoutManager = layoutManager
        val adapter = PremiumAdExampleAdapter(applicationContext!!)
        adapter.setClicklistner(this)
        rv_premium_ads.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun itemclickProduct(bean: GetMyPostBean.getMyPostList) {
    }

}
