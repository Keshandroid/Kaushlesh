package com.kaushlesh.view.fragment.Chat

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.*
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.R
import com.kaushlesh.adapter.chat.BuyChatAdapter
import com.kaushlesh.bean.chat.*
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.ChatActivity
import com.kaushlesh.view.activity.ChatDetailActivity
import kotlinx.android.synthetic.main.fragment_buying_chat.*
import kotlinx.android.synthetic.main.fragment_buying_chat.rl_no_chat
import kotlinx.android.synthetic.main.layout_no_chat.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class BuyingChatFragment : Fragment()  ,BuyChatAdapter.ItemClickListener , ParseControllerListener {

    lateinit var storeUserData: StoreUserData
    var luserid : Int ?= null
    private val latestMessagesMap = HashMap<String, ChatNewList>()
    internal var list: ArrayList<ChatNewList> = ArrayList()
    internal lateinit var rvchats: RecyclerView
    lateinit var getListingController: GetListingController
    var lusername : String ?= null
    var luserprofile : String ?= null
    internal var listid: ArrayList<Int> = ArrayList()
    internal lateinit var dialog: Dialog


    companion object {

        val TAG = "BuyingChatFragment"

        fun newInstance(): BuyingChatFragment {
            return BuyingChatFragment()
        }

    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_buying_chat, container, false)

        val view: View = inflater.inflate(R.layout.fragment_buying_chat, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getListingController = GetListingController(activity!!, this)
        getListingController.getLoginUserDetails()

        rvchats = view.findViewById(R.id.rv_chat_buy)

        storeUserData = StoreUserData(context!!)
        luserid = storeUserData.getString(Constants.USER_ID).toString().toInt()

        listenForLatestMessages()

        tv_deleteb.setOnClickListener {
            openDeleteDialog()
        }

        btn_start_chat.setOnClickListener {
            (activity as ChatActivity).displayHomeTab()
        }
    }

    private fun listenForLatestMessages()
    {
        list.clear()
        val ref = FirebaseDatabase.getInstance().getReference("/List/").child("$luserid")

        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                Log.d(TAG, "database error: " + databaseError.message)
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                Log.d(TAG, "has children: " + dataSnapshot.hasChildren())
                try {
                    if (dataSnapshot.childrenCount > 0) {
                        setChatView(View.VISIBLE,View.GONE)
                    } else {
                        setChatView(View.GONE,View.VISIBLE)
                    }
                }
                catch (e:NullPointerException)
                {
                    e.printStackTrace()
                }
            }

        })


        ref.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
            }

            override fun onChildMoved(dataSnapshot: DataSnapshot, previousChildName: String?) {
            }

            @RequiresApi(Build.VERSION_CODES.O)
            override fun onChildChanged(dataSnapshot: DataSnapshot, previousChildName: String?) {
                dataSnapshot.getValue(ChatNewList::class.java)?.let {
                    latestMessagesMap[dataSnapshot.key!!] = it
                    refreshRecyclerViewMessages(1)
                }
            }

            @RequiresApi(Build.VERSION_CODES.O)
            override fun onChildAdded(dataSnapshot: DataSnapshot, previousChildName: String?) {
                dataSnapshot.getValue(ChatNewList::class.java)?.let {
                    latestMessagesMap[dataSnapshot.key!!] = it
                    refreshRecyclerViewMessages(1)
                }
            }

            override fun onChildRemoved(p0: DataSnapshot) {
            }

        })
    }

    private fun setChatView(v1: Int, v2: Int) {
        try {
            rvchats.visibility = v1
            rl_no_chat.visibility = v2
        }
        catch (e : NullPointerException)
        {
            e.printStackTrace()
        }
        catch (e : IllegalStateException)
        {
            e.printStackTrace()
        }
    }

    private fun refreshRecyclerViewMessages(i: Int) {

        list.clear()

        try {

            // latestMessagesMap.forEach { (key, value) -> println("$key = $value") }
            latestMessagesMap.keys.forEach {
                Utils.showLog("id",latestMessagesMap[it]?.proId.toString())

                if(latestMessagesMap[it]?.chattype.toString().equals("b")) {
                    list.add(
                        ChatNewList(
                            latestMessagesMap[it]?.proId.toString(),
                            latestMessagesMap[it]?.chattype.toString(),
                            latestMessagesMap[it]?.counter.toString(),
                            latestMessagesMap[it]?.date.toString(),
                            latestMessagesMap[it]?.id.toString(),
                            latestMessagesMap[it]?.image.toString(),
                            latestMessagesMap[it]?.name.toString(),
                            latestMessagesMap[it]?.text.toString(),
                            latestMessagesMap[it]?.time.toString(),
                            latestMessagesMap[it]?.subCatName.toString(),
                            latestMessagesMap[it]?.userProfile.toString(),
                            latestMessagesMap[it]?.timestamp.toString(),
                            latestMessagesMap[it]?.subCategoryID.toString(),
                            latestMessagesMap[it]?.categoryID.toString(),
                            latestMessagesMap[it]?.adTitle.toString(),
                            latestMessagesMap[it]?.price.toString(),
                            latestMessagesMap[it]?.deleteDate.toString()
                        )
                    )

                }
            }

         /*   list.sortByDescending{it.time}

            //Utils.showLog(TAG, "==list time==" + GsonBuilder().setPrettyPrinting().create().toJson(list))

            list.sortByDescending{it.date}

            //Utils.showLog(TAG, "==list date==" + GsonBuilder().setPrettyPrinting().create().toJson(list))*/

            list.sortByDescending{it.time}

            list.sortByDescending{it.date.toDate()}

            //bindReclerviewChat(list,i)
            if(list.size > 0) {
                setChatView(View.VISIBLE,View.GONE)

                bindReclerviewChat(list, i)
            }
            else{
                setChatView(View.GONE,View.VISIBLE)
            }

        }
        catch (e : NullPointerException)
        {
            e.printStackTrace()
        }
    }

    fun String.toDate(): Date{
        return SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).parse(this)
    }

    private fun bindReclerviewChat(it: ArrayList<ChatNewList>, i: Int) {
        val recyclerLayoutManager = LinearLayoutManager(context)
        rvchats.layoutManager = recyclerLayoutManager
        rvchats.itemAnimator = DefaultItemAnimator()
        val adapter = BuyChatAdapter(it, i, context!!)
        rvchats.adapter = adapter
        adapter.setClicklistner(this)
        adapter.notifyDataSetChanged()
    }

    override fun itemclick(bean: ChatNewList) {

       // val sender_ref = FirebaseDatabase.getInstance().getReference("/List/").child("$luserid").child("$luserid-${bean.proId}").child("counter").setValue("0")
       // val sender_ref = FirebaseDatabase.getInstance().getReference("/List/").child("$luserid").child("${bean.id}-$luserid-${bean.proId}").child("counter").setValue("0")
        setChatCounterZero(bean)
        val intent = Intent(activity, ChatDetailActivity::class.java)
        intent.putExtra("userid", bean.id.toString())
        intent.putExtra("postid", bean.proId.toString())
        intent.putExtra("sendername",lusername)
        intent.putExtra("recivername",bean.name)
        intent.putExtra("reciverImage",bean.userProfile)
        intent.putExtra("senderImage",luserprofile)
        intent.putExtra("productImg",bean.image)
        intent.putExtra("sub_cat_name",bean.subCatName)
        intent.putExtra("chatType",bean.chattype.toString())
        intent.putExtra("sub_cat_id",bean.subCategoryID.toString())
        intent.putExtra("cat_id",bean.categoryID.toString())
        intent.putExtra("adTitle",bean.adTitle.toString())
        intent.putExtra("price",bean.price.toString())
        intent.putExtra("deleteDate", bean.deleteDate.toString())
        intent.putExtra("from","all")

        /*  intent.putExtra("userid", bean.id.toString())
          intent.putExtra("postid", bean.proId.toString())
          intent.putExtra("sendername",bean.name)
          intent.putExtra("recivername", storeUserData.getString(Constants.USER_NAME))
          intent.putExtra("reciverImage",storeUserData.getString(Constants.PROFILE))
          intent.putExtra("senderImage",bean.userProfile)
          intent.putExtra("productImg",bean.image)
          intent.putExtra("sub_cat_name",bean.subCatName)
          intent.putExtra("chatType",bean.chattype.toString())*/
        startActivity(intent)
    }

    private fun setChatCounterZero(bean: ChatNewList) {
        Log.d("TAG", "counter" + bean.counter.toInt())
        if(bean.counter != null && bean.counter.toInt() > 0) {
            FirebaseDatabase.getInstance().getReference("/List/").child("$luserid").child("${bean.id}-$luserid-${bean.proId}").child("counter").setValue("0")
        }
    }

    override fun itemdelete(position: Int, type: String) {
        if(type.equals("add"))
        {
            tv_deleteb.visibility = View.VISIBLE
            listid.add(position)
        }
        else{

            listid.remove(position)
        }

        if(listid.size > 0)
        {
            tv_deleteb.visibility = View.VISIBLE
        }
        else{
            tv_deleteb.visibility = View.GONE
        }
    }

    override fun itemdeleteAuto(position: ChatNewList, s: String) {
        Utils.showLog(AllChatFragment.TAG, "==AUTO delete call===" + luserid + "===" + position.id)
        FirebaseDatabase.getInstance().getReference("/List/").child("$luserid").child("${position.id}-$luserid-${position.proId}").removeValue()

        if (position.id.toInt() < luserid!!.toInt()) {
            FirebaseDatabase.getInstance().getReference("$luserid").child("${position.id}-$luserid").removeValue()
        } else {
            FirebaseDatabase.getInstance().getReference("$luserid").child("$luserid-${position.id}").removeValue()
        }
        (activity as ChatActivity).loadAllTabAgain()
    }

    override fun showInfoDialog() {
        dialog = Dialog(context!!)
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_info_chat_delete)
        // Set dialog title
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        // set values for custom dialog components - text, image and button
        val ivclose = dialog.findViewById<ImageView>(R.id.ivclose)
        ivclose.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (method.equals("GetUser")) {

            val data = da.getJSONObject("result")
            lusername = data.getString("username")
            luserprofile = data.getString("profilePicture")
        }
    }

    override fun onFail(msg: String, method: String) {

        lusername = "GL User"
        luserprofile = ""
    }

    override fun onResume() {
        super.onResume()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun performEdit(type : Int) {
        refreshRecyclerViewMessages(type)
        if(type == 1)
        {
            tv_deleteb.visibility = View.GONE
        }
    }

    fun performSearch(query: String) {

        if(query.toString().equals("clear"))
        {
            bindReclerviewChat(list, 1)
        }
        else {
            val temp: MutableList<ChatNewList> = ArrayList()
            for (d in list) {
                //or use .equal(text) with you want equal match
                //use .toLowerCase() for better matches
                if (d.name.contains(query)) {
                    temp.add(d)
                }
            }
            //update recyclerview

            bindReclerviewChat(temp as ArrayList<ChatNewList>, 1)
        }
    }

    private fun openDeleteDialog() {
        dialog = Dialog(context!!)
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_logout)
        // Set dialog title
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        //dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_white);
        // set values for custom dialog components - text, image and button

        val tvtitle = dialog.findViewById(R.id.tv_title) as TextView
        tvtitle.setText(resources.getText(R.string.txt_delete_warning))
        val btnyes = dialog.findViewById(R.id.btn_yes) as TextView
        val btnno = dialog.findViewById(R.id.btn_no) as TextView

        btnyes.setOnClickListener(View.OnClickListener {

          /*  for(i in listid.indices) {
                Utils.showLog(AllChatNewFragment.TAG,"===SELECTED ID===" + list.get(listid.get(i)).proId)
                FirebaseDatabase.getInstance().getReference("/List/").child("$luserid").child("$luserid-${list.get(listid.get(i)).proId}").removeValue()

                if (list.get(listid.get(i)).id.toInt() < luserid!!.toInt()) {
                    FirebaseDatabase.getInstance().getReference("$luserid").child("${list.get(listid.get(i)).id}-$luserid").removeValue()
                }
                else{
                    FirebaseDatabase.getInstance().getReference("$luserid").child("$luserid-${list.get(listid.get(i)).id}").removeValue()
                }

            }*/

            for(i in listid.indices) {
                Utils.showLog(AllChatFragment.TAG,"===SELECTED ID===" + list.get(listid.get(i)).proId)
                FirebaseDatabase.getInstance().getReference("/List/").child("$luserid").child("${list.get(listid.get(i)).id}-$luserid-${list.get(listid.get(i)).proId}").removeValue()

                if (list.get(listid.get(i)).id.toInt() < luserid!!.toInt()) {
                    FirebaseDatabase.getInstance().getReference("$luserid").child("${list.get(listid.get(i)).id}-$luserid").removeValue()
                }
                else{
                    FirebaseDatabase.getInstance().getReference("$luserid").child("$luserid-${list.get(listid.get(i)).id}").removeValue()
                }
            }

            tv_deleteb.visibility = View.GONE
            (activity as ChatActivity).loadBuyingTabAgain()

            dialog.dismiss()
        })

        btnno.setOnClickListener(View.OnClickListener { dialog.dismiss() })

        dialog.show()
    }

}

