package com.kaushlesh.view.fragment.MyAccount.MyNetwork

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.view.animation.TranslateAnimation
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kaushlesh.Controller.*
import com.kaushlesh.R
import com.kaushlesh.adapter.FollowDetailPostAdapter
import com.kaushlesh.bean.BusinessUserProfileBean
import com.kaushlesh.bean.FollowingUserDetailBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.reusable.EndlessRecyclerOnScrollListenerList
import com.kaushlesh.utils.CallTiming
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.PostDetail.CheckCategoryForPostDetails
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.Business.ViewBusinessProfileActivity
import com.kaushlesh.view.activity.Login.LoginMainActivity
import kotlinx.android.synthetic.main.activity_follower_profile_detail.*
import kotlinx.android.synthetic.main.fragment_following.*
import kotlinx.android.synthetic.main.item_my_post_favourite.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class FollowerProfileDetailActivity : AppCompatActivity(),View.OnClickListener,FollowDetailPostAdapter.ItemClickListener ,ParseControllerListener{

    val TAG = "FollowerProfileDetail"
    var profileId: String = ""
    lateinit var getListingController: GetListingController
    var dataModel: FollowingUserDetailBean = FollowingUserDetailBean()
    internal var plist: ArrayList<FollowingUserDetailBean.GetFPostBean> = ArrayList()
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    lateinit var storeUserData: StoreUserData
    var userId: String = ""
    var start = 0
    var limit:Int = 10
    private var progressStatus = 0
    private var handler: Handler = Handler()
    lateinit var progressBar: ProgressBar
    private var featureRcvScrollListener: EndlessRecyclerOnScrollListenerList? = null
    lateinit var rvMyPost : RecyclerView
    lateinit var adapter : FollowDetailPostAdapter
    lateinit var rlViewBusinessProfile: RelativeLayout

    private var date: Date? = null

    var langListBusiness: java.util.ArrayList<Int> = java.util.ArrayList()
    var langListWp: java.util.ArrayList<Int> = java.util.ArrayList()

    var langArray = arrayOf(
            "Monday", "Tuesday", "Wednesday", "Thursday",
            "Friday", "Saturday", "Sunday"
    )

    var contact: String = ""
    var whatsappNumber = ""
    var eMail = ""
    var otherUserBusinessProfileID = ""

    var isUp = false

    var isBusinessProfileCountCalled: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_follower_profile_detail)

        storeUserData=StoreUserData(this)

        getListingController = GetListingController(this, this)

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)
        progressBar = findViewById(R.id.preogressbar)
        rvMyPost = findViewById(R.id.rv_post)
        rlViewBusinessProfile = findViewById(R.id.rlViewBusinessProfile)


        //tvtoolbartitle.text = getString(R.string.txt_profile)

        btnback.setOnClickListener(this)
        rlViewBusinessProfile.setOnClickListener(this)

        btn_follow.setOnClickListener(this)
        btn_start_post_now.setOnClickListener(this)

        userId = storeUserData.getString(Constants.USER_ID)

        if (intent != null) {
            profileId = intent.getStringExtra("profileId").toString()
            Utils.showLog(TAG, "==profile id==" + profileId)
        }

        try {
            Utils.showProgress(this)
        }catch (e:java.lang.Exception){
            e.printStackTrace()
        }

        getListingController.getFollowingUserDetail(profileId.toString(), start)

        if(userId != null && userId.length >0) {

            if (userId.toInt() == profileId.toInt()) {
                btn_follow.visibility = View.GONE
            } else {
                btn_follow.visibility = View.VISIBLE
            }
        }

        rvMyPost.itemAnimator = null
        rvMyPost.getRecycledViewPool().clear()
        val layoutManager = GridLayoutManager(this, 2)
        rvMyPost.setLayoutManager(layoutManager)
        adapter = FollowDetailPostAdapter(this, arrayListOf())
        adapter.setClicklistner(this)
        rvMyPost.adapter = adapter

        featureRcvScrollListener = object : EndlessRecyclerOnScrollListenerList()
        {
            override fun onLoadMore(page: Int, totalItemsCount: Int) {
                Log.e("Pagination", "onLoadMore Invocke : ${page}")
                val marginLayoutParams = RelativeLayout.LayoutParams(rvMyPost.getLayoutParams())
                //val marginLayoutParams = ViewGroup.MarginLayoutParams(rcvListView.getLayoutParams())
                marginLayoutParams.setMargins(0, 0, 0, 130)
                rvMyPost.setLayoutParams(marginLayoutParams)
                progressBar.setVisibility(View.VISIBLE)
                Thread {
                    while (progressStatus < 100) {
                        progressStatus +=10
                        handler.post(Runnable {
                            progressBar.progress = progressStatus
                        })
                        try {
                            // Sleep for 200 milliseconds.
                            Thread.sleep(100)
                        } catch (e: InterruptedException) {
                            e.printStackTrace()
                        }
                    }
                }.start()
                Utils.showLog(TAG, "====page===" + page)
                Utils.showLog(TAG, "=== page multiple===" + page * 20)
                getListingController.getFollowingUserDetail(profileId.toString(), page * 20)
                //getListingController.getHomePostList(locationName.toString(), loc_id, latitude.toString(), longitude.toString(), searchword, page * 20)
            }
        }

        rvMyPost.addOnScrollListener(featureRcvScrollListener!!)

        rlBusinessInquiries.setOnClickListener{
            if (AppConstants.checkUserIsLoggedin(this)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(arrayOf(Manifest.permission.CALL_PHONE), 200)
                }
            } else {
                val intent = Intent(this, LoginMainActivity::class.java)
                AppConstants.alertLogin(this, getString(R.string.txt_login_to_call), intent)
            }
        }

        rlWhatsappInquiries.setOnClickListener{

            var appPackage = ""
            if (isAppInstalled(this, "com.whatsapp")) {
                appPackage = "com.whatsapp"
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse("http://api.whatsapp.com/send?phone=+91" + whatsappNumber + "&text=")
                startActivity(intent)
            }else if (isAppInstalled(this, "com.whatsapp.w4b")) {
                appPackage = "com.whatsapp.w4b"
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse("http://api.whatsapp.com/send?phone=+91" + whatsappNumber + "&text=")
                startActivity(intent)
            } else {
                Utils.showToast(this, "Whatsapp not installed on your device")
            }
        }

        rlEmail.setOnClickListener{
            val selectorIntent = Intent(Intent.ACTION_SENDTO)
            selectorIntent.data = Uri.parse("mailto:")

            val emailIntent = Intent(Intent.ACTION_SEND)
            emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(eMail))
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "")
            emailIntent.putExtra(Intent.EXTRA_TEXT, "")
            emailIntent.selector = selectorIntent

            startActivity(Intent.createChooser(emailIntent, "Send email..."))
        }

        //hide/show & up/down views
        isUp = true;
        llMainView.visibility = View.VISIBLE


        //share profile
        shareUserProfile.setOnClickListener{
            if(userId.length >0) {
                if (userId.toInt() == profileId.toInt()) {
                    Utils.shareMyProfileLink("profile",userId, this);
                } else {
                    Utils.shareOtherUserProfileLink("profile",profileId, this);
                }
            }
        }

    }


    fun slideUp(view: View) {
        view.visibility = View.VISIBLE
        val animate = TranslateAnimation(
                0F,  // fromXDelta
                0F,  // toXDelta
                view.height.toFloat(),  // fromYDelta
                0F) // toYDelta
        animate.setDuration(500)
        animate.setFillAfter(true)
        view.startAnimation(animate)
    }

    // slide the view from its current position to below itself
    fun slideDown(view: View) {
        val animate = TranslateAnimation(
                0F,  // fromXDelta
                0F,  // toXDelta
                0F,  // fromYDelta
                view.height.toFloat()) // toYDelta
        animate.setDuration(500)
        animate.setFillAfter(true)
        view.startAnimation(animate)
        view.visibility = View.GONE

    }

    fun onSlideViewButtonClick(view: View?) {
        if (isUp) {
            slideDown(llMainView)
            //myButton.setText("Slide up")
            imgUpDown.setImageResource(R.drawable.ic_arrow_drop_down_black_24dp)
        } else {
            slideUp(llMainView)
            imgUpDown.setImageResource(R.drawable.ic_arrow_drop_up_black_24dp)
            //myButton.setText("Slide down")
        }
        isUp = !isUp
    }


    private fun isAppInstalled(ctx: Context, packageName: String): Boolean {
        val pm: PackageManager = ctx.getPackageManager()
        val app_installed: Boolean
        app_installed = try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES)
            true
        } catch (e: PackageManager.NameNotFoundException) {
            false
        }
        return app_installed
    }

    private fun getUserBusinessProfile(){
        getListingController = GetListingController(this, this)
        getListingController.getUserBusinessProfile(otherUserBusinessProfileID)
    }

    private fun bindRecyclerview() {
        val layoutManager = GridLayoutManager(this, 2)
        rv_post.layoutManager = layoutManager
        val adapter = FollowDetailPostAdapter(this, plist)
        adapter.setClicklistner(this)
        rv_post.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_back -> {

                onBackPressed()
            }

            R.id.rlViewBusinessProfile -> {
                val intent = Intent(this@FollowerProfileDetailActivity, ViewBusinessProfileActivity::class.java)
                intent.putExtra("businessProfileID", otherUserBusinessProfileID)
                startActivity(intent)
            }

            R.id.btn_follow -> {

                if (AppConstants.checkUserIsLoggedin(this)) {
                    if (btn_follow.text.equals(resources.getString(R.string.txt_follow))) {
                        val c = FollowUnfollowController(this, profileId, this)
                        c.addToFollow()
                    } else {
                        val c = FollowUnfollowController(this, profileId, this)
                        c.removeFromFollow()
                    }
                } else {
                    val intent = Intent(applicationContext, LoginMainActivity::class.java)
                    AppConstants.alertLogin(this, getString(R.string.txt_login_to_follow), intent)
                }

            }

            R.id.btn_start_post_now -> {
                val screenRedirectionController = ScreenRedirectionController(this)
                screenRedirectionController.openAdPostTab()
            }
        }
    }






    override fun itemclickProduct(bean: FollowingUserDetailBean.GetFPostBean) {
        val storeusedata = StoreUserData(this)
        storeusedata.setString(Constants.OPEN_POST_DETAIL, "my_post_ad")
        CheckCategoryForPostDetails.OpenScreen(this, bean.category_id!!.toInt(), bean.sub_category_id!!.toInt(), bean.post_id!!.toInt())
    }

    override fun itemRemove(bean: Int) {
        //Utils.showToast(this, "clicked")

        if(AppConstants.checkUserIsLoggedin(this))
        {
            val c = AddFavPostController(this, this, plist.get(bean).post_id.toString())
            c.onClick(iv_like)
        }
        else{
            val intent = Intent(applicationContext, LoginMainActivity::class.java)
            AppConstants.alertLogin(this, getString(R.string.txt_login_ad_fav), intent)
        }
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        try {
            Utils.dismissProgress()
        }catch (e: Exception){
            e.printStackTrace()
        }

        if(method.equals("GetFollowingUser")) {
            Utils.dismissProgress()
            progressBar.setVisibility(View.GONE)
            progressStatus = 0
            val marginLayoutParams = RelativeLayout.LayoutParams(rvMyPost.getLayoutParams())
            marginLayoutParams.setMargins(0, 0, 0, 0)
            rvMyPost.setLayoutParams(marginLayoutParams)

            plist.clear()

            dataModel.FollowingUserDetailBean(da)

            plist = dataModel.productlist
            Utils.showLog(TAG, "==product list size==" + plist.size)

            setview()
        }

        if (method.equals("adFavPost")) {
            Utils.showToast(this, message)

            plist.clear()
            adapter.removeItems()
            featureRcvScrollListener?.reset()
            getListingController.getFollowingUserDetail(profileId.toString(), 0)
        }

        if(method.equals("addFollow"))
        {
            btn_follow.text = resources.getString(R.string.txt_unfollow)

            plist.clear()
            adapter.removeItems()
            getListingController.getFollowingUserDetail(profileId.toString(), 0)
        }
        if(method.equals("removeFollow"))
        {
            btn_follow.text = resources.getString(R.string.txt_follow)

            plist.clear()
            adapter.removeItems()
            getListingController.getFollowingUserDetail(profileId.toString(), 0)
        }

        if(method.equals("getBusinessProfile")){

            val userModel = BusinessUserProfileBean()
            userModel.businessUserProfileBean(da)

            setBusinessData(userModel)

        }


    }

    override fun onFail(msg: String, method: String) {
        if(method.equals("GetFollowingUser")) {
            rv_post.visibility = View.GONE
            ll_no_post.visibility = View.VISIBLE
        }

        if (method.equals("adFavPost")) {
            Utils.showToast(this, msg)

            plist.clear()
            adapter.removeItems()
            getListingController.getFollowingUserDetail(profileId.toString(), 0)
        }

        if(method.equals("addFollow") ||method.equals("removeFollow") )
        {
            Utils.showToast(this, msg)
        }
        if(method.equals("GetFollowingUser"))
        {
            if(adapter.itemCount > 0)
            {
                progressBar.visibility = View.GONE
            }
            else{

                progressBar.visibility = View.GONE
                //rlnopost.visibility = View.VISIBLE
                adapter.removeItems()
                rv_post.visibility = View.GONE
                ll_no_post.visibility = View.VISIBLE
            }
        }
    }

    private fun setBusinessData(userModel: BusinessUserProfileBean) {

        if(userModel.is_verified!=null){

            if(userModel.is_verified == "1"){
                llBusinessProfileData.visibility = View.VISIBLE
            }else {
                llBusinessProfileData.visibility = View.GONE
            }
        }

        if(userModel.profile_counter!=null){
            if(userModel.profile_counter.toString() == "1"){
                profileCounterText.setText("Profile Visited : " + userModel.profile_counter.toString() + " View")
            }else{
                profileCounterText.setText("Profile Visited : " + userModel.profile_counter.toString() + " Views")
            }
        }


        //Business call
        if(userModel.is_calls_allow.toString() == "1"){
            rlBusinessInquiries.visibility = View.VISIBLE

            //Business call
            if(!userModel.bussiness_call_timing.isEmpty()){
                compareDatesBusiness(userModel.bussiness_call_timing)
            }

        }else{
            rlBusinessInquiries.visibility = View.GONE
        }

        if(userModel.mobile_no!=null){
            contact = userModel.mobile_no.toString()
        }

        //Whatsapp Call
        if(userModel.is_whatsapp_calls_allow.toString() == "1"){
            rlWhatsappInquiries.visibility = View.VISIBLE

            //Whatsapp call
            if(!userModel.whatsapp_inquiry_call_timing.isEmpty()){
                compareDatesWhatsapp(userModel.whatsapp_inquiry_call_timing)
            }else{
                rlWhatsappInquiries.visibility = View.GONE
            }

        }else{
            rlWhatsappInquiries.visibility = View.GONE
        }

        if(userModel.whatsapp_no!=null){
            whatsappNumber = userModel.whatsapp_no.toString()
        }


        if(userModel.email!=null){
            eMail = userModel.email.toString()
        }



    }

    private fun compareDatesBusiness(bussinessCallTiming: java.util.ArrayList<CallTiming>) {

        langListBusiness.clear()

        val businessOpenTime = bussinessCallTiming.get(0).openTime
        val businessCloseTime = bussinessCallTiming.get(0).closeTime


        val now: Calendar = Calendar.getInstance()
        val hour: Int = now.get(Calendar.HOUR_OF_DAY)
        val minute: Int = now.get(Calendar.MINUTE)

        var dateCompareOne: Date? = null
        var dateCompareTwo: Date? = null

        //current weekday
        var date: Date? = now.getTime()
        val weekday: String = SimpleDateFormat("EEEE", Locale.ENGLISH).format(date!!.time)


        for (i in 0 until bussinessCallTiming.size) {
            for (j in 1 until langArray.size + 1){
                if(bussinessCallTiming.get(i).dayId == j){
                    langListBusiness.add(j - 1)
                }
            }
        }


        val stringBuilder = StringBuilder()
        for (j in langListBusiness.indices) {

            stringBuilder.append(langArray[langListBusiness[j]])

            if (j != langListBusiness.size - 1) {
                stringBuilder.append(", ")
            }
        }

        Log.d("ComparedDate", "STRING : " + stringBuilder)


        if(stringBuilder.toString().contains(weekday)){
            try {
                date = Utils.parseDate("$hour:$minute")
                dateCompareOne = Utils.parseDate(businessOpenTime)
                dateCompareTwo = Utils.parseDate(businessCloseTime)

                Log.d("ComparedDate", "OPEN : " + businessOpenTime + "CLOSE : " + businessCloseTime)
                Log.d("ComparedDate", "ONE : " + dateCompareOne + "TWO : " + dateCompareTwo)



                if (dateCompareOne!!.before(date) && dateCompareTwo!!.after(date)) {
                    Log.d("ComparedDate", "Time Matched")
                    rlBusinessInquiries.visibility = View.VISIBLE

                }else{
                    Log.d("ComparedDate", "Time Not Matched")
                    rlBusinessInquiries.visibility = View.GONE

                }

            }catch (e: Exception){
                e.printStackTrace()
            }
        }else{

            for (i in 0 until bussinessCallTiming.size) {
                Log.d("ComparedDate", "" + bussinessCallTiming.get(i).dayId)
                if(bussinessCallTiming.get(i).dayId.toString() == "8"){
                    Log.d("ComparedDate", "All Days")
                    try {
                        date = Utils.parseDate("$hour:$minute")
                        dateCompareOne = Utils.parseDate(businessOpenTime)
                        dateCompareTwo = Utils.parseDate(businessCloseTime)
                        if (dateCompareOne!!.before(date) && dateCompareTwo!!.after(date)) {

                            Log.d("ComparedDate", "Time Matched")
                            rlBusinessInquiries.visibility = View.VISIBLE

                        }else{
                            Log.d("ComparedDate", "Time Not Matched")
                            rlBusinessInquiries.visibility = View.GONE
                        }

                    }catch (e: Exception){
                        e.printStackTrace()
                    }
                }else{
                    Log.d("ComparedDate", "ELSE....")

                    rlBusinessInquiries.visibility = View.GONE

                }
            }

        }


    }

    private fun compareDatesWhatsapp(whatsappCallTiming: java.util.ArrayList<CallTiming>) {

        langListWp.clear()

        val whatsappOpenTime = whatsappCallTiming.get(0).openTime
        val whatsappCloseTime = whatsappCallTiming.get(0).closeTime


        val now: Calendar = Calendar.getInstance()
        val hour: Int = now.get(Calendar.HOUR_OF_DAY)
        val minute: Int = now.get(Calendar.MINUTE)

        var dateCompareOne: Date? = null
        var dateCompareTwo: Date? = null

        //current weekday
        var date: Date? = now.getTime()
        val weekday: String = SimpleDateFormat("EEEE", Locale.ENGLISH).format(date!!.time)


        for (i in 0 until whatsappCallTiming.size) {
            for (j in 1 until langArray.size + 1){
                if(whatsappCallTiming.get(i).dayId == j){
                    langListWp.add(j - 1)
                }
            }
        }


        val stringBuilder = StringBuilder()
        for (j in langListWp.indices) {

            stringBuilder.append(langArray[langListWp[j]])

            if (j != langListWp.size - 1) {
                stringBuilder.append(", ")
            }
        }

        Log.d("ComparedDate", "STRING : " + stringBuilder)


        if(stringBuilder.toString().contains(weekday)){
            try {
                date = Utils.parseDate("$hour:$minute")
                dateCompareOne = Utils.parseDate(whatsappOpenTime)
                dateCompareTwo = Utils.parseDate(whatsappCloseTime)

                Log.d("ComparedDate", "OPEN : " + whatsappOpenTime + "CLOSE : " + whatsappCloseTime)
                Log.d("ComparedDate", "ONE : " + dateCompareOne + "TWO : " + dateCompareTwo)

                if (dateCompareOne!!.before(date) && dateCompareTwo!!.after(date)) {

                    Log.d("ComparedDate", "Time Matched")
                    rlWhatsappInquiries.visibility = View.VISIBLE

                }else{

                    Log.d("ComparedDate", "Time Not Matched")
                    rlWhatsappInquiries.visibility = View.GONE

                }

            }catch (e: Exception){
                e.printStackTrace()
            }
        }else{

            for (i in 0 until whatsappCallTiming.size) {
                Log.d("ComparedDate", "" + whatsappCallTiming.get(i).dayId)
                if(whatsappCallTiming.get(i).dayId.toString() == "8"){
                    Log.d("ComparedDate", "All Days")
                    try {
                        date = Utils.parseDate("$hour:$minute")
                        dateCompareOne = Utils.parseDate(whatsappOpenTime)
                        dateCompareTwo = Utils.parseDate(whatsappCloseTime)
                        if (dateCompareOne!!.before(date) && dateCompareTwo!!.after(date)) {

                            Log.d("ComparedDate", "Time Matched")
                            rlWhatsappInquiries.visibility = View.VISIBLE

                        }else{

                            Log.d("ComparedDate", "Time Not Matched")
                            rlWhatsappInquiries.visibility = View.GONE




                        }

                    }catch (e: Exception){
                        e.printStackTrace()
                    }
                }else{
                    Log.d("ComparedDate", "ELSE....")

                    rlWhatsappInquiries.visibility = View.GONE

                }
            }

        }


    }

    private fun setview() {

        //original
        //tv_name.text = dataModel.username

        //new
        tvtoolbartitle.text = dataModel.username + "'s Profile"


        tv_date.text = getString(R.string.txt_member_since)+" " + Utils.formateDateFromstring("yyyy-MM-dd", "dd-MM-yyyy", dataModel.signupDate.toString())
        tv_followers.text = dataModel.followercount
        tv_followings.text = dataModel.followingcount

        if(dataModel.bussinessprofile_id!=null){
            otherUserBusinessProfileID = dataModel.bussinessprofile_id.toString()
        }

        if(dataModel.aboutInfo != null && dataModel.aboutInfo.toString().length > 0)
        {
            tv_about_info.visibility = View.VISIBLE
            tv_about_info.text = dataModel.aboutInfo
        }
        else{
            tv_about_info.visibility = View.GONE
        }

        try {
            Glide.with(this)
                    .load(dataModel.profile).placeholder(R.drawable.ic_user_pic)
                    .into(civ_profile)
        }
        catch (e: IllegalArgumentException)
        {
            e.printStackTrace()
        }

        if(dataModel.isfollowing.toString().toInt() == 0)
        {
            btn_follow.text = resources.getString(R.string.txt_follow)
        }
        else{
            btn_follow.text = resources.getString(R.string.txt_unfollow)
        }

      /*  if(plist.size > 0)
        {
            rv_post.visibility = View.VISIBLE
            ll_no_post.visibility = View.GONE
            bindRecyclerview()
        }
        else{
           rv_post.visibility = View.GONE
           ll_no_post.visibility = View.VISIBLE
       }*/

        if(!plist.isEmpty() && plist.size >0)
        {
            Handler(Looper.getMainLooper()).postDelayed({
                rvMyPost.visibility = View.VISIBLE
                ll_no_post.visibility = View.GONE
                adapter.addItems(plist)
                rvMyPost.setItemViewCacheSize(adapter.itemCount)
            }, 100)

            Log.e(TAG, "Item : ${dataModel.productlist.size} RcvItemCount : ${adapter.itemCount}")
        }

        getUserBusinessProfile()

        businessProfileViewCount()


    }

    private fun businessProfileViewCount() {

        if(!isBusinessProfileCountCalled){
            if(otherUserBusinessProfileID!=""){
                isBusinessProfileCountCalled = true
                BusinessProfileCountController(this,otherUserBusinessProfileID)
            }
        }



    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == 200) {
            val result = checkCallingOrSelfPermission(Manifest.permission.CALL_PHONE)
            if (result == PackageManager.PERMISSION_GRANTED) {

                val c = PhoneCallController(this, contact)
                c.makePhoneCall()
            } else {
                Utils.showToast(this, resources.getString(R.string.txt_phone_permission))
            }
        }
    }

}