package com.kaushlesh.view.fragment.MyAccount.MyAds

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.kaushlesh.R
import kotlinx.android.synthetic.main.activity_know_more_detail.*
import android.content.Intent
import com.facebook.appevents.codeless.internal.ViewHierarchy.setOnClickListener
import android.text.method.LinkMovementMethod
import android.net.Uri
import android.text.*
import android.text.style.ClickableSpan
import android.widget.Toast
import com.kaushlesh.Controller.MyPostShowcaseDeleteController
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Utils
import com.kaushlesh.widgets.CustomTextView
import kotlinx.android.synthetic.main.activity_login_main.*
import org.json.JSONObject

class KnowMoreDetailPostActivity : AppCompatActivity() , View.OnClickListener,ParseControllerListener{

    val TAG = "KnowMoreDetailPostActivity"
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    var postid : Int ?= null
    private lateinit var myPostShowcaseDeleteController: MyPostShowcaseDeleteController


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_know_more_detail)

        myPostShowcaseDeleteController = MyPostShowcaseDeleteController(this,this)


        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)
        tvtoolbartitle.text = getString(R.string.txt_know_more)

        if(intent != null)
        {
            postid = intent.getStringExtra("postId").toString().toInt()
        }
        Utils.showLog(TAG,"===post id===" + postid)

        btnback.setOnClickListener(this)
        btn_delete_ad.setOnClickListener(this)

        try {
            tvtnc.setAllCaps(false);
            tvtnc.makeLinks(
                    Pair(getString(R.string.txt_terms_of_use_know_more), View.OnClickListener {
                        val browserIntent = Intent(Intent.ACTION_VIEW)
                        browserIntent.data = Uri.parse("https://gujaratliving.com/terms-conditions")
                        startActivity(browserIntent)
                    }),
                    Pair(getString(R.string.txt_privacy_policy_know_more), View.OnClickListener {
                        val browserIntent = Intent(Intent.ACTION_VIEW)
                        browserIntent.data = Uri.parse("https://gujaratliving.com/privacy-policy")
                        startActivity(browserIntent)
                    }))
        }catch (e: Exception){
            e.printStackTrace()
        }


    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btn_back -> onBackPressed()

            R.id.btn_delete_ad ->
            {
                myPostShowcaseDeleteController.add("post",postid)
            }
        }
    }

    fun TextView.makeLinks(vararg links: Pair<String, View.OnClickListener>) {
        val spannableString = SpannableString(this.text)
        var startIndexOfLink = -1
        for (link in links) {
            val clickableSpan = object : ClickableSpan() {
                override fun updateDrawState(textPaint: TextPaint) {
                    // use this to change the link color
                   // textPaint.color = textPaint.linkColor
                    textPaint.color = resources.getColor(R.color.blue_color)
                    // toggle below value to enable/disable
                    // the underline shown below the clickable text
                    textPaint.isUnderlineText = true
                }

                override fun onClick(view: View) {
                    Selection.setSelection((view as TextView).text as Spannable, 0)
                    view.invalidate()
                    link.second.onClick(view)
                }
            }
            startIndexOfLink = this.text.toString().indexOf(link.first, startIndexOfLink + 1)
//      if(startIndexOfLink == -1) continue // todo if you want to verify your texts contains links text
            spannableString.setSpan(
                clickableSpan, startIndexOfLink, startIndexOfLink + link.first.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
        this.movementMethod =
            LinkMovementMethod.getInstance() // without LinkMovementMethod, link can not click
        this.setText(spannableString, TextView.BufferType.SPANNABLE)
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        Utils.dismissProgress()
        Utils.showToast(this,message)
        onBackPressed()
    }

    override fun onFail(msg: String, method: String) {
        Utils.dismissProgress()
        Utils.showToast(this,msg)
    }
}
