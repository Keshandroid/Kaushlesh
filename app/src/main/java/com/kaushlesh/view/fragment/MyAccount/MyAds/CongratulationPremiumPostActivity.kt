package com.kaushlesh.view.fragment.MyAccount.MyAds

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.gson.GsonBuilder
import com.kaushlesh.R
import com.kaushlesh.bean.AdPremiunPostBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.Utils
import kotlinx.android.synthetic.main.activity_congratulation_adpost.btn_back1
import kotlinx.android.synthetic.main.activity_congratulation_premium_post.*
import org.json.JSONObject


class CongratulationPremiumPostActivity : AppCompatActivity() {

    private lateinit var dataModel: AdPremiunPostBean
    private lateinit var pkgdata: JSONObject
    val TAG  = "CongratulationPremiumPostActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_congratulation_premium_post)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        if(intent.extras != null)
        {
            val data = intent.getStringExtra("data").toString()
            pkgdata = JSONObject(data)

            dataModel = AdPremiunPostBean()
            dataModel.AdPremiunPostBean(pkgdata)

            Utils.showLog(TAG, "==data=" + GsonBuilder().setPrettyPrinting().create().toJson(pkgdata))
            setdata()
        }

        btn_back1.setOnClickListener {
            val intent = Intent(applicationContext, MyPostActivity::class.java)
            intent.putExtra("from","attention")
            startActivity(intent)
            finish()
        }
    }

    private fun setdata() {


         //   tv_details.text = dataModel.noofad + " Ads for " + dataModel.duration + " days"
            tv_details.text = dataModel.name
            tvpuchase.text = dataModel.noofad
            tvused.text = dataModel.remainingpost
            tvavailable.text = (dataModel.noofad!!.toInt() - dataModel.remainingpost!!.toInt()).toString()

            val catname1 = Constants.getCategoryFromId(dataModel.catid!!.toInt(),this)
            tv_category1.text = catname1


            val data = intent.getStringExtra("isfrom").toString()
            if(data.equals("exphide"))
            {
                tv_date_exp.text = "-"
            }
            else{
                val date_after = Utils.formateDateFromstring("dd-MM-yyyy", "dd/MM/yyyy", dataModel.expdate.toString())
                AppConstants.printLog("TAG", "==FORMATED DATE===$date_after")
                tv_date_exp.text = date_after
            }
    }


    override fun onBackPressed() {
        val intent = Intent(applicationContext, MyPostActivity::class.java)
        intent.putExtra("from","attention")
        startActivity(intent)
        super.onBackPressed()
    }
}