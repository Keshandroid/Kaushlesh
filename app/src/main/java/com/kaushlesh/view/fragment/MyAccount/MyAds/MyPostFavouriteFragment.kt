package com.kaushlesh.view.fragment.MyAccount.MyAds


import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.Controller.RemoveFavPostController
import com.kaushlesh.R
import com.kaushlesh.adapter.MyFavouritepostAdapter
import com.kaushlesh.bean.GetFavPostBean
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.reusable.EndlessRecyclerOnScrollListenerList
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.PostDetail.CheckCategoryForPostDetails
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.MainActivity
import kotlinx.android.synthetic.main.fragment_post_favourite.*
import kotlinx.android.synthetic.main.item_my_post_favourite.*
import org.json.JSONObject
import java.util.*


class MyPostFavouriteFragment : Fragment(), MyFavouritepostAdapter.ItemClickListener ,
    ParseControllerListener {

//    new added
    //private var featureRcvScrollListener: EndlessRecyclerOnScrollListenerList? = null
    lateinit var rv_mypost : RecyclerView
    lateinit var progressBar: ProgressBar
    private var progressStatus = 0
    private var handler: Handler = Handler()
    var start = 0
    var limit:Int = 10
    lateinit var adapter : MyFavouritepostAdapter
    private var isLoading = false
    private var nextPage = 0


    val list = ArrayList<GetFavPostBean.FavPostlist>()
    private lateinit var getListingController: GetListingController

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_post_favourite, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rv_mypost = view.findViewById(R.id.rv_mypost)
        progressBar = view.findViewById(R.id.preogressbar)

        //callApiToGetFavPostList(0)
      /*  if(list.size > 0) {
            ll_no_post_fav.visibility = View.GONE
            ll_no_showcase_fvrt.visibility = View.GONE
            bindRecyclerviewFuel()
        }
        else{
            ll_no_post_fav.visibility = View.VISIBLE
            ll_no_showcase_fvrt.visibility = View.GONE
        }*/

        //for no favourite
       /* ll_no_post_fav.visibility = View.VISIBLE
        ll_no_showcase_fvrt.visibility = View.GONE

        btn_start_explore_now.setOnClickListener {
            (activity as MyPostActivity).setAdPost()
        }
*/
        //bindRecyclerviewFuel()

        btn_start_explore_now.setOnClickListener {
            var storeUserData = StoreUserData(context!!)
            val from = storeUserData.getString(Constants.POST_FROM)
            if(from.equals("activity")) {
                (activity as MyPostActivity).setHomeScreen()
            }
            else{
                (activity as MainActivity).homeTabDisplay()
            }

        }



        //init recyclerview
        rv_mypost.itemAnimator = null
        rv_mypost.getRecycledViewPool().clear()
        val layoutManager = GridLayoutManager(context, 2)

        rv_mypost.layoutManager = layoutManager
        adapter = MyFavouritepostAdapter(context!!, list)
        adapter.setClicklistner(this)
        rv_mypost.adapter = adapter







        rv_mypost.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                var visibleItemCount = 0
                var totalItemCount = 0
                var pastVisibleItem = 0
                if (recyclerView.layoutManager != null) {
                    visibleItemCount = recyclerView.layoutManager!!.childCount
                    totalItemCount = recyclerView.layoutManager!!.itemCount
                    pastVisibleItem = (recyclerView.layoutManager as LinearLayoutManager?)!!.findFirstVisibleItemPosition()
                }

                if (!isLoading && dy > 0 && visibleItemCount + pastVisibleItem >= totalItemCount) {

                    progressBar.setVisibility(View.VISIBLE)
                    Thread {
                        while (progressStatus < 100) {
                            progressStatus += 10
                            handler.post(Runnable {
                                progressBar.progress = progressStatus
                            })
                            try {
                                // Sleep for 200 milliseconds.
                                Thread.sleep(100)
                            } catch (e: InterruptedException) {
                                e.printStackTrace()
                            }
                        }
                    }.start()


                    isLoading = true
                    nextPage = nextPage + 10
                    Utils.showLog(TAG, "====page===" + nextPage)

                    callApiToGetFavPostList(nextPage)


                }
            }
        })










        /*featureRcvScrollListener = object : EndlessRecyclerOnScrollListenerList() {
            override fun onLoadMore(page: Int, totalItemsCount: Int) {
                Log.e("MyPostFavouriteFragment", "onLoadMore Invocke : ${page}")
                val marginLayoutParams = LinearLayout.LayoutParams(rv_mypost.getLayoutParams())
                marginLayoutParams.setMargins(0, 0, 0, 130)
                rv_mypost.setLayoutParams(marginLayoutParams)
                progressBar.setVisibility(View.VISIBLE)
                Thread {
                    while (progressStatus < 100) {
                        progressStatus +=10
                        handler.post(Runnable {
                            progressBar.progress = progressStatus
                        })
                        try {
                            // Sleep for 200 milliseconds.
                            Thread.sleep(100)
                        } catch (e: InterruptedException) {
                            e.printStackTrace()
                        }
                    }
                }.start()
                Utils.showLog(TAG, "====page===" + page)
                Utils.showLog(TAG, "=== page multiple===" + page * 10)

                callApiToGetFavPostList(page * 10)
            }
        }

        rv_mypost.addOnScrollListener(featureRcvScrollListener!!)*/






    }

    private fun callApiToGetFavPostList(start: Int) {
        //Utils.showProgress(activity)
        getListingController = GetListingController(activity!!, this)
        getListingController.getMyFavPost(start, limit)
    }

    private fun bindRecyclerviewFuel() {

        val layoutManager = GridLayoutManager(context, 2)
        rv_mypost.layoutManager = layoutManager
        adapter = MyFavouritepostAdapter(context!!, list)
        adapter.setClicklistner(this)
        rv_mypost.adapter = adapter
        rv_mypost.getRecycledViewPool().clear()
        adapter.notifyDataSetChanged()
    }

    companion object {
        val TAG = "MyPostFavouriteFragment"

        fun newInstance(): MyPostFavouriteFragment {
            return MyPostFavouriteFragment()
        }
    }

    override fun itemclickProduct(bean: GetFavPostBean.FavPostlist) {
        val storeusedata = StoreUserData(context!!)
        storeusedata.setString(Constants.OPEN_POST_DETAIL, "my_post_fav")
        storeusedata.setString(Constants.CATEGORY_ID, bean.category_id.toString())
        storeusedata.setString(Constants.SUBCATEGORYID, bean.sub_category_id.toString())
        CheckCategoryForPostDetails.OpenScreen(context!!, bean.category_id!!.toInt(), bean.sub_category_id!!.toInt(), bean.post_id!!.toInt())
    }

    override fun itemRemove(bean: Int) {
        val c = RemoveFavPostController(this, activity!!, list[bean].post_id.toString())
        c.onClick(iv_like)
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (method.equals("getMyFavPost")) {

            Log.d(MyPostFavouriteFragment.TAG, "On success called...")
            isLoading = false

            //new added
            progressBar.setVisibility(View.GONE)
            progressStatus = 0

           /* val marginLayoutParams = LinearLayout.LayoutParams(rv_mypost.getLayoutParams())
            marginLayoutParams.setMargins(0, 0, 0, 0)
            rv_mypost.setLayoutParams(marginLayoutParams)*/

            //original
            /*list.clear()
            val dataModel = GetFavPostBean()
            dataModel.GetFavPostBean(da)
            list.addAll(dataModel.getFavPostlist)
            Utils.dismissProgress()
            checkResponse()*/

            //list.clear()
            val dataModel = GetFavPostBean()
            dataModel.GetFavPostBean(da)


            Log.e(MyPostFavouriteFragment.TAG, "Response Item Size: ${dataModel.getFavPostlist.size}")
            list.addAll(dataModel.getFavPostlist)
            if(!list.isEmpty() && list.size >0) {


                if(ll_no_post_fav!=null && ll_no_showcase_fvrt!=null){
                    Handler(Looper.getMainLooper()).postDelayed({
                        ll_no_post_fav.visibility = View.GONE
                        ll_no_showcase_fvrt.visibility = View.GONE
                        rlpostFav.visibility = View.VISIBLE
                        rv_mypost.visibility = View.VISIBLE

                        //adapter.addItems(list)

                        adapter.notifyDataSetChanged()

                        rv_mypost.setItemViewCacheSize(adapter.itemCount)

                    }, 100)

                }


                /*Log.e(
                        MyPostFavouriteFragment.TAG,
                        "Item : ${dataModel.getFavPostlist.size} RcvItemCount : ${adapter.itemCount}"
                )*/
            }
        }

        if (method.equals("removeFavPost")) {
            Utils.showToast(activity, message)
            list.clear()
            adapter.notifyDataSetChanged()

            //new added
            //featureRcvScrollListener?.reset()
            nextPage = 0
            callApiToGetFavPostList(0)
        }
    }

    override fun onFail(msg: String, method: String) {
        //Utils.dismissProgress()
        Log.d(MyPostFavouriteFragment.TAG, "On Failed called..." + msg)

        try {
            progressBar.setVisibility(View.GONE)
            progressStatus = 0
            if (method.equals("removeFavPost")) {
                ll_no_post_fav.visibility = View.GONE
                ll_no_showcase_fvrt.visibility = View.GONE
                rlpostFav.visibility = View.VISIBLE
            }else if(method.equals("getMyFavPost")){
                if (msg == "Favorite Post Not Found" && list.size==0){
                    ll_no_post_fav.visibility = View.VISIBLE
                    ll_no_showcase_fvrt.visibility = View.GONE
                    rlpostFav.visibility = View.GONE
                }else{
                    ll_no_post_fav.visibility = View.GONE
                    ll_no_showcase_fvrt.visibility = View.GONE
                    rlpostFav.visibility = View.VISIBLE
                }
            }else{
                ll_no_post_fav.visibility = View.VISIBLE
                ll_no_showcase_fvrt.visibility = View.GONE
            }


        }
        catch (e: IllegalStateException)
        {
            e.printStackTrace()
        }
        catch (e: NullPointerException)
        {
            e.printStackTrace()
        }
    }

    private fun checkResponse() {
        try {

            if (list.size > 0) {
                ll_no_post_fav.visibility = View.GONE
                ll_no_showcase_fvrt.visibility = View.GONE
                rv_mypost.visibility = View.VISIBLE
                bindRecyclerviewFuel()
            } else {
                ll_no_post_fav.visibility = View.VISIBLE
                ll_no_showcase_fvrt.visibility = View.GONE
                rv_mypost.visibility = View.GONE
            }
        }
        catch (e: IllegalStateException)
        {
            e.printStackTrace()
        }
        catch (e: NullPointerException)
        {
            e.printStackTrace()
        }
    }

    override fun onResume() {
        //list.clear()

        //new added
        //featureRcvScrollListener?.reset()


        val storeusedata = StoreUserData(context!!)

        Log.d("OPEN_POST_DETAIL",""+storeusedata.getString(Constants.OPEN_POST_DETAIL))

        if(storeusedata.getString(Constants.OPEN_POST_DETAIL) == "my_post_fav"){
            nextPage = 0
            list.clear()
            callApiToGetFavPostList(0)
        }else{
            nextPage = 0
            storeusedata.setString(Constants.OPEN_POST_DETAIL, "opened")
            callApiToGetFavPostList(0)
        }

        //original
        //callApiToGetFavPostList(0)
        super.onResume()
    }

    /*override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            // Refresh your fragment here
            fragmentManager!!.beginTransaction().detach(this).attach(this).commit()
        }
    }*/
}