package com.kaushlesh.view.fragment.MyAccount.MyAds

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.GsonBuilder
import com.kaushlesh.Controller.AdPremiumToPostController
import com.kaushlesh.R
import com.kaushlesh.adapter.AdPostSelectPackageAdapter
import com.kaushlesh.bean.GetMyPackagesBean
import com.kaushlesh.bean.GetMyPostBean
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.fragment.MyAccount.PurchasePackage.AdPostPackagesActivity
import kotlinx.android.synthetic.main.activity_select_premium_package.*
import kotlinx.android.synthetic.main.activity_select_premium_package.btn_ad_post
import org.json.JSONObject

class SelectPremiumPackageActivity : AppCompatActivity(),View.OnClickListener ,AdPostSelectPackageAdapter.ItemClickListener,
    ParseControllerListener {
    private val TAG = "SelectPremiumPackageActivity"
    lateinit var storeUserData: StoreUserData
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal var pkglist: ArrayList<GetMyPackagesBean.Packages> = ArrayList()
    internal var packageid: String? = ""
    internal var postId: String? = ""
    var catId : String = ""
    var adcatdata : GetMyPostBean.getMyPostList ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_premium_package)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
        }

        storeUserData = StoreUserData(this)
        pkglist.clear()

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)
        tvtoolbartitle.text = resources.getString(R.string.txt_ad_post_select_premium_pkg)

        initBindViews()

        if(intent != null)
        {
            postId = intent.getStringExtra("postId")
            catId = intent.getStringExtra("catId").toString()
            Utils.showLog(TAG,"==cat id==" + catId + "==post id==" + postId)

            adcatdata = intent.getSerializableExtra("data") as GetMyPostBean.getMyPostList

            storeUserData.setString(Constants.POST_ID, postId.toString())
        }
        val extras = intent.extras
        if (extras != null) {
            pkglist = extras.getSerializable("pkglist") as ArrayList<GetMyPackagesBean.Packages>
            Utils.showLog(TAG, "==list==" + pkglist.size)
        }

        if(pkglist.size > 0)
        {
            bindRecyclerViewAdPostPackage(pkglist,0)
        }

    }

    private fun bindRecyclerViewAdPostPackage(pkglist: java.util.ArrayList<GetMyPackagesBean.Packages>,pos: Int) {
        val recyclerLayoutManager = LinearLayoutManager(this)
        rv_premium_Package.layoutManager = recyclerLayoutManager
        val adapter = AdPostSelectPackageAdapter(pkglist, this)
        adapter.setClicklistner(this)
        rv_premium_Package.adapter = adapter
        adapter.notifyDataSetChanged()
        rv_premium_Package.scrollToPosition(pos)
    }

    private fun initBindViews() {

        btnback.setOnClickListener(this)
        btn_ad_post.setOnClickListener(this)
        tvsellalllist.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_back -> onBackPressed()

            R.id.btn_ad_post -> {
                if (packageid != null && packageid.toString().length == 0) {
                    Utils.showToast(this,getString(R.string.txt_select_pkg))
                } else {

                    val c = AdPremiumToPostController(this, packageid!!,postId.toString(),this)
                    c.purchasePackage()
                }
            }

            R.id.tvsellalllist -> {
                val intent = Intent(applicationContext, AdPostPackagesActivity::class.java)
                intent.putExtra("isPremium", "yes")
                intent.putExtra("address", adcatdata!!.address)
                intent.putExtra("catId", catId)
                intent.putExtra("lat", adcatdata!!.latitude)
                intent.putExtra("long", adcatdata!!.longitude)
                intent.putExtra("price", adcatdata!!.price.toString())

                //new added
                intent.putExtra("location_name",adcatdata!!.location_name)

                startActivity(intent)
            }
        }
    }

    override fun itemClicked(pos: Int, selected: Boolean) {
        if (selected == true) {
            //addButtonView(premiumpkglist[pos].price)
            packageid = pkglist[pos].packageid!!
            val userpostpackageid = pkglist[pos].userpostpackageid
            storeUserData.setString(Constants.USER_PKG_ID,userpostpackageid.toString())
        }
        for (i in 0 until pkglist.size) {
            if (pos == i && selected == true) {
                pkglist[i].selected = true
            } else {
                pkglist[i].selected = false
            }
        }
        bindRecyclerViewAdPostPackage(pkglist,pos)
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if(method.equals("adPremium"))
        {
            Utils.showLog(TAG, "==else=" + GsonBuilder().setPrettyPrinting().create().toJson(da))
            Utils.showToast(this,message)




            storeUserData.setString(Constants.USER_PKG_ID,"")
            val intent = Intent(this@SelectPremiumPackageActivity, CongratulationPremiumPostActivity::class.java)
            intent.putExtra("data",da.toString())
            startActivity(intent)
            finishAffinity()

            /*Handler().postDelayed({
                if (!isFinishing) {
                    storeUserData.setString(Constants.USER_PKG_ID,"")
                    val intent = Intent(this@SelectPremiumPackageActivity, CongratulationPremiumPostActivity::class.java)
                    intent.putExtra("data",da.toString())
                    startActivity(intent)
                    finishAffinity()
                }
            }, 1000)*/

        }
    }

    override fun onFail(msg: String, method: String) {
        Utils.showToast(this,msg)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}