package com.kaushlesh.view.fragment.MyAccount.MyAds

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.text.Html
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.kaushlesh.Controller.AdPremiumToPostController
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.Controller.PaymentController
import com.kaushlesh.Controller.PurchasePackageController
import com.kaushlesh.R
import com.kaushlesh.bean.GetCategoryPackagesBean
import com.kaushlesh.bean.GetMyPostBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.MainActivity
import com.kaushlesh.view.fragment.MyAccount.PurchasePackage.AdPostPackagesActivity
import com.kaushlesh.view.fragment.MyAccount.PurchasePackage.PremiumAdExampleActivity
import com.razorpay.Checkout
import com.razorpay.PaymentResultListener
import kotlinx.android.synthetic.main.activity_sell_urgent_detail.*
import org.json.JSONObject

class SellUrgentDetailActivity : AppCompatActivity() , View.OnClickListener,ParseControllerListener,
    PaymentResultListener {

    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    lateinit var checkout: Checkout
    internal lateinit var paymentController: PaymentController
    var catId : String = ""
    var postId : String = ""
    var locationId : String = ""
    internal var pkglist: ArrayList<GetCategoryPackagesBean.Packages> = ArrayList()
    lateinit var getListingController: GetListingController
    var pkgid10 : String = ""
    var pkgid30 : String = ""
    internal var packageid : String?= ""

    var days10 : Boolean = true
    var days30 : Boolean = false
    internal var userpostpackageid: String? = ""
    var adcatdata : GetMyPostBean.getMyPostList ?= null

    lateinit var storeUserData: StoreUserData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sell_urgent_detail)

        Checkout.preload(applicationContext)

        checkout = Checkout()
        checkout.setKeyID(getString(R.string.key_razorpay))

        storeUserData = StoreUserData(this)

        getListingController = GetListingController(this, this)


        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)
        tvtoolbartitle.text = getString(R.string.txt_sell_urgent_now)

        if(intent != null)
        {
            catId = intent.getStringExtra("catId").toString()
            postId = intent.getStringExtra("postId").toString()
            locationId = intent.getStringExtra("locationId").toString()

            storeUserData.setString(Constants.POST_ID, postId.toString())
            storeUserData.setString(Constants.LOC_ID_PKG, locationId.toString())


            adcatdata = intent.getSerializableExtra("data") as GetMyPostBean.getMyPostList

            getListingController.getSingleAdPremiumPkgList(catId)
        }
        Utils.showLog(TAG,"==cat id==" + catId + "==post id==" + postId)

        btnback.setOnClickListener(this)
        tv_view_example.setOnClickListener(this)
        tv_pay.setOnClickListener(this)
        iv_radio.setOnClickListener(this)
        iv_radio1.setOnClickListener(this)
        rl10.setOnClickListener(this)
        rl30.setOnClickListener(this)
        tvsellall.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btn_back ->
                onBackPressed()

            R.id.tv_view_example ->
            {
                val intent = Intent(this, PremiumAdExampleActivity::class.java)
                startActivity(intent)
            }

            R.id.tv_pay ->
            {
               /* val activity: Activity = this
                paymentController = PaymentController(activity,this)
                paymentController.startPay()*/

                val total = tv_pay.text.toString().replace("Pay  ₹ ","").toInt() * 100
                startpay(total.toString())
            }

            R.id.rl10 ->
            {
                iv_radio.performClick()
            }
            R.id.rl30 ->
            {
                iv_radio1.performClick()
            }
            R.id.iv_radio ->
            {
                tv_pay.text = "Pay  " +tv_price.text.toString()
                days10= true
                days30 = false
                iv_radio.setBackgroundResource(R.drawable.ic_radio_select)
                iv_radio1.setBackgroundResource(R.drawable.bg_rounded_circle)
            }
            R.id.iv_radio1 ->
            {
                tv_pay.text = "Pay  "+tv_price1.text.toString()
                days10 = false
                days30 = true
                iv_radio1.setBackgroundResource(R.drawable.ic_radio_select)
                iv_radio.setBackgroundResource(R.drawable.bg_rounded_circle)
            }

            R.id.tvsellall ->
            {
                val intent = Intent(applicationContext, AdPostPackagesActivity::class.java)
                intent.putExtra("isPremium", "yes")
                intent.putExtra("address", adcatdata!!.address)
                intent.putExtra("catId", catId)
                intent.putExtra("lat", adcatdata!!.latitude)
                intent.putExtra("long", adcatdata!!.longitude)
                intent.putExtra("price", adcatdata!!.price.toString())
                intent.putExtra("postaddress","")

                //new added
                intent.putExtra("location_name",adcatdata!!.location_name)

                startActivity(intent)
                //finish()
            }
        }
    }

    private fun startpay(price: String) {

        val activity: Activity = this
        val co = Checkout()
        co.setKeyID(activity.getString(R.string.key_razorpay))

        try {
            val options = JSONObject()
            if(storeUserData.getString(Constants.NAME).equals("")){
                options.put("name", "Gujarat Living User")
            }else{
                options.put("name", ""+storeUserData.getString(Constants.NAME))
            }
            options.put("description", "Amount")
            options.put("theme.color", "#000000");
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png")
            options.put("currency", "INR")
            options.put("amount", price)

            val prefill = JSONObject()
            prefill.put("email", ""+storeUserData.getString(Constants.EMAIL))
            prefill.put("contact", ""+storeUserData.getString(Constants.CONTACT_NO))
            //prefill.put("contact", storeUserData.getString(Constants.PHONE_NUMBER))

            options.put("prefill", prefill)
            co.open(activity, options)
        } catch (e: Exception) {
            Utils.showLog(TAG,"============="+e.message)
            e.printStackTrace()
        }
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if(method.equals("GetPkg"))
        {
            val dataModel = GetCategoryPackagesBean()
            dataModel.GetCategoryPackagesBean(da)

            pkglist = dataModel.packagelist
            Utils.showLog(TAG,"==my pckg list size==" + pkglist.size)

            if(pkglist.size > 0)
            {
                for(i in pkglist.indices)
                {
                   /* if(pkglist.get(i).packagetype.equals("Premium1") ||  pkglist.get(i).packagetype.equals("Premium2"))
                    {
                        if(pkglist.get(i).noofAd.equals("1") && pkglist.get(i).duration.equals("10")) {
                            //tv1.text = Html.fromHtml("<b>"+pkglist.get(i).name+"</b>" + "\n"+resources.getString(R.string.txt_premium_ad_for_10_days))
                            tv1.text = Html.fromHtml("<b>"+pkglist.get(i).name+"</b>")
                            val day10spack = pkglist.get(i).price
                            tv_price.text = "₹ "+day10spack.toString()
                            pkgid10 = pkglist.get(i).packageid.toString()

                            tv_pay.visibility = View.VISIBLE
                            tv_pay.text = "Pay  ₹ " + day10spack
                        }

                        if(pkglist.get(i).noofAd.equals("1") && pkglist.get(i).duration.equals("30")) {
                            //tv2.text = Html.fromHtml("<b>"+pkglist.get(i).name+"</b>" + "\n"+resources.getString(R.string.txt_premium_ad_for_30_days))
                            tv2.text = Html.fromHtml("<b>"+pkglist.get(i).name+"</b>")
                            val day30spack = pkglist.get(i).price
                            tv_price1.text = "₹ "+day30spack.toString()
                            pkgid30 = pkglist.get(i).packageid.toString()
                        }
                    }*/


                            //tv1.text = Html.fromHtml("<b>"+pkglist.get(i).name+"</b>" + "\n"+resources.getString(R.string.txt_premium_ad_for_10_days))
                            tv1.text = Html.fromHtml("<b>"+pkglist.get(0).name+"</b>")
                            val day10spack = pkglist.get(0).price
                            tv_price.text = "₹ "+day10spack.toString()
                            pkgid10 = pkglist.get(0).packageid.toString()

                            tv_pay.visibility = View.VISIBLE
                            tv_pay.text = "Pay  ₹ " + day10spack



                            //tv2.text = Html.fromHtml("<b>"+pkglist.get(i).name+"</b>" + "\n"+resources.getString(R.string.txt_premium_ad_for_30_days))
                            tv2.text = Html.fromHtml("<b>"+pkglist.get(1).name+"</b>")
                            val day30spack = pkglist.get(1).price
                            tv_price1.text = "₹ "+day30spack.toString()
                            pkgid30 = pkglist.get(1).packageid.toString()

                }

                Utils.showLog(TAG,"==10 days==" + pkgid10 + "== 30 days==" + pkgid30)
            }
        }

        if(method.equals("purchasePkg"))
        {
            Utils.showToast(this,message)
            userpostpackageid = da.getString("userpostpurchasepackage_id")
            storeUserData.setString(Constants.USER_PKG_ID,userpostpackageid.toString())

            if(days10)
            {
                packageid = pkgid10
            }
            else{
                packageid = pkgid30
            }

            if(packageid != null && packageid.toString().length  >0)
            {
                val c = AdPremiumToPostController(this, packageid!!,postId,this)
                c.purchasePackage()
            }
        }
        if(method.equals("adPremium"))
        {
           // val intent = Intent(this, PremiumAdExampleActivity::class.java)
            /*val intent = Intent(this, MyPostActivity::class.java)
            startActivity(intent)
            finish()*/



            storeUserData.setString(Constants.USER_PKG_ID,"")
            storeUserData.setString(Constants.LOC_ID_PKG,"")
            val intent = Intent(this@SellUrgentDetailActivity, CongratulationPremiumPostActivity::class.java)
            intent.putExtra("data",da.toString())
            intent.putExtra("isfrom","exphide")
            startActivity(intent)
            finishAffinity()

            /*Handler().postDelayed({
                if (!isFinishing) {
                    storeUserData.setString(Constants.USER_PKG_ID,"")
                    storeUserData.setString(Constants.LOC_ID_PKG,"")
                    val intent = Intent(this@SellUrgentDetailActivity, CongratulationPremiumPostActivity::class.java)
                    intent.putExtra("data",da.toString())
                    intent.putExtra("isfrom","exphide")
                    startActivity(intent)
                    finishAffinity()
                }
            }, 1000)*/



        }

       /* val intent = Intent(applicationContext, MainActivity::class.java)
        startActivity(intent)
        finishAffinity()*/
    }

    override fun onFail(msg: String, method: String) {

        Utils.showLog(TAG,msg)
    }

    override fun onPaymentError(errocode: Int, response: String?) {
        try {
            //AppConstants.printToast(applicationContext, "Payment failed: " + errocode.toString() + " " + response)
            AppConstants.printLog("test", response + "Exception in onPaymentSuccess" + errocode)

        } catch (e: java.lang.Exception) {
            AppConstants.printLog("test", "Exception in onPaymentError" + e)
        }

        //listener.onFail(errocode.toString(),response.toString())
    }

    override fun onPaymentSuccess(razorpayPaymentId: String?) {
        try {
            //AppConstants.printToast(applicationContext, "Payment Successful: $razorpayPaymentId")

            if(days10)
            {
                packageid = pkgid10
            }
            else{
                packageid = pkgid30
            }

            Utils.showLog(TAG,"==purchase pkg id==" + packageid)

            if(packageid != null && packageid.toString().length  >0) {
                val c = PurchasePackageController(this, packageid!!,razorpayPaymentId.toString(),"", this)
                c.purchasePackage()
            }

        } catch (e: java.lang.Exception) {
            AppConstants.printLog("test", "Exception in onPaymentSuccess"+e)

        }
    }
    companion object {

        private val TAG = "SellUrgentDetailActivity"
    }
}
