package com.kaushlesh.view.fragment.MyAccount.PurchasePackage

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.kaushlesh.Controller.AdPremiumToPostController
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.Controller.PostLiveAgainController
import com.kaushlesh.Controller.PurchasePackageController
import com.kaushlesh.R
import com.kaushlesh.adapter.AdPostPackagesPagerAdapter
import com.kaushlesh.bean.CategoryBean
import com.kaushlesh.bean.GetCategoryPackagesBean
import com.kaushlesh.bean.LocationListBeans
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.AdPost.FromPackage.CheckCategoryAndApiCall2
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.fragment.MyAccount.MyAds.CongratulationPremiumPostActivity
import com.kaushlesh.view.fragment.MyAccount.MyAds.MyPostActivity
import com.razorpay.Checkout
import com.razorpay.PaymentResultListener
import kotlinx.android.synthetic.main.activity_ad_post_packages.*
import org.json.JSONObject


class AdPostPackagesActivity : AppCompatActivity(), View.OnClickListener ,ParseControllerListener,
    ExtraAdFragment.OnHeadlineSelectedListener,PremiumAdFragment.OnHeadlineSelectedListener,
    PaymentResultListener {

    private lateinit var type: String
    private lateinit var data: GetCategoryPackagesBean.Packages
    val TAG = "AdPostPackagesActivity"
    var tabLayout: TabLayout? = null
    var viewPager: ViewPager? = null

    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal lateinit var dialog: Dialog

    internal var catId : String?= ""
    internal var address : String?= ""

    internal var price : String?= ""
    internal var lattitude : String?= ""
    internal var longitude : String?= ""
    internal var packageid : String?= ""
    internal var isPremium : String?= ""
    internal var repost : String?= ""
    internal var postId : String?= ""

    internal var extraadlist: ArrayList<GetCategoryPackagesBean.Packages> = ArrayList()
    internal var premiumpkglist: ArrayList<GetCategoryPackagesBean.Packages> = ArrayList()
    internal var pkglist: ArrayList<GetCategoryPackagesBean.Packages> = ArrayList()
    internal var typeidlist: MutableList<String> = java.util.ArrayList()
    internal var typelist: MutableList<String> = java.util.ArrayList()
    internal var categorylist: java.util.ArrayList<CategoryBean.Category> = ArrayList()
    internal lateinit var type_spinner_type: ArrayAdapter<String>
    lateinit var getListingController: GetListingController
    internal var typeidlistloc: MutableList<String> = java.util.ArrayList()
    internal var typelistloc: MutableList<String> = java.util.ArrayList()
    var locationid: String? = null
    var locationname: String? = null
    var postaddress :String?=null
    var isFromAccount :String?=null

    lateinit var storeUserData: StoreUserData
    internal var locationlist: java.util.ArrayList<LocationListBeans.LocationList> = java.util.ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ad_post_packages)
        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = getString(R.string.ad_post_packages)

        storeUserData = StoreUserData(this)

        getListingController = GetListingController(this, this)
        getListingController.getLocationList()

        if(intent != null)
        {
            catId = intent.getStringExtra("catId")


            if (intent.getStringExtra("isFromAccount") != null) {
                isFromAccount = intent.getStringExtra("isFromAccount").toString()
                Utils.showLog(TAG, "==isFromAccount==" + isFromAccount)
            }

        }

        Utils.showLog(TAG, "==cat id==" + catId)

        postId = storeUserData.getString(Constants.POST_ID)

        if(intent!=null && intent.getStringExtra("location_name") != null){
            locationname =  intent.getStringExtra("location_name")
        }else{
            locationname =  storeUserData.getString(Constants.LOC_NAME)
        }


        Utils.showLog(TAG, "==post id==" + postId)
        Utils.showLog(TAG, "==location id==" + storeUserData.getString(Constants.LOCATION_ID))
        Utils.showLog(TAG, "==location name==" + locationname)
        if(catId != null && locationname !=null)
        {
            val catname = Constants.getCategoryFromId(catId!!.toInt(), this)
            et_category.setText(catname)
            et_category.isEnabled = false
            et_category.visibility = View.VISIBLE
            rl_brand_type.visibility = View.GONE

            act_location.visibility = View.VISIBLE
            act_location.setText(locationname)
            act_location.isEnabled = false
            tvloc.visibility = View.VISIBLE
            rl_dis_type.visibility = View.GONE
            tvtitletype.visibility = View.GONE

            isPremium = intent.getStringExtra("isPremium")
            price = intent.getStringExtra("price")
            lattitude = intent.getStringExtra("lat")
            longitude = intent.getStringExtra("long")
            address = intent.getStringExtra("address")
            repost = intent.getStringExtra("repost")
            postaddress = intent.getStringExtra("postaddress")

            address = locationname
            locationid = storeUserData.getString(Constants.LOCATION_ID)

            storeUserData.setString(
                Constants.LOC_ID_PKG,
                storeUserData.getString(Constants.LOCATION_ID)
            )

            //act_location.setText(address)
            Utils.showLog(TAG, "==post address name==" + postaddress)
            Utils.showLog(TAG, "==lat==" + lattitude)
            Utils.showLog(TAG, "==long==" + longitude)

            getListingController.getCategoryPackgesList(catId.toString())
        }
        else{
            et_category.isEnabled = false
            et_category.visibility = View.GONE
            rl_brand_type.visibility = View.VISIBLE
            getListingController.getCategoryList()

        }
        Utils.showLog(TAG, "== repost ==" + repost)
        Utils.showLog(
            TAG,
            "== price ==" + price + "==lattitude== " + lattitude + "== longitude===" + longitude + "== is premium==" + isPremium + "==address== " + address
        )

        setOnclick()

        tabLayout = findViewById(R.id.tablaout)
        viewPager = findViewById(R.id.pager)

       /* tabLayout!!.addTab(tabLayout!!.newTab().setText("Extra Ad"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Premium Ad"))

       *//* val view = layoutInflater.inflate(R.layout.layout_icon, null)
        tabLayout!!.addTab(tabLayout!!.newTab().setCustomView(view))*//*

        tabLayout!!.tabGravity = TabLayout.GRAVITY_FILL

        val adapter = AdPostPackagesPagerAdapter(applicationContext, supportFragmentManager, tabLayout!!.tabCount)
        viewPager!!.adapter = adapter
*/
        viewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))

        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager!!.currentItem = tab.position
                btn_pay.visibility = View.GONE
                viewPager!!.adapter?.notifyDataSetChanged()

                Utils.showLog(TAG, "====on selected====" + tab.position)

                if(tab.position == 0) {
                    tv_view_example.visibility = View.GONE
                    txtLablePackage.setText(R.string.txt_ad_post_packages)
                    ExtraAdFragment.getInstance()?.refershData(extraadlist)
                } else{
                    tv_view_example.visibility = View.VISIBLE
                    txtLablePackage.setText(R.string.txt_premium_post_packages)
                    PremiumAdFragment.getInstance()?.refershData(premiumpkglist)
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                Utils.showLog(TAG, "====on unselected====" + tab.position)

            }

            override fun onTabReselected(tab: TabLayout.Tab) {
                Utils.showLog(TAG, "====on reselected====" + tab.position)
            }
        })

        spinner_brand_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>,
                view: View,
                position: Int,
                l: Long
            ) {
                AppConstants.printLog(TAG, "onItemSelected: ===" + adapterView.selectedItem)
                AppConstants.printLog(TAG, "onItemSelected: ===" + typeidlist.get(position))

                tabLayout!!.removeAllTabs()
                extraadlist.clear()
                premiumpkglist.clear()
                catId = typeidlist.get(position).toString()
                getListingController.getCategoryPackgesList(typeidlist.get(position).toString())

            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        }

    }

    private fun setOnclick() {
        btnback.setOnClickListener(this)
        ll_info.setOnClickListener(this)
        tv_view_example.setOnClickListener(this)
        btn_pay.setOnClickListener(this)
        tv_dis_type.setOnClickListener(this)

        if(catId != null && catId.toString().length == 0)
        {
            et_category.setOnClickListener(this)
            rl_brand_type.setOnClickListener(this)
            ib_brand_type.setOnClickListener(this)
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btn_back -> onBackPressed()

            R.id.ll_info -> {
                openInfoDialog()
            }

            R.id.tv_view_example -> {
                val intent = Intent(this, PremiumAdExampleActivity::class.java)
                startActivity(intent)
            }

            R.id.btn_pay -> {
                Utils.showLog(TAG, "==current tab==" + viewPager!!.currentItem)

                Utils.showLog(TAG, "==data btn==" + data.price)
                Utils.showLog(TAG, "=type==" + type)

                if (locationid == null || locationid.toString().length == 0) {
                    Utils.showToast(this, getString(R.string.txt_select_district_hint))
                } else {

                    if(isFromAccount != null && isFromAccount == "account"){
                        openConfirmationDialog()
                    }else{
                        if (data.price != null && data.price.toString().length > 0) {
                            packageid = data.packageid
                            val total = data.price!!.toInt() * 100

                            Utils.setButtonEnabled(btn_pay, false)

                            if(Utils.isConnected()){
                                startpay(total.toString())
                            }else{
                                Utils.showAlertConnection(this)
                            }

                        }
                    }

                    /*if (catId != null && locationname != null) {
                        if (data.price != null && data.price.toString().length > 0) {
                            packageid = data.packageid
                            val total = data.price!!.toInt() * 100

                            Utils.setButtonEnabled(btn_pay, false)
                            startpay(total.toString())
                        }
                    } else {
                        openConfirmationDialog()
                    }*/

                    //take confirmation before pay
                    /*if (data.price != null && data.price.toString().length > 0) {
                        packageid = data.packageid
                        val total = data.price!!.toInt() * 100

                        Utils.setButtonEnabled(btn_pay, false)
                        startpay(total.toString())
                    }*/
                }
            }

            R.id.rl_brand_type -> {

                tv_brand_type.visibility = View.GONE
                spinner_brand_type.visibility = View.VISIBLE
                rl_brand_type.performClick()
            }

            R.id.ib_brand_type -> {

                tv_brand_type.visibility = View.GONE
                spinner_brand_type.visibility = View.VISIBLE
                rl_brand_type.performClick()
            }

            R.id.tv_dis_type -> {
                spinner_dis_type.visibility = View.VISIBLE
                spinner_dis_type.performClick()
                tv_dis_type.visibility = View.GONE
                spinner_dis_type.onItemSelectedListener =
                    object : AdapterView.OnItemSelectedListener {
                        override fun onItemSelected(
                            adapterView: AdapterView<*>,
                            view: View,
                            position: Int,
                            l: Long
                        ) {
                            AppConstants.printLog(
                                TAG,
                                "onItemSelected: ===" + adapterView.selectedItem
                            )
                            AppConstants.printLog(
                                TAG,
                                "onItemSelected: ===" + adapterView.selectedItem
                            )
                            AppConstants.printLog(
                                TAG, "onItemSelected: ===" + typeidlistloc.get(
                                    position
                                )
                            )
                            locationid = typeidlistloc.get(position)
                            storeUserData.setString(Constants.LOC_ID_PKG, locationid.toString())
                            rl_dis_type.setBackgroundResource(R.drawable.bg_edittext_black)
                        }

                        override fun onNothingSelected(adapterView: AdapterView<*>) {

                        }
                    }

            }
        }

    }

    private fun openConfirmationDialog() {

        dialog = Dialog(this)
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_logout)
        // Set dialog title
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set values for custom dialog components - text, image and button
        val tv_title = dialog.findViewById<TextView>(R.id.tv_title)
        val btnno = dialog.findViewById<Button>(R.id.btn_no)
        val btnyes = dialog.findViewById<Button>(R.id.btn_yes)

        tv_title.text = resources.getString(R.string.txt_packge_confirmation)
        btnno.text = resources.getString(R.string.txt_review)
        btnyes.text = resources.getString(R.string.txt_pay_now)
        btnno.setTextColor(resources.getColor(R.color.black))
        btnyes.setTextColor(resources.getColor(R.color.orange))

        btnno.setOnClickListener {
            dialog.dismiss()
        }

        btnyes.setOnClickListener {
            if (data.price != null && data.price.toString().length > 0) {
                packageid = data.packageid
                val total = data.price!!.toInt() * 100

                Utils.setButtonEnabled(btn_pay, false)

                if(Utils.isConnected()){
                    startpay(total.toString())
                }else{
                    Utils.showAlertConnection(this)
                }


            }
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun openInfoDialog() {

        dialog = Dialog(this)
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_info_packages)
        // Set dialog title
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        // set values for custom dialog components - text, image and button
        val ivclose = dialog.findViewById<ImageView>(R.id.ivclose)
        ivclose.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if(method.equals("GetPkg"))
        {
            val dataModel = GetCategoryPackagesBean()
            dataModel.GetCategoryPackagesBean(da)

            pkglist = dataModel.packagelist
            Utils.showLog(TAG, "==my pckg list size==" + pkglist.size)

            if(pkglist.size > 0)
            {
                for(i in pkglist.indices)
                {

                    if(pkglist.get(i).packagetype.equals("More"))
                    {
                        extraadlist.add(pkglist.get(i))
                    }

                    if(pkglist.get(i).packagetype.equals("Premium1") ||  pkglist.get(i).packagetype.equals(
                            "Premium2"
                        ))
                    {
                        premiumpkglist.add(pkglist.get(i))
                    }
                }
            }

            Utils.showLog(TAG, "==extra ad===" + extraadlist.size)
            Utils.showLog(TAG, "==premium ad===" + premiumpkglist.size)

            if(isPremium.equals("yes"))
            {
                tv_view_example.visibility = View.VISIBLE
                //tabLayout!!.addTab(tabLayout!!.newTab().setText("Extra Ad"))
                tabLayout!!.addTab(tabLayout!!.newTab().setText(getString(R.string.txt_premium_ad)))

             /*   val view = layoutInflater.inflate(R.layout.layout_icon, null)
                tabLayout!!.addTab(tabLayout!!.newTab().setCustomView(view))*/

                tabLayout!!.tabGravity = TabLayout.GRAVITY_FILL

                val adapter = AdPostPackagesPagerAdapter(
                    applicationContext,
                    supportFragmentManager,
                    tabLayout!!.tabCount,
                    isPremium.toString()
                )
                viewPager!!.adapter = adapter
            }
            else if(isPremium.equals("no"))
            {
                tv_view_example.visibility = View.GONE
                tabLayout!!.addTab(tabLayout!!.newTab().setText(getString(R.string.txt_extra_ad)))
               // tabLayout!!.addTab(tabLayout!!.newTab().setText("Premium Ad"))

                 /*val view = layoutInflater.inflate(R.layout.layout_icon, null)
                 tabLayout!!.addTab(tabLayout!!.newTab().setCustomView(view))*/

                tabLayout!!.tabGravity = TabLayout.GRAVITY_FILL

                val adapter = AdPostPackagesPagerAdapter(
                    applicationContext,
                    supportFragmentManager,
                    tabLayout!!.tabCount,
                    isPremium.toString()
                )
                viewPager!!.adapter = adapter
            }
            else {
                tv_view_example.visibility = View.VISIBLE
                tabLayout!!.addTab(tabLayout!!.newTab().setText(getString(R.string.txt_extra_ad)))
                tabLayout!!.addTab(tabLayout!!.newTab().setText(getString(R.string.txt_premium_ad)))

                 /*val view = layoutInflater.inflate(R.layout.layout_icon, null)
                 tabLayout!!.addTab(tabLayout!!.newTab().setCustomView(view))*/

                tabLayout!!.tabGravity = TabLayout.GRAVITY_FILL

                val adapter = AdPostPackagesPagerAdapter(
                    applicationContext,
                    supportFragmentManager,
                    tabLayout!!.tabCount,
                    ""
                )
                viewPager!!.adapter = adapter
            }
        }

        else if (method.equals("Get Category")) {
            if (da.length() > 0) {

                categorylist.clear()
                typelist.clear()

                val dataModel = CategoryBean()
                dataModel.CategoryBean(da)

                categorylist = dataModel.categoryList
                Utils.showLog(TAG, "==category list size==" + categorylist.size)

                for(i in categorylist.indices)
                {
                    val name = categorylist.get(i).categoryName
                    val id = categorylist.get(i).categoryId
                    typelist.add(name.toString())
                    typeidlist.add(id.toString())
                }

                type_spinner_type = ArrayAdapter(
                    applicationContext,
                    android.R.layout.simple_spinner_dropdown_item,
                    typelist
                )
                // Creating adapter for spinner
                val dataAdapter1 =
                    ArrayAdapter(applicationContext, android.R.layout.simple_spinner_item, typelist)

                // Drop down layout style - list view with radio button
                dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                // attaching data adapter to spinner
                spinner_brand_type.adapter = dataAdapter1
            }
        }

        else if(method.equals("purchasePkg"))
        {
            Utils.showToast(this, message)

            /* if(catId != null)
             {
                 Utils.showLog(TAG,"=type==" + type)
                 CheckCategoryAndApiCall2.checkforCategory(this, catId!!.toInt(),storeUserData.getString(Constants.SUBCATEGORYID).toInt(),address.toString(),lattitude.toString(),longitude.toString(),price.toString(),
                     packageid.toString()
                 )
             }*/

            val userpostpackageid = da.getString("userpostpurchasepackage_id")
            storeUserData.setString(Constants.USER_PKG_ID, userpostpackageid.toString())

            if(isPremium != null && isPremium.toString().length > 0 && isPremium.toString().equals("no"))
            {
                Utils.showLog(TAG, "=type==" + type)
                if(repost != null && repost.toString().length >0 && repost.toString().equals("yes"))
                {
                    PostLiveAgainController(this, postId.toString(), packageid.toString(), this)
                }
                else {
                    CheckCategoryAndApiCall2.checkforCategory(
                        this,
                        catId!!.toInt(),
                        storeUserData.getString(
                            Constants.SUBCATEGORYID
                        ).toInt(),
                        postaddress.toString(),
                        lattitude.toString(),
                        longitude.toString(),
                        price.toString(),
                        packageid.toString()
                    )
                }
            }

            else if(isPremium != null && isPremium.toString().length > 0 && isPremium.toString().equals(
                    "yes"
                ))
            {
                //call api to add premium tag and redirect to premium listing
                val c = AdPremiumToPostController(this, packageid!!, postId.toString(), this)
                c.purchasePackage()
            }
            else{
                storeUserData.setString(Constants.LOC_ID_PKG, "")

                val intent = Intent(this, MyOrdersActivity::class.java)
                startActivity(intent)
                finish()


                /*Handler().postDelayed({
                    if (!isFinishing) {
                        val intent = Intent(this, MyOrdersActivity::class.java)
                        startActivity(intent)
                        finish()

                    }
                }, 1000)*/
            }
        }

        else if(method.equals("adPremium"))
        {
            storeUserData.setString(Constants.POST_ID, "")
            storeUserData.setString(Constants.USER_PKG_ID, "")
            storeUserData.setString(Constants.LOC_ID_PKG, "")
            //val intent = Intent(this, PremiumAdExampleActivity::class.java)
           /* val intent = Intent(this, MyPostActivity::class.java)
            startActivity(intent)
            finish()*/



            val intent = Intent(this@AdPostPackagesActivity, CongratulationPremiumPostActivity::class.java)
            intent.putExtra("data", da.toString())
            startActivity(intent)
            finish()

            /*Handler().postDelayed({
                if (!isFinishing) {
                    val intent = Intent(this@AdPostPackagesActivity, CongratulationPremiumPostActivity::class.java)
                    intent.putExtra("data", da.toString())
                    startActivity(intent)
                    finish()
                }
            }, 1000)*/



        }

        else if(method.equals("GetLocation")) {
            locationlist.clear()
            typelistloc.clear()

            val dataModel = LocationListBeans()
            dataModel.LocationListBeans(da)
            locationlist = dataModel.locationList
            Utils.showLog(TAG, "==location list size==" + locationlist.size)

            for (i in locationlist.indices) {
                val name = locationlist.get(i).locName
                val id = locationlist.get(i).locId
                typelistloc.add(name.toString())
                typeidlistloc.add(id.toString())
            }

            type_spinner_type = ArrayAdapter(
                applicationContext,
                android.R.layout.simple_spinner_dropdown_item,
                typelistloc
            )
            // Creating adapter for spinner
            val dataAdapter1 =
                    ArrayAdapter(
                        applicationContext,
                        android.R.layout.simple_spinner_item,
                        typelistloc
                    )

            // Drop down layout style - list view with radio button
            dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

            // attaching data adapter to spinner
            spinner_dis_type.adapter = dataAdapter1

            tv_dis_type.visibility = View.VISIBLE
            tv_dis_type.text = resources.getText(R.string.txt_select_district_hint)
            spinner_dis_type.visibility = View.GONE
            rl_dis_type.setBackgroundResource(R.drawable.bg_edittext)
        }

        else{

            storeUserData.setString(Constants.USER_PKG_ID, "")
            storeUserData.setString(Constants.LOC_ID_PKG, "")
            storeUserData.setString(Constants.LOCATION_ID, "")
            storeUserData.setString(Constants.POST_ID, "")
            val intent = Intent(applicationContext, MyPostActivity::class.java)
            intent.putExtra("from", "attention")
            startActivity(intent)
            finishAffinity()
        }
    }

    override fun onFail(msg: String, method: String) {
        Utils.showToast(this, msg)
    }

    fun getDataExtraAd(): ArrayList<GetCategoryPackagesBean.Packages> {

        return extraadlist
    }

    fun getDataPremiumaAd(): ArrayList<GetCategoryPackagesBean.Packages> {
        return premiumpkglist
    }

    override fun onArticleSelected(position: String, bean: GetCategoryPackagesBean.Packages) {
        Utils.showLog(TAG, "==type==" + position)

        Utils.showLog(TAG, "==data==" + bean.price)

        type= position
        data = bean

        if(type.equals("null"))
        {
            btn_pay.visibility = View.GONE
        }
        else {
            if (bean.price != null && bean.price.toString().length > 0) {
                Utils.showLog(TAG, "==if==" + bean.price)
                btn_pay.visibility = View.VISIBLE
                btn_pay.text = "Pay  ₹ " + bean.price
            } else {
                Utils.showLog(TAG, "==else" + bean.price)
                btn_pay.visibility = View.GONE
                btn_pay.text = "Pay  ₹ " + bean.price
            }
        }
    }

    private fun startpay(price: String) {

        val activity: Activity = this
        val co = Checkout()
        co.setKeyID(activity.getString(R.string.key_razorpay))

        try {
            val options = JSONObject()
            if(storeUserData.getString(Constants.NAME).equals("")){
                options.put("name", "Gujarat Living User")
            }else{
                options.put("name", ""+storeUserData.getString(Constants.NAME))
            }
            options.put("description", "Amount")
            options.put("theme.color", "#000000");
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png")
            options.put("currency", "INR")
            options.put("amount", price)

            val prefill = JSONObject()
            prefill.put("email", ""+storeUserData.getString(Constants.EMAIL))
            prefill.put("contact", ""+storeUserData.getString(Constants.CONTACT_NO))
            //prefill.put("contact", storeUserData.getString(Constants.PHONE_NUMBER))

            options.put("prefill", prefill)
            co.open(activity, options)
        } catch (e: Exception) {
            Utils.showLog(TAG, "=============" + e.message)
            //Toast.makeText(activity, "Error in payment: " + e.message, Toast.LENGTH_LONG).show()
            e.printStackTrace()
        }
    }

    override fun onPaymentError(errocode: Int, response: String?) {
        try {
            //AppConstants.printToast(applicationContext, "Payment failed: " + errocode.toString() + " " + response)
            AppConstants.printLog("test", response + "Exception in onPaymentSuccess" + errocode)
            Utils.showAlert(this, "Payment cancelled by User")

        } catch (e: java.lang.Exception) {
            AppConstants.printLog("test", "Exception in onPaymentError" + e)
        }

        //listener.onFail(errocode.toString(),response.toString())
    }

    override fun onPaymentSuccess(razorpayPaymentId: String?) {
        try {
            //AppConstants.printToast(applicationContext, "Payment Successful: $razorpayPaymentId")
            Utils.showLog(TAG, "==$razorpayPaymentId==" + razorpayPaymentId)
            Utils.showLog(TAG, "==purchase pkg id==" + packageid)

            if(packageid != null && packageid.toString().length > 0)
            {
               /* if(isPremium.equals("yes"))
                {
                    //call api to ad premium tag
                }
                else{
                    //add below code of purchase
                }*/
                val c = PurchasePackageController(
                    this,
                    packageid!!,
                    razorpayPaymentId.toString(),
                        "",
                    this
                )
                c.purchasePackage()
            }

        } catch (e: java.lang.Exception) {
            AppConstants.printLog("test", "Exception in onPaymentSuccess" + e)

        }
    }

    override fun onResume() {
        Utils.setButtonEnabled(btn_pay, true)
        super.onResume()
    }

}