package com.kaushlesh.view.fragment.MyAccount.PurchasePackage

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.pm.PackageManager
import android.media.MediaScannerConnection
import android.os.*
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.R
import com.kaushlesh.adapter.InvoicesAdapter
import com.kaushlesh.bean.MyPackageOrderBean
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Utils
import kotlinx.android.synthetic.main.activity_my_orders.*
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class InvoicesActivity : AppCompatActivity(),InvoicesAdapter.ItemClickListener, ParseControllerListener {

    internal lateinit var rvinvoice: RecyclerView
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView

    private lateinit var getListingController: GetListingController
    val listOrder = ArrayList<MyPackageOrderBean.MyPackageOrderList>()

    val MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 123
    var result = false

    /*private val list: List<InvoicesBean>
        get() {
            val list = ArrayList<InvoicesBean>()

            list.add(InvoicesBean("332211", "15 october", "200.00"))
            list.add(InvoicesBean("332211", "14 october", "200.00"))

            return list
        }*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_invoices)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.txt_invoices)

        rvinvoice = findViewById(R.id.rv_invoices)

        getOrderList()

        btnback.setOnClickListener {
          onBackPressed()

        }

    }

    private fun getOrderList() {
        Utils.showProgress(this)
        getListingController = GetListingController(this, this)
        getListingController.getPackageOrder()
    }

    private fun bindRecyclerview() {
        val recyclerLayoutManager = LinearLayoutManager(applicationContext)
        rvinvoice.layoutManager = recyclerLayoutManager
        rvinvoice.itemAnimator = DefaultItemAnimator()
        val adapter = InvoicesAdapter(listOrder, applicationContext!!)
        adapter.setClicklistner(this)
        rvinvoice.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun itemclick(bean: MyPackageOrderBean.MyPackageOrderList) {
        if(bean.invoiceLink!=null && bean.invoiceLink != ""){
            downloadPDF(bean.invoiceLink)
        }
    }

    private fun checkResponse() {
        try {
            if (listOrder.size > 0) {
                bindRecyclerview()
            }
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (method.equals("GetOrder")) {
            listOrder.clear()
            val dataModel = MyPackageOrderBean()
            dataModel.MyPackageOrderBean(da)
            listOrder.addAll(dataModel.myPackageOrderList)
            Utils.dismissProgress()

            checkResponse()

        }
    }

    override fun onFail(msg: String, method: String) {
        Utils.dismissProgress()
    }

    fun downloadPDF(url: String?) {

        val originalFilename = url!!.substring(url.lastIndexOf("/") + 1)
        result = checkPermission()
        if (result) {

            Log.d("Folder", "Already checkFolder")

            checkFolder()

            /*val file = File(Environment.getExternalStorageDirectory().absolutePath + "/AfrocamgistVideos/" + originalFilename)
            if (file.exists()) {
                val filePath = Environment.getExternalStorageDirectory().absolutePath + "/AfrocamgistVideos/" + originalFilename
                shareVideoToSocialMedia(filePath)
            } else {
                callWatermarkVideoAPI(videoUrl)
            }*/
        }


        val downloadTask = DownloadTask(this@InvoicesActivity)
        downloadTask.execute(url)
    }

    fun checkFolder() {
        val path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).absolutePath + "/GujaratLivingPDF/"
        val dir = File(path)
        var isDirectoryCreated = dir.exists()
        if (!isDirectoryCreated) {
            isDirectoryCreated = dir.mkdir()
        }
        if (isDirectoryCreated) {
            // do something
            Log.d("Folder", "Already Created")
        }
    }

    class DownloadTask(private val context: Context) : AsyncTask<String?, Int?, String?>() {

        //Download PDF
        var fileN: String? = null
        var mProgressDialog: ProgressDialog? = null
        private val waterMarkVideo: String? = null
        private val popup: Dialog? = null
        private val shareOrDownload = ""

        private var mWakeLock: PowerManager.WakeLock? = null

        protected override fun doInBackground(vararg p0: String?): String? {
            var input: InputStream? = null
            var output: OutputStream? = null
            var connection: HttpURLConnection? = null
            try {
                val url = URL(p0[0])

                Log.d("ExternalStorage", "Scanned $url")


                connection = url.openConnection() as HttpURLConnection
                connection.connect()
                if (connection!!.responseCode != HttpURLConnection.HTTP_OK) {
                    return ("Server returned HTTP " + connection.responseCode
                            + " " + connection.responseMessage)
                }
                val fileLength = connection.contentLength
                input = connection.inputStream
                val filePath = p0[0]
                fileN = filePath?.substring(filePath.lastIndexOf("/") + 1)
                //fileN = "Afrocamgist_" + UUID.randomUUID().toString().substring(0, 10) + ".mp4";
                val filename = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).absolutePath + "/GujaratLivingPDF/", fileN)
                output = FileOutputStream(filename)
                val data = ByteArray(4096)
                var total: Long = 0
                var count: Int
                while (input.read(data).also { count = it } != -1) {
                    if (isCancelled) {
                        input.close()
                        return null
                    }
                    total += count.toLong()
                    if (fileLength > 0) publishProgress((total * 100 / fileLength).toInt())
                    output.write(data, 0, count)
                }
            } catch (e: Exception) {
                return e.toString()
            } finally {
                try {
                    output?.close()
                    input?.close()
                } catch (ignored: IOException) {
                }
                connection?.disconnect()
            }
            return null
        }

        override fun onPreExecute() {
            super.onPreExecute()
            val pm = context.getSystemService(POWER_SERVICE) as PowerManager
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, javaClass.name)
            mWakeLock!!.acquire(10 * 60 * 1000L /*10 minutes*/)
            //mProgressDialog = ProgressDialog(context)
            mProgressDialog =  ProgressDialog(context, R.style.AppCompatAlertDialogStyle)

            mProgressDialog!!.setMessage("Downloading...")
            mProgressDialog!!.setIndeterminate(true)
            mProgressDialog!!.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL)
            mProgressDialog!!.setCancelable(true)
            mProgressDialog!!.show()
        }

        protected override fun onProgressUpdate(vararg values: Int?) {
            super.onProgressUpdate(*values)
            mProgressDialog!!.setIndeterminate(false)
            mProgressDialog!!.setMax(100)
            mProgressDialog!!.setProgress(values[0]!!)
        }

        override fun onPostExecute(result: String?) {
            mWakeLock!!.release()
            mProgressDialog!!.dismiss()
            if (result != null) {
                showToast(context, "Download Error" )
            } else {
                showToast( context,"Invoice downloaded in your mobile files")
            }
            MediaScannerConnection.scanFile(context, arrayOf(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).absolutePath +
                    "/GujaratLivingPDF/" + fileN), null
            ) { newpath, newuri ->
                Log.d("ExternalStorage", "Scanned $newpath:")
                Log.d("ExternalStorage", "-> uri=$newuri")
            }
            val filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).absolutePath + "/GujaratLivingPDF/" + fileN
//            shareVideoToSocialMedia(filePath)
        }

        fun showToast(activity: Context?, message: String?) {
            //Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
            val toast = Toast(activity)
            val view: View = LayoutInflater.from(activity).inflate(R.layout.toast_custom, null)
            val textView = view.findViewById<View>(R.id.custom_toast_text) as TextView
            textView.text = message
            toast.setView(view)
            // toast.setGravity(Gravity.BOTTOM or Gravity.CENTER, 0, 0)
            toast.setDuration(Toast.LENGTH_LONG)
            toast.show()
        }

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    fun checkPermission(): Boolean {
        val currentAPIVersion = Build.VERSION.SDK_INT
        return if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this@InvoicesActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this@InvoicesActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    val alertBuilder = AlertDialog.Builder(this@InvoicesActivity)
                    alertBuilder.setCancelable(true)
                    alertBuilder.setTitle("Permission necessary")
                    alertBuilder.setMessage("Write Storage permission is necessary to Download Files!!!")
                    alertBuilder.setPositiveButton(android.R.string.yes) { dialog, which -> ActivityCompat.requestPermissions(this@InvoicesActivity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), MY_PERMISSIONS_REQUEST_WRITE_STORAGE) }
                    val alert = alertBuilder.create()
                    alert.show()
                } else {
                    ActivityCompat.requestPermissions(this@InvoicesActivity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), MY_PERMISSIONS_REQUEST_WRITE_STORAGE)
                }
                false
            } else {
                true
            }
        } else {
            true
        }
    }


    fun checkAgain() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this@InvoicesActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            val alertBuilder = AlertDialog.Builder(this@InvoicesActivity)
            alertBuilder.setCancelable(true)
            alertBuilder.setTitle("Permission necessary")
            alertBuilder.setMessage("Write Storage permission is necessary to Download Files!!!")
            alertBuilder.setPositiveButton(android.R.string.yes) { dialog, which -> ActivityCompat.requestPermissions(this@InvoicesActivity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), MY_PERMISSIONS_REQUEST_WRITE_STORAGE) }
            val alert = alertBuilder.create()
            alert.show()
        } else {
            ActivityCompat.requestPermissions(this@InvoicesActivity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), MY_PERMISSIONS_REQUEST_WRITE_STORAGE)
        }
    }

    //Here you can check App Permission
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            MY_PERMISSIONS_REQUEST_WRITE_STORAGE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                checkFolder()
            } else {
                //code for deny
                checkAgain()
            }
        }
    }


}
