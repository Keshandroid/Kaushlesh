package com.kaushlesh.view.fragment.MyAccount.MyAds

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.GsonBuilder
import com.kaushlesh.Controller.*
import com.kaushlesh.R
import com.kaushlesh.adapter.MenuAdapter
import com.kaushlesh.adapter.MypostAdapter
import com.kaushlesh.bean.AdPost.AdPostResponseBean
import com.kaushlesh.bean.AdsFilterCategoryBean
import com.kaushlesh.bean.GetMyPackagesBean
import com.kaushlesh.bean.GetMyPostBean
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.reusable.EndlessRecyclerOnScrollListenerList
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.PostDetail.CheckCategoryForPostDetails
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.AdPost.CongratulationAdpostActivity
import com.kaushlesh.view.activity.MainActivity
import com.kaushlesh.view.fragment.MyAccount.PurchasePackage.AdPostPackagesActivity
import kotlinx.android.synthetic.main.drop_down_menu.*
import kotlinx.android.synthetic.main.fragment_home_new.*
import kotlinx.android.synthetic.main.fragment_my_post.*
import org.json.JSONObject
import java.io.Serializable
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList


class MyPostAdFragment : Fragment(), MypostAdapter.ItemClickListener, ParseControllerListener, MenuAdapter.OnMenuItemClickListener {

    private var filterCategotyType: FilterType = FilterType.All_AD


    private var loading: Boolean = false
    private lateinit var postdata: GetMyPostBean.getMyPostList
    internal var pkglist: ArrayList<GetMyPackagesBean.Packages> = ArrayList()
    internal var pkglist1: ArrayList<GetMyPackagesBean.Packages> = ArrayList()
    var list = ArrayList<GetMyPostBean.getMyPostList>()

    private lateinit var filterCategory: AdsFilterCategoryBean

    private lateinit var myPostShowcaseDeleteController: MyPostShowcaseDeleteController
    private lateinit var getListingController: GetListingController
    internal lateinit var dialog: Dialog
    var typePost = ""
    internal var newpkglist: ArrayList<GetMyPackagesBean.Packages> = ArrayList()
    internal var packageid: String? = ""
    private lateinit var posttype: String
    private lateinit var type: String
    private lateinit var post_id: String
    private lateinit var cat_id: String
    internal var userpostpackageid: String? = ""
    lateinit var storeUserData: StoreUserData
    lateinit var adapter : MypostAdapter
    var start = 0
    var limit:Int = 20
    var filterType:Int = 1
    private var featureRcvScrollListener: EndlessRecyclerOnScrollListenerList? = null
    lateinit var rvMyPost : RecyclerView
    //private var scrollListener: EndlessScrollRecyclListener? = null
    var current_page = 1
    private var progressStatus = 0
    private var handler: Handler = Handler()
    lateinit var progressBar: ProgressBar
    var pos = 0
    internal lateinit var date: DatePickerDialog.OnDateSetListener
    private val myCalendar = Calendar.getInstance()
    var picker: DatePickerDialog? = null
    var startDate: String? = ""
    var endDate: String? = ""
    var totalPostCount: String? = ""


    private var popup: PopupWindow? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_post, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        instance=this
        getListingController = GetListingController(activity!!, this)
        storeUserData = StoreUserData(context!!)
        rvMyPost = view.findViewById(R.id.rv_mypost)
        progressBar = view.findViewById(R.id.preogressbar)

        myPostShowcaseDeleteController = MyPostShowcaseDeleteController(activity!!, this)

        //rvMyPost.setHasFixedSize(true)
        rvMyPost.itemAnimator = null
        rvMyPost.getRecycledViewPool().clear()
        val mLayoutManager = LinearLayoutManager(context)
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvMyPost.setLayoutManager(mLayoutManager)
        adapter = MypostAdapter(context!!, arrayListOf(), "post")
        adapter.setClicklistner(this)
        rvMyPost.adapter = adapter

        featureRcvScrollListener = object : EndlessRecyclerOnScrollListenerList()
        {
            override fun onLoadMore(page: Int, totalItemsCount: Int) {
                Log.e("Pagination", "onLoadMore Invocke : ${page}")
                val marginLayoutParams = RelativeLayout.LayoutParams(rvMyPost.getLayoutParams())
                //val marginLayoutParams = ViewGroup.MarginLayoutParams(rcvListView.getLayoutParams())
                marginLayoutParams.setMargins(0, 0, 0, 130)
                rvMyPost.setLayoutParams(marginLayoutParams)
                progressBar.setVisibility(View.VISIBLE)
                Thread {
                    while (progressStatus < 100) {
                        progressStatus +=10
                        handler.post(Runnable {
                            progressBar.progress = progressStatus
                        })
                        try {
                            // Sleep for 200 milliseconds.
                            Thread.sleep(100)
                        } catch (e: InterruptedException) {
                            e.printStackTrace()
                        }
                    }
                }.start()
                Utils.showLog(TAG, "====page===" + page)
                Utils.showLog(TAG, "=== page multiple===" + page * 20)
                getMyPostApiCall(page * 20)
                //getListingController.getHomePostList(locationName.toString(), loc_id, latitude.toString(), longitude.toString(), searchword, page * 20)
            }
        }

        rvMyPost.addOnScrollListener(featureRcvScrollListener!!)

        //check below api call in onresume
        //getMyPostApiCall()

     /*   if(list.size > 0) {
            ll_no_post.visibility = View.GONE
            ll_no_showcase.visibility = View.GONE
            bindRecyclerviewFuel()
        }
        else{
            ll_no_post.visibility = View.VISIBLE
            ll_no_showcase.visibility = View.GONE
        }*/


        //for no post
     /*   ll_no_post.visibility = View.VISIBLE
        ll_no_showcase.visibility = View.GONE

        btn_start_post_now.setOnClickListener {
            (activity as MyPostActivity).setAdPost()
        }*/

        btn_start_post_now.setOnClickListener {
            val storeUserData = StoreUserData(context!!)
            val from = storeUserData.getString(Constants.POST_FROM)
            if(from.equals("activity"))
            {
            (activity as MyPostActivity).setAdPost()
            }
            else{
                (activity as MainActivity).setPostAdTab()
            }
        }

        tvsellallpkg.setOnClickListener {
            val intent = Intent(context, AdPostPackagesActivity::class.java)
            startActivity(intent)
        }

        //getMyPostApiCall(0)


        txtFilterType.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                if (filterCategory.totalAllAds != null) {
                    showMenu(txtFilterType)
                }
            }
        })



        tvfromdate.setOnClickListener {

            openCalener(0)
        }

       tvtodate.setOnClickListener {
           openCalener(1)
       }

       tv_apply.setOnClickListener {


           if(tvfromdate.getText().toString().isEmpty() && tvtodate.getText().toString().isEmpty()){
               list.clear()
               adapter.removeItems()
               featureRcvScrollListener?.reset()
               getMyPostApiCall(0)
           }else if(tvfromdate.getText().toString().isEmpty() && tvtodate.getText().toString().isNotEmpty()){
               Toast.makeText(context, "Please select from date", Toast.LENGTH_SHORT).show()
           }else if(tvfromdate.getText().toString().isNotEmpty() && tvtodate.getText().toString().isEmpty()){
               Toast.makeText(context, "Please select to date", Toast.LENGTH_SHORT).show()
           }else{
               if(checkDate()) {
                   list.clear()
                   adapter.removeItems()
                   featureRcvScrollListener?.reset()
                   getMyPostApiCall(0)
               }
               else{
                   Toast.makeText(context, "Please select valid end date", Toast.LENGTH_SHORT).show()
               }
           }

           //original code
           /*if (tvfromdate.getText().toString().isEmpty()) {
               Toast.makeText(context, "Please select from date", Toast.LENGTH_SHORT).show()
           } else if (tvtodate.getText().toString().isEmpty()) {
               Toast.makeText(context, "Please select to date", Toast.LENGTH_SHORT).show()
           } else {
               if(checkDate()) {

                   list.clear()
                   adapter.removeItems()
                   featureRcvScrollListener?.reset()
                   getMyPostApiCall(0)
               }
               else{
                   Toast.makeText(context, "Please select valid end date", Toast.LENGTH_SHORT).show()
               }
           }*/

       }
    }

    private fun showMenu(menu: View) {
        val displayMetrics = DisplayMetrics()
        activity!!.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics)
        val popupWidth = LinearLayout.LayoutParams.WRAP_CONTENT
        val popupHeight = LinearLayout.LayoutParams.WRAP_CONTENT
        val layoutInflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layout: View = layoutInflater.inflate(R.layout.drop_down_menu, menu_layout)
        popupMenu(layout)
        popup = PopupWindow(layout, popupWidth, popupHeight)
        popup!!.setFocusable(true)
        popup!!.setOutsideTouchable(false)
        //popup!!.showAsDropDown(menu, 0, -300, Gravity.TOP)
        popup!!.showAsDropDown(menu)
    }

    private fun popupMenu(layout: View) {
        val menu: RecyclerView = layout.findViewById(R.id.menuList)
        menu.setHasFixedSize(true)
        val mLayoutManager = LinearLayoutManager(context)
        menu.layoutManager = mLayoutManager
        val adapter: MenuAdapter



        //val strMenu = arrayListOf<String>(*resources.getStringArray(R.array.my_ads_filter))

        val strMenu  = arrayListOf(
                "All Ads(" + filterCategory.totalAllAds.toString() + ")",
                "Live Ads(" + filterCategory.totalLiveAds.toString() + ")",
                "Expired Ads(" + filterCategory.totalExpiredAds.toString() + ")",
                "Premium Ads(" + filterCategory.totalPremiumAds.toString() + ")",
                "Ad Publish Date")
        adapter = MenuAdapter(strMenu, this)
        menu.adapter = adapter
    }


    override fun onMenuItemClicked(item: String?) {
        if (popup != null && popup!!.isShowing()) popup!!.dismiss()

        if (item != null) {
            if(item.contains("All")){
                filterCategotyType = FilterType.All_AD
                showFilteredList(1, item, true)
            }else if(item.contains("Live")){
                filterCategotyType = FilterType.LIVE_AD
                showFilteredList(2, item, true)
            }else if(item.contains("Expired")){
                filterCategotyType = FilterType.EXPIRED_AD
                showFilteredList(3, item, true)
            }else if(item.contains("Premium")){
                filterCategotyType = FilterType.PREMIUM_AD
                showFilteredList(4, item, true)
            }else if(item.contains("Publish")){
                filterCategotyType = FilterType.PUBLISH_DATE
                showFilteredList(5, item, true)
            }
        }
    }

    enum class FilterType : Serializable {
        All_AD, LIVE_AD, EXPIRED_AD, PREMIUM_AD, PUBLISH_DATE
    }

    private fun showFilteredList(i: Int, item: String, isFilter: Boolean) {

        if(isFilter){
            startDate = ""
            tvfromdate.setText("")
            tvfromdate.setBackgroundResource(R.drawable.bg_edittext)

            endDate = ""
            tvtodate.setText("")
            tvtodate.setBackgroundResource(R.drawable.bg_edittext)

        }

        if(i==1){
            rl_date.visibility = View.GONE
        }else{
            rl_date.visibility = View.VISIBLE
        }

        txtFilterName.setText(item)
        txtFilterType.setText(item)

        filterType=i
        list.clear()
        adapter.removeItems()
        featureRcvScrollListener?.reset()
        getMyPostApiCall(0)
    }

    /*fun onMenuItemClicked(item: String) {
        if (popup != null && popup.isShowing()) popup.dismiss()
        when (item.toLowerCase()) {
            "edit" -> listener.onEditPost(swaggerPosts.get(position), position)
            "delete" -> listener.onDeletePost(swaggerPosts.get(position))
            "hide" -> listener.onHidePost(swaggerPosts.get(position))
            else -> {
            }
        }
    }
*/

    private fun checkDate(): Boolean {
        try {
            val dateFormat = SimpleDateFormat("dd/MM/yyyy")
            var convertedDate: Date? = Date()
            var convertedDate2: Date? = Date()
            convertedDate = dateFormat.parse(tvfromdate.getText().toString())
            convertedDate2 = dateFormat.parse(tvtodate.getText().toString())
            assert(convertedDate2 != null)
            return if (convertedDate2.after(convertedDate)) {
                Log.e(TAG, "===after=== true")
                true
            }
            else if(convertedDate2.equals(convertedDate))
            {
              true
            }
            else{
                Log.e(TAG, "===after=== false")
                false
            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return false
    }

    private fun openCalener(i: Int) {
        val cldr = Calendar.getInstance()
        val day = cldr[Calendar.DAY_OF_MONTH]
        val month = cldr[Calendar.MONTH]
        val year = cldr[Calendar.YEAR]
        // date picker dialog
        picker = DatePickerDialog(
                context!!, R.style.DialogTheme,
                { datePicker, year, monthOfYear, dayOfMonth ->
                    if (i == 0) {
                        startDate =
                                year.toString() + "-" + (monthOfYear + 1) + "-" + dayOfMonth.toString()
                        tvfromdate.setText(dayOfMonth.toString() + "/" + (monthOfYear + 1) + "/" + year)
                        tvfromdate.setBackgroundResource(R.drawable.bg_edit_text_focused)
                    }
                    if (i == 1) {
                        endDate =
                                year.toString() + "-" + (monthOfYear + 1) + "-" + dayOfMonth.toString()
                        tvtodate.setText(dayOfMonth.toString() + "/" + (monthOfYear + 1) + "/" + year)
                        tvtodate.setBackgroundResource(R.drawable.bg_edit_text_focused)
                    }
                }, year, month, day
        )


        picker!!.setButton(DialogInterface.BUTTON_NEUTRAL, "Reset", object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
                if(i == 0){
                    startDate = ""
                    tvfromdate.setText("")
                    tvfromdate.setBackgroundResource(R.drawable.bg_edittext)
                }
                if(i == 1){
                    endDate = ""
                    tvtodate.setText("")
                    tvtodate.setBackgroundResource(R.drawable.bg_edittext)
                }
            }
        })


        picker!!.show()
    }

    private fun getMyPostApiCall(start: Int) {
        getListingController.getMyPost(start, limit, startDate.toString(), endDate.toString(), filterType,true)
    }

    private fun getMyPostFilters(){
        getListingController.getMyPostFilters()

    }

    private fun bindRecyclerviewFuel(list1: java.util.ArrayList<GetMyPostBean.getMyPostList>) {

        try {
            val recyclerLayoutManager = LinearLayoutManager(context)
            rv_mypost.itemAnimator = null
            rv_mypost.getRecycledViewPool().clear()
            adapter = MypostAdapter(context!!, this.list, "post")
            rv_mypost.layoutManager = recyclerLayoutManager
            rv_mypost.adapter = adapter
            //rv_mypost.smoothScrollToPosition(0)
            adapter.setClicklistner(this)
            adapter.notifyDataSetChanged()
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
        }

    }

    companion object {
        val TAG = "MyPostAdFragment"

        private var instance: MyPostAdFragment? = null

        fun newInstance(): MyPostAdFragment {
            return MyPostAdFragment()
        }

        @Synchronized
        fun getInstance(): MyPostAdFragment? {
            return instance
        }

    }

    override fun itemclickProduct(bean: GetMyPostBean.getMyPostList, type: String, position: Int) {

        if(type.equals("sellurgent")) {
            //check the package is already exists or not
            typePost = "premium"
            getListingController.getMyPackgesList(bean.category_id.toString())
            storeUserData.setString(Constants.LOC_ID_PKG, bean.location_id.toString())
            storeUserData.setString(Constants.LOCATION_ID, bean.location_id.toString())
            postdata = bean
            storeUserData.setString(Constants.FROM_DETAIL, "true")
        }
        else if(type.equals("editPrice"))
        {
            pos = position
            openEditPriceDialog(bean.post_id.toString())
        }
        else{
            storeUserData.setString(Constants.FROM_DETAIL, "true")
            val storeusedata = StoreUserData(context!!)
            storeusedata.setString(Constants.OPEN_POST_DETAIL, "my_post_ad")
            storeusedata.setString(Constants.CATEGORY_ID, bean.category_id.toString())
            storeusedata.setString(Constants.SUBCATEGORYID, bean.sub_category_id.toString())
            CheckCategoryForPostDetails.OpenScreen(
                    context!!,
                    bean.category_id!!.toInt(),
                    bean.sub_category_id!!.toInt(),
                    bean.post_id!!.toInt()
            )
        }

    }

    private fun openEditPriceDialog(postid: String) {

        dialog = Dialog(context!!)
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_edit_price)
        // Set dialog title
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.getWindow()!!.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );
        // set values for custom dialog components - text, image and button
        val ivclose = dialog.findViewById<ImageView>(R.id.ivclose)
        val etprice = dialog.findViewById<EditText>(R.id.et_price)
        val btnedit = dialog.findViewById<Button>(R.id.btn_next)
        ivclose.setOnClickListener {
            dialog.dismiss()

            getActivity()!!.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        }

        btnedit.setOnClickListener {
            if(etprice.text.length == 0)
            {
                etprice.setError("Required Field")
            }
            else {

                if(etprice.text.startsWith("0"))
                {
                    Utils.showToast(activity, getString(R.string.txt_valid_price))
                    return@setOnClickListener
                }
                else {
                    val c = EditPricePostController(
                            this,
                            activity!!,
                            etprice.text.toString(),
                            postid
                    )
                    c.onClick(btnedit)
                }

                dialog.dismiss()
            }
        }

        dialog.show()
    }

    override fun itemclickDelete(dataModel: GetMyPostBean.getMyPostList, position: Int) {

        myPostShowcaseDeleteController.add("post", dataModel.post_id)
    }

    override fun itemclickLiveAgain(dataModel: GetMyPostBean.getMyPostList) {
        storeUserData.setString(Constants.LOC_ID_PKG, dataModel.location_id.toString())
        storeUserData.setString(Constants.LOCATION_ID, dataModel.location_id.toString())
        storeUserData.setString(Constants.LOC_NAME, dataModel.location_name.toString())
        post_id = dataModel.post_id.toString()
        cat_id = dataModel.category_id.toString()
        storeUserData.setString(Constants.POST_ID, post_id)
        typePost = "liveagain"

        openRepostDialog(cat_id)
    }

    private fun openRepostDialog(catId: String) {
        dialog = Dialog(activity!!)
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_logout)
        // Set dialog title
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        //dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_white);
        // set values for custom dialog components - text, image and button
        val title = dialog.findViewById<TextView>(R.id.tv_title)
        val btnyes = dialog.findViewById<Button>(R.id.btn_yes)
        val btnno = dialog.findViewById<Button>(R.id.btn_no)
        title.text =context!!.getString(R.string.txt_warning_repost)

        btnyes.setOnClickListener{

            pkglist.clear()
            pkglist1.clear()
            getListingController.getMyPackgesList(catId)

            dialog.dismiss()
        }

        btnno.setOnClickListener { dialog.dismiss()}

        dialog.show()
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {

        if (method.equals("GetMyPackages")) {
            Utils.dismissProgress()

            val dataModel = GetMyPackagesBean()
            dataModel.GetMyPackagesBean(da)

            if(typePost.equals("premium")) {
                pkglist.clear()
                pkglist1.clear()

                pkglist = dataModel.premiumpkglist
                Utils.showLog(TAG, "==my pckg list size==" + pkglist.size)

                Utils.showLog(
                        TAG, "==pkg list==" + GsonBuilder().setPrettyPrinting().create().toJson(
                        pkglist
                )
                )

                if (pkglist.size > 0) {
                    Utils.showLog(TAG, "==my pckg list size==" + pkglist.size)

                    for (i in pkglist.indices) {
                        if (pkglist.get(i).remaiingpost!!.toInt() > 0) {
                            pkglist1.add(pkglist.get(i))
                        }
                    }

                    if (pkglist1.size > 0) {
                        pkglist1.sortWith(Comparator { o1, o2 -> o1.pkgvalidity!!.compareTo(o2.pkgvalidity!!) })
                        Utils.showLog(
                                TAG,
                                "==sorted list==" + GsonBuilder().setPrettyPrinting().create().toJson(
                                        pkglist1
                                )
                        )


                        val intent = Intent(context, SelectPremiumPackageActivity::class.java)
                        intent.putExtra("pkglist", pkglist1)
                        intent.putExtra("postId", postdata.post_id.toString());
                        intent.putExtra("data", postdata)
                        intent.putExtra("catId", postdata.category_id.toString());
                        startActivity(intent)
                        //activity!!.finish()
                    } else {
                        openSellUrgentScreen()
                    }

                }
                else {
                    openSellUrgentScreen()
                }
            }
            else{
                pkglist = dataModel.freepkglist
                Utils.showLog(TAG, "==my pckg list size==" + pkglist.size)

                Utils.showLog(
                        TAG, "==pkg list==" + GsonBuilder().setPrettyPrinting().create().toJson(
                        pkglist
                )
                )

                if(pkglist.size > 0)
                {
                    for (i in pkglist.indices) {
                        if (pkglist.get(i).remaiingpost!!.toInt() > 0) {
                            pkglist1.add(pkglist.get(i))
                        }
                    }

                    if(pkglist1.size > 0) {
                        pkglist1.sortWith(Comparator { o1, o2 -> o1.pkgvalidity!!.compareTo(o2.pkgvalidity!!) })

                        // val sortedModel= Collections.sort(pkglist, comparator)
                        Utils.showLog(
                                TAG,
                                "==sorted list==" + GsonBuilder().setPrettyPrinting().create().toJson(
                                        pkglist1
                                )
                        )
                        val fpkglist: ArrayList<GetMyPackagesBean.Packages> = ArrayList()
                        newpkglist.clear()
                        for(l in pkglist1.indices)
                        {
                            if(pkglist1.get(l).packagetype.equals("Free"))
                            {
                                fpkglist.add(pkglist1.get(l))
                            }

                            if (pkglist1.get(l).packagetype.equals("More")) {
                                newpkglist.add(pkglist1.get(l))
                            }
                        }

                        if(fpkglist.size > 0)
                        {
                            Utils.showLog(
                                    TAG,
                                    "==ad post free===" + GsonBuilder().setPrettyPrinting().create()
                                            .toJson(
                                                    fpkglist.get(
                                                            0
                                                    )
                                            )
                            )

                            packageid = fpkglist.get(0).packageid.toString()
                            userpostpackageid = fpkglist.get(0).userpostpackageid.toString()
                            storeUserData.setString(
                                    Constants.USER_PKG_ID,
                                    userpostpackageid.toString()
                            )
                            type = "normal"
                            posttype = "free"
                            PostLiveAgainController(context!!, post_id, packageid.toString(), this)
                        }
                        else
                        {
                            Utils.showLog(
                                    TAG,
                                    "==new list==" + GsonBuilder().setPrettyPrinting().create().toJson(
                                            newpkglist
                                    )
                            )
                            if (newpkglist.size > 0) {
                                newpkglist.sortWith(Comparator { o1, o2 ->
                                    o1.pkgvalidity!!.compareTo(
                                            o2.pkgvalidity!!
                                    )
                                })
                                Utils.showLog(
                                        TAG,
                                        "==sorted list==" + GsonBuilder().setPrettyPrinting().create()
                                                .toJson(
                                                        pkglist1
                                                )
                                )

                                // openPkgSelectionScreen(pkglist1.size)

                                val intent = Intent(
                                        context,
                                        SelectExtraAdPackageActivity::class.java
                                )
                                //intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                intent.putExtra("pkglist", newpkglist)
                                intent.putExtra("postId", post_id)
                                intent.putExtra("catId", cat_id)
                                startActivity(intent)
                            }
                            else{
                                openAttentionScreen(post_id, cat_id)
                            }
                        }
                    }
                    else {
                        newpkglist.clear()
                        Utils.showLog(
                                TAG, "==else=" + GsonBuilder().setPrettyPrinting().create().toJson(
                                pkglist1
                        )
                        )
                        //val newpkglist: ArrayList<GetMyPackagesBean.Packages> = ArrayList()

                        for (k in pkglist1.indices) {
                            if (pkglist1.get(k).packagetype.equals("More")) {
                                newpkglist.add(pkglist1.get(k))
                            }
                        }

                        Utils.showLog(
                                TAG, "==new list==" + GsonBuilder().setPrettyPrinting().create().toJson(
                                newpkglist
                        )
                        )
                        if (newpkglist.size > 0) {
                            newpkglist.sortWith(Comparator { o1, o2 -> o1.pkgvalidity!!.compareTo(o2.pkgvalidity!!) })
                            Utils.showLog(
                                    TAG,
                                    "==sorted list==" + GsonBuilder().setPrettyPrinting().create()
                                            .toJson(
                                                    pkglist1
                                            )
                            )

                            // openPkgSelectionScreen(pkglist1.size)

                            val intent = Intent(context, SelectExtraAdPackageActivity::class.java)
                            //intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            intent.putExtra("pkglist", newpkglist)
                            intent.putExtra("postId", post_id)
                            intent.putExtra("catId", cat_id)
                            startActivity(intent)
                        }
                        else{
                            openAttentionScreen(post_id, cat_id)
                        }
                    }
                }

                else {
                    openAttentionScreen(post_id, cat_id)
                }
            }

        }

        if (method.equals("getMyPost")) {
            Utils.dismissProgress()
            progressBar.setVisibility(View.GONE)
            progressStatus = 0

            val marginLayoutParams = RelativeLayout.LayoutParams(rvMyPost.getLayoutParams())
            marginLayoutParams.setMargins(0, 0, 0, 0)
            rvMyPost.setLayoutParams(marginLayoutParams)

            list.clear()
            //adapter.notifyDataSetChanged()

            val dataModel = GetMyPostBean()
            dataModel.GetMyPostBean(da)

            if(da.has("totalResponseCount")){
                totalPostCount = da.getString("totalResponseCount")
                Log.e(TAG, "MYADS1 Total Item Size: $totalPostCount")

                setFilterDataCounter(totalPostCount)

            }

            //list = dataModel.getMyPostBeanlist
            Log.e(TAG, "Response Item Size: ${dataModel.getMyPostBeanlist.size}")
            list.addAll(dataModel.getMyPostBeanlist)
            if(!list.isEmpty() && list.size >0)
            {

                if(rl_no_result_found!=null && ll_no_post!=null && ll_no_showcase!=null && rlpost!=null){
                    Handler(Looper.getMainLooper()).postDelayed({
                        rl_no_result_found.visibility = View.GONE
                        ll_no_post.visibility = View.GONE
                        ll_no_showcase.visibility = View.GONE
                        rvMyPost.visibility = View.VISIBLE
                        rlpost.visibility = View.VISIBLE
                        adapter.addItems(list)
                        rvMyPost.setItemViewCacheSize(adapter.itemCount)
                    }, 100)
                }


                Log.e(
                        TAG,
                        "Item : ${dataModel.getMyPostBeanlist.size} RcvItemCount : ${adapter.itemCount}"
                )
            }

            //checkResponse()
        }

        if(method.equals("getMyPostFilters")) {
            filterCategory = AdsFilterCategoryBean()
            filterCategory.AdsFilterCategoryBean(da)
        }

        if (method.equals("deleteMyPost")) {
            Utils.dismissProgress()
            Utils.showToast(activity, message)
            list.clear()
            adapter.removeItems()
            featureRcvScrollListener?.reset()
            getMyPostApiCall(0)
        }

        if (method.equals("ads")) {
            Utils.dismissProgress()
            Utils.showToast(activity, message)
            list.clear()
            adapter.removeItems()
            featureRcvScrollListener?.reset()
            getMyPostApiCall(0)
        }

        if(method.equals("editPrice"))
        {
            getActivity()!!.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
            Utils.dismissProgress()
            Utils.showToast(activity, message)
            list.clear()
            adapter.removeItems()
            featureRcvScrollListener?.reset()
            getMyPostApiCall(0)
        }
        if(method.equals("rePost"))
        {
            try{
            Utils.showToast(activity, "Your Ad Posted Successfully")
            val dataModel = AdPostResponseBean()
            dataModel.AdPostResponseBean(da)

            Utils.showLog(TAG, "===data===" + dataModel)
            val data = dataModel.pkglist.get(0)






                storeUserData.setString(Constants.USER_PKG_ID, "")
                storeUserData.setString(Constants.FROM_DETAIL, "false")
                val intent = Intent(context, CongratulationAdpostActivity::class.java)
                intent.putExtra("data", data)
                intent.putExtra("type", posttype)
                startActivity(intent)
                activity!!.finishAffinity()


            /*Handler().postDelayed({
                if (!activity!!.isFinishing) {
                    storeUserData.setString(Constants.USER_PKG_ID, "")
                    storeUserData.setString(Constants.FROM_DETAIL, "false")
                    val intent = Intent(context, CongratulationAdpostActivity::class.java)
                    intent.putExtra("data", data)
                    intent.putExtra("type", posttype)
                    startActivity(intent)
                    activity!!.finishAffinity()
                }
            }, 2000)*/
            }
            catch (e: NullPointerException)
            {
                e.printStackTrace()
            }
        }
    }

    private fun setFilterDataCounter(totalPostCount: String?) {

        if(txtFilterType!=null){
            if(filterCategotyType.equals(FilterType.All_AD)){
                txtFilterType.setText("All Ads(" + totalPostCount + ")")
                txtFilterName.setText("All Ads(" + totalPostCount + ")")
            }else if(filterCategotyType.equals(FilterType.LIVE_AD)){
                txtFilterName.setText("Live Ads(" + totalPostCount + ")")
            }else if(filterCategotyType.equals(FilterType.EXPIRED_AD)){
                txtFilterName.setText("Expired Ads(" + totalPostCount + ")")
            }else if(filterCategotyType.equals(FilterType.PREMIUM_AD)){
                txtFilterName.setText("Premium Ads(" + totalPostCount + ")")
            }else if(filterCategotyType.equals(FilterType.PUBLISH_DATE)){
                txtFilterName.setText("Ad Publish Date(" + totalPostCount + ")")
            }
        }
    }

    private fun openPkgSelectionScreen(size: Int) {
        val totalsize = pkglist1.size
        Utils.showLog(TAG, "===list size ==" + totalsize + "=== number===" + size)
        if(totalsize == size)
        {
            val intent = Intent(context, SelectExtraAdPackageActivity::class.java)
            //intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            intent.putExtra("pkglist", newpkglist)
            intent.putExtra("postId", post_id)
            intent.putExtra("catId", cat_id)
            startActivity(intent)
        }
    }

    private fun openAttentionScreen(postId: String, catid: String) {
        val intent = Intent(context, AttentionAdPostActivity::class.java)
        intent.putExtra("postId", postId)
        intent.putExtra("catId", catid)
        startActivity(intent)
    }

    override fun onFail(msg: String, method: String) {
        Utils.dismissProgress()
        try{

        if (method.equals("getMyPost"))
        {
            setFilterDataCounter("0")

            if(txtFilterType.text.toString().contains("All")){
                ll_no_post.visibility = View.VISIBLE
                rl_no_result_found.visibility = View.GONE
            }else{
                ll_no_post.visibility = View.GONE
                rl_no_result_found.visibility = View.VISIBLE
            }

            ll_no_showcase.visibility = View.GONE
        }
        if (method.equals("editPrice")) {
            //getActivity()!!.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
            Utils.dismissProgress()
            Utils.showToast(activity, msg)
        }
        if (method.equals("ads")) {
               //getActivity()!!.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
               Utils.dismissProgress()
               Utils.showToast(activity, msg)
         }

        //ll_no_post.visibility = View.VISIBLE

            if(txtFilterType.text.toString().contains("All")){
                ll_no_post.visibility = View.VISIBLE
                rl_no_result_found.visibility = View.GONE
            }else{
                ll_no_post.visibility = View.GONE
                rl_no_result_found.visibility = View.VISIBLE
            }

        ll_no_showcase.visibility = View.GONE
        }
        catch (e: IllegalStateException)
        {
            e.printStackTrace()
        }
        catch (e: NullPointerException)
        {
            e.printStackTrace()
        }
        }

    private fun openSellUrgentScreen() {

        storeUserData.setString(Constants.POST_ID, postdata.post_id.toString())
        val intent = Intent(context, SellUrgentDetailActivity::class.java)
        intent.putExtra("data", postdata)
        intent.putExtra("catId", postdata.category_id.toString());
        intent.putExtra("postId", postdata.post_id.toString());
        intent.putExtra("locationId", postdata.location_id.toString());
        startActivity(intent)
        //activity!!.finish()
    }

    private fun checkResponse() {
        try {
            if (list.size > 0) {
                ll_no_post.visibility = View.GONE
                ll_no_showcase.visibility = View.GONE

                bindRecyclerviewFuel(list)

            } else {

                ll_no_post.visibility = View.VISIBLE
                ll_no_showcase.visibility = View.GONE
            }
        }
        catch (e: IllegalStateException)
        {
            e.printStackTrace()
        }
        catch (e: NullPointerException)
        {
            e.printStackTrace()
        }
    }


    override fun onResume() {
        Utils.showLog(TAG, "==on resume===")
        //list.clear()
        val isfrom = storeUserData.getString(Constants.FROM_DETAIL)
        Utils.showLog(TAG, "====screen is from detail ===" + isfrom)
        if(isfrom.equals("true"))
        {
            storeUserData.setString(Constants.FROM_DETAIL, "false");
        }
        else {
            try {
                getMyPostFilters()
                list.clear()
                adapter.removeItems()
                featureRcvScrollListener?.reset()
                getMyPostApiCall(0)
            }catch (e: Exception){
                e.printStackTrace()
            }

        }

        super.onResume()
    }

    fun callApiToRefreshDeletedPost(i: String) {

        DeleteMultiAdPostsController(activity!!, i, this)

    }

    /* override fun setUserVisibleHint(isVisibleToUser: Boolean) {
         super.setUserVisibleHint(isVisibleToUser)
         if (isVisibleToUser) {
             // Refresh your fragment here
             fragmentManager!!.beginTransaction().detach(this).attach(this).commit()
         }
     }*/
}