package com.kaushlesh.view.fragment

import android.Manifest
import android.Manifest.permission.*
import android.annotation.SuppressLint
import android.content.*
import android.content.pm.PackageManager
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.*
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.kaushlesh.Controller.AddFavPostController
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.Controller.PhoneCallController
import com.kaushlesh.Controller.PurchasePackageController
import com.kaushlesh.R
import com.kaushlesh.adapter.CategoryAdapterNew
import com.kaushlesh.adapter.RcvListViewAdapter
import com.kaushlesh.adapter.RecommendationAdapter
import com.kaushlesh.bean.*
import com.kaushlesh.bean.ads.SliderAdapter
import com.kaushlesh.bean.chat.ChatNewList
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.reusable.EndlessScrollRecyclListener
import com.kaushlesh.reusable.LocationAddress
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.Identity
import com.kaushlesh.utils.PostDetail.CheckCategoryForAdPostDetailRedirection
import com.kaushlesh.utils.PostDetail.CheckCategoryForPostDetails
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.ChatActivity
import com.kaushlesh.view.activity.Login.LoginMainActivity
import com.kaushlesh.view.activity.MainActivity
import com.kaushlesh.view.activity.NotificationsActivity
import com.kaushlesh.view.filters.HomeSearch.HomeProductArrayAdapter
import com.kaushlesh.view.filters.HomeSearch.HomeSearchModel
import kotlinx.android.synthetic.main.fragment_home_new.*
import kotlinx.android.synthetic.main.item_row_fresh_recommendation.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.net.URLDecoder
import java.util.*
import kotlin.collections.ArrayList


class HomeFragmentNew: Fragment(),ParseControllerListener ,CategoryAdapterNew.ItemClickListener,SliderAdapter.ItemClickListener,
    RcvListViewAdapter.ItemClickListener,RecommendationAdapter.ItemClickListener,View.OnClickListener
,DistrictListDialogActivity.BottomSheetListener{

    private var scrollStart: Boolean = false
    internal var actlocation: AutoCompleteTextView? = null
    //internal var svfind: androidx.appcompat.widget.SearchView? = null
    private var scrollListener: EndlessScrollRecyclListener? = null
    lateinit var rcvListView : RecyclerView
    lateinit var rcvListViewAdapter : RcvListViewAdapter
    lateinit var getListingController: GetListingController
    lateinit var ll_location : LinearLayout
    lateinit var tv_noti_count: TextView
    lateinit var userId: String
    lateinit var userToken: String
    var chatCount: Int = 0
    var k: Int = 0
    internal lateinit var placesClient: PlacesClient
    internal var latitude: Double = 0.toDouble()
    internal var longitude: Double = 0.toDouble()
    internal var curlatitude: Double = 0.toDouble()
    internal var curlongitude: Double = 0.toDouble()
    internal var addlatitude: Double = 0.toDouble()
    internal var addlongitude: Double = 0.toDouble()

    var locationName : String = ""
    var searchword : String = ""
    var loc_id : String = ""
    private var fusedLocationClient: FusedLocationProviderClient? = null
    var freshrecomlist: ArrayList<RecommendationBean.FreshProductListBean> = arrayListOf()
    private var progressStatus = 0
    private var handler: Handler = Handler()
    lateinit var progressBar: ProgressBar
    private val locationPermission = ACCESS_FINE_LOCATION
    lateinit var storeusedata: StoreUserData
    lateinit var filterbean : HomeFilterLocationBean
    lateinit var lastLocation : String
    lateinit var ivRefresh : ImageView
    var pageNo: Int = 0
    var radius: Int = 70
    lateinit var rlnopost : RelativeLayout

    var notificationCounter = "0"

    var mQueryString = ""
    private var searchHandler: Handler = Handler()

    lateinit var autoTextView: AutoCompleteTextView

    //broadcast
    protected var localBroadcastManager: LocalBroadcastManager? = null

    //slider images
    var contactAds: String = ""
    var whatsappAds: String = ""
    var websiteAds: String = ""

    //var adsTabLayout: TabLayout? = null


    private var locationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val locationList = locationResult.locations
            if (locationList.isNotEmpty()) {
                //The last location in the list is the newest
                val location = locationList.last()
                //latitude = location.latitude
                //longitude = location.longitude
                curlatitude = location.latitude
                curlongitude = location.longitude
                Log.i(TAG, "--- LATITUDE ---$curlatitude---LONGITUDE---$curlongitude")
                val locationAddress = LocationAddress()
                locationAddress.getAddressFromLocation(
                    curlatitude,
                    curlongitude,
                    context!!,
                    GeoCodeHandler()
                )
                //Toast.makeText(context, "Got Location: " + location.toString(), Toast.LENGTH_LONG).show()
            }
        }
    }

    companion object {

        val TAG = "HomeFragmentNew"
        fun newInstance(): HomeFragmentNew {
            return HomeFragmentNew()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_new, container, false)
    }

    private val mLikeDislikePostReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.getStringExtra("islikedState") != null && intent.getStringExtra("postIdLikeDislike") != null) {
                val likeState = intent.getStringExtra("islikedState")
                val postIdLikeDislike = intent.getStringExtra("postIdLikeDislike")

                Log.d(
                    "CLICKED_ITEM",
                    " Updated : POST_ID - " + postIdLikeDislike + " - notifyStatus - " + likeState
                )

                updateLikeDislikeStatus(likeState, postIdLikeDislike)

            }
        }
    }

    fun updateLikeDislikeStatus(likeState: String?, postIdLikeDislike: String?) {
        try {

            if(!rcvListViewAdapter.getAllItems().isEmpty()){
                rcvListViewAdapter.updateLikeDislikePost(likeState, postIdLikeDislike)
            }
        }catch (e: Exception){
            e.printStackTrace()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        localBroadcastManager!!.unregisterReceiver(mLikeDislikePostReceiver)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        localBroadcastManager = LocalBroadcastManager.getInstance(activity!!)
        localBroadcastManager!!.registerReceiver(
            mLikeDislikePostReceiver,
            IntentFilter("NOTIFY_POST_LIKE_DISLIKE")
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Utils.showLog(TAG, "==encoded===" + URLDecoder.decode("જોબ", "UTF-8"))

        storeusedata = StoreUserData(context!!)
        getListingController = GetListingController(activity!!, this)

        filterbean = HomeFilterLocationBean()
        //Focus Location Client for user location
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity!!)

        userId = storeusedata.getString(Constants.USER_ID)
        userToken = storeusedata.getString(Constants.TOKEN)

        Utils.showLog(TAG, "==userid==" + userId + "== userToken==" + userToken)

        rcvListView = view.findViewById(R.id.rcvList)
        actlocation = view.findViewById(R.id.act_location)
        ll_location = view.findViewById(R.id.ll_loc)
        tv_noti_count = view.findViewById(R.id.tv_noti_count)
        //svfind = view.findViewById(R.id.sv_find)
        progressBar = view.findViewById(R.id.preogressbar)
        ivRefresh = view.findViewById(R.id.iv_refresh)
        rlnopost = view.findViewById(R.id.rl_no_post)
        autoTextView = view.findViewById(R.id.autoTextView)

        //slider images
        //adsTabLayout = view.findViewById(R.id.advertisement_tablayout)

        Utils.setontouchAuto(act_location)

      /*  if(actlocation!!.text.toString().length > 30) {
            val marquee: Animation = AnimationUtils.loadAnimation(context, R.anim.marquee)
            actlocation!!.startAnimation(marquee)
        }*/

        rl_chat.setOnClickListener(this)
        rl_noti.setOnClickListener(this)
        ll_location.setOnClickListener(this)
        act_location.setOnClickListener(this)
        ivRefresh.setOnClickListener(this)

        val dr = resources.getDrawable(R.drawable.ic_down)
        val bitmap: Bitmap = (dr as BitmapDrawable).getBitmap()
        val ddr: Drawable = BitmapDrawable(
            resources, Bitmap.createScaledBitmap(
                bitmap,
                15,
                10,
                true
            )
        )
        actlocation!!.setCompoundDrawablePadding(10)
        actlocation!!.setCompoundDrawablesWithIntrinsicBounds(null, null, ddr, null)

        val layoutManager = GridLayoutManager(context, 2)
        layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return when (position) {
                    //old logic before sponsored ads
                    /*0, 1, 2, 3 -> 2
                    else -> 1*/

                    0, 1, 2, 3, 4 -> 2
                    else -> 1
                }
            }
        }
        rcvListView.layoutManager = layoutManager

        rcvListViewAdapter = RcvListViewAdapter(context!!, this, this, this,this,requireActivity())

        rcvListView.adapter = rcvListViewAdapter

        getListingController.getCategoryList()

        //home advertisments
        getListingController.getHomeAdvertisements()



        scrollListener = object : EndlessScrollRecyclListener()
        {
            override fun onLoadMore(page: Int, totalItemsCount: Int) {
                Log.e("Pagination", "onLoadMore Invocke : ${page}")

                val marginLayoutParams = LinearLayout.LayoutParams(rcvListView.getLayoutParams())
                //val marginLayoutParams = ViewGroup.MarginLayoutParams(rcvListView.getLayoutParams())
                marginLayoutParams.setMargins(0, 0, 0, 130)
                rcvListView.setLayoutParams(marginLayoutParams)
                Log.e("progressBar1 : ", "222")
                progressBar.setVisibility(View.VISIBLE)
                Thread {
                    while (progressStatus < 100) {
                        progressStatus +=20
                        handler.post(Runnable {
                            Log.e("progressBar1 : ", "333 progressStatus " + progressStatus)
                            progressBar.progress = progressStatus
                        })
                        try {
                            // Sleep for 200 milliseconds.
                            Thread.sleep(100)
                        } catch (e: InterruptedException) {
                            e.printStackTrace()
                        }
                    }
                }.start()

                pageNo = page * 20
                radius = (page+1) * 70
                Utils.showLog(TAG, "====page===" + page)
                Utils.showLog(TAG, "=== page multiple===" + page * 20)
                if(loc_id.length == 0) {


                    if(page == 1){
                        freshrecomlist.clear()
                        rcvListViewAdapter.removeHomeItems()

                        callApitoGetHomeFreshPost(
                                locationName.toString(),
                                loc_id,
                                curlatitude.toString(),
                                curlongitude.toString(),
                                searchword,
                                0,
                                radius,
                                true
                        )

                    }else{
                        callApitoGetHomeFreshPost(
                                locationName.toString(),
                                loc_id,
                                curlatitude.toString(),
                                curlongitude.toString(),
                                searchword,
                                (page-1) * 20,
                                radius,
                                true
                        )
                    }



                }
                else{

                    Utils.showLog("DUPLICATE_ISSUE", "222")
                    Utils.showLog("DUPLICATE_ISSUE", "Location set Page : "+page)

                    if(page == 1){

                        freshrecomlist.clear()
                        rcvListViewAdapter.removeHomeItems()

                        callApitoGetHomeFreshPost(
                                locationName.toString(),
                                loc_id,
                                addlatitude.toString(),
                                addlongitude.toString(),
                                searchword,
                                0,
                                radius,
                                true
                        )


                    }else{
                        callApitoGetHomeFreshPost(
                                locationName.toString(),
                                loc_id,
                                addlatitude.toString(),
                                addlongitude.toString(),
                                searchword,
                                (page-1) * 20,
                                radius,
                                true
                        )
                    }



                }
                //getListingController.getHomePostList(locationName.toString(), loc_id, latitude.toString(), longitude.toString(), searchword, page * 20)
            }
        }

        rcvListView.addOnScrollListener(scrollListener!!)


        callApiForFilter()

        //check if any pending package is stored in local....if yes than upload it to the server

        if(Utils.isConnected()){
            checkIfAnyPendingPackageAvailableToUpload();
        }

        //takepermissionNew() // original added here



    }

    private fun checkIfAnyPendingPackageAvailableToUpload() {
        val pendingPackage = Identity.getPackagePurchase(context)

        if(pendingPackage != ""){
            val gson = Gson()
            val obj: JSONObject = gson.fromJson(pendingPackage, JSONObject::class.java)

            val packageId = obj.get("packageId")
            val transactionId = obj.get("transactionId")
            val location_id = obj.get("location_id")

            Log.d(
                TAG, "PurchasePackageError {PARAMS} " +
                        " packageID: " + packageId +
                        " transactionID: " + transactionId +
                        " location_id: " + location_id
            )

            val c = PurchasePackageController(
                activity!!,
                packageId.toString(),
                transactionId.toString(),
                location_id.toString(),
                this
            )
            c.purchasePackage()

        }



    }


    fun isAlpha(name: String): Boolean {
        val chars = name.toCharArray()
        for (c in chars) {
            if (!Character.isLetter(c)) {
                return false
            }
        }
        return true
    }

    /*fun isAlpha(name: String): Boolean {
        return name.matches("[a-zA-Z]+")
    }*/

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun takepermissionNew() {

        val permissionList = arrayListOf<String>()
        permissionList.add(INTERNET)
        permissionList.add(Manifest.permission.CAMERA)
        permissionList.add(READ_EXTERNAL_STORAGE)
        permissionList.add(WRITE_EXTERNAL_STORAGE)
        permissionList.add(CALL_PHONE)
        permissionList.add(ACCESS_COARSE_LOCATION)
        permissionList.add(ACCESS_FINE_LOCATION)




        Dexter.withActivity(activity)
                .withPermissions(
                    permissionList

                    /*arrayListOf(Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.CALL_PHONE,
                                Manifest.permission.ACCESS_BACKGROUND_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION)*/

                )
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            Utils.showLog(TAG, "=== All permissions Granted ====")
                            checkLocation()
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied) {
                            // permission is denied permenantly, navigate user to app settings
                            //grantAllPermissions()

                            Utils.showLog(TAG, "=== isAnyPermissionPermanentlyDenied ====")

                            showSettingsDialog()
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permissions: List<PermissionRequest>, token: PermissionToken
                    ) {
                        token.continuePermissionRequest()
                        //showSettingsDialog()
                        Utils.showLog(TAG, "=== onPermissionRationaleShouldBeShown ====")

                    }
                })
                .withErrorListener { error ->
                    Toast.makeText(
                        context,
                        "Error occurred! $error",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                .onSameThread()
                .check()





















        /*val permissionList = arrayListOf<String>()
        permissionList.add(INTERNET)
        permissionList.add(Manifest.permission.CAMERA)
        permissionList.add(READ_EXTERNAL_STORAGE)
        permissionList.add(WRITE_EXTERNAL_STORAGE)
        permissionList.add(CALL_PHONE)
        permissionList.add(ACCESS_COARSE_LOCATION)
        permissionList.add(ACCESS_FINE_LOCATION)
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            permissionList.add(ACCESS_BACKGROUND_LOCATION)
        }
        Utils.showLog(TAG, "===take permission====")

        //Dexter.withContext(requireContext()).withPermissions(permissionList).withListener(this).check()

        Dexter.withContext(context)
                .withPermissions(permissionList).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(multiplePermissionsReport: MultiplePermissionsReport) {
                    if (multiplePermissionsReport.areAllPermissionsGranted()) {
                        Utils.showLog(TAG, "===permission checked====")
                        //getUserLocation()
                        checkLocation()
                        return
                    }
                    if (multiplePermissionsReport.isAnyPermissionPermanentlyDenied) {
                        Utils.showLog(TAG, "===permission denied====")
                        //showSettingsDialog()
                    }

                }

                override fun onPermissionRationaleShouldBeShown(
                    list: List<PermissionRequest>, permissionToken: PermissionToken) {
                    Utils.showLog(TAG, "===permission continue====")
                    permissionToken.continuePermissionRequest()
                    *//*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                            showSettingsDialog()
                        }*//*
                    //showSettingsDialog()
                }

            }).withErrorListener { error ->
                    Toast.makeText(
                            context,
                            "Error occurred! $error",
                            Toast.LENGTH_SHORT
                    ).show()
                }
                .onSameThread().check()*/
    }











    private fun takeRefreshPermissionNew() {

        val permissionList = arrayListOf<String>()
        permissionList.add(Manifest.permission.INTERNET)
        permissionList.add(Manifest.permission.ACCESS_COARSE_LOCATION)
        permissionList.add(Manifest.permission.ACCESS_FINE_LOCATION)

        //original added
        /*if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
        {
            permissionList.add(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
        }*/



        Dexter.withActivity(activity)
                .withPermissions(
                    permissionList
                )
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            Utils.showLog(TAG, "=== All permissions Granted ====")
                            checkLocation()
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied) {
                            // permission is denied permenantly, navigate user to app settings
                            //grantAllPermissions()

                            Utils.showLog(TAG, "=== isAnyPermissionPermanentlyDenied ====")

                            //showSettingsDialog()
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permissions: List<PermissionRequest>, token: PermissionToken
                    ) {
                        token.continuePermissionRequest()
                        //showSettingsDialog()
                        Utils.showLog(TAG, "=== onPermissionRationaleShouldBeShown ====")

                    }
                })
                .withErrorListener { error ->
                    Toast.makeText(
                        context,
                        "Error occurred! $error",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                .onSameThread()
                .check()

    }










    private fun callApiForFilter(){
        getListingController.getHomeFilterList()
    }

    private fun callApitoGetHomeFreshPost(
        locationName: String,
        loc_id: String,
        latitude: String,
        longitude: String,
        searchword: String,
        page: Int,
        radius: Int,
        showProgrss: Boolean
    ) {

        getListingController.getHomePostList(
            locationName.toString().replace(" ", ""),
            loc_id,
            latitude.toString(),
            longitude.toString(),
            searchword,
            page,
            radius,
            showProgrss
        )

        //getListingController.getHomePostList(locationName.toString(), loc_id, latitude.toString(), longitude.toString(), searchword, page,radius)

        filterbean.locationName = locationName
        filterbean.loc_id = loc_id
        filterbean.latitude = latitude
        filterbean.longitude= longitude
        filterbean.searchword = searchword

        val gson = Gson()
        val json = gson.toJson(filterbean)
        storeusedata.setString(Constants.HOME_LOC_FILTER, json)
        storeusedata.setBoolean(Constants.GET_LOCA_DATA, true)
    }

    private fun setSearchAdapter(homeSearchList: ArrayList<HomeSearchBean.HomeSearch>) {

        autoTextView.setThreshold(1);

        val searchFilteredList: ArrayList<HomeSearchModel> = ArrayList()

        for (i in 0 until homeSearchList.size) {

            val dataModel = HomeSearchModel()
            dataModel.categoryID = homeSearchList.get(i).categoryId
            dataModel.caregoryName = homeSearchList.get(i).categoryName
            dataModel.filterCategoryName = homeSearchList.get(i).categoryName
            searchFilteredList.add(dataModel)
            for (j in 0 until homeSearchList.get(i).subCategory.size) {
                val dataModel2 = HomeSearchModel()
                dataModel2.categoryID = homeSearchList.get(i).categoryId
                dataModel2.caregoryName = homeSearchList.get(i).categoryName
                dataModel2.subCategoryID = homeSearchList.get(i).subCategory.get(j).subcategoryId
                dataModel2.subCaregoryName = homeSearchList.get(i).subCategory.get(j).subcategoryName
                dataModel2.filterCategoryName = homeSearchList.get(i).subCategory.get(j).subcategoryName
                searchFilteredList.add(dataModel2)
            }
        }

        if(context != null){
            val mDepartmentArrayAdapter = HomeProductArrayAdapter(
                    context!!,
                    R.layout.item_home_search,
                    searchFilteredList
            )

            autoTextView.setAdapter(mDepartmentArrayAdapter)
            autoTextView.setOnItemClickListener(object : AdapterView.OnItemClickListener {
                override fun onItemClick(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        rowID: Long
                ) {
                    val selection = parent?.getItemAtPosition(position) as HomeSearchModel

                    val caregoryName = selection.caregoryName
                    val categoryID = selection.categoryID
                    val filterCategoryName = selection.filterCategoryName

                    if (filterCategoryName.equals("Properties", ignoreCase = true)) {
                        openViewAllProductScreen(caregoryName, categoryID, 1002)
                    } else if (filterCategoryName.equals("Mobiles", ignoreCase = true)) {
                        openViewAllProductScreen(caregoryName, categoryID, 1003)
                    } else if (filterCategoryName.equals("Bikes", ignoreCase = true)) {
                        openViewAllProductScreen(caregoryName, categoryID, 1004)
                    } else if (filterCategoryName.equals(
                                    "Electronics & Appliances",
                                    ignoreCase = true
                            )
                    ) {
                        openViewAllProductScreen(caregoryName, categoryID, 1005)
                    } else if (filterCategoryName.equals("Furniture", ignoreCase = true)) {
                        openViewAllProductScreen(caregoryName, categoryID, 1006)
                    } else if (filterCategoryName.equals("Shops in City", ignoreCase = true)) {
                        openViewAllProductScreen(caregoryName, categoryID, 1007)
                    } else if (filterCategoryName.equals("Fashion & Beauty", ignoreCase = true)) {
                        openViewAllProductScreen(caregoryName, categoryID, 1008)
                    } else if (filterCategoryName.equals("Art & Lifestyle", ignoreCase = true)) {
                        openViewAllProductScreen(caregoryName, categoryID, 1009)
                    } else if (filterCategoryName.equals("Services", ignoreCase = true)) {
                        openViewAllProductScreen(caregoryName, categoryID, 1010)
                    } else if (filterCategoryName.equals("Jobs", ignoreCase = true)) {
                        openViewAllProductScreen(caregoryName, categoryID, 1011)
                    } else if (filterCategoryName.equals("Khedut", ignoreCase = true)) {
                        openViewAllProductScreen(caregoryName, categoryID, 1012)
                    } else if (filterCategoryName.equals("Vehicles", ignoreCase = true)) {
                        openViewAllProductScreen(caregoryName, categoryID, 1001)
                    } else {
                        val subCaregoryName = selection.subCaregoryName
                        val subCategoryID = selection.subCategoryID
                        openSubcategoryViewallScreen(
                                caregoryName,
                                categoryID,
                                subCaregoryName,
                                subCategoryID
                        )

                    }

                }

            })
        }



    }

    private fun openViewAllProductScreen(
        caregoryName: String?,
        categoryID: Int?,
        subCategoryID: Int
    ) {
        CheckCategoryForAdPostDetailRedirection.checkCategory(
            activity as MainActivity,
            caregoryName.toString(),
            categoryID!!.toInt(),
            "",
            subCategoryID
        )
    }

    private fun openSubcategoryViewallScreen(
        caregoryName: String?,
        categoryID: Int?,
        subCaregoryName: String?,
        subCategoryID: Int?
    ) {
        CheckCategoryForAdPostDetailRedirection.checkCategory(
            activity as MainActivity,
            caregoryName.toString(),
            categoryID!!.toInt(),
            subCaregoryName.toString(),
            subCategoryID!!.toInt()
        )
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {

        if(method.equals("notificationCounter")){
            if (da.length() > 0) {
                Log.e("notificationCount", "$da==")
                try {

                    var jsonArray: JSONArray? = null
                    if (da.has("result") && da.get("result") is JSONArray) {
                        jsonArray = da.getJSONArray("result")
                    }
                    if (jsonArray != null) {
                        if(jsonArray.length() != 0){
                            var counterObject: JSONObject? = null
                            counterObject = jsonArray.getJSONObject(0)
                            if(counterObject.has("notificationCounter")){
                                notificationCounter = counterObject.get("notificationCounter").toString()
                                Log.e("notificationCount", " : "+notificationCounter)

                                val notiCount = notificationCounter.toIntOrNull()

                                if(notiCount!=null && notiCount>99){
                                    tv_noti_count.setText("99+")
                                }else{
                                    tv_noti_count.setText(notificationCounter)
                                }



                                if(notificationCounter.toString() == "0"){
                                    tv_noti_count.visibility = View.INVISIBLE
                                }else{
                                    tv_noti_count.visibility = View.VISIBLE
                                }


                            }
                        }
                    }

                    /*val dataModel = HomeSearchBean()
                    dataModel.HomeSearchBean(da)

                    setSearchAdapter(dataModel.homeSearchList)*/

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }

        if(method.equals("homeFilterList")){
            if (da.length() > 0) {
                Log.e("homeFilterList", "$da==")
                try {

                    val dataModel = HomeSearchBean()
                    dataModel.HomeSearchBean(da)

                    setSearchAdapter(dataModel.homeSearchList)

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }

        if (method.equals("Get Category")) {

            if (da.length() > 0) {

                Log.e("get profile Result-", "$da==")
                try {
                    //et_phone.text=da.getString("ConatctNo")

                    val dataModel = CategoryBean()
                    dataModel.CategoryBean(da)

                    rcvListViewAdapter.setCategoryItems(dataModel.categoryList)

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }

        if (method.equals("homeAds")) {

            if (da.length() > 0) {

                Log.e("get homeAds Result-", "$da==")
                try {

                    val dataModel = AdvertisementBean()
                    dataModel.AdvertisementBean(da)

                    Log.e(
                        "HOME_ADS",
                        "" + GsonBuilder().setPrettyPrinting().create().toJson(dataModel)
                    )

                    if(dataModel.adsList.isNotEmpty()) {
                        rcvListViewAdapter.setSponsoredAdsItems(dataModel.adsList)
                    } else {
                        rcvListViewAdapter.setSponsoredAdsItems(ArrayList<AdvertisementBean.Advertisement>())
                    }

                } catch (e: JSONException) {
                    //llHomeAds.visibility = View.GONE
                    e.printStackTrace()
                }
            }
        }

        if (method.equals("freshProduct")) {
            val exFinishTime = System.currentTimeMillis()
            val diff = exFinishTime - exTime
            Log.e("Performance", "API on Success Time : $exFinishTime Diff : $diff")
            Utils.dismissProgress()
            Log.e("progressBar1 : ", "freshproduct 111")

            progressBar.setVisibility(View.GONE)
            progressStatus = 0
            val marginLayoutParams = LinearLayout.LayoutParams(rcvListView.getLayoutParams())
            marginLayoutParams.setMargins(0, 0, 0, 0)
            rcvListView.setLayoutParams(marginLayoutParams)

            if (da.length() > 0) {

                Log.e(TAG, "fresh Product Result-" + "$da==")
                try {

                    freshrecomlist.clear()
                    val dataModel = RecommendationBean()
                    dataModel.RecommendationBean(da)

                    freshrecomlist = dataModel.freshproductlist
                    if(!freshrecomlist.isEmpty() && freshrecomlist.size >0)
                    {
                        Utils.showLog("DUPLICATE_ISSUE", "====scroll start====" + scrollStart)
                        if(scrollStart) {
                            rcvListView.scrollToPosition(0)
                            rcvListViewAdapter.removeHomeItems()
                        }
                        Handler(Looper.getMainLooper()).postDelayed({
                            val distinct = freshrecomlist.distinctBy { it.post_id }
                            //rcvListViewAdapter.setHomeItems(freshrecomlist)
                            rcvListViewAdapter.addItems(
                                distinct as java.util.ArrayList<RecommendationBean.FreshProductListBean>,
                                scrollStart
                            )
                            rcvListView.setItemViewCacheSize(rcvListViewAdapter.itemCount) // originaly added
                            scrollStart = false
                        }, 100)

                        Log.e(
                            TAG,
                            "Item : ${dataModel.freshproductlist.size} RcvItemCount : ${rcvListViewAdapter.itemCount}"
                        )
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onFail(msg: String, method: String) {
        Utils.dismissProgress()

        if (method.equals("freshProduct")) {
            if(rcvListViewAdapter.itemCount > 0)
            {
                Log.e("progressBar1 : ", "444")

                progressBar.visibility = View.GONE
            }
            else{

                Log.e("progressBar1 : ", "555")


                Utils.showToast(activity, msg)
                progressBar.visibility = View.GONE
                //rlnopost.visibility = View.VISIBLE
                rcvListViewAdapter.removeHomeItems()
            }
            //rcvListViewAdapter.removeHomeItems()
        }

        if (method.equals("homeAds")){
            try {
                //llHomeAds.visibility = View.GONE
            }catch (e: Exception){
                e.printStackTrace()
            }
        }

    }

    override fun itemclickCategory(bean: CategoryBean.Category) {
//        val storeusedata = StoreUserData(context!!)
//        storeusedata.setString(Constants.FROM_DETAIL, "true")



        val bundle = Bundle()
        bundle.putString("id", bean.categoryId.toString())
        bundle.putString("name", bean.categoryName)
        bundle.putString("isfrom", "home")
        (activity as MainActivity).changeFragment(
            SubCategoryFragment.newInstance(),
            SubCategoryFragment.TAG,
            bundle,
            true
        )
        pageNo = 0
    }

    override fun itemclickCategoryViewAll() {
        //original commented
//        val storeusedata = StoreUserData(context!!)
//        storeusedata.setString(Constants.FROM_DETAIL, "true")


        val bundle = Bundle()
        bundle.putString("from", "home")
        (activity as MainActivity).changeFragment(
            AllCategoryFragment.newInstance(),
            AllCategoryFragment.TAG,
            bundle,
            true
        )
        pageNo = 0
    }

    override fun itemclickHomePost(bean: RecommendationBean.FreshProductListBean) {
        val storeusedata = StoreUserData(context!!)
        storeusedata.setString(Constants.OPEN_POST_DETAIL, "product_list")
        storeusedata.setString(Constants.CATEGORY_ID, bean.category_id.toString())
        storeusedata.setString(Constants.SUBCATEGORYID, bean.sub_category_id.toString())
        storeusedata.setString(Constants.FROM_DETAIL, "true")
        CheckCategoryForPostDetails.OpenScreen(
            context!!,
            bean.category_id!!.toInt(),
            bean.sub_category_id!!.toInt(),
            bean.post_id!!.toInt()
        )
    }

    override fun itemclickRecommendation(bean: RecommendationBean.FreshProductListBean) {

    }

    override fun itemFavUnfavourite(dataModel: RecommendationBean.FreshProductListBean, i: Int) {
        if(AppConstants.checkUserIsLoggedin(activity!!)) {
            val c = AddFavPostController(this, activity!!, dataModel.post_id.toString())
            c.onClick(iv_like)
        } else{
            val intent = Intent(context, LoginMainActivity::class.java)
            AppConstants.alertLogin(activity!!, getString(R.string.txt_login_ad_fav), intent)
        }
    }

    override fun onClick(view: View) {
        when (view.id) {

            R.id.rl_chat -> {

                if (AppConstants.checkUserIsLoggedin(activity!!)) {
                    val intent1 = Intent(context, ChatActivity::class.java)
                    startActivity(intent1)
                    val storeusedata = StoreUserData(context!!)
                    storeusedata.setString(Constants.FROM_DETAIL, "true")
                } else {
                    val intent = Intent(activity, LoginMainActivity::class.java)
                    AppConstants.alertLogin(
                        activity!!,
                        getString(R.string.txt_login_to_see_chat),
                        intent
                    )
                }

            }

            R.id.rl_noti -> {
                if (AppConstants.checkUserIsLoggedin(activity!!)) {
                    //Utils.showToast(activity, "clicked")
                    val storeusedata = StoreUserData(context!!)
                    storeusedata.setString(Constants.FROM_DETAIL, "true")

                    val intent1 = Intent(context, NotificationsActivity::class.java)
                    startActivity(intent1)

                } else {
                    val intent = Intent(activity, LoginMainActivity::class.java)
                    AppConstants.alertLogin(
                        activity!!,
                        getString(R.string.txt_login_to_see_notification),
                        intent
                    )
                }
            }

            R.id.ll_loc -> {
                DistrictListDialogActivity.display(childFragmentManager, this)
                //(activity as MainActivity).changeFragment(DistrictListDialogActivity.newInstance(this), DistrictListDialogActivity.TAG, null, true)
            }

            R.id.act_location -> {
                ll_location.performClick()
            }

            R.id.iv_refresh -> {

                freshrecomlist.clear()
                //rcvListViewAdapter.removeHomeItems()
                //scrollListener?.reset()

                //takeRefreshPermissionNew()
                takepermissionNew()

                val anim = RotateAnimation(
                    0.0f,
                    360.0f,
                    Animation.RELATIVE_TO_SELF,
                    .5f,
                    Animation.RELATIVE_TO_SELF,
                    .5f
                )
                anim.setInterpolator(LinearInterpolator())
                anim.setRepeatCount(1)
                anim.setDuration(1000)
                ivRefresh.setAnimation(anim)
                ivRefresh.startAnimation(anim)
            }
        }
    }

    var exTime = 0L
    override fun onResume() {
        /*if(svfind!=null){
            svfind!!.setQuery("", false)
            svfind!!.clearFocus()
        }*/

        if(autoTextView!=null){
            autoTextView.setText("")
        }



        Utils.showLog(TAG, "==on resume==" + pageNo)
        Utils.showLog(TAG, "==on resume called ==" + k++)
        val ref = FirebaseDatabase.getInstance().getReference("/List/").child(userId)
        //Utils.showLog(TAG,"==db ref==" + ref)
        chatCount = 0
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (child in dataSnapshot.getChildren()) {
                    //Utils.showLog(TAG,"==db ref 1==" + dataSnapshot.getChildren())
                    val msg: ChatNewList = child.getValue(ChatNewList::class.java)!!
                    Utils.showLog("count", msg.counter)
                    if (!msg.counter.isEmpty() && msg.counter.length > 0 && msg.counter.toInt() > 0) {
                        chatCount++;
                    }
                }
                Utils.showLog("count", "total===" + chatCount)
                try {
                    if (chatCount > 0) {
                        tv_chat_counter.visibility = View.VISIBLE

                        if(chatCount>99){
                            tv_chat_counter.setText("99+")
                        }else{
                            tv_chat_counter.setText(chatCount.toString())
                        }

                    } else {
                        tv_chat_counter.visibility = View.INVISIBLE
                    }
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })


        exTime = System.currentTimeMillis()
        Log.e("Performance", "Product API Execution time : $exTime")

        //callApitoGetHomeFreshPost(locationName.toString(), loc_id, latitude.toString(), longitude.toString(), searchword, 0)
        //getListingController.getHomePostList(locationName.toString(), loc_id, latitude.toString(), longitude.toString(), searchword, 0)

        Utils.showLog("LOCATION_ISSUE", "111")


        if(k == 1 || k==2) {
            Utils.showLog("LOCATION_ISSUE", "222")
            checkLocationData()
        }

        //notification counter
        getListingController.getNotificationCounter()



        super.onResume()
    }

    private fun checkLocationData() {
        Utils.showLog(TAG, "===check location data===")
        k = 1
        freshrecomlist.clear()
        val locData = storeusedata.getBoolean(Constants.GET_LOCA_DATA)

        if(locData) {
            Utils.showLog(TAG, "===check location data if===")
            val gson = Gson()
            val json: String = storeusedata.getString(Constants.HOME_LOC_FILTER)
            Utils.showLog(TAG, "===location saved data===" + json)

            if (!json.isEmpty() || json != null) {
                filterbean = gson.fromJson(json, HomeFilterLocationBean::class.java)
                Utils.showLog(TAG, "===location saved data===" + filterbean)

                if(filterbean.locationName.isEmpty() || filterbean.locationName.length ==0) {
                    actlocation?.setText("Searching Location")
                    locationName = ""
                }
                else {
                    locationName = filterbean.locationName
                    actlocation?.setText(locationName)
                }
                loc_id = filterbean.loc_id
                latitude = filterbean.latitude.toDouble()
                longitude = filterbean.longitude.toDouble()
                //searchword = filterbean.searchword

                if(loc_id.length == 0)
                {
                    curlatitude = filterbean.latitude.toDouble()
                    curlongitude = filterbean.longitude.toDouble()
                    //checkLocation()
                    //ivRefresh.visibility = View.VISIBLE
                    //callApitoGetHomeFreshPost(locationName.toString(), loc_id, latitude.toString(), longitude.toString(), searchword, 0)
                }
                else {
                    addlatitude =filterbean.latitude.toDouble()
                    addlongitude = filterbean.longitude.toDouble()
                    ivRefresh.visibility = View.GONE
                    //callApitoGetHomeFreshPost(locationName.toString(), loc_id, latitude.toString(), longitude.toString(), searchword, pageNo)
                }
                val storeusedata = StoreUserData(context!!)
                val isfrom = storeusedata.getString(Constants.FROM_DETAIL)
                Utils.showLog(TAG, "====screen is from detail ===" + isfrom + pageNo)
                if(isfrom.equals("true"))
                {
                    storeusedata.setString(Constants.FROM_DETAIL, "false");
                } else {

                    //original added here
                    /*scrollListener?.reset()
                    rcvListViewAdapter.removeHomeItems()
                    pageNo = 0
                    radius = 70
                    Utils.showLog("DUPLICATE_ISSUE", "555")
                    callApitoGetHomeFreshPost(
                            locationName.toString(),
                            loc_id,
                            latitude.toString(),
                            longitude.toString(),
                            searchword,
                            pageNo,
                            radius,
                            true
                    )*/


                }
            }
        }
        else{
            Utils.showLog(TAG, "===check location data else===")
            //takepermissionNew()
        }
    }


    override fun onPause() {
        super.onPause()
        if (checkSelfPermission(context!!, ACCESS_FINE_LOCATION) == PERMISSION_GRANTED) {
            fusedLocationClient?.removeLocationUpdates(locationCallback)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {

        when (requestCode) {
            2012 -> {
                Log.e(TAG, "=====ONPERMISSION RESULY===")
                getPermissionList()
                for (grantResult in grantResults) {
                    if (grantResult != PERMISSION_GRANTED) {
                        return
                    }
                }
                checkLocation()
            }

            200 -> {
                val result = context!!.checkCallingOrSelfPermission(Manifest.permission.CALL_PHONE)
                if (result == PackageManager.PERMISSION_GRANTED) {

                    val c = PhoneCallController(requireActivity(), contactAds)
                    c.makePhoneCall()
                } else {
                    Utils.showToast(
                        requireActivity(),
                        resources.getString(R.string.txt_phone_permission)
                    )
                }
            }

            1200 -> {
                if (grantResults.size <= 0) {
                    // If user interaction was interrupted, the permission request is cancelled and you
                    // receive empty arrays.
                    Log.i("WoosmapGeofencing", "User interaction was cancelled.")
                } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i(
                        "WoosmapGeofencing",
                        "Permission granted, updates requested, starting location updates"
                    )
                } else {
                    // Permission denied.
                    Log.i("WoosmapGeofencing", "Permission denied....")

                    // Notify the user via a SnackBar that they have rejected a core permission for the
                    // app, which makes the Activity useless. In a real app, core permissions would
                    // typically be best requested during a welcome-screen flow.

                    // Additionally, it is important to remember that a permission might have been
                    // rejected without asking the user for permission (device policy or "Never ask
                    // again" prompts). Therefore, a user interface affordance is typically implemented
                    // when permissions are denied. Otherwise, your app could appear unresponsive to
                    // touches or interactions which have required permissions.
                    showSettingsDialog()
                    /* showSnackbar(android.R.string.permission_denied_explanation, android.R.string.settings,
                                View.OnClickListener { // Build intent that displays the App settings screen.
                                    val intent = Intent()
                                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                    val uri = Uri.fromParts("package",
                                            BuildConfig.APPLICATION_ID, null)
                                    intent.data = uri
                                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                    startActivity(intent)
                                })*/
                }
            }
        }
    }

    override fun selectedLocation(
        district: String,
        locId: String,
        name: String,
        lat: String,
        long: String
    ) {
        Utils.showLog(TAG, "---district---" + district + "--- id ---" + locId)
        Utils.showLog(TAG, "---name---" + name + "--- lat ---" + lat + "--- long ---" + long)


        scrollListener?.reset()
        storeusedata.setBoolean(Constants.GET_LOCA_DATA, false)
        loc_id = locId
        //latitude = lat.toDouble()
        //longitude = long.toDouble()
        addlatitude = lat.toDouble()
        addlongitude = long.toDouble()
        if(!name.isEmpty() && name.toString().length > 0)
        {
            actlocation!!.setText(name)
            locationName = name
        }
        else {
            actlocation!!.setText(district)
            locationName = district
        }
        if(loc_id.length >0) {
            scrollStart = true
        }
        ivRefresh.visibility = View.GONE

        Utils.showLog("DUPLICATE_ISSUE", "444")
        callApitoGetHomeFreshPost(
            locationName.toString(),
            loc_id,
            addlatitude.toString(),
            addlongitude.toString(),
            searchword,
            0,
            70,
            true
        )
        //getListingController.getHomePostList(locationName.toString(), loc_id, latitude.toString(), longitude.toString(), searchword, 0)
    }

    override fun currentLocation() {
        scrollListener?.reset()
        loc_id = ""
        storeusedata.setBoolean(Constants.GET_LOCA_DATA, false)
        takepermissionNew() //original commented
        ivRefresh.visibility = View.VISIBLE
    }

    fun takepermission() {

        Log.e(TAG, "Requesting Permissions :)")
        val deniedPermissionList = getPermissionList()

        if(deniedPermissionList.isNotEmpty()) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                deniedPermissionList.toList().toTypedArray(),
                2012
            )
        }
        else{
            checkLocation()
        }
    }

    private fun getPermissionList(): ArrayList<String> {
        val permissionList = arrayListOf<String>()
        permissionList.add(INTERNET)
        permissionList.add(ACCESS_COARSE_LOCATION)
        permissionList.add(ACCESS_FINE_LOCATION)

        //original added
        /*if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
        {
            permissionList.add(ACCESS_BACKGROUND_LOCATION)
        }*/
        val deniedPermissionList = arrayListOf<String>()
        permissionList.forEach {
            if(ContextCompat.checkSelfPermission(requireContext(), it) != PERMISSION_GRANTED)
            {
                Log.d(TAG, "Permission Not Grandted : $it")
                deniedPermissionList.add(it)
            }
        }

        return deniedPermissionList
    }

    fun isGPSEnabled(mContext: Context): Boolean {
        val locationManager = mContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    private var locationManager: LocationManager? = null
    private var myLocationListener: LocationListener? = null



    @SuppressLint("MissingPermission")
    fun checkLocation() {
        Log.e(TAG, "checkLocation called!")
        if(!isGPSEnabled(requireContext()))
        {
            checkGPSEnabled()
            Log.e(TAG, "GPS provider is not enabled ")
        }
        else {

            ivRefresh.visibility = View.VISIBLE
            try {
                val serviceString = Context.LOCATION_SERVICE
                locationManager = context?.getSystemService(serviceString) as LocationManager?

                handler = Handler()
                val location = locationManager!!.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                Log.e(TAG, "last location" + location)
                if (location != null) {
                    //latitude = location.getLatitude()
                    //longitude = location.getLongitude()
                    curlatitude = location.getLatitude()
                    curlongitude = location.getLongitude()
                }
                Log.e(TAG, "Lat1 : $curlatitude  Long 1: $curlongitude")
                //scrollListener?.reset()
                //rcvListViewAdapter.removeHomeItems()
                val locationAddress = LocationAddress()
                locationAddress.getAddressFromLocation(
                    curlatitude,
                    curlongitude,
                    context!!,
                    GeoCodeHandler()
                )

                myLocationListener = object : LocationListener {
                    override fun onLocationChanged(locationListener: Location) {
                        Log.e(TAG, "onLocationChanged invoked")

                        if (locationListener != null) {
                                //latitude = locationListener.latitude
                                //longitude = locationListener.longitude

                                curlatitude = locationListener.latitude
                                curlongitude = locationListener.longitude

                                Log.e(TAG, "Lat : $curlatitude  Long : $curlongitude")

                                val locData = storeusedata.getBoolean(Constants.GET_LOCA_DATA)
                                if (locData) {
                                    val gson = Gson()
                                    val json: String = storeusedata.getString(Constants.HOME_LOC_FILTER)
                                    Utils.showLog(TAG, "===location saved data===" + json)

                                    if (!json.isEmpty() || json != null) {
                                        filterbean = gson.fromJson(
                                            json,
                                            HomeFilterLocationBean::class.java
                                        )
                                        Utils.showLog(TAG, "===location saved data===" + filterbean)
                                        loc_id = filterbean.loc_id

                                        if (loc_id.length == 0 && location != null) {
                                           /* if (location.getLatitude() != latitude && location.getLongitude() != longitude)*/
                                            if (location.getLatitude() != curlatitude && location.getLongitude() != curlongitude){
                                                Log.e(TAG, "if")
                                                //curlatitude = location.getLatitude()
                                                //curlongitude = location.getLongitude()

                                                if(rcvListViewAdapter.itemCount <= 0) {
                                                    freshrecomlist.clear()
                                                    scrollListener?.reset()
                                                    rcvListViewAdapter.removeHomeItems()
                                                    val locationAddress1 = LocationAddress()
                                                    locationAddress1.getAddressFromLocation(
                                                        curlatitude,
                                                        curlongitude,
                                                        context!!,
                                                        GeoCodeHandler()
                                                    )
                                                }
                                            }
                                            else{
                                                curlatitude = locationListener.latitude
                                                curlongitude = locationListener.longitude
                                                Log.e(TAG, "if else")

                                                if(rcvListViewAdapter.itemCount <= 0) {
                                                    freshrecomlist.clear()
                                                    scrollListener?.reset()
                                                    rcvListViewAdapter.removeHomeItems()
                                                    val locationAddress1 = LocationAddress()
                                                    locationAddress1.getAddressFromLocation(
                                                        locationListener.latitude,
                                                        locationListener.longitude,
                                                        context!!,
                                                        GeoCodeHandler()
                                                    )
                                                }
                                            }
                                        }
                                    }

                                } else {
                                    Log.e(TAG, "else")
                                    try {
                                        if (location!!.getLatitude() != curlatitude && location.getLongitude() != curlongitude) {
                                            Log.e(TAG, "if")
                                            //curlatitude = location.getLatitude()
                                            //curlongitude = location.getLongitude()

                                            if (rcvListViewAdapter.itemCount <= 0)
                                            {
                                                freshrecomlist.clear()
                                                scrollListener?.reset()
                                                rcvListViewAdapter.removeHomeItems()
                                            val locationAddress1 = LocationAddress()
                                            locationAddress1.getAddressFromLocation(
                                                curlatitude,
                                                curlongitude,
                                                context!!,
                                                GeoCodeHandler()
                                            )
                                            }
                                        }
                                        else{
                                            curlatitude = locationListener.latitude
                                            curlongitude = locationListener.longitude
                                            Log.e(TAG, "else if")

                                            if(rcvListViewAdapter.itemCount <= 0) {
                                                freshrecomlist.clear()
                                                scrollListener?.reset()
                                                rcvListViewAdapter.removeHomeItems()
                                                val locationAddress1 = LocationAddress()
                                                locationAddress1.getAddressFromLocation(
                                                    locationListener.latitude,
                                                    locationListener.longitude,
                                                    context!!,
                                                    GeoCodeHandler()
                                                )
                                            }
                                       }
                                    } catch (e: NullPointerException) {
                                        e.printStackTrace()
                                    }
                                }

                                /*  if (location == null || location.toString().length == 0) {
                                val locationAddress = LocationAddress()
                                locationAddress.getAddressFromLocation(latitude, longitude, context!!, GeoCodeHandler())
                            }*/
                            }

                    }

                    override fun onProviderDisabled(provider: String) {}
                    override fun onProviderEnabled(provider: String) {}
                    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
                }
                //locationManager!!.requestLocationUpdates(LocationManager.GPS_PROVIDER, 60000, 0f, myLocationListener!!)
                locationManager!!.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    5000, //1000 * 60 * 5 original time
                    0f,
                    myLocationListener!!
                )

            } catch (e: NullPointerException) {
                e.printStackTrace()
            }
        }
    }

    private fun checkGPSEnabled() {
        val manager = requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER).not()) {
            turnOnGPS()
        }
    }

    private fun turnOnGPS() {
        val request = LocationRequest.create().apply {
            interval = 1000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
        val builder = LocationSettingsRequest.Builder().addLocationRequest(request)
        val client: SettingsClient = LocationServices.getSettingsClient(requireActivity())
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
        task.addOnFailureListener {
            if (it is ResolvableApiException) {
                try {
                    it.startResolutionForResult(requireActivity(), 12345)
                } catch (sendEx: IntentSender.SendIntentException) {
                }
            }
        }.addOnSuccessListener {
            //here GPS is On
            checkLocation()
        }
    }

    @SuppressLint("MissingPermission")
    private fun getUserLocation() {
        try {
            fusedLocationClient?.lastLocation?.addOnSuccessListener { location: Location ->
                Utils.showLog(TAG, "location!." + location)
                if(location.equals("null"))
                {
                    Utils.showToast(activity, "Failed to get user current location!.")
                }
                else {
                    latitude = location.latitude
                    longitude = location.longitude
                    Log.i(TAG, "--- LATITUDE ---$latitude---LONGITUDE---$longitude")
                    val locationAddress = LocationAddress()
                    locationAddress.getAddressFromLocation(
                        latitude,
                        longitude,
                        context!!,
                        GeoCodeHandler()
                    )
                }
            }
        }
        catch (e: NullPointerException)
        {
            e.printStackTrace()
        }
        catch (e: IllegalArgumentException)
        {
            e.printStackTrace()
        }
    }

    internal inner class GeoCodeHandler : Handler() {
        override fun handleMessage(message: Message) {
            val locationAddress: String
            locationAddress = when (message.what) {
                1 -> {
                    val bundle = message.data
                    bundle.getString("address").toString()
                }
                /*else -> null.toString()*/
                else -> "Gujarat"
                /*else -> "Searching Location"*/
            }
            Utils.showLog(TAG, "---- address---" + locationAddress)
            if(locationAddress.equals("null") || locationAddress.isEmpty())
            {
                actlocation?.setText("Searching Location")
                progressBar.visibility = View.VISIBLE
            }
            else {
                actlocation?.setText(locationAddress)
            }

            locationName = locationAddress
            freshrecomlist.clear()
            scrollStart = true
            Log.i(
                TAG,
                "======= CURRENT LOCATION =========" + locationAddress + "Lattitude : " + curlatitude + "Longitude : " + curlongitude
            )

            scrollListener?.reset()

            Utils.showLog("DUPLICATE_ISSUE", "333")

            callApitoGetHomeFreshPost(
                locationAddress.replace(" ", ""),
                "",
                curlatitude.toString(),
                curlongitude.toString(),
                searchword,
                0,
                70,
                true
            )



            //getListingController.getHomePostList(locationAddress.replace(" ", ""), "", latitude.toString(), longitude.toString(), searchword, 0)
        }
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    /*private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle("Need Location Permissions")
        builder.setMessage("This app needs location permission to use this application. You can grant them in app settings.")
        builder.setPositiveButton(
                "GOTO SETTINGS"
        ) { dialog, which ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton(
                "Cancel"
        ) { dialog, which -> dialog.cancel() }
        builder.show()
    }*/

    private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle("Need Permissions")
        builder.setMessage("This app needs permission to use this application. You can grant them in app settings.")

        builder.setPositiveButton(
            "GOTO SETTINGS"
        ) { dialog, which ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton(
            "Cancel"
        ) { dialog, which -> dialog.dismiss() }
        builder.show()
    }

    // navigating user to app settings
    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", context?.getPackageName(), null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            101 -> {
                checkLocation()
            }
        }
    }

    override fun itemClickAdvertisement(advertisement: AdvertisementBean.Advertisement) {
        showBottomsheetMoreOptions(requireContext(), advertisement)
    }

    private fun showBottomsheetMoreOptions(
        context: Context,
        advertisement: AdvertisementBean.Advertisement
    ) {

        val bottomSheetDialog = BottomSheetDialog(context)
        bottomSheetDialog.setContentView(R.layout.bottom_sheet_advertisement)


        val llWebsite: LinearLayout? = bottomSheetDialog.findViewById(R.id.llWebsite)
        val llCall: LinearLayout? = bottomSheetDialog.findViewById(R.id.llCall)
        val llWhatsapp: LinearLayout? = bottomSheetDialog.findViewById(R.id.llWhatsapp)

        if(advertisement.appbanner_link!=null && advertisement.appbanner_link != ""){
            websiteAds = advertisement.appbanner_link.toString()
            llWebsite!!.visibility = View.VISIBLE
        }else{
            llWebsite!!.visibility = View.GONE
        }

        if(advertisement.whatsapp_no!=null && advertisement.whatsapp_no != ""){
            whatsappAds = advertisement.whatsapp_no.toString()
            llWhatsapp!!.visibility = View.VISIBLE
        }else{
            llWhatsapp!!.visibility = View.GONE
        }

        if(advertisement.mobile_no!=null && advertisement.mobile_no != ""){
            contactAds = advertisement.mobile_no.toString()
            llCall!!.visibility = View.VISIBLE
        }else{
            llCall!!.visibility = View.GONE
        }


        llWebsite!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {

                val httpIntent = Intent(Intent.ACTION_VIEW)
                httpIntent.data = Uri.parse(advertisement.appbanner_link.toString())
                startActivity(httpIntent)

                bottomSheetDialog.dismiss()

            }

        })

        llCall!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {

                if (AppConstants.checkUserIsLoggedin(requireActivity())) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(arrayOf(Manifest.permission.CALL_PHONE), 200)
                    }
                } else {
                    val intent = Intent(requireContext(), LoginMainActivity::class.java)
                    AppConstants.alertLogin(
                        requireActivity(),
                        getString(R.string.txt_login_to_call),
                        intent
                    )
                }

                bottomSheetDialog.dismiss()
            }

        })

        llWhatsapp!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {

                var appPackage = ""
                if (isAppInstalled(requireContext(), "com.whatsapp")) {
                    appPackage = "com.whatsapp"
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data =
                        Uri.parse("http://api.whatsapp.com/send?phone=+91" + whatsappAds + "&text=")
                    startActivity(intent)
                } else if (isAppInstalled(requireContext(), "com.whatsapp.w4b")) {
                    appPackage = "com.whatsapp.w4b"
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data =
                        Uri.parse("http://api.whatsapp.com/send?phone=+91" + whatsappAds + "&text=")
                    startActivity(intent)
                } else {
                    Utils.showToast(requireActivity(), "Whatsapp not installed on your device")
                }

                bottomSheetDialog.dismiss()
            }

        })

        bottomSheetDialog.show()
    }

    private fun isAppInstalled(ctx: Context, packageName: String): Boolean {
        val pm: PackageManager = ctx.getPackageManager()
        val app_installed: Boolean
        app_installed = try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES)
            true
        } catch (e: PackageManager.NameNotFoundException) {
            false
        }
        return app_installed
    }

}