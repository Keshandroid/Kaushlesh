package com.kaushlesh.view.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.Controller.GetListingController

import com.kaushlesh.R
import com.kaushlesh.adapter.SubCategoryAdapter
import com.kaushlesh.bean.SubCategoryBean
import com.kaushlesh.bean.subCategoryBean1
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.AdPost.CheckCategoryForAdPostRedirection
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.FiltersSaveRemoveData
import com.kaushlesh.utils.PostDetail.CheckCategoryForAdPostDetailRedirection
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.MainActivity
import kotlinx.android.synthetic.main.fragment_sub_category.*
import org.json.JSONException
import org.json.JSONObject
import java.util.ArrayList

class SubCategoryFragment : Fragment(),SubCategoryAdapter.ItemClickListener,ParseControllerListener {

    internal lateinit var rvsubcategory: RecyclerView
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal var list: MutableList<SubCategoryBean> = ArrayList()
    internal var category: String? = null
    internal var isfrom: String? = null
    var categoryId: Int? = null
    lateinit var getListingController: GetListingController
    internal var list1: ArrayList<subCategoryBean1.SubCategory> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sub_category, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar = view.findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        rvsubcategory = view.findViewById(R.id.rv_sub_category)

        list.clear()
        list1.clear()

        if (arguments != null) {
            categoryId = arguments!!.getString("id")!!.toInt()
            category = arguments!!.getString("name")
            isfrom = arguments!!.getString("isfrom")

            AppConstants.printLog(TAG, "==category id==$categoryId")
            AppConstants.printLog(TAG, "==title==$category==isfrom==$isfrom")

            if (category!!.equals("Vehicles", ignoreCase = true)) {
                tvtoolbartitle.text =
                    category + "\n" + resources.getString(R.string.txt_vehicle_toolbar_title)
            } else {
                tvtoolbartitle.text = category
             }
        }


        getListingController = GetListingController(activity!!, this)

        getListingController.getSubCategoryList(categoryId.toString())

        //checkAnsSetSubCategory(category!!)


        btnback.setOnClickListener {
            if (activity != null) {
                activity!!.onBackPressed()
            }
        }

        rl_view_all.setOnClickListener {
            val subcateId = Utils.getViewAllId(categoryId)
            CheckCategoryForAdPostDetailRedirection.checkCategory(activity as MainActivity,category.toString(), categoryId!!.toInt(),resources.getString(R.string.txt_view_all), subcateId)

        }
    }

    override fun itemclickSubCategory(bean: subCategoryBean1.SubCategory) {
        val storeusedata = StoreUserData(context!!)
        storeusedata.setString(Constants.CATEGORY_NAME, category.toString())
        storeusedata.setString(Constants.CATEGORY_ID, categoryId.toString())
        storeusedata.setString(Constants.SUBCATEGORYID, bean.subcategoryId.toString())
        storeusedata.setString(Constants.SUBCATEGORY_NMAE, bean.subcategoryName.toString())

        if (isfrom!!.equals("adpost", ignoreCase = true)) {
            CheckCategoryForAdPostRedirection.openDetailScreen(context!!,category.toString(),categoryId,bean.subcategoryName,bean.subcategoryId)
        } else{
            FiltersSaveRemoveData.cleardata(categoryId.toString(),bean.subcategoryId.toString())
            CheckCategoryForAdPostDetailRedirection.checkCategory(activity as MainActivity,category.toString(), categoryId!!.toInt(),bean.subcategoryName.toString(), bean.subcategoryId!!.toInt())
        }
    }

    companion object {

        val TAG = "SubCategoryFragment"

        fun newInstance(): SubCategoryFragment {
            return SubCategoryFragment()
        }
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (method.equals("Get subCategory")) {
            if (da.length() > 0) {

                Log.e("get profile Result-", "$da==")
                try {

                    list1.clear()
                    val dataModel = subCategoryBean1()
                    dataModel.subCategoryBean1(da)

                    list1 = dataModel.subcategoryList
                    Utils.showLog(TAG,"==product list size==" + list1.size)

                    bindRecyclerviewSubCategory(list1)


                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun bindRecyclerviewSubCategory(list1: ArrayList<subCategoryBean1.SubCategory>) {
        checkForViewAll(isfrom)
        try {
            val recyclerLayoutManager = LinearLayoutManager(context)
            rvsubcategory.layoutManager = recyclerLayoutManager
            rvsubcategory.itemAnimator = DefaultItemAnimator()
            val adapter = SubCategoryAdapter(list1, context!!)
            adapter.setClicklistner(this)
            rvsubcategory.adapter = adapter
            adapter.notifyDataSetChanged()
        }
        catch (e : NullPointerException)
        {
            e.printStackTrace()
        }
    }

    private fun checkForViewAll(isfrom: String?) {
        try {
            if (!isfrom!!.equals("adpost", ignoreCase = true)) {
                rl_view_all.visibility = View.VISIBLE
            }
        }
        catch (e : IllegalStateException)
        {
            e.printStackTrace()
        }
    }

    override fun onFail(msg: String, method: String) {
        rvsubcategory.visibility = View.GONE
        Utils.showToast(activity,msg)
    }
}

