package com.kaushlesh.view.fragment.MyAccount.PurchasePackage

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.GridLayoutManager
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.R
import com.kaushlesh.adapter.PremiumAdExampleAdapter
import com.kaushlesh.bean.GetMyPostBean
import com.kaushlesh.bean.ProductDataBean
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Utils
import kotlinx.android.synthetic.main.activity_premium_ad_example.*
import org.json.JSONObject

class PremiumAdExampleActivity : AppCompatActivity() ,PremiumAdExampleAdapter.ItemClickListener,
    ParseControllerListener {

    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView

    private lateinit var getListingController: GetListingController
    val list = ArrayList<GetMyPostBean.getMyPostList>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_premium_ad_example)

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = getString(R.string.txt_premium_ad_example)

        btnback.setOnClickListener {
            this.onBackPressed()
        }

        //callApitoGetListWithPremium()
        bindRecyclerview()
    }

    private fun callApitoGetListWithPremium() {

        getListingController = GetListingController(this, this)
        //getListingController.getMyPost()
    }

    private fun bindRecyclerview() {

        val layoutManager = GridLayoutManager(applicationContext, 2)
        rv_premium_ads.layoutManager = layoutManager
        //val adapter = PremiumAdExampleAdapter(applicationContext!!,list)
        val adapter = PremiumAdExampleAdapter(applicationContext!!)
        adapter.setClicklistner(this)
        rv_premium_ads.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun itemclickProduct(bean: GetMyPostBean.getMyPostList) {
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (method.equals("getMyPost")) {
            list.clear()
            val dataModel = GetMyPostBean()
            dataModel.GetMyPostBean(da)
            list.addAll(dataModel.getMyPostBeanlist)
            Utils.dismissProgress()
            if(list.size > 0) {
                bindRecyclerview()
            }
        }

    }

    override fun onFail(msg: String, method: String) {
        Utils.dismissProgress()
        Utils.showToast(this,msg)
    }

}
