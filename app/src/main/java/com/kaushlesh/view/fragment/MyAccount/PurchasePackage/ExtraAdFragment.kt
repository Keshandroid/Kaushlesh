package com.kaushlesh.view.fragment.MyAccount.PurchasePackage

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.kaushlesh.R
import com.kaushlesh.adapter.AdPostPackageAdapter
import com.kaushlesh.bean.GetCategoryPackagesBean
import com.kaushlesh.utils.Utils
import kotlinx.android.synthetic.main.fragment_extraad.*

import android.content.Context
import com.google.gson.GsonBuilder


class ExtraAdFragment : Fragment(), AdPostPackageAdapter.ItemClickListener  {

    var mCallback: OnHeadlineSelectedListener? = null

    internal var extraadlist: ArrayList<GetCategoryPackagesBean.Packages> = ArrayList()
    var templist  : ArrayList<GetCategoryPackagesBean.Packages> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_extraad, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //bindRecyclerviewFuel()
        instance = this
        extraadlist = (activity as AdPostPackagesActivity).getDataExtraAd()
        Utils.showLog(TAG,"==extra addd===" + extraadlist.size)


        setupdata(extraadlist)

    }

    private fun setupdata(extraadlist: java.util.ArrayList<GetCategoryPackagesBean.Packages>) {
        if(extraadlist.size >0)
        {
            extraadlist.sortByDescending{ it.noofAd!!.toInt()}

            Utils.showLog(TAG, "==list duration==" + GsonBuilder().setPrettyPrinting().create().toJson(extraadlist))

            templist = extraadlist

            templist.sortByDescending{ it.duration!!.toInt()}

            Utils.showLog(TAG, "==list ad==" + GsonBuilder().setPrettyPrinting().create().toJson(templist))

            templist.reverse()

            //extraadlist.sortedWith(compareBy({ it.duration!!.toInt() }, { it.noofAd?.toInt() }))

            Utils.showLog(TAG, "==list ad==" + GsonBuilder().setPrettyPrinting().create().toJson(templist))

            bindRecyclerviewFuel(templist)
        }
    }

    interface OnHeadlineSelectedListener {
        fun onArticleSelected(
            position: String,
            bean: GetCategoryPackagesBean.Packages
        )
    }

    private fun bindRecyclerviewFuel(extraadlist: ArrayList<GetCategoryPackagesBean.Packages>) {
        if(rv_extra!=null){
            val recyclerLayoutManager = GridLayoutManager(context, 2)
            rv_extra.layoutManager = recyclerLayoutManager
            rv_extra.setHasFixedSize(true);
            val adapter = AdPostPackageAdapter(context!!,"extraAd", extraadlist)
            rv_extra.adapter = adapter
            adapter.setClicklistner(this)
            adapter.notifyDataSetChanged()
        }

    }

    companion object {
        val TAG = "ExtraAdFragment"
        fun newInstance(): ExtraAdFragment {
            return ExtraAdFragment()
        }

        private var instance: ExtraAdFragment? = null

        @Synchronized
        fun getInstance(): ExtraAdFragment? {
            return instance
        }

    }

    override fun itemclickProduct(bean: GetCategoryPackagesBean.Packages, type: String) {

        //Utils.showToast(activity,"clicked")
        Utils.showLog(TAG,"===type===" + type + "id===" + bean.packageid)

        mCallback?.onArticleSelected(type,bean);

    }

    fun refershData(extraadlist: java.util.ArrayList<GetCategoryPackagesBean.Packages>) {
        setupdata(extraadlist)

    }

     override fun onAttach(context: Context) {
         super.onAttach(context)
         try {
                 mCallback = context as OnHeadlineSelectedListener
         } catch (e: ClassCastException) {
             throw ClassCastException(context.toString() + " must implement FragmentToActivity")
         }

     }

}