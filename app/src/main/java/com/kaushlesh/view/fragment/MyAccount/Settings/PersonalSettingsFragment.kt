package com.kaushlesh.view.fragment.MyAccount.Settings

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.preference.PreferenceManager
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.kaushlesh.Controller.LogoutController
import com.kaushlesh.Controller.NotificationViewController
import com.kaushlesh.R
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.Login.LoginMainActivity
import com.kaushlesh.view.fragment.MyAccount.EditProfileActivity
import com.kaushlesh.view.fragment.MyAccount.PurchasePackage.PurchasePackageFragment
import kotlinx.android.synthetic.main.fragment_my_account.*
import org.json.JSONObject


class PersonalSettingsFragment : Fragment(),View.OnClickListener, ParseControllerListener {

    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal lateinit var dialog: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_personal_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar = view.findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.txt_settings)

        btnback.setOnClickListener {
            if (activity != null) {
                activity!!.onBackPressed()
            }
        }

        addOnclicked()

    }

    private fun addOnclicked() {

        ll_logout.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ll_logout ->
            {
                openLogoutDialog()
            }
        }
    }

    private fun openLogoutDialog() {

        dialog = Dialog(context!!)
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_logout)
        // Set dialog title
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        //dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_white);
        // set values for custom dialog components - text, image and button

        val tvtitle = dialog.findViewById(R.id.tv_title) as TextView
        tvtitle.setText(resources.getText(R.string.txt_logout_warning))
        val btnyes = dialog.findViewById(R.id.btn_yes) as TextView
        val btnno = dialog.findViewById(R.id.btn_no) as TextView

        btnyes.setOnClickListener(View.OnClickListener {

            //clearDataFromLocal()

            logoutApiCall()


            //original here
            /*val storeusedata = StoreUserData(context!!)
            storeusedata.setString(Constants.USER_ID, "")
            storeusedata.setString(Constants.TOKEN, "")
            storeusedata.setString(Constants.IS_LOGGED_IN, "")
            storeusedata.setString(Constants.IS_OTP_VERIFED, "")
            storeusedata.setString(Constants.IS_ACTIVE,"")
            storeusedata.setString(Constants.DEVICE_TOKEN,"")
            storeusedata.setString(Constants.DEVICE_TYPE, "")
            storeusedata.setString(Constants.REGISTREATION_TYPE, "")

            val intent = Intent(context, LoginMainActivity::class.java)
            startActivity(intent)
            activity!!.finishAffinity()*/

            dialog.dismiss()
        })

        btnno.setOnClickListener(View.OnClickListener { dialog.dismiss() })

        dialog.show()
    }

    private fun logoutApiCall() {
        LogoutController(activity!!,this)
    }

    private fun clearDataFromLocal() {
        val mySPrefs = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = mySPrefs.edit()
        editor.remove("last_time_started")
        editor.apply()
    }

    companion object {

        val TAG = "PersonalSettingsFragment"

        fun newInstance(): PersonalSettingsFragment {
            return PersonalSettingsFragment()
        }
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (method.equals("logoutApp")) {
            val storeusedata = StoreUserData(context!!)
            storeusedata.setString(Constants.USER_ID, "")
            storeusedata.setString(Constants.TOKEN, "")
            storeusedata.setString(Constants.IS_LOGGED_IN, "")
            storeusedata.setString(Constants.IS_OTP_VERIFED, "")
            storeusedata.setString(Constants.IS_ACTIVE,"")
            storeusedata.setString(Constants.DEVICE_TOKEN,"")
            storeusedata.setString(Constants.DEVICE_TYPE, "")
            storeusedata.setString(Constants.REGISTREATION_TYPE, "")

            storeusedata.setString(Constants.NAME, "")
            storeusedata.setString(Constants.CONTACT_NO, "")
            storeusedata.setString(Constants.EMAIL, "" )

            val intent = Intent(context, LoginMainActivity::class.java)
            startActivity(intent)
            activity!!.finishAffinity()
        }
    }

    override fun onFail(msg: String, method: String) {
        if (method.equals("logoutApp")) {
            //log
        }
    }
}