package com.kaushlesh.view.fragment.MyAccount.Communication

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.R
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.view.fragment.ProductListFragment
import org.json.JSONException
import org.json.JSONObject


class PrivacyPolicyFragment : Fragment(), View.OnClickListener, ParseControllerListener {
    var cname: String = ""
    var category: String = ""
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView

    lateinit var getListingController: GetListingController
    lateinit var webViewFaqBuyer: WebView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_support_base, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getListingController = GetListingController(activity!!, this)


        toolbar = view.findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        webViewFaqBuyer = view.findViewById(R.id.webViewFaqBuyer)

        if (arguments != null) {
            tvtoolbartitle.text = arguments!!.getString("name").toString()
            AppConstants.printLog(
                ProductListFragment.TAG,
                "==title==$cname" + "==category==$category"
            )
        }
        btnback.setOnClickListener {
            if (activity != null) {
                activity!!.onBackPressed()
            }
        }


        getListingController.getPrivacyPolicy(4)

        addOnclicked()
    }

    private fun addOnclicked() {


    }

    override fun onClick(v: View?) {
        when (v?.id) {

        }

    }

    companion object {
        val TAG = "PrivacyFragment"
        fun newInstance(): PrivacyPolicyFragment {
            return PrivacyPolicyFragment()
        }
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (method.equals("privacyPolicy")) {
            if (da.length() > 0) {
                Log.e("get privacyPolicy", "$da==")
                try {

                    if(da.has("result")) {
                        val data = da.getJSONObject("result")
                        val faqBuyersData = data.getString("staticDescription")

                        setWebView(faqBuyersData)

                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun setWebView(faqBuyersData: String) {
        webViewFaqBuyer.getSettings().setJavaScriptEnabled(true);
        webViewFaqBuyer.loadData(faqBuyersData, "text/html; charset=utf-8", "UTF-8");

    }

    override fun onFail(msg: String, method: String) {

    }

}