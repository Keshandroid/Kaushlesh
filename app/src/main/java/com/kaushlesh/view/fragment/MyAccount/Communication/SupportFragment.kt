package com.kaushlesh.view.fragment.MyAccount.Communication

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.kaushlesh.R
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.view.activity.MainActivity

import com.kaushlesh.view.fragment.MyAccount.PurchasePackage.PurchasePackageFragment
import com.kaushlesh.view.fragment.ProductListFragment
import kotlinx.android.synthetic.main.fragment_purchase_package.*
import kotlinx.android.synthetic.main.fragment_support.*


class SupportFragment : Fragment(), View.OnClickListener {
    var cname: String = ""
    var category: String = ""
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_support, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar = view.findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)


        if (arguments != null) {
            tvtoolbartitle.text = arguments!!.getString("name").toString()
            AppConstants.printLog(
                ProductListFragment.TAG,
                "==title==$cname" + "==category==$category"
            )
        }
        btnback.setOnClickListener {
            if (activity != null) {
                activity!!.onBackPressed()
            }
        }

        addOnclicked()
    }

    private fun addOnclicked() {
        ll_contact_us.setOnClickListener(this)
        ll_buyers.setOnClickListener(this)
        ll_sellers.setOnClickListener(this)
        ll_private_policy.setOnClickListener(this)
        ll_terms_use.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.ll_contact_us -> {
                val bundle = Bundle()
                bundle.putString("name", getString(R.string.txt_contact_us))
                //  bundle.putString("catName",getString(R.string.txt_purchase_package))
                (activity as MainActivity).changeFragment(
                    ContactUsFragment.newInstance(),
                    ContactUsFragment.TAG,
                    bundle,
                    true
                )
            }

            R.id.ll_buyers -> {
                val bundle = Bundle()
                bundle.putString("name", getString(R.string.faqs_for_buyers))
                //  bundle.putString("catName",getString(R.string.txt_purchase_package))
                (activity as MainActivity).changeFragment(
                        FAQBuyersFragment.newInstance(),
                        FAQBuyersFragment.TAG,
                        bundle,
                        true
                )
            }

            R.id.ll_sellers -> {
                val bundle = Bundle()
                bundle.putString("name", getString(R.string.faqs_for_sellers))
                //  bundle.putString("catName",getString(R.string.txt_purchase_package))
                (activity as MainActivity).changeFragment(
                        FAQSellersFragment.newInstance(),
                        FAQSellersFragment.TAG,
                        bundle,
                        true
                )
            }

            R.id.ll_private_policy -> {
                val bundle = Bundle()
                bundle.putString("name", getString(R.string.txt_privacy_policy))
                //  bundle.putString("catName",getString(R.string.txt_purchase_package))
                (activity as MainActivity).changeFragment(
                        PrivacyPolicyFragment.newInstance(),
                        PrivacyPolicyFragment.TAG,
                        bundle,
                        true
                )
            }

            R.id.ll_terms_use -> {
                val bundle = Bundle()
                bundle.putString("name", getString(R.string.txt_terms_of_use))
                //  bundle.putString("catName",getString(R.string.txt_purchase_package))
                (activity as MainActivity).changeFragment(
                        TermsConditionFragment.newInstance(),
                        TermsConditionFragment.TAG,
                        bundle,
                        true
                )
            }

        }
    }

    companion object {
        val TAG = "SupportFragment"
        fun newInstance(): SupportFragment {
            return SupportFragment()
        }
    }
}