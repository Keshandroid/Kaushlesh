package com.kaushlesh.view.fragment.MyAccount.MyAds

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.kaushlesh.R
import com.kaushlesh.adapter.MyFavouritepostAdapter
import com.kaushlesh.bean.GetFavPostBean


import kotlinx.android.synthetic.main.fragment_my_post.rv_mypost


class MyShowcaseFavouriteFragment : Fragment(), MyFavouritepostAdapter.ItemClickListener {
    override fun itemRemove(bean: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_post_favourite, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

       /* if(list.size > 0) {
            ll_no_post_fav.visibility = View.GONE
            ll_no_showcase_fvrt.visibility = View.GONE
            bindRecyclerviewFuel()
        }
        else{
            ll_no_post_fav.visibility = View.GONE
            ll_no_showcase_fvrt.visibility = View.VISIBLE
        }*/

        bindRecyclerviewFuel()

        //for no favourite
      /*  ll_no_post_fav.visibility = View.GONE
        ll_no_showcase_fvrt.visibility = View.VISIBLE

        btn_start_explore_showcase_now.setOnClickListener {
            (activity as MyShowcaseActivity).setShowcase()
        }*/
    }

    private fun bindRecyclerviewFuel() {

        val layoutManager = GridLayoutManager(context, 2)
        rv_mypost.layoutManager = layoutManager
        val adapter = MyFavouritepostAdapter(context!!)
        adapter.setClicklistner(this)
        rv_mypost.adapter = adapter
        adapter.notifyDataSetChanged()

    }

    companion object {
        val TAG = "MyShowcaseFavouriteFragment"

        fun newInstance(): MyShowcaseFavouriteFragment {
            return MyShowcaseFavouriteFragment()
        }
    }

    override fun itemclickProduct(bean: GetFavPostBean.FavPostlist) {

    }
}