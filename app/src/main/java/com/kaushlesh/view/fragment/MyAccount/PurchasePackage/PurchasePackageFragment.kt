package com.kaushlesh.view.fragment.MyAccount.PurchasePackage

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar

import com.kaushlesh.R
import com.kaushlesh.view.activity.AdPost.AddBillingInformationActivity
import kotlinx.android.synthetic.main.fragment_purchase_package.*

class PurchasePackageFragment : Fragment(),View.OnClickListener {

    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_purchase_package, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar = view.findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.txt_purchase_package)

        btnback.setOnClickListener {
            if (activity != null) {
                activity!!.onBackPressed()
            }
        }

        addOnclicked()
    }

    private fun addOnclicked() {
        ll_choose_pkg.setOnClickListener(this)
        ll_myorder.setOnClickListener(this)
        ll_invoice.setOnClickListener(this)
        ll_submit_invoice.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.ll_choose_pkg -> {
                //now showcase is hide, so directly redirect to ad post package
                /*val intent = Intent(context, ChoosePackageActivity::class.java)
                startActivity(intent)*/

                val intent = Intent(context, AdPostPackagesActivity::class.java).putExtra("isFromAccount","account")
                startActivity(intent)
            }

            R.id.ll_invoice -> {
                val intent = Intent(context, InvoicesActivity::class.java)
                startActivity(intent)
            }

            R.id.ll_myorder -> {
                val intent = Intent(context, MyOrdersActivity::class.java)
                startActivity(intent)
            }

            R.id.ll_submit_invoice -> {
                val intent = Intent(context, AddBillingInformationActivity::class.java)
                startActivity(intent)
            }
        }

    }

    companion object {

        val TAG = "PurchasePackageFragment"

        fun newInstance(): PurchasePackageFragment {
            return PurchasePackageFragment()
        }
    }
}
