package com.kaushlesh.view.fragment.MyAccount.MyAds

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.kaushlesh.Controller.ScreenRedirectionController
import com.kaushlesh.R
import com.kaushlesh.adapter.MyShowcasePagerAdapter
import com.kaushlesh.view.activity.MainActivity


class MyShowcaseActivity : AppCompatActivity() {
    var tabLayout: TabLayout? = null
    var viewPager: ViewPager? = null

    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView

    private lateinit var screenRedirectionController: ScreenRedirectionController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_post)

        screenRedirectionController = ScreenRedirectionController(this)

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = getString(R.string.txt_my_ad_showcase)

        btnback.setOnClickListener {
            this.onBackPressed()
        }

        tabLayout = findViewById(R.id.tablaout)
        viewPager = findViewById(R.id.pager)

        tabLayout!!.addTab(tabLayout!!.newTab().setText(getString(R.string.txt_my_showcase)))
        tabLayout!!.addTab(tabLayout!!.newTab().setText(getString(R.string.txt_favourite)))
        tabLayout!!.tabGravity = TabLayout.GRAVITY_FILL

        val adapter = MyShowcasePagerAdapter(applicationContext, supportFragmentManager, tabLayout!!.tabCount)
        viewPager!!.adapter = adapter

        viewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))

        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager!!.currentItem = tab.position
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {

            }
            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
    }

    fun setShowcase() {

        screenRedirectionController.openShowCaseTab()
      /*  val mActivity = MainActivity()
        (mActivity as MainActivity).setAdShowcaseTab()
*/
    }

}