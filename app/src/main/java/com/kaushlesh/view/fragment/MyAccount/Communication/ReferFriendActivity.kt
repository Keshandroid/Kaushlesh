package com.kaushlesh.view.fragment.MyAccount.Communication

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.kaushlesh.BuildConfig
import com.kaushlesh.R
import kotlinx.android.synthetic.main.activity_refer_friend.*
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

class ReferFriendActivity : AppCompatActivity() {

    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_refer_friend)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        toolbar = findViewById(R.id.toolbar)
        tvtoolbartitle = toolbar.findViewById(R.id.title)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle.text = getString(R.string.invite_friends_on_gujrat_living)


        btnback.setOnClickListener { onBackPressed() }

        btn_refer.setOnClickListener {
            try {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.setType("text/plain")
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Gujarat Living")
                var shareMessage = "*Gujarat Living* \nGujarat's largest market place for Buy and Sell all type of products\n\n"
                shareMessage = """
                    ${shareMessage}https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}
                    """.trimIndent()
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                startActivity(Intent.createChooser(shareIntent, "choose one"))
            } catch (e: Exception) {
                //e.toString();
            }


            /*AsyncTask.execute {
                try {
                    val url = URL("https://picsum.photos/200")
                    var connection: HttpURLConnection? = null
                    connection = url.openConnection() as HttpURLConnection?
                    connection!!.connect()
                    var inputStream: InputStream? = null
                    inputStream = connection.inputStream
                    val myBitmap = BitmapFactory.decodeStream(inputStream)
                    val share = Intent(Intent.ACTION_SEND)
                    share.type = "Image/jpeg"
                    share.type = "text/html"
                    share.putExtra(Intent.EXTRA_TEXT, "Text message here")
                    val bytes = ByteArrayOutputStream()
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
                    val path = MediaStore.Images.Media.insertImage(
                            getContentResolver(),
                            myBitmap,
                            "Title",
                            null
                    )
                    val imageUri = Uri.parse(path)
                    share.putExtra(Intent.EXTRA_STREAM, imageUri)
                    startActivity(Intent.createChooser(share, "Select"))
                } catch (e: Exception) {
                }*/

            }


    }
}