package com.kaushlesh.view.fragment.MyAccount.MyAds

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.kaushlesh.Controller.PostLiveAgainController
import com.kaushlesh.R
import com.kaushlesh.adapter.AdPostSelectPackageAdapter
import com.kaushlesh.bean.AdPost.AdPostResponseBean
import com.kaushlesh.bean.GetMyPackagesBean
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.AdPost.CongratulationAdpostActivity
import com.kaushlesh.view.fragment.MyAccount.PurchasePackage.AdPostPackagesActivity
import kotlinx.android.synthetic.main.activity_select_extra_ad_package.*
import org.json.JSONObject


class SelectExtraAdPackageActivity : AppCompatActivity(),AdPostSelectPackageAdapter.ItemClickListener,View.OnClickListener ,ParseControllerListener{
    private val TAG = "SelectExtraAdPackageActivity"
    lateinit var storeUserData: StoreUserData
    internal var pkglist: ArrayList<GetMyPackagesBean.Packages> = ArrayList()
    internal var packageid: String? = ""
    internal var postId: String? = ""
    internal var catId: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_extra_ad_package)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
        }

        storeUserData = StoreUserData(this)

        initBindViews()

        if(intent != null)
        {
            postId = intent.getStringExtra("postId")
            catId = intent.getStringExtra("catId")
            Utils.showLog(TAG,"==post id==" + postId + "===cat id===" + catId)

            storeUserData.setString(Constants.POST_ID, postId.toString())

        }
        val extras = intent.extras
        if (extras != null) {
            pkglist = extras.getSerializable("pkglist") as ArrayList<GetMyPackagesBean.Packages>
            Utils.showLog(TAG, "==list==" + pkglist.size)
        }

        if(pkglist.size > 0)
        {
            bindRecyclerViewAdPostPackage(pkglist,0)
        }
    }

    private fun initBindViews() {

        btn_ad_post.setOnClickListener(this)
        tvsellalllist.setOnClickListener(this)
        btn_back.setOnClickListener(this)
    }


    private fun bindRecyclerViewAdPostPackage(newpkglist: ArrayList<GetMyPackagesBean.Packages>,pos: Int) {
        val recyclerLayoutManager = LinearLayoutManager(this)
        rv_Package.layoutManager = recyclerLayoutManager
        val adapter = AdPostSelectPackageAdapter(newpkglist, this)
        adapter.setClicklistner(this)
        rv_Package.adapter = adapter
        adapter.notifyDataSetChanged()
        rv_Package.scrollToPosition(pos)
    }

    override fun itemClicked(pos: Int, selected: Boolean) {

        if (selected == true) {
            //addButtonView(premiumpkglist[pos].price)
            packageid = pkglist[pos].packageid!!
            val userpostpackageid = pkglist[pos].userpostpackageid
            storeUserData.setString(Constants.USER_PKG_ID,userpostpackageid.toString())
        }
        for (i in 0 until pkglist.size) {
            if (pos == i && selected == true) {
                pkglist[i].selected = true
            } else {
                pkglist[i].selected = false
            }
        }
        bindRecyclerViewAdPostPackage(pkglist,pos)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_back -> {
              /*  val intent = Intent(this, MyPostActivity::class.java)
                intent.putExtra("from","myads")
                startActivity(intent)
                finishAffinity()*/
                onBackPressed()
            }
            R.id.btn_ad_post -> {
                if (packageid != null && packageid.toString().length == 0) {
                    Utils.showToast(this,getString(R.string.txt_select_pkg))
                } else {
                    PostLiveAgainController(this,postId.toString(),packageid.toString(),this)
                }
            }

            R.id.tvsellalllist -> {
                val intent = Intent(applicationContext, AdPostPackagesActivity::class.java)
                intent.putExtra("isPremium", "no")
                intent.putExtra("address", "")
                intent.putExtra("catId", catId)
                intent.putExtra("lat", "")
                intent.putExtra("long","")
                intent.putExtra("price", "")
                intent.putExtra("repost", "yes")
                intent.putExtra("postaddress","")
                startActivity(intent)
            }
        }
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if(method.equals("rePost"))
        {
            Utils.showToast(this,"Package Applied Successfully")
            val dataModel = AdPostResponseBean()
            dataModel.AdPostResponseBean(da)

            Utils.showLog(TAG, "===data===" + dataModel)
            val data = dataModel.pkglist.get(0)




            storeUserData.setString(Constants.USER_PKG_ID,"")
            //val intent = Intent(applicationContext, MyPostActivity::class.java)
            //intent.putExtra("from","attention")
            val intent = Intent(this@SelectExtraAdPackageActivity, CongratulationAdpostActivity::class.java)
            intent.putExtra("data", data)
            intent.putExtra("type", "pkglist")
            startActivity(intent)
            finishAffinity()


            /*
            Handler().postDelayed({
                if (!isFinishing) {

                    storeUserData.setString(Constants.USER_PKG_ID,"")
                    //val intent = Intent(applicationContext, MyPostActivity::class.java)
                    //intent.putExtra("from","attention")
                    val intent = Intent(this@SelectExtraAdPackageActivity, CongratulationAdpostActivity::class.java)
                    intent.putExtra("data", data)
                    intent.putExtra("type", "pkglist")
                    startActivity(intent)
                    finishAffinity()
                }
            }, 1000)*/
        }
    }

    override fun onFail(msg: String, method: String) {

    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}