package com.kaushlesh.view.fragment.ShowcaseDetail

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.kaushlesh.R
import com.kaushlesh.adapter.AppartmentAdapter
import com.kaushlesh.bean.BrandShowCaseBean
import kotlinx.android.synthetic.main.fragment_appartments.*
import java.util.ArrayList


class AppartmentsFragment : Fragment(),AppartmentAdapter.ItemClickListener {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_appartments, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindRecyclerviewAdShowCase()
    }

    companion object {
        val TAG = "Appartment"
        fun newInstance(): AppartmentsFragment {
            return AppartmentsFragment()
        }
    }


    @SuppressLint("UseRequireInsteadOfGet")
    private fun bindRecyclerviewAdShowCase() {

        val layoutManager = LinearLayoutManager(context)
        rv_appartment.layoutManager = layoutManager
        val adapter = AppartmentAdapter(categoryList, context!!)
        adapter.setClicklistner(this)
        rv_appartment.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    val categoryList: List<BrandShowCaseBean>
        get() {
            val list = ArrayList<BrandShowCaseBean>()

            list.add(BrandShowCaseBean(R.drawable.ic_property, "Residential Project"))
            list.add(BrandShowCaseBean(R.drawable.ic_shop_city, "Commercial Project"))
            list.add(BrandShowCaseBean(R.drawable.ic_jwellery, "Jewellery Showroom"))
            list.add(BrandShowCaseBean(R.drawable.ic_bikes, "Bikes Showroom"))
            list.add(BrandShowCaseBean(R.drawable.ic_cras, "Car Showroom"))
            list.add(BrandShowCaseBean(R.drawable.ic_industries, "Industry Showcase"))
            return list
        }

    override fun itemclickCategory(bean: BrandShowCaseBean) {

    }

}