package com.kaushlesh.view.fragment.ShowcaseDetail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.kaushlesh.R
import com.kaushlesh.view.activity.MainActivity
import kotlinx.android.synthetic.main.fragment_explore_residetialproject_detail.*
import kotlinx.android.synthetic.main.layout_toolbar.*


class ExploreResidetialprojectDetailFragment : Fragment(), View.OnClickListener {

    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(
            R.layout.fragment_explore_residetialproject_detail,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar = view.findViewById(R.id.toolbar)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = "Palarish Parsh"

        addOnClicked()
    }

    private fun addOnClicked() {
        btn_back.setOnClickListener(this)
        tv_read_more.setOnClickListener(this)
        ll_important_things.setOnClickListener(this)
        ll_location.setOnClickListener(this)
        ll_about_Builder.setOnClickListener(this)
        iv_property.setOnClickListener(this)

    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_back -> {
                if (activity != null) {
                    activity!!.onBackPressed()
                }
            }    R.id.iv_property -> {
            val bundle = Bundle()
            bundle.putString("name", getString(R.string.location))
            //    bundle.putString("catName",getString(R.string.txt_purchase_package))
            (activity as MainActivity).changeFragment(
                UnitConfigFragment.newInstance(),
                UnitConfigFragment.TAG, bundle, true
            )
            }
            R.id.ll_location -> {
                val bundle = Bundle()
                bundle.putString("name", getString(R.string.location))
                //    bundle.putString("catName",getString(R.string.txt_purchase_package))
                (activity as MainActivity).changeFragment(
                    LocationFragment.newInstance(),
                    LocationFragment.TAG, bundle, true
                )

            }
            R.id.ll_important_things -> {
                val bundle = Bundle()
                bundle.putString("name", "Important things")
                //    bundle.putString("catName",getString(R.string.txt_purchase_package))
                (activity as MainActivity).changeFragment(
                    ImportantThingsKnowFragment.newInstance(),
                    ImportantThingsKnowFragment.TAG, bundle, true
                )
            }
            R.id.ll_about_Builder -> {
                val bundle = Bundle()
                bundle.putString("name", getString(R.string.about_builder))
                //    bundle.putString("catName",getString(R.string.txt_purchase_package))
                (activity as MainActivity).changeFragment(
                    AboutBuilderFragment.newInstance(),
                    AboutBuilderFragment.TAG, bundle, true
                )
            }
            R.id.tv_read_more -> {
                val bundle = Bundle()
                bundle.putString("name", getString(R.string.about_project))
                //bundle.putString("catName",getString(R.string.txt_purchase_package))
                (activity as MainActivity).changeFragment(
                    AboutProjectFragment.newInstance(),
                    AboutProjectFragment.TAG, bundle, true
                )
            }
        }
    }

    companion object {
        val TAG = "ExploreResidetialprojectDetailFragment"
        fun newInstance(): ExploreResidetialprojectDetailFragment {
            return ExploreResidetialprojectDetailFragment()
        }
    }


}