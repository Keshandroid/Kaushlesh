package com.kaushlesh.view.fragment.ShowcaseDetail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.kaushlesh.R
import com.kaushlesh.adapter.MyAdapter


class UnitConfigFragment : Fragment() {

    var tabLayout: TabLayout? = null
    var viewPager: ViewPager? = null

    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_unit_config, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar = view.findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = getString(R.string.str_unit_config)

        btnback.setOnClickListener {
            if (activity != null) {
                activity!!.onBackPressed()
            }
        }


        tabLayout = view.findViewById(R.id.tablaout)
        viewPager = view.findViewById(R.id.pager)

        tabLayout!!.addTab(tabLayout!!.newTab().setText("2BHK"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("3BHK"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("4BHK"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("5BHK"))
        tabLayout!!.tabGravity = TabLayout.GRAVITY_FILL

        val adapter = MyAdapter(view.context, childFragmentManager, tabLayout!!.tabCount)
        viewPager!!.adapter = adapter

        viewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))

        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager!!.currentItem = tab.position
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {

            }
            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
}
    companion object {
        val TAG = "Location"
        fun newInstance(): UnitConfigFragment {
            return UnitConfigFragment()
        }
    }
}
