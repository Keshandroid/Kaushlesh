package com.kaushlesh.view.fragment

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.kaushlesh.Controller.ScreenRedirectionController
import com.kaushlesh.R
import com.kaushlesh.adapter.MyAdsPagerAdapter
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.fragment.MyAccount.MyAds.MyPostAdFragment

class MyAdsFragment : Fragment() {

    var tabLayout: TabLayout? = null
    var viewPager: ViewPager? = null

    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var btndelete: ImageView
    internal lateinit var tvtoolbartitle: TextView
    var isfrom : String = ""
    internal lateinit var dialog: Dialog
    private lateinit var screenRedirectionController: ScreenRedirectionController
    lateinit var storeUserData: StoreUserData
    var radioGroup: RadioGroup? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_ads, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        screenRedirectionController = ScreenRedirectionController(activity!!)
        storeUserData=StoreUserData(context!!)
        storeUserData.setString(Constants.POST_FROM,"fragment")
        storeUserData.setString(Constants.FROM_DETAIL, "false")

        toolbar = view.findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        btndelete = toolbar.findViewById(R.id.btn_delete)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        btnback.visibility = View.GONE
        tvtoolbartitle.text = getString(R.string.my_post)


        btndelete.setOnClickListener {
            openConfirmationDialog()
        }


        if (arguments != null) {
            isfrom = arguments!!.getString("from").toString()
            AppConstants.printLog(TAG, "==from==" + isfrom!!)
        }

        tabLayout = view.findViewById(R.id.tablaout)
        viewPager = view.findViewById(R.id.pager)

        tabLayout!!.addTab(tabLayout!!.newTab().setText(getString(R.string.txt_my_ads)))
        tabLayout!!.addTab(tabLayout!!.newTab().setText(getString(R.string.txt_favourite)))
        tabLayout!!.tabGravity = TabLayout.GRAVITY_FILL

        val adapter = MyAdsPagerAdapter(context!!, childFragmentManager, tabLayout!!.tabCount)
        viewPager!!.adapter = adapter

        viewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))

        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager!!.currentItem = tab.position
                Utils.showLog(TAG,"current tab==> " + tab.position.toString())
                if(tab.position == 1)
                {
                    btndelete.visibility = View.GONE
                }
                else{
                    btndelete.visibility = View.VISIBLE
                }
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {

            }
            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
    }

    companion object {
            val TAG = "MyAdsFragment"

            fun newInstance(): MyAdsFragment {
                return MyAdsFragment()
            }
    }

    fun setAdPost() {
        screenRedirectionController.openAdPostTab()
    }

    fun setHomeScreen() {
        screenRedirectionController.openHomeTab()
    }


    private fun openConfirmationDialog() {

        dialog = Dialog(context!!)
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_delete_ads)
        // Set dialog title
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        //dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_white);
        // set values for custom dialog components - text, image and button
        radioGroup = dialog.findViewById(R.id.radioGroup)

        val cbexpired = dialog.findViewById(R.id.cbexpired) as CheckBox
        val cbrejected = dialog.findViewById(R.id.cbrejected) as CheckBox
        val btnyes = dialog.findViewById(R.id.btn_yes) as TextView
        val btnno = dialog.findViewById(R.id.btn_no) as TextView

        btnyes.setOnClickListener(View.OnClickListener {
            //checkbox
            if(cbexpired.isChecked && cbrejected.isChecked)
            {
                MyPostAdFragment.getInstance()?.callApiToRefreshDeletedPost("2,3")
                dialog.dismiss()
            }
            else {
                if (cbexpired.isChecked || cbrejected.isChecked) {
                    if (cbexpired.isChecked) {
                        MyPostAdFragment.getInstance()?.callApiToRefreshDeletedPost("3")
                    } else if (cbrejected.isChecked) {
                        MyPostAdFragment.getInstance()?.callApiToRefreshDeletedPost("2")
                    }
                    dialog.dismiss()
                } else {
                    Toast.makeText(context, "Please select one option", Toast.LENGTH_SHORT).show()
                }
            }

            //radio buttoon
            /* val selectedId: Int = radioGroup!!.checkedRadioButtonId

             val radioButton = dialog.findViewById<RadioButton>(selectedId)
             Log.e(TAG, "===selected radio Button===$selectedId")
             if (selectedId == -1) {
                 Toast.makeText(this, "Please select one option", Toast.LENGTH_SHORT).show()
             } else {

                 if(radioButton.getText().toString().equals(resources.getString(R.string.txt_delete_expired_post)))
                 {
                     MyPostAdFragment.getInstance()?.callApiToRefreshDeletedPost(1)
                 }
                 else{
                     MyPostAdFragment.getInstance()?.callApiToRefreshDeletedPost(2)
                 }

                 dialog.dismiss()
             }*/
        })

        btnno.setOnClickListener(View.OnClickListener { dialog.dismiss() })
        dialog.show()
    }
}