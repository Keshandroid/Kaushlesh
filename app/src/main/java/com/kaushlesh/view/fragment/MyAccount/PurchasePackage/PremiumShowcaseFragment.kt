package com.kaushlesh.view.fragment.MyAccount.PurchasePackage

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.kaushlesh.R
import com.kaushlesh.adapter.AdPostPackageAdapter
import com.kaushlesh.bean.GetCategoryPackagesBean
import com.kaushlesh.utils.Utils
import kotlinx.android.synthetic.main.fragment_extraad.*


class PremiumShowcaseFragment : Fragment(), AdPostPackageAdapter.ItemClickListener  {
    internal var extraadlist: ArrayList<GetCategoryPackagesBean.Packages> = ArrayList()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_extraad, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindRecyclerviewFuel()
    }

    private fun bindRecyclerviewFuel() {
        val recyclerLayoutManager = GridLayoutManager(context, 2)
        rv_extra.layoutManager = recyclerLayoutManager
        rv_extra.setHasFixedSize(true);
        val adapter = AdPostPackageAdapter(context!!,"premiumShowcase",extraadlist)
        rv_extra.adapter = adapter
        adapter.notifyDataSetChanged()
    }
    companion object {
        val TAG = "PremiumShowcaseFragment"
        fun newInstance(): PremiumShowcaseFragment {
            return PremiumShowcaseFragment()
        }
    }

    override fun itemclickProduct(bean: GetCategoryPackagesBean.Packages, type: String) {

        Utils.showToast(activity,"clicked")
    }
}