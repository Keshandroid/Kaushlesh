package com.kaushlesh.view.fragment.MyAccount.PurchasePackage
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.kaushlesh.R
import com.kaushlesh.adapter.BrandShowcasePackagesPagerAdapter
import kotlinx.android.synthetic.main.activity_brand_showcase.*


class BrandShowcasePackagesActivity : AppCompatActivity(), View.OnClickListener {
    var tabLayout: TabLayout? = null
    var viewPager: ViewPager? = null

    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal lateinit var dialog: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_brand_showcase)
        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)
        tvtoolbartitle.text = getString(R.string.brand_showcase_package)

        setOnclick()

        tabLayout = findViewById(R.id.tablaout)
        viewPager = findViewById(R.id.pager)

        tabLayout!!.addTab(tabLayout!!.newTab().setText(getString(R.string.txt_regualr_showcase)))
        tabLayout!!.addTab(tabLayout!!.newTab().setText(getString(R.string.txt_premium_showcase)))

        tabLayout!!.tabGravity = TabLayout.GRAVITY_FILL

        val adapter = BrandShowcasePackagesPagerAdapter(applicationContext, supportFragmentManager, tabLayout!!.tabCount)

        viewPager!!.adapter = adapter

        viewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))

        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager!!.currentItem = tab.position
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {

            }
            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

    }

    private fun setOnclick() {

        btnback.setOnClickListener(this)
        ll_info.setOnClickListener(this)
        tv_view_example.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btn_back -> onBackPressed()

            R.id.ll_info ->
            {
                openInfoDialog()
            }

            R.id.tv_view_example ->
            {
                val intent = Intent(this, PremiumShowcaseExampleActivity::class.java)
                startActivity(intent)
            }
        }
    }

    private fun openInfoDialog() {

        dialog = Dialog(this)
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_info_showcase_packages)
        // Set dialog title
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        // set values for custom dialog components - text, image and button
        val ivclose = dialog.findViewById<ImageView>(R.id.ivclose)
        ivclose.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }
}