package com.kaushlesh.view.fragment.MyAccount.MyAds

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.kaushlesh.R
import kotlinx.android.synthetic.main.activity_know_more_detail.*
import kotlinx.android.synthetic.main.activity_know_more_detail_showcase.*
import kotlinx.android.synthetic.main.activity_know_more_detail_showcase.tvtnc
import java.lang.Exception

class KnowMoreDetailShowcaseActivity : AppCompatActivity()  , View.OnClickListener{


    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_know_more_detail_showcase)

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)
        tvtoolbartitle.text = getString(R.string.txt_know_more)

        btnback.setOnClickListener(this)

        try {
            tvtnc.setAllCaps(false);
            tvtnc.makeLinks(
                    Pair(getString(R.string.txt_terms_of_use), View.OnClickListener {
                        val browserIntent = Intent(Intent.ACTION_VIEW)
                        browserIntent.data = Uri.parse("http://www.google.com")
                        startActivity(browserIntent)
                    }),
                    Pair(getString(R.string.txt_privacy_policy), View.OnClickListener {
                        val browserIntent = Intent(Intent.ACTION_VIEW)
                        browserIntent.data = Uri.parse("https://www.freeprivacypolicy.com/")
                        startActivity(browserIntent)
                    }))
        }catch (e: Exception){
            e.printStackTrace()
        }


    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btn_back -> onBackPressed()
        }
    }

    fun TextView.makeLinks(vararg links: Pair<String, View.OnClickListener>) {
        val spannableString = SpannableString(this.text)
        var startIndexOfLink = -1
        for (link in links) {
            val clickableSpan = object : ClickableSpan() {
                override fun updateDrawState(textPaint: TextPaint) {
                    // use this to change the link color
                    // textPaint.color = textPaint.linkColor
                    textPaint.color = resources.getColor(R.color.black)
                    // toggle below value to enable/disable
                    // the underline shown below the clickable text
                    textPaint.isUnderlineText = true
                }

                override fun onClick(view: View) {
                    Selection.setSelection((view as TextView).text as Spannable, 0)
                    view.invalidate()
                    link.second.onClick(view)
                }
            }
            startIndexOfLink = this.text.toString().indexOf(link.first, startIndexOfLink + 1)
//      if(startIndexOfLink == -1) continue // todo if you want to verify your texts contains links text
            spannableString.setSpan(
                clickableSpan, startIndexOfLink, startIndexOfLink + link.first.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
        this.movementMethod =
            LinkMovementMethod.getInstance() // without LinkMovementMethod, link can not click
        this.setText(spannableString, TextView.BufferType.SPANNABLE)
    }
}
