package com.kaushlesh.view.fragment.ShowcaseDetail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.R
import com.kaushlesh.adapter.ExporeResidetialprojectAdapter
import com.kaushlesh.bean.BrandShowCaseBean
import com.kaushlesh.view.activity.MainActivity
import kotlinx.android.synthetic.main.fragment_expore_residetialproject.*
import java.util.ArrayList

class ExporeResidetialprojectFragment : Fragment(),ExporeResidetialprojectAdapter.ItemClickListener {

    internal lateinit var rvshowcase: RecyclerView
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_expore_residetialproject, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar = view.findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.txt_explore_Residetial_project)

        //rvshowcase = view.findViewById(R.id.rv_adshowcase)

        bindRecyclerviewAdShowCase()

        btnback.setOnClickListener {
            if (activity != null) {
                activity!!.onBackPressed()
            }
        }
    }

    private fun bindRecyclerviewAdShowCase() {

        val layoutManager = LinearLayoutManager(context)
        rv_adshowcase.layoutManager = layoutManager
        val adapter = ExporeResidetialprojectAdapter(categoryList, context!!)
        adapter.setClicklistner(this)
        rv_adshowcase.adapter = adapter
        adapter.notifyDataSetChanged()
    }


    val categoryList: List<BrandShowCaseBean>
        get() {
            val list = ArrayList<BrandShowCaseBean>()

            list.add(BrandShowCaseBean(R.drawable.ic_property, "Residential Project"))
            list.add(BrandShowCaseBean(R.drawable.ic_shop_city, "Commercial Project"))
            list.add(BrandShowCaseBean(R.drawable.ic_jwellery, "Jewellery Showroom"))
            list.add(BrandShowCaseBean(R.drawable.ic_bikes, "Bikes Showroom"))
            list.add(BrandShowCaseBean(R.drawable.ic_cras, "Car Showroom"))
            list.add(BrandShowCaseBean(R.drawable.ic_industries, "Industry Showcase"))
            //list.add(BrandShowCaseBean(R.drawable.ic_electronics, "Electronics & Appliances Showroom"))
            //list.add(BrandShowCaseBean(R.drawable.ic_furniture, "Furniture Showroom"))
            //list.add(BrandShowCaseBean(R.drawable.ic_art, "Hotel & Resorts Showcase"))
            //list.add(BrandShowCaseBean(R.drawable.ic_art, "Restora Showcase"))
            //list.add(BrandShowCaseBean(R.drawable.ic_grain, "View All Categories"))

            return list
        }

    override fun itemclickCategory(bean: BrandShowCaseBean) {
        val bundle = Bundle()
        bundle.putString("name", "Expore Residetial project")
        //  bundle.putString("catName",getString(R.string.txt_purchase_package))
        (activity as MainActivity).changeFragment(
            ExploreResidetialprojectDetailFragment.newInstance(),
            ExploreResidetialprojectDetailFragment.TAG,
            bundle,
            true
        )
    }

    companion object {
        val TAG = "ExporeResidetialprojectFragment"
        fun newInstance(): ExporeResidetialprojectFragment {
            return ExporeResidetialprojectFragment()
        }
    }
}