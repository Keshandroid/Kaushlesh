package com.kaushlesh.view.fragment.MyAccount.MyAds

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.kaushlesh.Controller.MyPostShowcaseDeleteController
import com.kaushlesh.R
import com.kaushlesh.adapter.MyShowcaseAdapter

import com.kaushlesh.bean.ProductDataBean
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.view.activity.MainActivity

import kotlinx.android.synthetic.main.fragment_my_post.*
import org.json.JSONObject


class MyShowcaseAdFragment : Fragment(), MyShowcaseAdapter.ItemClickListener , ParseControllerListener {

    private lateinit var myPostShowcaseDeleteController: MyPostShowcaseDeleteController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_post, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        myPostShowcaseDeleteController = MyPostShowcaseDeleteController(activity!!,this)

        if(list.size > 0) {
            ll_no_post.visibility = View.GONE
            ll_no_showcase.visibility = View.GONE
            bindRecyclerviewFuel()
        }
        else{
            ll_no_post.visibility = View.GONE
            ll_no_showcase.visibility = View.VISIBLE
        }

        //for no showcase
      /*  ll_no_post.visibility = View.GONE
        ll_no_showcase.visibility = View.VISIBLE

        btn_start_post_showcase.setOnClickListener {

            (activity as MyShowcaseActivity).setShowcase()
        }*/
    }

    private fun bindRecyclerviewFuel() {
        val recyclerLayoutManager = LinearLayoutManager(context)
        rv_mypost.layoutManager = recyclerLayoutManager
        val adapter = MyShowcaseAdapter(context!!,list,"showcase")
        rv_mypost.adapter = adapter
        adapter.setClicklistner(this)
        adapter.notifyDataSetChanged()
    }

    companion object {
        val TAG = "MyShowcaseAdFragment"

        fun newInstance(): MyShowcaseAdFragment {
            return MyShowcaseAdFragment()
        }
    }

    private val list: ArrayList<String>
        get() {
            val list = ArrayList<String>()

            list.add("1")
            list.add("2")
            list.add("3")
            list.add("2")
            list.add("1")

            return list
        }

    override fun itemclickProduct(bean: ProductDataBean) {

    }

    override fun itemclickDelete(position: Int) {
        myPostShowcaseDeleteController.deleteAd("showcase")
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {

    }

    override fun onFail(msg: String, method: String) {

    }
}