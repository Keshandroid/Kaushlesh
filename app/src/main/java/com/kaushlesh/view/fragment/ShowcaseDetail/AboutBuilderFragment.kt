package com.kaushlesh.view.fragment.ShowcaseDetail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.kaushlesh.R


class AboutBuilderFragment : Fragment() {
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_about_builder, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar = view.findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.about_builder)


        //rvshowcase = view.findViewById(R.id.rv_adshowcase)

        btnback.setOnClickListener {
            if (activity != null) {
                requireActivity().onBackPressed()
            }
        }
    }
    companion object {
        val TAG = "About Builder"
        fun newInstance(): AboutBuilderFragment {
            return AboutBuilderFragment()
        }
    }

}