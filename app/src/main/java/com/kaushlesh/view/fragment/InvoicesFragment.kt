package com.kaushlesh.view.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.kaushlesh.R
import com.kaushlesh.adapter.InvoicesAdapter
import com.kaushlesh.bean.InvoicesBean
import com.kaushlesh.bean.MyPackageOrderBean
import java.util.ArrayList

class InvoicesFragment : Fragment(), InvoicesAdapter.ItemClickListener {
    internal lateinit var rvinvoice: RecyclerView
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView

    val listOrder = ArrayList<MyPackageOrderBean.MyPackageOrderList>()


    /*private val list: List<InvoicesBean>
        get() {
            val list = ArrayList<InvoicesBean>()

            list.add(InvoicesBean("9988776655", "15 october", "200.00"))
            list.add(InvoicesBean("9988776654", "14 october", "200.00"))

            return list
        }*/


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_invoices, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar = view.findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.txt_invoices)

        rvinvoice = view.findViewById(R.id.rv_invoices)

        bindRecyclerview()

        btnback.setOnClickListener {
            if (activity != null) {
                activity!!.onBackPressed()
            }
        }
    }

    private fun bindRecyclerview() {
        val recyclerLayoutManager = LinearLayoutManager(context)
        rvinvoice.layoutManager = recyclerLayoutManager
        rvinvoice.itemAnimator = DefaultItemAnimator()
        val adapter = InvoicesAdapter(listOrder, context!!)
        adapter.setClicklistner(this)
        rvinvoice.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    companion object {

        val TAG = "InvoicesFragment"

        fun newInstance(): InvoicesFragment {
            return InvoicesFragment()
        }
    }

    override fun itemclick(bean: MyPackageOrderBean.MyPackageOrderList) {
    }
}

