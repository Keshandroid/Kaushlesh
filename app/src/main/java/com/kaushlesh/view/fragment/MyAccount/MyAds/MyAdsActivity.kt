package com.kaushlesh.view.fragment.MyAccount.MyAds

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.kaushlesh.R
import kotlinx.android.synthetic.main.activity_my_ads.*


class MyAdsActivity : AppCompatActivity(), View.OnClickListener {

    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_ads)
        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)
        tvtoolbartitle.text = getString(R.string.str_my_ads_post)

        setOnclick()
    }

    private fun setOnclick() {
        rlpost.setOnClickListener(this)
        btnback.setOnClickListener(this)
        rlshowcase.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btn_back -> onBackPressed()

            R.id.rlpost -> {
                val intent = Intent(this, MyPostActivity::class.java)
                intent.putExtra("from","myads")
                startActivity(intent)
            }
            R.id.rlshowcase -> {
                val intent = Intent(this, MyShowcaseActivity::class.java)
                startActivity(intent)
            }
        }
    }
}