package com.kaushlesh.view.fragment

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.bumptech.glide.Glide
import com.kaushlesh.Controller.GetListingController

import com.kaushlesh.R
import com.kaushlesh.bean.UserProfileBean
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.Identity
import com.kaushlesh.utils.PostDetail.SetPostDetailData
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.ChatActivity
import com.kaushlesh.view.activity.Login.LoginMainActivity
import com.kaushlesh.view.activity.MainActivity
import com.kaushlesh.view.fragment.MyAccount.PurchasePackage.MyOrdersActivity
import com.kaushlesh.view.fragment.MyAccount.Communication.CommunicationFragment
import com.kaushlesh.view.fragment.MyAccount.Communication.QRCodeActivity
import com.kaushlesh.view.fragment.MyAccount.EditProfileActivity
import com.kaushlesh.view.fragment.MyAccount.MyAds.MyPostActivity
import com.kaushlesh.view.fragment.MyAccount.MyNetwork.FollowFollowingListActivity
import com.kaushlesh.view.fragment.MyAccount.PurchasePackage.PurchasePackageFragment
import com.kaushlesh.view.fragment.MyAccount.Settings.PersonalSettingsFragment
import kotlinx.android.synthetic.main.fragment_my_account.*
import kotlinx.android.synthetic.main.fragment_my_account.civ_profile
import org.json.JSONException
import org.json.JSONObject
import java.lang.Exception

class MyAccountFragment : Fragment(), View.OnClickListener ,ParseControllerListener{
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var imgQr: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal lateinit var llnetwork: LinearLayout
    internal lateinit var llorderpkg: LinearLayout
    internal lateinit var llsetting: LinearLayout
    internal lateinit var llhelp: LinearLayout
    internal lateinit var llads: LinearLayout
    internal lateinit var llchat: LinearLayout
    internal lateinit var llpurchasepkg: LinearLayout
    internal lateinit var llcommunication: LinearLayout
    internal lateinit var llmyprofile: LinearLayout
    internal lateinit var dialog: Dialog
    internal lateinit var ll_sponsered_ad: LinearLayout
    lateinit var getListingController: GetListingController
    var profilecount : Int =0

    var staticWhatsappNumber: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_account, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar = view.findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        imgQr = toolbar.findViewById(R.id.imgQr)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        btnback.visibility = View.GONE
        //imgQr.visibility = View.VISIBLE
        tvtoolbartitle.text = resources.getString(R.string.txt_my_account)

        llmyprofile = view.findViewById(R.id.ll_myprofile)
        ll_sponsered_ad = view.findViewById(R.id.ll_sponsered_ad);
        llnetwork = view.findViewById(R.id.ll_network)
        llorderpkg = view.findViewById(R.id.ll_pkg_order)
        llsetting = view.findViewById(R.id.ll_setting)
        llhelp = view.findViewById(R.id.ll_help)
        llads = view.findViewById(R.id.ll_adds)
        llchat = view.findViewById(R.id.ll_chat)
        llpurchasepkg = view.findViewById(R.id.ll_purchase_pkg)
        llcommunication = view.findViewById(R.id.ll_communication)


        llnetwork.setOnClickListener(this)
        llorderpkg.setOnClickListener(this)
        llsetting.setOnClickListener(this)
        llhelp.setOnClickListener(this)
        llads.setOnClickListener(this)
        llchat.setOnClickListener(this)
        llpurchasepkg.setOnClickListener(this)
        llcommunication.setOnClickListener(this)
        llmyprofile.setOnClickListener(this)
        ll_sponsered_ad.setOnClickListener(this)
        btn_editprofile.setOnClickListener(this)
        ll_logout.setOnClickListener(this)


        imgQr.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_editprofile -> {
                val intent = Intent(context, EditProfileActivity::class.java)
                intent.putExtra("from","profile")
                startActivity(intent)
            }

            R.id.ll_myprofile -> {
            }

            R.id.imgQr -> {
                val intent = Intent(context, QRCodeActivity::class.java)
                startActivity(intent)
            }

            R.id.ll_sponsered_ad -> {
                var appPackage = ""
                if (SetPostDetailData.isAppInstalled(requireContext(), "com.whatsapp")) {
                    appPackage = "com.whatsapp"
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data =
                            Uri.parse("http://api.whatsapp.com/send?phone=" + staticWhatsappNumber + "&text=")
                    startActivity(intent)
                } else if (SetPostDetailData.isAppInstalled(requireContext(), "com.whatsapp.w4b")) {
                    appPackage = "com.whatsapp.w4b"
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data =
                            Uri.parse("http://api.whatsapp.com/send?phone=" + staticWhatsappNumber + "&text=")

                    startActivity(intent)
                } else {
                    Utils.showToast(requireActivity(), "Whatsapp not installed on your device")
                }
            }

            R.id.ll_network -> {
                val intent = Intent(context, FollowFollowingListActivity::class.java)
                startActivity(intent)
            }

            R.id.ll_pkg_order -> {
                val intent = Intent(context, MyOrdersActivity::class.java)
                startActivity(intent)
            }

            R.id.ll_setting -> {
                (activity as MainActivity).changeFragment(PersonalSettingsFragment.newInstance(),PersonalSettingsFragment.TAG, null, true)

            }

            R.id.ll_help -> {
            }

            R.id.ll_adds -> {
                //now showcase is hide, so direclty redirect to myads screen
              /*  val intent = Intent(context, MyAdsActivity::class.java)
                startActivity(intent)*/

                val intent = Intent(context, MyPostActivity::class.java)
                intent.putExtra("from","myads")
                startActivity(intent)
            }

            R.id.ll_chat -> {
                val intent1 = Intent(context, ChatActivity::class.java)
                startActivity(intent1)
            }

            R.id.ll_purchase_pkg -> {
                (activity as MainActivity).changeFragment(PurchasePackageFragment.newInstance(),PurchasePackageFragment.TAG, null, true)
            }

            R.id.ll_communication -> {
                (activity as MainActivity).changeFragment(CommunicationFragment.newInstance(),CommunicationFragment.TAG, null, true)

            }
            R.id.ll_logout ->
            {
                openLogoutDialog()
            }
        }
    }


    private fun openLogoutDialog() {

        dialog = Dialog(context!!)
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_logout)
        // Set dialog title
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        //dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_white);
        // set values for custom dialog components - text, image and button

        val tvtitle = dialog.findViewById(R.id.tv_title) as TextView
        tvtitle.setText(resources.getText(R.string.txt_logout_warning))
        val btnyes = dialog.findViewById(R.id.btn_yes) as TextView
        val btnno = dialog.findViewById(R.id.btn_no) as TextView

        btnyes.setOnClickListener(View.OnClickListener {

            val storeusedata = StoreUserData(context!!)
            storeusedata.setString(Constants.USER_ID, "")
            storeusedata.setString(Constants.TOKEN, "")
            storeusedata.setString(Constants.IS_LOGGED_IN, "")
            storeusedata.setString(Constants.IS_OTP_VERIFED, "")
            storeusedata.setString(Constants.IS_ACTIVE,"")
            storeusedata.setString(Constants.DEVICE_TOKEN,"")
            storeusedata.setString(Constants.DEVICE_TYPE, "")
            storeusedata.setString(Constants.REGISTREATION_TYPE, "")

            val intent = Intent(context, LoginMainActivity::class.java)
            startActivity(intent)
            activity!!.finishAffinity()

            dialog.dismiss()
        })

        btnno.setOnClickListener(View.OnClickListener { dialog.dismiss() })

        dialog.show()
    }

    companion object {

        val TAG = "MyAccountFragment"

        fun newInstance(): MyAccountFragment {
            return MyAccountFragment()
        }
    }

    override fun onResume() {
        super.onResume()
        profilecount = 0
        getListingController = GetListingController(activity!!, this)
        getListingController.getUserProfile()

        //contact us whatsapp
        getListingController.getContactUs(2)


    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if(method.equals("profile"))
        {
            val userModel = UserProfileBean()
            userModel.UserProfileBean(da)

            setProgressData(userModel)
            try {
                /*if (userModel.fname != null && userModel.fname.toString().length > 0 && userModel.email != null && userModel.email.toString().length > 0) {
//                    tv_name.setText(userModel.fname.toString())
                }*/

                if(userModel.fname != null && userModel.fname.toString().length > 0){
                    tv_name.setText(userModel.fname.toString())
                }



                if(userModel.phone != null && userModel.phone.toString().length >0 && userModel.locationId != null && userModel.locationId.toString().length >0)
                {
                    val storeusedata = StoreUserData(context!!)
                    storeusedata.setString(Constants.IS_LOGGED_IN, "true")
                }
                else{
                    val storeusedata = StoreUserData(context!!)
                    storeusedata.setString(Constants.IS_LOGGED_IN, "false")
                }

                Utils.showLog(TAG, "===profile img===" + userModel.userProfile)

                if (userModel.userProfile != null && userModel.userProfile.toString().length > 0) {
                    Glide.with(this)
                        .load(userModel.userProfile).placeholder(R.drawable.ic_user_pic)
                        .into(civ_profile)
                } else {
                    //civ_profile.setBackgroundResource(R.drawable.ic_user_pic)
                    civ_profile.setImageDrawable(null)
                }

            }catch (e: NullPointerException)
            {
                e.printStackTrace()
            }

        }

        if (method.equals("contactUs")) {
            if (da.length() > 0) {
                Log.e("get contactUs", "$da==")
                try {

                    if(da.has("result")) {
                        val data = da.getJSONObject("result")

                        if(data.has("staticWhatsappNumber")){
                            val wpNumber  = data.getString("staticWhatsappNumber")

                            if(wpNumber.toString() != ""){
                                staticWhatsappNumber = wpNumber
                            }
                        }

                        if(data.has("staticNumber")){
                            try {
                                val contactUsNumber  = data.getString("staticNumber")
                                Identity.setContactUsNumber(context, contactUsNumber)
                            }catch (e: Exception){
                                e.printStackTrace()
                            }
                        }

                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }

    }

    private fun setProgressData(userModel: UserProfileBean) {

        val profile = userModel.userProfile.toString()
        val fname = userModel.fname.toString()
        val phone = userModel.phone.toString()
        val district = userModel.locationId.toString()
        val email = userModel.email.toString()
        val about = userModel.aboutyou.toString()

        if(profile.isNotEmpty())
        {
            profilecount += 1
        }

        if(fname.isNotEmpty())
        {
            profilecount += 1
        }

        if(phone.isNotEmpty())
        {
            profilecount += 1
        }

        if(district.isNotEmpty())
        {
            profilecount += 1
        }

        if(email.isNotEmpty())
        {
            profilecount += 1
        }

        if(about.isNotEmpty())
        {
            profilecount += 1
        }

        Utils.showLog(TAG,"====count====" + profilecount)

        try {
            setProgress(profilecount)
        }
        catch (e : NullPointerException)
        {
            e.printStackTrace()
        }
        catch (e : IllegalStateException)
        {
            e.printStackTrace()
        }
    }

    private fun setProgress(count: Int) {
        when(count)
        {
            1->
            {
                ll1.setBackgroundColor(resources.getColor(R.color.orangelight))
                ll2.setBackgroundColor(resources.getColor(R.color.line))
                ll3.setBackgroundColor(resources.getColor(R.color.line))
                ll4.setBackgroundColor(resources.getColor(R.color.line))
                ll5.setBackgroundColor(resources.getColor(R.color.line))
                ll6.setBackgroundColor(resources.getColor(R.color.line))
            }
            2->
            {
                ll1.setBackgroundColor(resources.getColor(R.color.orangelight))
                ll2.setBackgroundColor(resources.getColor(R.color.orangelight))
                ll3.setBackgroundColor(resources.getColor(R.color.line))
                ll4.setBackgroundColor(resources.getColor(R.color.line))
                ll5.setBackgroundColor(resources.getColor(R.color.line))
                ll6.setBackgroundColor(resources.getColor(R.color.line))
            }
            3->
            {
                ll1.setBackgroundColor(resources.getColor(R.color.orangelight))
                ll2.setBackgroundColor(resources.getColor(R.color.orangelight))
                ll3.setBackgroundColor(resources.getColor(R.color.orangelight))
                ll4.setBackgroundColor(resources.getColor(R.color.line))
                ll5.setBackgroundColor(resources.getColor(R.color.line))
                ll6.setBackgroundColor(resources.getColor(R.color.line))
            }
            4->
            {
                ll1.setBackgroundColor(resources.getColor(R.color.orangelight))
                ll2.setBackgroundColor(resources.getColor(R.color.orangelight))
                ll3.setBackgroundColor(resources.getColor(R.color.orangelight))
                ll4.setBackgroundColor(resources.getColor(R.color.orangelight))
                ll5.setBackgroundColor(resources.getColor(R.color.line))
                ll6.setBackgroundColor(resources.getColor(R.color.line))
            }
            5->
            {
                ll1.setBackgroundColor(resources.getColor(R.color.orangelight))
                ll2.setBackgroundColor(resources.getColor(R.color.orangelight))
                ll3.setBackgroundColor(resources.getColor(R.color.orangelight))
                ll4.setBackgroundColor(resources.getColor(R.color.orangelight))
                ll5.setBackgroundColor(resources.getColor(R.color.orangelight))
                ll6.setBackgroundColor(resources.getColor(R.color.line))
            }
            6->
            {
                ll1.setBackgroundColor(resources.getColor(R.color.orangelight))
                ll2.setBackgroundColor(resources.getColor(R.color.orangelight))
                ll3.setBackgroundColor(resources.getColor(R.color.orangelight))
                ll4.setBackgroundColor(resources.getColor(R.color.orangelight))
                ll5.setBackgroundColor(resources.getColor(R.color.orangelight))
                ll6.setBackgroundColor(resources.getColor(R.color.orangelight))
            }
        }
    }

    override fun onFail(msg: String, method: String) {
        Utils.showToast(activity,msg)
    }
}
