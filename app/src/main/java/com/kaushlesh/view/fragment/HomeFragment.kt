package com.kaushlesh.view.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.FetchPlaceRequest
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.kaushlesh.AutoCompleteAdapterCity
import com.kaushlesh.Controller.AddFavPostController
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.R
import com.kaushlesh.adapter.BrandShowCaseAdapter
import com.kaushlesh.adapter.CategoryAdapter
import com.kaushlesh.adapter.CategoryAdapterNew
import com.kaushlesh.adapter.RecommendationAdapter
import com.kaushlesh.bean.BrandShowCaseBean
import com.kaushlesh.bean.CategoryBean
import com.kaushlesh.bean.RecommendationBean
import com.kaushlesh.bean.chat.ChatNewList
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.reusable.GPSTracker
import com.kaushlesh.reusable.LocationAddress
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.PaginationHelper
import com.kaushlesh.utils.PostDetail.CheckCategoryForPostDetails
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.utils.Utils.showToast
import com.kaushlesh.view.activity.ChatActivity
import com.kaushlesh.view.activity.Login.LoginMainActivity
import com.kaushlesh.view.activity.MainActivity
import com.kaushlesh.view.fragment.ShowcaseDetail.ExporeResidetialprojectFragment
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.item_row_fresh_recommendation.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList


class HomeFragment : Fragment(), View.OnClickListener, BrandShowCaseAdapter.ItemClickListener,CategoryAdapterNew.ItemClickListener,
    CategoryAdapter.ItemClickListener, RecommendationAdapter.ItemClickListener,ParseControllerListener,DistrictListDialogActivity.BottomSheetListener {

    internal var actlocation: AutoCompleteTextView? = null
    internal var svfind: androidx.appcompat.widget.SearchView? = null
    internal var tvnoticount: TextView? = null
    lateinit var getListingController: GetListingController
    lateinit var ll_location : LinearLayout
    lateinit var userId: String
    lateinit var userToken: String

    var categoryList1: ArrayList<CategoryBean.Category> = arrayListOf()
    var freshrecomlist: MutableList<RecommendationBean.FreshProductListBean> = arrayListOf()
    var chatCount: Int = 0
    internal lateinit var rvcategory: RecyclerView
   // internal lateinit var rvfreshproduct: RecyclerView
   lateinit var nsView : NestedScrollView
   internal lateinit var rcvRecommendationList : RecyclerView


    internal lateinit var placesClient: PlacesClient
    internal lateinit var autoCompleteAdapter: AutoCompleteAdapterCity
    internal var latitude: Double = 0.toDouble()
    internal var longitude: Double = 0.toDouble()
    lateinit var rcvRecommendationAdapter: RecommendationAdapter
    internal var AUTOCOMPLETE_REQUEST_CODE_SOURCE = 110
    var locationName : String ?= null
    var searchword : String = ""
    var loc_id : String = ""

    private var fusedLocationClient: FusedLocationProviderClient? = null

    private val showCaseList: List<BrandShowCaseBean>
        get() {
            val list = ArrayList<BrandShowCaseBean>()

            list.add(BrandShowCaseBean(R.drawable.img_one, "Residential Project"))
            list.add(BrandShowCaseBean(R.drawable.img_two, "Commercial Project"))
            list.add(BrandShowCaseBean(R.drawable.img_three, "Jewellery Showroom"))
            list.add(BrandShowCaseBean(R.drawable.img_five, "Car Showroom"))
            return list
        }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Focus Location Client for user location
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity!!)

        getListingController = GetListingController(activity!!, this)

        actlocation = view.findViewById(R.id.act_location)
        ll_location = view.findViewById(R.id.ll_loc)
        svfind = view.findViewById(R.id.sv_find)

        //requestMultiplePermissions()
        takepermission()

        val apiKey = getString(R.string.google_place_api_key)
        if (apiKey.isEmpty()) {
            //responseView.setText(getString(R.string.error));
            return
        }

        // Setup Places Client
        if (!Places.isInitialized()) {
            Places.initialize(context!!, apiKey)
        }

        placesClient = Places.createClient(context!!)

        initAutoCompleteTextView()

        val storeusedata = StoreUserData(context!!)

        userId = storeusedata.getString(Constants.USER_ID)
        userToken = storeusedata.getString(Constants.TOKEN)

        Utils.showLog(TAG, "==userid==" + userId + "== userToken==" + userToken)

        nsView = view.findViewById(R.id.nsView)
        nsView.isSmoothScrollingEnabled =  true
        rvcategory = view.findViewById(R.id.rv_category)
        rcvRecommendationList = view.findViewById(R.id.rv_recom)

      /*  val layoutManager = GridLayoutManager(context, 2)
        rcvRecommendationList.layoutManager = layoutManager
        rcvRecommendationList.itemAnimator = null
        rcvRecommendationAdapter = RecommendationAdapter(arrayListOf(), context!!)
        rcvRecommendationAdapter.setClicklistner(this)
        rcvRecommendationList.adapter = rcvRecommendationAdapter*/

        //call api to get fresh recommonded product list
        //getListingController.getFreshRecommondedList(0)
        //getListingController.getHomePostList(locationName.toString(), "", latitude, longitude)

        //call api to get category list
        getListingController.getCategoryList()

        bindRecyclerviewShowCase()

        //bindRecyclerviewCategories()

        //bindRecyclerviewFreshRecommendation()

        tv_viewall_showcase.setOnClickListener(this)
        tv_viewall_category.setOnClickListener(this)
        rl_chat.setOnClickListener(this)
        rl_noti.setOnClickListener(this)
        ll_location.setOnClickListener(this)
        act_location.setOnClickListener(this)

        //setupPagination()

        svfind!!.setOnQueryTextListener(object :
            androidx.appcompat.widget.SearchView.OnQueryTextListener,
            SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
             /*   if (newText.length == 0) {
                    getListingController.getHomePostList(
                        locationName.toString(),
                        loc_id,
                        latitude.toString(),
                        longitude.toString(),
                        ""
                    )

                }*/
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                getListingController.getHomePostList(
                    locationName.toString(),
                    loc_id,
                    latitude.toString(),
                    longitude.toString(),
                    query,0,70,
                        true
                )
                return false
            }

        })

        svfind!!.setOnCloseListener(object : androidx.appcompat.widget.SearchView.OnCloseListener {
            override fun onClose(): Boolean {
                getListingController.getHomePostList(
                    locationName.toString(),
                    loc_id,
                    latitude.toString(),
                    longitude.toString(),
                    "",0,70,
                        true
                )
                AppConstants.hideInputSoftKey(activity!!)
                return false
            }
        })
    }

    private fun takepermission() {

        Dexter.withActivity(activity)
            .withPermissions(
                Manifest.permission.INTERNET,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(multiplePermissionsReport: MultiplePermissionsReport) {
                    if (multiplePermissionsReport.areAllPermissionsGranted()) {
                        getUserLocation()
                        return
                    }
                    if (multiplePermissionsReport.isAnyPermissionPermanentlyDenied) {
                        showSettingsDialog()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    list: List<PermissionRequest>,
                    permissionToken: PermissionToken
                ) {
                }
            }).check()
    }

    @SuppressLint("MissingPermission")
    private fun getUserLocation() {
        try {
            fusedLocationClient?.lastLocation?.addOnSuccessListener { location: Location ->
                if(location.equals("null"))
                {
                    Utils.showToast(activity, "Failed to get user current location!.")
                }
                else {
                    if (location != null) {
                        latitude = location.latitude
                        longitude = location.longitude
                        Log.i(TAG, "--- LATITUDE ---$latitude---LONGITUDE---$longitude")
                        val locationAddress = LocationAddress()
                        locationAddress.getAddressFromLocation(latitude, longitude, context!!, GeoCodeHandler())

                    } else {
                        Utils.showToast(activity, "Failed to get user current location!.")
                    }
                }
            }
        }
        catch (e : NullPointerException)
        {
            e.printStackTrace()
        }

    }

    internal inner class GeoCodeHandler : Handler() {
        override fun handleMessage(message: Message) {
            val locationAddress: String
            locationAddress = when (message.what) {
                1 -> {
                    val bundle = message.data
                    bundle.getString("address").toString()
                }
                /*else -> null.toString()*/
                else -> "Gujarat"
            }
            Utils.showLog(TAG,"---- address---"+ locationAddress)
            actlocation?.setText(locationAddress)

            Log.i(TAG, "======= CURRENT LOCATION =========" + locationAddress + "Lattitude : " + latitude + "Longitude : " + longitude)
            getListingController.getHomePostList(
                locationAddress,
                "",
                latitude.toString(),
                longitude.toString(),
                searchword,0,70,
                    true
            )
        }
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle("Need Permissions")
        builder.setMessage("This app needs permission to use this application. You can grant them in app settings.")
        builder.setPositiveButton(
            "GOTO SETTINGS"
        ) { dialog, which ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton(
            "Cancel"
        ) { dialog, which -> dialog.cancel() }
        builder.show()
    }

    // navigating user to app settings
    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", context?.getPackageName(), null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }
    var paginationHelper : PaginationHelper ? = null
    private fun setupPagination() {
        paginationHelper = object : PaginationHelper(nsView)
        {
            override fun onLoadMore(start: Int) {
                Log.e(TAG, "Pagination onLoadMore invoked ${start}")
                getListingController.getFreshRecommondedList(start)
            }
        }.apply {
            setLoading(true)
        }
    }

    private fun initAutoCompleteTextView() {

        act_location.setThreshold(1)
        act_location.setOnItemClickListener(autocompleteClickListener)
        autoCompleteAdapter = AutoCompleteAdapterCity(context!!, placesClient)
        act_location.setAdapter(autoCompleteAdapter)

    }

    private val autocompleteClickListener =
            AdapterView.OnItemClickListener { adapterView, view, i, l ->
                try {
                    val item = autoCompleteAdapter.getItem(i)
                    var placeID: String = ""

                    if (item != null) {
                        placeID = item.placeId
                    }

                    //To specify which data types to return, pass an array of Place.Fields in your FetchPlaceRequest
                    //Use only those fields which are required.

                    val placeFields = Arrays.asList(
                        Place.Field.ID,
                        Place.Field.NAME,
                        Place.Field.ADDRESS,
                        Place.Field.LAT_LNG
                    )

                    var request: FetchPlaceRequest? = null
                    request = FetchPlaceRequest.builder(placeID, placeFields)
                            .build()

                    if (request != null) {
                        placesClient.fetchPlace(request).addOnSuccessListener { task ->
                            Log.i(
                                TAG,
                                task.place.name + "\n" + task.place.address + "\n" + task.place.latLng
                            )

                            //String address = task.getPlace().getName() + task.getPlace().getAddress();
                            val address = task.place.address
                            Log.i(TAG, "======== LOCATION=======" + address!!)

                            val latLng = task.place.latLng

                            if (latLng != null) {
                                latitude = latLng.latitude
                                longitude = latLng.longitude
                            }

                            Log.i(
                                TAG,
                                "======== LATITUDE =======$latitude========LONGITUDE=======$longitude"
                            )

                            locationName = address
                            if (!address.isEmpty()) {

                                /* val intent = Intent(getContext(), ActivitySavedPlacesAddressDetails::class.java)
                                intent.putExtra("isfor", "add")
                                intent.putExtra("address", address)
                                intent.putExtra("latitude", latitude.toString())
                                intent.putExtra("longitude", longitude.toString())
                                startActivity(intent)*/

                            }
                        }.addOnFailureListener { e ->
                            e.printStackTrace()
                            Log.i(TAG, e.message.toString())
                        }
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                } catch (e: IllegalArgumentException) {
                    e.printStackTrace()
                }
            }

    override fun onResume() {
        Utils.showLog(TAG, "==on resume==")
        val ref = FirebaseDatabase.getInstance().getReference("/List/").child(userId)
        //Utils.showLog(TAG,"==db ref==" + ref)
        chatCount = 0
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (child in dataSnapshot.getChildren()) {
                    //Utils.showLog(TAG,"==db ref 1==" + dataSnapshot.getChildren())
                    val msg: ChatNewList = child.getValue(ChatNewList::class.java)!!
                    Utils.showLog("count", msg.counter)
                    if (!msg.counter.isEmpty() && msg.counter.length > 0 && msg.counter.toInt() > 0) {
                        chatCount++;
                    }
                }
                Utils.showLog("count", "total===" + chatCount)
                try {
                    if (chatCount > 0) {
                        tv_chat_counter.visibility = View.VISIBLE
                        tv_chat_counter.setText(chatCount.toString())
                    } else {
                        tv_chat_counter.visibility = View.GONE
                    }
                } catch (e: java.lang.IllegalStateException) {
                    e.printStackTrace()
                }

            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })

        /*getListingController.getHomePostList(
                locationName.toString(),
                loc_id,
                latitude.toString(),
                longitude.toString(),
                searchword
        )*/

        super.onResume()
    }

    private fun bindRecyclerviewShowCase() {

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rv_showcase.layoutManager = layoutManager
        val adapter = BrandShowCaseAdapter(showCaseList, context!!)
        adapter.setClicklistner(this)
        rv_showcase.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun itemclick(bean: BrandShowCaseBean) {

        //open details screens
        val bundle = Bundle()
        bundle.putString("name", bean.brandname)
        (activity as MainActivity).changeFragment(
            ExporeResidetialprojectFragment.newInstance(),
            ExporeResidetialprojectFragment.TAG,
            bundle,
            true
        )

    }

    override fun itemclickCategory(bean: BrandShowCaseBean) {
        val bundle = Bundle()
        bundle.putString("name", bean.brandname)
        bundle.putString("isfrom", "home")
        (activity as MainActivity).changeFragment(
            SubCategoryFragment.newInstance(),
            SubCategoryFragment.TAG,
            bundle,
            true
        )

    }

    override fun itemclickCategory(bean: CategoryBean.Category) {
        val bundle = Bundle()
        bundle.putString("id", bean.categoryId.toString())
        bundle.putString("name", bean.categoryName)
        bundle.putString("isfrom", "home")
        (activity as MainActivity).changeFragment(
            SubCategoryFragment.newInstance(),
            SubCategoryFragment.TAG,
            bundle,
            true
        )
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.tv_viewall_showcase -> {

                val bundle = Bundle()
                bundle.putString("from", "home")
                (activity as MainActivity).changeFragment(
                    AdShowcaseFragment.newInstance(),
                    AdShowcaseFragment.TAG,
                    bundle,
                    true
                )
            }

            R.id.tv_viewall_category -> {
                val bundle = Bundle()
                bundle.putString("from", "home")
                (activity as MainActivity).changeFragment(
                    AllCategoryFragment.newInstance(),
                    AllCategoryFragment.TAG,
                    bundle,
                    true
                )

            }

            R.id.rl_chat -> {

                if (AppConstants.checkUserIsLoggedin(activity!!)) {
                    val intent1 = Intent(context, ChatActivity::class.java)
                    startActivity(intent1)
                } else {
                    val intent = Intent(activity, LoginMainActivity::class.java)
                    AppConstants.alertLogin(
                        activity!!,
                        getString(R.string.txt_login_to_see_chat),
                        intent
                    )
                }

            }

            R.id.rl_noti -> {
                if (AppConstants.checkUserIsLoggedin(activity!!)) {
                    //val intent1 = Intent(context, ChatActivity::class.java)
                    //startActivity(intent1)
                    Utils.showToast(activity, "clicked")
                } else {
                    val intent = Intent(activity, LoginMainActivity::class.java)
                    AppConstants.alertLogin(
                        activity!!,
                        getString(R.string.txt_login_to_see_notification),
                        intent
                    )
                }
            }

            R.id.ll_loc -> {
                DistrictListDialogActivity.display(childFragmentManager, this)
                //(activity as MainActivity).changeFragment(DistrictListDialogActivity.newInstance(this), DistrictListDialogActivity.TAG, null, true)
            }

            R.id.act_location -> {
                ll_location.performClick()
            }
        }
    }

    companion object {

        val TAG = "HomeFragment"

        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (method.equals("Get Category")) {

            if (da.length() > 0) {

                Log.e("get profile Result-", "$da==")
                try {
                    //et_phone.text=da.getString("ConatctNo")

                    categoryList1.clear()

                    val dataModel = CategoryBean()
                    dataModel.CategoryBean(da)

                    categoryList1 = dataModel.categoryList
                    Utils.showLog(TAG, "==product list size==" + categoryList1.size)

                    bindRecyclerviewCategory(categoryList1)

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }

        if (method.equals("freshProduct")) {
            //Utils.dismissProgress()
            if (da.length() > 0) {

                Log.e("fresh Product Result-", "$da==")
                try {

                    freshrecomlist.clear()

                    val dataModel = RecommendationBean()
                    dataModel.RecommendationBean(da)

                    freshrecomlist = dataModel.freshproductlist
                    Utils.showLog(TAG, "==fresh recom product list size==" + freshrecomlist.size)

                    //freshrecomlist.sortByDescending { it.ispremium == 1 }

                    //margeList(freshrecomlist as java.util.ArrayList<RecommendationBean.FreshProductListBean>)
                    try{
                        rl_no_post.visibility = View.GONE
                        rcvRecommendationList.visibility = View.VISIBLE

                        bindHomePost(freshrecomlist as java.util.ArrayList<RecommendationBean.FreshProductListBean>)

                        AppConstants.hideInputSoftKey(activity!!)
                    }
                    catch (e: NullPointerException)
                    {
                        e.printStackTrace()
                    }


                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }

        if (method.equals("adFavPost")) {
            Utils.showToast(activity, message)
        }
    }

    private fun bindHomePost(freshrecomlist: java.util.ArrayList<RecommendationBean.FreshProductListBean>) {
        val layoutManager = GridLayoutManager(context, 2)
        rcvRecommendationList.layoutManager = layoutManager
        rcvRecommendationList.itemAnimator = null
        rcvRecommendationAdapter = RecommendationAdapter(freshrecomlist, context!!)
        rcvRecommendationAdapter.setClicklistner(this)
        rcvRecommendationList.adapter = rcvRecommendationAdapter
        rcvRecommendationAdapter.notifyDataSetChanged()

        Handler().postDelayed({
            Utils.dismissProgress()
        }, 100)

    }

    private fun margeList(freshrecomlist: ArrayList<RecommendationBean.FreshProductListBean>) {

        try {

            bindRecyclerviewFreshRecom(freshrecomlist)
        }
        catch (e: java.lang.Exception)
        {
            e.printStackTrace()
        }


    }

    override fun onFail(msg: String, method: String) {

        Utils.dismissProgress()
        //Utils.showToast(activity, msg)

        if (method.equals("freshProduct"))
        {
            Utils.showToast(activity, msg)
            rcvRecommendationList.visibility = View.GONE
            rl_no_post.visibility = View.VISIBLE
        }
    }

    private fun bindRecyclerviewFreshRecom(freshrecomlist: ArrayList<RecommendationBean.FreshProductListBean>) {
        val old: Int = rcvRecommendationAdapter.getItemCount()
        rcvRecommendationAdapter.list.addAll(freshrecomlist)
        rcvRecommendationAdapter.notifyItemRangeChanged(old, rcvRecommendationAdapter.list.size)
    /*    rcvRecommendationList.post {
            paginationHelper?.setPreviousLoadedItemsCount(rcvRecommendationAdapter.itemCount)
            paginationHelper?.setLoading(false)
        }*/
    }


    private fun bindRecyclerviewCategory(categoryList1: ArrayList<CategoryBean.Category>) {
        try {
            val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            rvcategory.layoutManager = layoutManager
            val adapter = CategoryAdapterNew(categoryList1, context!!, 1)
            adapter.setClicklistner(this)
            rvcategory.adapter = adapter
            adapter.notifyDataSetChanged()
        }catch (e: IllegalStateException)
        {
            e.printStackTrace()
        }
        catch (e: NullPointerException)
        {
            e.printStackTrace()
        }
    }

    override fun itemclickRecommendation(bean: RecommendationBean.FreshProductListBean) {
        val storeusedata = StoreUserData(context!!)
        storeusedata.setString(Constants.OPEN_POST_DETAIL, "product_list")
        storeusedata.setString(Constants.CATEGORY_ID, bean.category_id.toString())
        storeusedata.setString(Constants.SUBCATEGORYID, bean.sub_category_id.toString())
        CheckCategoryForPostDetails.OpenScreen(
            context!!,
            bean.category_id!!.toInt(),
            bean.sub_category_id!!.toInt(),
            bean.post_id!!.toInt()
        )
    }

    override fun itemFavUnfavourite(dataModel: RecommendationBean.FreshProductListBean, i: Int) {
        if(AppConstants.checkUserIsLoggedin(activity!!))
        {
            val c = AddFavPostController(this, activity!!, dataModel.post_id.toString())
            c.onClick(iv_like)
        }
        else{
            val intent = Intent(context, LoginMainActivity::class.java)
            AppConstants.alertLogin(activity!!, getString(R.string.txt_login_ad_fav), intent)
        }
    }

    private fun requestMultiplePermissions() {
        Dexter.withActivity(getActivity())
                .withPermissions(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                )
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            Log.i("TAG", "All permissions are granted")
                            val gpsTracker = GPSTracker(context!!)
                            if (gpsTracker.getIsGPSTrackingEnabled()) {
                                val stringLatitude =
                                    java.lang.String.valueOf(gpsTracker.getLatitude())
                                val stringLongitude =
                                    java.lang.String.valueOf(gpsTracker.getLongitude())
                                val addressLine = gpsTracker.getAddressLine(context).toString()
                                actlocation?.setText(addressLine)

                                Log.i(
                                    TAG,
                                    "======= CURRENT LOCATION =========" + addressLine + "Lattitude : " + stringLatitude + "Longitude : " + stringLongitude
                                )
                                getListingController.getHomePostList(
                                    addressLine,
                                    "",
                                    stringLatitude.toString(),
                                    stringLongitude.toString(),
                                    searchword,0,70,
                                        true
                                )

                            } else {
                                // can't get location
                                // GPS or Network is not enabled
                                // Ask user to enable GPS/network in settings
                                gpsTracker.showSettingsAlert()
                            }
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied) {
                            // show alert dialog navigating to Settings
                            //openSettingsDialog();
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permissions: List<PermissionRequest>,
                        token: PermissionToken
                    ) {
                        token.continuePermissionRequest()
                    }
                }).withErrorListener { Log.i("TAG", "Some Error!") }
                .onSameThread()
                .check()
    }

    override fun selectedLocation(
        district: String,
        locId: String,
        name: String,
        lat: String,
        long: String
    ) {
        Utils.showLog(TAG, "---district---" + district + "--- id ---" + locId)
        Utils.showLog(TAG, "---name---" + name + "--- lat ---" + lat + "--- long ---" + long)
        loc_id = locId
        if(name != null && name.toString().length > 0)
        {
            actlocation!!.setText(name)
            locationName = name
        }
        else {
            actlocation!!.setText(district)
            locationName = district
        }

        getListingController.getHomePostList(locationName.toString(), loc_id, lat, long, searchword,0,70,true)
    }

    override fun currentLocation() {
        takepermission()
    }

}

