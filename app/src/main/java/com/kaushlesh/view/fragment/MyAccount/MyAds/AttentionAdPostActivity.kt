package com.kaushlesh.view.fragment.MyAccount.MyAds

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.text.Html
import android.util.Log
import com.google.gson.GsonBuilder
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.Controller.PaymentController
import com.kaushlesh.Controller.PostLiveAgainController
import com.kaushlesh.Controller.PurchasePackageController
import com.kaushlesh.R
import com.kaushlesh.bean.AdPost.AdPostResponseBean
import com.kaushlesh.bean.ExtraAdPackageBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.AdPost.CongratulationAdpostActivity
import com.kaushlesh.view.fragment.MyAccount.PurchasePackage.AdPostPackagesActivity
import com.razorpay.Checkout
import com.razorpay.PaymentResultListener
import kotlinx.android.synthetic.main.activity_attention_ad_post.*
import org.json.JSONObject

class AttentionAdPostActivity : AppCompatActivity() ,ParseControllerListener,PaymentResultListener {

    lateinit var checkout: Checkout
    internal lateinit var paymentController: PaymentController
    internal var price : String?= ""
    internal var packageid : String?= ""
    internal var postid : String?= ""
    internal var catid : String?= ""
    lateinit var getListingController: GetListingController
    internal var payamount : String?= ""
    lateinit var storeUserData: StoreUserData


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_attention_ad_post)

        storeUserData = StoreUserData(this)
        getListingController = GetListingController(this, this)

        if (intent != null) {
            postid = intent.getStringExtra("postId")
            catid = intent.getStringExtra("catId")
            Utils.showLog(TAG,"==poistid==" + postid + "== cat id===" + catid)
        }


        getListingController.getExtraAdPackage(catid)

        Checkout.preload(applicationContext)

        checkout = Checkout()
        checkout.setKeyID(getString(R.string.key_razorpay))

        btn_back_att.setOnClickListener {
            onBackPressed()
        }
        tv_pkg.setOnClickListener {

            if(payamount != null && payamount.toString().length > 0)
            {
                val total = payamount!!.toInt() * 100

                if(Utils.isConnected()){
                    startpay(total.toString())
                }else{
                    Utils.showAlertConnection(this)
                }

            }
        }

        tvsellall.setOnClickListener {
            val intent = Intent(applicationContext, AdPostPackagesActivity::class.java)
            intent.putExtra("isPremium", "no")
            intent.putExtra("address", "")
            intent.putExtra("catId", catid)
            intent.putExtra("lat", "")
            intent.putExtra("long","")
            intent.putExtra("price", "")
            intent.putExtra("repost", "yes")
            intent.putExtra("postaddress","")
            startActivity(intent)
        }
    }

    private fun startpay(price: String) {

        val activity: Activity = this
        val co = Checkout()
        co.setKeyID(activity.getString(R.string.key_razorpay))

        try {
            val options = JSONObject()
            if(storeUserData.getString(Constants.NAME).equals("")){
                options.put("name", "Gujarat Living User")
            }else{
                options.put("name", ""+storeUserData.getString(Constants.NAME))
            }
            options.put("description", "Amount")
            options.put("theme.color", "#000000");
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png")
            options.put("currency", "INR")
            options.put("amount", price)

            val prefill = JSONObject()
            prefill.put("email", ""+storeUserData.getString(Constants.EMAIL))
            prefill.put("contact", ""+storeUserData.getString(Constants.CONTACT_NO))
            //prefill.put("contact", storeUserData.getString(Constants.PHONE_NUMBER))

            options.put("prefill", prefill)
            co.open(activity, options)
        } catch (e: Exception) {
            Utils.showLog(TAG,"============="+e.message)
            AppConstants.printToast(this, e.message.toString())
            e.printStackTrace()
        }
    }

    companion object {

        private val TAG = "AttentionAdPostActivity"
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        Utils.showLog(TAG,"==method call==")
        if(method.equals("GetExtraAd"))
        {
            val dataModel = ExtraAdPackageBean()
            dataModel.ExtraAdPackageBean(da)

            Log.d("GetExtraAd","Attention : " + GsonBuilder().setPrettyPrinting().create().toJson(dataModel))

            packageid = dataModel.packageId.toString()
            Utils.showLog(TAG,"===pkg id==" + packageid)
            payamount = dataModel.price.toString()

            Utils.showLog(TAG,"===total price =" + payamount)

            tv_pkg_name.setText(dataModel.name)

            val ss = "<b>" + dataModel.freeAdRenewAfter + "</b>"
            tv_renew_day.text = Html.fromHtml(resources.getString(R.string.txt_due_to_free_ad) + " " + ss + " days.")
            tv_price.text = "₹ " + dataModel.price.toString()
            tv_pkg.text = resources.getString(R.string.txt_pay_rupee)+ " "+ dataModel.price.toString()
        }

        if (method.equals("purchasePkg")) {

            val userpostpackageid = da.getString("userpostpurchasepackage_id")
            storeUserData.setString(Constants.USER_PKG_ID,userpostpackageid.toString())

            Utils.showToast(this,message)
            PostLiveAgainController(this,postid.toString(),packageid.toString(),this)
        }
        if(method.equals("rePost"))
        {
            Utils.showToast(this,"Package Applied Successfully")
            val dataModel = AdPostResponseBean()
            dataModel.AdPostResponseBean(da)

            Utils.showLog(TAG, "===data===" + dataModel)
            val data = dataModel.pkglist.get(0)









            storeUserData.setString(Constants.USER_PKG_ID,"")
            storeUserData.setString(Constants.LOC_ID_PKG,"")
            val intent = Intent(this@AttentionAdPostActivity, CongratulationAdpostActivity::class.java)
            intent.putExtra("data", data)
            intent.putExtra("type", "rpay")
            startActivity(intent)
            finishAffinity()



            /*Handler().postDelayed({
                if (!isFinishing) {

                    storeUserData.setString(Constants.USER_PKG_ID,"")
                    storeUserData.setString(Constants.LOC_ID_PKG,"")
                    val intent = Intent(this@AttentionAdPostActivity, CongratulationAdpostActivity::class.java)
                    intent.putExtra("data", data)
                    intent.putExtra("type", "rpay")
                    startActivity(intent)
                    finishAffinity()
                }
            }, 1000)*/
        }
    }

    override fun onFail(msg: String, method: String) {
        Utils.showLog(TAG,msg)
    }


    override fun onPaymentError(errocode: Int, response: String?) {
        try {
            //AppConstants.printToast(applicationContext, "Payment failed: " + errocode.toString() + " " + response)
            AppConstants.printLog("test", response + "Exception in onPaymentSuccess" + errocode)
            Utils.showAlert(this,getString(R.string.txt_payment_canceled_user))

        } catch (e: java.lang.Exception) {
            AppConstants.printLog("test", "Exception in onPaymentError" + e)
        }

        //listener.onFail(errocode.toString(),response.toString())
    }

    override fun onPaymentSuccess(razorpayPaymentId: String?) {
        try {
            Utils.showLog(TAG, "==purchase pkg id==" + packageid)

            if (packageid != null && packageid.toString().length > 0) {
                val c = PurchasePackageController(this, packageid!!,razorpayPaymentId.toString(),"", this)
                c.purchasePackage()
            }

        } catch (e: java.lang.Exception) {
            AppConstants.printLog("test", "Exception in onPaymentSuccess" + e)

        }
    }
}
