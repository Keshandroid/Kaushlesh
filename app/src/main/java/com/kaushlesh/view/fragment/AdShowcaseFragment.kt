package com.kaushlesh.view.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.kaushlesh.R
import com.kaushlesh.adapter.AdShowCaseAdapter
import com.kaushlesh.bean.BrandShowCaseBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.view.activity.AdShowcase.Cars.CarshowroomFillGeneralProjectDetail
import com.kaushlesh.view.activity.AdShowcase.Commercial.ComercialFillGeneralProjectDetail
import com.kaushlesh.view.activity.AdShowcase.Jwellery.JewelleryGeneralProjectDetail
import com.kaushlesh.view.activity.AdShowcase.Residential.ResidentialFillGeneralProjectDetailActivity
import com.kaushlesh.view.activity.MainActivity
import com.kaushlesh.view.fragment.ShowcaseDetail.ExporeResidetialprojectFragment
import java.util.ArrayList

class AdShowcaseFragment : Fragment(),AdShowCaseAdapter.ItemClickListener {


    internal lateinit var rvshowcase: RecyclerView
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal var from: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ad_showcase, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null) {
            from = arguments!!.getString("from")
            AppConstants.printLog(TAG, "==from==" + from!!)
        }

        toolbar = view.findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)


        tvtoolbartitle.text = resources.getString(R.string.txt_ad_showcase_title)
        btnback.visibility = View.GONE

        rvshowcase = view.findViewById(R.id.rv_adshowcase)

        bindRecyclerviewAdShowCase()

        btnback.setOnClickListener {
            if (activity != null) {
                activity!!.onBackPressed()
            }
        }
    }

    companion object {

        val TAG = "AdShowcaseFragment"

        fun newInstance(): AdShowcaseFragment {
            return AdShowcaseFragment()
        }
    }

    private fun bindRecyclerviewAdShowCase() {

        val layoutManager = GridLayoutManager(context, 2)
        rvshowcase.layoutManager = layoutManager
        val adapter = AdShowCaseAdapter(categoryList, context!!)
        adapter.setClicklistner(this)
        rvshowcase.adapter = adapter
        adapter.notifyDataSetChanged()
    }


    val categoryList: List<BrandShowCaseBean>
        get() {
            val list = ArrayList<BrandShowCaseBean>()

            list.add(BrandShowCaseBean(R.drawable.ic_property, "Residential Project"))
            list.add(BrandShowCaseBean(R.drawable.ic_shop_city, "Commercial Project"))
            list.add(BrandShowCaseBean(R.drawable.ic_jwellery, "Jewellery Showroom"))
            //list.add(BrandShowCaseBean(R.drawable.ic_bikes, "Bikes Showroom"))
            list.add(BrandShowCaseBean(R.drawable.ic_cras, "Car Showroom"))
            //list.add(BrandShowCaseBean(R.drawable.ic_industries, "Industry Showcase"))
            //list.add(BrandShowCaseBean(R.drawable.ic_electronics, "Electronics & Appliances Showroom"))
            //list.add(BrandShowCaseBean(R.drawable.ic_furniture, "Furniture Showroom"))
            //list.add(BrandShowCaseBean(R.drawable.ic_art, "Hotel & Resorts Showcase"))
            //list.add(BrandShowCaseBean(R.drawable.ic_art, "Restora Showcase"))
            //list.add(BrandShowCaseBean(R.drawable.ic_grain, "View All Categories"))

            return list
        }

    override fun itemclickCategory(bean: BrandShowCaseBean) {
        if(from.equals("home"))
        {
            val bundle = Bundle()
            bundle.putString("name", bean.brandname)
            //  bundle.putString("catName",getString(R.string.txt_purchase_package))
            (activity as MainActivity).changeFragment(ExporeResidetialprojectFragment.newInstance(), ExporeResidetialprojectFragment.TAG, bundle, true)
        }

        else{
            checkAndSet(bean.brandname)
        }
    }

    private fun checkAndSet(brandname: String) {
        AppConstants.printLog(TAG, "==name==" + brandname)

        if(brandname.equals("Residential Project"))
        {
            val intent = Intent(context, ResidentialFillGeneralProjectDetailActivity::class.java)
            startActivity(intent)
        }

        if(brandname.equals("Commercial Project"))
        {
            val intent = Intent(context, ComercialFillGeneralProjectDetail::class.java)
            startActivity(intent)
        }

        if(brandname.equals("Jewellery Showroom"))
        {
            val intent = Intent(context, JewelleryGeneralProjectDetail::class.java)
            startActivity(intent)
        }

        if(brandname.equals("Car Showroom"))
        {
            val intent = Intent(context, CarshowroomFillGeneralProjectDetail::class.java)
            startActivity(intent)
        }
    }
}

