package com.kaushlesh.view.fragment.MyAccount.PurchasePackage

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.Controller.DeleteExpiredOrderController
import com.kaushlesh.Controller.DeleteOrderController
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.Controller.MyPostShowcaseDeleteController
import com.kaushlesh.R
import com.kaushlesh.adapter.MyOrdersAdapter
import com.kaushlesh.bean.MyOrdersBean
import com.kaushlesh.bean.MyPackageOrderBean
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.PostDetail.GetDetailsFromValue
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.Login.LoginMainActivity
import kotlinx.android.synthetic.main.activity_my_orders.*
import kotlinx.android.synthetic.main.fragment_followers.*
import org.json.JSONObject
import java.util.ArrayList

class MyOrdersActivity : AppCompatActivity(), MyOrdersAdapter.ItemClickListener ,ParseControllerListener {

    val listOrder = ArrayList<MyPackageOrderBean.MyPackageOrderList>()
    private val TAG = "MyOrdersActivity"
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var btndelete: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal lateinit var rvorders: RecyclerView
    private lateinit var getListingController: GetListingController
    internal lateinit var dialog: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_orders)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.greylight)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.black)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        toolbar = findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        btndelete = toolbar.findViewById(R.id.btn_delete)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        tvtoolbartitle.text = resources.getString(R.string.txt_my_orders)

        rvorders = findViewById(R.id.rv_my_orders)

        //bindRecyclerview()

        getOrderList()

        btnback.setOnClickListener { onBackPressed() }
        btn_start_purchase.setOnClickListener {
            val intent = Intent(this, AdPostPackagesActivity::class.java)
            startActivity(intent)
            finish()
        }
        btndelete.setOnClickListener {
            openLogoutDialog()
        }
    }

    private fun openLogoutDialog() {

        dialog = Dialog(this)
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_logout)
        // Set dialog title
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        //dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_white);
        // set values for custom dialog components - text, image and button

        val tvtitle = dialog.findViewById(R.id.tv_title) as TextView
        tvtitle.setText(resources.getText(R.string.txt_warning_delete_multi_order))
        val btnyes = dialog.findViewById(R.id.btn_yes) as TextView
        val btnno = dialog.findViewById(R.id.btn_no) as TextView

        val expiredOrder: ArrayList<String> = ArrayList<String>()
        if (listOrder.size > 0) {
            for (i in listOrder.indices) {
                if(listOrder.get(i).isexpired == 1) {
                    val expireed = listOrder.get(i).order_id.toString()
                    expiredOrder.add(expireed)
                }
            }
        }

        val tofrooms = TextUtils.join(",", expiredOrder)
        Utils.showLog("hotel", "==expiredOrder==" + tofrooms)

        btnyes.setOnClickListener(View.OnClickListener {

            //call api
            val deleteController = DeleteExpiredOrderController(this, this)
            deleteController.onClick(btnyes)
            dialog.dismiss()
        })

        btnno.setOnClickListener(View.OnClickListener { dialog.dismiss() })

        dialog.show()
    }


    private fun getOrderList() {
        Utils.showProgress(this)
        getListingController = GetListingController(this, this)
        getListingController.getPackageOrder()
    }

    private fun bindRecyclerview() {
        val recyclerLayoutManager = LinearLayoutManager(applicationContext)
        rvorders.layoutManager = recyclerLayoutManager
        rvorders.itemAnimator = DefaultItemAnimator()
        val adapter = MyOrdersAdapter(listOrder, applicationContext)
        adapter.setClicklistner(this)
        rvorders.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun itemclick(bean: MyPackageOrderBean.MyPackageOrderList) {

    }

    override fun itemclickDelete(position: Int) {
        val myPostShowcaseDeleteController = MyPostShowcaseDeleteController(this, this)
        myPostShowcaseDeleteController.addOrder("order",listOrder[position].purchased_order_id!!,listOrder[position].package_id!!)
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (method.equals("GetOrder")) {
            listOrder.clear()
            val dataModel = MyPackageOrderBean()
            dataModel.MyPackageOrderBean(da)
            listOrder.addAll(dataModel.myPackageOrderList)
            Utils.dismissProgress()

            checkResponse()

        }

        if (method.equals("deleteOrder") || method.equals("deleteExpiredOrder")) {
            Utils.dismissProgress()
            Utils.showToast(this, message)
            getOrderList()
        }
    }
    override fun onFail(msg: String, method: String) {
        Utils.dismissProgress()
        rl_no_order.visibility = View.VISIBLE
        btndelete.visibility = View.GONE
        rvorders.visibility = View.GONE
    }

    private fun checkResponse() {
        try {
            if (listOrder.size > 0) {
                rl_no_order.visibility = View.GONE
                rvorders.visibility = View.VISIBLE
                bindRecyclerview()
            } else {
                rl_no_order.visibility = View.VISIBLE
                rvorders.visibility = View.GONE
            }
        }
        catch (e : NullPointerException)
        {
            e.printStackTrace()
        }
    }
}
