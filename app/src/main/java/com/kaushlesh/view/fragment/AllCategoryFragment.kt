package com.kaushlesh.view.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.Controller.GetListingController

import com.kaushlesh.R
import com.kaushlesh.view.activity.MainActivity
import com.kaushlesh.adapter.CategoryAdapter
import com.kaushlesh.adapter.CategoryAdapterNew
import com.kaushlesh.bean.BrandShowCaseBean
import com.kaushlesh.bean.CategoryBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Utils
import org.json.JSONException
import org.json.JSONObject
import java.util.ArrayList

class AllCategoryFragment : Fragment(), CategoryAdapter.ItemClickListener ,CategoryAdapterNew.ItemClickListener,ParseControllerListener{
    internal lateinit var rvcategory: RecyclerView
    internal lateinit var toolbar: Toolbar
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal var from: String? = null

    lateinit var getListingController: GetListingController
    var categoryList1: ArrayList<CategoryBean.Category> = arrayListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_all_category, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null) {
            from = arguments!!.getString("from")
            AppConstants.printLog(TAG, "==from==" + from!!)
        }


        toolbar = view.findViewById(R.id.toolbar)
        btnback = toolbar.findViewById(R.id.btn_back)
        tvtoolbartitle = toolbar.findViewById(R.id.title)

        if(from.equals("adpost"))
        {
            btnback.visibility = View.GONE
            tvtoolbartitle.text = resources.getString(R.string.txt_what_you_want_to_sell)
        }

        else{
            tvtoolbartitle.text = resources.getString(R.string.txt_explore_category)
        }
        rvcategory = view.findViewById(R.id.rv_category)

        getListingController = GetListingController(activity!!, this)

        getListingController.getCategoryList()

        //bindRecyclerviewCategories()

        btnback.setOnClickListener {
            if (activity != null) {
                activity!!.onBackPressed()
            }
        }
    }


    override fun itemclickCategory(bean: BrandShowCaseBean) {

        val bundle = Bundle()
        bundle.putString("name", bean.brandname)
        bundle.putString("isfrom", from)
        (activity as MainActivity).changeFragment(SubCategoryFragment.newInstance(),SubCategoryFragment.TAG, bundle, true)
    }

    override fun itemclickCategory(bean: CategoryBean.Category) {
        val bundle = Bundle()
        bundle.putString("id", bean.categoryId.toString())
        bundle.putString("name", bean.categoryName)
        bundle.putString("isfrom", from)
        (activity as MainActivity).changeFragment(SubCategoryFragment.newInstance(),SubCategoryFragment.TAG, bundle, true)
    }

    companion object {

        val TAG = "AllCategoryFragment"

        fun newInstance(): AllCategoryFragment {
            return AllCategoryFragment()
        }
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (method.equals("Get Category")) {
            if (da.length() > 0) {

                Log.e("get profile Result-", "$da==")
                try {
                    //et_phone.text=da.getString("ConatctNo")

                    categoryList1.clear()
                    val dataModel = CategoryBean()
                    dataModel.CategoryBean(da)

                    categoryList1 = dataModel.categoryList
                    Utils.showLog(TAG,"==product list size==" + categoryList1.size)

                    bindRecyclerviewCategory(categoryList1)

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }

    }

    private fun bindRecyclerviewCategory(categoryList1: ArrayList<CategoryBean.Category>) {
        try {
            val layoutManager = GridLayoutManager(context, 3)
            rvcategory.layoutManager = layoutManager
            val adapter = CategoryAdapterNew(categoryList1, context!!, 3)
            adapter.setClicklistner(this)
            rvcategory.adapter = adapter
            adapter.notifyDataSetChanged()
        }catch (e : NullPointerException)
        {
            e.printStackTrace()
        }
    }

    override fun onFail(msg: String, method: String) {

        Utils.showToast(activity,msg)
    }
}

