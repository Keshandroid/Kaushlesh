package com.kaushlesh.view.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.RelativeLayout
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.FetchPlaceRequest
import com.google.android.libraries.places.api.net.PlacesClient
import com.kaushlesh.AutoCompleteAdapter
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.R
import com.kaushlesh.bean.LatLongBean
import com.kaushlesh.bean.LocationListBeans
import com.kaushlesh.bean.LocationSuggestionBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.AdPost.SetLocationActivity
import kotlinx.android.synthetic.main.activity_set_location.*
import kotlinx.android.synthetic.main.layout_add_area.*
import kotlinx.android.synthetic.main.layout_add_area.act_location
import kotlinx.android.synthetic.main.layout_add_area.spinner_dis_type
import org.json.JSONObject
import java.util.*

class AddAreaDialogActivity: DialogFragment(),View.OnClickListener, ParseControllerListener,AdapterView.OnItemClickListener {

    lateinit var rlclose : RelativeLayout
    lateinit var btnsave : Button
    var bottomSheetListener: BottomSheetListener? = null
    internal var latitude: Double = 0.toDouble()
    internal var longitude: Double = 0.toDouble()
    internal var dislatitude: Double = 0.toDouble()
    internal var dislongitude: Double = 0.toDouble()
    internal var AUTOCOMPLETE_REQUEST_CODE_SOURCE = 110
    internal lateinit var placesClient: PlacesClient
    internal lateinit var autoCompleteAdapter: AutoCompleteAdapter
    var district : String = ""
    var districtId : String = ""
    internal var typelist: MutableList<String> = java.util.ArrayList()
    internal var typeidlist: MutableList<String> = java.util.ArrayList()
    internal var locationlist: ArrayList<LocationListBeans.LocationList> = ArrayList()
    internal lateinit var type_spinner_type: ArrayAdapter<String>
    internal var sugplaceList: MutableList<LocationSuggestionBean.Lacation> = java.util.ArrayList()
    internal var placeList: MutableList<String> = java.util.ArrayList()

    lateinit var getListingController: GetListingController
    var locationName: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppTheme_FullScreenDialog)
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window!!.setLayout(width, height)
            //dialog.window!!.setWindowAnimations(R.style.AppTheme_Slide)
        }
    }


        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)
        val view: View = inflater.inflate(R.layout.layout_add_area, container, false)

        rlclose  = view.findViewById(R.id.rl_close)
        btnsave = view.findViewById(R.id.btn_save)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getListingController = GetListingController(activity!!, this)

        if (arguments != null) {
            district = arguments!!.getString("district").toString()
            districtId = arguments!!.getString("id").toString()
        }
        Utils.showLog(TAG, "---district---" + district + "--- id ---" + districtId)

        tv_dis.setText(district)

        getListingController.getLatLongOfSelectedStrict(district + "," + resources.getString(R.string.txt_gujarat))

        rlclose.setOnClickListener(this)
        btnsave.setOnClickListener(this)
        //rl_dis_type.setOnClickListener(this)

        getListingController.getLocationList()

        val apiKey = getString(R.string.google_place_api_key)
        if (apiKey.isEmpty()) {
            //responseView.setText(getString(R.string.error));
            return
        }

        // Setup Places Client
        if (!Places.isInitialized()) {
            Places.initialize(context!!, apiKey)
        }

        placesClient = Places.createClient(context!!)

        //initAutoCompleteTextView()

        spinner_dis_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                    adapterView: AdapterView<*>,
                    view: View,
                    position: Int,
                    l: Long
            ) {
                AppConstants.printLog(TAG, "onItemSelected: ===" + adapterView.selectedItem)
                AppConstants.printLog(TAG, "onItemSelected: ===" + typeidlist.get(position))

                districtId = typeidlist.get(position)
                district = adapterView.selectedItem.toString()

                Utils.showLog(TAG, "====stored location id====" + districtId.toString())

                getListingController.getLatLongOfSelectedStrict(district + "," + resources.getString(R.string.txt_gujarat))

                if (act_location.text.toString().length > 0) {
                    act_location.setText("")
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        }

        act_location.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                getListingController.callforGetLocationList(s.toString(), dislatitude, dislongitude, district.toString())
            }

            override fun afterTextChanged(s: Editable?) {}
        })


        // Set a focus change listener for auto complete text view
        act_location.onFocusChangeListener = View.OnFocusChangeListener { view, b ->
            if (b) {
                // Display the suggestion dropdown on focus
                act_location.showDropDown()
            }
        }

    }

    companion object {
        const val TAG = "AddAreaDialogActivity"
        fun display(fragmentManager: FragmentManager, listener: BottomSheetListener, args: Bundle): AddAreaDialogActivity {
            val exampleDialog = AddAreaDialogActivity()
            exampleDialog.show(fragmentManager, TAG)
            exampleDialog.bottomSheetListener = listener
            if (args != null) exampleDialog.arguments = args
            return exampleDialog

        }

       /* fun newInstance(listener: BottomSheetListener): DistrictListDialogActivity {
            val fragment = DistrictListDialogActivity()
            fragment.bottomSheetListener = listener
            return fragment
        }*/
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.rl_close -> {
                dismiss()
            }

            R.id.btn_save -> {

                if (act_location.text.toString().equals(locationName)) {
                    bottomSheetListener!!.selectedLocation(district, districtId, act_location.text.toString(), latitude.toString(), longitude.toString())
                    dismiss()
                } else {
                    Utils.showLocationAlert(activity!!, resources.getString(R.string.txt_check_location_refer))
                }

            }

            R.id.rl_dis_type -> {

            }

        }
    }

    interface BottomSheetListener {
        fun selectedLocation(district: String, locId: String, name: String, lat: String, long: String)
    }

    private fun initAutoCompleteTextView() {

        //val type = Typeface.createFromAsset(getContext()!!.getAssets(), "fonts/" + "SFRegular.otf")
        //atvaddress.setTypeface(type)
        act_location.setThreshold(1)
        act_location.setOnItemClickListener(autocompleteClickListener)
        autoCompleteAdapter = AutoCompleteAdapter(context!!, placesClient)
        act_location.setAdapter(autoCompleteAdapter)

    }

    private val autocompleteClickListener =
        AdapterView.OnItemClickListener { adapterView, view, i, l ->
            try {
                val item = autoCompleteAdapter.getItem(i)
                var placeID: String = ""

                if (item != null) {
                    placeID = item.placeId
                }

                //                To specify which data types to return, pass an array of Place.Fields in your FetchPlaceRequest
                //                Use only those fields which are required.

                val placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)

                var request: FetchPlaceRequest? = null
                request = FetchPlaceRequest.builder(placeID, placeFields)
                    .build()

                if (request != null) {
                    placesClient.fetchPlace(request).addOnSuccessListener { task ->
                        Log.i(TAG, task.place.name + "\n" + task.place.address + "\n" + task.place.latLng)

                        //String address = task.getPlace().getName() + task.getPlace().getAddress();
                        val address = task.place.address
                        Log.i(TAG, "======== LOCATION=======" + address!!)

                        val latLng = task.place.latLng

                        if (latLng != null) {
                            latitude = latLng.latitude
                            longitude = latLng.longitude
                        }

                        Log.i(TAG, "======== LATITUDE =======$latitude========LONGITUDE=======$longitude")

                        if (!address.isEmpty()) {

                            /* val intent = Intent(getContext(), ActivitySavedPlacesAddressDetails::class.java)
                            intent.putExtra("isfor", "add")
                            intent.putExtra("address", address)
                            intent.putExtra("latitude", latitude.toString())
                            intent.putExtra("longitude", longitude.toString())
                            startActivity(intent)*/

                        }
                    }.addOnFailureListener { e ->
                        e.printStackTrace()
                        Log.i(TAG, e.message.toString())
                    }
                }

            } catch (e: Exception) {
                e.printStackTrace()
            } catch (e: IllegalArgumentException) {
                e.printStackTrace()
            }
        }


    private fun initAutoCompleteTextViewSearch() {
        val adapter = ArrayAdapter<String>(context!!, // Context
                android.R.layout.simple_spinner_dropdown_item  , // Layout
                placeList // Array
        )
        // Set the AutoCompleteTextView adapter
        act_location.setAdapter(adapter)
        act_location.setOnItemClickListener(this)
        // Auto complete threshold
        // The minimum number of characters to type to show the drop down
        act_location.threshold = 1
        adapter.notifyDataSetChanged()

    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (method.equals("GetLocation")) {
            locationlist.clear()
            typelist.clear()

            val dataModel = LocationListBeans()
            dataModel.LocationListBeans(da)

            locationlist = dataModel.locationList
            Utils.showLog(TAG, "==location list size==" + locationlist.size)

            for (i in locationlist.indices) {
                val name = locationlist.get(i).locName
                val id = locationlist.get(i).locId
                typelist.add(name.toString())
                typeidlist.add(id.toString())
            }

            type_spinner_type = ArrayAdapter(
                    activity!!,
                    android.R.layout.simple_spinner_dropdown_item,
                    typelist
            )
            // Creating adapter for spinner
            val dataAdapter1 =
                    ArrayAdapter(activity!!, android.R.layout.simple_spinner_item, typelist)

            // Drop down layout style - list view with radio button
            dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

            // attaching data adapter to spinner
            spinner_dis_type.adapter = dataAdapter1

            spinner_dis_type.setSelection(typeidlist.indexOf(districtId))

           /* val spinnerPosition = (spinner_dis_type.adapter as ArrayAdapter<String>).getPosition(districtId)
            spinner_dis_type.setSelection(spinnerPosition);*/
        }
        else if (method.equals("latLong")) {
            val dataModel = LatLongBean()
            dataModel.LatLongBean(da)

            Utils.showLog(TAG, "---lat--" + dataModel.latLngList.get(0).latitude)
            Utils.showLog(TAG, "---long--" + dataModel.latLngList.get(0).longitude)

            dislatitude = dataModel.latLngList.get(0).latitude!!.toDouble()
            dislongitude = dataModel.latLngList.get(0).longitude!!.toDouble()
        }

        else if (method.equals("suggestList")) {
            placeList.clear()
            val dataModel = LocationSuggestionBean()
            dataModel.LocationSuggestionBean(da)

            Utils.showLog(TAG, "---list size--" + dataModel.locationsList.size)

            Utils.showLog(TAG, "TEST_LOCATION 111")


            sugplaceList = dataModel.locationsList
            for (i in sugplaceList.indices) {

                Utils.showLog(TAG, "TEST_LOCATION 222")

                //old condition
                /*if (sugplaceList.get(i).locationName.toString().contains(resources.getString(R.string.txt_gujarat))) {
                    Utils.showLog(TAG, "TEST_LOCATION 333")
                    placeList.add(sugplaceList.get(i).locationName.toString())
                }*/

                //new condition for gujarati and english language of phone settings
                if (sugplaceList.get(i).locationName.toString().contains("Gujarat") ||
                        sugplaceList.get(i).locationName.toString().contains("ગુજરાત")) {
                    Utils.showLog(TAG, "TEST_LOCATION 333")
                    placeList.add(sugplaceList.get(i).locationName.toString())
                }

            }

            initAutoCompleteTextViewSearch()
        }

        else if (method.equals("addresslatLong")) {
            val dataModel = LatLongBean()
            dataModel.LatLongBean(da)

//            Utils.showLog(TAG, "---add lat--" + dataModel.latLngList.get(0).latitude)
//            Utils.showLog(TAG, "---add long--" + dataModel.latLngList.get(0).longitude)

            //This if condition is newly added (old code doesn't have conditions)
            if(!dataModel.latLngList.isEmpty()){
                latitude = dataModel.latLngList.get(0).latitude!!.toDouble()
                longitude = dataModel.latLngList.get(0).longitude!!.toDouble()
            }else{
                act_location.setText("")
                Utils.showToast(requireActivity(),"Address not found kindly use another nearby location")
            }



        }
    }

    override fun onFail(msg: String, method: String) {
        Utils.showToast(activity, method)
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        locationName = parent?.getItemAtPosition(position).toString()
        //call api to get location latlng
        getListingController.getLatLongOfSelectedAddress(locationName.toString())
    }

}