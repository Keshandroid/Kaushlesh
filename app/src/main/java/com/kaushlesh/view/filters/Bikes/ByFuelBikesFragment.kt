package com.kaushlesh.view.filters.Bikes

import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager

import com.kaushlesh.R
import com.kaushlesh.adapter.filter.FilterCommanAdapter
import com.kaushlesh.bean.FilterBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.view.filters.Property.ByBathroomFragment
import kotlinx.android.synthetic.main.fragment_by_fuel.*

class ByFuelBikesFragment : Fragment(), FilterCommanAdapter.ItemClickListener {
    private var bottomSheetListener: ByFuelBikesFragment.BottomSheetListener? = null
    val list = ArrayList<FilterBean>()
    lateinit var adapter: FilterCommanAdapter
    var cname: String? = null
    var cId: Int? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_by_fuel, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null) {
            cname = arguments!!.getString("ccname").toString()
            cId = arguments!!.getInt("cid")
            if (arguments!!.getParcelableArrayList<Parcelable>("ByFuelList")!!.isNotEmpty()) {
                list.clear()
                list.addAll(arguments!!.getParcelableArrayList<Parcelable>("ByFuelList") as ArrayList<FilterBean>)
                bindRecyclerviewFuel()
            } else {
                bindRecyclerviewFuel()
            }
            AppConstants.printLog(ByBathroomFragment.TAG, "==title==$cname" + cId)
        }
    }

    private fun bindRecyclerviewFuel() {

        val recyclerLayoutManager = LinearLayoutManager(context)
        recyclerview.layoutManager = recyclerLayoutManager
        if (list.isNotEmpty()) {
            adapter = FilterCommanAdapter(list, context!!)
        } else {
            adapter = FilterCommanAdapter(ownerList(), context!!)
        }
        adapter.setClicklistner(this)
        recyclerview.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    private fun ownerList(): List<FilterBean> {
        if (cId == 18) {
            list.add(FilterBean("CNG & Hybrid", 1))
            list.add(FilterBean("Diesel", 3))
            list.add(FilterBean("Petrol", 2))
            list.add(FilterBean("LPG", 4))
            list.add(FilterBean("Electric", 5))
        } else {
            list.add(FilterBean("Petrol", 1))
            list.add(FilterBean("Electric", 2))
        }
        return list
    }

    companion object {
        val TAG = "ByFuelBikesFragment"
        fun newInstance(): ByFuelBikesFragment {
            return ByFuelBikesFragment()
        }

        fun newInstance(listener: BottomSheetListener?): ByFuelBikesFragment {
            val fragment = ByFuelBikesFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }
    }

    override fun itemClickAllBrand(position: Int) {
        bottomSheetListener!!.selected(list, "By Fuel")
    }

    interface BottomSheetListener {
        fun selected(bathroomList: List<FilterBean>, property: String)
    }
}
