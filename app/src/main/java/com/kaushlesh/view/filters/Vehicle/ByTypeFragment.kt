package com.kaushlesh.view.filters.Vehicle

import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import com.kaushlesh.Controller.GetListingController

import com.kaushlesh.R
import com.kaushlesh.adapter.filter.FilterCommanAdapter
import com.kaushlesh.bean.FilterBean
import com.kaushlesh.bean.MobileBrandBeans
import com.kaushlesh.bean.VehicleCarDecorTypesBeans
import com.kaushlesh.bean.VehicleTypesBeans
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Utils
import kotlinx.android.synthetic.main.fragment_by_fuel.*
import org.json.JSONObject

class ByTypeFragment : Fragment(), FilterCommanAdapter.ItemClickListener,ParseControllerListener {
    lateinit var getListingController: GetListingController
/*    internal var brandlist: java.util.ArrayList<VehicleTypesBeans.VehicleTypeBeansList> = java.util.ArrayList()*/
internal var brandlist: java.util.ArrayList<MobileBrandBeans.MobileBrandBeansList> = java.util.ArrayList()
    internal var CarDecorTypelist: java.util.ArrayList<VehicleCarDecorTypesBeans.VehicleTypeBeansList> =
        java.util.ArrayList()

    var cname: String? = null
    var cid: Int? = null
    val list = ArrayList<FilterBean>()
    lateinit var bin:FilterBean

    private var bottomSheetListener: ByTypeFragment.BottomSheetListener? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_by_fuel, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null) {
            cname = arguments!!.getString("ccname").toString()
            cid = arguments!!.getInt("subcatid")

            if (arguments!!.getParcelableArrayList<Parcelable>("VehicalTypesList")!!.isNotEmpty()) {
                list.clear()
                list.addAll(arguments!!.getParcelableArrayList<Parcelable>("VehicalTypesList") as ArrayList<FilterBean>)
                bindRecyclerviewFuel()
            } else {
                bindRecyclerviewFuel()
            }
        }
    }

    private fun bindRecyclerviewFuel() {

        Log.e(TAG, "selected: " + list)
        val recyclerLayoutManager = LinearLayoutManager(context)
        recyclerview.layoutManager = recyclerLayoutManager

        if (list.isNotEmpty()) {
            Log.e(TAG, "if: " + list)
            var adapter: FilterCommanAdapter = FilterCommanAdapter(list, context!!)
            adapter.setClicklistner(this)
            recyclerview.adapter = adapter
            adapter.notifyDataSetChanged()

        } else {
            Log.e(TAG, "else: " + list)
            CallapiData()
            //adapter = FilterCommanAdapter(ownerList(), context!!)
        }



        /*       val recyclerLayoutManager = LinearLayoutManager(context)
               recyclerview.layoutManager = recyclerLayoutManager
               val adapter = FilterCommanAdapter(ownerList(), context!!)
               recyclerview.adapter = adapter
               adapter.notifyDataSetChanged()*/
    }

    private fun CallapiData() {
        getListingController = GetListingController(activity!!, this)

       /* if (cid==19) {
            getListingController.getVehicleTypeList()

        }else{
            getListingController.getVehicleCarDecorTypeList()
        }*/
        getListingController.getBrandList()
    }

    private fun ownerList(): List<FilterBean> {

        if (cid==19) {
            list.add(FilterBean("Rickshaws", 1))
            list.add(FilterBean("Buses", 2))
            list.add(FilterBean("Trucks", 3))
            list.add(FilterBean("Pick up Vans", 4))
            list.add(FilterBean("Tractors", 5))
            list.add(FilterBean("Heavy Machinery", 6))
            list.add(FilterBean("Others", 7))

        } else {
            list.add(FilterBean("Audio & car all Decor", 1))
            list.add(FilterBean("Wheels & Types", 2))
            list.add(FilterBean("Auto Parts", 3))
        }

        return list
    }

    companion object {
        val TAG = "ByTypeFragment"

        fun newInstance(): ByTypeFragment {
            return ByTypeFragment()
        }

        fun newInstance(listener: ByTypeFragment.BottomSheetListener): ByTypeFragment {
            val fragment = ByTypeFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }
    }

    interface BottomSheetListener {
        fun selected(typesList: List<FilterBean>, property: String)
    }

    override fun itemClickAllBrand(position: Int) {
        Log.e(TAG, "selected: " + list[position].selected)
        bottomSheetListener!!.selected(list, "Type")
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {

      /*  if (method.equals("GetVehicleType")) {
            brandlist.clear()
            list.clear()
            val dataModel = VehicleTypesBeans()
            dataModel.VehicleTypesBeans(da)
            brandlist = dataModel.vehicleTypeList
            Utils.showLog(TAG,"==vehicle type list size==" + brandlist.size)
            for(i in brandlist.indices)
            {
                val name = brandlist.get(i).typeName
                val id = brandlist.get(i).typeId
                list.add(FilterBean(name.toString(),id!!.toInt()))
            }
            bindRecyclerviewFuel()
        }*/

        if (method.equals("GetBrands")) {
            brandlist.clear()
            list.clear()
            val dataModel = MobileBrandBeans()
            dataModel.MobileBrandBeans(da)
            brandlist = dataModel.mobileBrandList
            Utils.showLog(TAG,"==vehicle type list size==" + brandlist.size)
            for(i in brandlist.indices)
            {
                val name = brandlist.get(i).brandName
                val id = brandlist.get(i).brandId
                list.add(FilterBean(name.toString(),id!!.toInt()))
            }
            bindRecyclerviewFuel()
        }

        if (method.equals("GetVehicleCarType")) {
            CarDecorTypelist.clear()
            list.clear()
            val dataModel = VehicleCarDecorTypesBeans()
            dataModel.VehicleCarDecorTypesBeans(da)

            CarDecorTypelist = dataModel.vehicleTypeList
            Utils.showLog(TAG,"==vehicle type list size==" + CarDecorTypelist.size)

            for(i in CarDecorTypelist.indices)
            {
                val name = CarDecorTypelist.get(i).typeName
                val id = CarDecorTypelist.get(i).typeId
                list.add(FilterBean(name.toString(),id!!.toInt()))
            }
            Utils.showLog(TAG,"==vehicle type list size==" + CarDecorTypelist.size)
            bindRecyclerviewFuel()
        }

    }


    override fun onFail(msg: String, method: String) {

    }
}
