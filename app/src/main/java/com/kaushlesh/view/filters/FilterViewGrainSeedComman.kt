package com.kaushlesh.view.filters

import android.app.Activity
import android.os.Bundle
import android.os.Parcelable
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.kaushlesh.R
import kotlinx.android.synthetic.main.filter_comman_view.*
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.kaushlesh.adapter.FilterAdapter
import com.kaushlesh.bean.FilterBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.view.filters.Vehicle.*
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.bean.Filter.FilterPropertyBean
import com.kaushlesh.bean.ProductListHouseBean
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.FiltersSaveRemoveData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.filters.GrainSeed.ByPriceGrainSeedFragment
import com.kaushlesh.view.filters.GrainSeed.ByStockGrainSeedFragment
import com.kaushlesh.view.filters.Mobile.ByPriceMobileFragment
import com.kaushlesh.view.filters.Property.*
import org.json.JSONObject


class FilterViewGrainSeedComman  : BottomSheetDialogFragment(), FilterAdapter.ItemClickListener,
    ByPremiumFragment.BottomSheetListener, ParseControllerListener,
    ByPriceGrainSeedFragment.BottomSheetListener, ByStockGrainSeedFragment.BottomSheetListener {

    // override fun getTheme(): Int = R.style.BottomSheetDialogTheme
    // override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = BottomSheetDialog(requireContext(), theme)
    var cname: String? = null
    var byPriceMin: String? = null
    var byPriceMax: String? = null
    var minStock: String? = null
    var maxStock: String? = null
    var url: String? = null
    val propertyByPremiumList = ArrayList<FilterBean>()
    var cId: Int? = null
    var categoryId: Int? = null
    private var plist: ArrayList<ProductListHouseBean.ProductListHouse> = ArrayList()
    var TAG = FilterViewGrainSeedComman::class.java.simpleName
    private var bottomSheetListener: BottomSheetListener? = null

    companion object {

        val TAG = "FilterViewGrainSeedComman"

        fun newInstance(
            listener: BottomSheetListener,
            name: String,
            id: Int,
            url: String,
            categoryId: Int
        ): FilterViewGrainSeedComman {
            val fragment = FilterViewGrainSeedComman()
            fragment.bottomSheetListener = listener
            fragment.cname = name
            fragment.cId = id
            fragment.url = url
            fragment.categoryId = categoryId
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.filter_comman_view, container, false)

        return v

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // this.getDialog()?.getWindow()?.setBackgroundDrawableResource(R.drawable.bg_white_top_rounded)

        AppConstants.printLog(TAG, "==title==$cname")
        tv_cate_name.setText(cname)

        //val scrollView = view.findViewById(R.id.scrollView) as MyScrollView

        //scrollView.setScrolling(true) // to enable scrolling.

        val coordinatorLayout = view.findViewById(R.id.cl_main) as CoordinatorLayout
        val bottomSheet = coordinatorLayout.findViewById(R.id.rl_bottom) as RelativeLayout

     /*   val behavior = BottomSheetBehavior.from(bottomSheet)
        behavior.isHideable = true
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED)
        behavior.peekHeight = Resources.getSystem().getDisplayMetrics().heightPixels
        //behavior.peekHeight = 1000
        bottomSheet.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT*/

        val offsetFromTop = 10
        (dialog as? BottomSheetDialog)?.behavior?.apply {
            isFitToContents = false
            setExpandedOffset(offsetFromTop)
            state = BottomSheetBehavior.STATE_EXPANDED
        }

        bindRecyclerview()

        btn_apply.setOnClickListener {
            lateinit var filterBeanDataModel: FilterPropertyBean
            filterBeanDataModel = FilterPropertyBean()

            filterBeanDataModel.categoryId = categoryId.toString()
            filterBeanDataModel.subCategoryId = cId.toString()
            filterBeanDataModel.property_byPremium_list = addSelectedDataPrimumsend(propertyByPremiumList, "id")

            if (byPriceMin != null) {
                filterBeanDataModel.minPrice = byPriceMin
                //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")

                FiltersSaveRemoveData.savePriceFilter(byPriceMin!!,categoryId.toString(),cId.toString(),"p1")
            } else {
                filterBeanDataModel.minPrice = ""
                //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")
                FiltersSaveRemoveData.savePriceFilter(byPriceMin.toString(),categoryId.toString(),cId.toString(),"p1")
            }
            if (byPriceMax != null) {
                filterBeanDataModel.maxPrice = byPriceMax
                //savePriceFilter(byPriceMax,categoryId.toString(),cId.toString(),"p2")

                FiltersSaveRemoveData.savePriceFilter(byPriceMax!!,categoryId.toString(),cId.toString(),"p2")
            } else {
                filterBeanDataModel.maxPrice = ""
                //savePriceFilter(byPriceMax,categoryId.toString(),cId.toString(),"p2")

                FiltersSaveRemoveData.savePriceFilter(byPriceMax.toString(),categoryId.toString(),cId.toString(),"p2")
            }

            if (minStock != null) {
                filterBeanDataModel.minStock = minStock
                FiltersSaveRemoveData.savePriceFilter(minStock!!,categoryId.toString(),cId.toString(),"s1")
            } else {
                filterBeanDataModel.minStock = ""
                FiltersSaveRemoveData.savePriceFilter(minStock.toString(),categoryId.toString(),cId.toString(),"s1")
            }
            if (maxStock != null) {
                filterBeanDataModel.maxStock = maxStock
                FiltersSaveRemoveData.savePriceFilter(maxStock!!,categoryId.toString(),cId.toString(),"s2")
            } else {
                filterBeanDataModel.maxStock = ""
                FiltersSaveRemoveData.savePriceFilter(maxStock.toString(),categoryId.toString(),cId.toString(),"s2")
            }

            lateinit var getListingController: GetListingController
            getListingController = GetListingController(activity!!, this)
            getListingController.callforDisplayfilterGainSeeds(filterBeanDataModel, url)

            FiltersSaveRemoveData.saveFilterDataList(propertyByPremiumList, categoryId.toString(), cId.toString(), "3")
        }

        btn_clear.setOnClickListener {
            dismiss()

            lateinit var filterBeanDataModel: FilterPropertyBean
            filterBeanDataModel = FilterPropertyBean()
            filterBeanDataModel.categoryId = categoryId.toString()
            filterBeanDataModel.subCategoryId = cId.toString()
            lateinit var getListingController: GetListingController
            getListingController = GetListingController(activity!!, this)
            getListingController.callforDisplayfilterGainSeeds(filterBeanDataModel, url)

            FiltersSaveRemoveData.cleardata(categoryId.toString(),cId.toString())

            Utils.showToast(context as Activity?,getString(R.string.txt_filter_reset_done))

        }

        iv_close.setOnClickListener {
            dismiss()
        }

        setdefaultFragment(cname)
    }

    private fun setdefaultFragment(cname: String?) {

        val lowPrice= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p1")
        var highPrice= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p2")
        if (lowPrice.toString()=="null"){
            //  Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
        }else{
            byPriceMin=FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p1")
            byPriceMax=FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p2")
        }

        val bundle = Bundle()
        bundle.putString("ccname", cname)
        bundle.putInt("cid", cId!!.toInt())
        bundle.putString("minPrice", byPriceMin)
        bundle.putString("maxPrice", byPriceMax)
        changeFilterFragment(
            ByPriceGrainSeedFragment.newInstance(this),
            ByPriceGrainSeedFragment.TAG,
            bundle,
            true
        )

    }

    private fun addSelectedDataPrimumsend(
        list: ArrayList<FilterBean>,
        typeGet: String
    ): ArrayList<String>? {
        val selectedList: ArrayList<String>? = arrayListOf()
        for (i in 0 until list.size) {
            if (list[i].selected) {
                if (list[i].id == 1) {
                    // selectedList!!.add("Premium")
                    selectedList!!.add("1")
                } else if (list[i].id == 2) {
                    //selectedList!!.add("Free")
                    selectedList!!.add("0")
                }
            }
        }
        return selectedList
    }

    private fun setupFullHeight(bottomSheetDialog: BottomSheetDialog) {
        val bottomSheet = bottomSheetDialog.findViewById(R.id.design_bottom_sheet) as FrameLayout?
        val behavior = BottomSheetBehavior.from(bottomSheet!!)
        val layoutParams = bottomSheet.layoutParams

        val windowHeight = getWindowHeight()
        if (layoutParams != null) {
            layoutParams.height = windowHeight
        }
        bottomSheet.layoutParams = layoutParams
        behavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    private fun getWindowHeight(): Int {
        // Calculate window height for fullscreen use
        val displayMetrics = DisplayMetrics()
        activity?.getWindowManager()?.defaultDisplay?.getMetrics(displayMetrics)
        return displayMetrics.heightPixels
    }

    private fun bindRecyclerview() {

        val layoutManager = LinearLayoutManager(context)
        rv_filter.layoutManager = layoutManager
        val adapter = FilterAdapter(typeList(), context!!)
        rv_filter.addItemDecoration(
            DividerItemDecoration(
                context,
                LinearLayout.VERTICAL
            ) as RecyclerView.ItemDecoration
        );
        adapter.setClicklistner(this)
        rv_filter.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    private fun typeList(): List<FilterBean> {
        val list = ArrayList<FilterBean>()

        list.add(FilterBean("By Price", 1))
        list.add(FilterBean("By Stock", 2))
        list.add(FilterBean("Premium Ad", 7))

        return list
    }

    interface BottomSheetListener : FilterViewComman.BottomSheetListener {
        override fun onButtonClicked(
            text: String,
            type: String,
            cname: ArrayList<ProductListHouseBean.ProductListHouse>
        )
    }


    override fun itemclickView(bean: FilterBean) {

        AppConstants.printLog(TAG, "==type==" + bean.name)
        val bundle = Bundle()
        if (bean.id == 1) {

            val lowPrice= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p1")
            var highPrice= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p2")
            if (lowPrice.toString()=="null"){
                //  Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            }else{
                byPriceMin=FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p1")
                byPriceMax=FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p2")
            }

            bundle.putString("ccname", cname)
            bundle.putInt("cid", cId!!.toInt())
            bundle.putString("minPrice", byPriceMin)
            bundle.putString("maxPrice", byPriceMax)
            changeFilterFragment(
                ByPriceGrainSeedFragment.newInstance(this),
                ByPriceGrainSeedFragment.TAG,
                bundle,
                true
            )

        }
        if (bean.id == 2) {

            val lowPrice= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "s1")
            var highPrice= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "s2")
            if (lowPrice.toString()=="null"){
                //  Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            }else{
                minStock=FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "s1")
                maxStock=FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "s2")
            }


            bundle.putString("ccname", cname)
            bundle.putInt("cid", cId!!.toInt())
            bundle.putString("minStock", minStock)
            bundle.putString("maxStock", maxStock)
            changeFilterFragment(
                ByStockGrainSeedFragment.newInstance(this),
                ByStockGrainSeedFragment.TAG,
                bundle,
                true
            )
        }

        if (bean.id == 7) {
            bundle.putString("ccname", cname)

            val data = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "3")
            //var data=getFilterData(categoryId.toString(), cId.toString(), "3")
            if (data.toString()=="null"){
                // Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            }else{
                propertyByPremiumList.clear()
                propertyByPremiumList.addAll(FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "3") as Collection<FilterBean>)
            }

            bundle.putParcelableArrayList(
                "propertyByPremiumList",
                propertyByPremiumList as java.util.ArrayList<out Parcelable>
            )
            changeFilterFragment(
                ByPremiumFragment.newInstance(this),
                ByPremiumFragment.TAG,
                bundle,
                true
            )
        }

    }

    private fun changeFilterFragment(fragment: Fragment, tag: String, args: Bundle?, add: Boolean) {
        val transaction = childFragmentManager.beginTransaction()
        if (args != null) fragment.arguments = args
        transaction.replace(R.id.fl_lmain, fragment, tag)
        transaction.commit()
    }

    override fun selected(premiumList: List<FilterBean>, property: String) {
        if (property.equals("Premium Ad")) {
            addPropertyData(propertyByPremiumList, premiumList)
        }
    }

    override fun selected(min: String, max: String, property: String) {
        if (property.equals("By Price")) {
            byPriceMax = max
            byPriceMin = min
        }
        if (property.equals("By Stock")) {
            maxStock = max
            minStock = min
        }
    }

    fun addPropertyData(
        listAdd: ArrayList<FilterBean>,
        typesList: List<FilterBean>
    ) {
        listAdd.clear()
        listAdd.addAll(typesList)
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (method == "Get Product") {
            Log.e(TAG, "onSuccess: " + ProductListHouseBean())
            plist.clear()
            val dataModel = ProductListHouseBean()
            dataModel.ProductListHouseBean(da)
            plist = dataModel.productlist
        }
        bottomSheetListener!!.onButtonClicked("Clicked", "apply", plist)
        dismiss()
    }

    override fun onFail(msg: String, method: String) {
        bottomSheetListener!!.onButtonClicked("Clicked", msg, plist)
        dismiss()
    }


}
