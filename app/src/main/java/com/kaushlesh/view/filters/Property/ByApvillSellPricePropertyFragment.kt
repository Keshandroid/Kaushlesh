package com.kaushlesh.view.filters.Property

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar
import com.kaushlesh.R
import com.kaushlesh.view.filters.FilterViewPropertyComman
import java.math.RoundingMode
import java.text.DecimalFormat

class ByApvillSellPricePropertyFragment : Fragment(), View.OnClickListener {

    var byPriceMin: String? = null
    var byPriceMax: String? = null
    var cname: String? = null
    internal lateinit var rangeSeekbar: CrystalRangeSeekbar
    internal lateinit var tvMin: TextView
    internal lateinit var tvMax: TextView
    private var bottomSheetListener: ByApvillSellPricePropertyFragment.BottomSheetListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_by_apvill_sell_price_property, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        rangeSeekbar = view.findViewById(R.id.rangeSeekbar) as CrystalRangeSeekbar
        rangeSeekbar.setMinValue(0F)
        rangeSeekbar.setMaxValue(30000000F)
        tvMin = view.findViewById(R.id.tvmin) as TextView
        tvMax = view.findViewById(R.id.tvmax) as TextView

        if (arguments != null) {
            val minfound = arguments!!.getString("minPrice").toString()
            if (!minfound.equals("null")) {

                byPriceMin = arguments!!.getString("minPrice")
                byPriceMax = arguments!!.getString("maxPrice")

                if(!byPriceMin!!.isEmpty() && !byPriceMax!!.isEmpty()) {
                    rangeSeekbar.setMinStartValue(byPriceMin!!.toFloat()).setMaxStartValue(byPriceMax!!.toFloat()).apply()
                }

                rangeSeekbar.setOnRangeSeekbarChangeListener { minValue, maxValue ->
                    SetTextviewValues(minValue, maxValue)
                }
            } else {
                rangeSeekbar.setOnClickListener(this)
                rangeSeekbar.setOnRangeSeekbarChangeListener { minValue, maxValue ->
                    SetTextviewValues(minValue, maxValue)
                }

            }
        }

    }

    private fun SetTextviewValues(minValue: Number?, maxValue: Number?) {

        if (minValue == 10000000 || minValue!!.toInt() >= 10000000) {
            var minNum = minValue!!.toDouble() / 10000000;
            val df = DecimalFormat("#.##")
            df.roundingMode = RoundingMode.FLOOR
            tvMin.text = df.format(minNum).toDouble().toString() + " cr"
        } else {
            tvMin.text = minValue.toString() + ""
        }

        if (maxValue == 10000000 || maxValue!!.toInt() >= 10000000) {
            var maxNum = maxValue.toDouble() / 10000000;
            val df = DecimalFormat("#.##")
            df.roundingMode = RoundingMode.FLOOR
            tvMax.text = df.format(maxNum).toDouble().toString() + " cr+"

        } else {
            tvMax.text = maxValue.toString() + "+"
        }

        rangeSeekbar.setOnRangeSeekbarFinalValueListener { minValue, maxValue ->
            bottomSheetListener!!.selected(
                minValue.toString(),
                maxValue.toString(),
                "By Price"
            )
        }
    }

    companion object {
        val TAG = "ByApvillSellPricePropertyFragment"

        fun newInstance(): ByApvillSellPricePropertyFragment {
            return ByApvillSellPricePropertyFragment()
        }

        fun newInstance(listener: ByApvillSellPricePropertyFragment.BottomSheetListener): ByApvillSellPricePropertyFragment {
            val fragment = ByApvillSellPricePropertyFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }
    }

    interface BottomSheetListener {
        fun selected(min: String, max: String, property: String)
    }

    override fun onClick(v: View?) {

    }
}