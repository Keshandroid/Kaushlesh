package com.kaushlesh.view.filters.Property

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.kaushlesh.R
import android.widget.TextView
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar
import kotlinx.android.synthetic.main.fragment_by_area.*

class ByLandAreaFragment : Fragment(), View.OnClickListener {
    private var bottomSheetListener: ByLandAreaFragment.BottomSheetListener? = null
    var byPriceMin: String? = null
    var byPriceMax: String? = null
    var cname: String? = null
    internal lateinit var rangeSeekbar: CrystalRangeSeekbar

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_by_area, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvtitle.setText(resources.getString(R.string.txt_fix_area_range_meters))
        // get seekbar from view
        rangeSeekbar = view.findViewById(R.id.rangeSeekbar) as CrystalRangeSeekbar

// get min and max text view
        val tvMin = view.findViewById(R.id.tvmin) as TextView
        val tvMax = view.findViewById(R.id.tvmax) as TextView

// set listener

        if (arguments != null) {
            val minfound = arguments!!.getString("minPrice").toString()
            if (!minfound.equals("null")) {
                byPriceMin = arguments!!.getString("minPrice")
                byPriceMax = arguments!!.getString("maxPrice")

                if(!byPriceMin!!.isEmpty() && !byPriceMax!!.isEmpty()) {
                    rangeSeekbar.setMinStartValue(byPriceMin!!.toFloat()).setMaxStartValue(byPriceMax!!.toFloat()).apply()
                }

                rangeSeekbar.setOnClickListener(this)

                rangeSeekbar.setOnRangeSeekbarChangeListener { minValue, maxValue ->
                    tvMin.text = minValue.toString()
                    tvMax.text = maxValue.toString() + "+"
                }
            } else {
                rangeSeekbar.setOnRangeSeekbarChangeListener { minValue, maxValue ->
                    tvMin.text = minValue.toString()
                    tvMax.text = maxValue.toString() + "+"
                }

            }
        }

// set final value listener
        rangeSeekbar.setOnRangeSeekbarFinalValueListener { minValue, maxValue ->
           /* bottomSheetListener!!.selected(
                minValue.toString(),
                maxValue.toString(),
                "By Land Area"
            )*/
            checkmaxValueandSet(minValue.toInt(),maxValue.toInt())
        }
    }

    private fun checkmaxValueandSet(minValue: Int, maxValue: Int) {
        if(maxValue == 10000)
        {
            bottomSheetListener!!.selected(
                    minValue.toString(),
                    (maxValue * 1000).toString(),
                    "By Land Area"
            )
        }
        else {

            bottomSheetListener!!.selected(
                    minValue.toString(),
                    maxValue.toString(),
                    "By Land Area"
            )
        }
    }

    companion object {
        val TAG = "ByLandAreaFragment"

        fun newInstance(): ByLandAreaFragment {
            return ByLandAreaFragment()
        }

        fun newInstance(listener: ByLandAreaFragment.BottomSheetListener): ByLandAreaFragment {
            val fragment = ByLandAreaFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }
    }

    interface BottomSheetListener {
        fun selected(min: String, max: String, property: String)
    }

    override fun onClick(v: View?) {
        TODO("Not yet implemented")
    }
}
