package com.kaushlesh.view.filters.GrainSeed

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.kaushlesh.R
import com.yahoo.mobile.client.android.util.rangeseekbar.RangeSeekBar
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener
import android.widget.TextView
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.view.filters.FilterViewGrainSeedComman
import com.kaushlesh.view.filters.FilterViewKhedutComman


class ByStockGrainSeedFragment : Fragment(),View.OnClickListener{

    private var bottomSheetListener: ByStockGrainSeedFragment.BottomSheetListener? = null
    var byPriceMin: String? = null
    var byPriceMax: String? = null
    internal lateinit var rangeSeekbar: CrystalRangeSeekbar
    internal lateinit var tvMin: TextView
    internal lateinit var tvMax: TextView
    var cname: String? = null
    var cid: Int? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_by_stock_grain_seed, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null) {
            cname = arguments!!.getString("ccname").toString()
            cid = arguments!!.getInt("cid").toInt()
        }
        // get seekbar from view
        rangeSeekbar = view.findViewById(R.id.rangeSeekbar) as CrystalRangeSeekbar

        rangeSeekbar.setMinValue(0F)
        rangeSeekbar.setMaxValue(10000F)

// get min and max text view
        tvMin = view.findViewById(R.id.tvmin) as TextView
        tvMax = view.findViewById(R.id.tvmax) as TextView

        if (arguments != null) {
            val minfound = arguments!!.getString("minStock").toString()
            if (!minfound.equals("null")) {
                byPriceMin = arguments!!.getString("minStock")
                byPriceMax = arguments!!.getString("maxStock")

                if(!byPriceMin!!.isEmpty() && !byPriceMax!!.isEmpty()) {
                    rangeSeekbar.setMinStartValue(byPriceMin!!.toFloat()).setMaxStartValue(byPriceMax!!.toFloat()).apply()
                }

                rangeSeekbar.setOnRangeSeekbarChangeListener { minValue, maxValue ->
                    SetTextviewValues(minValue, maxValue)
                }

                rangeSeekbar.setOnClickListener(this)

                rangeSeekbar.setOnRangeSeekbarFinalValueListener { minValue, maxValue ->
                   /* bottomSheetListener!!.selected(
                        minValue.toString(),
                        maxValue.toString(),
                        "By Stock"
                    )*/
                    checkmaxValueandSet(minValue.toInt(),maxValue.toInt())
                }
            } else {
                rangeSeekbar.setOnRangeSeekbarChangeListener { minValue, maxValue ->
                    SetTextviewValues(minValue, maxValue)
                }
                rangeSeekbar.setOnClickListener(this)


                rangeSeekbar.setOnRangeSeekbarFinalValueListener { minValue, maxValue ->
                   /* bottomSheetListener!!.selected(
                        minValue.toString(),
                        maxValue.toString(),
                        "By Stock"
                    )*/
                    checkmaxValueandSet(minValue.toInt(),maxValue.toInt())
                }
            }
        }
    }

    private fun checkmaxValueandSet(minValue: Int, maxValue: Int) {
        if(maxValue == 10000)
        {
            bottomSheetListener!!.selected(
                    minValue.toString(),
                    (maxValue * 1000).toString(),
                    "By Stock"
            )
        }
        else {

            bottomSheetListener!!.selected(
                    minValue.toString(),
                    maxValue.toString(),
                    "By Stock"
            )
        }
    }
    private fun SetTextviewValues(minValue: Number?, maxValue: Number?) {
        tvMin.text = minValue.toString() + ""
        tvMax.text = maxValue.toString() + "+"
    }

    companion object {
        val TAG = "ByStockGrainSeedFragment"

        fun newInstance(): ByStockGrainSeedFragment {
            return ByStockGrainSeedFragment()
        }

        fun newInstance(listener: FilterViewGrainSeedComman): ByStockGrainSeedFragment {
            val fragment = ByStockGrainSeedFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }


        fun newInstance(listener: FilterViewKhedutComman): ByStockGrainSeedFragment {
            val fragment = ByStockGrainSeedFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }
    }

    interface BottomSheetListener {
        fun selected(min: String, max: String, property: String)
    }

    override fun onClick(v: View?) {

    }
}