package com.kaushlesh.view.filters

import android.app.Activity
import android.content.res.Resources
import android.os.Bundle
import android.os.Parcelable
import android.util.DisplayMetrics
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.R
import com.kaushlesh.adapter.FilterAdapter
import com.kaushlesh.bean.Filter.FilterPropertyBean
import com.kaushlesh.bean.FilterBean
import com.kaushlesh.bean.MobileBrandBeans
import com.kaushlesh.bean.ProductListHouseBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.FiltersSaveRemoveData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.filters.Comman.ByPriceCommanFragment
import com.kaushlesh.view.filters.Mobile.ByPriceMobileFragment
import com.kaushlesh.view.filters.Vehicle.ByPremiumFragment
import kotlinx.android.synthetic.main.filter_comman_view.*
import org.json.JSONObject


class FilterViewGeneral : BottomSheetDialogFragment(), FilterAdapter.ItemClickListener,
    ParseControllerListener, ByPremiumFragment.BottomSheetListener,
    ByPriceCommanFragment.BottomSheetListener,
    ByPriceMobileFragment.BottomSheetListener {
    var TAG = FilterViewComman::class.java.simpleName
    private var bottomSheetListener: BottomSheetListener? = null
    var cname: String? = null
    var byPriceMin: String? = null
    var byPriceMax: String? = null
    var url: String? = null
    var propertyByPremiumList = ArrayList<FilterBean>()
    var cId: Int? = null
    var categoryId: Int? = null
    private var plist: ArrayList<ProductListHouseBean.ProductListHouse> = ArrayList()

    companion object {

        val TAG = "FilterViewGeneral"

        fun newInstance(
            listener: FilterViewGeneral.BottomSheetListener,
            name: String,
            id: Int,
            url: String,
            categoryId: Int
        ): FilterViewGeneral {
            val fragment = FilterViewGeneral()
            fragment.bottomSheetListener = listener
            fragment.cname = name
            fragment.cId = id
            fragment.url = url
            fragment.categoryId = categoryId

            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.filter_comman_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        AppConstants.printLog(TAG, "==title==$cname")
        tv_cate_name.setText(cname)
        val coordinatorLayout = view.findViewById(R.id.cl_main) as CoordinatorLayout
        val bottomSheet = coordinatorLayout.findViewById(R.id.rl_bottom) as RelativeLayout

       /* val behavior = BottomSheetBehavior.from(bottomSheet)
        behavior.isHideable = true
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED)
        behavior.peekHeight = Resources.getSystem().getDisplayMetrics().heightPixels
        //behavior.peekHeight = 1000
        bottomSheet.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT*/

        val offsetFromTop = 10
        (dialog as? BottomSheetDialog)?.behavior?.apply {
            isFitToContents = false
            setExpandedOffset(offsetFromTop)
            state = BottomSheetBehavior.STATE_EXPANDED
        }

        bindRecyclerview()

        btn_apply.setOnClickListener {
            var mobileTypes: String? = ""
            if (cId!!.equals(13)) {
                mobileTypes = "1"
            }

            lateinit var filterBeanDataModel: FilterPropertyBean
            filterBeanDataModel = FilterPropertyBean()

            filterBeanDataModel.categoryId = categoryId.toString()
            filterBeanDataModel.subCategoryId = cId.toString()
            filterBeanDataModel.property_byPremium_list = addSelectedDataPrimumsend(propertyByPremiumList, "id")

            //types

            //premimum
            val data = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "1")
            if (!propertyByPremiumList.isEmpty()) {

                filterBeanDataModel.property_byPremium_list =addSelectedDataPrimumsend(propertyByPremiumList, "id")

            }
            else {
                if (data != null) {
                    propertyByPremiumList.clear()
                    propertyByPremiumList.addAll(data as Collection<FilterBean>)
                    filterBeanDataModel.property_byPremium_list = addSelectedDataPrimumsend(propertyByPremiumList, "id")

                } else {
                    propertyByPremiumList.clear()
                    filterBeanDataModel.property_byPremium_list = arrayListOf()
                    propertyByPremiumList = arrayListOf()
                }
            }

           /* if (byPriceMin != null) {
                filterBeanDataModel.minPrice = byPriceMin
                FiltersSaveRemoveData.savePriceFilter(byPriceMin!!,categoryId.toString(),cId.toString(),"p1")
            }
            if (byPriceMax != null) {
                filterBeanDataModel.maxPrice = byPriceMax
                FiltersSaveRemoveData.savePriceFilter(byPriceMax!!,categoryId.toString(),cId.toString(),"p2")

            }*/

            val lowPrice= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p1")
            val highPrice= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p2")
            Utils.showLog(TAG, "==saved price1==" + lowPrice)
            Utils.showLog(TAG, "==saved price2==" + highPrice)

            if (byPriceMin != null) {
                filterBeanDataModel.minPrice = byPriceMin
                //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")

                FiltersSaveRemoveData.savePriceFilter(byPriceMin!!, categoryId.toString(), cId.toString(), "p1")
            }
            else {
                if (byPriceMin.toString().equals("null")) {
                    filterBeanDataModel.minPrice = lowPrice
                    byPriceMin = lowPrice
                    FiltersSaveRemoveData.savePriceFilter(byPriceMin.toString(), categoryId.toString(), cId.toString(), "p1")
                } else {
                    filterBeanDataModel.minPrice = ""

                    //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")
                    FiltersSaveRemoveData.savePriceFilter("", categoryId.toString(), cId.toString(), "p1")
                }
            }

            if (byPriceMax != null) {
                filterBeanDataModel.maxPrice = byPriceMax
                //savePriceFilter(byPriceMax,categoryId.toString(),cId.toString(),"p2")

                FiltersSaveRemoveData.savePriceFilter(byPriceMax!!, categoryId.toString(), cId.toString(), "p2")
            }
            else {
                if (byPriceMax.toString().equals("null")) {
                    filterBeanDataModel.maxPrice = highPrice
                    byPriceMax = highPrice
                    FiltersSaveRemoveData.savePriceFilter(byPriceMax.toString(), categoryId.toString(), cId.toString(), "p2")
                } else {
                    filterBeanDataModel.maxPrice = ""
                    //savePriceFilter(byPriceMax,categoryId.toString(),cId.toString(),"p2")

                    FiltersSaveRemoveData.savePriceFilter("", categoryId.toString(), cId.toString(), "p2")
                }
            }

            lateinit var getListingController: GetListingController
            getListingController = GetListingController(activity!!, this)
            //getListingController.callforDisplayfilterGenralData(filterBeanDataModel, url)

            bottomSheetListener!!.callApiWithFilter(filterBeanDataModel,url.toString())
            dismiss()

            FiltersSaveRemoveData.saveFilterDataList(propertyByPremiumList, categoryId.toString(), cId.toString(), "1")
        }

        btn_clear.setOnClickListener {
            dismiss()

            lateinit var filterBeanDataModel: FilterPropertyBean
            filterBeanDataModel = FilterPropertyBean()
            filterBeanDataModel.categoryId = categoryId.toString()
            filterBeanDataModel.subCategoryId = cId.toString()
            lateinit var getListingController: GetListingController
            getListingController = GetListingController(activity!!, this)
            //getListingController.callforDisplayfilterGenralData(filterBeanDataModel, url)

            bottomSheetListener!!.callApiWithFilter(filterBeanDataModel,url.toString())

            FiltersSaveRemoveData.cleardata(categoryId.toString(),cId.toString())

            Utils.showToast(context as Activity?,getString(R.string.txt_filter_reset_done))
        }

        iv_close.setOnClickListener {
            dismiss()
        }

        setdefaultFragment(cname)
    }

    private fun setdefaultFragment(cname: String?) {
        val bundle = Bundle()

        if (categoryId == 7 || categoryId == 10 || categoryId == 11 || cId == 1005 || cId == 1006 || cId == 1007 || cId == 1008 || cId == 1009) {
            bundle.putString("ccname", cname)
            val data=FiltersSaveRemoveData.getFilterData(categoryId.toString(),cId.toString(),"1")
            if (data.toString()=="null"){
                // Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            }else{

                propertyByPremiumList.clear()
                propertyByPremiumList.addAll(FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "1") as Collection<FilterBean>)
            }
            bundle.putParcelableArrayList("propertyByPremiumList", propertyByPremiumList as java.util.ArrayList<out Parcelable>)
            changeFilterFragment(ByPremiumFragment.newInstance(this), ByPremiumFragment.TAG, bundle, true)
        } else {

            val lowPrice=FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p1")
            var highPrice=FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p2")
            if (lowPrice.toString()=="null"){
                //  Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            }else{
                byPriceMin=FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p1")
                byPriceMax=FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p2")
            }

            bundle.putString("ccname", cname)
            bundle.putInt("cid", cId!!.toInt())
            bundle.putString("minPrice", byPriceMin)
            bundle.putString("maxPrice", byPriceMax)
            changeFilterFragment(
                ByPriceCommanFragment.newInstance(this),
                ByPriceCommanFragment.TAG,
                bundle,
                true
            )
        }
    }


    private fun bindRecyclerview() {

        val layoutManager = LinearLayoutManager(context)
        rv_filter.layoutManager = layoutManager as RecyclerView.LayoutManager?
        val adapter = FilterAdapter(typeList(), context!!)
        rv_filter.addItemDecoration(
            DividerItemDecoration(
                context,
                LinearLayout.VERTICAL
            ) as RecyclerView.ItemDecoration
        );
        adapter.setClicklistner(this)
        rv_filter.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    private fun typeList(): List<FilterBean> {
        val list = ArrayList<FilterBean>()


         if(cId == 1005 || cId == 1006 || cId == 1007 || cId == 1008 || cId == 1009)
         {
             //list.add(FilterBean("By Price", 1))
             list.add(FilterBean("Premium Ad", 7))
         }
         else {
            list.add(FilterBean("By Price", 1))
            list.add(FilterBean("Premium Ad", 7))
        }

        return list
    }

    interface BottomSheetListener {
        fun onButtonClicked(
                text: String,
                type: String,
                cname: ArrayList<ProductListHouseBean.ProductListHouse>
        )

        fun callApiWithFilter(filterBeanDataModel : FilterPropertyBean, url : String)
    }


    override fun itemclickView(bean: FilterBean) {
        AppConstants.printLog(TAG, "==type==" + bean.name)
        val bundle = Bundle()

        if (bean.id == 1) {

            val lowPrice=FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p1")
            var highPrice=FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p2")
            if (lowPrice.toString()=="null"){
                //  Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            }else{
                byPriceMin=FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p1")
                byPriceMax=FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p2")
            }

            bundle.putString("ccname", cname)
            bundle.putInt("cid", cId!!.toInt())
            bundle.putString("minPrice", byPriceMin)
            bundle.putString("maxPrice", byPriceMax)
            changeFilterFragment(
                ByPriceCommanFragment.newInstance(this),
                ByPriceCommanFragment.TAG,
                bundle,
                true
            )
        }
        if (bean.id == 7) {
            bundle.putString("ccname", cname)

            //propertyListedByList.clear()
            val data=FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "1")
            Log.e(TAG, "logdata: "+data )
            if (data.toString()=="null"){
                //  Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            }else{
                propertyByPremiumList.clear()
                propertyByPremiumList.addAll(FiltersSaveRemoveData.getFilterData(
                        categoryId.toString(),
                        cId.toString(),
                        "1"
                ) as Collection<FilterBean>)

            }

            bundle.putParcelableArrayList(
                "propertyByPremiumList",
                propertyByPremiumList as java.util.ArrayList<out Parcelable>
            )
            changeFilterFragment(
                ByPremiumFragment.newInstance(this),
                ByPremiumFragment.TAG,
                bundle,
                true
            )
        }
    }

    private fun addSelectedDataPrimumsend(
        list: ArrayList<FilterBean>,
        typeGet: String
    ): ArrayList<String>? {
        val selectedList: ArrayList<String>? = arrayListOf()
        for (i in 0 until list.size) {
            if (list[i].selected) {
                if (list[i].id == 1) {
                    // selectedList!!.add("Premium")
                    selectedList!!.add("1")
                } else if (list[i].id == 2) {
                    //selectedList!!.add("Free")
                    selectedList!!.add("0")
                }
            }
        }
        return selectedList
    }

    private fun changeFilterFragment(fragment: Fragment, tag: String, args: Bundle?, add: Boolean) {
        val transaction = childFragmentManager.beginTransaction()
        if (args != null) fragment.arguments = args
        transaction.replace(R.id.fl_lmain, fragment, tag)
        transaction.commit()
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (method == "Get Product") {
            Log.e(TAG, "onSuccess: " + ProductListHouseBean())
            plist.clear()
            val dataModel = ProductListHouseBean()
            dataModel.ProductListHouseBean(da)
            plist = dataModel.productlist
        }
        bottomSheetListener!!.onButtonClicked("Clicked", "apply", plist)
        dismiss()

    }

    override fun onFail(msg: String, method: String) {
        bottomSheetListener!!.onButtonClicked("Clicked", msg, plist)
        dismiss()
    }

    override fun selected(min: String, max: String, property: String) {
        if (property.equals("By Price")) {
            byPriceMax = max
            byPriceMin = min
        }

        savePremium()
    }

    override fun selected(typesList: List<FilterBean>, property: String) {
        if (property.equals("Premium Ad")) {
            addPropertyData(propertyByPremiumList, typesList)
        }
    }

    fun addPropertyData(
        listAdd: ArrayList<FilterBean>,
        typesList: List<FilterBean>
    ) {
        listAdd.clear()
        listAdd.addAll(typesList)
    }

    private fun savePremium() {

        lateinit var filterBeanDataModel: FilterPropertyBean
        filterBeanDataModel = FilterPropertyBean()
        val data= FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "5")
        if (data.toString()=="null"){
            // Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            filterBeanDataModel.property_byPremium_list = addSelectedDataPrimumsend(propertyByPremiumList, "id")
            FiltersSaveRemoveData.saveFilterDataList(propertyByPremiumList, categoryId.toString(), cId.toString(), "5")
        }else{
            propertyByPremiumList.clear()
            propertyByPremiumList.addAll(FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "5") as Collection<FilterBean>)
            filterBeanDataModel.property_byPremium_list = addSelectedDataPrimumsend(propertyByPremiumList, "id")
            FiltersSaveRemoveData.saveFilterDataList(propertyByPremiumList, categoryId.toString(), cId.toString(), "5")
        }
    }
}

