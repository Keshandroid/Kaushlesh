package com.kaushlesh.view.filters.Khedut

import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.kaushlesh.R
import com.kaushlesh.adapter.filter.FilterCommanAdapter
import com.kaushlesh.bean.FilterBean
import kotlinx.android.synthetic.main.fragment_by_fuel.*

class ByProductTypesFragment : Fragment(),FilterCommanAdapter.ItemClickListener {

    var bottomSheetListener: ByProductTypesFragment.BottomSheetListener? = null
    val list = ArrayList<FilterBean>()
    var cname  : String ?= null
    lateinit var adapter:FilterCommanAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_by_fuel, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null) {
            cname = arguments!!.getString("ccname").toString()
            if (arguments!!.getParcelableArrayList<Parcelable>("propertyByBachelorAllowList")!!.isNotEmpty()) {
                list.clear()
                list.addAll(arguments!!.getParcelableArrayList<Parcelable>("propertyByBachelorAllowList") as ArrayList<FilterBean>)
                bindRecyclerviewFuel()
            }else{
                bindRecyclerviewFuel()
            }
        }
    }

    private fun bindRecyclerviewFuel() {

        val recyclerLayoutManager = LinearLayoutManager(context)
        recyclerview.layoutManager = recyclerLayoutManager
        if (list.isNotEmpty()){
            adapter = FilterCommanAdapter(list, context!!)
        }
        else{
            adapter = FilterCommanAdapter(ownerList(), context!!)
        }
        adapter.setClicklistner(this)
        recyclerview.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    private fun ownerList(): List<FilterBean> {
        list.add(FilterBean("Organic",1))
        list.add(FilterBean("Inorganic",2))
        return list
    }

    companion object {
        val TAG = "ByProductTypesFragment"

        fun newInstance(listener: ByProductTypesFragment.BottomSheetListener): ByProductTypesFragment {
            val fragment = ByProductTypesFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }
    }

    override fun itemClickAllBrand(position: Int) {
        //bottomSheetListener!!.selected(list,"BachelorsAllow")

        for (i in 0 until list.size){
            if (position!=i){
                list[i].selected=false
            }
            adapter.notifyDataSetChanged()
        }
        bottomSheetListener!!.selected(list,"BachelorsAllow")

    }

    interface BottomSheetListener {
        fun selected(bachelorAllowList:List<FilterBean>,property:String)
    }
}
