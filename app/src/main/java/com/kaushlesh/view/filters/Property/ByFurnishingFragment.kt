package com.kaushlesh.view.filters.Property

import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager

import com.kaushlesh.R
import com.kaushlesh.adapter.filter.FilterCommanAdapter
import com.kaushlesh.bean.FilterBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.view.filters.FilterViewPropertyComman
import kotlinx.android.synthetic.main.fragment_by_fuel.*

class ByFurnishingFragment : Fragment() ,FilterCommanAdapter.ItemClickListener{
    private var bottomSheetListener: ByFurnishingFragment.BottomSheetListener? = null
    val list = ArrayList<FilterBean>()
    var cname  : String ?= null
    lateinit var adapter:FilterCommanAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_by_fuel, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null) {
            cname = arguments!!.getString("ccname").toString()

            if (arguments!!.getParcelableArrayList<Parcelable>("propertyByFunishingList")!!.isNotEmpty()) {
                list.clear()
                list.addAll(arguments!!.getParcelableArrayList<Parcelable>("propertyByFunishingList") as ArrayList<FilterBean>)
                bindRecyclerviewFuel()
            }else{
                bindRecyclerviewFuel()
            }

            AppConstants.printLog(TAG, "==title==$cname")
        }
    }

    private fun bindRecyclerviewFuel() {
        val recyclerLayoutManager = LinearLayoutManager(context)
        recyclerview.layoutManager = recyclerLayoutManager
        if (list.isNotEmpty()){
            adapter = FilterCommanAdapter(list, context!!)
        }
        else{
            adapter = FilterCommanAdapter(ownerList(), context!!)
        }
        adapter.setClicklistner(this)
        recyclerview.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    private fun ownerList(): List<FilterBean> {

        list.add(FilterBean("Full Furnished",1))
        list.add(FilterBean("Semi Furnished",2))
        list.add(FilterBean("Unfurnished",3))

        return list
    }

    companion object {
        val TAG = "ByFurnishingFragment"

        fun newInstance(listener: FilterViewPropertyComman): ByFurnishingFragment {
            val fragment = ByFurnishingFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }
    }

    override fun itemClickAllBrand(position: Int) {
        bottomSheetListener!!.selected(list,"By Furnishing")
        Log.e(ListedByFragment.TAG, "selected: Furnishing "+list[position].selected)

    }

    interface BottomSheetListener {
        fun selected(furnishingList:List<FilterBean>,property:String)
    }
}
