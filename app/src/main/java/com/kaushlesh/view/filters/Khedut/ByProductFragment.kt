package com.kaushlesh.view.filters.Khedut

import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.Controller.GetListingController

import com.kaushlesh.R
import com.kaushlesh.adapter.filter.Mobiles.ByBrandMobileAdapter
import com.kaushlesh.adapter.filter.Mobiles.ByMobileOtherBrandAdapter
import com.kaushlesh.bean.*
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Utils
import kotlinx.android.synthetic.main.fragment_by_product.*
import org.json.JSONObject
import java.util.ArrayList
import kotlin.IllegalStateException

class ByProductFragment : Fragment(), ByMobileOtherBrandAdapter.ItemClickListener,
    ByBrandMobileAdapter.ItemClickListener,
    ParseControllerListener {
    val listbrand = ArrayList<MobileBrandBeans.MobileBrandBeansList>()
    val listbrandAll = ArrayList<MobileBrandBeans.MobileBrandBeansList>()
    val list = ArrayList<MobileBrandBeans.MobileBrandBeansList>()
    var bottomSheetListener: BottomSheetListener? = null
    var cname: String? = null
    var cid: Int? = null
    var scid  : Int ?= null
    val addAll = ArrayList<MobileBrandBeans.MobileBrandBeansList>()
    private lateinit var getListingController: GetListingController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_by_product, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null) {
            cname = arguments!!.getString("ccname").toString()
            cid = arguments!!.getInt("catid")
            scid = arguments!!.getInt("subcatid")

            if(scid == 113)
            {
                tv_heading2.text = resources.getString(R.string.txt_select_animal)
            }

            if (arguments!!.getParcelableArrayList<Parcelable>("ByBrand")!!.isNotEmpty()) {
                list.clear()
                listbrand.clear()
                listbrandAll.addAll(arguments!!.getParcelableArrayList<Parcelable>("ByBrand") as ArrayList<MobileBrandBeans.MobileBrandBeansList>)

                for (i in 0 until listbrandAll.size) {
                    if (listbrandAll[i].isPopular == 1) {
                        listbrand.add(listbrandAll[i])
                    } else {
                        list.add(listbrandAll[i])
                    }
                }
                if (list.isNotEmpty()){
                    bindRecyclerviewAllBrand()

                }
            } else {
              apiCall()
            }
        }
    }

    private fun bindRecyclerviewAllBrand() {
        try {
            if (list.isNotEmpty()) {
                val recyclerLayoutManager = LinearLayoutManager(context)
                rvallbrands.layoutManager = recyclerLayoutManager
                val adapter = ByMobileOtherBrandAdapter(list, context!!)
                adapter.setClicklistner(this)
                rvallbrands.adapter = adapter
                rvallbrands.setNestedScrollingEnabled(false);
                adapter.notifyDataSetChanged()
            }
        }
        catch (e : IllegalStateException)
        {
            e.printStackTrace()
        }
    }

    fun apiCall(){
            Utils.showProgress(this.activity!!)
            getListingController = GetListingController(activity!!, this)
            getListingController.getBrandList()

    }

    companion object {
        val TAG = "ByProductFragment"

        fun newInstance(): ByProductFragment {
            return ByProductFragment()
        }

        fun newInstance(listener: BottomSheetListener): ByProductFragment {
            val fragment = ByProductFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }


    }



    override fun onSuccess(da: JSONObject, message: String, method: String) {
        listbrand.clear()
        val dataModel = MobileBrandBeans()
        dataModel.MobileBrandBeans(da)

        for (i in 0 until dataModel.mobileBrandList.size) {
            if (dataModel.mobileBrandList[i].isPopular == 1) {
                listbrand.add(dataModel.mobileBrandList[i])
            } else {
                list.add(dataModel.mobileBrandList[i])
            }
        }
        bindRecyclerviewAllBrand()
        Utils.dismissProgress()
    }

    override fun onFail(msg: String, method: String) {
    }

    override fun itemclick() {
        addAll.clear()
        addAll.addAll(list)
        addAll.addAll(listbrand)
        bottomSheetListener!!.selectedBrand(addAll, "brands")
      //  bottomSheetListener!!.selectedBrand(list, "brands")
    }

    override fun itemclick1() {
        addAll.clear()
        addAll.addAll(list)
        addAll.addAll(listbrand)
        bottomSheetListener!!.selectedBrand(addAll, "brands")
    }

    interface BottomSheetListener {
        fun selectedBrand(BrandCase: List<MobileBrandBeans.MobileBrandBeansList>, property: String)
    }

}
