package com.kaushlesh.view.filters.Mobile

import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.kaushlesh.Controller.GetListingController

import com.kaushlesh.R
import com.kaushlesh.adapter.filter.Mobiles.TabBrandAdapter

import com.kaushlesh.bean.MobileBrandBeans
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.filters.FilterViewMobileComman
import kotlinx.android.synthetic.main.fragment_by_fuel.*
import org.json.JSONObject

class ByBrandTabsFragment : Fragment(), TabBrandAdapter.ItemClickListener, ParseControllerListener {
    private lateinit var getListingController: GetListingController
    private var bottomSheetListener: ByBrandTabsFragment.BottomSheetListener? = null
    val list = ArrayList<MobileBrandBeans.MobileBrandBeansList>()
    var cname: String? = null
    var cid: Int? = null
    var scid: Int? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_by_fuel, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (arguments != null) {
            cname = arguments!!.getString("ccname").toString()
            cid = arguments!!.getInt("catid")
            scid = arguments!!.getInt("subcatid")
            if (arguments!!.getParcelableArrayList<Parcelable>("ByBrand")!!.isNotEmpty()) {
                list.clear()
                list.addAll(arguments!!.getParcelableArrayList<Parcelable>("ByBrand") as java.util.ArrayList<MobileBrandBeans.MobileBrandBeansList>)
                bindRecyclerviewFuel()
            } else {
                bindRecyclerviewFuel()
            }
        }
    }

    private fun bindRecyclerviewFuel() {
        if (list.isNotEmpty()) {
            val recyclerLayoutManager = LinearLayoutManager(context)
            recyclerview.layoutManager = recyclerLayoutManager
            var adapter: TabBrandAdapter = TabBrandAdapter(list, context!!)
            adapter.setClicklistner(this)
            recyclerview.adapter = adapter
            recyclerview.setNestedScrollingEnabled(false);
            adapter.notifyDataSetChanged()
        } else {
            Utils.showProgress(this.activity!!)
            getListingController = GetListingController(activity!!, this)
            getListingController.getBrandList()
        }
    }

    companion object {
        val TAG = "ByBrandTabsFragment"

        fun newInstance(): ByBrandTabsFragment {
            return ByBrandTabsFragment()
        }

        fun newInstance(listener: FilterViewMobileComman): ByBrandTabsFragment {
            val fragment = ByBrandTabsFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }
    }

    interface BottomSheetListener {
        fun TabBrandselected(
            BrandList: List<MobileBrandBeans.MobileBrandBeansList>,
            property: String
        )
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        list.clear()
        val dataModel = MobileBrandBeans()
        dataModel.MobileBrandBeans(da)
        list.addAll(dataModel!!.mobileBrandList)
        bindRecyclerviewFuel()
        Utils.dismissProgress()
    }

    override fun onFail(msg: String, method: String) {

    }

    override fun itemClickBrand() {
        bottomSheetListener!!.TabBrandselected(list, "Brand")
    }
}
