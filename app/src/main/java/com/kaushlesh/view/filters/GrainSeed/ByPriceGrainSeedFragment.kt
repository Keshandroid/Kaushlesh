package com.kaushlesh.view.filters.GrainSeed

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.kaushlesh.R
import com.yahoo.mobile.client.android.util.rangeseekbar.RangeSeekBar
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener
import android.widget.TextView
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.view.filters.FilterViewGrainSeedComman
import com.kaushlesh.view.filters.FilterViewKhedutComman
import kotlinx.android.synthetic.main.fragment_by_price.*

class ByPriceGrainSeedFragment : Fragment(),View.OnClickListener {

        var byPriceMin: String? = null
        var byPriceMax: String? = null
        internal lateinit var rangeSeekbar: CrystalRangeSeekbar
        internal lateinit var tvMin: TextView
        internal lateinit var tvMax: TextView
        var cname: String? = null
        var cid: Int? = null

        private var bottomSheetListener: BottomSheetListener? = null

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            // Inflate the layout for this fragment
            return inflater.inflate(R.layout.fragment_by_price, container, false)
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)


            if (arguments != null) {
                cname = arguments!!.getString("ccname").toString()
                cid = arguments!!.getInt("cid").toInt()

                AppConstants.printLog(ByPriceGrainSeedFragment.TAG, "==title==$cname")
            }

            // get seekbar from view
            rangeSeekbar = view.findViewById(R.id.rangeSeekbar) as CrystalRangeSeekbar

            //rangeSeekbar.setMinValue(0F)
            //rangeSeekbar.setMaxValue(200F)

            if(cid == 110 || cid == 111 || cid == 112)
            {

                tv_heading_title.text = resources.getText(R.string.txt_price_kg_filter)
            }
            if (cid == 113) {
                rangeSeekbar.setMinValue(0F)
                rangeSeekbar.setMaxValue(50000F)
            } else{
                rangeSeekbar.setMinValue(0F)
                rangeSeekbar.setMaxValue(200F)
            }

            tvMin = view.findViewById(R.id.tvmin) as TextView
            tvMax = view.findViewById(R.id.tvmax) as TextView


            if (arguments != null) {
                var minfound = arguments!!.getString("minPrice").toString()
                if (!minfound.equals("null")) {
                    byPriceMin = arguments!!.getString("minPrice")
                    byPriceMax = arguments!!.getString("maxPrice")

                    if(!byPriceMin!!.isEmpty() && !byPriceMax!!.isEmpty()) {
                        rangeSeekbar.setMinStartValue(byPriceMin!!.toFloat()).setMaxStartValue(byPriceMax!!.toFloat()).apply()
                    }

                    rangeSeekbar.setOnRangeSeekbarChangeListener { minValue, maxValue ->
                        SetTextviewValues(minValue, maxValue)
                    }
                    rangeSeekbar.setOnClickListener(this)

                    rangeSeekbar.setOnRangeSeekbarFinalValueListener { minValue, maxValue ->
                      /*  bottomSheetListener!!.selected(
                            minValue.toString(),
                            maxValue.toString(),
                            "By Price"
                        )*/
                        checkmaxValueandSet(minValue.toInt(),maxValue.toInt())
                    }

                } else {
                    rangeSeekbar.setOnRangeSeekbarChangeListener { minValue, maxValue ->
                        SetTextviewValues(minValue, maxValue)
                    }
                    rangeSeekbar.setOnClickListener(this)


                    rangeSeekbar.setOnRangeSeekbarFinalValueListener { minValue, maxValue ->
                        /*bottomSheetListener!!.selected(
                            minValue.toString(),
                            maxValue.toString(),
                            "By Price"
                        )*/
                        checkmaxValueandSet(minValue.toInt(),maxValue.toInt())
                    }
                }
            }

        }

     private fun checkmaxValueandSet(minValue: Int, maxValue: Int) {

         if(maxValue == 200|| maxValue == 50000)
         {
             bottomSheetListener!!.selected(
                     minValue.toString(),
                     (maxValue * 1000).toString(),
                     "By Price")
         }
         else {

             bottomSheetListener!!.selected(
                     minValue.toString(),
                     maxValue.toString(),
                     "By Price")
         }
     }

        private fun SetTextviewValues(minValue: Number?, maxValue: Number?) {
            tvMin.text = minValue.toString() + ""
            tvMax.text = maxValue.toString() + "+"
        }


        companion object {
            val TAG = "ByPriceGrainSeedFragment"

            fun newInstance(): ByPriceGrainSeedFragment {
                return ByPriceGrainSeedFragment()
            }
            fun newInstance(listener: FilterViewGrainSeedComman): ByPriceGrainSeedFragment {
                val fragment = ByPriceGrainSeedFragment()
                fragment.bottomSheetListener = listener
                return fragment
            }

            fun newInstance(listener: FilterViewKhedutComman): ByPriceGrainSeedFragment {
                val fragment = ByPriceGrainSeedFragment()
                fragment.bottomSheetListener = listener
                return fragment
            }
        }
        interface BottomSheetListener {
            fun selected(min: String, max: String, property: String)
        }

        override fun onClick(v: View?) {

        }

    }
