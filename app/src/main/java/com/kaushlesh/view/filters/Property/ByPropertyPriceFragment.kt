package com.kaushlesh.view.filters.Property
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kaushlesh.R
import android.widget.TextView
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.filters.Bikes.ByPriceBikesFragment


class ByPropertyPriceFragment : Fragment(), View.OnClickListener {
    private var bottomSheetListener: ByPropertyPriceFragment.BottomSheetListener? = null
    var byPriceMin: String? = null
    var byPriceMax: String? = null
    var cname: String? = null
    var cid: Int? = null
    internal lateinit var rangeSeekbar: CrystalRangeSeekbar
    internal lateinit var tvMin: TextView
    internal lateinit var tvMax: TextView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_by_price, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rangeSeekbar = view.findViewById(R.id.rangeSeekbar) as CrystalRangeSeekbar
        if (arguments != null) {
            cname = arguments!!.getString("ccname").toString()
            cid = arguments!!.getInt("cid")
            byPriceMin = arguments!!.getString("minPrice")
            byPriceMax = arguments!!.getString("maxPrice")

        }
        if (cid!!.equals(2)) {
            SetMinMaxValues(0F, 300000F)
        } else if (cid!!.equals(3)) {
            SetMinMaxValues(0F, 10000000F)
        } else if (cid!!.equals(4)) {
            SetMinMaxValues(0F, 100000F)
        } else if (cid!!.equals(5)) {
            SetMinMaxValues(0F, 10000000F)
        } else if (cid!!.equals(6)) {
            SetMinMaxValues(0F, 200000F)
        } else if (cid!!.equals(7)) {
            SetMinMaxValues(0F, 100000F)
        } else if (cid!!.equals(8)) {
            SetMinMaxValues(0F, 100000F)
        } else if (cid!!.equals(9)) {
            SetMinMaxValues(0F, 50000F)
        }

        tvMin = view.findViewById(R.id.tvmin) as TextView
        tvMax = view.findViewById(R.id.tvmax) as TextView

        if (arguments != null) {
            val minfound = arguments!!.getString("minPrice").toString()
            if (!minfound.equals("null")) {
                byPriceMin = arguments!!.getString("minPrice")
                byPriceMax = arguments!!.getString("maxPrice")

                if(!byPriceMin!!.isEmpty() && !byPriceMax!!.isEmpty()) {
                    rangeSeekbar.setMinStartValue(byPriceMin!!.toFloat()).setMaxStartValue(byPriceMax!!.toFloat()).apply()
                }
                rangeSeekbar.setOnRangeSeekbarChangeListener { minValue, maxValue ->
                    SetTextviewValues(minValue, maxValue)
                }
                rangeSeekbar.setOnClickListener(this)

                rangeSeekbar.setOnRangeSeekbarFinalValueListener { minValue, maxValue ->
                    Utils.showLog(TAG,"===SELECTED MAX VALUE 1===" + maxValue)
                    /*bottomSheetListener!!.selected(
                        minValue.toString(),
                        maxValue.toString(),
                        "By Price"
                    )*/
                    Utils.showLog(TAG,"==max value==" + maxValue)
                    checkmaxValueandSet(minValue.toInt(),maxValue.toInt())
                }

            } else {
                rangeSeekbar.setOnRangeSeekbarChangeListener { minValue, maxValue ->
                    SetTextviewValues(minValue, maxValue)
                }
                rangeSeekbar.setOnClickListener(this)


                rangeSeekbar.setOnRangeSeekbarFinalValueListener { minValue, maxValue ->
                   /* bottomSheetListener!!.selected(
                        minValue.toString(),
                        maxValue.toString(),
                        "By Price"
                    )*/
                    Utils.showLog(TAG,"==max value==" + maxValue)
                    checkmaxValueandSet(minValue.toInt(),maxValue.toInt())
                }

            }
        }
    }

    private fun checkmaxValueandSet(minValue: Int, maxValue: Int) {
        Utils.showLog(TAG,"==max value==" + maxValue)
        if(maxValue == 300000 || maxValue >= 10000000 || maxValue == 100000 || maxValue == 200000 || maxValue == 100000 || maxValue == 50000 || maxValue >= 30000000)
        {
            Utils.showLog(TAG,"==if max value==" + maxValue)
            bottomSheetListener!!.selected(
                    minValue.toString(),
                    (maxValue * 1000).toString(),
                    "By Price")
        }
        else {
            Utils.showLog(TAG,"==else max value==" + maxValue)
            bottomSheetListener!!.selected(
                    minValue.toString(),
                    maxValue.toString(),
                    "By Price")
        }
    }

    private fun SetTextviewValues(minValue: Number?, maxValue: Number?) {
        if (minValue == 10000000 || minValue!!.toInt() >= 10000000) {
            val minNum = minValue.toLong() / 10000000
            tvMin.text = minNum.toString() + " cr"
        } else {
            tvMin.text = minValue.toString() + ""
        }
        if (maxValue == 10000000 || maxValue!!.toInt() >= 10000000) {
            val maxNum = maxValue.toLong() / 10000000
            tvMax.text = maxNum.toString() + " cr+"
        } else {
            tvMax.text = maxValue.toString() + "+"
        }
    }

    private fun SetMinMaxValues(min: Float, max: Float) {
        rangeSeekbar.setMinValue(min)
        rangeSeekbar.setMaxValue(max)
    }

    override fun onClick(v: View?) {

    }

    companion object {
        val TAG = "ByPropertyPriceFragment"
        fun newInstance(): ByPropertyPriceFragment {
            return ByPropertyPriceFragment()
        }

        fun newInstance(listener: ByPropertyPriceFragment.BottomSheetListener): ByPropertyPriceFragment {
            val fragment = ByPropertyPriceFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }
    }

    interface BottomSheetListener {
        fun selected(min: String, max: String, property: String)
    }

}
