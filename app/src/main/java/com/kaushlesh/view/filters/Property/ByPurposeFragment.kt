package com.kaushlesh.view.filters.Property

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.kaushlesh.R
import com.kaushlesh.adapter.filter.FilterCommanAdapter
import com.kaushlesh.bean.FilterBean
import kotlinx.android.synthetic.main.fragment_by_fuel.*

class ByPurposeFragment : Fragment(),FilterCommanAdapter.ItemClickListener {
    var bottomSheetListener: ByPurposeFragment.BottomSheetListener? = null
    val list = ArrayList<FilterBean>()
    var cname  : String ?= null
    lateinit var adapter:FilterCommanAdapter
    var cid: Int? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_by_fuel, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null) {
            cname = arguments!!.getString("ccname").toString()
            cid = arguments!!.getInt("cid")
            if (arguments!!.getParcelableArrayList<Parcelable>("puposerlist")!!.isNotEmpty()) {
                list.clear()
                list.addAll(arguments!!.getParcelableArrayList<Parcelable>("puposerlist") as ArrayList<FilterBean>)
                bindRecyclerviewFuel()
            }else{
                bindRecyclerviewFuel()
            }

        }
    }

    private fun bindRecyclerviewFuel() {
        val recyclerLayoutManager = LinearLayoutManager(context)
        recyclerview.layoutManager = recyclerLayoutManager
        if (list.isNotEmpty()){
            adapter = FilterCommanAdapter(list, context!!)
        }
        else{
            adapter = FilterCommanAdapter(ownerList(), context!!)
        }
        adapter.setClicklistner(this)
        recyclerview.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    private fun ownerList(): List<FilterBean> {

        if(cid!!.equals(5) || cid!!.equals(8))
        {
            list.add(FilterBean("Rent",1))
            list.add(FilterBean("Sell",2))
        }
        else if(cid!!.equals(7) || cid!!.equals(6))
        {
            list.add(FilterBean("Booking",3))
            list.add(FilterBean("Sell",2))
        }

        return list
    }

    companion object {
        val TAG = "ByPurposeFragment"

        fun newInstance(listener: ByPurposeFragment.BottomSheetListener): ByPurposeFragment {
            val fragment = ByPurposeFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }

    }
    override fun itemClickAllBrand(position: Int) {
        bottomSheetListener!!.selected(list,"purpose")
    }
    interface BottomSheetListener {
        fun selected(purposeList:List<FilterBean>,property:String)
    }
}
