package com.kaushlesh.view.filters.Vehicle

import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.kaushlesh.R
import com.kaushlesh.adapter.filter.FilterCommanAdapter
import com.kaushlesh.adapter.filter.FilterPremiumCommanAdapter
import com.kaushlesh.bean.FilterBean
import com.kaushlesh.constant.AppConstants
import kotlinx.android.synthetic.main.fragment_by_fuel.*

class ByPremiumFragment : Fragment(),FilterCommanAdapter.ItemClickListener{
    var bottomSheetListener: ByPremiumFragment.BottomSheetListener? = null
    val list = ArrayList<FilterBean>()
    var cname  : String ?= null
    lateinit var adapter:FilterCommanAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_by_fuel, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (arguments != null) {
            cname = arguments!!.getString("ccname").toString()
            if (arguments!!.getParcelableArrayList<Parcelable>("propertyByPremiumList")!!.isNotEmpty()) {
                list.clear()
                list.addAll(arguments!!.getParcelableArrayList<Parcelable>("propertyByPremiumList") as ArrayList<FilterBean>)
                bindRecyclerviewFuel()
            }else{
                bindRecyclerviewFuel()
            }
            AppConstants.printLog(TAG, "==title==$cname")
        }
    }

    private fun bindRecyclerviewFuel() {

        val recyclerLayoutManager = LinearLayoutManager(context)
        recyclerview.layoutManager = recyclerLayoutManager
        if (list.isNotEmpty()){
            adapter = FilterCommanAdapter(list, context!!)
        }

        else{
            adapter = FilterCommanAdapter(ownerList(), context!!)
        }
        adapter.setClicklistner(this)
        recyclerview.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    private fun ownerList(): List<FilterBean> {

        list.add(FilterBean("Premium Ad",1))
        //list.add(FilterBean("General Ad",2))

        return list
    }

    companion object {
        val TAG = "ByPremiumFragment"

        fun newInstance(listener: BottomSheetListener): ByPremiumFragment {
            val fragment = ByPremiumFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }

      /*  fun newInstance(listener: FilterViewMobileComman): ByPremiumFragment {
            val fragment = ByPremiumFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }
        fun newInstance(listener: FilterViewComman): ByPremiumFragment {
            val fragment = ByPremiumFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }
        fun newInstance(listener: FilterViewGeneral): ByPremiumFragment {
            val fragment = ByPremiumFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }
        fun newInstance(listener: FilterViewBikesComman): ByPremiumFragment {
            val fragment = ByPremiumFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }
  fun newInstance(listener: FilterViewVehicleComman): ByPremiumFragment {
            val fragment = ByPremiumFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }*/

        fun newInstance(): ByPremiumFragment {
            val fragment = ByPremiumFragment()
            return fragment
        }
    }

    /*override fun itemClickAllBrand(position: Int) {
        bottomSheetListener!!.selected(list,"Premium Ad")

    }*/

    override fun itemClickAllBrand(position: Int) {
        for (i in 0 until list.size){
            if (position!=i){
                list[i].selected=false
            }
            adapter.notifyDataSetChanged()
        }
        bottomSheetListener!!.selected(list,"Premium Ad")

    }
    interface BottomSheetListener {
        fun selected(premiumList:List<FilterBean>,property:String)
    }
}
