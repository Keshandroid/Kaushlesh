package com.kaushlesh.view.filters.Vehicle

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager

import com.kaushlesh.R
import com.kaushlesh.adapter.filter.FilterCommanAdapter
import com.kaushlesh.bean.FilterBean
import kotlinx.android.synthetic.main.fragment_by_fuel.*

class ByFuelFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_by_fuel, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindRecyclerviewFuel()
    }

    private fun bindRecyclerviewFuel() {

        val recyclerLayoutManager = LinearLayoutManager(context)
        recyclerview.layoutManager = recyclerLayoutManager
        val adapter = FilterCommanAdapter(ownerList(), context!!)
        recyclerview.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    private fun ownerList(): List<FilterBean> {
        val list = ArrayList<FilterBean>()

        list.add(FilterBean("CNG & Hybrid",1))
        list.add(FilterBean("Diesel",3))
        list.add(FilterBean("Petrol",2))
        list.add(FilterBean("LPG",4))
        list.add(FilterBean("Electric",5))

        return list
    }

    companion object {
        val TAG = "ByFuelFragment"

        fun newInstance(): ByFuelFragment {
            return ByFuelFragment()
        }
    }
}
