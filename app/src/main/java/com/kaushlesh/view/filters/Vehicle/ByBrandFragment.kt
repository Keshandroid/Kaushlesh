package com.kaushlesh.view.filters.Vehicle

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.kaushlesh.R
import com.kaushlesh.adapter.filter.Vehicle.AllBrandAdapter
import com.kaushlesh.adapter.filter.Vehicle.ByBrandAdapter
import com.kaushlesh.bean.BrandShowCaseBean
import com.kaushlesh.bean.SubCategoryBean
import kotlinx.android.synthetic.main.fragment_by_brand.*
import java.util.ArrayList

class ByBrandFragment : Fragment() /*,ByBrandAdapter.ItemClickListener*/{


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_by_brand, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindRecyclerviewPopularBrands()

        bindRecyclerviewAllBrand()
    }

    private fun bindRecyclerviewAllBrand() {

        val recyclerLayoutManager = LinearLayoutManager(context)
        rvallbrands.layoutManager = recyclerLayoutManager
        val adapter = AllBrandAdapter(allBrandList(), context!!)
        rvallbrands.adapter = adapter
        rvallbrands.setNestedScrollingEnabled(false);
        adapter.notifyDataSetChanged()
    }

    private fun allBrandList(): List<SubCategoryBean> {
        val list = ArrayList<SubCategoryBean>()

        list.add(SubCategoryBean("Mercedes"))
        list.add(SubCategoryBean("BMW"))
        list.add(SubCategoryBean("Audi"))
        list.add(SubCategoryBean("Porche"))
        list.add(SubCategoryBean("Toyota"))
        list.add(SubCategoryBean("Tata"))
        list.add(SubCategoryBean("Kia"))
        list.add(SubCategoryBean("Renualt"))
        list.add(SubCategoryBean("Nissan"))
        list.add(SubCategoryBean("Jeep"))
        list.add(SubCategoryBean("Others"))

        return list
    }

    private fun bindRecyclerviewPopularBrands() {

        val layoutManager = GridLayoutManager(context, 3)
        rvpopbrands.layoutManager = layoutManager
        val adapter = ByBrandAdapter(popularBrandList(), context!!)
        //adapter.setClicklistner(this)
        rvpopbrands.adapter = adapter
        rvpopbrands.setNestedScrollingEnabled(false);
        adapter.notifyDataSetChanged()
    }

    private fun popularBrandList(): List<BrandShowCaseBean> {

        val list = ArrayList<BrandShowCaseBean>()

        list.add(BrandShowCaseBean(R.drawable.ic_maruti, "Maruti Suzuki"))
        list.add(BrandShowCaseBean(R.drawable.ic_huindai, "Hyundai"))
        list.add(BrandShowCaseBean(R.drawable.ic_voxwegan, "Voxwegan"))
        list.add(BrandShowCaseBean(R.drawable.ic_scoda, "Skoda"))
        list.add(BrandShowCaseBean(R.drawable.ic_honda, "Honda"))
        list.add(BrandShowCaseBean(R.drawable.ic_mahindra, "Mahindra"))

        return list
    }

    companion object {
        val TAG = "ByBrandFragment"

        fun newInstance(): ByBrandFragment {
            return ByBrandFragment()
        }
    }

/*
    override fun itemclickBrand(position: Int) {
        TODO("Not yet implemented")
    }
*/

}
