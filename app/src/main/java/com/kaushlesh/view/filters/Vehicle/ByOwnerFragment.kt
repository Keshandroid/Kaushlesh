package com.kaushlesh.view.filters.Vehicle

import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager

import com.kaushlesh.R
import com.kaushlesh.adapter.filter.FilterCommanAdapter
import com.kaushlesh.bean.FilterBean
import kotlinx.android.synthetic.main.fragment_by_owner.*

class ByOwnerFragment : Fragment(), FilterCommanAdapter.ItemClickListener {

    var cname: String? = null
    var cid: Int? = null
    val list = ArrayList<FilterBean>()
    lateinit var adapter: FilterCommanAdapter

    private var bottomSheetListener: ByOwnerFragment.BottomSheetListener? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_by_owner, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (arguments != null) {
            cname = arguments!!.getString("ccname").toString()
            cid = arguments!!.getInt("cid")
            if (arguments!!.getParcelableArrayList<Parcelable>("ByOwnerTypeList")!!
                    .isNotEmpty()
            ) {
                list.clear()
                list.addAll(arguments!!.getParcelableArrayList<Parcelable>("ByOwnerTypeList") as ArrayList<FilterBean>)
                bindRecyclerviewOwners()
            } else {
                bindRecyclerviewOwners()
            }
        }
    }

    private fun bindRecyclerviewOwners() {
        val recyclerLayoutManager = LinearLayoutManager(context)
        rvowner.layoutManager = recyclerLayoutManager

        if (list.isNotEmpty()) {
            adapter = FilterCommanAdapter(list, context!!)
        } else {
            adapter = FilterCommanAdapter(ownerList(), context!!)
        }
        adapter.setClicklistner(this)

        rvowner.adapter = adapter
        adapter.notifyDataSetChanged()

       /* val recyclerLayoutManager = LinearLayoutManager(context)
        rvowner.layoutManager = recyclerLayoutManager
        val adapter = FilterCommanAdapter(ownerList(), context!!)
        rvowner.adapter = adapter
        adapter.notifyDataSetChanged()*/
    }

    private fun ownerList(): List<FilterBean> {

        list.add(FilterBean("1st",1))
        list.add(FilterBean("2nd",2))
        list.add(FilterBean("3rd",3))
        list.add(FilterBean("4th",4))
        list.add(FilterBean("4th +",5))

        return list
    }

    companion object {
        val TAG = "ByOwnerFragment"

        fun newInstance(): ByOwnerFragment {
            return ByOwnerFragment()
        }

        fun newInstance(listener: ByOwnerFragment.BottomSheetListener): ByOwnerFragment {
            val fragment = ByOwnerFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }
    }

    interface BottomSheetListener {
        fun selected(typesList: List<FilterBean>, property: String)
    }

    override fun itemClickAllBrand(position: Int) {
        Log.e(ByTypeFragment.TAG, "selected: " + list[position].selected)
        bottomSheetListener!!.selected(list, "Owner")
    }
}
