package com.kaushlesh.view.filters.HomeSearch

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import com.kaushlesh.R
import com.kaushlesh.bean.HomeSearchBean

class HomeProductArrayAdapter(private val mContext: Context, private val mLayoutResourceId: Int,
                              departments: List<HomeSearchModel>) :
        ArrayAdapter<HomeSearchModel>(mContext, mLayoutResourceId, departments) {

    var mDepartments = mutableListOf<HomeSearchModel>()
    var mDepartmentsAll: List<HomeSearchModel>

    override fun getCount(): Int {
        return mDepartments.size
    }

    override fun getItem(position: Int): HomeSearchModel {
        return mDepartments[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        try {
            if (convertView == null) {
                val inflater = (mContext as Activity).layoutInflater
                convertView = inflater.inflate(mLayoutResourceId, parent, false)
            }
            val department: HomeSearchModel = getItem(position)
            val name = convertView!!.findViewById<View>(R.id.txtItemSearch) as TextView
            name.setText(department.filterCategoryName)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return convertView!!
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun convertResultToString(resultValue: Any): String {
                return (resultValue as HomeSearchModel).filterCategoryName.toString()
            }

            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val filterResults = FilterResults()
                val departmentsSuggestion: MutableList<HomeSearchModel> = ArrayList()
                if (constraint != null) {
                    for (department in mDepartmentsAll) {
                        if (department.filterCategoryName!!.toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                            departmentsSuggestion.add(department)
                        }
                    }
                    filterResults.values = departmentsSuggestion
                    filterResults.count = departmentsSuggestion.size
                }
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                mDepartments.clear()
                if (results != null && results.count > 0) {
                    // avoids unchecked cast warning when using mDepartments.addAll((ArrayList<HomeSearchModel>) results.values);
                    for (`object` in results.values as List<*>) {
                        if (`object` is HomeSearchModel) {
                            mDepartments.add(`object` as HomeSearchModel)
                        }
                    }
                    notifyDataSetChanged()
                } else if (constraint == null) {
                    // no filter, add entire original list back in
                    mDepartments.addAll(mDepartmentsAll)
                    notifyDataSetInvalidated()
                }
            }
        }
    }


    init {
        mDepartments = ArrayList<HomeSearchModel>(departments)
        mDepartmentsAll = ArrayList<HomeSearchModel>(departments)
    }
}

/*
class HomeProductArrayAdapter(
        private val mContext: Context,
        private val mLayoutResourceId: Int,
        departments: List<HomeSearchModel>) :
        ArrayAdapter<HomeSearchModel>(mContext, mLayoutResourceId, departments) {
    private val mDepartments: MutableList<HomeSearchModel>
    private var mDepartmentsAll: List<HomeSearchModel>
    override fun getCount(): Int {
        return mDepartments.size
    }

    override fun getItem(position: Int): HomeSearchModel {
        return mDepartments[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        try {
            if (convertView == null) {
                val inflater: LayoutInflater = (mContext as Activity).getLayoutInflater()
                convertView = inflater.inflate(mLayoutResourceId, parent, false)
            }
            val department: HomeSearchModel = getItem(position)
            val name: TextView = convertView?.findViewById<View>(R.id.txtItemSearch) as TextView
            name.setText(department.name)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return convertView!!
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun convertResultToString(resultValue: Any): String {
                return (resultValue as HomeSearchModel).name.toString()
            }

            override fun performFiltering(constraint: CharSequence): FilterResults {
                val filterResults = FilterResults()
                val departmentsSuggestion: MutableList<HomeSearchModel> = ArrayList()
                if (constraint != null) {
                    for (department in mDepartmentsAll) {
                        if (department.name?.toLowerCase()?.startsWith(constraint.toString().toLowerCase())!!) {
                            departmentsSuggestion.add(department)
                        }
                    }
                    filterResults.values = departmentsSuggestion
                    filterResults.count = departmentsSuggestion.size
                }
                return filterResults
            }

            override fun publishResults(constraint: CharSequence, results: FilterResults?) {
                mDepartments.clear()

                if (results != null && results.count > 0) {
                    // avoids unchecked cast warning when using mDepartments.addAll((ArrayList<HomeSearchModel>) results.values);
                    for (`object` in results.values as List<*>) {
                        if (`object` is HomeSearchModel) {
                            mDepartments.add(`object` as HomeSearchModel)
                        }
                    }
                    notifyDataSetChanged()
                } else if (constraint == null) {
                    // no filter, add entire original list back in
                    mDepartments.addAll(mDepartmentsAll)
                    notifyDataSetInvalidated()
                }
            }
        }
    }

    init {
        mDepartments = ArrayList(departments)
        mDepartmentsAll = ArrayList<HomeSearchModel>(departments)
    }
}*/
