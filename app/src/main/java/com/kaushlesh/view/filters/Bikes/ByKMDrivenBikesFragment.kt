package com.kaushlesh.view.filters.Bikes

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.kaushlesh.R
import com.yahoo.mobile.client.android.util.rangeseekbar.RangeSeekBar
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener
import android.widget.TextView
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar
import com.kaushlesh.view.filters.FilterViewBikesComman
import com.kaushlesh.view.filters.Mobile.ByPriceMobileFragment


class ByKMDrivenBikesFragment : Fragment(), View.OnClickListener {
    var byPriceMin: String? = null
    var byPriceMax: String? = null
    internal lateinit var rangeSeekbar: CrystalRangeSeekbar
    internal lateinit var tvMin: TextView
    internal lateinit var tvMax: TextView
    var cid: Int? = null
    var cname: String? = null
    private var bottomSheetListener: ByKMDrivenBikesFragment.BottomSheetListener? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_by_km_driven, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rangeSeekbar = view.findViewById(R.id.rangeSeekbar) as CrystalRangeSeekbar

        tvMin = view.findViewById(R.id.tvmin) as TextView
        tvMax = view.findViewById(R.id.tvmax) as TextView

        rangeSeekbar.setMinValue(0F)
        rangeSeekbar.setMaxValue(50000F)

        if (arguments != null) {
            val minfound = arguments!!.getString("minPrice").toString()
            if (!minfound.equals("null")) {
                byPriceMin = arguments!!.getString("minPrice")
                byPriceMax = arguments!!.getString("maxPrice")

                if(!byPriceMin!!.isEmpty() && !byPriceMax!!.isEmpty()) {
                    rangeSeekbar.setMinStartValue(byPriceMin!!.toFloat()).setMaxStartValue(byPriceMax!!.toFloat()).apply()
                }

                rangeSeekbar.setOnRangeSeekbarChangeListener { minValue, maxValue ->
                    SetTextviewValues(minValue, maxValue)
                }
                rangeSeekbar.setOnClickListener(this)

                rangeSeekbar.setOnRangeSeekbarFinalValueListener { minValue, maxValue ->
                    //bottomSheetListener!!.selected(minValue.toString(), maxValue.toString(), "By Drive")
                    checkmaxValueandSet(minValue.toInt(),maxValue.toInt())
                }

            } else {
                rangeSeekbar.setOnRangeSeekbarChangeListener { minValue, maxValue ->
                    SetTextviewValues(minValue, maxValue)
                }
                rangeSeekbar.setOnClickListener(this)


                rangeSeekbar.setOnRangeSeekbarFinalValueListener { minValue, maxValue ->
                 /*   bottomSheetListener!!.selected(
                        minValue.toString(),
                        maxValue.toString(),
                        "By Drive"
                    )*/
                    checkmaxValueandSet(minValue.toInt(),maxValue.toInt())
                }
            }
        }

    }

    private fun checkmaxValueandSet(minValue: Int, maxValue: Int) {
        if(maxValue == 50000)
        {
            bottomSheetListener!!.selected(
                    minValue.toString(),
                    (maxValue * 1000).toString(),
                    "By Drive"
            )
        }
        else {

            bottomSheetListener!!.selected(
                    minValue.toString(),
                    maxValue.toString(),
                    "By Drive"
            )
        }
    }

    private fun SetTextviewValues(minValue: Number?, maxValue: Number?) {
        tvMin.text = minValue.toString() + ""
        tvMax.text = maxValue.toString() + "+"
    }


    companion object {
        val TAG = "ByKMDrivenBikesFragment"

        fun newInstance(): ByKMDrivenBikesFragment {
            return ByKMDrivenBikesFragment()
        }

        fun newInstance(listener: ByKMDrivenBikesFragment.BottomSheetListener?): ByKMDrivenBikesFragment {
            val fragment = ByKMDrivenBikesFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }
    }

    interface BottomSheetListener {
        fun selected(min: String, max: String, property: String)
    }

    override fun onClick(v: View?) {
    }
}
