package com.kaushlesh.view.filters

import android.app.Activity
import android.content.res.Resources
import android.os.Bundle
import android.os.Parcelable
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.kaushlesh.R
import kotlinx.android.synthetic.main.filter_comman_view.*
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.kaushlesh.adapter.FilterAdapter
import com.kaushlesh.bean.FilterBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.view.filters.Vehicle.*
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.bean.Filter.FilterPropertyBean
import com.kaushlesh.bean.MobileBrandBeans
import com.kaushlesh.bean.ProductListHouseBean
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.CommanPropertyTypeListFilter
import com.kaushlesh.utils.FiltersSaveRemoveData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.filters.Bikes.ByFuelBikesFragment
import com.kaushlesh.view.filters.Bikes.ByYearBikesFragment
import com.kaushlesh.view.filters.Mobile.ByBrandMobileFragment
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class FilterViewVehicleComman : BottomSheetDialogFragment(), FilterAdapter.ItemClickListener,
    ByFuelBikesFragment.BottomSheetListener, ByTypeFragment.BottomSheetListener,
    ByPriceFragment.BottomSheetListener, ByKMDrivenFragment.BottomSheetListener,
    ParseControllerListener, ByPremiumFragment.BottomSheetListener,
    ByOwnerFragment.BottomSheetListener,
    ByBrandMobileFragment.BottomSheetListener, ByYearBikesFragment.BottomSheetListener {
    var VehicalTypesList = ArrayList<FilterBean>()

    var cId: Int? = null
    var categoryId: Int? = null
    var url: String? = null
    var TAG = FilterViewVehicleComman::class.java.simpleName
    private var bottomSheetListener: BottomSheetListener? = null
    var cname: String? = null
    var plist: ArrayList<ProductListHouseBean.ProductListHouse> = ArrayList()
    var byPriceMin: String? = null
    var byPriceMax: String? = null
    var byDriveMax: String? = null
    var byDriveMin: String? = null
    var byYearMax: String? = null
    var byYearMin: String? = null
    var propertyByPremiumList = ArrayList<FilterBean>()
    var BrandShowCaseBeanList = ArrayList<MobileBrandBeans.MobileBrandBeansList>()
    var ByFuelList = ArrayList<FilterBean>()
    var ByOwnerTypeList = ArrayList<FilterBean>()

    companion object {
        val TAG = "FilterViewVehicleComman"
        fun newInstance(
            listener: BottomSheetListener, name: String, id: Int, url: String, categoryId: Int
        ): FilterViewVehicleComman {
            val fragment = FilterViewVehicleComman()
            fragment.bottomSheetListener = listener
            fragment.cname = name
            fragment.cname = name
            fragment.cId = id
            fragment.url = url
            fragment.categoryId = categoryId
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.filter_comman_view, container, false)
        return v

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        AppConstants.printLog(TAG, "==title==$cname")
        tv_cate_name.setText(cname)
        val coordinatorLayout = view.findViewById(R.id.cl_main) as CoordinatorLayout
        val bottomSheet = coordinatorLayout.findViewById(R.id.rl_bottom) as RelativeLayout

      /*  val behavior = BottomSheetBehavior.from(bottomSheet)
        behavior.isHideable = true
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED)
        behavior.peekHeight = Resources.getSystem().getDisplayMetrics().heightPixels
        //behavior.peekHeight = 1000
        bottomSheet.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT*/

        val offsetFromTop = 10
        (dialog as? BottomSheetDialog)?.behavior?.apply {
            isFitToContents = false
            setExpandedOffset(offsetFromTop)
            state = BottomSheetBehavior.STATE_EXPANDED
        }

        bindRecyclerview()

        btn_apply.setOnClickListener {

            lateinit var filterBeanDataModel: FilterPropertyBean
            filterBeanDataModel = FilterPropertyBean()

            filterBeanDataModel.categoryId = categoryId.toString()
            filterBeanDataModel.subCategoryId = cId.toString()
            filterBeanDataModel.property_byPremium_list = addSelectedDataPrimumsend(propertyByPremiumList, "id")
            filterBeanDataModel.fuel = addSelectedData(ByFuelList, "id")
            filterBeanDataModel.property_listedby_list = addSelectedData(ByOwnerTypeList, "id")
            filterBeanDataModel.property_type_list = addSelectedData(VehicalTypesList, "id")
            filterBeanDataModel.brand_id = addSelectedDataadd(BrandShowCaseBeanList, "brandId")

            //types

            //premimum
            val data = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "5")
            if (!propertyByPremiumList.isEmpty()) {

                filterBeanDataModel.property_byPremium_list =addSelectedDataPrimumsend(propertyByPremiumList, "id")

            }
            else {
                if (data != null) {
                    propertyByPremiumList.clear()
                    propertyByPremiumList.addAll(data as Collection<FilterBean>)
                    filterBeanDataModel.property_byPremium_list = addSelectedDataPrimumsend(propertyByPremiumList, "id")

                } else {
                    propertyByPremiumList.clear()
                    filterBeanDataModel.property_byPremium_list = arrayListOf()
                    propertyByPremiumList = arrayListOf()
                }
            }

            //fuel
            val datafuel = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "2")
            if (!ByFuelList.isEmpty()) {

                filterBeanDataModel.fuel = addSelectedData(ByFuelList, "id")
            }
            else {
                if (datafuel != null) {
                    ByFuelList.clear()
                    ByFuelList.addAll(datafuel as Collection<FilterBean>)
                    filterBeanDataModel.fuel =  addSelectedData(ByFuelList, "id")

                } else {
                    ByFuelList.clear()
                    filterBeanDataModel.fuel = arrayListOf()
                    ByFuelList = arrayListOf()
                }
            }

            //listedby
            val datalistedby = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "3")
            Utils.showLog(TAG, "==listed by==" + addSelectedData(ByOwnerTypeList, "id"))
            Utils.showLog(TAG, "==listed by saved==" + data)
            if (!ByOwnerTypeList.isEmpty()) {

                filterBeanDataModel.property_listedby_list = addSelectedData(ByOwnerTypeList, "id")

            }
            else {
                if (datalistedby != null) {
                    ByOwnerTypeList.clear()
                    ByOwnerTypeList.addAll(datalistedby as Collection<FilterBean>)
                    filterBeanDataModel.property_listedby_list = addSelectedData(ByOwnerTypeList, "id")
                    // FiltersSaveRemoveData.saveFilterDataList(propertyListedByList, categoryId.toString(), cId.toString(), "2")
                } else {
                    ByOwnerTypeList.clear()
                    filterBeanDataModel.property_listedby_list = arrayListOf()
                    ByOwnerTypeList = arrayListOf()
                    //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")
                    //FiltersSaveRemoveData.saveFilterDataList(propertyListedByList, categoryId.toString(), cId.toString(), "2")
                }
            }

            //types
            val datatype = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "4")
            Utils.showLog(TAG, "==property type==" + addSelectedData(VehicalTypesList, "id"))
            Utils.showLog(TAG, "==property type saved==" + datatype)
            if (!VehicalTypesList.isEmpty()) {

                filterBeanDataModel.property_type_list = addSelectedData(VehicalTypesList, "id")

            }
            else {
                if (datatype != null) {
                    VehicalTypesList.clear()
                    VehicalTypesList.addAll(datatype as Collection<FilterBean>)
                    filterBeanDataModel.property_type_list = addSelectedData(VehicalTypesList, "id")

                } else {
                    VehicalTypesList.clear()
                    filterBeanDataModel.property_type_list = arrayListOf()
                    VehicalTypesList = arrayListOf()
                }
            }

            //brand
            val databrand =FiltersSaveRemoveData.getFilterDataBrand(categoryId.toString(), cId.toString(), "1")
            if (!BrandShowCaseBeanList.isEmpty()) {

                filterBeanDataModel.brand_id =addSelectedDataadd(BrandShowCaseBeanList, "brandId")
            }
            else {
                if (databrand != null) {
                    BrandShowCaseBeanList.clear()
                    BrandShowCaseBeanList.addAll(databrand as Collection<MobileBrandBeans.MobileBrandBeansList>)
                    filterBeanDataModel.brand_id = addSelectedDataadd(BrandShowCaseBeanList, "brandId")

                } else {
                    BrandShowCaseBeanList.clear()
                    filterBeanDataModel.brand_id = arrayListOf()
                    BrandShowCaseBeanList = arrayListOf()
                }
            }

            //range filter

          /*  if (byPriceMin != null) {
                filterBeanDataModel.minPrice = byPriceMin
                FiltersSaveRemoveData.savePriceFilter(byPriceMin!!,categoryId.toString(),cId.toString(),"p1")
            } else {
                filterBeanDataModel.minPrice = ""
                FiltersSaveRemoveData.savePriceFilter(byPriceMin.toString(),categoryId.toString(),cId.toString(),"p1")
            }
            if (byPriceMax != null) {
                filterBeanDataModel.maxPrice = byPriceMax
                FiltersSaveRemoveData.savePriceFilter(byPriceMax!!,categoryId.toString(),cId.toString(),"p2")
            } else {
                filterBeanDataModel.maxPrice = ""
                FiltersSaveRemoveData.savePriceFilter(byPriceMax.toString(),categoryId.toString(),cId.toString(),"p2")
            }*/
            val lowPrice= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p1")
            val highPrice= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p2")
            Utils.showLog(TAG, "==saved price1==" + lowPrice)
            Utils.showLog(TAG, "==saved price2==" + highPrice)

            if (byPriceMin != null) {
                filterBeanDataModel.minPrice = byPriceMin
                //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")

                FiltersSaveRemoveData.savePriceFilter(byPriceMin!!, categoryId.toString(), cId.toString(), "p1")
            }
            else {
                if (byPriceMin.toString().equals("null")) {
                    filterBeanDataModel.minPrice = lowPrice
                    byPriceMin = lowPrice
                    FiltersSaveRemoveData.savePriceFilter(byPriceMin.toString(), categoryId.toString(), cId.toString(), "p1")
                } else {
                    filterBeanDataModel.minPrice = ""

                    //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")
                    FiltersSaveRemoveData.savePriceFilter("", categoryId.toString(), cId.toString(), "p1")
                }
            }

            if (byPriceMax != null) {
                filterBeanDataModel.maxPrice = byPriceMax
                //savePriceFilter(byPriceMax,categoryId.toString(),cId.toString(),"p2")

                FiltersSaveRemoveData.savePriceFilter(byPriceMax!!, categoryId.toString(), cId.toString(), "p2")
            }
            else {
                if (byPriceMax.toString().equals("null")) {
                    filterBeanDataModel.maxPrice = highPrice
                    byPriceMax = highPrice
                    FiltersSaveRemoveData.savePriceFilter(byPriceMax.toString(), categoryId.toString(), cId.toString(), "p2")
                } else {
                    filterBeanDataModel.maxPrice = ""
                    //savePriceFilter(byPriceMax,categoryId.toString(),cId.toString(),"p2")

                    FiltersSaveRemoveData.savePriceFilter("", categoryId.toString(), cId.toString(), "p2")
                }
            }

         /*   if (byDriveMin != null) {
                filterBeanDataModel.minkm_driven = byDriveMin
                FiltersSaveRemoveData.savePriceFilter(byDriveMin!!,categoryId.toString(),cId.toString(),"d1")
            } else {
                filterBeanDataModel.minkm_driven = ""
                FiltersSaveRemoveData.savePriceFilter(byDriveMin.toString(),categoryId.toString(),cId.toString(),"d1")
            }
            if (byDriveMax != null) {
                filterBeanDataModel.maxkm_driven = byDriveMax
                FiltersSaveRemoveData.savePriceFilter(byDriveMax!!,categoryId.toString(),cId.toString(),"d2")
            } else {
                filterBeanDataModel.maxkm_driven = ""
                FiltersSaveRemoveData.savePriceFilter(byDriveMax.toString(),categoryId.toString(),cId.toString(),"d2")
            }*/

            val lowdrive= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "d1")
            val highdrive= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "d2")
            if (byDriveMin != null) {
                filterBeanDataModel.minkm_driven = byDriveMin
                //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")

                FiltersSaveRemoveData.savePriceFilter(byDriveMin!!, categoryId.toString(), cId.toString(), "d1")
            }
            else {
                if (byDriveMin.toString().equals("null")) {
                    filterBeanDataModel.minkm_driven = lowdrive
                    byDriveMin = lowdrive
                    FiltersSaveRemoveData.savePriceFilter(byDriveMin.toString(), categoryId.toString(), cId.toString(), "d1")
                } else {
                    filterBeanDataModel.minkm_driven = ""

                    //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")
                    FiltersSaveRemoveData.savePriceFilter("", categoryId.toString(), cId.toString(), "d1")
                }
            }

            if (byDriveMax != null) {
                filterBeanDataModel.maxkm_driven = byDriveMax
                //savePriceFilter(byPriceMax,categoryId.toString(),cId.toString(),"p2")

                FiltersSaveRemoveData.savePriceFilter(byDriveMax!!, categoryId.toString(), cId.toString(), "d2")
            }
            else {
                if (byDriveMax.toString().equals("null")) {
                    filterBeanDataModel.maxkm_driven = highdrive
                    byDriveMax = highdrive
                    FiltersSaveRemoveData.savePriceFilter(byDriveMax.toString(), categoryId.toString(), cId.toString(), "d2")
                } else {
                    filterBeanDataModel.maxkm_driven = ""
                    //savePriceFilter(byPriceMax,categoryId.toString(),cId.toString(),"p2")

                    FiltersSaveRemoveData.savePriceFilter("", byDriveMax.toString(), cId.toString(), "d2")
                }
            }

           /*
            if (byYearMin != null) {
                filterBeanDataModel.minbike_year = byYearMin
                FiltersSaveRemoveData.savePriceFilter(byYearMin!!,categoryId.toString(),cId.toString(),"y1")
            } else {
                filterBeanDataModel.minbike_year = ""
                FiltersSaveRemoveData.savePriceFilter(byYearMin.toString(),categoryId.toString(),cId.toString(),"y1")
            }
            if (byYearMax != null) {
                filterBeanDataModel.maxbike_year = byYearMax
                FiltersSaveRemoveData.savePriceFilter(byYearMax!!,categoryId.toString(),cId.toString(),"y2")
            } else {
                filterBeanDataModel.maxbike_year = ""
                FiltersSaveRemoveData.savePriceFilter(byYearMax.toString(),categoryId.toString(),cId.toString(),"y2")
            }*/

            val lowyear = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "y1")
            val highyear = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "y2")

            if (byYearMin != null) {
                filterBeanDataModel.minbike_year = byYearMin
                //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")

                FiltersSaveRemoveData.savePriceFilter(byYearMin!!, categoryId.toString(), cId.toString(), "y1")
            }
            else {
                if (byYearMin.toString().equals("null")) {
                    filterBeanDataModel.minbike_year = lowyear
                    byYearMin = lowyear
                    FiltersSaveRemoveData.savePriceFilter(byYearMin.toString(), categoryId.toString(), cId.toString(), "y1")
                } else {
                    filterBeanDataModel.minbike_year = ""

                    //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")
                    FiltersSaveRemoveData.savePriceFilter("", categoryId.toString(), cId.toString(), "y1")
                }
            }

            if (byYearMax != null) {
                filterBeanDataModel.maxbike_year = byYearMax
                //savePriceFilter(byPriceMax,categoryId.toString(),cId.toString(),"p2")

                FiltersSaveRemoveData.savePriceFilter(byYearMax!!, categoryId.toString(), cId.toString(), "y2")
            }
            else {
                if (byYearMax.toString().equals("null")) {
                    filterBeanDataModel.maxbike_year = highyear
                    byDriveMax = highyear
                    FiltersSaveRemoveData.savePriceFilter(byDriveMax.toString(), categoryId.toString(), cId.toString(), "y2")
                } else {
                    filterBeanDataModel.maxbike_year = ""
                    //savePriceFilter(byPriceMax,categoryId.toString(),cId.toString(),"p2")

                    FiltersSaveRemoveData.savePriceFilter("", byYearMax.toString(), cId.toString(), "y2")
                }
            }


            lateinit var getListingController: GetListingController
            getListingController = GetListingController(activity!!, this)
            //getListingController.callforDisplayfilterVehicalData(filterBeanDataModel, url)

            if (byYearMin.toString().isNotEmpty() && byYearMin.toString().length != 4) {
                Utils.showLog(TAG, "== if if min==" + byYearMin.toString().length)
                Utils.showToast(activity, resources.getString(R.string.txt_valid_year))
            }
            else if(byYearMax.toString().isNotEmpty() && byYearMax.toString().length != 4 && byYearMax.toString().startsWith("0"))
            {
                Utils.showLog(TAG, "== if if max==" + byYearMax.toString().length)
                Utils.showToast(activity, resources.getString(R.string.txt_valid_year))
            }else{

                if(byYearMin.toString().startsWith("0") || byYearMax.toString().startsWith("0"))
                {
                    Utils.showToast(activity, resources.getString(R.string.txt_valid_year))
                }

                else {
                    val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                    val curDate: String = sdf.format(Date())
                    val startyear = curDate.split("-")[0]
                    Utils.showLog(TAG, "==start =" + startyear + "===" + byYearMin)
                    if(byYearMin.toString().length !=0 && byYearMax.toString().length != 0) {
                        if (startyear.toInt() < byYearMin!!.toInt()) {
                            Utils.showToast(activity, resources.getString(R.string.txt_valid_year))
                        } else if (startyear.toInt() < byYearMax!!.toInt()) {
                            Utils.showToast(activity, resources.getString(R.string.txt_valid_year))
                        } else {

                            if (compareYear(byYearMin, byYearMax)) {

                                Utils.showLog(TAG, "==else =" + byYearMin + "===" + byYearMax)
                                bottomSheetListener!!.callApiWithFilter(filterBeanDataModel, url.toString())
                                dismiss()
                            } else {
                                Utils.showToast(activity, resources.getString(R.string.txt_valid_year))
                            }
                        }
                    }
                    else{
                        bottomSheetListener!!.callApiWithFilter(filterBeanDataModel, url.toString())
                        dismiss()
                    }
                }
            }

            //bottomSheetListener!!.callApiWithFilter(filterBeanDataModel,url.toString())
            //dismiss()

            FiltersSaveRemoveData.saveFilterDataListBrand(BrandShowCaseBeanList, categoryId.toString(), cId.toString(), "1")
            FiltersSaveRemoveData.saveFilterDataList(ByFuelList, categoryId.toString(), cId.toString(), "2")
            FiltersSaveRemoveData.saveFilterDataList(ByOwnerTypeList, categoryId.toString(), cId.toString(), "3")
            FiltersSaveRemoveData.saveFilterDataList(VehicalTypesList, categoryId.toString(), cId.toString(), "4")
            FiltersSaveRemoveData.saveFilterDataList(propertyByPremiumList, categoryId.toString(), cId.toString(), "5")
        }

        btn_clear.setOnClickListener {
            dismiss()
            lateinit var filterBeanDataModel: FilterPropertyBean
            filterBeanDataModel = FilterPropertyBean()
            filterBeanDataModel.categoryId = categoryId.toString()
            filterBeanDataModel.subCategoryId = cId.toString()
            lateinit var getListingController: GetListingController
            getListingController = GetListingController(activity!!, this)
            //getListingController.callforDisplayfilterVehicalData(filterBeanDataModel, url)

            bottomSheetListener!!.callApiWithFilter(filterBeanDataModel,url.toString())

            FiltersSaveRemoveData.cleardata(categoryId.toString(),cId.toString())

            Utils.showToast(context as Activity?,getString(R.string.txt_filter_reset_done))

        }

        iv_close.setOnClickListener {
            dismiss()
        }

        setdefaultFragment(cname)
    }

    private fun compareYear(byYearMin: String?, byYearMax: String?): Boolean {
        try {
            val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)

            val curDate: String = sdf.format(Date())
            Log.i(TAG, "current date" + curDate);

            val date11 = byYearMin +"-"+curDate.split("-")[1]+"-"+ curDate.split("-")[2]
            val date22 = byYearMax +"-"+curDate.split("-")[1]+"-"+ curDate.split("-")[2]

            Log.i(TAG, "date11" + date11)
            Log.i(TAG, "date22" + date22)
            val date1 = sdf.parse(date11)
            val date2 = sdf.parse(date22)

            if (date1.compareTo(date2) > 0) {
                Log.i(TAG, "Date1 is after Date2")
                return false
            } else if (date1.compareTo(date2) < 0) {
                Log.i(TAG, "Date1 is before Date2")
                return true
            } else if (date1.compareTo(date2) == 0) {
                Log.i(TAG, "Date1 is equal to Date2")
                return true
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return false
    }

    private fun setdefaultFragment(cname: String?) {
        val bundle = Bundle()
        bundle.putInt("catid", categoryId!!.toInt())
        bundle.putInt("subcatid", cId!!.toInt())
        if (cId==18) {

            val data=FiltersSaveRemoveData.getFilterDataBrand(categoryId.toString(),cId.toString(),"1")
            if (data.toString()=="null"){
                // Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            }else{

                BrandShowCaseBeanList.clear()
                BrandShowCaseBeanList.addAll(FiltersSaveRemoveData.getFilterDataBrand(categoryId.toString(), cId.toString(), "1") as Collection<MobileBrandBeans.MobileBrandBeansList>)
            }

            bundle.putParcelableArrayList(
                "ByBrand",
                BrandShowCaseBeanList as java.util.ArrayList<out Parcelable>
            )
            changeFilterFragment(
                ByBrandMobileFragment.newInstance(this),
                ByBrandMobileFragment.TAG,
                bundle,
                true
            )
        }
        if (cId==19 ||cId==20) {

            val data = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "4")
            //var data=getFilterData(categoryId.toString(), cId.toString(), "3")
            if (data.toString()=="null"){
                // Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            }else{
                VehicalTypesList.clear()
                VehicalTypesList.addAll(FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "4") as Collection<FilterBean>)
            }

            bundle.putParcelableArrayList(
                "VehicalTypesList",
                VehicalTypesList as java.util.ArrayList<out Parcelable>
            )
            changeFilterFragment(ByTypeFragment.newInstance(this), ByTypeFragment.TAG, bundle, true)
        }
        if(cId == 1001)
        {
            val data = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "5")
            //var data=getFilterData(categoryId.toString(), cId.toString(), "3")
            if (data.toString()=="null"){
                // Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            }else{
                propertyByPremiumList.clear()
                propertyByPremiumList.addAll(FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "5") as Collection<FilterBean>)
            }

            bundle.putParcelableArrayList(
                    "propertyByPremiumList",
                    propertyByPremiumList as java.util.ArrayList<out Parcelable>
            )
            changeFilterFragment(
                    ByPremiumFragment.newInstance(this),
                    ByPremiumFragment.TAG,
                    bundle,
                    true
            )
        }
    }

    private fun addSelectedDataPrimumsend(
        list: ArrayList<FilterBean>,
        typeGet: String
    ): ArrayList<String>? {
        val selectedList: ArrayList<String>? = arrayListOf()
        for (i in 0 until list.size) {
            if (list[i].selected) {
                if (list[i].id == 1) {
                    // selectedList!!.add("Premium")
                    selectedList!!.add("1")
                } else if (list[i].id == 2) {
                    //selectedList!!.add("Free")
                    selectedList!!.add("0")
                }
            }
        }
        return selectedList
    }

    private fun addSelectedData(
        list: ArrayList<FilterBean>,
        typeGet: String
    ): ArrayList<String>? {
        val selectedList: ArrayList<String>? = arrayListOf()
        for (i in 0 until list.size) {
            if (list[i].selected) {
                if (typeGet == "name") {
                    selectedList!!.add(list[i].name)
                } else {
                    selectedList!!.add(list[i].id.toString())
                }
            }
        }
        return selectedList
    }

    private fun addSelectedDataadd(
        list: ArrayList<MobileBrandBeans.MobileBrandBeansList>,
        typeGet: String
    ): ArrayList<String>? {
        val selectedList: ArrayList<String>? = arrayListOf()
        for (i in 0 until list.size) {
            if (list[i].selected!!) {
                selectedList!!.add(list[i].brandId.toString())
            }
        }
        return selectedList
    }


    private fun bindRecyclerview() {
        val list = ArrayList<FilterBean>()
        val comman = CommanPropertyTypeListFilter()
        val layoutManager = LinearLayoutManager(context)
        rv_filter.layoutManager = layoutManager
        val adapter = FilterAdapter(comman.VehicalList(list, cId), context!!)
        rv_filter.addItemDecoration(
            DividerItemDecoration(
                context,
                LinearLayout.VERTICAL
            ) as RecyclerView.ItemDecoration
        );
        adapter.setClicklistner(this)
        rv_filter.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun itemclickView(bean: FilterBean) {
        val bundle = Bundle()

        AppConstants.printLog(TAG, "==type==" + bean.name)
        bundle.putInt("catid", categoryId!!.toInt())
        bundle.putInt("subcatid", cId!!.toInt())
        bundle.putString("ccname", cname)
        bundle.putInt("cid", cId!!.toInt())
        if (bean.id == 1) {

            val data=FiltersSaveRemoveData.getFilterDataBrand(categoryId.toString(), cId.toString(), "1")
            if (data.toString()=="null"){
                // Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            }else{
                BrandShowCaseBeanList.clear()
                BrandShowCaseBeanList.addAll(FiltersSaveRemoveData.getFilterDataBrand(categoryId.toString(), cId.toString(), "1") as Collection<MobileBrandBeans.MobileBrandBeansList>)
            }

            bundle.putParcelableArrayList(
                "ByBrand",
                BrandShowCaseBeanList as java.util.ArrayList<out Parcelable>
            )
            changeFilterFragment(
                ByBrandMobileFragment.newInstance(this),
                ByBrandMobileFragment.TAG,
                bundle,
                true
            )
        }
        if (bean.id == 2) {

            val lowPrice= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p1")
            var highPrice= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p2")
            if (lowPrice.toString()=="null"){
                //  Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            }else{
                byPriceMin= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p1")
                byPriceMax= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p2")
            }

            bundle.putString("minPrice", byPriceMin)
            bundle.putString("maxPrice", byPriceMax)
            changeFilterFragment(
                ByPriceFragment.newInstance(this),
                ByPriceFragment.TAG,
                bundle,
                true
            )
        }
        if (bean.id == 3) {

            val lowPrice= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "d1")
            var highPrice= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "d2")
            if (lowPrice.toString()=="null"){
                //  Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            }else{
                byDriveMin= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "d1")
                byDriveMax= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "d2")
            }

            bundle.putString("minPrice", byDriveMin)
            bundle.putString("maxPrice", byDriveMax)
            changeFilterFragment(
                ByKMDrivenFragment.newInstance(this),
                ByKMDrivenFragment.TAG,
                bundle,
                true
            )
        }
        if (bean.id == 4) {

            val data = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "3")
            //var data=getFilterData(categoryId.toString(), cId.toString(), "3")
            if (data.toString()=="null"){
                // Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            }else{
                ByOwnerTypeList.clear()
                ByOwnerTypeList.addAll(FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "3") as Collection<FilterBean>)
            }

            bundle.putParcelableArrayList(
                "ByOwnerTypeList",
                ByOwnerTypeList as java.util.ArrayList<out Parcelable>
            )
            changeFilterFragment(
                ByOwnerFragment.newInstance(this),
                ByOwnerFragment.TAG,
                bundle,
                true
            )
        }
        if (bean.id == 5) {

            val lowPrice= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "y1")
            var highPrice= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "y2")
            if (lowPrice.toString()=="null"){
                //  Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            }else{
                byYearMin= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "y1")
                byYearMax= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "y2")
            }

            bundle.putString("minYear", byYearMin)
            bundle.putString("maxYear", byYearMax)
            changeFilterFragment(
                ByYearBikesFragment.newInstance(this),
                ByYearBikesFragment.TAG,
                bundle,
                true
            )
        }
        if (bean.id == 6) {

            val data = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "2")
            //var data=getFilterData(categoryId.toString(), cId.toString(), "3")
            if (data.toString()=="null"){
                // Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            }else{
                ByFuelList.clear()
                ByFuelList.addAll(FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "2") as Collection<FilterBean>)
            }


            bundle.putParcelableArrayList(
                "ByFuelList",
                ByFuelList as java.util.ArrayList<out Parcelable>
            )
            changeFilterFragment(
                ByFuelBikesFragment.newInstance(this),
                ByFuelBikesFragment.TAG,
                bundle,
                true
            )
        }

        if (bean.id == 7) {

            val data = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "5")
            //var data=getFilterData(categoryId.toString(), cId.toString(), "3")
            if (data.toString()=="null"){
                // Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            }else{
                propertyByPremiumList.clear()
                propertyByPremiumList.addAll(FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "5") as Collection<FilterBean>)
            }

            bundle.putParcelableArrayList(
                "propertyByPremiumList",
                propertyByPremiumList as java.util.ArrayList<out Parcelable>
            )
            changeFilterFragment(
                ByPremiumFragment.newInstance(this),
                ByPremiumFragment.TAG,
                bundle,
                true
            )
        }
        if (bean.id == 8) {

            val data = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "4")
            //var data=getFilterData(categoryId.toString(), cId.toString(), "3")
            if (data.toString()=="null"){
                // Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            }else{
                VehicalTypesList.clear()
                VehicalTypesList.addAll(FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "4") as Collection<FilterBean>)
            }

            bundle.putParcelableArrayList(
                "VehicalTypesList",
                VehicalTypesList as java.util.ArrayList<out Parcelable>
            )
            changeFilterFragment(ByTypeFragment.newInstance(this), ByTypeFragment.TAG, bundle, true)
        }
    }

    private fun changeFilterFragment(fragment: Fragment, tag: String, args: Bundle?, add: Boolean) {
        val transaction = childFragmentManager.beginTransaction()
        if (args != null) fragment.arguments = args
        transaction.replace(R.id.fl_lmain, fragment, tag)
        transaction.commit()
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (method == "Get Product") {
            Log.e(TAG, "onSuccess: " + ProductListHouseBean())
            plist.clear()
            val dataModel = ProductListHouseBean()
            dataModel.ProductListHouseBean(da)
            plist = dataModel.productlist
        }
        bottomSheetListener!!.onButtonClicked("Clicked", "apply", plist)
        dismiss()
    }

    interface BottomSheetListener {
        fun onButtonClicked(
            text: String,
            type: String,
            cname: ArrayList<ProductListHouseBean.ProductListHouse>
        )

        fun callApiWithFilter(filterBeanDataModel : FilterPropertyBean, url : String)
    }

    override fun onFail(msg: String, method: String) {
        bottomSheetListener!!.onButtonClicked("Clicked", msg, plist)
        dismiss()
    }

    override fun selected(typesList: List<FilterBean>, property: String) {
        if (property.equals("Premium Ad")) {
            addPropertyData(propertyByPremiumList, typesList)
        } else if (property.equals("By Fuel")) {
            addPropertyData(ByFuelList, typesList)
        } else if (property.equals("Type")) {
            addPropertyData(VehicalTypesList, typesList)
        }else if (property.equals("Owner")) {
            addPropertyData(ByOwnerTypeList, typesList)
        }
    }

    fun addPropertyData(
        listAdd: ArrayList<FilterBean>,
        typesList: List<FilterBean>
    ) {
        listAdd.clear()
        listAdd.addAll(typesList)
    }

    override fun selectedBrand(
        BrandCase: List<MobileBrandBeans.MobileBrandBeansList>,
        property: String
    ) {
        BrandShowCaseBeanList.clear()
        BrandShowCaseBeanList.addAll(BrandCase)

        savePremium()
    }


    override fun selected(min: String, max: String, property: String) {
        if (property.equals("By Price")) {
            byPriceMax = max
            byPriceMin = min
        } else if (property.equals("By Drive")) {
            byDriveMax = max
            byDriveMin = min
        } else if (property.equals("By Year")) {
            byYearMax = max
            byYearMin = min
        }

        savePremium()
    }

    private fun savePremium() {

        lateinit var filterBeanDataModel: FilterPropertyBean
        filterBeanDataModel = FilterPropertyBean()
        val data= FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "5")
        if (data.toString()=="null"){
            // Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            filterBeanDataModel.property_byPremium_list = addSelectedDataPrimumsend(propertyByPremiumList, "id")
            FiltersSaveRemoveData.saveFilterDataList(propertyByPremiumList, categoryId.toString(), cId.toString(), "5")
        }else{
            propertyByPremiumList.clear()
            propertyByPremiumList.addAll(FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "5") as Collection<FilterBean>)
            filterBeanDataModel.property_byPremium_list = addSelectedDataPrimumsend(propertyByPremiumList, "id")
            FiltersSaveRemoveData.saveFilterDataList(propertyByPremiumList, categoryId.toString(), cId.toString(), "5")
        }
    }
}
