package com.kaushlesh.view.filters.Property

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager

import com.kaushlesh.R
import com.kaushlesh.adapter.filter.FilterCommanAdapter
import com.kaushlesh.bean.FilterBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.view.filters.FilterViewPropertyComman
import kotlinx.android.synthetic.main.fragment_by_fuel.*

class ByBedroomFragment : Fragment(),FilterCommanAdapter.ItemClickListener {
    private var bottomSheetListener: ListedByFragment.BottomSheetListener? = null
    val list = ArrayList<FilterBean>()
    lateinit var adapter:FilterCommanAdapter
    var cname  : String ?= null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_by_fuel, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (arguments != null) {
            cname = arguments!!.getString("ccname").toString()
            if (arguments!!.getParcelableArrayList<Parcelable>("propertyByBedroomList")!!.isNotEmpty()) {
                list.clear()
                list.addAll(arguments!!.getParcelableArrayList<Parcelable>("propertyByBedroomList") as ArrayList<FilterBean>)
                bindRecyclerviewFuel()
            }else{
                bindRecyclerviewFuel()
            }
            AppConstants.printLog(TAG, "==title==$cname")
        }
    }

    private fun bindRecyclerviewFuel() {

        val recyclerLayoutManager = LinearLayoutManager(context)
        recyclerview.layoutManager = recyclerLayoutManager

        if (list.isNotEmpty()){
            adapter = FilterCommanAdapter(list, context!!)
        }
        else{
            adapter = FilterCommanAdapter(ownerList(), context!!)
        }

        adapter.setClicklistner(this)
        recyclerview.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    private fun ownerList(): List<FilterBean> {


        list.add(FilterBean("1 Bedroom",1))
        list.add(FilterBean("2 Bedroom",2))
        list.add(FilterBean("3 Bedroom",3))
        list.add(FilterBean("4 Bedroom",4))
        list.add(FilterBean("4+ Bedroom",5))

        return list
    }

    companion object {
        val TAG = "ByBedroomFragment"

        fun newInstance(listener: FilterViewPropertyComman): ByBedroomFragment {
            val fragment = ByBedroomFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }
    }

    override fun itemClickAllBrand(position: Int) {
        bottomSheetListener!!.selected(list,"By Bedroom")
        Log.e(ListedByFragment.TAG, "selected: Bedroom "+list[position].selected)

    }

    interface BottomSheetListener {
        fun selected(typesList:List<FilterBean>,property:String)
    }


}
