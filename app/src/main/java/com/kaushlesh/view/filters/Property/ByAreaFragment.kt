package com.kaushlesh.view.filters.Property

import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.kaushlesh.R
import android.widget.TextView
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar
import com.kaushlesh.bean.FilterBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.view.filters.FilterViewPropertyComman
import com.kaushlesh.view.filters.Vehicle.ByPremiumFragment

class ByAreaFragment : Fragment(), View.OnClickListener {

    var byPriceMin: String? = null
    var byPriceMax: String? = null
    var cname: String? = null

    private var bottomSheetListener: ByAreaFragment.BottomSheetListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_by_area, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        // get seekbar from view
        val rangeSeekbar = view.findViewById(R.id.rangeSeekbar) as CrystalRangeSeekbar

// get min and max text view
        val tvMin = view.findViewById(R.id.tvmin) as TextView
        val tvMax = view.findViewById(R.id.tvmax) as TextView

// set listener


        if (arguments != null) {
            cname = arguments!!.getString("ccname").toString()
            var minfound = arguments!!.getString("minPrice").toString()
            if (minfound.equals("null")) {
                rangeSeekbar.setOnRangeSeekbarChangeListener { minValue, maxValue ->
                    tvMin.text = minValue.toString()
                    tvMax.text = maxValue.toString() + "+"
                   // bottomSheetListener!!.selected(minValue.toString(), maxValue.toString(), "By Area")
                    checkmaxValueandSet(minValue.toInt(),maxValue.toInt())
                }

                // set final value listener

                rangeSeekbar.setOnRangeSeekbarFinalValueListener { minValue, maxValue ->

                }

            } else {

                byPriceMin = arguments!!.getString("minPrice")
                byPriceMax = arguments!!.getString("maxPrice")

                tvMin.text = byPriceMin.toString()
                tvMax.text = byPriceMax.toString() + "+"

                if(!byPriceMin!!.isEmpty() && !byPriceMax!!.isEmpty()) {
                    rangeSeekbar.setMinStartValue(byPriceMin!!.toFloat()).setMaxStartValue(byPriceMax!!.toFloat()).apply()
                }

                rangeSeekbar.setOnRangeSeekbarChangeListener { minValue, maxValue ->
                    tvMin.text = minValue.toString()
                    tvMax.text = maxValue.toString() + "+"
                }

                // set final value listener

                rangeSeekbar.setOnRangeSeekbarFinalValueListener { minValue, maxValue ->
                   /* bottomSheetListener!!.selected(
                        minValue.toString(),
                        maxValue.toString(),
                        "By Area"
                    )*/
                    checkmaxValueandSet(minValue.toInt(),maxValue.toInt())
                }
            }
        }
    }

    private fun checkmaxValueandSet(minValue: Int, maxValue: Int) {
        if(maxValue == 10000)
        {
            bottomSheetListener!!.selected(
                    minValue.toString(),
                    (maxValue * 1000).toString(),
                    "By Area"
            )
        }
        else {
            bottomSheetListener!!.selected(
                    minValue.toString(),
                    maxValue.toString(),
                    "By Area"
            )
        }
    }
    companion object {
        val TAG = "ByAreaFragment"

        fun newInstance(): ByAreaFragment {
            return ByAreaFragment()
        }

        fun newInstance(listener: ByAreaFragment.BottomSheetListener?): ByAreaFragment {
            val fragment = ByAreaFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }
    }

    interface BottomSheetListener {
        fun selected(min: String, max: String, property: String)
    }

    override fun onClick(v: View?) {
    }
}

/*    private operator fun Number.invoke(toFloat: Float): Float {
    return toFloat
    }*/
