package com.kaushlesh.view.filters.Property

import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.kaushlesh.R
import com.kaushlesh.adapter.filter.FilterCommanAdapter
import com.kaushlesh.bean.FilterBean
import com.kaushlesh.view.filters.Khedut.ByProductTypesFragment
import kotlinx.android.synthetic.main.fragment_by_fuel.*

class ByBachelorsAllowedFragment : Fragment(),FilterCommanAdapter.ItemClickListener {

    var bottomSheetListener: ByBachelorsAllowedFragment.BottomSheetListener? = null
    val list = ArrayList<FilterBean>()
    var cname  : String ?= null
    lateinit var adapter:FilterCommanAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_by_fuel, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null) {
            cname = arguments!!.getString("ccname").toString()
            if (arguments!!.getParcelableArrayList<Parcelable>("propertyByBachelorAllowList")!!.isNotEmpty()) {
                list.clear()
                list.addAll(arguments!!.getParcelableArrayList<Parcelable>("propertyByBachelorAllowList") as ArrayList<FilterBean>)
                bindRecyclerviewFuel()
            }else{
                bindRecyclerviewFuel()
            }
        }
    }

    private fun bindRecyclerviewFuel() {

        val recyclerLayoutManager = LinearLayoutManager(context)
        recyclerview.layoutManager = recyclerLayoutManager
        if (list.isNotEmpty()){
            adapter = FilterCommanAdapter(list, context!!)
        }
        else{
            adapter = FilterCommanAdapter(ownerList(), context!!)
        }
        adapter.setClicklistner(this)
        recyclerview.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    private fun ownerList(): List<FilterBean> {
        list.add(FilterBean("Bachelor Allowed",1))
        list.add(FilterBean("Bachelor Not Allowed",2))
        return list
    }

    companion object {
        val TAG = "ByBachelorsAllowedFragment"

        fun newInstance(listener: ByBachelorsAllowedFragment.BottomSheetListener): ByBachelorsAllowedFragment {
            val fragment = ByBachelorsAllowedFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }

        fun newInstance(listener: ByProductTypesFragment.BottomSheetListener): ByProductTypesFragment {
            val fragment = ByProductTypesFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }
    }

    override fun itemClickAllBrand(position: Int) {
        bottomSheetListener!!.selected(list,"BachelorsAllow")

    }

    interface BottomSheetListener {
        fun selected(bachelorAllowList:List<FilterBean>,property:String)
    }
}
