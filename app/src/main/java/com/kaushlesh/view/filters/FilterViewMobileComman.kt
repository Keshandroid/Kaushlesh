package com.kaushlesh.view.filters

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.kaushlesh.R
import kotlinx.android.synthetic.main.filter_comman_view.*
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.kaushlesh.adapter.FilterAdapter
import com.kaushlesh.bean.FilterBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.view.filters.Vehicle.*
import androidx.recyclerview.widget.RecyclerView
import com.facebook.FacebookSdk
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.bean.Filter.FilterPropertyBean
import com.kaushlesh.bean.MobileBrandBeans
import com.kaushlesh.bean.ProductListHouseBean
import com.kaushlesh.bean.SubCategoryBean
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.FiltersSaveRemoveData
import com.kaushlesh.utils.FiltersSaveRemoveData.getFilterData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.filters.Mobile.ByBrandMobileFragment
import com.kaushlesh.view.filters.Mobile.ByBrandTabsFragment
import com.kaushlesh.view.filters.Mobile.ByPriceMobileFragment
import com.kaushlesh.view.filters.Property.*
import org.json.JSONObject
import java.lang.reflect.Type


class FilterViewMobileComman : BottomSheetDialogFragment(),FilterAdapter.ItemClickListener, ByPremiumFragment.BottomSheetListener, ByBrandMobileFragment.BottomSheetListener,
    ByBrandTabsFragment.BottomSheetListener,
    ListedByFragment.BottomSheetListener, ByPriceMobileFragment.BottomSheetListener,
    ParseControllerListener {

   // override fun getTheme(): Int = R.style.BottomSheetDialogTheme
   // override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = BottomSheetDialog(requireContext(), theme)

    var TAG = FilterViewMobileComman::class.java.simpleName
    private var bottomSheetListener: BottomSheetListener? = null
    var cname  : String ?= null

    var byPriceMin: String? = null
    var byPriceMax: String? = null
    var url: String? = null
    val propertyByPremiumList = ArrayList<FilterBean>()
    val mobileByBrandList = ArrayList<FilterBean>()
    val BrandShowCaseBeanList = ArrayList<MobileBrandBeans.MobileBrandBeansList>()
    val otherBrandCaseBeanList = ArrayList<SubCategoryBean>()
    private var plist: ArrayList<ProductListHouseBean.ProductListHouse> = ArrayList()

    var cId: Int? = null
    var categoryId: Int? = null

    companion object {

        val TAG = "FilterViewMobileComman"

        fun newInstance(
            listener: BottomSheetListener, name: String, id: Int, url: String, categoryId: Int
        ): FilterViewMobileComman {
            val fragment = FilterViewMobileComman()
            fragment.bottomSheetListener = listener
            fragment.cname = name
            fragment.cId = id
            fragment.url = url
            fragment.categoryId = categoryId
            return fragment
        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.filter_comman_view, container, false)
        return v
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

       // this.getDialog()?.getWindow()?.setBackgroundDrawableResource(R.drawable.bg_white_top_rounded)

        AppConstants.printLog(TAG, "==title==$cname")
        tv_cate_name.setText(cname)

        //val scrollView = view.findViewById(R.id.scrollView) as MyScrollView

        //scrollView.setScrolling(true) // to enable scrolling.

        val coordinatorLayout = view.findViewById(R.id.cl_main) as CoordinatorLayout
        val bottomSheet = coordinatorLayout.findViewById(R.id.rl_bottom) as RelativeLayout

       /* val behavior = BottomSheetBehavior.from(bottomSheet)
        behavior.isHideable = true
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED)
        behavior.peekHeight = Resources.getSystem().getDisplayMetrics().heightPixels
        //behavior.peekHeight = 1000
        bottomSheet.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT*/

        val offsetFromTop = 10
        (dialog as? BottomSheetDialog)?.behavior?.apply {
            isFitToContents = false
            setExpandedOffset(offsetFromTop)
            state = BottomSheetBehavior.STATE_EXPANDED
        }

        bindRecyclerview()
        setdefaultFragment(cname, cId)


        btn_apply.setOnClickListener {
            var mobileTypes: String? = ""
           /* if (cId!!.equals(13)) {
                mobileTypes = "1"
            }*/

            lateinit var filterBeanDataModel: FilterPropertyBean
            filterBeanDataModel = FilterPropertyBean()
            filterBeanDataModel.categoryId = categoryId.toString()
            filterBeanDataModel.subCategoryId = cId.toString()
            filterBeanDataModel.mobile_type = mobileTypes
            filterBeanDataModel.property_byPremium_list = addSelectedDataPrimumsend(propertyByPremiumList, "id")
            filterBeanDataModel.brand_id = addSelectedDataadd(BrandShowCaseBeanList, "brandId")

            Utils.showLog(TAG,"==brand id==" + addSelectedDataadd(BrandShowCaseBeanList, "brandId"))
            Utils.showLog(TAG,"==premium id saved ==" + FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "3"))
            Utils.showLog(TAG,"==premium id==" + addSelectedDataPrimumsend(propertyByPremiumList, "id"))


            Utils.showLog(TAG,"==price1==" +  filterBeanDataModel.minPrice)
            Utils.showLog(TAG,"==price2==" +  filterBeanDataModel.maxPrice)
            Utils.showLog(TAG,"==price1==" +  byPriceMin)
            Utils.showLog(TAG,"==price2==" +  byPriceMax)
            val lowPrice= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p1")
            val highPrice= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p2")
            Utils.showLog(TAG,"==saved price1==" +  lowPrice)
            Utils.showLog(TAG,"==saved price2==" +  highPrice)


            if (byPriceMin != null) {
                filterBeanDataModel.minPrice = byPriceMin
                //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")

                FiltersSaveRemoveData.savePriceFilter(byPriceMin!!,categoryId.toString(),cId.toString(),"p1")
            } else {
                if(byPriceMin.toString().equals("null"))
                {
                    filterBeanDataModel.minPrice = lowPrice
                    byPriceMin = lowPrice
                    FiltersSaveRemoveData.savePriceFilter(byPriceMin.toString(), categoryId.toString(), cId.toString(), "p1")
                }
                else {
                    filterBeanDataModel.minPrice = ""

                    //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")
                    FiltersSaveRemoveData.savePriceFilter("", categoryId.toString(), cId.toString(), "p1")
                }
            }
            if (byPriceMax != null) {
                filterBeanDataModel.maxPrice = byPriceMax
                //savePriceFilter(byPriceMax,categoryId.toString(),cId.toString(),"p2")

                FiltersSaveRemoveData.savePriceFilter(byPriceMax!!,categoryId.toString(),cId.toString(),"p2")
            } else {
                if(byPriceMax.toString().equals("null"))
                {
                    filterBeanDataModel.maxPrice = highPrice
                    byPriceMax = highPrice
                    FiltersSaveRemoveData.savePriceFilter(byPriceMax.toString(), categoryId.toString(), cId.toString(), "p2")
                }
                else {
                    filterBeanDataModel.maxPrice = ""
                    //savePriceFilter(byPriceMax,categoryId.toString(),cId.toString(),"p2")

                    FiltersSaveRemoveData.savePriceFilter("", categoryId.toString(), cId.toString(), "p2")
                }
            }

            lateinit var getListingController: GetListingController
            getListingController = GetListingController(activity!!, this)
            //getListingController.callforDisplayfilterMobileData(filterBeanDataModel, url)

            bottomSheetListener!!.callApiWithFilter(filterBeanDataModel,url.toString())
            dismiss()

            FiltersSaveRemoveData.saveFilterDataListBrand(BrandShowCaseBeanList, categoryId.toString(), cId.toString(), "1")
            FiltersSaveRemoveData.saveFilterDataList(propertyByPremiumList, categoryId.toString(), cId.toString(), "3")
            //saveFilterDataListBrand(BrandShowCaseBeanList, categoryId.toString(), cId.toString(), "1")
            //saveFilterDataList(propertyByPremiumList, categoryId.toString(), cId.toString(), "3")
        }

        btn_clear.setOnClickListener {
            dismiss()
            lateinit var filterBeanDataModel: FilterPropertyBean
            filterBeanDataModel = FilterPropertyBean()
            filterBeanDataModel.categoryId = categoryId.toString()
            filterBeanDataModel.subCategoryId = cId.toString()
            lateinit var getListingController: GetListingController
            getListingController = GetListingController(activity!!, this)
            //getListingController.callforDisplayfilterMobileData(filterBeanDataModel, url)
            bottomSheetListener!!.callApiWithFilter(filterBeanDataModel,url.toString())
            FiltersSaveRemoveData.cleardata(categoryId.toString(),cId.toString())

            Utils.showToast(context as Activity?,getString(R.string.txt_filter_reset_done))
        }

        iv_close.setOnClickListener {
            dismiss()
        }
    }

    private fun setdefaultFragment(cname: String?, cid: Int?) {
        if (cid!!.equals(11)||cid.equals(12)) {
            val bundle = Bundle()
            bundle.putString("ccname", cname)
            bundle.putInt("catid", categoryId!!.toInt())
            bundle.putInt("subcatid", cId!!.toInt())

            val data=FiltersSaveRemoveData.getFilterDataBrand(categoryId.toString(),cId.toString(),"1")
            if (data.toString()=="null"){
                // Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            }else{

                BrandShowCaseBeanList.clear()
                BrandShowCaseBeanList.addAll(FiltersSaveRemoveData.getFilterDataBrand(categoryId.toString(), cId.toString(), "1") as Collection<MobileBrandBeans.MobileBrandBeansList>)
            }

            bundle.putParcelableArrayList("ByBrand", BrandShowCaseBeanList as java.util.ArrayList<out Parcelable>)
            changeFilterFragment(ByBrandMobileFragment.newInstance(this), ByBrandMobileFragment.TAG, bundle, true)
        }
        if (cid.equals(13)) {


            val lowPrice= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p1")
            var highPrice= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p2")
            if (lowPrice.toString()=="null"){
                //  Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            }else{
                byPriceMin=FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p1")
                byPriceMax=FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p2")
            }

            val bundle = Bundle()
            bundle.putString("ccname", cname)
            bundle.putInt("cid",cId!!.toInt())
            bundle.putString("minPrice", byPriceMin)
            bundle.putString("maxPrice", byPriceMax)
            changeFilterFragment(
                ByPriceMobileFragment.newInstance(this),
                ByPriceMobileFragment.TAG,
                bundle,
                true
            )
        }
        if (cid.equals(1003))
        {
            val bundle = Bundle()
            bundle.putString("ccname", cname)

            val data = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "3")
            //var data=getFilterData(categoryId.toString(), cId.toString(), "3")
            if (data.toString()=="null"){
                // Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            }else{
                propertyByPremiumList.clear()
                propertyByPremiumList.addAll(FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "3") as Collection<FilterBean>)
            }

            bundle.putParcelableArrayList(
                "propertyByPremiumList",
                propertyByPremiumList as java.util.ArrayList<out Parcelable>
            )
            changeFilterFragment(
                ByPremiumFragment.newInstance(this),
                ByPremiumFragment.TAG,
                bundle,
                true
            )
        }
    }


    private fun bindRecyclerview() {

        val layoutManager = LinearLayoutManager(context)
        rv_filter.layoutManager = layoutManager
        val adapter = FilterAdapter(typeList(), context!!)
        rv_filter.addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL) as RecyclerView.ItemDecoration);
        adapter.setClicklistner(this)
        rv_filter.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    private fun typeList(): List<FilterBean> {
        val list = ArrayList<FilterBean>()

        if (cId == 11) {
            list.add(FilterBean("By Brand", 1))
            list.add(FilterBean("By Price", 2))
            list.add(FilterBean("Premium Ad", 7))
        }
        if (cId == 12) {
            list.add(FilterBean("By Brand", 1))
            list.add(FilterBean("By Price", 2))
            list.add(FilterBean("Premium Ad", 7))
        }
        if (cId == 13) {
            list.add(FilterBean("By Price", 2))
            list.add(FilterBean("Premium Ad", 7))
        }
        if (cId == 1003) {
            list.add(FilterBean("Premium Ad", 7))
        }

        return list
    }

    interface BottomSheetListener {
        fun onButtonClicked(
            text: String,
            type: String,
            cname: ArrayList<ProductListHouseBean.ProductListHouse>
        )

        fun callApiWithFilter(filterBeanDataModel : FilterPropertyBean, url : String)
    }

    override fun itemclickView(bean: FilterBean) {
        AppConstants.printLog(TAG, "==type==" + bean.name)
        val bundle = Bundle()


        if (bean.id == 1) {
            bundle.putInt("catid", categoryId!!.toInt())
            bundle.putInt("subcatid", cId!!.toInt())

            val data=FiltersSaveRemoveData.getFilterDataBrand(categoryId.toString(), cId.toString(), "1")
            if (data.toString()=="null"){
                // Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            }else{
                BrandShowCaseBeanList.clear()
                BrandShowCaseBeanList.addAll(FiltersSaveRemoveData.getFilterDataBrand(categoryId.toString(), cId.toString(), "1") as Collection<MobileBrandBeans.MobileBrandBeansList>)
            }

            bundle.putParcelableArrayList(
                "ByBrand",
                BrandShowCaseBeanList as java.util.ArrayList<out Parcelable>
            )
            changeFilterFragment(
                ByBrandMobileFragment.newInstance(this),
                ByBrandMobileFragment.TAG,
                bundle,
                true
            )
        }
        if (bean.id == 2) {
          //  FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p1")
            val lowPrice= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p1")
            var highPrice= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p2")

            if (lowPrice.toString()=="null"){
                //  Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            }else{
                byPriceMin= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p1")
                byPriceMax= FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p2")
            }

            bundle.putString("ccname", cname)
            bundle.putInt("cid", cId!!.toInt())
            bundle.putString("minPrice", byPriceMin)
            bundle.putString("maxPrice", byPriceMax)
            changeFilterFragment(
                ByPriceMobileFragment.newInstance(this),
                ByPriceMobileFragment.TAG,
                bundle,
                true
            )
        }

        if (bean.id == 7) {
            bundle.putString("ccname", cname)

            val data = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "3")
            //var data=getFilterData(categoryId.toString(), cId.toString(), "3")
            if (data.toString()=="null"){
                // Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            }else{
                propertyByPremiumList.clear()
                propertyByPremiumList.addAll(FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "3") as Collection<FilterBean>)
            }

            bundle.putParcelableArrayList(
                "propertyByPremiumList",
                propertyByPremiumList as java.util.ArrayList<out Parcelable>
            )
            changeFilterFragment(
                ByPremiumFragment.newInstance(this),
                ByPremiumFragment.TAG,
                bundle,
                true
            )
        }

    }

    private fun changeFilterFragment(fragment: Fragment, tag: String, args: Bundle?, add: Boolean) {
        val transaction = childFragmentManager.beginTransaction()
        if (args != null) fragment.arguments = args
        transaction.replace(R.id.fl_lmain,fragment,tag)
        transaction.commit()
    }

    private fun addSelectedDataadd(
        list: ArrayList<MobileBrandBeans.MobileBrandBeansList>,
        typeGet: String
    ): ArrayList<String>? {
        val selectedList: ArrayList<String>? = arrayListOf()
        for (i in 0 until list.size) {
            if (list[i].selected!!) {
                selectedList!!.add(list[i].brandId.toString().trim())
            }
        }
        return selectedList
    }


    private fun addSelectedDataPrimumsend(list: ArrayList<FilterBean>, typeGet: String): ArrayList<String> {
        val selectedList: ArrayList<String> = arrayListOf()
        for (i in 0 until list.size) {
            if (list[i].selected) {
                if (list[i].id == 1) {
                    // selectedList!!.add("Premium")
                    selectedList.add("1")
                } else if (list[i].id == 2) {
                    //selectedList!!.add("Free")
                    selectedList.add("0")
                }
            }
        }
        return selectedList
    }

    override fun selected(typesList: List<FilterBean>, property: String) {
        Utils.showLog(TAG,"==premimum data====" + typesList.size)
        Utils.showLog(TAG,"==type====" + property)
        if (property.equals("Premium Ad")) {
            addPropertyData(propertyByPremiumList, typesList)
        }
    }

    fun addPropertyData(
        listAdd: ArrayList<FilterBean>,
        typesList: List<FilterBean>
    ) {
        listAdd.clear()
        listAdd.addAll(typesList)
    }


    override fun selected(min: String, max: String, property: String) {
        if (property.equals("By Price")) {
            byPriceMax = max
            byPriceMin = min
        }

        savePremium()
    }

    private fun savePremium() {

        lateinit var filterBeanDataModel: FilterPropertyBean
        filterBeanDataModel = FilterPropertyBean()
        val data= getFilterData(categoryId.toString(), cId.toString(), "3")
        if (data.toString()=="null"){
            // Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            filterBeanDataModel.property_byPremium_list = addSelectedDataPrimumsend(propertyByPremiumList, "id")
            FiltersSaveRemoveData.saveFilterDataList(propertyByPremiumList, categoryId.toString(), cId.toString(), "3")
        }else{
            propertyByPremiumList.clear()
            propertyByPremiumList.addAll(FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "3") as Collection<FilterBean>)
            filterBeanDataModel.property_byPremium_list = addSelectedDataPrimumsend(propertyByPremiumList, "id")
            FiltersSaveRemoveData.saveFilterDataList(propertyByPremiumList, categoryId.toString(), cId.toString(), "3")
        }
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (method == "Get Product") {
            Log.e(TAG, "onSuccess: " + ProductListHouseBean())
            plist.clear()
            val dataModel = ProductListHouseBean()
            dataModel.ProductListHouseBean(da)
            plist = dataModel.productlist
        }
        bottomSheetListener!!.onButtonClicked("Clicked", "apply", plist)
        dismiss()
    }

    override fun onFail(msg: String, method: String) {
        bottomSheetListener!!.onButtonClicked("Clicked", msg, plist)
        dismiss()
    }

    override fun selectedBrand(
        BrandCase: List<MobileBrandBeans.MobileBrandBeansList>,
        property: String
    ) {
        BrandShowCaseBeanList.clear()
        BrandShowCaseBeanList.addAll(BrandCase)

        savePremium()
    }

    override fun TabBrandselected(
        BrandList: List<MobileBrandBeans.MobileBrandBeansList>,
        property: String
    ) {
        BrandShowCaseBeanList.clear()
        BrandShowCaseBeanList.addAll(BrandList)

        savePremium()
    }
}
