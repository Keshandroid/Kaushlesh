package com.kaushlesh.view.filters.Property

import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager

import com.kaushlesh.R
import com.kaushlesh.adapter.FilterAdapter
import com.kaushlesh.adapter.filter.FilterCommanAdapter
import com.kaushlesh.bean.FilterBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.filters.FilterViewPropertyComman
import kotlinx.android.synthetic.main.fragment_by_fuel.*

class ByPropertyTypeFragment : Fragment(), FilterCommanAdapter.ItemClickListener{
    val posionselectedList = ArrayList<FilterBean>()
    var list = ArrayList<FilterBean>()
    var TAG = ByPropertyTypeFragment::class.java.simpleName
    private var bottomSheetListener: ByPropertyTypeFragment.BottomSheetListener? = null
    lateinit var adapter:FilterCommanAdapter
    var cname  : String ?= null
    var cid: Int? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_by_fuel, container, false)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null) {
            cname = arguments!!.getString("ccname").toString()
            cid = arguments!!.getInt("cid")
            Utils.showLog(TAG,"===cat id== " + cid)
            if (arguments!!.getParcelableArrayList<Parcelable>("propertyTypesList")!!.isNotEmpty()) {
                list.clear()
                list.addAll(arguments!!.getParcelableArrayList<Parcelable>("propertyTypesList") as ArrayList<FilterBean>)
                bindRecyclerviewFuel()
            }else{
                bindRecyclerviewFuel()
            }
            AppConstants.printLog(TAG, "==title==$cname")
        }

    }


    private fun bindRecyclerviewFuel() {
        // AppConstants.printLog(TAG, "==title==${list.size}")
        val recyclerLayoutManager = LinearLayoutManager(context)
        recyclerview.layoutManager = recyclerLayoutManager

        if (list.isNotEmpty()){
            adapter = FilterCommanAdapter(list, context!!)
        }

        else{
            adapter = FilterCommanAdapter(ownerList(), context!!)
        }
        adapter.setClicklistner(this)

        recyclerview.adapter = adapter
        adapter.notifyDataSetChanged()
    }


    private fun ownerList(): List<FilterBean> {
        if(cid!!.equals(1) || cid!!.equals(2)) {
            list.add(FilterBean("Appartment", 1))
            list.add(FilterBean("Villa & Houses", 4))
            list.add(FilterBean("Farm House", 3))
            list.add(FilterBean("Builder Floor", 2))
        }

        else if(cid!!.equals(6))
        {
            list.add(FilterBean("Booking",3))
            list.add(FilterBean("Sell",2))
        }

        else if(cid!!.equals(5))
        {
            list.add(FilterBean("Agriculture", 5))
            list.add(FilterBean("Commercial", 6))   
            list.add(FilterBean("Residential", 7))
        }
        else if(cid!!.equals(7))
        {
            list.add(FilterBean("Hotel", 8))
            list.add(FilterBean("Resort", 9))
        }
        else if(cid!!.equals(8))
        {
            list.add(FilterBean("Labour camp", 11))
            list.add(FilterBean("Ware House", 10))
        }
        else if(cid!!.equals(9))
        {
            list.add(FilterBean("Guest House", 12))
            list.add(FilterBean("PG", 13))
            list.add(FilterBean("Roommate", 14))
        }
        return list
    }

    override fun itemClickAllBrand(posion:Int) {
        Log.e(TAG, "selected: "+list[posion].selected + list[posion].id)
        bottomSheetListener!!.selected(list,"Type")
    }

    companion object {
        val TAG = "ByPropertyTypeFragment"
        fun newInstance(listener:BottomSheetListener): ByPropertyTypeFragment {
            val fragment = ByPropertyTypeFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }
    }
    interface BottomSheetListener {
        fun selected(typesList:List<FilterBean>,property:String)
    }
}
