package com.kaushlesh.view.filters.Bikes

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.kaushlesh.R
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.filters.Property.ByPropertyPriceFragment
import kotlinx.android.synthetic.main.fragment_by_year.*

class ByYearBikesFragment : Fragment(), View.OnClickListener {
    private var bottomSheetListener: ByYearBikesFragment.BottomSheetListener? = null
    var cname: String? = null
    var cid: Int? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_by_year, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        if (arguments != null) {
            val minfound = arguments!!.getString("minYear").toString()
            val maxfound = arguments!!.getString("maxYear").toString()
            if (!minfound.equals("null") && !maxfound.equals("null")) {
                etFrom.setText(arguments!!.getString("minYear"))
                etTo.setText(arguments!!.getString("maxYear"))
            }
            etFrom.addTextChangedListener(object : TextWatcher {

                override fun afterTextChanged(s: Editable) {

                }

                override fun beforeTextChanged(
                    s: CharSequence, start: Int,
                    count: Int, after: Int
                ) {

                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                        if (etFrom.text.length > 0) {

                            if (etTo.text.length > 0) {
                                bottomSheetListener!!.selected(etFrom.text.toString(), etTo.text.toString(), "By Year")
                            }

                        }
                        else{
                            bottomSheetListener!!.selected("", "", "By Year")
                        }
                }
            })
            etTo.addTextChangedListener(object : TextWatcher {

                override fun afterTextChanged(s: Editable) {}

                override fun beforeTextChanged(
                    s: CharSequence, start: Int,
                    count: Int, after: Int
                ) {

                }

                override fun onTextChanged(
                    s: CharSequence, start: Int,
                    before: Int, count: Int
                ) {
                    if (etTo.text.length > 0) {
                        if (etFrom.text.length > 0) {
                            bottomSheetListener!!.selected(etFrom.text.toString(), etTo.text.toString(), "By Year")
                        }
                    }
                    else{
                        bottomSheetListener!!.selected("", "", "By Year")
                    }
                }
            })

        }
    }

    companion object {
        val TAG = "ByYearBikesFragment"

        fun newInstance(): ByYearBikesFragment {
            return ByYearBikesFragment()
        }

        fun newInstance(listener: BottomSheetListener): ByYearBikesFragment {
            val fragment = ByYearBikesFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }
    }

    interface BottomSheetListener {
        fun selected(min: String, max: String, property: String)
    }

    override fun onClick(v: View?) {

    }
}
