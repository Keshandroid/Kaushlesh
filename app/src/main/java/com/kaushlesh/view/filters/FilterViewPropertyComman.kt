package com.kaushlesh.view.filters

import android.app.Activity
import android.content.Context.MODE_PRIVATE
import android.content.res.Resources
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.facebook.FacebookSdk.getApplicationContext
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.R
import com.kaushlesh.adapter.FilterAdapter
import com.kaushlesh.bean.*
import com.kaushlesh.bean.Filter.FilterPropertyBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.CommanPropertyTypeListFilter
import com.kaushlesh.utils.FiltersSaveRemoveData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.filters.Property.*
import com.kaushlesh.view.filters.Vehicle.*
import kotlinx.android.synthetic.main.filter_comman_view.*
import org.json.JSONObject
import java.lang.reflect.Type


class FilterViewPropertyComman : BottomSheetDialogFragment(), FilterAdapter.ItemClickListener,
        ByPropertyTypeFragment.BottomSheetListener,
        ByFurnishingFragment.BottomSheetListener, ByBathroomFragment.BottomSheetListener,
        ByAreaFragment.BottomSheetListener, ListedByFragment.BottomSheetListener,
        ByConstructionStatusFragment.BottomSheetListener, ByPremiumFragment.BottomSheetListener,
        ByPropertyPriceFragment.BottomSheetListener, ByPurposeFragment.BottomSheetListener,
        ByApvillSellPricePropertyFragment.BottomSheetListener, ByCarpetAreaFragment.BottomSheetListener,
        ByGuestCapcityFragment.BottomSheetListener, ByLandAreaFragment.BottomSheetListener,
        ParseControllerListener, ByBachelorsAllowedFragment.BottomSheetListener {

    private var plist: ArrayList<ProductListHouseBean.ProductListHouse> = ArrayList()
    var propertyTypesList = ArrayList<FilterBean>()
    var propertyListedByList = ArrayList<FilterBean>()
    var propertyByBedroomList = ArrayList<FilterBean>()
    var propertyByConstructionList = ArrayList<FilterBean>()
    var propertyByBathroomList = ArrayList<FilterBean>()
    var propertyByFunishingList = ArrayList<FilterBean>()
    var propertyByPremiumList = ArrayList<FilterBean>()
    var propertyByBachelorAllowList = ArrayList<FilterBean>()
    var propertyByPurposeList = ArrayList<FilterBean>()
    var byAreamMin: String? = null
    var bycarpetAreaMax: String? = null
    var bycarpetAreamMin: String? = null
    var byAreaMax: String? = null
    var guestMin: String? = null
    var guestMax: String? = null
    var landAreamMin: String? = null
    var landAreaMax: String? = null
    var byPriceMin: String? = null
    var byPriceMax: String? = null
    var TAG = FilterViewPropertyComman::class.java.simpleName
    var bottomSheetListener: BottomSheetListener? = null
    var cname: String? = null
    var cId: Int? = null
    var categoryId: Int? = null
    var url: String? = null

    companion object {
        val TAG = "FilterViewPropertyComman"
        fun newInstance(
                listener: BottomSheetListener, name: String, id: Int, url: String, categoryId: Int
        ): FilterViewPropertyComman {
            val fragment = FilterViewPropertyComman()
            fragment.bottomSheetListener = listener
            fragment.cname = name
            fragment.cId = id
            fragment.url = url
            fragment.categoryId = categoryId
            return fragment
        }
    }

    lateinit var filterallbeans: FilterAllBean

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.filter_comman_view, container, false)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        AppConstants.printLog(TAG, "==title==$cname")
        tv_cate_name.setText(cname)
        filterallbeans = FilterAllBean()
        val coordinatorLayout = view.findViewById(R.id.cl_main) as CoordinatorLayout
        val bottomSheet = coordinatorLayout.findViewById(R.id.rl_bottom) as RelativeLayout


        val offsetFromTop = 10
        (dialog as? BottomSheetDialog)?.behavior?.apply {
            isFitToContents = false
            setExpandedOffset(offsetFromTop)
            state = BottomSheetBehavior.STATE_EXPANDED
        }

        bindRecyclerview()
        btn_apply.setOnClickListener {

            lateinit var filterBeanDataModel: FilterPropertyBean
            filterBeanDataModel = FilterPropertyBean()
            filterBeanDataModel.categoryId = categoryId.toString()
            filterBeanDataModel.subCategoryId = cId.toString()
            filterBeanDataModel.property_type_list = addSelectedData(propertyTypesList, "id")
            filterBeanDataModel.property_listedby_list = addSelectedData(propertyListedByList, "id")
            filterBeanDataModel.property_bybadroom_list = addSelectedData(propertyByBedroomList, "id")
            filterBeanDataModel.property_byBathroom_list = addSelectedData(propertyByBathroomList, "id")
            filterBeanDataModel.property_byFunishing_list = addSelectedData(propertyByFunishingList, "id")
            filterBeanDataModel.property_byConstruction_list = addSelectedData(propertyByConstructionList, "id")
            filterBeanDataModel.property_byBachelor_Allow_list = addSelectedData(propertyByBachelorAllowList, "id")
            filterBeanDataModel.property_bypurpose_list = addSelectedData(propertyByPurposeList, "id")
            filterBeanDataModel.property_byPremium_list = addSelectedDataPrimumsend(propertyByPremiumList, "name")


            //types
            val datatype = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "1")
            Utils.showLog(TAG, "==property type==" + addSelectedData(propertyTypesList, "id"))
            Utils.showLog(TAG, "==property type saved==" + datatype)
            if (!propertyTypesList.isEmpty()) {

                filterBeanDataModel.property_type_list = addSelectedData(propertyTypesList, "id")

            }
            else {
                if (datatype != null) {
                    propertyTypesList.clear()
                    propertyTypesList.addAll(datatype as Collection<FilterBean>)
                    filterBeanDataModel.property_type_list = addSelectedData(propertyTypesList, "id")

                } else {
                    propertyTypesList.clear()
                    filterBeanDataModel.property_type_list = arrayListOf()
                    propertyTypesList = arrayListOf()
                }
            }

            //listedby
            val data = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "2")
            Utils.showLog(TAG, "==listed by==" + addSelectedData(propertyListedByList, "id"))
            Utils.showLog(TAG, "==listed by saved==" + data)
            if (!propertyListedByList.isEmpty()) {

                filterBeanDataModel.property_listedby_list = addSelectedData(propertyListedByList, "id")
                //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")

                // FiltersSaveRemoveData.saveFilterDataList(propertyListedByList, categoryId.toString(), cId.toString(), "2")

            }
            else {
                if (data != null) {
                    propertyListedByList.clear()
                    propertyListedByList.addAll(data as Collection<FilterBean>)
                    filterBeanDataModel.property_listedby_list = addSelectedData(propertyListedByList, "id")
                    // FiltersSaveRemoveData.saveFilterDataList(propertyListedByList, categoryId.toString(), cId.toString(), "2")
                } else {
                    propertyListedByList.clear()
                    filterBeanDataModel.property_listedby_list = arrayListOf()
                    propertyListedByList = arrayListOf()
                    //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")
                    //FiltersSaveRemoveData.saveFilterDataList(propertyListedByList, categoryId.toString(), cId.toString(), "2")
                }
            }

            //bedrooms
            val databed = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "3")
            if (!propertyByBedroomList.isEmpty()) {

                filterBeanDataModel.property_bybadroom_list = addSelectedData(propertyByBedroomList, "id")
            }
            else {
                if (databed != null) {
                    propertyByBedroomList.clear()
                    propertyByBedroomList.addAll(databed as Collection<FilterBean>)
                    filterBeanDataModel.property_bybadroom_list = addSelectedData(propertyByBedroomList, "id")
                } else {
                    propertyByBedroomList.clear()
                    filterBeanDataModel.property_bybadroom_list = arrayListOf()
                    propertyByBedroomList = arrayListOf()
                }
            }

            //bathrooms
            val databath = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "4")
            if (!propertyByBathroomList.isEmpty()) {

                filterBeanDataModel.property_byBathroom_list = addSelectedData(propertyByBathroomList, "id")
            }
            else {
                if (databath != null) {
                    propertyByBathroomList.clear()
                    propertyByBathroomList.addAll(databath as Collection<FilterBean>)
                    filterBeanDataModel.property_byBathroom_list = addSelectedData(propertyByBathroomList, "id")
                } else {
                    propertyByBathroomList.clear()
                    filterBeanDataModel.property_byBathroom_list = arrayListOf()
                    propertyByBathroomList = arrayListOf()
                }
            }

            //furnishing
            val datafurniture = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "5")
            if (!propertyByFunishingList.isEmpty()) {

                filterBeanDataModel.property_byFunishing_list = addSelectedData(propertyByFunishingList, "id")
            }
            else {
                if (datafurniture != null) {
                    propertyByFunishingList.clear()
                    propertyByFunishingList.addAll(datafurniture as Collection<FilterBean>)
                    filterBeanDataModel.property_byFunishing_list = addSelectedData(propertyByFunishingList, "id")
                } else {
                    propertyByFunishingList.clear()
                    filterBeanDataModel.property_byFunishing_list = arrayListOf()
                    propertyByFunishingList = arrayListOf()
                }
            }

            //construction
            val dataconstuction = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "6")
            if (!propertyByConstructionList.isEmpty()) {

                filterBeanDataModel.property_byConstruction_list = addSelectedData(propertyByConstructionList, "id")
            }
            else {
                if (dataconstuction != null) {
                    propertyByConstructionList.clear()
                    propertyByConstructionList.addAll(dataconstuction as Collection<FilterBean>)
                    filterBeanDataModel.property_byConstruction_list = addSelectedData(propertyByConstructionList, "id")
                } else {
                    propertyByConstructionList.clear()
                    filterBeanDataModel.property_byConstruction_list = arrayListOf()
                    propertyByConstructionList = arrayListOf()
                }
            }

            //bachlors
            val databechlor = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "7")
            if (!propertyByBachelorAllowList.isEmpty()) {

                filterBeanDataModel.property_byBachelor_Allow_list = addSelectedData(propertyByBachelorAllowList, "id")
            }
            else {
                if (databechlor != null) {
                    propertyByBachelorAllowList.clear()
                    propertyByBachelorAllowList.addAll(databechlor as Collection<FilterBean>)
                    filterBeanDataModel.property_byBachelor_Allow_list = addSelectedData(propertyByBachelorAllowList, "id")
                } else {
                    propertyByBachelorAllowList.clear()
                    filterBeanDataModel.property_byBachelor_Allow_list = arrayListOf()
                    propertyByBachelorAllowList = arrayListOf()
                }
            }

            //purpose
            val datapurpose = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "8")
            if (!propertyByPurposeList.isEmpty()) {

                filterBeanDataModel.property_bypurpose_list = addSelectedData(propertyByPurposeList, "id")
            }
            else {
                if (datapurpose != null) {
                    propertyByPurposeList.clear()
                    propertyByPurposeList.addAll(datapurpose as Collection<FilterBean>)
                    filterBeanDataModel.property_bypurpose_list = addSelectedData(propertyByPurposeList, "id")
                } else {
                    propertyByPurposeList.clear()
                    filterBeanDataModel.property_bypurpose_list = arrayListOf()
                    propertyByPurposeList = arrayListOf()
                }
            }

            //premium
            val data1 = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "9")

            if (!propertyByPremiumList.isEmpty()) {

                filterBeanDataModel.property_byPremium_list = addSelectedDataPrimumsend(propertyByPremiumList, "name")
                //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")

                //FiltersSaveRemoveData.saveFilterDataList(propertyByPremiumList, categoryId.toString(), cId.toString(), "9")

            }
            else {
                if (data1 != null) {
                    propertyByPremiumList.clear()
                    propertyByPremiumList.addAll(data1 as Collection<FilterBean>)
                    filterBeanDataModel.property_byPremium_list = addSelectedDataPrimumsend(propertyByPremiumList, "name")
                    //FiltersSaveRemoveData.saveFilterDataList(propertyByPremiumList, categoryId.toString(), cId.toString(), "9")

                } else {
                    propertyByPremiumList.clear()
                    filterBeanDataModel.property_byPremium_list = arrayListOf()
                    propertyByPremiumList = arrayListOf()
                    //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")
                    //FiltersSaveRemoveData.saveFilterDataList(propertyByPremiumList, categoryId.toString(), cId.toString(), "9")
                }
            }

            //range filters
            val lowPrice = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p1")
            val highPrice = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p2")
            Utils.showLog(TAG, "==saved price1==" + lowPrice)
            Utils.showLog(TAG, "==saved price2==" + highPrice)

            val lowarea = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "a1")
            val higharea = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "a2")
            Utils.showLog(TAG, "==saved area1==" + lowarea)
            Utils.showLog(TAG, "==saved area2==" + higharea)

            val lowarealand = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "l1")
            val higharealand = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "l2")
            Utils.showLog(TAG, "==saved land area1==" + lowarealand)
            Utils.showLog(TAG, "==saved land area2==" + higharealand)

            val lowguest = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "g1")
            val highguest = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "g2")
            Utils.showLog(TAG, "==saved guest1==" + lowguest)
            Utils.showLog(TAG, "==saved guest2==" + highguest)

            val lowcarpetarea = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "b1")
            val highcarpetarea = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "b2")
            Utils.showLog(TAG, "==saved guest1==" + lowcarpetarea)
            Utils.showLog(TAG, "==saved guest2==" + highcarpetarea)

            //price
            /* if (byPriceMin != null) {
                 filterBeanDataModel.minPrice = byPriceMin
                 FiltersSaveRemoveData.savePriceFilter(byPriceMin!!,categoryId.toString(),cId.toString(),"p1")
             }
             if (byPriceMax != null) {
                 filterBeanDataModel.maxPrice = byPriceMax
                 FiltersSaveRemoveData.savePriceFilter(byPriceMax!!,categoryId.toString(),cId.toString(),"p2")

             }*/
            if (byPriceMin != null) {
                filterBeanDataModel.minPrice = byPriceMin
                //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")

                FiltersSaveRemoveData.savePriceFilter(byPriceMin!!, categoryId.toString(), cId.toString(), "p1")
            } else {
                if (byPriceMin.toString().equals("null")) {
                    filterBeanDataModel.minPrice = lowPrice
                    byPriceMin = lowPrice
                    FiltersSaveRemoveData.savePriceFilter(byPriceMin.toString(), categoryId.toString(), cId.toString(), "p1")
                } else {
                    filterBeanDataModel.minPrice = ""

                    //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")
                    FiltersSaveRemoveData.savePriceFilter("", categoryId.toString(), cId.toString(), "p1")
                }
            }

            if (byPriceMax != null) {
                filterBeanDataModel.maxPrice = byPriceMax
                //savePriceFilter(byPriceMax,categoryId.toString(),cId.toString(),"p2")

                FiltersSaveRemoveData.savePriceFilter(byPriceMax!!, categoryId.toString(), cId.toString(), "p2")
            } else {
                if (byPriceMax.toString().equals("null")) {
                    filterBeanDataModel.maxPrice = highPrice
                    byPriceMax = highPrice
                    FiltersSaveRemoveData.savePriceFilter(byPriceMax.toString(), categoryId.toString(), cId.toString(), "p2")
                } else {
                    filterBeanDataModel.maxPrice = ""
                    //savePriceFilter(byPriceMax,categoryId.toString(),cId.toString(),"p2")

                    FiltersSaveRemoveData.savePriceFilter("", categoryId.toString(), cId.toString(), "p2")
                }
            }

            //super builtup area
            /* if (byAreamMin != null) {
                 filterBeanDataModel.AreaMin = byAreamMin
                 FiltersSaveRemoveData.savePriceFilter(byAreamMin!!,categoryId.toString(),cId.toString(),"a1")
             }
             if (byAreaMax != null) {
                 filterBeanDataModel.AreaMax = byAreaMax
                 FiltersSaveRemoveData.savePriceFilter(byAreaMax!!,categoryId.toString(),cId.toString(),"a2")

             }*/

            if (byAreamMin != null) {
                filterBeanDataModel.AreaMin = byAreamMin
                //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")

                FiltersSaveRemoveData.savePriceFilter(byAreamMin!!, categoryId.toString(), cId.toString(), "a1")
            } else {
                if (byAreamMin.toString().equals("null")) {
                    filterBeanDataModel.AreaMin = lowarea
                    byAreamMin = lowarea
                    FiltersSaveRemoveData.savePriceFilter(byAreamMin!!, categoryId.toString(), cId.toString(), "a1")
                } else {
                    filterBeanDataModel.AreaMin = ""

                    //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")
                    FiltersSaveRemoveData.savePriceFilter(byAreamMin!!, categoryId.toString(), cId.toString(), "a1")
                }
            }

            if (byAreaMax != null) {
                filterBeanDataModel.AreaMax = byAreaMax
                //savePriceFilter(byPriceMax,categoryId.toString(),cId.toString(),"p2")

                FiltersSaveRemoveData.savePriceFilter(byAreaMax!!, categoryId.toString(), cId.toString(), "a2")
            } else {
                if (byAreaMax.toString().equals("null")) {
                    filterBeanDataModel.AreaMax = higharea
                    byAreaMax = higharea
                    FiltersSaveRemoveData.savePriceFilter(byAreaMax!!, categoryId.toString(), cId.toString(), "a2")
                } else {
                    filterBeanDataModel.AreaMax = ""
                    //savePriceFilter(byPriceMax,categoryId.toString(),cId.toString(),"p2")

                    FiltersSaveRemoveData.savePriceFilter(byAreaMax!!, categoryId.toString(), cId.toString(), "a2")
                }
            }

            //land area
            /* if (landAreamMin != null) {
                 filterBeanDataModel.landAreaMin = landAreamMin
                 FiltersSaveRemoveData.savePriceFilter(landAreamMin!!,categoryId.toString(),cId.toString(),"l1")

             }
             if (landAreaMax != null) {
                 filterBeanDataModel.landAreaMax = landAreaMax
                 FiltersSaveRemoveData.savePriceFilter(landAreaMax!!,categoryId.toString(),cId.toString(),"l2")

             }*/

            if (landAreamMin != null) {
                filterBeanDataModel.landAreaMin = landAreamMin
                //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")

                FiltersSaveRemoveData.savePriceFilter(landAreamMin!!, categoryId.toString(), cId.toString(), "l1")
            } else {
                if (landAreamMin.toString().equals("null")) {
                    filterBeanDataModel.landAreaMin = lowarealand
                    landAreamMin = lowarealand
                    FiltersSaveRemoveData.savePriceFilter(landAreamMin!!, categoryId.toString(), cId.toString(), "l1")
                } else {
                    filterBeanDataModel.landAreaMin = ""

                    //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")
                    FiltersSaveRemoveData.savePriceFilter(landAreamMin!!, categoryId.toString(), cId.toString(), "l1")
                }
            }

            if (landAreaMax != null) {
                filterBeanDataModel.landAreaMax = landAreaMax
                //savePriceFilter(byPriceMax,categoryId.toString(),cId.toString(),"p2")

                FiltersSaveRemoveData.savePriceFilter(landAreaMax!!, categoryId.toString(), cId.toString(), "l2")
            } else {
                if (landAreaMax.toString().equals("null")) {
                    filterBeanDataModel.landAreaMax = higharealand
                    landAreaMax = higharealand
                    FiltersSaveRemoveData.savePriceFilter(landAreaMax!!, categoryId.toString(), cId.toString(), "l2")
                } else {
                    filterBeanDataModel.landAreaMax = ""
                    //savePriceFilter(byPriceMax,categoryId.toString(),cId.toString(),"p2")

                    FiltersSaveRemoveData.savePriceFilter(landAreaMax!!, categoryId.toString(), cId.toString(), "l2")
                }
            }

            //guest capacity

            /* if (guestMin != null) {
                 filterBeanDataModel.guestAreaMin = guestMin
                 FiltersSaveRemoveData.savePriceFilter(guestMin!!,categoryId.toString(),cId.toString(),"g1")
             }
             if (guestMax != null) {
                 filterBeanDataModel.guestAreaMax = guestMax
                 FiltersSaveRemoveData.savePriceFilter(guestMax!!,categoryId.toString(),cId.toString(),"g2")
             }*/

            if (guestMin != null) {
                filterBeanDataModel.guestAreaMin = guestMin
                //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")

                FiltersSaveRemoveData.savePriceFilter(guestMin!!, categoryId.toString(), cId.toString(), "g1")
            } else {
                if (guestMin.toString().equals("null")) {
                    filterBeanDataModel.guestAreaMin = lowguest
                    guestMin = lowguest
                    FiltersSaveRemoveData.savePriceFilter(guestMin!!, categoryId.toString(), cId.toString(), "g1")
                } else {
                    filterBeanDataModel.guestAreaMin = ""

                    //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")
                    FiltersSaveRemoveData.savePriceFilter(guestMin!!, categoryId.toString(), cId.toString(), "g1")
                }
            }

            if (guestMax != null) {
                filterBeanDataModel.guestAreaMax = guestMax
                //savePriceFilter(byPriceMax,categoryId.toString(),cId.toString(),"p2")

                FiltersSaveRemoveData.savePriceFilter(guestMax!!, categoryId.toString(), cId.toString(), "g2")
            }
            else {
                if (guestMax.toString().equals("null")) {
                    filterBeanDataModel.guestAreaMax = highguest
                    guestMax = highguest
                    FiltersSaveRemoveData.savePriceFilter(guestMax!!, categoryId.toString(), cId.toString(), "g2")
                } else {
                    filterBeanDataModel.guestAreaMax = ""
                    //savePriceFilter(byPriceMax,categoryId.toString(),cId.toString(),"p2")

                    FiltersSaveRemoveData.savePriceFilter(guestMax!!, categoryId.toString(), cId.toString(), "g2")
                }
            }

            //carpet area
            /*if (bycarpetAreamMin != null) {
                filterBeanDataModel.carpetAreaMin = bycarpetAreamMin
                FiltersSaveRemoveData.savePriceFilter(bycarpetAreamMin!!,categoryId.toString(),cId.toString(),"b1")
            }
            if (bycarpetAreaMax != null) {
                filterBeanDataModel.carpetAreaMax = bycarpetAreaMax
                FiltersSaveRemoveData.savePriceFilter(bycarpetAreaMax!!,categoryId.toString(),cId.toString(),"b2")
            }*/

            if (bycarpetAreamMin != null) {
                filterBeanDataModel.carpetAreaMin = bycarpetAreamMin
                //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")

                FiltersSaveRemoveData.savePriceFilter(bycarpetAreamMin!!, categoryId.toString(), cId.toString(), "b1")
            } else {
                if (bycarpetAreamMin.toString().equals("null")) {
                    filterBeanDataModel.carpetAreaMin = lowcarpetarea
                    bycarpetAreamMin = lowcarpetarea
                    FiltersSaveRemoveData.savePriceFilter(bycarpetAreamMin!!, categoryId.toString(), cId.toString(), "b1")
                } else {
                    filterBeanDataModel.carpetAreaMin = ""

                    //savePriceFilter(byPriceMin,categoryId.toString(),cId.toString(),"p1")
                    FiltersSaveRemoveData.savePriceFilter(bycarpetAreamMin!!, categoryId.toString(), cId.toString(), "b1")
                }
            }

            if (bycarpetAreaMax != null) {
                filterBeanDataModel.carpetAreaMax = bycarpetAreaMax
                //savePriceFilter(byPriceMax,categoryId.toString(),cId.toString(),"p2")

                FiltersSaveRemoveData.savePriceFilter(bycarpetAreaMax!!, categoryId.toString(), cId.toString(), "b2")
            } else {
                if (bycarpetAreaMax.toString().equals("null")) {
                    filterBeanDataModel.carpetAreaMax = highcarpetarea
                    bycarpetAreaMax = highcarpetarea
                    FiltersSaveRemoveData.savePriceFilter(bycarpetAreaMax!!, categoryId.toString(), cId.toString(), "b2")
                } else {
                    filterBeanDataModel.carpetAreaMax = ""
                    //savePriceFilter(byPriceMax,categoryId.toString(),cId.toString(),"p2")

                    FiltersSaveRemoveData.savePriceFilter(bycarpetAreaMax!!, categoryId.toString(), cId.toString(), "b2")
                }
            }


            lateinit var getListingController: GetListingController
            getListingController = GetListingController(activity!!, this)
            //getListingController.callforDisplayfilterData(filterBeanDataModel, url)

            bottomSheetListener!!.callApiWithFilter(filterBeanDataModel, url.toString())
            dismiss()

            //set data for save
            FiltersSaveRemoveData.saveFilterDataList(propertyListedByList, categoryId.toString(), cId.toString(), "2")
            FiltersSaveRemoveData.saveFilterDataList(propertyTypesList, categoryId.toString(), cId.toString(), "1")
            FiltersSaveRemoveData.saveFilterDataList(propertyByBedroomList, categoryId.toString(), cId.toString(), "3")
            FiltersSaveRemoveData.saveFilterDataList(propertyByBathroomList, categoryId.toString(), cId.toString(), "4")
            FiltersSaveRemoveData.saveFilterDataList(propertyByFunishingList, categoryId.toString(), cId.toString(), "5")
            FiltersSaveRemoveData.saveFilterDataList(propertyByConstructionList, categoryId.toString(), cId.toString(), "6")
            FiltersSaveRemoveData.saveFilterDataList(propertyByBachelorAllowList, categoryId.toString(), cId.toString(), "7")
            FiltersSaveRemoveData.saveFilterDataList(propertyByPurposeList, categoryId.toString(), cId.toString(), "8")
            FiltersSaveRemoveData.saveFilterDataList(propertyByPremiumList, categoryId.toString(), cId.toString(), "9")
        }

        btn_clear.setOnClickListener {
            dismiss()
            lateinit var filterBeanDataModel: FilterPropertyBean
            filterBeanDataModel = FilterPropertyBean()
            filterBeanDataModel.categoryId = categoryId.toString()
            filterBeanDataModel.subCategoryId = cId.toString()
            lateinit var getListingController: GetListingController
            getListingController = GetListingController(activity!!, this)
            //getListingController.callforDisplayfilterData(filterBeanDataModel, url)

            bottomSheetListener!!.callApiWithFilter(filterBeanDataModel, url.toString())

            FiltersSaveRemoveData.cleardata(categoryId.toString(), cId.toString())

            Utils.showToast(context as Activity?, getString(R.string.txt_filter_reset_done))
        }

        iv_close.setOnClickListener {
            dismiss()
        }

        setdefaultFragment(cname)
    }

    private fun addSelectedData(list: ArrayList<FilterBean>, typeGet: String): ArrayList<String> {
        val selectedList: ArrayList<String> = arrayListOf()
        for (i in 0 until list.size) {
            if (list[i].selected) {
                if (typeGet == "name") {
                    selectedList.add(list[i].name)
                } else {
                    selectedList.add(list[i].id.toString())
                }
            }
        }
        return selectedList
    }

    private fun addSelectedDataPrimumsend(list: ArrayList<FilterBean>, typeGet: String): ArrayList<String>? {
        val selectedList: ArrayList<String>? = arrayListOf()
        for (i in 0 until list.size) {
            if (list[i].selected) {
                if (list[i].id == 1) {
                    // selectedList!!.add("Premium")
                    selectedList!!.add("1")
                } else if (list[i].id == 2) {
                    //selectedList!!.add("Free")
                    selectedList!!.add("0")
                }
            }
        }
        return selectedList
    }

    private fun setdefaultFragment(cname: String?) {
        if (cId!!.equals(1) || cId!!.equals(2)
                || cId!!.equals(5) || cId!!.equals(7) || cId!!.equals(8) || cId!!.equals(9)) {

            val bundle = Bundle()
            bundle.putString("ccname", cname)
            bundle.putInt("cid", cId!!)
            val data = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "1")
            if (data.toString() == "null") {
                // Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            } else {

                propertyTypesList.clear()
                propertyTypesList.addAll(FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "1") as Collection<FilterBean>)
            }

            bundle.putParcelableArrayList("propertyTypesList", propertyTypesList as java.util.ArrayList<out Parcelable>)
            changeFilterFragment(ByPropertyTypeFragment.newInstance(this), ByPropertyTypeFragment.TAG, bundle, true)
        }

        if (cId!!.equals(6))
        {
            val bundle = Bundle()
            bundle.putString("ccname", cname)
            bundle.putInt("cid", cId!!)
            val data = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "8")
            if (data.toString() == "null") {
                //Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            } else {
                propertyByPurposeList.clear()
                propertyByPurposeList.addAll(FiltersSaveRemoveData.getFilterData(
                        categoryId.toString(),
                        cId.toString(),
                        "8"
                ) as Collection<FilterBean>)
            }

            bundle.putParcelableArrayList(
                    "puposerlist",
                    propertyByPurposeList as java.util.ArrayList<out Parcelable>
            )
            changeFilterFragment(
                    ByPurposeFragment.newInstance(this),
                    ByPurposeFragment.TAG,
                    bundle,
                    true
            )
        }

        if (cId!!.equals(3) || cId!!.equals(4)) {

            val lowPrice = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p1")
            var highPrice = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p2")
            if (lowPrice.toString() == "null") {
                //  Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            } else {
                byPriceMin = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p1")
                byPriceMax = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p2")
            }

            val bundle = Bundle()
            bundle.putString("ccname", cname)
            bundle.putInt("cid", cId!!)
            bundle.putString("minPrice", byPriceMin)
            bundle.putString("maxPrice", byPriceMax)
            changeFilterFragment(
                    ByPropertyPriceFragment.newInstance(this),
                    ByPropertyPriceFragment.TAG,
                    bundle,
                    true
            )
        }
        if (cId == 1002) {
            val bundle = Bundle()
            bundle.putString("ccname", cname)
            val data = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "9")
            if (data.toString() == "null") {
                // Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            } else {

                propertyByPremiumList.clear()
                propertyByPremiumList.addAll(FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "9") as Collection<FilterBean>)
            }
            bundle.putParcelableArrayList("propertyByPremiumList", propertyByPremiumList as java.util.ArrayList<out Parcelable>)
            changeFilterFragment(ByPremiumFragment.newInstance(this), ByPremiumFragment.TAG, bundle, true)
        }
    }

    private fun bindRecyclerview() {
        val list = ArrayList<FilterBean>()
        val comman = CommanPropertyTypeListFilter()
        val layoutManager = LinearLayoutManager(context)
        rv_filter.layoutManager = layoutManager
        val adapter = FilterAdapter(comman.typeList(list, cname, cId!!), context!!)
        rv_filter.addItemDecoration(
                DividerItemDecoration(
                        context,
                        LinearLayout.VERTICAL
                ) as RecyclerView.ItemDecoration
        );
        adapter.setClicklistner(this)
        rv_filter.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    interface BottomSheetListener {
        fun onButtonClicked(
                text: String,
                type: String,
                cname: ArrayList<ProductListHouseBean.ProductListHouse>
        )

        fun callApiWithFilter(filterBeanDataModel: FilterPropertyBean, url: String)
    }

    override fun itemclickView(bean: FilterBean) {
        val bundle = Bundle()

        if (bean.id == 9) {
            bundle.putString("ccname", cname)
            bundle.putInt("cid", cId!!)

            val data = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "1")
            if (data.toString() == "null") {
                // Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            } else {
                propertyTypesList.clear()
                propertyTypesList.addAll(FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "1") as Collection<FilterBean>)
            }

            bundle.putParcelableArrayList("propertyTypesList", propertyTypesList as java.util.ArrayList<out Parcelable>)
            changeFilterFragment(ByPropertyTypeFragment.newInstance(this), ByPropertyTypeFragment.TAG, bundle, true)
        }

        if (bean.id == 10) {

            val lowPrice = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p1")
            var highPrice = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p2")
            if (lowPrice.toString() == "null") {
                //  Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            } else {
                byPriceMin = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p1")
                byPriceMax = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p2")
            }


            bundle.putString("ccname", cname)
            bundle.putInt("cid", cId!!)
            bundle.putString("minPrice", byPriceMin)
            bundle.putString("maxPrice", byPriceMax)
            changeFilterFragment(
                    ByPropertyPriceFragment.newInstance(this),
                    ByPropertyPriceFragment.TAG,
                    bundle,
                    true
            )
        }

        if (bean.id == 100) {
            val lowPrice = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p1")
            var highPrice = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p2")
            if (lowPrice.toString() == "null") {
                //  Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            } else {
                byPriceMin = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p1")
                byPriceMax = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "p2")
            }

            bundle.putString("ccname", cname)
            bundle.putInt("cid", cId!!)
            bundle.putString("minPrice", byPriceMin)
            bundle.putString("maxPrice", byPriceMax)
            changeFilterFragment(
                    ByApvillSellPricePropertyFragment.newInstance(this),
                    ByApvillSellPricePropertyFragment.TAG,
                    bundle,
                    true
            )

        }

        if (bean.id == 11) {
            bundle.putString("ccname", cname)
            bundle.putInt("cid", cId!!)
            val data = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "3")
            if (data.toString() == "null") {
                //  Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            } else {
                propertyByBedroomList.clear()
                propertyByBedroomList.addAll(FiltersSaveRemoveData.getFilterData(
                        categoryId.toString(),
                        cId.toString(),
                        "3"
                ) as Collection<FilterBean>)

            }
            bundle.putParcelableArrayList(
                    "propertyByBedroomList",
                    propertyByBedroomList as java.util.ArrayList<out Parcelable>
            )
            changeFilterFragment(
                    ByBedroomFragment.newInstance(this),
                    ByBedroomFragment.TAG,
                    bundle,
                    true
            )
        }

        if (bean.id == 12) {
            bundle.putString("ccname", cname)
            bundle.putInt("cid", cId!!)
            val data = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "4")
            if (data.toString() == "null") {
                //  Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            } else {
                propertyByBathroomList.clear()
                propertyByBathroomList.addAll(FiltersSaveRemoveData.getFilterData(
                        categoryId.toString(),
                        cId.toString(),
                        "4"
                ) as Collection<FilterBean>)

            }
            bundle.putParcelableArrayList(
                    "propertyByBathroomList",
                    propertyByBathroomList as java.util.ArrayList<out Parcelable>
            )
            changeFilterFragment(
                    ByBathroomFragment.newInstance(this),
                    ByBathroomFragment.TAG,
                    bundle,
                    true
            )
        }

        if (bean.id == 13) {
            bundle.putString("ccname", cname)
            bundle.putInt("cid", cId!!)

            //propertyListedByList.clear()
            val data = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "2")
            Log.e(TAG, "logdata: " + data)
            if (data.toString() == "null") {
                //  Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            } else {
                propertyListedByList.clear()
                propertyListedByList.addAll(FiltersSaveRemoveData.getFilterData(
                        categoryId.toString(),
                        cId.toString(),
                        "2"
                ) as Collection<FilterBean>)

            }
            bundle.putParcelableArrayList(
                    "propertyListedByList",
                    propertyListedByList as java.util.ArrayList<out Parcelable>
            )
            changeFilterFragment(
                    ListedByFragment.newInstance(this),
                    ListedByFragment.TAG,
                    bundle,
                    true
            )
        }

        if (bean.id == 14) {
            bundle.putString("ccname", cname)
            bundle.putInt("cid", cId!!)
            val data = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "6")
            if (data.toString() == "null") {
                //  Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            } else {
                propertyByConstructionList.clear()
                propertyByConstructionList.addAll(FiltersSaveRemoveData.getFilterData(
                        categoryId.toString(),
                        cId.toString(),
                        "6"
                ) as Collection<FilterBean>)
            }
            bundle.putParcelableArrayList(
                    "propertyByConstructionList",
                    propertyByConstructionList as java.util.ArrayList<out Parcelable>
            )

            changeFilterFragment(
                    ByConstructionStatusFragment.newInstance(this),
                    ByConstructionStatusFragment.TAG,
                    bundle,
                    true
            )
        }

        if (bean.id == 15) {
            bundle.putString("ccname", cname)
            bundle.putInt("cid", cId!!)
            val data = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "5")
            if (data.toString() == "null") {
                // Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            } else {
                propertyByFunishingList.clear()
                propertyByFunishingList.addAll(FiltersSaveRemoveData.getFilterData(
                        categoryId.toString(),
                        cId.toString(),
                        "5"
                ) as Collection<FilterBean>)
            }
            bundle.putParcelableArrayList(
                    "propertyByFunishingList",
                    propertyByFunishingList as java.util.ArrayList<out Parcelable>
            )

            changeFilterFragment(
                    ByFurnishingFragment.newInstance(this),
                    ByFurnishingFragment.TAG,
                    bundle,
                    true
            )
        }

        if (bean.id == 16) {
            val lowPrice = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "a1")
            var highPrice = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "a2")
            if (lowPrice.toString() == "null") {
                //  Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            } else {
                byAreamMin = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "a1")
                byAreaMax = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "a2")
            }

            bundle.putString("ccname", cname)
            bundle.putInt("cid", cId!!)
            bundle.putString("minPrice", byAreamMin)
            bundle.putString("maxPrice", byAreaMax)
            changeFilterFragment(ByAreaFragment.newInstance(this), ByAreaFragment.TAG, bundle, true)
        }

        if (bean.id == 17) {
            bundle.putString("ccname", cname)
            bundle.putInt("cid", cId!!)

            val data = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "9")
            if (data.toString() == "null") {
                // Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            } else {
                propertyByPremiumList.clear()
                propertyByPremiumList.addAll(FiltersSaveRemoveData.getFilterData(
                        categoryId.toString(),
                        cId.toString(),
                        "9"
                ) as Collection<FilterBean>)
            }

            bundle.putParcelableArrayList(
                    "propertyByPremiumList",
                    propertyByPremiumList as java.util.ArrayList<out Parcelable>
            )
            changeFilterFragment(
                    ByPremiumFragment.newInstance(this),
                    ByPremiumFragment.TAG,
                    bundle,
                    true
            )
        }

        if (bean.id == 18) {
            bundle.putString("ccname", cname)

            val data = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "7")
            if (data.toString() == "null") {
                //  Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            } else {
                propertyByBachelorAllowList.clear()
                propertyByBachelorAllowList.addAll(FiltersSaveRemoveData.getFilterData(
                        categoryId.toString(),
                        cId.toString(),
                        "7"
                ) as Collection<FilterBean>)
            }

            bundle.putParcelableArrayList(
                    "propertyByBachelorAllowList",
                    propertyByBachelorAllowList as java.util.ArrayList<out Parcelable>
            )
            changeFilterFragment(
                    ByBachelorsAllowedFragment.newInstance(this),
                    ByBachelorsAllowedFragment.TAG,
                    bundle,
                    true
            )
        }

        if (bean.id == 19) {

            val lowPrice = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "b1")
            var highPrice = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "b2")
            if (lowPrice.toString() == "null") {
                //  Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            } else {
                bycarpetAreamMin = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "b1")
                bycarpetAreaMax = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "b2")
            }

            val bundle = Bundle()
            bundle.putString("ccname", cname)
            bundle.putInt("cid", cId!!)
            bundle.putString("minPrice", bycarpetAreamMin)
            bundle.putString("maxPrice", bycarpetAreaMax)
            changeFilterFragment(ByCarpetAreaFragment.newInstance(this), ByCarpetAreaFragment.TAG, bundle, true)
        }

        if (bean.id == 20) {
            bundle.putString("ccname", cname)
            bundle.putInt("cid", cId!!)
            val data = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "8")
            if (data.toString() == "null") {
                //Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            } else {
                propertyByPurposeList.clear()
                propertyByPurposeList.addAll(FiltersSaveRemoveData.getFilterData(
                        categoryId.toString(),
                        cId.toString(),
                        "8"
                ) as Collection<FilterBean>)
            }

            bundle.putParcelableArrayList(
                    "puposerlist",
                    propertyByPurposeList as java.util.ArrayList<out Parcelable>
            )
            changeFilterFragment(
                    ByPurposeFragment.newInstance(this),
                    ByPurposeFragment.TAG,
                    bundle,
                    true
            )
        }

        if (bean.id == 21) {


            val lowPrice = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "l1")
            var highPrice = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "l2")
            if (lowPrice.toString() == "null") {
                //  Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            } else {
                landAreamMin = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "l1")
                landAreaMax = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "l2")
            }
            bundle.putString("ccname", cname)
            bundle.putString("minPrice", landAreamMin)
            bundle.putString("maxPrice", landAreaMax)
            changeFilterFragment(
                    ByLandAreaFragment.newInstance(this),
                    ByLandAreaFragment.TAG,
                    bundle,
                    true
            )
        }

        if (bean.id == 22) {

            val lowPrice = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "g1")
            var highPrice = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "g2")
            if (lowPrice.toString() == "null") {
                //  Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()
            } else {
                guestMin = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "g1")
                guestMax = FiltersSaveRemoveData.getPriceData(categoryId.toString(), cId.toString(), "g2")
            }
            bundle.putString("ccname", cname)
            bundle.putString("minPrice", guestMin)
            bundle.putString("maxPrice", guestMax)
            changeFilterFragment(ByGuestCapcityFragment.newInstance(this), ByGuestCapcityFragment.TAG, bundle, true)
        }
    }

    private fun changeFilterFragment(fragment: Fragment, tag: String, args: Bundle?, add: Boolean) {
        val transaction = childFragmentManager.beginTransaction()
        if (args != null) fragment.arguments = args
        transaction.replace(R.id.fl_lmain, fragment, tag)
        transaction.commit()
    }

    override fun selected(typesList: List<FilterBean>, property: String) {
        if (property.equals("Listed By")) {
            addPropertyData(propertyListedByList, typesList)
        } else if (property.equals("Type")) {
            addPropertyData(propertyTypesList, typesList)
        } else if (property.equals("By Bedroom")) {
            addPropertyData(propertyByBedroomList, typesList)
        } else if (property.equals("By Bathroom")) {
            addPropertyData(propertyByBathroomList, typesList)
        } else if (property.equals("Construction Status")) {
            addPropertyData(propertyByConstructionList, typesList)
        } else if (property.equals("Premium Ad")) {
            addPropertyData(propertyByPremiumList, typesList)
        } else if (property.equals("By Furnishing")) {
            addPropertyData(propertyByFunishingList, typesList)
        } else if (property.equals("BachelorsAllow")) {
            addPropertyData(propertyByBachelorAllowList, typesList)
        } else if (property.equals("purpose")) {
            addPropertyData(propertyByPurposeList, typesList)
        }
    }

    private fun addPropertyData(
            listAdd: ArrayList<FilterBean>,
            typesList: List<FilterBean>
    ) {
        listAdd.clear()
        listAdd.addAll(typesList)
    }

    override fun selected(min: String, max: String, property: String) {
        if (property.equals("By Price")) {
            byPriceMax = max
            byPriceMin = min
        } else if (property.equals("By Area")) {
            byAreaMax = max
            byAreamMin = min
        } else if (property.equals("By Land Area")) {
            landAreaMax = max
            landAreamMin = min
        } else if (property.equals("By Guest")) {
            guestMax = max
            guestMin = min
        } else if (property.equals("By carpet")) {
            bycarpetAreamMin = min
            bycarpetAreaMax = max
        }

        savePremium()
    }

    private fun savePremium() {

        lateinit var filterBeanDataModel: FilterPropertyBean
        filterBeanDataModel = FilterPropertyBean()
        val data = FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "9")
        if (data.toString() == "null") {
            // Toast.makeText(requireContext(), "null", Toast.LENGTH_SHORT).show()

            filterBeanDataModel.property_byPremium_list = addSelectedDataPrimumsend(propertyByPremiumList, "name")
            FiltersSaveRemoveData.saveFilterDataList(propertyByPremiumList, categoryId.toString(), cId.toString(), "9")
        } else {
            propertyByPremiumList.clear()
            propertyByPremiumList.addAll(FiltersSaveRemoveData.getFilterData(categoryId.toString(), cId.toString(), "9") as Collection<FilterBean>)
            filterBeanDataModel.property_byPremium_list = addSelectedDataPrimumsend(propertyByPremiumList, "name")
            FiltersSaveRemoveData.saveFilterDataList(propertyByPremiumList, categoryId.toString(), cId.toString(), "9")
        }
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (method == "Get Product") {
            Log.e(TAG, "onSuccess: " + ProductListHouseBean())
            plist.clear()
            val dataModel = ProductListHouseBean()
            dataModel.ProductListHouseBean(da)
            plist = dataModel.productlist
        }

        bottomSheetListener!!.onButtonClicked("Clicked", "apply", plist)
        dismiss()

    }

    override fun onFail(msg: String, method: String) {
        bottomSheetListener!!.onButtonClicked("Clicked", msg, plist)
        dismiss()
    }
}
