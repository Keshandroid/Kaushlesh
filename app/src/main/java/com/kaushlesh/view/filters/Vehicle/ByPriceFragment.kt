package com.kaushlesh.view.filters.Vehicle

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.kaushlesh.R
import com.yahoo.mobile.client.android.util.rangeseekbar.RangeSeekBar
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener
import android.widget.TextView
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.view.filters.Bikes.ByPriceBikesFragment
import kotlinx.android.synthetic.main.fragment_by_area.*


class ByPriceFragment : Fragment(), View.OnClickListener {

    var cname: String? = null
    var byPriceMin: String? = null
    var byPriceMax: String? = null
    internal lateinit var rangeSeekbar: CrystalRangeSeekbar
    internal lateinit var tvMin: TextView
    internal lateinit var tvMax: TextView
    var cid: Int? = null
    private var bottomSheetListener: ByPriceFragment.BottomSheetListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_by_price, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null) {
            cname = arguments!!.getString("ccname").toString()
            cid = arguments!!.getInt("cid")
        }
        // get seekbar from view
        rangeSeekbar = view.findViewById(R.id.rangeSeekbar) as CrystalRangeSeekbar
// get min and max text view
        tvMin = view.findViewById(R.id.tvmin) as TextView
        tvMax = view.findViewById(R.id.tvmax) as TextView

        if (cid==18) {
            rangeSeekbar.setMinValue(0F)
            rangeSeekbar.setMaxValue(1000000F)
        }

        if (cid==19) {
            rangeSeekbar.setMinValue(0F)
            rangeSeekbar.setMaxValue(1000000F)
        }
        if (cid==20) {
            rangeSeekbar.setMinValue(0F)
            rangeSeekbar.setMaxValue(25000F)
        }
        if (arguments != null) {
            var minfound = arguments!!.getString("minPrice").toString()
            if (!minfound.equals("null")) {
                byPriceMin = arguments!!.getString("minPrice")
                byPriceMax = arguments!!.getString("maxPrice")

                if(!byPriceMin!!.isEmpty() && !byPriceMax!!.isEmpty()) {
                    rangeSeekbar.setMinStartValue(byPriceMin!!.toFloat()).setMaxStartValue(byPriceMax!!.toFloat()).apply()
                }

                rangeSeekbar.setOnRangeSeekbarChangeListener { minValue, maxValue ->
                    SetTextviewValues(minValue, maxValue)
                }
                rangeSeekbar.setOnClickListener(this)

                rangeSeekbar.setOnRangeSeekbarFinalValueListener { minValue, maxValue ->
                    //bottomSheetListener!!.selected(minValue.toString(), maxValue.toString(), "By Price")
                    checkmaxValueandSet(minValue.toInt(),maxValue.toInt())
                }

            } else {
                rangeSeekbar.setOnRangeSeekbarChangeListener { minValue, maxValue ->
                    SetTextviewValues(minValue, maxValue)
                }
                rangeSeekbar.setOnClickListener(this)


                rangeSeekbar.setOnRangeSeekbarFinalValueListener { minValue, maxValue ->
                    //bottomSheetListener!!.selected(minValue.toString(), maxValue.toString(), "By Price")
                    checkmaxValueandSet(minValue.toInt(),maxValue.toInt())
                }
            }
        }

    }

    private fun checkmaxValueandSet(minValue: Int, maxValue: Int) {

        if(maxValue == 1000000 || maxValue == 25000)
        {
            bottomSheetListener!!.selected(
                    minValue.toString(),
                    (maxValue * 1000).toString(),
                    "By Price")
        }
        else {

            bottomSheetListener!!.selected(
                    minValue.toString(),
                    maxValue.toString(),
                    "By Price")
        }
    }

    private fun SetTextviewValues(minValue: Number?, maxValue: Number?) {
        tvMin.text = minValue.toString() + ""
        tvMax.text = maxValue.toString() + "+"
    }

    companion object {
        val TAG = "ByPriceFragment"

        fun newInstance(): ByPriceFragment {
            return ByPriceFragment()
        }

        fun newInstance(listener: ByPriceFragment.BottomSheetListener?): ByPriceFragment {
            val fragment = ByPriceFragment()
            fragment.bottomSheetListener = listener
            return fragment
        }
    }

    interface BottomSheetListener {
        fun selected(min: String, max: String, property: String)
    }

    override fun onClick(v: View?) {
    }
}
