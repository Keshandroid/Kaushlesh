package com.kaushlesh.Controller

import android.app.Activity
import android.util.Log
import com.kaushlesh.R
import com.kaushlesh.api.API
import com.kaushlesh.bean.RegisterPhoneBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.parser.PostRequestParsing
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import org.json.JSONObject
import java.util.HashMap

class DeleteMultiAdPostsController(activity: Activity, poststatus: String,listener: ParseControllerListener) : ParseControllerListener {

    private val TAG = "DeleteMultiAdPostsController"
    internal var activity: Activity = activity
    internal var registerBean: RegisterPhoneBean? = null
    //internal var dbHelper: DBHelper? = dbHelper
    private  var listener: ParseControllerListener = listener

    val storeusedata = StoreUserData(activity)
    val userid = storeusedata.getString(Constants.USER_ID)
    val token = storeusedata.getString(Constants.TOKEN)

    init {
        Log.i(TAG, "onClick")

        val url = API.DELETE_MULTIPLE_POST

        AppConstants.printLog(TAG, "DELETE_MULTIPLE_POST URL ==>$url")

        val p = PostRequestParsing()

        val params = HashMap<String, String>()

        params.put("userId", userid.toString());
        params.put("userToken", token.toString());
        params.put("post_status",poststatus.toString());


        p.call(activity,url,"ads",params,this);

        AppConstants.printLog(TAG, "ads : " + params);

        showProgressDialog(activity, activity.resources.getString(R.string.txt_please_wait))
    }



    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (da.length() > 0) {
            Log.e("Result-", "$da==")
            dismissProgressDialog()
            try {
                Utils.showLog(TAG,message)
                listener.onSuccess(da,message,method)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            dismissProgressDialog()
            //  listener.onFail(message,method)
        }
    }

    override fun onFail(msg: String, method: String) {
        dismissProgressDialog()
        Utils.showLog(TAG,msg)
        listener.onFail(msg,method)
        //Toast.makeText(activity!!, ""+message, Toast.LENGTH_SHORT).show()

        // listener.onFail(message,method)
    }

    fun showProgressDialog(activity: Activity, msg: String) {
        if (Utils.progressDialog==null){
            Utils.showProgress(activity)
        }
    }

    fun dismissProgressDialog() {

        if (Utils.progressDialog != null && Utils.progressDialog!!.isShowing()) {
            Utils.dismissProgress()
        }
    }

}