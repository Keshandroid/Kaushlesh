package com.kaushlesh.Controller

import android.app.Activity
import android.util.Log
import android.view.View
import com.kaushlesh.R
import com.kaushlesh.api.API
import com.kaushlesh.bean.RegisterPhoneBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.parser.PostRequestParsing
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import org.json.JSONObject
import java.util.HashMap

class AddContaintReportPostController(listener: ParseControllerListener, activity: Activity,postId: String, reporttype: String, message: String ) : View.OnClickListener, ParseControllerListener {

    private val TAG = "AddContaintReportPostController"
    internal var activity: Activity = activity
    internal var registerBean: RegisterPhoneBean? = null
    //internal var dbHelper: DBHelper? = dbHelper
    private  var listener: ParseControllerListener = listener

    val storeusedata = StoreUserData(activity)
    val userid = storeusedata.getString(Constants.USER_ID)
    val token = storeusedata.getString(Constants.TOKEN)
    val postId = postId
    val reporttype = reporttype
    val message = message

    override fun onClick(v: View) {
        Log.i("TAG", "onClick")

        val url = API.ADD_REPORT_POST

        AppConstants.printLog(TAG, "ADD_REPORT_POST URL ==>$url")
        AppConstants.hideInputSoftKey(activity)

        val p = PostRequestParsing()

        val params = HashMap<String, String>()

        params.put("userId", userid.toString());
        params.put("userToken", token.toString());
        params.put("postId",postId);
        params.put("report_type",reporttype );
        params.put("message", message);

        p.call(activity,url,"reportAd",params,this);

        AppConstants.printLog(TAG, "reportAd: " + params);

        showProgressDialog(activity, activity.resources.getString(R.string.txt_please_wait))
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (da.length() > 0) {
            Log.e("LoginResult-", "$da==")
            dismissProgressDialog()
            try {
                listener.onSuccess(da,message,method)

            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            dismissProgressDialog()
            listener.onFail(message,method)
        }
    }

    override fun onFail(msg: String, method: String) {
        dismissProgressDialog()
        listener.onFail(msg,method)
    }

    fun showProgressDialog(activity: Activity, msg: String) {
        if (Utils.progressDialog==null){
            Utils.showProgress(activity)
        }
    }

    fun dismissProgressDialog() {

        if (Utils.progressDialog != null && Utils.progressDialog!!.isShowing()) {
            Utils.dismissProgress()
        }
    }

}