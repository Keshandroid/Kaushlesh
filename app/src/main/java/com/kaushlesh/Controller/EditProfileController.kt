package com.kaushlesh.Controller

import android.app.Activity
import android.content.ContentValues.TAG
import android.util.Log
import com.android.volley.*
import com.kaushlesh.R
import com.kaushlesh.api.API
import com.kaushlesh.bean.UserProfileBean
import com.kaushlesh.constant.AppConstants.printLog
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.reusable.VolleyMultipartRequests
import com.kaushlesh.reusable.VolleySingleTon
import com.kaushlesh.utils.RegisterData
import com.kaushlesh.utils.Utils
import org.json.JSONException
import org.json.JSONObject
import java.net.URLEncoder
import java.util.*

class EditProfileController(var context: Activity, var parseControllerListener: ParseControllerListener) {

    fun editProfille(registerData: RegisterData, userModel: UserProfileBean) {

        showProgressDialog(context, context.resources.getString(R.string.txt_please_wait))
        val multipartRequest =
            object : VolleyMultipartRequests(Request.Method.POST, API.EDIT_PROFILE,
                Response.Listener<NetworkResponse> { response ->

                    Log.e("test", "response : $response")

                    dismissProgressDialog()

                    val resultResponse = String(response.data)
                    try {
                        val resultJson = JSONObject(resultResponse)

                        val status = resultJson.getString("status")
                        val msg = resultJson.getString("message")

                        if (status == "1") {

                            if(resultJson.has("result")) {
                                val data = resultJson.getJSONObject("result")

                                parseControllerListener.onSuccess(data,msg,"EditProfile")
                            }

                        }
                        if (status == "0") {

                            parseControllerListener.onFail(msg,"EditProfile")
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }, Response.ErrorListener { error ->
                    val networkResponse = error.networkResponse
                    var errorMessage = "Unknown error"

                Utils.dismissProgress() //new added

                    if (networkResponse == null) {
                        if (error.javaClass == TimeoutError::class.java) {
                            errorMessage = "Request timeout"
                        } else if (error.javaClass == NoConnectionError::class.java) {
                            errorMessage = "Failed to connect server"
                            Utils.showAlertConnection(context)
                        }
                    } else {
                        val result = String(networkResponse.data)
                        try {
                            val response = JSONObject(result)
                            val status = response.getString("status")
                            val message = response.getString("message")

                            Log.e("Error Status", status)
                            Log.e("Error Message", message)

                            if (networkResponse.statusCode == 404) {
                                errorMessage = "Resource not found"
                            } else if (networkResponse.statusCode == 401) {
                                errorMessage = "$message Please login again"
                            } else if (networkResponse.statusCode == 400) {
                                errorMessage = "$message Check your inputs"
                            } else if (networkResponse.statusCode == 500) {
                                errorMessage = "$message Something is getting wrong"
                            }

                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }

                    }

                    Log.i("Error", errorMessage)
                    error.printStackTrace()
                }) {
                override fun getParams(): Map<String, String> {
                    val params = HashMap<String, String>()
                    params["userId"] = userModel.userId.toString()
                    params["userToken"] = userModel.userToken.toString()
                    params["name"] = registerData.name
                    params["registration_type"] = userModel.registrationType.toString()
                    params["device_type"] = userModel.device_type.toString()
                    params["device_id"] = registerData.deviceid
                    params["device_token"] = registerData.devicetoken
                    params["phone"] = registerData.phone
                    params["email"] = registerData.email
                    params["fname"] = userModel.fname.toString()
                    params["lname"] = userModel.lname.toString()
                    params["address_street"] = userModel.address_street.toString()
                    params["address_apt"] = userModel.address_apt.toString()
                    params["city"] = userModel.city.toString()
                    params["state"] = userModel.state.toString()
                    params["zip"] = userModel.zip.toString()
                    params["about"] = registerData.aboutyou.toString()
                    params["isPhone"] = registerData.isPhone.toString()
                    params["location_id"] = registerData.locationid.toString()

                    params["whatsapp_inquiry_allow"] = registerData.whatsapp_inquiry_allow.toString()
                    params["same_whatsapp_no"] = registerData.same_whatsapp_no.toString()
                    params["whatsapp_no"] = registerData.whatsapp_no.toString()

                    params.keys.forEach {
                        if(it == "name" || it == "about" ){
                            params[it] = URLEncoder.encode(params[it],"UTF-8")
                        }
                    }

                    printLog("test", "Edit profile Param : $params")
                    return params
                }

                protected override val byteData: Map<String, DataPart>
                    get() {
                        val params = HashMap<String, DataPart>()

                        // file name could found file base or direct access from real path
                        // for now just get bitmap data from ImageView
                        if (registerData.image!= null) {
                            Log.i(TAG, "====profile drawable==" + "not null")
                            params["profilePicture"] = DataPart(
                                "file_profile.jpg",
                                Utils.getFileDataFromDrawable(context, registerData.image!!),
                                "image/jpeg"
                            )
                        }
                      /*  else{
                            params["profilePicture"] = DataPart(
                                "file_profile.jpg",   Utils.getFileDataFromDrawable(context, context.resources.getDrawable(R.drawable.ic_user_pic)),
                                "image/jpeg"
                            )
                        }*/
                        Log.i(TAG, "=======Profile param ========$params")

                        return params

                    }

            }
        VolleySingleTon.getInstance(context).addToRequestQueue(multipartRequest)
    }


    fun showProgressDialog(activity: Activity, msg: String) {
        if (Utils.progressDialog==null){
            Utils.showProgress(activity)
        }
    }

    fun dismissProgressDialog() {

        if (Utils.progressDialog != null && Utils.progressDialog!!.isShowing()) {
            Utils.dismissProgress()
        }
    }

}

