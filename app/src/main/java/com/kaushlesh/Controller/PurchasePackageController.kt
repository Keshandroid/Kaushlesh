package com.kaushlesh.Controller

import android.app.Activity
import android.preference.PreferenceManager
import android.util.Log
import com.kaushlesh.api.API
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.parser.PostRequestParsing
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.Identity
import com.kaushlesh.utils.StoreUserData
import org.json.JSONObject
import java.util.*

class PurchasePackageController(var context: Activity,
                                internal var packageid: String,
                                internal var transactionId: String,
                                internal var locid: String,
                                listener: ParseControllerListener) :ParseControllerListener{
    private val TAG = "PurchasePackageController"
    val storeusedata = StoreUserData(context)
    val userid = storeusedata.getString(Constants.USER_ID)
    val token = storeusedata.getString(Constants.TOKEN)
    val locationid = storeusedata.getString(Constants.LOC_ID_PKG)
    val url = API.PURCHASE_PACKAGE
    private  var listener: ParseControllerListener = listener

    fun purchasePackage() {

        val p = PostRequestParsing()

        val params = HashMap<String, String>()

        params.put("userId", userid.toString())
        params.put("userToken", token)
        params.put("packageId", packageid)
        params.put("transactionId", transactionId)

        if(locid == ""){
            params.put("location_id", locationid)
            storedatainlocal()
        }else{
            params.put("location_id", locid)
            //Store data in local for safe purchase and re upload to server from home screen
        }



        p.call(context, url, "purchasePkg", params, this);

        AppConstants.printLog(TAG, "purchase pkg param  : " + params);
    }

    private fun storedatainlocal() {
        val jsonObject = JSONObject()
        jsonObject.put("packageId", packageid)
        jsonObject.put("transactionId", transactionId)
        jsonObject.put("location_id", locationid)

        AppConstants.printLog(TAG, "PurchasePackageError " + jsonObject.toString());

        Identity.setPackagePurchase(context, jsonObject)
    }

    private fun clearPendingPackageDataFromLocal() {
        val mySPrefs = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = mySPrefs.edit()
        editor.remove("AUTO_PLAY_VIDEO_PREFERENCE")
        editor.apply()
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (da.length() > 0) {

            try {
                clearPendingPackageDataFromLocal()

                Log.d(TAG,"PurchasePackageError Uploaded successfully")

                listener.onSuccess(da, message, "purchasePkg")

            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            listener.onFail(message, "purchasePkg")
        }
    }

    override fun onFail(msg: String, method: String) {
        listener.onFail(msg, "purchasePkg")
    }

}