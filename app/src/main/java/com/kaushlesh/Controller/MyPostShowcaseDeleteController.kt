package com.kaushlesh.Controller

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.kaushlesh.R
import com.kaushlesh.interfaces.ParseControllerListener

class MyPostShowcaseDeleteController(var context: Activity, var parseControllerListener: ParseControllerListener) {
    internal lateinit var dialog: Dialog
    var a:Int = 0
    var b:Int = 0
    fun deleteAd(type: String) {
        showDialog(type)
    }

    fun add(s: String, postId: Int?) {
        showDialog(s)
        a= postId!!
    }

    fun addOrder(s: String, purchasedOrderId: Int, packageId: Int) {
        showDialog(s)
        a= purchasedOrderId
        b= packageId
    }

    private fun showDialog(type: String) {

        dialog = Dialog(context)
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_logout)
        // Set dialog title
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        //dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_white);
        // set values for custom dialog components - text, image and button
        val title = dialog.findViewById<TextView>(R.id.tv_title)
        val btnyes = dialog.findViewById<Button>(R.id.btn_yes)
        val btnno = dialog.findViewById<Button>(R.id.btn_no)

        if(type.equals("order"))
        {
            title.text =context.getString(R.string.txt_warning_delete_order)
        }
        else if(type.equals("post"))
        {
            title.text =context.getString(R.string.txt_warning_delete_ad)
        }

        else{
           title.text =context.getString(R.string.txt_warning_delete_showcase)
        }

        btnyes.setOnClickListener(View.OnClickListener {

            if(type.equals("order"))
            {
                //callApiToDeleteOrder()
                val c = DeleteOrderController(parseControllerListener, context, a.toString(),b.toString())
                c.onClick(btnyes)
            }
            else if(type.equals("post"))
            {
                //callApiToDeletePost()
                val c = DeletePostController(parseControllerListener, context, a.toString())
                c.onClick(btnyes)
            }
            else{
                //callApiToDeleteShowcase()
            }
            dialog.dismiss()
        })

        btnno.setOnClickListener(View.OnClickListener { dialog.dismiss() })

        dialog.show()
    }


}