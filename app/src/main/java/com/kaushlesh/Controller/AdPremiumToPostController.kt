package com.kaushlesh.Controller

import android.app.Activity
import com.kaushlesh.api.API
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.parser.PostRequestParsing
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import org.json.JSONObject
import java.util.HashMap

class AdPremiumToPostController(var context: Activity, internal var packageid: String, internal var postid: String, listener: ParseControllerListener) :
    ParseControllerListener {
    private val TAG = "AdPremiumToPostController"
    val storeusedata = StoreUserData(context)
    val userid = storeusedata.getString(Constants.USER_ID)
    val token = storeusedata.getString(Constants.TOKEN)
    val url = API.AD_PREMIUM_POST
    private  var listener: ParseControllerListener = listener

    fun purchasePackage() {

        val p = PostRequestParsing()

        val params = HashMap<String, String>()

        params.put("userId", userid.toString())
        params.put("userToken", token)
        params.put("packageId", packageid)
        params.put("postId", postid)
        params.put("userpostpurchasepackage_id", storeusedata.getString(Constants.USER_PKG_ID))

        p.call(context,url,"adPremium",params,this);

        AppConstants.printLog(TAG, "adPremium param  : " + params);
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (da.length() > 0) {

            try {
                listener.onSuccess(da,message,"adPremium")

            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            listener.onFail(message,"adPremium")
        }
    }

    override fun onFail(msg: String, method: String) {
        listener.onFail(msg,"adPremium")
    }

}