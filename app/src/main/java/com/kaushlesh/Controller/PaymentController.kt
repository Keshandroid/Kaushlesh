package com.kaushlesh.Controller

import android.app.Activity
import android.content.Intent
import com.kaushlesh.R
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.MainActivity
import com.razorpay.Checkout
import com.razorpay.PaymentResultListener
import org.json.JSONObject

class PaymentController(internal var activity: Activity,internal var listener: ParseControllerListener) :PaymentResultListener {

    val storeusedata = StoreUserData(activity)
    val name = storeusedata.getString(Constants.NAME)
    val email = storeusedata.getString(Constants.EMAIL)
    val contact_no = storeusedata.getString(Constants.CONTACT_NO)

    fun startPay() {

          val activity: Activity = activity
          val co = Checkout()
          co.setKeyID(activity.getString(R.string.key_razorpay))

        try {
            val options = JSONObject()
            if(name.equals("")){
                options.put("name", "Gujarat Living User")
            }else{
                options.put("name", ""+name)
            }
            options.put("description", "Amount")
            options.put("theme.color", "#000000");
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png")
            options.put("currency", "INR")
            options.put("amount", "14900")

            val prefill = JSONObject()
            prefill.put("email", ""+email)
            prefill.put("contact", ""+contact_no)

            options.put("prefill", prefill)
            co.open(activity, options)
        } catch (e: Exception) {
            Utils.showToast(activity,e.message)
            e.printStackTrace()
        }
    }

    override fun onPaymentError(errocode: Int, response: String?) {
        try {
            AppConstants.printToast(activity, "Payment failed: " + errocode.toString() + " " + response)
            AppConstants.printLog("test", response + "Exception in onPaymentSuccess" + errocode)

        } catch (e: java.lang.Exception) {
            AppConstants.printLog("test", "Exception in onPaymentError" + e)
        }

        listener.onFail(errocode.toString(),response.toString())
    }

    override fun onPaymentSuccess(razorpayPaymentId: String?) {
        try {
            AppConstants.printToast(activity, "Payment Successful: $razorpayPaymentId")

            //listener.onSuccess(jdata!!,razorpayPaymentId.toString(),"")
            val intent = Intent(activity, MainActivity::class.java)
            activity.startActivity(intent)
            activity.finishAffinity()

        } catch (e: java.lang.Exception) {
            AppConstants.printLog("test", "Exception in onPaymentSuccess"+e)
            listener.onFail(razorpayPaymentId.toString(),e.toString())
        }
    }
}