package com.kaushlesh.Controller

import android.app.Activity
import android.content.Context
import android.util.Log
import com.kaushlesh.R
import com.kaushlesh.api.API
import com.kaushlesh.bean.RegisterPhoneBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.parser.PostRequestParsing
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import org.json.JSONObject
import java.util.HashMap

class PostLiveAgainController(context: Context, postId: String,pkgId : String,listener: ParseControllerListener) : ParseControllerListener {

    private val TAG = "AddPostViewrController"
    val storeusedata = StoreUserData(context)
    val userid = storeusedata.getString(Constants.USER_ID)
    val token = storeusedata.getString(Constants.TOKEN)
    private  var listener: ParseControllerListener = listener
    init {
        Log.i(TAG, "onClick")

        val url = API.ADD_POST_LIVE_AGAIN

        AppConstants.printLog(TAG, "ADD_POST_LIVE_AGAIN URL ==>$url")

        val p = PostRequestParsing()

        val params = HashMap<String, String>()

        params.put("userId", userid.toString())
        params.put("userToken", token.toString())
        params.put("post_id",postId)
        params.put("package_id",pkgId)
        params.put("userpostpurchasepackage_id", storeusedata.getString(Constants.USER_PKG_ID))

        p.callarray(context as Activity,url,"rePost",params,this);

        AppConstants.printLog(TAG, "repost: " + params);

        showProgressDialog(context, context.resources.getString(R.string.txt_please_wait))
    }



    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (da.length() > 0) {
            Log.e("repost Result-", "$da==")
            dismissProgressDialog()
            try {
                listener.onSuccess(da,message,method)

            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            dismissProgressDialog()
            listener.onFail(message,method)
        }
    }

    override fun onFail(msg: String, method: String) {
        dismissProgressDialog()
        listener.onFail(msg,method)
    }

    fun showProgressDialog(activity: Activity, msg: String) {
        if (Utils.progressDialog==null){
            Utils.showProgress(activity)
        }
    }

    fun dismissProgressDialog() {

        if (Utils.progressDialog != null && Utils.progressDialog!!.isShowing()) {
            Utils.dismissProgress()
        }

    }
}