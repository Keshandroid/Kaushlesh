package com.kaushlesh.Controller

import android.app.Activity
import android.util.Log
import android.view.View
import com.kaushlesh.R
import com.kaushlesh.api.API
import com.kaushlesh.bean.RegisterPhoneBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.parser.PostRequestParsing
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import org.json.JSONObject
import java.util.HashMap

class EditPhoneNumberController(listener: ParseControllerListener,
                                activity: Activity,
                                userId: String, phone: String, userToken: String) : View.OnClickListener,
    ParseControllerListener {
   // var mProgressDialog: Utils.? = null
    private val TAG = "EditPhoneController"
    internal var activity: Activity = activity
    internal var registerBean: RegisterPhoneBean? = null
    //internal var dbHelper: DBHelper? = dbHelper
    private  var listener: ParseControllerListener = listener
    internal var userid = userId
    internal var userToken = userToken
    internal var phone = phone

    override fun onClick(v: View) {
        Log.i(TAG, "onClick")

        val url = API.EDIT_PHONE_NUMBER

        AppConstants.printLog(TAG, "LOGIN URL ==>$url")

        //dbHelper = DBHelper.getInstance(activity)
        //dbHelper!!.opendb()
        //registerBean = dbHelper!!.getRegisterData()
        //printLog(TAG, "==GET REGISTER DATA===$registerBean")
        // dbHelper!!.closedb()


        AppConstants.hideInputSoftKey(activity)

        val p = PostRequestParsing()

        val params = HashMap<String, String>()

        params.put("userId", userid.toString());
        params.put("userToken", userToken.toString());
        params.put("phone", phone);

        p.call(activity,url,"EditNumber",params,this);

        AppConstants.printLog(TAG, "Login param  : " + params);

        showProgressDialog(activity, activity.resources.getString(R.string.txt_please_wait))
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (da.length() > 0) {
            Log.e("LoginResult-", "$da==")

            dismissProgressDialog()
            try {
                listener.onSuccess(da,message,method)

            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            dismissProgressDialog()
            listener.onFail(message,method)
        }
    }

    override fun onFail(msg: String, method: String) {
        dismissProgressDialog()
        listener.onFail(msg,method)
    }


    private fun setAPValueByKey(
        userId: String?,
        token: String?,
        isloggedin: String,
        isOTPVerified: String,
        isActive: String
    ) {
        val storeusedata = StoreUserData(activity)
        storeusedata.setString(Constants.USER_ID, userId.toString())
        storeusedata.setString(Constants.TOKEN, token.toString())
        storeusedata.setString(Constants.IS_LOGGED_IN, isloggedin.toString())
        storeusedata.setString(Constants.IS_OTP_VERIFED, isOTPVerified.toString())
        storeusedata.setString(Constants.IS_ACTIVE, isActive.toString())
        storeusedata.setString(Constants.DEVICE_TOKEN,"")
        storeusedata.setString(Constants.DEVICE_TYPE, "android")
        storeusedata.setString(Constants.REGISTREATION_TYPE, "")

    }

    fun showProgressDialog(activity: Activity, msg: String) {
        if (Utils.progressDialog==null){
            Utils.showProgress(activity)
        }
    }

    fun dismissProgressDialog() {

        if (Utils.progressDialog != null && Utils.progressDialog!!.isShowing()) {
            Utils.dismissProgress()
        }
    }

}