package com.kaushlesh.Controller

import android.app.Activity
import android.content.ContentValues.TAG
import android.content.Context
import android.os.Build
import android.provider.Settings
import android.util.Base64
import android.util.Log
import com.android.volley.*
import com.kaushlesh.R
import com.kaushlesh.api.API
import com.kaushlesh.constant.AppConstants.printLog
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.reusable.VolleyMultiPartRequest
import com.kaushlesh.reusable.VolleyMultipartRequests
import com.kaushlesh.reusable.VolleySingleTon
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.RegisterData
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap

class SocialLoginContoller(var context: Activity, var parseControllerListener: ParseControllerListener) {

    var deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID)
    internal var devicetype:String = "android"


    fun facebookLogin(registerData: RegisterData) {

        showProgressDialog(context, context.resources.getString(R.string.txt_please_wait))
        val multipartRequest =
            object : VolleyMultipartRequests(Request.Method.POST, API.SOCIAL_LOGIN,
                Response.Listener<NetworkResponse> { response ->

                    Log.e("test", "response : $response")

                    dismissProgressDialog()

                    val resultResponse = String(response.data)
                    try {
                        val resultJson = JSONObject(resultResponse)

                        val status = resultJson.getString("status")
                        val msg = resultJson.getString("message")

                        if (status == "1") {

                            if(resultJson.has("result")) {
                                val data = resultJson.getJSONObject("result")
                                setAPValueByKey(data.getString("userid"),data.getString("userToken"),"false",data.getString("isOTPVerified"),data.getString("isActive"),"facebook")

                                parseControllerListener.onSuccess(data,msg,"fblogin")
                            }

                        }
                        if (status == "0") {

                            parseControllerListener.onFail(msg,"fblogin")
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }, Response.ErrorListener { error ->
                    val networkResponse = error.networkResponse
                    var errorMessage = "Unknown error"

                Utils.dismissProgress() //new added

                    if (networkResponse == null) {
                        if (error.javaClass == TimeoutError::class.java) {
                            errorMessage = "Request timeout"
                        } else if (error.javaClass == NoConnectionError::class.java) {
                            errorMessage = "Failed to connect server"
                            Utils.showAlertConnection(context)
                        }
                    } else {
                        val result = String(networkResponse.data)
                        try {
                            val response = JSONObject(result)
                            val status = response.getString("status")
                            val message = response.getString("message")

                            Log.e("Error Status", status)
                            Log.e("Error Message", message)

                            if (networkResponse.statusCode == 404) {
                                errorMessage = "Resource not found"
                            } else if (networkResponse.statusCode == 401) {
                                errorMessage = "$message Please login again"
                            } else if (networkResponse.statusCode == 400) {
                                errorMessage = "$message Check your inputs"
                            } else if (networkResponse.statusCode == 500) {
                                errorMessage = "$message Something is getting wrong"
                            }

                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }

                    }

                    Log.i("Error", errorMessage)
                    error.printStackTrace()
                }) {
                override fun getParams(): Map<String, String> {
                        val params = HashMap<String, String>()
                        params["email"] = registerData.email
                        params["registration_type"] = registerData.socialtype
                        params["social_id"] = registerData.socialid
                        params["social_token"] = registerData.socialtoken
                        params["phone"] = registerData.phone
                        params["fname"] = registerData.name
                        params["registerfrom"] = "android" //new added

                        //Device name
                        params["deviceName"] = Build.MODEL;

                        //Device OS version
                        params["versionName"] = Build.VERSION.RELEASE;

                        printLog("test", "Add profile Param : $params")
                        return params
                    }

                protected override val byteData: Map<String, DataPart>
                    get() {
                        val params = HashMap<String, DataPart>()

                        Log.i(TAG, "====profile drawable==" + "not null")

                        params.put(
                            "profilePicture",
                            DataPart(
                                "file_profile.jpg", Utils.getFileDataFromDrawable(registerData.profile), "image/jpeg")
                        )

                        Log.i(TAG, "=======Profile param ========$params")

                        return params

                    }

            }
        VolleySingleTon.getInstance(context).addToRequestQueue(multipartRequest)
    }

    fun googleLogin(registerData: RegisterData) {

        Utils.showProgress(context) // new added
        //showProgressDialog(context, context.resources.getString(R.string.txt_please_wait)) // removed
        val multipartRequest =
            object : VolleyMultipartRequests(Request.Method.POST, API.SOCIAL_LOGIN,
                Response.Listener<NetworkResponse> { response ->

                    Log.e("test", "response : $response")

                    //dismissProgressDialog() // removed

                    val resultResponse = String(response.data)
                    try {
                        val resultJson = JSONObject(resultResponse)

                        val status = resultJson.getString("status")
                        val msg = resultJson.getString("message")

                        if (status == "1") {

                            if(resultJson.has("result")) {
                                val data = resultJson.getJSONObject("result")
                                setAPValueByKey(data.getString("userid"),data.getString("userToken"),"false",data.getString("isOTPVerified"),data.getString("isActive"),"google")

                                parseControllerListener.onSuccess(data,msg,"googlelogin")
                            }

                        }
                        if (status == "0") {

                            parseControllerListener.onFail(msg,"googlelogin")
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }, Response.ErrorListener { error ->
                    val networkResponse = error.networkResponse
                    var errorMessage = "Unknown error"

                Utils.dismissProgress() //new added

                    if (networkResponse == null) {
                        if (error.javaClass == TimeoutError::class.java) {
                            errorMessage = "Request timeout"
                        } else if (error.javaClass == NoConnectionError::class.java) {
                            errorMessage = "Failed to connect server"
                        }
                    } else {
                        val result = String(networkResponse.data)
                        try {
                            val response = JSONObject(result)
                            val status = response.getString("status")
                            val message = response.getString("message")

                            Log.e("Error Status", status)
                            Log.e("Error Message", message)

                            if (networkResponse.statusCode == 404) {
                                errorMessage = "Resource not found"
                            } else if (networkResponse.statusCode == 401) {
                                errorMessage = "$message Please login again"
                            } else if (networkResponse.statusCode == 400) {
                                errorMessage = "$message Check your inputs"
                            } else if (networkResponse.statusCode == 500) {
                                errorMessage = "$message Something is getting wrong"
                            }

                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }

                    }

                    Log.i("Error", errorMessage)
                    error.printStackTrace()
                }) {
                override fun getParams(): Map<String, String> {
                    val params = HashMap<String, String>()
                    params["email"] = registerData.email
                    params["registration_type"] = registerData.socialtype
                    params["social_id"] = registerData.socialid
                    params["social_token"] = registerData.socialtoken
                    params["phone"] = registerData.phone
                    params["fname"] = registerData.name
                    params["device_token"] = registerData.devicetoken

                    params["registerfrom"] = "android" // new added


                    params["device_type"] = devicetype
                    params["device_id"] = deviceId

                    //Device name
                    params["deviceName"] =  Build.MODEL;

                    //Device OS version
                    params["versionName"] = Build.VERSION.RELEASE;


                    printLog("test", "Add profile Param : $params")
                    return params
                }

                protected override val byteData: Map<String, DataPart>
                    get() {
                        val params = HashMap<String, DataPart>()

                        Log.i(TAG, "====profile drawable==" + "not null")

                        params.put(
                            "profilePicture",
                            DataPart(
                                "file_profile.jpg", Utils.getFileDataFromDrawable(registerData.profile), "image/jpeg")
                        )

                        Log.i(TAG, "=======Profile param ========$params")

                        return params

                    }

            }
        VolleySingleTon.getInstance(context).addToRequestQueue(multipartRequest)
    }

    fun showProgressDialog(activity: Activity, msg: String) {
        if (Utils.progressDialog==null){
            Utils.showProgress(activity)
        }
    }

    fun dismissProgressDialog() {

        if (Utils.progressDialog != null && Utils.progressDialog!!.isShowing()) {
            Utils.dismissProgress()
        }
    }

    private fun setAPValueByKey(
        userId: String?,
        token: String?,
        isloggedin: String,
        isOTPVerified: String,
        isActive: String,
        registrationType : String
    ) {


        val storeusedata = StoreUserData(context)
        storeusedata.setString(Constants.USER_ID, userId.toString())
        storeusedata.setString(Constants.TOKEN, token.toString())
        storeusedata.setString(Constants.IS_LOGGED_IN, isloggedin.toString())
        storeusedata.setString(Constants.IS_OTP_VERIFED, isOTPVerified.toString())
        storeusedata.setString(Constants.IS_ACTIVE, isActive.toString())
        storeusedata.setString(Constants.DEVICE_TOKEN,"")
        storeusedata.setString(Constants.DEVICE_TYPE, "android")
        storeusedata.setString(Constants.REGISTREATION_TYPE, registrationType.toString())
    }
}