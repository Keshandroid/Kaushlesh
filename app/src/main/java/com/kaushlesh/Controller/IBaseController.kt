package com.kaushlesh.Controller

import java.lang.Exception

open interface IBaseController {
    fun destroy();
}
