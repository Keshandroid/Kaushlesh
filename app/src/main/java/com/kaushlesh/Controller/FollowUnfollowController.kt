package com.kaushlesh.Controller

import android.app.Activity
import android.util.Log
import com.kaushlesh.api.API
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.parser.PostRequestParsing
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import org.json.JSONObject
import java.util.HashMap

class FollowUnfollowController(var context: Activity, var followingid: String,listener: ParseControllerListener) :ParseControllerListener{
    private val TAG = "FollowUnfollowController"
    val storeusedata = StoreUserData(context)
    val userid = storeusedata.getString(Constants.USER_ID)
    val token = storeusedata.getString(Constants.TOKEN)
    private  var listener: ParseControllerListener = listener
    val followingId = followingid

    fun removeFromFollow() {

        val url = API.REMOVE_FROM_FOLLOW

        AppConstants.printLog(TAG, "REMOVE FROM FOLLOW URL ==>$url")

        val p = PostRequestParsing()

        val params = HashMap<String, String>()

        params.put("userId", userid.toString())
        params.put("userToken", token.toString())
        params.put("followingId",followingId)
        params.put("followerId",userid)

        p.call(context,url,"removeFollow",params,this)

        AppConstants.printLog(TAG, "removeFollow: " + params);
    }

    fun addToFollow() {
        val url = API.ADD_TO_FOLLOW

        AppConstants.printLog(TAG, "ADD TO FOLLOW URL ==>$url")

        val p = PostRequestParsing()

        val params = HashMap<String, String>()

        params.put("userId", userid.toString())
        params.put("userToken", token.toString())
        params.put("followingId",followingId)
        params.put("followerId",userid)

        p.call(context,url,"addFollow",params,this)

        AppConstants.printLog(TAG, "addFollow: " + params);
    }


    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (da.length() > 0) {

            try {
                listener.onSuccess(da,message,method)

            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            listener.onFail(message,method)
        }
    }

    override fun onFail(msg: String, method: String) {
        listener.onFail(msg,method)
    }


}