package com.kaushlesh.Controller

import android.app.Activity
import android.util.Log
import com.kaushlesh.R
import com.kaushlesh.api.API
import com.kaushlesh.bean.RegisterPhoneBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.parser.PostRequestParsing
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import org.json.JSONObject
import java.util.HashMap

class BusinessProfileCountController(activity: Activity,otherUserBusinessProfileID: String) : ParseControllerListener {

    private val TAG = "BusinessCount"
    internal var activity: Activity = activity

    val storeusedata = StoreUserData(activity)
      val userid = storeusedata.getString(Constants.USER_ID)
      val token = storeusedata.getString(Constants.TOKEN)
    //val businessProfileId = storeusedata.getString(Constants.BUSINESS_USER_ID)


    init {
        Log.i(TAG, "onClick")

        val url = API.BUSINESS_PROFILE_VIEW_COUNT

        AppConstants.printLog(TAG, "BusinessProfileCount URL ==>$url")

        val p = PostRequestParsing()

        val params = HashMap<String, String>()

        params.put("userId", userid.toString());
        params.put("userToken", token.toString());
        params.put("bussinessprofile_id", otherUserBusinessProfileID)


        p.call(activity,url,"businessProfileCount",params,this);

        AppConstants.printLog(TAG, "BusinessProfileCount params: " + params);

        showProgressDialog(activity, activity.resources.getString(R.string.txt_please_wait))
    }



    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (da.length() > 0) {
            Log.e(TAG, "BusinessProfileCount result"+ "$da==")
            dismissProgressDialog()
            try {
                Utils.showLog(TAG,message)

            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            dismissProgressDialog()
            //  listener.onFail(message,method)
        }
    }

    override fun onFail(msg: String, method: String) {
        dismissProgressDialog()
        Utils.showLog(TAG,msg)
        //Toast.makeText(activity!!, ""+message, Toast.LENGTH_SHORT).show()

        // listener.onFail(message,method)
    }

    fun showProgressDialog(activity: Activity, msg: String) {
        if (Utils.progressDialog==null){
            Utils.showProgress(activity)
        }
    }

    fun dismissProgressDialog() {

        if (Utils.progressDialog != null && Utils.progressDialog!!.isShowing()) {
            Utils.dismissProgress()
        }
    }

}