package com.kaushlesh.Controller

import android.app.Activity
import android.content.ContentValues.TAG
import android.text.TextUtils
import android.util.Log
import com.kaushlesh.api.API
import com.kaushlesh.bean.Filter.FilterPropertyBean
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.parser.GetRequestParsing
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.R

class GetListingController(var context: Activity, internal var listener: ParseControllerListener) {

    val storeusedata = StoreUserData(context)
    val userid = storeusedata.getString(Constants.USER_ID)
    val token = storeusedata.getString(Constants.TOKEN)
    val businessProfileId = storeusedata.getString(Constants.BUSINESS_USER_ID)

    val googleKey = context.resources.getString(R.string.google_place_api_key)
    fun getCategoryList() {

        //Utils.showProgress(context)

        val p = GetRequestParsing()
        val url = API.GET_CATEGORY + "?userId=" + userid + "&userToken=" + token

        Utils.showLog(TAG,"GET CATEGORY URL : " + url)
        p.callApi(context, url, "Get Category", listener)
    }

    fun getSubCategoryList(categoryId: String) {
        //val storeusedata = StoreUserData(context)
        //Utils.showProgress(context)

        val p = GetRequestParsing()
        val url = API.GET_SUBCATEGORY + "?userId=" + userid + "&userToken=" + token + "&categoryId=" + categoryId

        Utils.showLog(TAG,"GET SUB CATEGORY URL : " + url)

        p.callApi(context, url, "Get subCategory", listener)
    }

    fun getProductList(categoryId: String, subcategoryId: String, url: String?) {
        //val storeusedata = StoreUserData(context)
        //Utils.showProgress(context)
        val p = GetRequestParsing()
        val url1 = url + "?userId=" + userid + "&userToken=" + token + "&category_id=" + categoryId + "&sub_category_id=" + subcategoryId
        Utils.showLog(TAG,"GET PRODUCT LIST URL : " + url1)

        p.callApi(context, url1, "Get Product", listener)
    }

    fun getProductDetail(postId: String) {
       // val storeusedata = StoreUserData(context)
        Utils.showProgress(context)

        val p = GetRequestParsing()
        val url = API.GET_POST_DETAILS + "?userId=" + userid + "&userToken=" + token + "&postId=" + postId

        Utils.showLog(TAG,"GET PRODUCT DETAIL URL : " + url)

        p.callApi(context, url, "Get PostDetails", listener)
    }

    fun getMyPackgesList(categoryId: String) {
        val p = GetRequestParsing()
        val url = API.GET_MY_PACKAGES_LIST + "?userId=" + userid + "&userToken=" + token + "&category_id=" + categoryId

        Utils.showLog(TAG,"GET PRODUCT DETAIL URL : " + url)

        p.callApi(context, url, "GetMyPackages", listener)
    }


    fun callforDisplayfilterData(bean: FilterPropertyBean, url1: String?) {

        var maxprice = bean.maxPrice.toString()
        Utils.showLog(TAG,"=== max price1===" + maxprice)
        if(maxprice.equals("30000001"))
        {
            maxprice = "3000000000000"
        }
        Utils.showLog(TAG,"=== max price===" + maxprice)
        val p = GetRequestParsing()
        val url = url1 + "?userId=" + userid + "&userToken=" + token +
                "&category_id=" + bean.categoryId +
                "&sub_category_id=" +  bean.subCategoryId +
                "&serchWord=" + bean.searchWord!!+
                "&latitude=" + bean.latitude!!+
                "&longitude=" + bean.longtude!! +
                "&minPrice=" + bean.minPrice.toString() +
                "&maxPrice=" + maxprice +
                "&property_type=" + TextUtils.join(",", bean.property_type_list!!)+
                "&listed_by=" + TextUtils.join(",", bean.property_listedby_list!!) +
                "&furnishing=" +  TextUtils.join(",", bean.property_byFunishing_list!!)+
                "&bathrooms=" +  TextUtils.join(",", bean.property_byBathroom_list!!)+
                "&bedrooms=" +  TextUtils.join(",", bean.property_bybadroom_list!!) +
                "&package_type=" +  TextUtils.join(",", bean.property_byPremium_list!!)+
                "&minsuper_builtup_area=" + bean.AreaMin+
                "&maxsuper_builtup_area=" + bean.AreaMax+
                "&minplot_area=" + bean.landAreaMin+
                "&maxplot_area=" + bean.landAreaMax+
                "&minguest_capacity=" + bean.guestAreaMin+
                "&maxguest_capacity=" + bean.guestAreaMax+
                "&mincarpet_area=" + bean.carpetAreaMin+
                "&maxcarpet_area=" + bean.carpetAreaMax+
                "&purpose=" + TextUtils.join(",", bean.property_bypurpose_list!!)+
                "&bachelors_allowed=" + TextUtils.join(",", bean.property_byBachelor_Allow_list!!)+
                "&construction_status=" +  TextUtils.join(",", bean.property_byConstruction_list!!)
        p.callApi(context, url, "Get Product", listener)
        Log.e("Url", "Get Product: "+url )
    }

    fun callforDisplayfilterMobileData(bean: FilterPropertyBean, url1: String?) {

        val p = GetRequestParsing()
        val url = url1 + "?userId=" + userid + "&userToken=" + token +
                "&category_id=" + bean.categoryId +
                "&sub_category_id=" +  bean.subCategoryId +
                "&serchWord=" + bean.searchWord!!+
                "&latitude=" + bean.latitude!!+
                "&longitude=" + bean.longtude!! +
                "&minPrice=" + bean.minPrice.toString() +
                "&maxPrice=" + bean.maxPrice.toString()+
                "&package_type=" + TextUtils.join(",", bean.property_byPremium_list!!)+
                "&mobile_type=" + bean.mobile_type.toString()+
                "&brand_id=" + TextUtils.join(",", bean.brand_id!!)

        p.callApi(context, url, "Get Product", listener)
        Log.e("Url", "Get Product: "+url )
    }

    fun callforDisplayfilterShopCityData(bean: FilterPropertyBean, url1: String?) {
        val p = GetRequestParsing()
        val url = url1 + "?userId=" + userid + "&userToken=" + token +
                "&category_id=" + bean.categoryId +
                "&sub_category_id=" +bean.subCategoryId +
                "&serchWord=" + bean.searchWord!!+
                "&latitude=" + bean.latitude!!+
                "&longitude=" + bean.longtude!! +
                /*    "&minPrice=" + bean.minPrice.toString()!! +
                    "&maxPrice=" + bean.maxPrice.toString()!!+*/
                "&package_type=" + TextUtils.join(",", bean.property_byPremium_list!!)
        p.callApi(context, url, "Get Product", listener)
        Log.e("Url", "Get Product: "+url )
    }

    fun callforDisplayfilterBikeData(bean: FilterPropertyBean, url1: String?) {

        val p = GetRequestParsing()
        val url = url1 + "?userId=" + userid + "&userToken=" + token +
                "&category_id=" + bean.categoryId +
                "&sub_category_id=" +  bean.subCategoryId +
                "&serchWord=" + bean.searchWord!!+
                "&latitude=" + bean.latitude!!+
                "&longitude=" + bean.longtude!! +
                "&minPrice=" + bean.minPrice.toString() +
                "&maxPrice=" + bean.maxPrice.toString()+
                "&minbike_year=" + bean.minbike_year.toString()+
                "&maxbike_year=" + bean.maxbike_year.toString()+
                "&minkm_driven=" + bean.minkm_driven.toString()+
                "&maxkm_driven=" + bean.maxkm_driven.toString()+
                "&package_type=" + TextUtils.join(",", bean.property_byPremium_list!!)+
                "&fuel=" + TextUtils.join(",", bean.fuel!!)+
                "&brand_id=" + TextUtils.join(",", bean.brand_id!!)

        p.callApi(context, url, "Get Product", listener)
        Log.e("Url", "Get Product: "+url )
    }

    fun callforDisplayfilterVehicalData(bean: FilterPropertyBean, url1: String?) {
        val p = GetRequestParsing()
        val url = url1 + "?userId=" + userid + "&userToken=" + token +
                "&category_id=" + bean.categoryId +
                "&sub_category_id=" +  bean.subCategoryId +
                "&serchWord=" + bean.searchWord!!+
                "&latitude=" + bean.latitude!!+
                "&longitude=" + bean.longtude!! +
                "&minPrice=" + bean.minPrice.toString() +
                "&maxPrice=" + bean.maxPrice.toString()+
                "&minvehical_year=" + bean.minbike_year.toString()+
                "&maxvehical_year=" + bean.maxbike_year.toString()+
                "&minkm_driven=" + bean.minkm_driven.toString()+
                "&maxkm_driven=" + bean.maxkm_driven.toString()+
                "&package_type=" + TextUtils.join(",", bean.property_byPremium_list!!)+
                "&vehical_type=" + TextUtils.join(",", bean.property_type_list!!)+
                "&no_of_owners=" + TextUtils.join(",", bean.property_listedby_list!!)+
                "&fuel=" + TextUtils.join(",", bean.fuel!!)+
                "&brand_id=" + TextUtils.join(",", bean.brand_id!!)

        p.callApi(context, url, "Get Product", listener)
        Log.e("Url", "Get Product: "+url )
    }

    fun callforDisplayfilterGenralData(bean: FilterPropertyBean, url1: String?) {
        val p = GetRequestParsing()
        val url = url1 + "?userId=" + userid + "&userToken=" + token +
                "&category_id=" + bean.categoryId +
                "&sub_category_id=" +bean.subCategoryId +
                "&serchWord=" + bean.searchWord!!+
                "&latitude=" + bean.latitude!!+
                "&longitude=" + bean.longtude!! +
                "&minPrice=" + bean.minPrice.toString() +
                "&maxPrice=" + bean.maxPrice.toString()+
                "&package_type=" + TextUtils.join(",", bean.property_byPremium_list!!)
        p.callApi(context, url, "Get Product", listener)
        Log.e("Url", "Get Product: "+url )
    }

    //khedut
    fun callforDisplayfilterGainSeeds(bean: FilterPropertyBean, url1: String?) {
        val p = GetRequestParsing()
        val url = url1 + "?userId=" + userid + "&userToken=" + token +
                "&category_id=" + bean.categoryId +
                "&sub_category_id=" + bean.subCategoryId +
                "&serchWord=" + bean.searchWord!! +
                "&latitude=" + bean.latitude!! +
                "&longitude=" + bean.longtude!! +
                "&minPrice=" + bean.minPrice.toString() +
                "&maxPrice=" + bean.maxPrice.toString() +
                "&minStock=" + bean.minStock.toString() +
                "&maxStock=" + bean.maxStock.toString() +
                "&grain_type=" + TextUtils.join(",", bean.brand_id!!) +
                "&types_of_product=" +TextUtils.join(",", bean.property_byBachelor_Allow_list!!) +
                "&package_type=" + TextUtils.join(",", bean.property_byPremium_list!!)
        p.callApi(context, url, "Get Product", listener)
        Log.e("Url", "Get Product: " + url)
    }

    //original grain seed
    /*fun callforDisplayfilterGainSeeds(bean: FilterPropertyBean, url1: String?) {
        val p = GetRequestParsing()
        val url = url1 + "?userId=" + userid + "&userToken=" + token +
                "&category_id=" + bean.categoryId +
                "&sub_category_id=" + bean.subCategoryId +
                "&serchWord=" + bean.searchWord!! +
                "&latitude=" + bean.latitude!! +
                "&longitude=" + bean.longtude!! +
                "&minPrice=" + bean.minPrice.toString() +
                "&maxPrice=" + bean.maxPrice.toString() +
                "&minStock=" + bean.minStock.toString() +
                "&maxStock=" + bean.maxStock.toString() +
                "&package_type=" + TextUtils.join(",", bean.property_byPremium_list!!)
        p.callApi(context, url, "Get Product", listener)
        Log.e("Url", "Get Product: " + url)
    }*/
    fun getBrandList() {
        val p = GetRequestParsing()
        val url = API.GET_BRAND_LIST + "?userId=" + userid + "&userToken=" + token + "&categoryId=" + storeusedata.getString(Constants.CATEGORY_ID) + "&subcategory_id=" +  storeusedata.getString(Constants.SUBCATEGORYID)

        Utils.showLog(TAG,"GET BRAND LIST URL : " + url)

        p.callApi(context, url, "GetBrands", listener)
    }

    fun getExtraAdPackage(catid: String?) {
        val p = GetRequestParsing()
        val url = API.GET_EXTRA_AD_PACKAGE + "?userId=" + userid + "&userToken=" + token + "&categoryId=" + catid

        Utils.showLog(TAG,"GET EXTA AD URL : " + url)

        p.callApi(context, url, "GetExtraAd", listener)
    }

    fun getVehicleTypeList() {
        val p = GetRequestParsing()
        val url = API.GET_VEHICLE_TYPE_LIST + "?userId=" + userid + "&userToken=" + token

        Utils.showLog(TAG,"GET VEHICLE TYPE URL : " + url)

        p.callApi(context, url, "GetVehicleType", listener)
    }

    fun getVehicleCarDecorTypeList() {
        val p = GetRequestParsing()
        val url = API.GET_VEHICLE_CAR_DECOR_TYPE_LIST + "?userId=" + userid + "&userToken=" + token

        Utils.showLog(TAG,"GET VEHICLE TYPE URL : " + url)

        p.callApi(context, url, "GetVehicleCarType", listener)

    }

    fun getCategoryPackgesList(categoryId: String) {

        val p = GetRequestParsing()
        val url = API.GET_CATEGORY_PACKAGE_LIST + "?userId=" + userid + "&userToken=" + token + "&categoryId=" + categoryId

        Utils.showLog(TAG,"GET CATEGORY PACKAGE URL : " + url)

        p.callApi(context, url, "GetPkg", listener)
    }


    fun getPackageOrder() {

        val p = GetRequestParsing()
        val url = API.GET_PACKAGE_ORDERS + "?userId=" + userid + "&userToken=" + token
        //  val url = API.GET_PACKAGE_ORDERS + "?userId=2&userToken=2848b4042b27a94b0255bbcf9532e906"
        Utils.showLog(TAG, "GET getorder LIST URL : " + url)
        p.callApi(context, url, "GetOrder", listener)
    }

    fun getNotificationList() {

        val p = GetRequestParsing()
        val url = API.GET_NOTIFICATIONS + "?userId=" + userid + "&userToken=" + token
        //  val url = API.GET_PACKAGE_ORDERS + "?userId=2&userToken=2848b4042b27a94b0255bbcf9532e906"
        Utils.showLog(TAG, "GET GetNotification LIST URL : " + url)
        p.callApi(context, url, "GetNotification", listener)
    }

    fun getMyPost(start: Int, limit: Int, startDate: String, endDate: String, filterType: Int, showProgress: Boolean) {
        //Utils.showProgress(context)

        if(start==0) {
            if(showProgress){
                Utils.showProgress(context)
            }
        }

        val p = GetRequestParsing()
        val url = API.GET_MY_POST + "?userId=" + userid + "&userToken=" + token + "&start=" + start + "&limit=" + 20+ "&start_date=" + startDate + "&end_date=" + endDate + "&filterType=" + filterType
        Utils.showLog(TAG, "GET MY POST LIST URL : " + url)
        p.callApi(context, url, "getMyPost", listener)
    }

    fun getMyPostFilters(){
        val p = GetRequestParsing()
        val url = API.GET_MY_FILTER_POST + "?userId=" + userid + "&userToken=" + token
        Utils.showLog(TAG, "GET getMyPostFilters URL : " + url)
        p.callApi(context, url, "getMyPostFilters", listener)
    }

    fun getMyFavPost(start: Int, limit: Int) {
        //Utils.showProgress(context)
        val p = GetRequestParsing()
        val url = API.GET_FAV_POST + "?userId=" + userid + "&userToken=" + token + "&start=" + start + "&limit=" + 10
        Utils.showLog(TAG, "GET MY FAV LIST URL : " + url)
        p.callApi(context, url, "getMyFavPost", listener)
    }

    fun getLocationList() {
        val p = GetRequestParsing()
        val url = API.GET_LOCATION_LIST + "?userId=" + userid + "&userToken=" + token

        Utils.showLog(TAG,"GET LOCATION LIST URL : " + url)

        p.callApi(context, url, "GetLocation", listener)
    }

    fun getUserProfile() {

        val p = GetRequestParsing()
        val url = API.USER_PROFILE + "?userId=" + userid + "&userToken=" + token + "&profileId=" + userid

        Utils.showLog(TAG,"GET USER PROFILE URL : " + url)

        p.callApi(context, url, "profile", listener)
    }

    fun getUserBusinessProfile(otherUserBusinessProfileID: String) {

        val p = GetRequestParsing()
        val url = API.GET_BUSINESS_PROFILE + "?userId=" + userid + "&userToken=" + token + "&bussinessprofile_id=" + otherUserBusinessProfileID

        Utils.showLog(TAG,"GET USER BUSINESS PROFILE URL : " + url)

        p.callApi(context, url, "getBusinessProfile", listener)
    }

    fun getInvoiceDetailData() {

        val p = GetRequestParsing()
        val url = API.GET_BILLING_PROFILE + "?userId=" + userid + "&userToken=" + token + "&profileId=" + userid

        Utils.showLog(TAG,"GET INVOICE URL : " + url)

        p.callApi(context, url, "getInvoice", listener)
    }

    fun getTutorialData() {

        val p = GetRequestParsing()
        val url = API.GET_TUTORIAL

        Utils.showLog(TAG,"GET TUTORIAL URL : " + url)

        p.callApi(context, url, "getTutorial", listener)
    }

    fun getSingleAdPremiumPkgList(categoryId: String) {

        val p = GetRequestParsing()
        val url = API.GET_SINGLE_AD_PREMIUM_PACKAGE_LIST + "?userId=" + userid + "&userToken=" + token + "&categoryId=" + categoryId

        Utils.showLog(TAG,"GET SINGLE_AD_PREMIUM PACKAGE URL : " + url)

        p.callApi(context, url, "GetPkg", listener)
    }

    fun getLoginUserDetails() {
        val p = GetRequestParsing()
        val url = API.GET_LOGIN_USER_DETAILS + "?userId=" + userid + "&userToken=" + token

        Utils.showLog(TAG,"GET SINGLE_AD_PREMIUM PACKAGE URL : " + url)

        p.callApi(context, url, "GetUser", listener)

    }

    fun getFollowingUserDetail(profileId: String,start: Int) {
        val p = GetRequestParsing()
        val url = API.GET_FOLLOWING_USER_DETAILS + "?userId=" + userid + "&userToken=" + token + "&profileId=" + profileId + "&start=" + start + "&limit=" + 20

        Utils.showLog(TAG,"GET SINGLE_AD_PREMIUM PACKAGE URL : " + url)

        p.callApi(context, url, "GetFollowingUser", listener)
    }

    fun getFollowerList() {

        val p = GetRequestParsing()
        val url = API.GET_FOLLOWERS_LIST + "?userId=" + userid + "&userToken=" + token

        Utils.showLog(TAG,"GET FOLLOWERS LIST URL : " + url)

        p.callApi(context, url, "GetFollowerList", listener)
    }

    fun getFollowingList() {

        val p = GetRequestParsing()
        val url = API.GET_FOLLOWING_LIST + "?userId=" + userid + "&userToken=" + token

        Utils.showLog(TAG,"GET FOLLOWERS LIST URL : " + url)

        p.callApi(context, url, "GetFollowingList", listener)
    }

    fun getUserOrPostExistDetail(postid: Int, luserid: String) {
        val p = GetRequestParsing()
        val url = API.GET_POST_USER_EXIST_OR_NOT + "?userId=" + userid + "&userToken=" + token + "&postId=" + postid + "&profileId=" + luserid

        Utils.showLog(TAG,"GET FOLLOWERS LIST URL : " + url)

        p.callApi(context, url, "userPostExist", listener)
    }

    fun getFreshRecommondedList(start : Int) {
        Utils.showProgress(context)
        val p = GetRequestParsing()
        val url = API.GET_FRESH_RECOM_PRODUCT_LIST + "?start=" + start + "&limit=" + "100" + "&userId=" + userid

        Utils.showLog(TAG,"GET FRESH_RECOM_PRODUCT LIST URL : " + url)

        p.callApi(context, url, "freshProduct", listener)
    }

    fun getHomeFilterList(){
        val p = GetRequestParsing()
        val url = API.GET_CATEGORY_SUBCATEGORY_IDS

        Utils.showLog(TAG,"GET GET_CATEGORY_SUBCATEGORY_IDS LIST URL : " + url)

        p.callApi(context, url, "homeFilterList", listener)
    }

    fun getHomePostList(name: String, id: String, latitude: String, longitude: String,searchword : String,start: Int,radius :Int,showProgress : Boolean) {
        //Utils.showProgress(context)
        if(start==0) {
            if(showProgress){
                Utils.showProgress(context)
            }
        }
        var url =""
        val p = GetRequestParsing()
        if(latitude.equals("0.0") || longitude.equals("0.0"))
        {
            url = API.GET_HOME_PRODUCT_LIST + "?userId=" + userid + "&location_id=" + id + "&serchWord=" +searchword + "&latitude=" + "" + "&longitude=" + "" + "&location_name=" + name+ "&start=" + start + "&limit=" +20+ "&radius=" + radius
        }
        else {
            url = API.GET_HOME_PRODUCT_LIST + "?userId=" + userid + "&location_id=" + id + "&serchWord=" + searchword + "&latitude=" + latitude + "&longitude=" + longitude + "&location_name=" + name + "&start=" + start + "&limit=" + 20 + "&radius=" + radius
        }
        Utils.showLog(TAG,"GET_HOME PRODUCT LIST URL : " + url)

        p.callApi(context, url, "freshProduct", listener)
    }

    fun getReportStatus(userId: String) {
        val p = GetRequestParsing()
        val url = API.GET_BLOCK_STATUS + "?profileId=" + userId

        Utils.showLog(TAG,"GET BLOCK STATUS URL : " + url)

        p.callApi(context, url, "block", listener)
    }

    fun getLatLongOfSelectedStrict(address: String) {
        val p = GetRequestParsing()
        val url = "https://maps.googleapis.com/maps/api/geocode/json?key="+googleKey+"&address="+address

        Utils.showLog(TAG,"GET Lat Long URL : " + url)

        p.callApiWithoutSM(context, url, "latLong", listener)

    }

    fun callforGetLocationList(searchword: String, dislatitude: Double, dislongitude: Double,district : String) {
        var radius = 100000
        if(district.equals("Kutch"))
        {
            radius = 500000
        }
        val p = GetRequestParsing()
        val url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?location="+dislatitude+","+dislongitude+"&strictbounds&radius="+radius+ "&key="+ googleKey+"&input="+ searchword

        Utils.showLog(TAG,"GET Lat Long URL : " + url)

        p.callApiWithoutSM(context, url, "suggestList", listener)
    }

    fun getLatLongOfSelectedAddress(locationName: String) {
        val p = GetRequestParsing()
        val url = "https://maps.googleapis.com/maps/api/geocode/json?key="+googleKey+"&address="+locationName.replace(" ","")

        Utils.showLog(TAG,"GET Address Lat Long URL : " + url)

        p.callApiWithoutSM(context, url, "addresslatLong", listener)
    }

    fun getKhedutProductTypeList() {

        val p = GetRequestParsing()
        val url = API.GET_KHEDUT_PRODUCT_LIST + "?userId=" + userid + "&userToken=" + token + "&categoryId=" + storeusedata.getString(Constants.CATEGORY_ID) + "&subcategoryId=" +  storeusedata.getString(Constants.SUBCATEGORYID)

        Utils.showLog(TAG,"GET KHEDUT PRODUCT LIST URL : " + url)

        p.callApi(context, url, "GetProduct", listener)
    }

    fun callforDisplayfilterMobileDataNew(locationName: String, locId: String, latitude: String, longitude: String, searchword: String, page: Int, radius: Int, bean: FilterPropertyBean, url1: String?, categoryId: String, subcategoryId: String) {
        val p = GetRequestParsing()
        var url =""
        if(latitude.equals("0.0") || longitude.equals("0.0"))
        {
            url = url1 + "?userId=" + userid + "&userToken=" + token +
                    /*   "&category_id=" + bean.categoryId +
                       "&sub_category_id=" +  bean.subCategoryId +*/
                    /*   "&serchWord=" + bean.searchWord!!+*/
                    "&category_id=" + categoryId +
                    "&sub_category_id=" +  subcategoryId +
                    "&serchWord=" + searchword+
                    /* "&latitude=" + bean.latitude!!+
                     "&longitude=" + bean.longtude!! +*/
                    "&latitude=" + "" +
                    "&longitude=" + "" +
                    "&minPrice=" + bean.minPrice.toString() +
                    "&maxPrice=" + bean.maxPrice.toString()+
                    "&package_type=" + TextUtils.join(",", bean.property_byPremium_list!!)+
                    "&mobile_type=" + bean.mobile_type.toString()+
                    "&brand_id=" + TextUtils.join(",", bean.brand_id!!) +
                    "&location_id=" + locId + "&location_name=" + locationName+ "&start=" + page + "&limit=" + 20 + "&radius=" + radius
        }
        else {
            url = url1 + "?userId=" + userid + "&userToken=" + token +
                    /*   "&category_id=" + bean.categoryId +
                       "&sub_category_id=" +  bean.subCategoryId +*/
                    /*   "&serchWord=" + bean.searchWord!!+*/
                    "&category_id=" + categoryId +
                    "&sub_category_id=" +  subcategoryId +
                    "&serchWord=" + searchword+
                    /* "&latitude=" + bean.latitude!!+
                     "&longitude=" + bean.longtude!! +*/
                    "&latitude=" + latitude +
                    "&longitude=" + longitude +
                    "&minPrice=" + bean.minPrice.toString() +
                    "&maxPrice=" + bean.maxPrice.toString()+
                    "&package_type=" + TextUtils.join(",", bean.property_byPremium_list!!)+
                    "&mobile_type=" + bean.mobile_type.toString()+
                    "&brand_id=" + TextUtils.join(",", bean.brand_id!!) +
                    "&location_id=" + locId + "&location_name=" + locationName+ "&start=" + page + "&limit=" + 20 + "&radius=" + radius
        }

        p.callApi(context, url, "Get Product", listener)
        Log.e("Url", "Get Product: "+url )
    }


    fun callforDisplayfilterBikeDataNew(locationName: String, locId: String, latitude: String, longitude: String, searchword: String, page: Int, radius: Int, bean: FilterPropertyBean, url1: String?, categoryId: String, subcategoryId: String) {

        val p = GetRequestParsing()
        var url =""
        if(latitude.equals("0.0") || longitude.equals("0.0")) {
            url = url1 + "?userId=" + userid + "&userToken=" + token +
                    /* "&category_id=" + bean.categoryId +
                "&sub_category_id=" +  bean.subCategoryId +
                "&serchWord=" + bean.searchWord!!+*/
                    "&category_id=" + categoryId +
                    "&sub_category_id=" + subcategoryId +
                    "&serchWord=" + searchword +
                    /*"&latitude=" + bean.latitude!!+
                "&longitude=" + bean.longtude!! +*/
                    "&latitude=" + "" +
                    "&longitude=" + "" +
                    "&minPrice=" + bean.minPrice.toString() +
                    "&maxPrice=" + bean.maxPrice.toString() +
                    "&minbike_year=" + bean.minbike_year.toString() +
                    "&maxbike_year=" + bean.maxbike_year.toString() +
                    "&minkm_driven=" + bean.minkm_driven.toString() +
                    "&maxkm_driven=" + bean.maxkm_driven.toString() +
                    "&package_type=" + TextUtils.join(",", bean.property_byPremium_list!!) +
                    "&fuel=" + TextUtils.join(",", bean.fuel!!) +
                    "&brand_id=" + TextUtils.join(",", bean.brand_id!!) +
                    "&location_id=" + locId + "&location_name=" + locationName + "&start=" + page + "&limit=" + 20 + "&radius=" + radius
        }
        else{
            url = url1 + "?userId=" + userid + "&userToken=" + token +
                    /* "&category_id=" + bean.categoryId +
                    "&sub_category_id=" +  bean.subCategoryId +
                    "&serchWord=" + bean.searchWord!!+*/
                    "&category_id=" + categoryId +
                    "&sub_category_id=" + subcategoryId +
                    "&serchWord=" + searchword +
                    /*"&latitude=" + bean.latitude!!+
                    "&longitude=" + bean.longtude!! +*/
                    "&latitude=" + latitude +
                    "&longitude=" + longitude +
                    "&minPrice=" + bean.minPrice.toString() +
                    "&maxPrice=" + bean.maxPrice.toString() +
                    "&minbike_year=" + bean.minbike_year.toString() +
                    "&maxbike_year=" + bean.maxbike_year.toString() +
                    "&minkm_driven=" + bean.minkm_driven.toString() +
                    "&maxkm_driven=" + bean.maxkm_driven.toString() +
                    "&package_type=" + TextUtils.join(",", bean.property_byPremium_list!!) +
                    "&fuel=" + TextUtils.join(",", bean.fuel!!) +
                    "&brand_id=" + TextUtils.join(",", bean.brand_id!!) +
                    "&location_id=" + locId + "&location_name=" + locationName + "&start=" + page + "&limit=" + 20 + "&radius=" + radius
        }
        p.callApi(context, url, "Get Product", listener)
        Log.e("Url", "Get Product: "+url )
    }

    fun callforDisplayfilterGenralDataNew(locationName: String, locId: String, latitude: String, longitude: String, searchword: String, page: Int, radius: Int, bean: FilterPropertyBean, url1: String?, categoryId: String, subcategoryId: String) {
        val p = GetRequestParsing()
        var url =""
        if(latitude.equals("0.0") || longitude.equals("0.0")) {
             url = url1 + "?userId=" + userid + "&userToken=" + token +
                   /* "&category_id=" + bean.categoryId +
                    "&sub_category_id=" +bean.subCategoryId +
                    "&serchWord=" + bean.searchWord!!+*/
                     "&category_id=" + categoryId +
                     "&sub_category_id=" + subcategoryId +
                     "&serchWord=" + searchword +
                   /* "&latitude=" + bean.latitude!!+
                    "&longitude=" + bean.longtude!! +*/
                     "&latitude=" + "" +
                     "&longitude=" + "" +
                    "&minPrice=" + bean.minPrice.toString() +
                    "&maxPrice=" + bean.maxPrice.toString()+
                    "&package_type=" + TextUtils.join(",", bean.property_byPremium_list!!) +
                    "&location_id=" + locId + "&location_name=" + locationName + "&start=" + page + "&limit=" + 20 + "&radius=" + radius
        }
        else{
             url = url1 + "?userId=" + userid + "&userToken=" + token +
                   /* "&category_id=" + bean.categoryId +
                    "&sub_category_id=" +bean.subCategoryId +
                    "&serchWord=" + bean.searchWord!!+*/
                     "&category_id=" + categoryId +
                     "&sub_category_id=" + subcategoryId +
                     "&serchWord=" + searchword +
                   /* "&latitude=" + bean.latitude!!+
                    "&longitude=" + bean.longtude!! +*/
                     "&latitude=" + latitude +
                     "&longitude=" + longitude +
                    "&minPrice=" + bean.minPrice.toString() +
                    "&maxPrice=" + bean.maxPrice.toString()+
                    "&package_type=" + TextUtils.join(",", bean.property_byPremium_list!!) +
                    "&location_id=" + locId + "&location_name=" + locationName + "&start=" + page + "&limit=" + 20 + "&radius=" + radius
        }

        p.callApi(context, url, "Get Product", listener)
        Log.e("Url", "Get Product: "+url )
    }

    fun callforDisplayfilterShopCityDataNew(locationName: String, locId: String, latitude: String, longitude: String, searchword: String, page: Int, radius: Int, bean: FilterPropertyBean, url1: String?, categoryId: String, subcategoryId: String) {
        val p = GetRequestParsing()
        var url =""
        if(latitude.equals("0.0") || longitude.equals("0.0"))
        {
            url = url1 + "?userId=" + userid + "&userToken=" + token +
                   /* "&category_id=" + bean.categoryId +
                    "&sub_category_id=" +bean.subCategoryId +
                    "&serchWord=" + bean.searchWord!!+*/
                    "&category_id=" + categoryId +
                    "&sub_category_id=" + subcategoryId +
                    "&serchWord=" + searchword +
                    "&latitude=" + "" +
                    "&longitude=" + "" +
                    /*    "&minPrice=" + bean.minPrice.toString()!! +
                        "&maxPrice=" + bean.maxPrice.toString()!!+*/
                    "&package_type=" + TextUtils.join(",", bean.property_byPremium_list!!) +
                    "&location_id=" + locId + "&location_name=" + locationName + "&start=" + page + "&limit=" + 20 + "&radius=" + radius
        }
        else{
            url = url1 + "?userId=" + userid + "&userToken=" + token +
                   /* "&category_id=" + bean.categoryId +
                    "&sub_category_id=" +bean.subCategoryId +
                    "&serchWord=" + bean.searchWord!!+*/
                    "&category_id=" + categoryId +
                    "&sub_category_id=" + subcategoryId +
                    "&serchWord=" + searchword +
                    "&latitude=" + latitude +
                    "&longitude=" + longitude +
                    /*    "&minPrice=" + bean.minPrice.toString()!! +
                        "&maxPrice=" + bean.maxPrice.toString()!!+*/
                    "&package_type=" + TextUtils.join(",", bean.property_byPremium_list!!) +
                    "&location_id=" + locId + "&location_name=" + locationName + "&start=" + page + "&limit=" + 20 + "&radius=" + radius
        }
        p.callApi(context, url, "Get Product", listener)
        Log.e("Url", "Get Product: "+url )
    }

    fun callforDisplayfilterVehicalDataNew(locationName: String, locId: String, latitude: String, longitude: String, searchword: String, page: Int, radius: Int, bean: FilterPropertyBean, url1: String?, categoryId: String, subcategoryId: String) {
        val p = GetRequestParsing()
        var url =""
        if(latitude.equals("0.0") || longitude.equals("0.0"))
        {
            url = url1 + "?userId=" + userid + "&userToken=" + token +
                  /*  "&category_id=" + bean.categoryId +
                    "&sub_category_id=" +  bean.subCategoryId +
                    "&serchWord=" + bean.searchWord!!+*/
                    "&category_id=" + categoryId +
                    "&sub_category_id=" + subcategoryId +
                    "&serchWord=" + searchword +
                    /*"&latitude=" + bean.latitude!!+
                    "&longitude=" + bean.longtude!! +*/
                    "&latitude=" + "" +
                    "&longitude=" + "" +
                    "&minPrice=" + bean.minPrice.toString() +
                    "&maxPrice=" + bean.maxPrice.toString()+
                    "&minvehical_year=" + bean.minbike_year.toString()+
                    "&maxvehical_year=" + bean.maxbike_year.toString()+
                    "&minkm_driven=" + bean.minkm_driven.toString()+
                    "&maxkm_driven=" + bean.maxkm_driven.toString()+
                    "&package_type=" + TextUtils.join(",", bean.property_byPremium_list!!)+
                    "&vehical_type=" + TextUtils.join(",", bean.property_type_list!!)+
                    "&no_of_owners=" + TextUtils.join(",", bean.property_listedby_list!!)+
                    "&fuel=" + TextUtils.join(",", bean.fuel!!)+
                    "&brand_id=" + TextUtils.join(",", bean.brand_id!!)+
                    "&location_id=" + locId + "&location_name=" + locationName + "&start=" + page + "&limit=" + 20 + "&radius=" + radius
        }
        else{
            url = url1 + "?userId=" + userid + "&userToken=" + token +
                   /* "&category_id=" + bean.categoryId +
                    "&sub_category_id=" +  bean.subCategoryId +
                    "&serchWord=" + bean.searchWord!!+*/
                    "&category_id=" + categoryId +
                    "&sub_category_id=" + subcategoryId +
                    "&serchWord=" + searchword +
                   /* "&latitude=" + bean.latitude!!+
                    "&longitude=" + bean.longtude!! +*/
                    "&latitude=" + latitude +
                    "&longitude=" + longitude +
                    "&minPrice=" + bean.minPrice.toString() +
                    "&maxPrice=" + bean.maxPrice.toString()+
                    "&minvehical_year=" + bean.minbike_year.toString()+
                    "&maxvehical_year=" + bean.maxbike_year.toString()+
                    "&minkm_driven=" + bean.minkm_driven.toString()+
                    "&maxkm_driven=" + bean.maxkm_driven.toString()+
                    "&package_type=" + TextUtils.join(",", bean.property_byPremium_list!!)+
                    "&vehical_type=" + TextUtils.join(",", bean.property_type_list!!)+
                    "&no_of_owners=" + TextUtils.join(",", bean.property_listedby_list!!)+
                    "&fuel=" + TextUtils.join(",", bean.fuel!!)+
                    "&brand_id=" + TextUtils.join(",", bean.brand_id!!)+
                    "&location_id=" + locId + "&location_name=" + locationName + "&start=" + page + "&limit=" + 20 + "&radius=" + radius
        }

        p.callApi(context, url, "Get Product", listener)
        Log.e("Url", "Get Product: "+url )
    }

    fun callforDisplayfilterDataNew(locationName: String, locId: String, latitude: String, longitude: String, searchword: String, page: Int, radius: Int, bean: FilterPropertyBean, url1: String?, categoryId: String, subcategoryId: String) {
        var maxprice = bean.maxPrice.toString()
        Utils.showLog(TAG,"=== max price1===" + maxprice)
        if(maxprice.equals("30000001"))
        {
            maxprice = "3000000000000"
        }
        Utils.showLog(TAG,"=== max price===" + maxprice)
        val p = GetRequestParsing()
        var url =""
        if(latitude.equals("0.0") || longitude.equals("0.0"))
        {
            url = url1 + "?userId=" + userid + "&userToken=" + token +
                   /* "&category_id=" + bean.categoryId +
                    "&sub_category_id=" +  bean.subCategoryId +
                    "&serchWord=" + bean.searchWord!!+*/
                    "&category_id=" + categoryId +
                    "&sub_category_id=" + subcategoryId +
                    "&serchWord=" + searchword +
                   /* "&latitude=" + bean.latitude!!+
                    "&longitude=" + bean.longtude!! +*/
                    "&latitude=" + "" +
                    "&longitude=" + "" +
                    "&minPrice=" + bean.minPrice.toString() +
                    "&maxPrice=" + maxprice +
                    "&property_type=" + TextUtils.join(",", bean.property_type_list!!)+
                    "&listed_by=" + TextUtils.join(",", bean.property_listedby_list!!) +
                    "&furnishing=" +  TextUtils.join(",", bean.property_byFunishing_list!!)+
                    "&bathrooms=" +  TextUtils.join(",", bean.property_byBathroom_list!!)+
                    "&bedrooms=" +  TextUtils.join(",", bean.property_bybadroom_list!!) +
                    "&package_type=" +  TextUtils.join(",", bean.property_byPremium_list!!)+
                    "&minsuper_builtup_area=" + bean.AreaMin+
                    "&maxsuper_builtup_area=" + bean.AreaMax+
                    "&minplot_area=" + bean.landAreaMin+
                    "&maxplot_area=" + bean.landAreaMax+
                    "&minguest_capacity=" + bean.guestAreaMin+
                    "&maxguest_capacity=" + bean.guestAreaMax+
                    "&mincarpet_area=" + bean.carpetAreaMin+
                    "&maxcarpet_area=" + bean.carpetAreaMax+
                    "&purpose=" + TextUtils.join(",", bean.property_bypurpose_list!!)+
                    "&bachelors_allowed=" + TextUtils.join(",", bean.property_byBachelor_Allow_list!!)+
                    "&construction_status=" +  TextUtils.join(",", bean.property_byConstruction_list!!)+
                    "&location_id=" + locId + "&location_name=" + locationName + "&start=" + page + "&limit=" + 20 + "&radius=" + radius
        }
        else{
            url = url1 + "?userId=" + userid + "&userToken=" + token +
                    /*"&category_id=" + bean.categoryId +
                    "&sub_category_id=" +  bean.subCategoryId +
                    "&serchWord=" + bean.searchWord!!+*/
                    "&category_id=" + categoryId +
                    "&sub_category_id=" + subcategoryId +
                    "&serchWord=" + searchword +
                   /* "&latitude=" + bean.latitude!!+
                    "&longitude=" + bean.longtude!! +*/
                    "&latitude=" + latitude +
                    "&longitude=" + longitude +
                    "&minPrice=" + bean.minPrice.toString() +
                    "&maxPrice=" + maxprice +
                    "&property_type=" + TextUtils.join(",", bean.property_type_list!!)+
                    "&listed_by=" + TextUtils.join(",", bean.property_listedby_list!!) +
                    "&furnishing=" +  TextUtils.join(",", bean.property_byFunishing_list!!)+
                    "&bathrooms=" +  TextUtils.join(",", bean.property_byBathroom_list!!)+
                    "&bedrooms=" +  TextUtils.join(",", bean.property_bybadroom_list!!) +
                    "&package_type=" +  TextUtils.join(",", bean.property_byPremium_list!!)+
                    "&minsuper_builtup_area=" + bean.AreaMin+
                    "&maxsuper_builtup_area=" + bean.AreaMax+
                    "&minplot_area=" + bean.landAreaMin+
                    "&maxplot_area=" + bean.landAreaMax+
                    "&minguest_capacity=" + bean.guestAreaMin+
                    "&maxguest_capacity=" + bean.guestAreaMax+
                    "&mincarpet_area=" + bean.carpetAreaMin+
                    "&maxcarpet_area=" + bean.carpetAreaMax+
                    "&purpose=" + TextUtils.join(",", bean.property_bypurpose_list!!)+
                    "&bachelors_allowed=" + TextUtils.join(",", bean.property_byBachelor_Allow_list!!)+
                    "&construction_status=" +  TextUtils.join(",", bean.property_byConstruction_list!!)+
                    "&location_id=" + locId + "&location_name=" + locationName + "&start=" + page + "&limit=" + 20 + "&radius=" + radius
        }
        p.callApi(context, url, "Get Product", listener)
        Log.e("Url", "Get Product: "+url )
    }

    fun callforDisplayfilterGainSeedsNew(locationName: String, locId: String, latitude: String, longitude: String, searchword: String, page: Int, radius: Int, bean: FilterPropertyBean, url1: String?, categoryId: String, subcategoryId: String) {
        val p = GetRequestParsing()
        var url =""
        if(latitude.equals("0.0") || longitude.equals("0.0"))
        {
            url = url1 + "?userId=" + userid + "&userToken=" + token +
                  /*  "&category_id=" + bean.categoryId +
                    "&sub_category_id=" + bean.subCategoryId +
                    "&serchWord=" + bean.searchWord!! +*/
                    "&category_id=" + categoryId +
                    "&sub_category_id=" + subcategoryId +
                    "&serchWord=" + searchword +
                  /*  "&latitude=" + bean.latitude!! +
                    "&longitude=" + bean.longtude!! +*/
                    "&latitude=" + "" +
                    "&longitude=" + "" +
                    "&minPrice=" + bean.minPrice.toString() +
                    "&maxPrice=" + bean.maxPrice.toString() +
                    "&minStock=" + bean.minStock.toString() +
                    "&maxStock=" + bean.maxStock.toString() +
                    "&grain_type=" + TextUtils.join(",", bean.brand_id!!) +
                    "&types_of_product=" +TextUtils.join(",", bean.property_byBachelor_Allow_list!!) +
                    "&package_type=" + TextUtils.join(",", bean.property_byPremium_list!!)+
                    "&location_id=" + locId + "&location_name=" + locationName + "&start=" + page + "&limit=" + 20 + "&radius=" + radius
        }
        else{
            url = url1 + "?userId=" + userid + "&userToken=" + token +
                   /* "&category_id=" + bean.categoryId +
                    "&sub_category_id=" + bean.subCategoryId +
                    "&serchWord=" + bean.searchWord!! +*/
                    "&category_id=" + categoryId +
                    "&sub_category_id=" + subcategoryId +
                    "&serchWord=" + searchword +
                   /* "&latitude=" + bean.latitude!! +
                    "&longitude=" + bean.longtude!! +*/
                    "&latitude=" + latitude +
                    "&longitude=" + longitude +
                    "&minPrice=" + bean.minPrice.toString() +
                    "&maxPrice=" + bean.maxPrice.toString() +
                    "&minStock=" + bean.minStock.toString() +
                    "&maxStock=" + bean.maxStock.toString() +
                    "&grain_type=" + TextUtils.join(",", bean.brand_id!!) +
                    "&types_of_product=" +TextUtils.join(",", bean.property_byBachelor_Allow_list!!) +
                    "&package_type=" + TextUtils.join(",", bean.property_byPremium_list!!)+
                    "&location_id=" + locId + "&location_name=" + locationName + "&start=" + page + "&limit=" + 20 + "&radius=" + radius
        }

        p.callApi(context, url, "Get Product", listener)
        Log.e("Url", "Get Product: " + url)
    }


    fun getHomeAdvertisements() {

        val p = GetRequestParsing()
        val url = API.ADVERTISEMENT_HOME

        Utils.showLog(TAG,"GET ADVERTISEMENT_HOME URL : " + url)
        p.callApi(context, url, "homeAds", listener)
    }

    fun getPostDetailAdvertisements(categoryId: String) {

        val p = GetRequestParsing()
        val url = API.ADVERTISEMENT_POST_DETAIL + "?category_id=" + categoryId
        //val url = API.ADVERTISEMENT_POST_DETAIL

        Utils.showLog(TAG,"GET ADVERTISEMENT_POST_DETAIL URL : " + url)
        p.callApi(context, url, "postDetailAds", listener)
    }


    //FAQs for buyers
    fun getFAQBuyers(staticId: Int) {

        val p = GetRequestParsing()
        val url = API.SUPPORT_FAQ_BUYERS +"?userId=" + userid + "&userToken=" + token +"&staticId=" + staticId

        Utils.showLog(TAG,"GET SUPPORT_FAQ_BUYERS URL : " + url)
        p.callApi(context, url, "FAQBuyers", listener)
    }

    //FAQs for buyers
    fun getFAQSellers(staticId: Int) {

        val p = GetRequestParsing()
        val url = API.SUPPORT_FAQ_BUYERS +"?userId=" + userid + "&userToken=" + token +"&staticId=" + staticId

        Utils.showLog(TAG,"GET SUPPORT_FAQ_BUYERS URL : " + url)
        p.callApi(context, url, "FAQSellers", listener)
    }

    //Privacy policy
    fun getPrivacyPolicy(staticId: Int) {

        val p = GetRequestParsing()
        val url = API.SUPPORT_FAQ_BUYERS +"?userId=" + userid + "&userToken=" + token +"&staticId=" + staticId

        Utils.showLog(TAG,"GET SUPPORT_FAQ_BUYERS URL : " + url)
        p.callApi(context, url, "privacyPolicy", listener)
    }

    //Terms conditions
    fun getTermsConditions(staticId: Int) {

        val p = GetRequestParsing()
        val url = API.SUPPORT_FAQ_BUYERS +"?userId=" + userid + "&userToken=" + token +"&staticId=" + staticId

        Utils.showLog(TAG,"GET SUPPORT_FAQ_BUYERS URL : " + url)
        p.callApi(context, url, "termsConditions", listener)
    }

    //Contact us
    fun getContactUs(staticId: Int) {

        val p = GetRequestParsing()
        val url = API.SUPPORT_FAQ_BUYERS +"?userId=" + userid + "&userToken=" + token +"&staticId=" + staticId

        Utils.showLog(TAG,"GET SUPPORT_FAQ_BUYERS URL : " + url)
        p.callApi(context, url, "contactUs", listener)
    }

    //Contact us
    fun getNotificationCounter() {

        val p = GetRequestParsing()
        val url = API.GET_NOTIFICATION_COUNTER +"?userId=" + userid + "&userToken=" + token

        Utils.showLog(TAG,"GET GET_NOTIFICATION_COUNTER URL : " + url)
        p.callApi(context, url, "notificationCounter", listener)
    }

    //Maintanance Mode
    //Contact us
    fun getMaintananceMode() {

        val p = GetRequestParsing()
        val url = API.MAINTANANCE_MODE +"?userId="  + "&userToken="

        Utils.showLog(TAG,"GET MAINTANANCE_MODE URL : " + url)
        p.callApi(context, url, "maintananceMode", listener)
    }

}