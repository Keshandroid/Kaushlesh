package com.kaushlesh.Controller

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.kaushlesh.R
import com.kaushlesh.interfaces.ParseControllerListener

class PhoneCallController(var context: Activity, var contact: String)  {
    internal lateinit var dialog: Dialog
    fun makePhoneCall() {
        openDialog()
    }

    private fun openDialog() {

        dialog = Dialog(context)
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_logout)
        // Set dialog title
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        //dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_white);
        // set values for custom dialog components - text, image and button
        val title = dialog.findViewById<TextView>(R.id.tv_title)
        val btnyes = dialog.findViewById<Button>(R.id.btn_yes)
        val btnno = dialog.findViewById<Button>(R.id.btn_no)


        title.text =context.getString(R.string.txt_call)+" +91 " + contact

        btnyes.setOnClickListener(View.OnClickListener {

            val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:$contact"))
            context.startActivity(intent)
            dialog.dismiss()
        })

        btnno.setOnClickListener(View.OnClickListener { dialog.dismiss() })

        dialog.show()
    }
}