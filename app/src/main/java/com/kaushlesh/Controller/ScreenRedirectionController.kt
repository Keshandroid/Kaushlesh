package com.kaushlesh.Controller

import android.app.Activity
import android.content.Intent
import com.kaushlesh.view.activity.MainActivity
import com.kaushlesh.view.fragment.MyAccount.MyAds.MyShowcaseActivity




class ScreenRedirectionController(var context: Activity) {

    fun openShowCaseTab() {
        val intent = Intent(context, MainActivity::class.java)
        intent.putExtra("code",9)
        context.startActivity(intent)
        context.finish()
    }

    fun openAdPostTab() {

        val intent = Intent(context, MainActivity::class.java)
        intent.putExtra("code",8)
        context.startActivity(intent)
        context.finish()
    }

    fun openHomeTab() {

        val intent = Intent(context, MainActivity::class.java)
        intent.putExtra("code",7)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        context.startActivity(intent)
        context.finish()
    }

}