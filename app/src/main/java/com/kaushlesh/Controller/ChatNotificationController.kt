package com.kaushlesh.Controller

import android.app.Activity
import android.util.Log
import com.kaushlesh.R
import com.kaushlesh.api.API
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.parser.PostRequestParsing
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import org.json.JSONObject
import java.util.HashMap

class ChatNotificationController(activity: Activity, fromId: Int?, toId: Int?, senderName: String?,textMessage: String?) : ParseControllerListener {

    private val TAG = "ChatNotification"
    internal var activity: Activity = activity

    val storeusedata = StoreUserData(activity)
      val userid = storeusedata.getString(Constants.USER_ID)
      val token = storeusedata.getString(Constants.TOKEN)

    init {
        Log.i(TAG, "onClick")

        val url = API.CHAT_PUSH_NOTIFICATION

        AppConstants.printLog(TAG, "CHAT_PUSH_NOTIFICATION URL ==>$url")

        val p = PostRequestParsing()

        val params = HashMap<String, String>()

        params.put("from_user_id", fromId.toString());
        params.put("to_user_id", toId.toString());
        params.put("from_username", senderName.toString())
        params.put("message", textMessage.toString())



        p.call(activity,url,"chatNotification",params,this);

        AppConstants.printLog(TAG, "chatNotification params: " + params);

        //showProgressDialog(activity, activity.resources.getString(R.string.txt_please_wait))
    }



    override fun onSuccess(da: JSONObject, message: String, method: String) {

        if (da.length() > 0) {
            Log.e(TAG, "ChatNotification result"+ "$da==")
            //dismissProgressDialog()
            try {
                Utils.showLog(TAG,message)

            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            //dismissProgressDialog()
            //  listener.onFail(message,method)
        }
    }

    override fun onFail(msg: String, method: String) {
        //dismissProgressDialog()
        Utils.showLog(TAG,msg)
        //Toast.makeText(activity!!, ""+message, Toast.LENGTH_SHORT).show()

        // listener.onFail(message,method)
    }

    /*fun showProgressDialog(activity: Activity, msg: String) {
        if (Utils.progressDialog==null){
            Utils.showProgress(activity)
        }
    }

    fun dismissProgressDialog() {

        if (Utils.progressDialog != null && Utils.progressDialog!!.isShowing()) {
            Utils.dismissProgress()
        }
    }*/

}