package com.kaushlesh.Controller

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.View
import com.kaushlesh.R
import com.kaushlesh.api.API
import com.kaushlesh.bean.RegisterPhoneBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.parser.PostRequestParsing
import com.kaushlesh.reusable.CustomDialogClass
import com.kaushlesh.utils.Utils
import org.json.JSONObject
import java.util.HashMap

class ResendOtpController(listener: ParseControllerListener,
                          activity: Activity,
                          phone: String) : View.OnClickListener, ParseControllerListener {
    var mProgressDialog: CustomDialogClass? = null
    private val TAG = "RegisterPhoneController"
    internal var activity: Activity = activity
    internal var registerBean: RegisterPhoneBean? = null
    //internal var dbHelper: DBHelper? = dbHelper
    private  var listener: ParseControllerListener = listener

    internal var phone: String = phone


    override fun onClick(v: View) {
        Log.i(TAG, "onClick")

        val url = API.RESEND_OTP

        AppConstants.printLog(TAG, "LOGIN URL ==>$url")
        AppConstants.hideInputSoftKey(activity)

        val p = PostRequestParsing()

        val params = HashMap<String, String>()

        params.put("phone", phone);
        p.call(activity,url,"otpResend",params,this);
        AppConstants.printLog(TAG, "Login param  : " + params);

        showProgressDialog(activity, activity.resources.getString(R.string.txt_please_wait))
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (da.length() > 0) {
            Log.e("LoginResult-", "$da==")

            dismissProgressDialog()
            try {

                listener.onSuccess(da,message,method)

            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            dismissProgressDialog()
            listener.onFail(message,method)
        }
    }

    override fun onFail(msg: String, method: String) {
        dismissProgressDialog()
        listener.onFail(msg,method)
    }


    private fun setAPValueByKey(userId: String?, token: String?, isloggedin: String) {

        val sharedPreferences = activity.getSharedPreferences("kaushlesh", Context.MODE_PRIVATE)
        val prefsEditor = sharedPreferences.edit()
        prefsEditor.putString("userid", userId)
        prefsEditor.putString("token", token)
        prefsEditor.putString("isloggedin", isloggedin)
        prefsEditor.apply()
    }

    fun showProgressDialog(activity: Activity, msg: String) {
        if (Utils.progressDialog==null){
            Utils.showProgress(activity)
        }

    }

    fun dismissProgressDialog() {

        if (Utils.progressDialog != null && Utils.progressDialog!!.isShowing()) {
            Utils.dismissProgress()
        }
    }


}