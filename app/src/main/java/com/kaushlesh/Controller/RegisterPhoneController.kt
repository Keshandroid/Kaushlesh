package com.kaushlesh.Controller

import android.app.Activity
import android.os.Build
import android.provider.Settings.Secure
import android.util.Log
import android.view.View
import com.kaushlesh.R
import com.kaushlesh.api.API
import com.kaushlesh.bean.RegisterPhoneBean
import com.kaushlesh.constant.AppConstants.hideInputSoftKey
import com.kaushlesh.constant.AppConstants.printLog
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.parser.PostRequestParsing
import com.kaushlesh.reusable.CustomDialogClass
import com.kaushlesh.service.LocalStorage
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import org.json.JSONObject
import java.util.*


class RegisterPhoneController(
    listener: ParseControllerListener,
    activity: Activity,
    phone: String, isphone: Int
) : View.OnClickListener, ParseControllerListener {
    var mProgressDialog: CustomDialogClass? = null
    private val TAG = "RegisterPhoneController"
    internal var activity: Activity = activity
    internal var registerBean: RegisterPhoneBean? = null
    //internal var dbHelper: DBHelper? = dbHelper
    private  var listener: ParseControllerListener = listener

    var deviceId = Secure.getString(activity.getContentResolver(), Secure.ANDROID_ID)


    internal var phone: String = phone
    internal var deviceid:String = deviceId
    internal var devicetype:String = "android"
    internal var registertypes:String = "phone"
    internal var devicetoken:String = ""
    internal var isphone: String = isphone.toString()

    lateinit var localStorage: LocalStorage


    override fun onClick(v: View) {
        Log.i(TAG, "onClick")

        val url = API.REGISTER_PHONE_URL

        printLog(TAG, "LOGIN URL ==>$url")
        hideInputSoftKey(activity)

        val p = PostRequestParsing()

        localStorage = LocalStorage(activity)

        devicetoken = localStorage.getToken().toString()


        val params = HashMap<String, String>()

        params.put("phone", phone);
        params.put("device_type", devicetype);
        params.put("device_id", deviceid);
        params.put("device_token", devicetoken);
        params.put("registration_type", registertypes);
        params.put("isPhone", isphone);

        //Device name
        params.put("deviceName", Build.MODEL);

        //Device OS version
        params.put("versionName", Build.VERSION.RELEASE);


        params.put("registerfrom", "android"); // new added

        p.call(activity, url, "registerphone", params, this);
        printLog(TAG, "Login param  : " + params);

        showProgressDialog(activity, activity.resources.getString(R.string.txt_please_wait))
    }

    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (da.length() > 0) {
            Log.e("LoginResult-", "$da==")

            dismissProgressDialog()
            try {

                setAPValueByKey(
                    da.getString("userid"), da.getString("userToken"), "false", da.getString(
                        "isOTPVerified"
                    ), da.getString("isActive")
                )
                listener.onSuccess(da, message, method)

            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            dismissProgressDialog()
            listener.onFail(message, method)
        }
    }

    override fun onFail(msg: String, method: String) {
        dismissProgressDialog()
        listener.onFail(msg, method)
    }


    private fun setAPValueByKey(
        userId: String?,
        token: String?,
        isloggedin: String,
        isOTPVerified: String,
        isActive: String
    ) {


        val storeusedata = StoreUserData(activity)
        storeusedata.setString(Constants.USER_ID, userId.toString())
        storeusedata.setString(Constants.TOKEN, token.toString())
        storeusedata.setString(Constants.IS_LOGGED_IN, isloggedin.toString())
        storeusedata.setString(Constants.IS_OTP_VERIFED, isOTPVerified.toString())
        storeusedata.setString(Constants.IS_ACTIVE, isActive.toString())
        storeusedata.setString(Constants.DEVICE_TOKEN, "")
        storeusedata.setString(Constants.DEVICE_TYPE, "android")
        storeusedata.setString(Constants.REGISTREATION_TYPE, "phone")

    }

    fun showProgressDialog(activity: Activity, msg: String) {
        if (Utils.progressDialog==null){
            Utils.showProgress(activity)
        }

        /*   if (mProgressDialog == null) {

               Utils.progressDialog =
               mProgressDialog!!.setMessage(msg)
           }
           mProgressDialog!!.show()*/
    }

    fun dismissProgressDialog() {

        if (Utils.progressDialog != null && Utils.progressDialog!!.isShowing()) {
            Utils.dismissProgress()
        }
    }

}