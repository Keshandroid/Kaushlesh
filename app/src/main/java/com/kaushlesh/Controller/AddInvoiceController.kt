package com.kaushlesh.Controller

import android.app.Activity
import android.util.Log
import com.android.volley.*
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.kaushlesh.R
import com.kaushlesh.api.API
import com.kaushlesh.bean.InvoiceRequest
import com.kaushlesh.bean.UserProfileBean
import com.kaushlesh.constant.AppConstants.printLog
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.reusable.VolleyMultipartRequests
import com.kaushlesh.reusable.VolleySingleTon
import com.kaushlesh.utils.BusinessProfileData
import com.kaushlesh.utils.Utils
import org.json.JSONException
import org.json.JSONObject
import java.lang.reflect.Type
import java.net.URLEncoder
import java.util.*


class AddInvoiceController(var context: Activity, var parseControllerListener: ParseControllerListener) {

    fun addInvoice(invoiceRequest: InvoiceRequest, userModel: UserProfileBean) {

        //showProgressDialog(context, context.resources.getString(R.string.txt_please_wait))
        Utils.showProgress(context)
        val multipartRequest =
            object : VolleyMultipartRequests(Request.Method.POST, API.ADD_BILLING_PROFILE,
                    Response.Listener<NetworkResponse> { response ->

                        Log.e("test", "response : $response")

                        //dismissProgressDialog()
                        Utils.dismissProgress()

                        val resultResponse = String(response.data)
                        try {
                            val resultJson = JSONObject(resultResponse)

                            val status = resultJson.getString("status")
                            val msg = resultJson.getString("message")

                            if (status == "1") {

                                Log.e("test", "JSON_DATA : "+GsonBuilder().setPrettyPrinting().create().toJson(resultJson))

                                parseControllerListener.onSuccess(resultJson, msg, "addInvoice")


                                /*if (resultJson.has("result")) {
                                    val data = resultJson.getJSONObject("result")

                                    parseControllerListener.onSuccess(data, msg, "addInvoice")
                                }*/

                            }
                            if (status == "0") {

                                parseControllerListener.onFail(msg, "addInvoice")
                            }

                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }, Response.ErrorListener { error ->
                val networkResponse = error.networkResponse
                var errorMessage = "Unknown error"

                Utils.dismissProgress() //new added

                if (networkResponse == null) {
                    if (error.javaClass == TimeoutError::class.java) {
                        errorMessage = "Request timeout"
                    } else if (error.javaClass == NoConnectionError::class.java) {
                        errorMessage = "Failed to connect server"
                        Utils.showAlertConnection(context)
                    }
                } else {
                    val result = String(networkResponse.data)
                    try {
                        val response = JSONObject(result)
                        val status = response.getString("status")
                        val message = response.getString("message")

                        Log.e("Error Status", status)
                        Log.e("Error Message", message)

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found"
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = "$message Please login again"
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = "$message Check your inputs"
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = "$message Something is getting wrong"
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                }

                Log.i("Error", errorMessage)
                error.printStackTrace()
            }) {
                override fun getParams(): Map<String, String> {
                    val params = HashMap<String, String>()

                    params["userId"] = invoiceRequest.userId.toString()
                    params["userToken"] = invoiceRequest.userToken.toString()

                    params["is_gst"] = invoiceRequest.is_gst.toString()

                    params["billing_email"] = invoiceRequest.billing_email.toString()
                    params["billing_name"] = invoiceRequest.billing_name.toString()
                    params["billing_bussiness_name"] = invoiceRequest.billing_bussiness_name.toString()
                    params["billing_gst_no"] = invoiceRequest.billing_gst_no.toString()
                    params["billing_address1"] = invoiceRequest.billing_address1.toString()
                    params["billing_address2"] = invoiceRequest.billing_address2.toString()
                    params["billing_state"] = invoiceRequest.billing_state.toString()
                    params["billing_city"] = invoiceRequest.billing_city.toString()
                    params["billing_district"] = invoiceRequest.billing_district.toString()




                    //for gujarati language
                    params.keys.forEach {
                        if(it == "billing_name" || it == "billing_bussiness_name" || it == "billing_address1" ||
                                it == "billing_address2" || it == "billing_state" || it == "billing_city"){
                            params[it] = URLEncoder.encode(params[it],"UTF-8")
                        }
                    }


                    printLog("AddInvoice", "AddInvoice OTHER VALUES : $params")
                    return params
                }




                /*protected override val byteData: Map<String, DataPart>
                    get() {
                        val params = HashMap<String, DataPart>()

                        if (businessProfileData.visitingcard_front!= null) {
                            params["visitingcard_front"] = DataPart(
                                    "file_profile.jpg",
                                    Utils.getFileDataFromDrawable(context, businessProfileData.visitingcard_front!!),
                                    "image/jpeg"
                            )
                        }

                        if (businessProfileData.visitingcard_back!= null) {

                            params["visitingcard_back"] = DataPart(
                                    "file_profile.jpg",
                                    Utils.getFileDataFromDrawable(context, businessProfileData.visitingcard_back!!),
                                    "image/jpeg"
                            )
                        }


                        Log.i("BusinessProfile", "=======AddBusinessProfile IMAGES========$params")

                        return params

                    }*/

            }
        VolleySingleTon.getInstance(context).addToRequestQueue(multipartRequest)
    }


    fun showProgressDialog(activity: Activity, msg: String) {
        if (Utils.progressDialog==null){
            Utils.showProgress(activity)
        }
    }

    fun dismissProgressDialog() {

        if (Utils.progressDialog != null && Utils.progressDialog!!.isShowing()) {
            Utils.dismissProgress()
        }
    }

}

