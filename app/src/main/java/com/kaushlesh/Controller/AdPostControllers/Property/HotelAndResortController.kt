package com.kaushlesh.Controller.AdPostControllers.Property

import android.app.Activity
import android.util.Log
import com.android.volley.*
import com.kaushlesh.bean.AdPost.HotelandResortBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.reusable.VolleyMultipartRequests
import com.kaushlesh.reusable.VolleySingleTon
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import org.json.JSONException
import org.json.JSONObject
import java.net.URLEncoder
import java.util.HashMap

class HotelAndResortController(var context: Activity, internal var listener: ParseControllerListener) {

    val storeusedata = StoreUserData(context)
    val userid = storeusedata.getString(Constants.USER_ID)
    val token = storeusedata.getString(Constants.TOKEN)


    fun adHotelandResort(hotelandResortBean: HotelandResortBean, url: String?) {
        //Utils.showProgress(context)

        Utils.showProgressDialogWithTitle(context,"wait processing...")


        val multipartRequest = object : VolleyMultipartRequests(
            Request.Method.POST, url.toString(),
            Response.Listener<NetworkResponse> { response ->
                Log.i("test", "==================On Response=========$response")
                val resultResponse = String(response.data)
                try {
                    val resultJson = JSONObject(resultResponse)

                    Utils.dismissProgress()
                    //progress_spinner!!.dismiss()
                    //  dismissProgressDialog()

                    val success = resultJson.getString("status")
                    val msg = resultJson.getString("message")

                    Log.i("test", "========= success msg==========$success")
                    if (success == "1") {

                        //Utils.showToast(context,msg)
                        listener.onSuccess(resultJson,msg,"hotelresort")

                        //val intent = Intent(this, MainActivity::class.java)
                        //startActivity(intent)
                    }
                    if (success == "0") {

                        listener.onFail(msg,"")
                        Utils.showToast(context,msg)
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }, Response.ErrorListener { error ->
                val networkResponse = error.networkResponse
                var errorMessage = "Unknown error"

            Utils.dismissProgress() //new added

                if (networkResponse == null) {
                    if (error.javaClass == TimeoutError::class.java) {
                        errorMessage = "Request timeout"
                    } else if (error.javaClass == NoConnectionError::class.java) {
                        errorMessage = "Failed to connect server"
                        Utils.showAlertConnection(context)
                    }
                } else {
                    val result = String(networkResponse.data)
                    try {
                        val response = JSONObject(result)
                        val status = response.getString("status")
                        val message = response.getString("message")

                        Log.e("Error Status", status)
                        Log.e("Error Message", message)

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found"
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = "$message Please login again"
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = "$message Check your inputs"
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = "$message Something is getting wrong"
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }


                }

                Log.i("Error", errorMessage)
                error.printStackTrace()
            }) {
            protected override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()

                params.put("userId", userid);
                params.put("userToken", token);
                params.put("category_id", storeusedata.getString(Constants.CATEGORY_ID));
                params.put("sub_category_id",storeusedata.getString(Constants.SUBCATEGORYID));
                params.put("package_id", hotelandResortBean.package_id.toString());
                params.put("other_information", hotelandResortBean.other_information.toString());
                params.put("title", hotelandResortBean.title.toString());
                params.put("location_id", storeusedata.getString(Constants.LOCATION_ID));
                //params.put("location_id", "1");
                params.put("address", hotelandResortBean.address.toString());
                params.put("latitude", hotelandResortBean.latitude.toString());
                params.put("longitude", hotelandResortBean.longitude.toString());
                params.put("price", hotelandResortBean.price.toString());
                params.put("post_image_counter", hotelandResortBean.post_images?.size.toString());
                params.put("purpose",  hotelandResortBean.purpose.toString());
                params.put("property_name",  hotelandResortBean.property_name.toString());
                params.put("listed_by",  hotelandResortBean.listed_by.toString());
                params.put("total_rooms",  hotelandResortBean.total_rooms.toString());
                params.put("type_of_rooms",  hotelandResortBean.type_of_rooms.toString());
                params.put("hotel_service_facility",  hotelandResortBean.hotel_service_facility.toString());
                params.put("property_type",  hotelandResortBean.property_type.toString());
                params.put("userpostpurchasepackage_id", storeusedata.getString(Constants.USER_PKG_ID));

                params.keys.forEach {
                    params[it] = URLEncoder.encode(params[it],"UTF-8")
                }

                Log.e("add task: ", params.toString())

                return params
            }

            override val byteData: Map<String, DataPart>

                get() {

                    val params = HashMap<String, DataPart>()

                    if(hotelandResortBean.post_images!!.size > 0) {
                        for (i in 1..hotelandResortBean.post_images!!.size)

                            params.put(
                                "post_images" + i,
                                DataPart(
                                    "post_images" + i + ".jpg",
                                    AppConstants.getByteArrayFromBitmap(hotelandResortBean.post_images!![i - 1]),
                                    "image/jpeg"
                                )
                            )

                        //params["image_file_$i"] = DataPart("image_$i.jpg", AppConstants.getByteArrayFromBitmap(imageList[i - 1]))
                        Log.e("add images: ", params.toString())
                    }

                    return params
                }

        }

        val socketTimeout = 30000;//30 seconds - change to what you want
        val policy = DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        multipartRequest.setRetryPolicy(policy);
        //  myReq.setRetryPolicy(DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))
        VolleySingleTon.getInstance(context).addToRequestQueue(multipartRequest)
    }
}