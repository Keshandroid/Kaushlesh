package com.kaushlesh.Controller.AdPostControllers.Bikes

import android.app.Activity
import android.util.Log
import com.android.volley.*
import com.kaushlesh.bean.AdPost.*
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.reusable.VolleyMultipartRequests
import com.kaushlesh.reusable.VolleySingleTon
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import org.json.JSONException
import org.json.JSONObject
import java.net.URLEncoder
import java.util.HashMap

class MotorScooterAdPostController(var context: Activity, internal var listener: ParseControllerListener) {

    val storeusedata = StoreUserData(context)
    val userid = storeusedata.getString(Constants.USER_ID)
    val token = storeusedata.getString(Constants.TOKEN)

    fun adMotorScooter(adPostBikesBean: AdPostBikesBean, url: String?) {
        //Utils.showProgress(context)

        Utils.showProgressDialogWithTitle(context,"wait processing...")


        val multipartRequest = object : VolleyMultipartRequests(
            Request.Method.POST, url.toString(),
            Response.Listener<NetworkResponse> { response ->
                Log.i("test", "==================On Response=========$response")
                val resultResponse = String(response.data)
                try {
                    val resultJson = JSONObject(resultResponse)

                    Utils.dismissProgress()
                    //progress_spinner!!.dismiss()
                    //  dismissProgressDialog()

                    val success = resultJson.getString("status")
                    val msg = resultJson.getString("message")

                    Log.i("test", "========= success msg==========$success")
                    if (success == "1") {

                        //Utils.showToast(context,msg)
                        listener.onSuccess(resultJson,msg,"bikes")

                        //val intent = Intent(this, MainActivity::class.java)
                        //startActivity(intent)
                    }
                    if (success == "0") {

                        listener.onFail(msg,"")
                        Utils.showToast(context,msg)
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }, Response.ErrorListener { error ->
                val networkResponse = error.networkResponse
                var errorMessage = "Unknown error"

            Utils.dismissProgress() //new added

                if (networkResponse == null) {
                    if (error.javaClass == TimeoutError::class.java) {
                        errorMessage = "Request timeout"
                    } else if (error.javaClass == NoConnectionError::class.java) {
                        errorMessage = "Failed to connect server"
                        Utils.showAlertConnection(context)
                    }
                } else {
                    val result = String(networkResponse.data)
                    try {
                        val response = JSONObject(result)
                        val status = response.getString("status")
                        val message = response.getString("message")

                        Log.e("Error Status", status)
                        Log.e("Error Message", message)

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found"
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = "$message Please login again"
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = "$message Check your inputs"
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = "$message Something is getting wrong"
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }


                }

                Log.i("Error", errorMessage)
                error.printStackTrace()
            }) {
            protected override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()

                params.put("userId", userid);
                params.put("userToken", token);
                params.put("category_id", storeusedata.getString(Constants.CATEGORY_ID));
                params.put("sub_category_id",storeusedata.getString(Constants.SUBCATEGORYID));
                params.put("package_id", adPostBikesBean.package_id.toString());
                params.put("other_information", adPostBikesBean.other_information.toString());
                params.put("title", adPostBikesBean.title.toString());
                params.put("location_id", storeusedata.getString(Constants.LOCATION_ID));
                //params.put("location_id", "1");
                params.put("address", adPostBikesBean.address.toString());
                params.put("latitude", adPostBikesBean.latitude.toString());
                params.put("longitude", adPostBikesBean.longitude.toString());
                params.put("price", adPostBikesBean.price.toString());
                params.put("post_image_counter", adPostBikesBean.post_images?.size.toString());
                params.put("brand_id", adPostBikesBean.brandId.toString());
                params.put("bike_year", adPostBikesBean.year.toString());
                params.put("fuel", adPostBikesBean.fuel.toString());
                params.put("km_driven", adPostBikesBean.kmDriven.toString());
                params.put("userpostpurchasepackage_id", storeusedata.getString(Constants.USER_PKG_ID));

                params.keys.forEach {
                    params[it] = URLEncoder.encode(params[it],"UTF-8")
                }
                Log.e("add task: ", params.toString())

                return params
            }

            override val byteData: Map<String, DataPart>

                get() {

                    val params = HashMap<String, DataPart>()

                    if(adPostBikesBean.post_images!!.size > 0) {
                        for (i in 1..adPostBikesBean.post_images!!.size)

                            params.put(
                                "post_images" + i,
                                DataPart(
                                    "post_images" + i + ".jpg",
                                    AppConstants.getByteArrayFromBitmap(adPostBikesBean.post_images!![i - 1]),
                                    "image/jpeg"
                                )
                            )

                        //params["image_file_$i"] = DataPart("image_$i.jpg", AppConstants.getByteArrayFromBitmap(imageList[i - 1]))
                        Log.e("add images: ", params.toString())
                    }

                    return params
                }

        }

        val socketTimeout = 30000;//30 seconds - change to what you want
        val policy = DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        multipartRequest.setRetryPolicy(policy);
        //  myReq.setRetryPolicy(DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))
        VolleySingleTon.getInstance(context).addToRequestQueue(multipartRequest)
    }
}