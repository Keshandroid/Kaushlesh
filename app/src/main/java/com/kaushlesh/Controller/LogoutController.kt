package com.kaushlesh.Controller

import android.app.Activity
import android.util.Log
import com.kaushlesh.R
import com.kaushlesh.api.API
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.parser.PostRequestParsing
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import org.json.JSONObject
import java.util.HashMap

class LogoutController(activity: Activity,listener: ParseControllerListener) : ParseControllerListener {

    private val TAG = "LogoutApp"
    internal var activity: Activity = activity
    private  var listener: ParseControllerListener = listener


    val storeusedata = StoreUserData(activity)
      val userid = storeusedata.getString(Constants.USER_ID)
      val token = storeusedata.getString(Constants.TOKEN)
    //val businessProfileId = storeusedata.getString(Constants.BUSINESS_USER_ID)


    init {
        Log.i(TAG, "onClick")

        val url = API.LOGOUT_APP

        AppConstants.printLog(TAG, "Logout URL ==>$url")

        val p = PostRequestParsing()

        val params = HashMap<String, String>()

        params.put("userId", userid.toString());
        params.put("userToken", token.toString());


        p.call(activity,url,"logoutApp",params,this);

        AppConstants.printLog(TAG, "logoutApp params: " + params);

        showProgressDialog(activity, activity.resources.getString(R.string.txt_please_wait))
    }



    override fun onSuccess(da: JSONObject, message: String, method: String) {
        if (da.length() > 0) {
            Log.e(TAG, "logoutApp result"+ "$da==")
            dismissProgressDialog()
            try {
                Utils.showLog(TAG,message)
                listener.onSuccess(da,message,method)

            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            dismissProgressDialog()
            //  listener.onFail(message,method)
        }
    }

    override fun onFail(msg: String, method: String) {
        dismissProgressDialog()
        Utils.showLog(TAG,msg)
        listener.onFail(msg,method)

        //Toast.makeText(activity!!, ""+message, Toast.LENGTH_SHORT).show()

        // listener.onFail(message,method)
    }

    fun showProgressDialog(activity: Activity, msg: String) {
        if (Utils.progressDialog==null){
            Utils.showProgress(activity)
        }
    }

    fun dismissProgressDialog() {

        if (Utils.progressDialog != null && Utils.progressDialog!!.isShowing()) {
            Utils.dismissProgress()
        }
    }

}