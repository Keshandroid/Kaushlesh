package com.kaushlesh.constant

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.graphics.drawable.ColorDrawable
import android.media.ExifInterface
import android.net.ConnectivityManager
import android.net.Uri
import android.os.ParcelFileDescriptor
import android.util.Base64
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.gson.Gson
import com.kaushlesh.R
import com.kaushlesh.bean.AdPost.*
import com.kaushlesh.utils.AdPostImages
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.AdPost.UploadActivity1
import com.kaushlesh.view.activity.Login.LoginMainActivity
import com.kaushlesh.view.fragment.MyAccount.EditProfileActivity
import java.io.*
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

object AppConstants {
    internal var imageList: ArrayList<Bitmap> = ArrayList()
    val AP_IS_LOGGED_IN: String = "is_logged_in"
    internal var imageListuri: ArrayList<Uri> = ArrayList()

    val currentDateTime:String
        get() {

            var strDate:String? = null
            val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val date = Date()
            strDate = formatter.format(date)

            return strDate
        }

    //public static CustomDialogClass mProgressDialog;

    fun isValidEmail(emailText: String):Boolean {
        val pattern: Pattern
        val matcher: Matcher

        val EMAIL_PATTERN = ("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")
        pattern = Pattern.compile(EMAIL_PATTERN)
        matcher = pattern.matcher(emailText)
        return matcher.matches()
    }

    fun nullCheck(activity: Activity, string: String?, message: String):Boolean {
        if (string != null && string.length > 0)
        {
            return true
        }
        else
        {
            if (message.length > 0)
                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
            return false
        }

    }

    fun nullCheckBoolean(activity: Activity, value: Boolean?, message: String):Boolean {
        if (value != null && value == true)
        {
            return true
        }
        else
        {
            if (message.length > 0)
                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
            return false
        }

    }

    fun colorChangeString(username: String, comment: String):String {
        return "<font color=#FB5A83>$username</font>  <font color =#828285>$comment </font > "
    }

    fun hashkey(context: Context) {
        try
        {
            val info = context.packageManager.getPackageInfo(
                    "com.myriam",
                    PackageManager.GET_SIGNATURES)
            for (signature in info.signatures)
            {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                AppConstants.printLog("SIGNACTIVITY", "KeyHash:" + Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        }
        catch (e: PackageManager.NameNotFoundException) {

        }
        catch (e: NoSuchAlgorithmException) {

        }

    }

    fun getCurrentDateFormat(date: String):String {
        var date = date


        var spf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        var newDate: Date
        try
        {
            newDate = spf.parse(date)
            spf = SimpleDateFormat("dd MMMM, yyyy")
            date = spf.format(newDate)

            AppConstants.printLog("F_Date", date + "")
        }
        catch (e: ParseException) {
            e.printStackTrace()
        }

        return date
    }


    fun modifyOrientation(bitmap: Bitmap, image_absolute_path: String): Bitmap? {

        try
        {
            val ei = ExifInterface(image_absolute_path)
            val orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)

            when (orientation) {
                ExifInterface.ORIENTATION_ROTATE_90 -> return rotate(bitmap, 90f)

                ExifInterface.ORIENTATION_ROTATE_180 -> return rotate(bitmap, 180f)

                ExifInterface.ORIENTATION_ROTATE_270 -> return rotate(bitmap, 270f)

                ExifInterface.ORIENTATION_FLIP_HORIZONTAL -> return flip(bitmap, true, false)

                ExifInterface.ORIENTATION_FLIP_VERTICAL -> return flip(bitmap, false, true)


                else -> return bitmap
            }

        }
        catch (e: Exception) {
            return null
        }

    }

    fun rotate(bitmap: Bitmap, degrees: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(degrees)
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
    }


    fun flip(bitmap: Bitmap, horizontal: Boolean, vertical: Boolean): Bitmap {
        val matrix = Matrix()
        matrix.preScale((if (horizontal) -1 else 1).toFloat(), (if (vertical) -1 else 1).toFloat())
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
    }

    fun isNetworkAvailable(context: Context):Boolean {

        var isMobile = false
        var isWifi = false
        val cm = context
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val infoAvailableNetworks = cm.allNetworkInfo

        for (network in infoAvailableNetworks)
        {

            if (network.type == ConnectivityManager.TYPE_WIFI)
            {
                if (network.isConnected && network.isAvailable)
                    isWifi = true
            }
            if (network.type == ConnectivityManager.TYPE_MOBILE)
            {
                if (network.isConnected && network.isAvailable)
                    isMobile = true
            }
        }

        if (isMobile || isWifi)
        {}
        else
        {
            Toast.makeText(context, context.getString(R.string.toast_internet), Toast.LENGTH_SHORT).show()
        }

        return isMobile || isWifi
    }


    fun printLog(msg: String, value: String) {
        Log.e(msg, value)
    }

    fun printToast(context: Context, message: String) {
        //Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        val toast = Toast(context)
        val view: View = LayoutInflater.from(context).inflate(R.layout.toast_custom, null)
        val textView =
                view.findViewById<View>(R.id.custom_toast_text) as TextView
        textView.text = message
        toast.setView(view)
        toast.setGravity(Gravity.BOTTOM or Gravity.CENTER, 0, 0)
        toast.setDuration(Toast.LENGTH_LONG)
        toast.show()
    }

    fun urlReplace(url: String):String {
        var url = url
        url = url.replace((" ").toRegex(), "%20")
        return url
    }

    fun findWord(string: String, word: String):Boolean {
        return if (string.toLowerCase().indexOf(word.toLowerCase()) > -1) {
            true
        } else false
    }

    fun isImage(image: String):Boolean {
        return if ((findWord(image, ".jpg")
                        || findWord(image, ".jpeg")
                        || findWord(image, ".png"))
        ) true else false
    }

    fun isVideo(video: String):Boolean {
        return if ((findWord(video, ".mp4")
                        || findWord(video, ".3gp") || findWord(video, ".mp3"))
        ) true else false
    }

    fun getMilliFromDate(dateFormat: String):Long {
        var date: Date? = Date()
        val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        try
        {
            date = formatter.parse(dateFormat)
        }
        catch (e: ParseException) {
            e.printStackTrace()
        }

        println("Today is " + date!!)
        return date.time
    }

    fun getByteArrayFromBitmap(bitmap: Bitmap):ByteArray {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
        return byteArrayOutputStream.toByteArray()
    }

    fun rotateImage(source: Bitmap, angle: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(source, 0, 0, source.width, source.height, matrix, true)
    }


    fun alertDialog(activity: Activity, message: String, intent: Intent) {
        val alertDialog = AlertDialog.Builder(activity, R.style.AlertDialogTheme).create()
        alertDialog.setCancelable(false)
        alertDialog.setTitle(R.string.app_name)
        alertDialog.setMessage(message)
        alertDialog.setIcon(R.drawable.ic_gl_logo)
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, activity.resources.getString(R.string.txt_ok)
        ) { aDialogInterface, aI ->
            aDialogInterface.dismiss()
            activity.startActivity(intent)
            activity.finish()
        }

        alertDialog.show()
    }


    fun alertLogin(activity: Activity, message: String, intent: Intent) {
        val dialog = Dialog(activity)
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_login_alert)
        // Set dialog title
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        //dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_white);
        // set values for custom dialog components - text, image and button
        val title = dialog.findViewById<TextView>(R.id.tv_title)
        val btnyes = dialog.findViewById<Button>(R.id.btn_yes)
        val btnno = dialog.findViewById<Button>(R.id.btn_no)

        val storeusedata = StoreUserData(activity)
        val regtype =storeusedata.getString(Constants.REGISTREATION_TYPE)
        if(regtype != null && regtype.length > 0)
        {
            if(regtype.equals("phone"))
            {
                title.text = activity.resources.getString(R.string.txt_update_district)
                btnyes.text = activity.resources.getString(R.string.txt_update)
            }
            else if(regtype.equals("google") || regtype.equals("facebook"))
            {
                title.text = activity.resources.getString(R.string.txt_update_mobile_district)
                btnyes.text =  activity.resources.getString(R.string.txt_update)
                btnno.text = activity.resources.getString(R.string.txt_logout)
            }
            else{
                title.text = message
            }
        }
        else{
            title.text = message
        }

        btnyes.setOnClickListener(View.OnClickListener {

            if (regtype != null && regtype.length > 0) {
                if (regtype.equals("phone") || regtype.equals("google") || regtype.equals("facebook")) {

                    val intent1 = Intent(activity, EditProfileActivity::class.java)
                    activity.startActivity(intent1)
                    activity.finish()
                    dialog.dismiss()
                } else {
                    activity.startActivity(intent)
                    activity.finish()
                    dialog.dismiss()
                }
            } else {
                activity.startActivity(intent)
                activity.finish()
                dialog.dismiss()
            }
            /* activity.startActivity(intent)
             activity.finish()
             dialog.dismiss()*/
        })

        btnno.setOnClickListener(View.OnClickListener {

            if (regtype != null && regtype.length > 0) {
                if (regtype.equals("google") || regtype.equals("google")) {
                    val intent2 = Intent(activity, LoginMainActivity::class.java)
                    activity.startActivity(intent2)
                    activity.finishAffinity()
                    dialog.dismiss()
                } else {
                    dialog.dismiss()
                }

            }
        })

        dialog.show()
    }

    fun alertBlock(activity: Activity, message: String, intent: Intent) {
        val dialog = Dialog(activity)
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_login_alert)
        // Set dialog title
        dialog.setTitle("")
        dialog.setCancelable(false)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        //dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_white);
        // set values for custom dialog components - text, image and button
        val title = dialog.findViewById<TextView>(R.id.tv_title)
        val btnyes = dialog.findViewById<Button>(R.id.btn_yes)
        val btnno = dialog.findViewById<Button>(R.id.btn_no)
        btnyes.setText(activity.resources.getString(R.string.txt_ok))
        title.setText(message)
        btnno.visibility = View.GONE

        btnyes.setOnClickListener(View.OnClickListener {

            activity.startActivity(intent)
            activity.finish()
            dialog.dismiss()
        })

        btnno.setOnClickListener(View.OnClickListener {

        })

        dialog.show()
    }

    fun alertDialog(activity: Activity, title: String, message: String) {
        val alertDialog = AlertDialog.Builder(activity).create()
        alertDialog.setCancelable(false)
        alertDialog.setTitle(R.string.app_name)
        alertDialog.setMessage(message)
        alertDialog.setIcon(R.mipmap.ic_launcher)
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, activity.resources.getString(R.string.txt_ok), object :
                DialogInterface.OnClickListener {
            override fun onClick(aDialogInterface: DialogInterface, aI: Int) {
                aDialogInterface.dismiss()

            }
        })

        alertDialog.show()

        // Getting the view elements
        val textView = alertDialog.window!!.findViewById<View>(android.R.id.message) as TextView
        val alertTitle = alertDialog.window!!.findViewById(R.id.alertTitle) as TextView
        val button1 = alertDialog.window!!.findViewById<View>(android.R.id.button1) as Button
        val button2 = alertDialog.window!!.findViewById<View>(android.R.id.button2) as Button

        val type = Typeface.createFromAsset(activity.assets, "fonts/" + "SFRegular.otf")

        // Setting font
        textView.setTypeface(type)
        alertTitle.setTypeface(type)
        button1.setTypeface(type)
        button2.setTypeface(type)
    }

    /* public static void  showProgressDialog(final Activity activity, String msg) {
           if (mProgressDialog == null) {
               mProgressDialog = new CustomDialogClass(activity, CustomDialogClass.DT_PROGRESS_MSG);
               mProgressDialog.setMessage(msg);
           }

          mProgressDialog.show();
       }

       public static void  dismissProgressDialog() {
           if (mProgressDialog != null && mProgressDialog.isShowing()) {
               mProgressDialog.dismiss();
           }
       }*/

    fun hideInputSoftKey(activity: Activity) {
        try
        {
            val inputManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)

        }
        catch (aE: Exception) {
            aE.printStackTrace()
        }

    }

    fun hideKeyboard(activity: Activity) {
        try {
            val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            var view = activity.currentFocus
            if (view == null) {
                view = View(activity)
            }
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }catch (e:Exception){
            e.printStackTrace()
        }

    }

    fun checkUserIsLoggedin(activity: Activity): Boolean {
        val storeusedata = StoreUserData(activity)
        val isloggedin = storeusedata.getString(Constants.IS_LOGGED_IN)
        return isloggedin.equals("true")
    }


    fun getSize(size: Long): String? {
        val n: Long = 1000
        var s = ""
        val kb = size / n.toDouble()
        val mb = kb / n
        val gb = mb / n
        val tb = gb / n
        if (size < n) {
            s = "$size Bytes"
        } else if (size >= n && size < n * n) {
            s = String.format("%.2f", kb) + " KB"
        } else if (size >= n * n && size < n * n * n) {
            s = String.format("%.2f", mb) + " MB"
        } else if (size >= n * n * n && size < n * n * n * n) {
            s = String.format("%.2f", gb) + " GB"
        } else if (size >= n * n * n * n) {
            s = String.format("%.2f", tb) + " TB"
        }
        return s
    }


    fun showMultiImage1(uriList: List<Uri>, context: Context, categoryId: Int, subCategoryId: Int) {
        var resizebitmapmain: Bitmap

        Log.d("ted=", "uriList: $uriList")
        Log.e("uriList.get(0)====", "uriList.get(0): $uriList.get(0)")

        Thread(Runnable {
            imageList.clear()
            imageListuri.clear()
            uriList.forEach {

                imageListuri.add(it)
                var imagepath = getPathFromGooglePhotosUri(it, context)
                resizebitmapmain = decodeImageFromFiles(imagepath, 280, 280)
                Log.e("resizebitmapmain====", "01--: $resizebitmapmain")
                imageList.add(resizebitmapmain);
            }

            Log.e("imageListsize====", "viewSize: ${imageList.size}")

            if (AppConstants.imageList != null) {
                Log.e("AppConstantsimageList==", "viewSize: ${imageList.size}")

                checkCategoryANDsubCategory(context, categoryId, subCategoryId)
                //checkAndSaveJob(context)
            }

        }).start()
        checkCategorytoRedirect(context, categoryId)
       // AdPostImages.changeScreentoLocation(context)

    }

    private fun checkCategorytoRedirect(context: Context, categoryId: Int) {
        if(categoryId == 1|| categoryId == 2|| categoryId == 3||categoryId == 4|| categoryId == 5 ||categoryId == 6 || categoryId == 8 || categoryId == 9)
        {
            AdPostImages.changeScreentoNext(context)
        }
        if(categoryId == 7 || categoryId == 10 || categoryId == 11 || categoryId == 12)
        {
            AdPostImages.changeScreentoLocation(context)
        }
    }

    private fun checkCategoryANDsubCategory(context: Context, categoryId: Int, subCategoryId: Int) {
        if(categoryId == 1)
        {
            checkAndSaveVehicles(context)
        }
        if(categoryId == 2)
        {
            checkAndSaveSubcategory(context, subCategoryId)
        }
        if(categoryId == 3)
        {
            checkAndSaveMobiles(context)
        }
        if(categoryId == 4)
        {
            checkAndSaveBikes(context)
        }
        if(categoryId == 5 || categoryId == 6 ||categoryId == 7 || categoryId == 8 || categoryId == 9 || categoryId == 10 || categoryId == 11)
        {
            checkAndSaveJob(context)
        }
        if(categoryId == 12)
        {
            checkAndSaveKhedut(context)
        }
    }

    private fun checkAndSaveSubcategory(context: Context, subCategoryId: Int) {
        when (subCategoryId.toInt()) {
            1 -> {
                checkAndSaveAppVilla(context)
            }
            2 -> {
                checkAndSaveAppVilla(context)
            }
            3 -> {
                checkAndSaveOffice(context)
            }
            4 -> {
                checkAndSaveOffice(context)
            }
            5 -> {
                checkAndSavePlotLand(context)
            }
            6 -> {
                checkAndSavePartyPlot(context)
            }
            7 -> {
                checkAndSaveHotelResort(context)
            }
            8 -> {
                checkAndSaveLobourCamp(context)
            }
            9 -> {
                checkAndSavePg(context)
            }
        }
    }

    private fun checkAndSavePg(context: Context) {
        ///  bimglist.clear()
        val storeUserData = StoreUserData(context)
        val gson = Gson()

        if (AppConstants.imageList.size >= 1) {

            Log.e("test", "imagelist: " + AppConstants.imageList.size + "===")

            val json: String = storeUserData.getString(Constants.ADDPGGUSTHOUSE)

            Log.e("String", "String data: " + json + "====")

            val pgGuestHouseBean = gson.fromJson(json, PgGuestHouseBean::class.java)

            Log.e("test", "listsave data: " + pgGuestHouseBean.toString() + "====")
            pgGuestHouseBean.post_images?.clear()
            pgGuestHouseBean.post_images = AppConstants.imageList
            Log.e("test", "post_images: " + pgGuestHouseBean.post_images!!.size + "===")


            Log.e("test", "post_images data: " + pgGuestHouseBean.post_images)

            val jsonset = gson.toJson(pgGuestHouseBean)

            storeUserData.setString(Constants.ADDPGGUSTHOUSE, jsonset)

            ////   AdPostImages.changeScreentoLocation(context)

        } else {
            Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
        }
    }

    private fun checkAndSaveLobourCamp(context: Context) {
        ///  bimglist.clear()
        val storeUserData = StoreUserData(context)
        val gson = Gson()

        if (AppConstants.imageList.size >= 1) {

            Log.e("test", "imagelist: " + AppConstants.imageList.size + "===")

            val json: String = storeUserData.getString(Constants.ADDLABOURWAREHOUSE)

            Log.e("String", "String data: " + json + "====")

            val labourWareHouseBean = gson.fromJson(json, LabourWareHouseBean::class.java)

            Log.e("test", "listsave data: " + labourWareHouseBean.toString() + "====")
            labourWareHouseBean.post_images?.clear()
            labourWareHouseBean.post_images = AppConstants.imageList
            Log.e("test", "post_images: " + labourWareHouseBean.post_images!!.size + "===")


            Log.e("test", "post_images data: " + labourWareHouseBean.post_images)

            val jsonset = gson.toJson(labourWareHouseBean)

            storeUserData.setString(Constants.ADDLABOURWAREHOUSE, jsonset)

            ////   AdPostImages.changeScreentoLocation(context)

        } else {
            Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
        }
    }

    private fun checkAndSaveHotelResort(context: Context) {
        ///  bimglist.clear()
        val storeUserData = StoreUserData(context)
        val gson = Gson()

        if (AppConstants.imageList.size >= 1) {

            Log.e("test", "imagelist: " + AppConstants.imageList.size + "===")

            val json: String = storeUserData.getString(Constants.ADDHOTELANDRESORT)

            Log.e("String", "String data: " + json + "====")

            val hotelandResortBean = gson.fromJson(json, HotelandResortBean::class.java)

            Log.e("test", "listsave data: " + hotelandResortBean.toString() + "====")
            hotelandResortBean.post_images?.clear()
            hotelandResortBean.post_images = AppConstants.imageList
            Log.e("test", "post_images: " + hotelandResortBean.post_images!!.size + "===")


            Log.e("test", "post_images data: " + hotelandResortBean.post_images)

            val jsonset = gson.toJson(hotelandResortBean)

            storeUserData.setString(Constants.ADDHOTELANDRESORT, jsonset)

            ////   AdPostImages.changeScreentoLocation(context)

        } else {
            Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
        }
    }

    private fun checkAndSavePartyPlot(context: Context) {
        ///  bimglist.clear()
        val storeUserData = StoreUserData(context)
        val gson = Gson()

        if (AppConstants.imageList.size >= 1) {

            Log.e("test", "imagelist: " + AppConstants.imageList.size + "===")

            val json: String = storeUserData.getString(Constants.ADDPARTYPLOT)

            Log.e("String", "String data: " + json + "====")

            val adPartyPlotBean = gson.fromJson(json, AdPartyPlotBean::class.java)

            Log.e("test", "listsave data: " + adPartyPlotBean.toString() + "====")
            adPartyPlotBean.post_images?.clear()
            adPartyPlotBean.post_images = AppConstants.imageList
            Log.e("test", "post_images: " + adPartyPlotBean.post_images!!.size + "===")


            Log.e("test", "post_images data: " + adPartyPlotBean.post_images)

            val jsonset = gson.toJson(adPartyPlotBean)

            storeUserData.setString(Constants.ADDPARTYPLOT, jsonset)

            ////   AdPostImages.changeScreentoLocation(context)

        } else {
            Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
        }
    }

    private fun checkAndSavePlotLand(context: Context) {
        ///  bimglist.clear()
        val storeUserData = StoreUserData(context)
        val gson = Gson()

        if (AppConstants.imageList.size >= 1) {

            Log.e("test", "imagelist: " + AppConstants.imageList.size + "===")

            val json: String = storeUserData.getString(Constants.ADDPLOTANDLAND)

            Log.e("String", "String data: " + json + "====")

            val adPlotAndLandBean = gson.fromJson(json, AdPlotAndLandBean::class.java)

            Log.e("test", "listsave data: " + adPlotAndLandBean.toString() + "====")
            adPlotAndLandBean.post_images?.clear()
            adPlotAndLandBean.post_images = AppConstants.imageList
            Log.e("test", "post_images: " + adPlotAndLandBean.post_images!!.size + "===")


            Log.e("test", "post_images data: " + adPlotAndLandBean.post_images)

            val jsonset = gson.toJson(adPlotAndLandBean)

            storeUserData.setString(Constants.ADDPLOTANDLAND, jsonset)

            ////   AdPostImages.changeScreentoLocation(context)

        } else {
            Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
        }
    }

    private fun checkAndSaveOffice(context: Context) {
        ///  bimglist.clear()
        val storeUserData = StoreUserData(context)
        val gson = Gson()

        if (AppConstants.imageList.size >= 1) {

            Log.e("test", "imagelist: " + AppConstants.imageList.size + "===")

            val json: String = storeUserData.getString(Constants.ADDOFFICESELLANDRENT)

            Log.e("String", "String data: " + json + "====")

            val adOfficeForSellBeanList = gson.fromJson(json, AdOfficeForSellRentBean::class.java)

            Log.e("test", "listsave data: " + adOfficeForSellBeanList.toString() + "====")
            adOfficeForSellBeanList.post_images?.clear()
            adOfficeForSellBeanList.post_images = AppConstants.imageList
            Log.e("test", "post_images: " + adOfficeForSellBeanList.post_images!!.size + "===")


            Log.e("test", "post_images data: " + adOfficeForSellBeanList.post_images)

            val jsonset = gson.toJson(adOfficeForSellBeanList)

            storeUserData.setString(Constants.ADDOFFICESELLANDRENT, jsonset)

            ////   AdPostImages.changeScreentoLocation(context)

        } else {
            Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
        }
    }

    private fun checkAndSaveAppVilla(context: Context) {
        ///  bimglist.clear()
        val storeUserData = StoreUserData(context)
        val gson = Gson()

        if (AppConstants.imageList.size >= 1) {

            Log.e("test", "imagelist: " + AppConstants.imageList.size + "===")

            val json: String = storeUserData.getString(Constants.APVILLSELLONE)

            Log.e("String", "String data: " + json + "====")

            val adVillsellandRentBeanList = gson.fromJson(json, ApVillsellandRentBean::class.java)

            Log.e("test", "listsave data: " + adVillsellandRentBeanList.toString() + "====")
            adVillsellandRentBeanList.post_images?.clear()
            adVillsellandRentBeanList.post_images = AppConstants.imageList
            Log.e("test", "post_images: " + adVillsellandRentBeanList.post_images!!.size + "===")


            Log.e("test", "post_images data: " + adVillsellandRentBeanList.post_images)

            val jsonset = gson.toJson(adVillsellandRentBeanList)

            storeUserData.setString(Constants.APVILLSELLONE, jsonset)

            ////   AdPostImages.changeScreentoLocation(context)

        } else {
            Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
        }
    }

    private fun checkAndSaveMobiles(context: Context) {
        //AdPostImages.bimglist.clear()

        val storeUserData = StoreUserData(context)
        val gson = Gson()

        if (AppConstants.imageList.size >= 1) {

            Log.e("test", "imagelist: " + AppConstants.imageList.size + "===")

            val json: String = storeUserData.getString(Constants.ADDMOBILE)

            Log.e("String", "String data: " + json + "====")

            val adPostMobilesBean = gson.fromJson(json, AdPostMobilesBean::class.java)

            Log.e("test", "listsave data: " + adPostMobilesBean.toString())
            adPostMobilesBean.post_images?.clear()
            adPostMobilesBean.post_images = AppConstants.imageList
            Log.e("test", "post_images: " + adPostMobilesBean.post_images!!.size + "===")


            Log.e("test", "post_images data: " + adPostMobilesBean.post_images)

            val jsonset = gson.toJson(adPostMobilesBean)

            storeUserData.setString(Constants.ADDMOBILE, jsonset)
        }
        else{
            Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
        }
    }

    private fun checkAndSaveVehicles(context: Context) {
        val storeUserData = StoreUserData(context)
        val gson = Gson()

        if (AppConstants.imageList.size >= 1) {

            Log.e("test", "imagelist: " + AppConstants.imageList.size + "===")

            val json: String = storeUserData.getString(Constants.ADDVEHICLES)

            Log.e("String", "String data: " + json + "====")

            val adPostVehiclesBean = gson.fromJson(json, AdPostVehiclesBean::class.java)

            Log.e("test", "listsave data: " + adPostVehiclesBean.toString())
            adPostVehiclesBean.post_images?.clear()
            adPostVehiclesBean.post_images = AppConstants.imageList
            Log.e("test", "post_images: " + adPostVehiclesBean.post_images!!.size + "===")

            Log.e("test", "post_images data: " + adPostVehiclesBean.post_images)
            val jsonset = gson.toJson(adPostVehiclesBean)

            storeUserData.setString(Constants.ADDVEHICLES, jsonset)
        }
        else{
            Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
        }
    }

    private fun checkAndSaveKhedut(context: Context) {
        val storeUserData = StoreUserData(context)
        val gson = Gson()
        if (AppConstants.imageList.size >= 1) {

            Log.e("test", "imagelist: " + AppConstants.imageList.size + "===")

            val json: String = storeUserData.getString(Constants.ADDKHEDUT)

            Log.e("String", "String data: " + json + "====")

            val adKhedutBean = gson.fromJson(json, AdKhedutBean::class.java)

            Log.e("test", "listsave data: " + adKhedutBean.toString() + "====")
            adKhedutBean.post_images?.clear()
            adKhedutBean.post_images = AppConstants.imageList
            Log.e("test", "post_images: " + adKhedutBean.post_images!!.size + "===")


            Log.e("test", "post_images data: " + adKhedutBean.post_images)

            val jsonset = gson.toJson(adKhedutBean)

            storeUserData.setString(Constants.ADDKHEDUT, jsonset)

        } else {
            Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
        }
    }

    fun getPathFromGooglePhotosUri(uriPhoto: Uri?, context: Context): String? {
        if (uriPhoto == null) return null
        var input: FileInputStream? = null
        var output: FileOutputStream? = null
        try {
            val pfd: ParcelFileDescriptor? =context.contentResolver.openFileDescriptor(uriPhoto, "r")
            val fd: FileDescriptor = pfd!!.getFileDescriptor()
            input = FileInputStream(fd)
            val tempFilename: String = getTempFilename(context)
            output = FileOutputStream(tempFilename)
            var read: Int = 0
            val bytes = ByteArray(4096)
            while (input.read(bytes).also({ read = it }) != -1) {
                output.write(bytes, 0, read)
            }
            return tempFilename
        } catch (ignored: IOException) {
            // Nothing we can do
        } finally {
            closeSilently(input)
            closeSilently(output)
        }
        return null
    }
    @Throws(IOException::class)
    private fun getTempFilename(context: Context): String {
        val outputDir = context.cacheDir
        val outputFile = File.createTempFile("image", "tmp", outputDir)
        return outputFile.absolutePath
    }

    fun closeSilently(c: Closeable?) {
        if (c == null) return
        try {
            c.close()
        } catch (t: Throwable) {
            // Do nothing
        }
    }

    fun decodeImageFromFiles(path: String?, width: Int, height: Int): Bitmap {
        val scaleOptions = BitmapFactory.Options()
        scaleOptions.inJustDecodeBounds = true
        BitmapFactory.decodeFile(path, scaleOptions)
        var scale = 1
        while (scaleOptions.outWidth / scale / 2 >= width
                && scaleOptions.outHeight / scale / 2 >= height
        ) {
            scale *= 2
        }
        // decode with the sample size
        val outOptions = BitmapFactory.Options()
        outOptions.inSampleSize = scale
        val bitmap: Bitmap = BitmapFactory.decodeFile(path, outOptions)
        Utils.showLog(
                UploadActivity1.TAG,
                "===BitmapFactory===width" + bitmap.width + "===height" + bitmap.height + "==="
        )

        return bitmap
    }

    fun checkAndSaveJob(context: Context) {
        ///  bimglist.clear()
        val storeUserData = StoreUserData(context)
        val gson = Gson()

        if (AppConstants.imageList.size >= 1) {

            Log.e("test", "imagelist: " + AppConstants.imageList.size + "===")

            val json: String = storeUserData.getString(Constants.ADDGENERAL)

            Log.e("String", "String data: " + json + "====")

            val adPostGeneralBean = gson.fromJson(json, AdPostGeneralBean::class.java)

            Log.e("test", "listsave data: " + adPostGeneralBean.toString() + "====")
            adPostGeneralBean.post_images?.clear()
            adPostGeneralBean.post_images = AppConstants.imageList
            Log.e("test", "post_images: " + adPostGeneralBean.post_images!!.size + "===")


            Log.e("test", "post_images data: " + adPostGeneralBean.post_images)

            val jsonset = gson.toJson(adPostGeneralBean)

            storeUserData.setString(Constants.ADDGENERAL, jsonset)


            ////   AdPostImages.changeScreentoLocation(context)

        } else {
            Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
        }
    }

    private fun checkAndSaveBikes(context: Context) {
        //AdPostImages.bimglist.clear()

        val storeUserData = StoreUserData(context)
        val gson = Gson()

        if (AppConstants.imageList.size >= 1) {

            Log.e("test", "imagelist: " + AppConstants.imageList.size + "===")

            val json: String = storeUserData.getString(Constants.ADDBIKES)

            Log.e("String", "String data: " + json + "====")

            val adPostBikesBean = gson.fromJson(json, AdPostBikesBean::class.java)

            Log.e("test", "listsave data: " + adPostBikesBean.toString())
            adPostBikesBean.post_images?.clear()
            adPostBikesBean.post_images = AppConstants.imageList
            Log.e("test", "post_images: " + adPostBikesBean.post_images!!.size + "===")


            Log.e("test", "post_images data: " + adPostBikesBean.post_images)

            val jsonset = gson.toJson(adPostBikesBean)

            storeUserData.setString(Constants.ADDBIKES, jsonset)

        }
        else{
            Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
        }
    }
}