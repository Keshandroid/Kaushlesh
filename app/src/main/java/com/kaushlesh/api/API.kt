package com.kaushlesh.api

class API {

    companion object {

        //Live
        private val BASE_URL = "https://gujaratliving.com/api/"

        //staging
//        private val BASE_URL = "https://beta.gujaratliving.com/api/"

        val SOCIAL_LOGIN = BASE_URL + "registerWithSocialMedia"
        val REGISTER_PHONE_URL = BASE_URL + "register"
        val OTP_VERIFICATION = BASE_URL + "otpVerification"
        val EDIT_PHONE_NUMBER = BASE_URL + "editPhone"
        val EDIT_PHONE_NUMBER_OTP_VERIFICATION = BASE_URL + "phoneOTPVerification"
        val RESEND_OTP = BASE_URL + "resendOTP"
        val USER_PROFILE = BASE_URL + "getProfile"
        val EDIT_PROFILE = BASE_URL + "editProfile"
        val GET_CATEGORY = BASE_URL + "getCategory"
        val GET_SUBCATEGORY = BASE_URL + "getSubcategory"

        val GET_MY_PACKAGES_LIST = BASE_URL + "getMyPackage"
        val GET_EXTRA_AD_PACKAGE = BASE_URL + "getSingleAdPackage"
        val PURCHASE_PACKAGE = BASE_URL + "purchaseUserPostPurchase"

        val GET_POST_DETAILS = BASE_URL + "getPostDetails"
        val GET_LOCATION_LIST = BASE_URL + "getLocation"


        //PROPERTY
        val GET_HOUSE_FOR_SELL = BASE_URL + "getHouseForSell"
        val GET_HOUSE_FOR_RENT = BASE_URL + "getHouseForRent"
        val GET_OFFICE_FOR_SELL = BASE_URL + "getOfficeForSell"
        val GET_OFFICE_FOR_RENT = BASE_URL + "getOfficeForRent"
        val GET_PARTY_PLOATS = BASE_URL + "getPartyPlot"
        val GET_HOUSE_AND_RESORT = BASE_URL + "getHotelAndResort"
        val GET_PLOTES_AND_LAND = BASE_URL + "getPlotesAndLand"
        val GET_WARE_HOUSE = BASE_URL + "getWarehouse"
        val GET_GUEST_HOUSE = BASE_URL + "getGuesthouse"
        val GET_VIEW_ALL_PROPERTY = BASE_URL + "getViewAllPropertyPost"


        val ADD_POST_HOUSE_FOR_SELL = BASE_URL + "addHouseForSell"
        val ADD_POST_HOUSE_FOR_RENT = BASE_URL + "addHouseForRent"
        val ADD_POST_OFFICE_FOR_RENT = BASE_URL + "addOfficeForRent"
        val ADD_POST_OFFICE_FOR_SELL = BASE_URL + "addOfficeForSell"
        val ADD_POST_PLOTS_LAND = BASE_URL + "addPlotesAndLand"
        val ADD_POST_PARTY_PLOTS = BASE_URL + "addPartyPlot"
        val ADD_WARE_HOUSE = BASE_URL + "addWarehouse"
        val ADD_GUEST_HOUSE = BASE_URL + "addGuesthouse"
        val ADD_HOTEL_AND_RESORT = BASE_URL + "addHotelAndResort"

        //MOBILE
        val ADD_POST_MOBILE = BASE_URL + "addMobiles"
        val ADD_POST_TABLET = BASE_URL + "addTabs"
        val ADD_POST_ACCESSORIES = BASE_URL + "addAccesseries"

        val GET_MOBILES = BASE_URL + "getMobiles"
        val GET_TABLETS = BASE_URL + "getTabs"
        val GET_ACCESSORIES = BASE_URL + "getAccesseries"
        val GET_VIEW_ALL_MOBILE = BASE_URL + "getViewAllMobilePost"

        //SHOPS IN CITY
        val ADD_POST_GROCERY_SUPER_MARKET = BASE_URL + "addGroceryShopSupermarket"
        val ADD_POST_CROCARY_PLASTIC = BASE_URL + "addCrocaryPlasticShop"
        val ADD_POST_SUNGLASS_WATCHSHOP = BASE_URL + "addSunglassWatchShop"
        val ADD_POST_PHARMACY_SHOP = BASE_URL + "addPharmacyShop"
        val ADD_POST_JWELLERY_SHOP = BASE_URL + "addJwelleryShop"
        val ADD_POST_HARDWARE_SANITARY = BASE_URL + "addHardwareSanetoryware"
        val ADD_POST_RESTORA_FASTFOOD = BASE_URL + "addRestoreFastFoodshop"
        val ADD_POST_BAKERY_SWEETSHOP = BASE_URL + "addBakerySweetShop"
        val ADD_POST_GIFTS_CARD_SHOP = BASE_URL + "addGiftCardsShop"
        val ADD_POST_ELECTRICAL_SHOP = BASE_URL + "addElectricalShop"
        val ADD_POST_STATIONARY_SHOP = BASE_URL + "addStationaryBooksShop"
        val ADD_POST_OTHER_SHOPS = BASE_URL + "addOtherShops"

        val GET_GROCERY_SUPER_MARKET = BASE_URL + "getGroceryShopSupermarket"
        val GET_CROCARY_PLASTIC = BASE_URL + "getCrocaryPlasticShop"
        val GET_SUNGLASS_WATCHSHOP = BASE_URL + "getSunglassWatchShop"
        val GET_PHARMACY_SHOP = BASE_URL + "getPharmacyShop"
        val GET_JWELLERY_SHOP = BASE_URL + "getJwelleryShop"
        val GET_HARDWARE_SANITARY = BASE_URL + "getHardwareSanetoryware"
        val GET_RESTORA_FASTFOOD = BASE_URL + "getRestoreFastFoodshop"
        val GET_BAKERY_SWEETSHOP = BASE_URL + "getBakerySweetShop"
        val GET_GIFTS_CARD_SHOP = BASE_URL + "getGiftCardsShop"
        val GET_ELECTRICAL_SHOP = BASE_URL + "getElectricalShop"
        val GET_STATIONARY_SHOP = BASE_URL + "getStationaryBooksShop"
        val GET_OTHER_SHOPS = BASE_URL + "getOtherShops"
        val GET_VIEW_ALL_SHOPS_CITY = BASE_URL + "getViewAllShopsInCityPost"

        //VEHICLES
        val ADD_POST_CAR_FOR_SELL = BASE_URL + "addCarforsell"
        val ADD_POST_COMMERCIAL_VEHICLE_FOR_SELL = BASE_URL + "addComercialVehicleforsell"
        val ADD_POST_CAR_DECOR_AUTO_PARTS = BASE_URL + "addCardecorAutoparts"

        val GET_CAR_FOR_SELL = BASE_URL + "getCarforsell"
        val GET_COMMERCIAL_VEHICLE_FOR_SELL = BASE_URL + "getComercialVehicleforsell"
        val GET_CAR_DECOR_AUTO_PARTSP = BASE_URL + "getCardecorAutoparts"
        val GET_VIEW_ALL_VEHICLE = BASE_URL + "getViewAllVehicalPost"

        //BIKES
        val ADD_POST_MOTORCYCLE = BASE_URL + "addMotorcycles"
        val ADD_POST_SCOOTER = BASE_URL + "addScooter"
        val ADD_POST_SPARE_PARTS = BASE_URL + "addSpareparts"
        val ADD_POST_BICYCLE = BASE_URL + "addBycycle"

        val GET_MOTORCYCLE = BASE_URL + "getMotorcycles"
        val GET_SCOOTER = BASE_URL + "getScooter"
        val GET_SPARE_PARTS = BASE_URL + "getSpareparts"
        val GET_BICYCLE = BASE_URL + "getBycycle"
        val GET_VIEW_ALL_BIKES = BASE_URL + "getViewAllBikePost"

        //ELECTRONICS /APPLIANCES

        val ADD_POST_TVS = BASE_URL + "addTvs"
        val ADD_POST_VIDEO_AUDIO = BASE_URL + "addVideoAudio"
        val ADD_POST_COMPUTER_LAPTOP = BASE_URL + "addComputerLaptop"
        val ADD_POST_GAMES_ENTERN = BASE_URL + "addGames"
        val ADD_POST_FRIDGE = BASE_URL + "addFridge"
        val ADD_POST_WASHING_MACHINE = BASE_URL + "addWashingmachine"
        val ADD_POST_ACS = BASE_URL + "addAcs"
        val ADD_POST_CAMERA_LANCES = BASE_URL + "addCameralances"
        val ADD_POST_HARDDISK_PRINTER = BASE_URL + "addHarddisk"
        val ADD_POST_KITCHEN = BASE_URL + "addKitchenAppliances"

        val GET_TVS = BASE_URL + "getTvs"
        val GET_VIDEO_AUDIO = BASE_URL + "getVideoAudio"
        val GET_COMPUTER_LAPTOP = BASE_URL + "getComputerLaptop"
        val GET_GAMES_ENTERN = BASE_URL + "getGames"
        val GET_FRIDGE = BASE_URL + "getFridge"
        val GET_WASHING_MACHINE = BASE_URL + "getWashingmachine"
        val GET_ACS = BASE_URL + "getAcs"
        val GET_CAMERA_LANCES = BASE_URL + "getCameralances"
        val GET_HARDDISK_PRINTER = BASE_URL + "getHarddisk"
        val GET_KITCHEN = BASE_URL + "getKitchenAppliances"
        val GET_VIEW_ALL_ELECTRONICS = BASE_URL + "getViewAllElectronicsPost"

        //FURNITURE
        val ADD_POST_SOFA_DINING = BASE_URL + "addSofadining"
        val ADD_POST_BED_WARDROOMS = BASE_URL + "addWardrooms"
        val ADD_POST_KIDS_FURNITURE = BASE_URL + "addKidsfurniture"
        val ADD_POST_OTHER_ITEMS = BASE_URL + "addOtherHouseHolditems"

        val GET_SOFA_DINING = BASE_URL + "getSofadining"
        val GET_BED_WARDROOMS = BASE_URL + "getWardrooms"
        val GET_KIDS_FURNITURE = BASE_URL + "getKidsfurniture"
        val GET_OTHER_ITEMS = BASE_URL + "getOtherHouseHolditems"
        val GET_VIEW_ALL_FURNITURE = BASE_URL + "getViewAllFurniturePost"

        //FASHION & BEAUTY
        val ADD_POST_MEN = BASE_URL + "addMen"
        val ADD_POST_WOMEN = BASE_URL + "addWomen"
        val ADD_POST_KIDS = BASE_URL + "addKids"

        val GET_MEN = BASE_URL + "getMen"
        val GET_WOMEN = BASE_URL + "getWomen"
        val GET_KIDS = BASE_URL + "getKids"
        val GET_VIEW_ALL_FASHION_BEAUTY = BASE_URL + "getViewAllFashionBeautyPost"

        //ART & LIFE STYLE
        val ADD_POST_PAINTING = BASE_URL + "addPainting"
        val ADD_POST_DESIGNER_CLOCK = BASE_URL + "addDesignerClocks"
        val ADD_POST_POTS_VASES = BASE_URL + "addPotsvases"
        val ADD_POST_DESIGNER_MIRROR = BASE_URL + "addDesignerMirror"
        val ADD_POST_GARDEN_DECOR = BASE_URL + "addGardendecorproducts"
        val ADD_POST_DECORATIVE_LIGHTINGS = BASE_URL + "addDecorativelighting"
        val ADD_POST_OTHER_DECORATIVE_PRODUCTS = BASE_URL + "addOtherdecorativeproducts"

        val GET_PAINTING = BASE_URL + "getPainting"
        val GET_DESIGNER_CLOCK = BASE_URL + "getDesignerClocks"
        val GET_POTS_VASES = BASE_URL + "getPotsvases"
        val GET_DESIGNER_MIRROR = BASE_URL + "getDesignerMirror"
        val GET_GARDEN_DECOR = BASE_URL + "getGardendecorproducts"
        val GET_DECORATIVE_LIGHTINGS = BASE_URL + "getDecorativelighting"
        val GET_OTHER_DECORATIVE_PRODUCTS = BASE_URL + "getOtherdecorativeproducts"
        val GET_VIEW_ALL_ART_LIFESTYLE = BASE_URL + "getViewAllArtLifestylePost"

        //SERVICES
        val ADD_POST_ELECTRONICS_HOME_APPL = BASE_URL + "addServiceHomeAppliances"
        val ADD_POST_RENT_CAR_SERVICE = BASE_URL + "addServiceCar"
        val ADD_POST_RENT_COMMERCIAL_VEHICLE = BASE_URL + "addServiceCommercialVehicle"
        val ADD_POST_HEALTH_BEAUTY = BASE_URL + "addServiceHealth"
        val ADD_POST_MOVERS_PACKERS = BASE_URL + "addServiceMovers"
        val ADD_POST_TRAVEL_AGENCY = BASE_URL + "addServiceTravelAgency"
        val ADD_POST_OTHER_HOMEDELIVERY_PRODUCTS = BASE_URL + "addServiceHomedeliveryofproducts"
        val ADD_POST_EVENT_MANAGEMENT = BASE_URL + "addServiceEventmanagement"
        val ADD_POST_CAR_POOL_SERVICE = BASE_URL + "addServiceCarpool"
        val ADD_POST_FINANCE_CA = BASE_URL + "addServiceFinance"
        val ADD_POST_LEGAL_SERVICE = BASE_URL + "addServiceLegalService"
        val ADD_POST_INTERIOR_CIVIL = BASE_URL + "addServiceInterior"
        val ADD_POST_FOOD_COOKING = BASE_URL + "addServiceCookingService"
        val ADD_POST_TUTIONS_CLASSES = BASE_URL + "addServiceTuition"
        val ADD_POST_OTHER_SERVICES = BASE_URL + "addServiceOther"

        val GET_ELECTRONICS_HOME_APPL = BASE_URL + "getServiceHomeAppliances"
        val GET_RENT_CAR_SERVICE = BASE_URL + "getServiceCar"
        val GET_RENT_COMMERCIAL_VEHICLE = BASE_URL + "getServiceCommercialVehicle"
        val GET_HEALTH_BEAUTY = BASE_URL + "getServiceHealth"
        val GET_MOVERS_PACKERS = BASE_URL + "getServiceMovers"
        val GET_TRAVEL_AGENCY = BASE_URL + "getServiceTravelAgency"
        val GET_OTHER_HOMEDELIVERY_PRODUCTS = BASE_URL + "getServiceHomedeliveryofproducts"
        val GET_EVENT_MANAGEMENT = BASE_URL + "getServiceEventmanagement"
        val GET_CAR_POOL_SERVICE = BASE_URL + "getServiceCarpool"
        val GET_FINANCE_CA = BASE_URL + "getServiceFinance"
        val GET_LEGAL_SERVICE = BASE_URL + "getServiceLegalService"
        val GET_INTERIOR_CIVIL = BASE_URL + "getServiceInterior"
        val GET_FOOD_COOKING = BASE_URL + "getServiceCookingService"
        val GET_TUTIONS_CLASSES = BASE_URL + "getServiceTuition"
        val GET_OTHER_SERVICES = BASE_URL + "getServiceOther"
        val GET_VIEW_ALL_SERVICES = BASE_URL + "getViewAllServicesPost"

        //JOBS
        val ADD_POST_DATA_ENTRY = BASE_URL + "addJobDataEntry"
        val ADD_POST_SALES_MARKETING = BASE_URL + "addJobMarketing"
        val ADD_POST_RENT_OIL_GAS = BASE_URL + "addJobOil"
        val ADD_POST_OPERATOR = BASE_URL + "addJobOperator"
        val ADD_POST_TECHNICIAN = BASE_URL + "addJobTechnician"
        val ADD_POST_TEACHER = BASE_URL + "addJobTeacher"
        val ADD_POST_CA = BASE_URL + "addJobAccountant"
        val ADD_POST_ENGINEER = BASE_URL + "addJobEngineer"
        val ADD_POST_INTERIOR_DESIGNOR = BASE_URL + "addJobInteriorDesigner"
        val ADD_POST_RECEPTIONIST = BASE_URL + "addJobReceptionist"
        val ADD_POST_HOTEL_MANAGEMENT = BASE_URL + "addJobHotelManagement"
        val ADD_POST_COURIER_SERVICE = BASE_URL + "addJobCourierService"
        val ADD_POST_DELIVERY_COLLECTION = BASE_URL + "addJobCollection"
        val ADD_POST_OFFICE_BOY = BASE_URL + "addJobOfficeboy"
        val ADD_POST_DRIVER = BASE_URL + "addJobDriver"
        val ADD_POST_COOK = BASE_URL + "addJobCook"
        val ADD_POST_DOCTOR_NURSE = BASE_URL + "addJobDoctor"
        val ADD_POST_OTHER_JOBS = BASE_URL + "addJobOther"

        val GET_DATA_ENTRY = BASE_URL + "getJobDataEntry"
        val GET_SALES_MARKETING = BASE_URL + "getJobMarketing"
        val GET_RENT_OIL_GAS = BASE_URL + "getJobOil"
        val GET_OPERATOR = BASE_URL + "getJobOperator"
        val GET_TECHNICIAN = BASE_URL + "getJobTechnician"
        val GET_TEACHER = BASE_URL + "getJobTeacher"
        val GET_CA = BASE_URL + "getJobAccountant"
        val GET_ENGINEER = BASE_URL + "getJobEngineer"
        val GET_INTERIOR_DESIGNOR = BASE_URL + "getJobInteriorDesigner"
        val GET_RECEPTIONIST = BASE_URL + "getJobReceptionist"
        val GET_HOTEL_MANAGEMENT = BASE_URL + "getJobHotelManagement"
        val GET_COURIER_SERVICE = BASE_URL + "getJobCourierService"
        val GET_DELIVERY_COLLECTION = BASE_URL + "getJobCollection"
        val GET_OFFICE_BOY = BASE_URL + "getJobOfficeboy"
        val GET_DRIVER = BASE_URL + "getJobDriver"
        val GET_COOK = BASE_URL + "getJobCook"
        val GET_DOCTOR_NURSE = BASE_URL + "getJobDoctor"
        val GET_OTHER_JOBS = BASE_URL + "getJobOther"
        val GET_VIEW_ALL_JOBS = BASE_URL + "getViewAllJobsPost"


        //GRAIN-SEED
        val ADD_POST_SORGHUM = BASE_URL + "addGrainSorghum"
        val ADD_POST_MILLET = BASE_URL + "addGrainMillet"
        val ADD_POST_RICE = BASE_URL + "addGrainRice"
        val ADD_POST_GROUNDNUT = BASE_URL + "addGrainGroundnut"
        val ADD_POST_CORN = BASE_URL + "addGrainCorn"
        val ADD_POST_COTTON = BASE_URL + "addGrainCotton"
        val ADD_POST_CASTOR = BASE_URL + "addGrainCastor"
        val ADD_POST_CUMINSEED = BASE_URL + "addGrainCuminseeds"
        val ADD_POST_TILL = BASE_URL + "addGrainTill"
        val ADD_POST_RAI = BASE_URL + "addGrainRai"
        val ADD_POST_AHI_SEED = BASE_URL + "addGrainAhiseeds"
        val ADD_POST_POTATO = BASE_URL + "addGrainPotato"
        val ADD_POST_ONION = BASE_URL + "addGrainOnion"
        val ADD_POST_OTHERS = BASE_URL + "addGrainOthers"

        val GET_SORGHUM = BASE_URL + "getGrainSorghum"
        val GET_MILLET = BASE_URL + "getGrainMillet"
        val GET_RICE = BASE_URL + "getGrainRice"
        val GET_GROUNDNUT = BASE_URL + "getGrainGroundnut"
        val GET_CORN = BASE_URL + "getGrainCorn"
        val GET_COTTON = BASE_URL + "getGrainCotton"
        val GET_CASTOR = BASE_URL + "getGrainCastor"
        val GET_CUMINSEED = BASE_URL + "getGrainCuminseeds"
        val GET_TILL = BASE_URL + "getGrainTill"
        val GET_RAI = BASE_URL + "getGrainRai"
        val GET_AHI_SEED = BASE_URL + "getGrainAhiseeds"
        val GET_POTATO = BASE_URL + "getGrainPotato"
        val GET_ONION = BASE_URL + "getGrainOnion"
        val GET_OTHERS = BASE_URL + "getGrainOthers"

        //KHEDUT
        val ADD_POST_GRAIN_FOR_SELL = BASE_URL + "addGrainForSell"
        val ADD_POST_FRUIT_FOR_SELL = BASE_URL + "addGrainFruitForSell"
        val ADD_POST_VEGETABLE_FOR_SELL = BASE_URL + "addGrainVegitableForSell"
        val ADD_POST_ANIMAL_FOR_SELL = BASE_URL + "addGrainAnimalForSell"
        val ADD_POST_NURSUERY_PLANT_FOR_SELL = BASE_URL + "addGrainNursaryForSell"
        val ADD_POST_FERTILIZER_SEEDS = BASE_URL + "addGrainFertilizerForSell"
        val ADD_POST_FARMING_MACHINE = BASE_URL + "addGrainFarmingForSell"
        val ADD_POST_TRACTOR_TROLLEY = BASE_URL + "addGrainTrollyForSell"
        val ADD_POST_TRANSPORT_FOR_KHEDUT = BASE_URL + "addGrainTransport"
        val ADD_POST_FARM_FOR_RENT = BASE_URL + "addGrainFarmForRent"

        val GET_GRAIN_FOR_SELL = BASE_URL + "getGrainForSell"
        val GET_FRUIT_FOR_SELL = BASE_URL + "getGrainFruitForSell"
        val GET_VEGETABLE_FOR_SELL = BASE_URL + "getGrainVegitableForSell"
        val GET_ANIMAL_FOR_SELL = BASE_URL + "getGrainAnimalForSell"
        val GET_NURSUERY_PLANT_FOR_SELL = BASE_URL + "getGrainNursaryForSell"
        val GET_FERTILIZER_SEEDS = BASE_URL + "getGrainFertilizerForSell"
        val GET_FARMING_MACHINE = BASE_URL + "getGrainFarmingForSell"
        val GET_TRACTOR_TROLLEY = BASE_URL + "getGrainTrollyForSell"
        val GET_TRANSPORT_FOR_KHEDUT = BASE_URL + "getGrainTransport"
        val GET_FARM_FOR_RENT = BASE_URL + "getGrainFarmForRent"
        val GET_VIEW_ALL_KHEDUT = BASE_URL + "getViewAllGrainsSeedsPost"

        //get data
        val GET_SEARCH_POST_DETAILS = BASE_URL + "getSerchPostDetails"

        val GET_BRAND_LIST = BASE_URL + "getBrand"
        val GET_VEHICLE_TYPE_LIST = BASE_URL + "getVehicalType"
        val GET_VEHICLE_CAR_DECOR_TYPE_LIST = BASE_URL + "getCarDecoreType"
        val GET_KHEDUT_PRODUCT_LIST = BASE_URL + "getSubSubcategory"

        val GET_CATEGORY_PACKAGE_LIST = BASE_URL + "getCategoryPackage"
        val GET_SINGLE_AD_PREMIUM_PACKAGE_LIST = BASE_URL + "getSinglePrimiumAdPackage"

        val ADD_REPORT_POST = BASE_URL + "addReportPost"
        val ADD_LIKE_DISLIKE_POST = BASE_URL + "likeDislikePost"
        val ADD_POST_VIEWER= BASE_URL + "addPostViewer"

        val GET_PACKAGE_ORDERS = BASE_URL + "getMyPackageOrders"
        val GET_MY_POST = BASE_URL + "getMyPost"
        val ADD_DELETE_POST = BASE_URL + "deletePost"
        val GET_MY_FILTER_POST = BASE_URL + "getMyPostFilters"
        val GET_FAV_POST = BASE_URL + "getFavPost"
        val REMOVE_FAV_POST = BASE_URL + "removeFavPost"
        val AD_PREMIUM_POST = BASE_URL + "addPrimiumToPost"
        val EDIT_POST_PRICE = BASE_URL + "editPostPrice"

        val GET_LOGIN_USER_DETAILS = BASE_URL + "getLoginUserData"
        val GET_FOLLOWING_USER_DETAILS = BASE_URL + "getOtherUserData"
        val REMOVE_FROM_FOLLOW = BASE_URL + "removeFollow"
        val ADD_TO_FOLLOW = BASE_URL + "addFollow"
        val GET_FOLLOWERS_LIST = BASE_URL + "getFollowers"
        val GET_FOLLOWING_LIST = BASE_URL + "getFollowing"

        val GET_POST_USER_EXIST_OR_NOT = BASE_URL + "getUserExistData"
        val DELETE_ORDER = BASE_URL + "deleteUserPackage"
        val DELETE_EXPIRED_ORDER = BASE_URL + "bulkDeleteUserPackage"
        val DELETE_MULTIPLE_POST = BASE_URL + "bulkDeletePost"
        val ADD_POST_LIVE_AGAIN = BASE_URL + "repost"
        val GET_FRESH_RECOM_PRODUCT_LIST = BASE_URL + "getHomePost"
        val GET_HOME_PRODUCT_LIST = BASE_URL + "getHomePost"

        val GET_BLOCK_STATUS = BASE_URL + "getUserBlockData"

        val GET_CATEGORY_SUBCATEGORY_IDS = BASE_URL + "getCategorySubcategory"

        //BUSINESS PROFILE
        val ADD_BUSINESS_PROFILE = BASE_URL + "addBussinessProfile"
        val GET_BUSINESS_PROFILE = BASE_URL + "getBussinessProfile"
        val DELETE_BUSINESS_PROFILE = BASE_URL + "deleteBussinessProfile"
        val EDIT_BUSINESS_PROFILE = BASE_URL + "editBussinessProfile"
        val EDIT_BUSINESS_TIMINGS = BASE_URL + "editBussinessTimings"
        val BUSINESS_PROFILE_VIEW_COUNT = BASE_URL + "addBussinessProfileViewCount"
        val REPORT_BUSINESS_PROFILE = BASE_URL + "addReportBussinessProfile"

        //ADVERTISEMENTS
        val ADVERTISEMENT_HOME = BASE_URL + "getHomeBanner"
        val ADVERTISEMENT_POST_DETAIL = BASE_URL + "getHomeBannerPostDetail"
        val SUPPORT_FAQ_BUYERS = BASE_URL + "getStaticPage"

        //Invoice
        val ADD_BILLING_PROFILE = BASE_URL + "editBillingProfile"
        val GET_BILLING_PROFILE = BASE_URL + "getBillingProfile"

        //Tutorial
        val GET_TUTORIAL = BASE_URL + "getTutorial"

        //Notifications
        val GET_NOTIFICATIONS = BASE_URL + "getNotificationList"
        val GET_NOTIFICATION_COUNTER = BASE_URL + "getNotificationCounter"
        val DELETE_NOTIFICATION = BASE_URL + "deleteNotification"
        val DELETE_ALL_NOTIFICATION = BASE_URL + "deleteAllNotification"
        val READ_NOTIFICATION = BASE_URL + "readNotification"

        //chat push notification
        val CHAT_PUSH_NOTIFICATION = BASE_URL + "sendMsgNotification"


        //Maintanance
        val MAINTANANCE_MODE = BASE_URL + "getSettingData"

        //Logout APP
        val LOGOUT_APP = BASE_URL + "logout"


    }
}