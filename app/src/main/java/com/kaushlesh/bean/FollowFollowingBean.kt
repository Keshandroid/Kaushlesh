package com.kaushlesh.bean

import android.util.Log
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable
import java.util.ArrayList

class FollowFollowingBean : Serializable {

    public var list: ArrayList<FollowFollowingBean.FFlist> = ArrayList<FollowFollowingBean.FFlist>()

    @Throws(JSONException::class)
    fun FollowFollowingBean(aJSONObject: JSONObject?) {

        if (aJSONObject != null) {

            var jsonArray: JSONArray? = null
            if (aJSONObject.has("result") && aJSONObject.get("result") is JSONArray) {
                jsonArray = aJSONObject.getJSONArray("result")
            }
            if (jsonArray != null) {
                for (i in 0 until jsonArray.length()) {
                    val dataModel = FFlist(jsonArray.getJSONObject(i))
                    list.add(dataModel)

                }
            }
        }
    }


    class FFlist(user: JSONObject?) : Serializable {
        var userId: Int? = null
        var userName: String? = null
        var userProfile: String? = null
        var isFollowing: Int? = null

        init {

            if (user != null) {
                userId = user.optInt("user_id")
                userName = user.optString("username")
                userProfile = user.optString("user_Image")
                isFollowing = user.optInt("isFollowing")
            }
        }
    }
}