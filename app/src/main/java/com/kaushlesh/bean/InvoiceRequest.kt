package com.kaushlesh.bean

import java.io.Serializable

class InvoiceRequest: Serializable {

    var userId: String? = null
    var userToken: String =""

    var is_gst: Int? = null

    var billing_email: String? = null
    var billing_name: String? = null
    var billing_bussiness_name: String? = null
    var billing_gst_no: String? = null
    var billing_address1: String? = null
    var billing_address2: String? = null
    var billing_state: String? = null
    var billing_city: String? = null
    var billing_district: String? = null


}