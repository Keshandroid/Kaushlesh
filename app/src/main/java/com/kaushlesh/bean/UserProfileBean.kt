package com.kaushlesh.bean

import java.io.Serializable



import org.json.JSONException
import org.json.JSONObject


class UserProfileBean : Serializable {
    var userId: String?=null
    var userToken: String?=null
    var userProfile: String?=null
    var isActive: String?=null
    var isOTPVerified: String?=null
    var device_token: String?=null
    var device_type: String?=null
    var registrationType: String?=null
    var device_id: String?=null
    var fname: String?=""
    var lname: String?=""
    var email: String?=""
    var name: String?=""
    var address_street: String?=""
    var address_apt: String?=""
    var city: String?=""
    var state: String?=""
    var zip: String?=""
    var phone: String?=""
    var isactive: String?=""
    var isBlock: String?=""
    var aboutyou: String?=""
    var isphone: String?=""
    var locationId: String?=""
    var locationName: String?=""
    var bussinessprofile_id: String?=""

    var whatsapp_inquiry_allow: String?=""
    var same_whatsapp_no: String?=""
    var whatsapp_no: String?=""

    @Throws(JSONException::class)
    fun UserProfileBean(aJSONObject: JSONObject?) {
        val user = aJSONObject?.optJSONObject("result")
        if (user != null) {
            userId = user.optString("userid")
            fname = user.optString("firstName")
            lname = user.optString("lastName")
            userProfile = user.optString("ProfilePic")
            email = user.optString("Email")
            zip = user.optString("Zip")
            phone = user.optString("ConatctNo")
            isactive = user.optString("isactive")
            isBlock = user.optString("isBlock")
            aboutyou = user.optString("About")
            isphone = user.optString("isPhone")
            locationId = user.optString("location_id")
            locationName = user.optString("locationName")
            registrationType = user.optString("registration_type")
            bussinessprofile_id = user.optString("bussinessprofile_id")

            whatsapp_inquiry_allow = user.optString("whatsapp_inquiry_allow")
            same_whatsapp_no = user.optString("same_whatsapp_no")
            whatsapp_no = user.optString("whatsapp_no")



        }
    }
}