package com.kaushlesh.bean

import android.util.Log
import org.json.JSONArray
import org.json.JSONObject
import java.io.Serializable
import java.util.ArrayList

class FollowingUserDetailBean {

    var bussinessprofile_id: String? = null
    var userid: String? = null
    var username: String? = null
    var profile: String? = null
    var signupDate: String? = null
    var aboutInfo: String? = null
    var followercount: String? = null
    var followingcount: String? = null
    var isfollowing: String? = null
    public var productlist: ArrayList<FollowingUserDetailBean.GetFPostBean> = ArrayList<FollowingUserDetailBean.GetFPostBean>()

    fun FollowingUserDetailBean(aJSONObject: JSONObject?) {
        val user = aJSONObject?.optJSONObject("result")
        if (user != null) {

            bussinessprofile_id = user.optString("bussinessprofile_id")
            userid = user.optString("userid")
            username = user.optString("username")
            profile = user.optString("profilePicture")
            signupDate = user.optString("signUpDate")
            aboutInfo = user.optString("about")
            followercount = user.optString("followersCount")
            followingcount = user.optString("followingCount")
            isfollowing = user.optString("isFollowing")

            var jsonArray: JSONArray? = null
            if (user.has("userPosts") && user.get("userPosts") is JSONArray) {
                jsonArray = user.getJSONArray("userPosts")
            }
            if (jsonArray != null) {
                for (i in 0 until jsonArray.length()) {
                    val dataModel = FollowingUserDetailBean.GetFPostBean(jsonArray.getJSONObject(i))
                    productlist.add(dataModel)
                }
            }
        }
    }

    class PostImageX(jsonObject: JSONObject?) : Serializable {

        var postImage: String? = null

        init {
            Log.i("KAUSHLESH", "===product detail img list===" + jsonObject!!)

            postImage = jsonObject.optString("postImage")
        }
    }
    class GetFPostBean(user: JSONObject?) : Serializable {
        var post_id: Int? = null
        var title: String? = null
        var category_id: Int? = null
        var sub_category_id: Int? = null
        var location_id: Int? = null
        var post_status: Int? = null
        var published_date: String? = null
        var expire_date: String? = null
        var price: String? = null
        var is_favourite: Int? = null
        var mainPostImg: String?=null
        var address: String? = null
        var postImages: ArrayList<FollowingUserDetailBean.PostImageX> = ArrayList<FollowingUserDetailBean.PostImageX>()

        init {

            if (user != null) {
                post_id = user.optInt("post_id")
                title = user.optString("title")
                category_id = user.optInt("category_id")
                sub_category_id = user.optInt("sub_category_id")
                location_id = user.optInt("location_id")
                post_status = user.optInt("post_status")
                published_date = user.optString("published_date")
                expire_date = user.optString("expire_date")
                price = user.optString("price")
                is_favourite = user.optInt("isFavorite")
                mainPostImg = user.optString("mainPostImage")
                address = user.optString("address")

                var jsonArray: JSONArray? = null
                if (user.has("postImages") && user.get("postImages") is JSONArray) {
                    jsonArray = user.getJSONArray("postImages")
                }
                if (jsonArray != null) {
                    for (i in 0 until jsonArray.length()) {
                        val dataModel = FollowingUserDetailBean.PostImageX(jsonArray.getJSONObject(i))
                        postImages.add(dataModel)

                    }
                }
            }

        }

    }
}