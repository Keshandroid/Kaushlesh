package com.kaushlesh.bean

import android.util.Log
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable

class ExtraAdPackageBean : Serializable {

    var packageId: Int?=null
    var name: String?=null
    var category_id: Int?=null
    var packageValidity: Int?=null
    var packageType: Int?=null
    var duration: String?=null
    var noofad: Int?=null
    var price: Int?=null
    var status: Int?=null
    var freeAdRenewAfter: Int?=null
    var userpostpackageId: Int?=null

    @Throws(JSONException::class)
    fun ExtraAdPackageBean(aJSONObject: JSONObject?) {

        if (aJSONObject != null) {

            val user = aJSONObject.optJSONObject("result")
            if (user != null) {
                packageId = user.optInt("package_id")
                name = user.optString("name")
                category_id = user.optInt("category_id")
                packageValidity = user.optInt("package_validity")
                packageType = user.optInt("package_type")
                duration = user.optString("duration")
                noofad = user.optInt("noOfAd")
                price = user.optInt("price")
                status = user.optInt("status")
                freeAdRenewAfter = user.optInt("freeAdRenewAfter")
                userpostpackageId  = user.optInt("userpostpurchasepackage_id")
            }
        }
    }
}