package com.kaushlesh.bean

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable
import java.util.ArrayList

class MyPackageOrderBean {
    public var myPackageOrderList: ArrayList<MyPackageOrderBean.MyPackageOrderList> =
        ArrayList<MyPackageOrderBean.MyPackageOrderList>()

    @Throws(JSONException::class)
    fun MyPackageOrderBean(aJSONObject: JSONObject?) {
        if (aJSONObject != null) {
            var jsonArray: JSONArray? = null
            if (aJSONObject.has("result") && aJSONObject.get("result") is JSONArray) {
                jsonArray = aJSONObject.getJSONArray("result")
            }
            if (jsonArray != null) {
                for (i in 0 until jsonArray.length()) {
                    val dataModel =
                        MyPackageOrderBean.MyPackageOrderList(jsonArray.getJSONObject(i))
                    myPackageOrderList.add(dataModel)
                }
            }

        }
    }

    class MyPackageOrderList(user: JSONObject?) : Serializable {

        var purchased_order_id: Int? = null
        var package_id: Int? = null
        var avilablePost: Int? = null
        var usedPost: Int? = null
        var order_id: String? = null
        var duration: Int? = null
        var category_id: Int? = null
        var name: String? = null
        var categoryName: String? = null
        var package_validity: String? = null
        var purchasedPost: String? = null
        var price: String? = null
        var activation_date: String? = null
        var expiry_date: String? = null
        var status: Int? = null
        var isSingleAd: Int? = null
        var isPremium: Int? = null
        var isexpired: Int? = null
        var location_id: String? = null
        var locationName: String? = null
        var invoiceLink: String? = null

        init {
            if (user != null) {
                purchased_order_id = user.optInt("userpostpurchasepackage_id")
                package_id = user.optInt("package_id")
                avilablePost = user.optInt("avilablePost")
                usedPost = user.optInt("usedPost")
                order_id = user.optString("order_id")
                duration = user.optInt("duration")
                category_id = user.optInt("category_id")
                name = user.optString("name")
                categoryName = user.optString("categoryName")
                package_validity = user.optString("package_validity")
                purchasedPost = user.optString("purchasedPost")
                price = user.optString("price")
                activation_date = user.optString("activation_date")
                expiry_date = user.optString("expiry_date")
                isSingleAd = user.optInt("isSingle")
                isPremium = user.optInt("is_primium")
                isexpired = user.optInt("isExpired")
                location_id = user.optString("location_id")
                locationName = user.optString("locationName")
                invoiceLink = user.optString("invoiceLink")
            }
        }
    }
}