package com.kaushlesh.bean.Filter

import android.graphics.Bitmap
import com.kaushlesh.api.API
import com.kaushlesh.bean.FilterBean
import java.io.Serializable

class FilterPropertyBean:Serializable{

    var searchWord: String? = ""
    var minPrice: String? = ""
    var maxPrice: String? = ""
    var categoryId: String? = ""
    var subCategoryId: String? = ""
    var latitude: String? = ""
    var longtude: String? = ""
    var AreaMin: String? = ""
    var AreaMax: String?=""
    var minbike_year: String?=""
    var maxbike_year: String?=""
    var minkm_driven: String?=""
    var maxkm_driven: String?=""
    var landAreaMin: String? = ""
    var landAreaMax: String?=""
    var carpetAreaMin: String? = ""
    var carpetAreaMax: String?=""
    var guestAreaMin: String? = ""
    var guestAreaMax: String?=""
    var minStock: String? = ""
    var maxStock: String? = ""
    var property_type_list: ArrayList<String>? = arrayListOf()
    var property_listedby_list: ArrayList<String>? = arrayListOf()
    var property_byFunishing_list: ArrayList<String>? = arrayListOf()
    var property_byBathroom_list: ArrayList<String>? = arrayListOf()
    var property_bybadroom_list: ArrayList<String>? = arrayListOf()
    var property_byPremium_list: ArrayList<String>? = arrayListOf()
    var property_byConstruction_list: ArrayList<String>? = arrayListOf()
    var property_byBachelor_Allow_list: ArrayList<String>? = arrayListOf()
    var property_bypurpose_list: ArrayList<String>? = arrayListOf()
    var mobile_type:  String?=""
    var package_type: ArrayList<String>? = arrayListOf()
    var brand_id: ArrayList<String>? = arrayListOf()
    var fuel: ArrayList<String>? = arrayListOf()


    var type_list: ArrayList<FilterBean>? = arrayListOf()
    var listedby_list: ArrayList<FilterBean>? = arrayListOf()
    var furnishing_list: ArrayList<FilterBean>? = arrayListOf()
    var bathroom_list: ArrayList<FilterBean>? = arrayListOf()
    var badroom_list: ArrayList<FilterBean>? = arrayListOf()
    var premiumad_list: ArrayList<FilterBean>? = arrayListOf()
    var construction_list: ArrayList<FilterBean>? = arrayListOf()
    var bachelors_list: ArrayList<FilterBean>? = arrayListOf()
    var purpose_list: ArrayList<FilterBean>? = arrayListOf()

}
