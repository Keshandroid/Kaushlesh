package com.kaushlesh.bean

import android.util.Log
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable

class CategoryBean:Serializable{

    public var categoryList: ArrayList<Category> = ArrayList<Category>()

    @Throws(JSONException::class)
    fun CategoryBean(aJSONObject: JSONObject?) {

        if (aJSONObject != null) {

            var jsonArray: JSONArray? = null
            if (aJSONObject.has("result") && aJSONObject.get("result") is JSONArray) {
                jsonArray = aJSONObject.getJSONArray("result")
            }
            if (jsonArray != null) {
                for (i in 0 until jsonArray.length()) {
                    val dataModel = Category(jsonArray.getJSONObject(i))
                    categoryList.add(dataModel)

                }
            }

        }
    }

    class Category(jsonObject: JSONObject?) : Serializable {

        var categoryId: Int?=null
        var categoryName: String?=null
        var categoryImage: String?=null

        init {

            if (jsonObject != null) {

                categoryId = jsonObject.optInt("categoryId")
                categoryName = jsonObject.optString("categoryName")
                categoryImage = jsonObject.optString("categoryImage")

            }
        }

    }


}