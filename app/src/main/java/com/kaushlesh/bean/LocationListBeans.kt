package com.kaushlesh.bean

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable
import java.util.ArrayList

class LocationListBeans : Serializable {
    public var locationList: ArrayList<LocationListBeans.LocationList> = ArrayList<LocationListBeans.LocationList>()

    @Throws(JSONException::class)
    fun LocationListBeans(aJSONObject: JSONObject?) {
        if (aJSONObject != null) {
            var jsonArray: JSONArray? = null
            if (aJSONObject.has("result") && aJSONObject.get("result") is JSONArray) {
                jsonArray = aJSONObject.getJSONArray("result")
            }
            if (jsonArray != null) {
                for (i in 0 until jsonArray.length()) {
                    val dataModel = LocationList(jsonArray.getJSONObject(i))
                    locationList.add(dataModel)
                }
            }

        }
    }

    class LocationList(user: JSONObject?) : Serializable {
        var locId: Int? = null
        var locName: String? = null

        init {
            if (user != null) {
                locId = user.optInt("locationId")
                locName = user.optString("locationName")
            }
        }
    }
}