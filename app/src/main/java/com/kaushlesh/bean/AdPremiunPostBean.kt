package com.kaushlesh.bean

import com.kaushlesh.bean.PostDetails.postDetailsBean
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable

class AdPremiunPostBean  : Serializable {

    var pkgid: String? = null
    var name: String? = null
    var catid: String? = null
    var pkgvalidity: String? = null
    var pkgtype: String? = null
    var duration: String? = null
    var noofad: String? = null
    var remainingpost: String? = null
    var price: String? = null
    var expdate: String? = null


    @Throws(JSONException::class)
    fun AdPremiunPostBean(jsonObject: JSONObject) {

      //  val jsonObject = aJSONObject.getJSONObject("result")

        pkgid = jsonObject.optString("package_id")
        name = jsonObject.optString("name")
        catid = jsonObject.optString("category_id")
        pkgvalidity = jsonObject.optString("package_validity")
        pkgtype = jsonObject.optString("package_type")
        duration = jsonObject.optString("duration")
        noofad = jsonObject.optString("noOfAd")
        remainingpost = jsonObject.optString("remainingPost")
        price = jsonObject.optString("price")
        expdate = jsonObject.optString("expDate")
    }
}