package com.kaushlesh.bean

import android.util.Log
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable

class GetCategoryPackagesBean  : Serializable {

    public var packagelist: ArrayList<Packages> = ArrayList<Packages>()


    @Throws(JSONException::class)
    fun GetCategoryPackagesBean(aJSONObject: JSONObject?) {


            if (aJSONObject != null) {

                var jsonArray: JSONArray? = null
                if (aJSONObject.has("result") && aJSONObject.get("result") is JSONArray) {
                    jsonArray = aJSONObject.getJSONArray("result")
                }
                if (jsonArray != null) {
                    for (i in 0 until jsonArray.length()) {
                        val dataModel = Packages(jsonArray.getJSONObject(i))
                        packagelist.add(dataModel)
                    }
                }

            }
    }

    class Packages(jsonObject: JSONObject?) : Serializable {

        var packageid: String? = null
        var name: String? = null
        var packagetype: String? = null
        var duration: String? = null
        var noofAd: String? = null
        var price: String? = null
        var validity: String? = null
        init {

            if (jsonObject != null) {

                packageid = jsonObject.optString("package_id")
                name = jsonObject.optString("name")
                packagetype = jsonObject.optString("package_type")
                duration = jsonObject.optString("duration")
                noofAd = jsonObject.optString("noOfAd")
                price = jsonObject.optString("price")
                validity = jsonObject.optString("package_validity")
            }
        }

    }

}