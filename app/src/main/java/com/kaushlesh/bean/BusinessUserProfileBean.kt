package com.kaushlesh.bean

import com.kaushlesh.utils.CallTiming
import org.json.JSONArray
import java.io.Serializable



import org.json.JSONException
import org.json.JSONObject
import java.util.ArrayList


class BusinessUserProfileBean : Serializable {
    var user_id: String?=""
    var bussinessprofile_id: String?=null
    var rejected_reason: String?=""
    var profileId: String?=null
    var address1: String?=""
    var address2: String?=""
    var bussiness_about: String?=null
    var service_info: String?=null
    var bussiness_name: String?=null
    var city: String?=null
    var email: String?=null
    var is_verified: String?=""
    var location_id: String?=""
    var location_name: String?=""
    var mobile_no: String?=""
    var profile_counter: String?=""

    var followingCount: String?=""
    var followersCount: String?=""

    var profile_created_date: String?=""
    var same_whatsapp_no: String?=""
    var state: String?=""
    var visitingcard_back: String?=""
    var visitingcard_front: String?=""
    var whatsapp_inquiry_allow: String?=""
//    var whatsapp_inquiry_call_timing: String?="" //array
//    var bussiness_call_timing: String?=null //array

    var is_calls_allow: String?=""
    var is_whatsapp_calls_allow: String?=""




    var whatsapp_inquiry_call_timing: ArrayList<CallTiming> = ArrayList<CallTiming>()
    var bussiness_call_timing: ArrayList<CallTiming> = ArrayList<CallTiming>()


    var whatsapp_inquiry_timing_same: String?=""
    var whatsapp_no: String?=""



    @Throws(JSONException::class)
    fun businessUserProfileBean(aJSONObject: JSONObject?) {
        val user = aJSONObject?.optJSONObject("result")
        if (user != null) {
            user_id = user.optString("user_id")
            rejected_reason = user.optString("rejected_reason")
            bussinessprofile_id = user.optString("bussinessprofile_id")
            address1 = user.optString("address1")
            address2 = user.optString("address2")
            bussiness_about = user.optString("bussiness_about")

            service_info = user.optString("service_info")

            bussiness_name = user.optString("bussiness_name")
            city = user.optString("city")
            email = user.optString("email")
            is_verified = user.optString("is_verified")
            location_id = user.optString("location_id")
            location_name = user.optString("location_name")
            mobile_no = user.optString("mobile_no")
            profile_counter = user.optString("profile_counter")

            followingCount = user.optString("followingCount")
            followersCount = user.optString("followersCount")

            is_calls_allow = user.optString("is_calls_allow")
            is_whatsapp_calls_allow = user.optString("is_whatsapp_calls_allow")


            profile_created_date = user.optString("profile_created_date")
            same_whatsapp_no = user.optString("same_whatsapp_no")
            state = user.optString("state")
            visitingcard_back = user.optString("visitingcard_back")
            visitingcard_front = user.optString("visitingcard_front")
            whatsapp_inquiry_allow = user.optString("whatsapp_inquiry_allow")
            whatsapp_inquiry_timing_same = user.optString("whatsapp_inquiry_timing_same")
            whatsapp_no = user.optString("whatsapp_no")
            profileId = user.optString("profileId")

            var jsonArray: JSONArray? = null
            if (user.has("whatsapp_inquiry_call_timing") && user.get("whatsapp_inquiry_call_timing") is JSONArray) {
                jsonArray = user.getJSONArray("whatsapp_inquiry_call_timing")
            }
            if (jsonArray != null) {
                for (i in 0 until jsonArray.length()) {
                    val dataModel = CallTiming()
                    dataModel.openTime = jsonArray.getJSONObject(i).optString("openTime")
                    dataModel.closeTime = jsonArray.getJSONObject(i).optString("closeTime")
                    dataModel.dayId = jsonArray.getJSONObject(i).optInt("dayId")
                    whatsapp_inquiry_call_timing.add(dataModel)
                }
            }


            var timingArray: JSONArray? = null
            if (user.has("bussiness_call_timing") && user.get("bussiness_call_timing") is JSONArray) {
                timingArray = user.getJSONArray("bussiness_call_timing")
            }
            if (timingArray != null) {
                for (i in 0 until timingArray.length()) {
                    val dataModel = CallTiming()
                    dataModel.openTime = timingArray.getJSONObject(i).optString("openTime")
                    dataModel.closeTime = timingArray.getJSONObject(i).optString("closeTime")
                    dataModel.dayId = timingArray.getJSONObject(i).optInt("dayId")
                    bussiness_call_timing.add(dataModel)
                }
            }




        }
    }
}