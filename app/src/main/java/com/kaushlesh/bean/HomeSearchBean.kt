package com.kaushlesh.bean

import android.util.Log
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable

class HomeSearchBean:Serializable{

    public var homeSearchList: ArrayList<HomeSearch> = ArrayList<HomeSearch>()

    @Throws(JSONException::class)
    fun HomeSearchBean(aJSONObject: JSONObject?) {

        if (aJSONObject != null) {

            var jsonArray: JSONArray? = null
            if (aJSONObject.has("result") && aJSONObject.get("result") is JSONArray) {
                jsonArray = aJSONObject.getJSONArray("result")
            }
            if (jsonArray != null) {
                for (i in 0 until jsonArray.length()) {
                    val dataModel = HomeSearch(jsonArray.getJSONObject(i))
                    homeSearchList.add(dataModel)

                }
            }

        }
    }

    public class SubCategoryData(jsonObject: JSONObject?) : Serializable {

        var subcategoryId: Int? = null
        var subcategoryImage: String? = null
        var subcategoryName: String? = null
        var subcategoryNameGujarati: String? = null

        init {

            if (jsonObject != null) {
                subcategoryId = jsonObject.optInt("subcategoryId")
                subcategoryImage = jsonObject.optString("subcategoryImage")
                subcategoryName = jsonObject.optString("subcategoryName")
                subcategoryNameGujarati = jsonObject.optString("subcategoryNameGujarati")
            }

        }
    }

    class HomeSearch(jsonObject: JSONObject?) : Serializable {

        var categoryId: Int?=null
        var categoryName: String?=null
        var categoryImage: String?=null
        var categoryNameGujarati: String?=null
        var subCategory: java.util.ArrayList<HomeSearchBean.SubCategoryData> = java.util.ArrayList<HomeSearchBean.SubCategoryData>()


        init {

            if (jsonObject != null) {

                categoryId = jsonObject.optInt("categoryId")
                categoryName = jsonObject.optString("categoryName")
                categoryImage = jsonObject.optString("categoryImage")
                categoryNameGujarati = jsonObject.optString("categoryNameGujarati")

                var jsonArray: JSONArray? = null
                if (jsonObject.has("subcategory") && jsonObject.get("subcategory") is JSONArray) {
                    jsonArray = jsonObject.getJSONArray("subcategory")
                }
                if (jsonArray != null) {
                    for (i in 0 until jsonArray.length()) {
                        val dataModel = HomeSearchBean.SubCategoryData(jsonArray.getJSONObject(i))
                        subCategory.add(dataModel)

                    }
                }
            }
        }

    }


}