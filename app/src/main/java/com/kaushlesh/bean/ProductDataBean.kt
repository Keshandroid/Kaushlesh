package com.kaushlesh.bean

import java.io.Serializable

class ProductDataBean : Serializable {

    var address: String?=null
    val bachelors_allowed: Int?=null
    val bathrooms: Int?=null
    val bedrooms: Int?=null
    val car_parking: Int?=null
    val carpet_area: Int?=null
    val categoryName: String?=null
    val category_id: Int?=null
    val construction_status: Int?=null
    val expire_date: String?=null
    val facing: Int?=null
    val floor_no: Int?=null
    val furnishing: Int?=null
    val listed_by: Int?=null
    val location_id: Int?=null
    val maintenance: Int?=null
    var other_information: String?=null
    var postImages: ArrayList<PostImage>?=null
    var post_id: Int?=null
    val post_status: Int?=null
    var price: String?=null
    var project_name: String?=null
    var property_detail_id: Int?=null
    val property_type: Int?=null
    val published_date: String?=null
    val sub_categoryName: String?=null
    val sub_category_id: Int?=null
    val super_builtup_area: Int?=null
    var title: String?=null
    val total_floors: Int?=null
}

class PostImage{
    var postImage: String?=null
}

