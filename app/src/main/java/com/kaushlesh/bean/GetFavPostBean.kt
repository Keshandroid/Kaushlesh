package com.kaushlesh.bean

import android.util.Log
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable
import java.util.ArrayList

class GetFavPostBean : Serializable {

    public var getFavPostlist: ArrayList<GetFavPostBean.FavPostlist> =
        ArrayList<GetFavPostBean.FavPostlist>()

    @Throws(JSONException::class)
    fun GetFavPostBean(aJSONObject: JSONObject?) {

        if (aJSONObject != null) {

            var jsonArray: JSONArray? = null
            if (aJSONObject.has("result") && aJSONObject.get("result") is JSONArray) {
                jsonArray = aJSONObject.getJSONArray("result")
            }
            if (jsonArray != null) {
                for (i in 0 until jsonArray.length()) {
                    val dataModel =
                        GetFavPostBean.FavPostlist(jsonArray.getJSONObject(i))
                    getFavPostlist.add(dataModel)

                }
            }
        }
    }

    class PostImageX(jsonObject: JSONObject?) : Serializable {

        var postImage: String? = null

        init {
            //Log.i("KAUSHLESH", "===product detail img list===" + jsonObject!!)

            postImage = jsonObject!!.optString("postImage")
        }
    }
    class FavPostlist(user: JSONObject?) : Serializable {
        var post_id: Int? = null
        var title: String? = null
        var category_id: Int? = null
        var sub_category_id: Int? = null
        var location_id: Int? = null
        var post_status: Int? = null
        var published_date: String? = null
        var expire_date: String? = null
        var price: String? = null
        var mainPostImg: String?=null

        var address: String?=null
        var other_information: String?=null

        var postImages: ArrayList<GetMyPostBean.PostImageX> = ArrayList<GetMyPostBean.PostImageX>()

        init {

            if (user != null) {
                post_id = user.optInt("post_id")
                title = user.optString("title")
                category_id = user.optInt("category_id")
                sub_category_id = user.optInt("sub_category_id")
                location_id = user.optInt("location_id")
                post_status = user.optInt("post_status")
                published_date = user.optString("published_date")
                expire_date = user.optString("expire_date")
                price = user.optString("price")
                mainPostImg = user.optString("mainPostImage")

                address = user.optString("address")
                other_information = user.optString("other_information")

                var jsonArray: JSONArray? = null
                if (user.has("postImages") && user.get("postImages") is JSONArray) {
                    jsonArray = user.getJSONArray("postImages")
                }
                if (jsonArray != null) {
                    for (i in 0 until jsonArray.length()) {
                        val dataModel = GetMyPostBean.PostImageX(jsonArray.getJSONObject(i))
                        postImages.add(dataModel)

                    }
                }
            }

        }

    }
}