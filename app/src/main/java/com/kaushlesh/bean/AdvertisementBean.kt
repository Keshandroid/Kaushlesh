package com.kaushlesh.bean

import android.util.Log
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable

class AdvertisementBean:Serializable{

    public var adsList: ArrayList<Advertisement> = ArrayList<Advertisement>()

    @Throws(JSONException::class)
    fun AdvertisementBean(aJSONObject: JSONObject?) {

        if (aJSONObject != null) {

            var jsonArray: JSONArray? = null
            if (aJSONObject.has("result") && aJSONObject.get("result") is JSONArray) {
                jsonArray = aJSONObject.getJSONArray("result")
            }
            if (jsonArray != null) {
                for (i in 0 until jsonArray.length()) {
                    val dataModel = Advertisement(jsonArray.getJSONObject(i))
                    adsList.add(dataModel)

                }
            }

        }
    }

    class Advertisement(jsonObject: JSONObject?) : Serializable {

        var appbanner_id: Int?=null
        var appbanner_img: String?=null
        var appbanner_link: String?=null

        var appbanner_title: String?=null
        var end_date: String?=null
        var mobile_no: String?=null
        var start_date: String?=null
        var whatsapp_no: String?=null



        init {

            if (jsonObject != null) {

                appbanner_id = jsonObject.optInt("appbanner_id")
                appbanner_img = jsonObject.optString("appbanner_img")
                appbanner_link = jsonObject.optString("appbanner_link")

                appbanner_title = jsonObject.optString("appbanner_title")
                end_date = jsonObject.optString("end_date")
                mobile_no = jsonObject.optString("mobile_no")
                start_date = jsonObject.optString("start_date")
                whatsapp_no = jsonObject.optString("whatsapp_no")


            }
        }

    }


}