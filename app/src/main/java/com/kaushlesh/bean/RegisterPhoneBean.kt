package com.kaushlesh.bean

import android.util.Log
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable
import java.util.ArrayList

class  RegisterPhoneBean : Serializable {
    var ConatctNo: String?=null
    var OTP: String?=null
    var isActive: String?=null
    var isOTPVerified: Int?=null
    var is_block: Int?=null
    var name: String?=""
    var purchasedPackage: ArrayList<purchasedPackage> = ArrayList<purchasedPackage>()
    var registrationType: String?=null
    var userToken: String?=null
    var userid: String?=null
    var isEditNumber:Boolean?=false
    var email: String?=null
    var isRegister: String?=null
    var locationId: String?=null

    @Throws(JSONException::class)
    fun RegisterPhoneBean(user: JSONObject?) {
        if (user != null) {

/*            val user = aJSONObject.optJSONObject("result")
            Log.e("data", "RegisterPhoneBean: "+ aJSONObject)
            Log.e("data", "RegisterPhoneBean: "+ aJSONObject.optJSONObject("result"))*/
            userid = user.optString("userid")
            isOTPVerified = user.optInt("isOTPVerified")
            ConatctNo = user.optString("ConatctNo")
            isActive = user.optString("isActive")
            name = user.optString("name")
            registrationType = user.optString("registrationType")
            OTP = user.optString("OTP")
            is_block = user.optInt("is_block")
            userToken = user.optString("userToken")
            // userToken = user.optString("isEditNumber")
            email = user.optString("email")
            isRegister = user.optString("isRegister")
            locationId = user.optString("location_id")


            var jsonArray: JSONArray? = null
            if (user.has("purchasedPackage") && user.get("purchasedPackage") is JSONArray) {
                jsonArray = user.getJSONArray("purchasedPackage")
            }
            if (jsonArray != null) {
                for (i in 0 until jsonArray.length()) {
                    val dataModel = purchasedPackage(jsonArray.getJSONObject(i))
                    purchasedPackage.add(dataModel)

                    Log.i("KAUSHLESH", "===product img list===" + purchasedPackage)

                }
            }
        }
    }
}

class purchasedPackage(jsonObject: JSONObject?) : Serializable {

    var package_id: String?=null
    var category_id: String?=null
    var noOfAd: String?=null
    var name: String?=null
    var package_validity: String?=null
    var package_type: String?=null
    var duration: String?=null
    var remainingPost: String?=null
    var price: String?=null

    init {
        if (jsonObject != null) {

            package_id = jsonObject.optString("package_id")
            category_id = jsonObject.optString("category_id")
            noOfAd = jsonObject.optString("noOfAd")
            name = jsonObject.optString("name")
            package_validity = jsonObject.optString("package_validity")
            package_type = jsonObject.optString("package_type")
            duration = jsonObject.optString("duration")
            remainingPost = jsonObject.optString("remainingPost")
            price = jsonObject.optString("price")

        }
    }

}

