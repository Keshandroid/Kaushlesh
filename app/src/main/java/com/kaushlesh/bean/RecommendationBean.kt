package com.kaushlesh.bean

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable
import java.util.ArrayList

class RecommendationBean : Serializable {

    var freshproductlist: ArrayList<FreshProductListBean> = ArrayList<FreshProductListBean>()

    @Throws(JSONException::class)
    fun RecommendationBean(aJSONObject: JSONObject?) {

        if (aJSONObject != null) {

            var jsonArray: JSONArray? = null
            if (aJSONObject.has("result") && aJSONObject.get("result") is JSONArray) {
                jsonArray = aJSONObject.getJSONArray("result")
            }
            if (jsonArray != null) {
                for (i in 0 until jsonArray.length()) {
                    val dataModel = FreshProductListBean(jsonArray.getJSONObject(i))
                    freshproductlist.add(dataModel)
                }
            }
        }
    }

    class FreshProductListBean(user: JSONObject?) : Serializable {

        var post_id: Int?=null
        var title: String?=null
        var category_id: Int?=null
        var categoryName: String?=null
        var sub_category_id: Int?=null
        var sub_categoryName: String?=null
        var location_id: Int?=null
        var post_status: Int?=null
        var price: Int?=null
        var address: String?=null
        var latitude: String?=null
        var longitude: String?=null

        var ispremium: Int?=null
        var likecount: Int?=null
        var viewcount: Int?=null
        var isFavorite: Int?=null

        var published_date: String?=null
        var expire_date: String?=null
        var pkg_expire_date: String?=null
        var premiumtag_expire_date: String?=null

        var other_information: String?=null
        var mainPostImage: String?=null

        var postImages: ArrayList<ProductListHouseBean.PostImageX> = ArrayList<ProductListHouseBean.PostImageX>()

        init {

            if (user != null) {

                post_id = user.optInt("post_id")
                title = user.optString("title")
                category_id = user.optInt("category_id")
                categoryName = user.optString("categoryName")
                sub_category_id = user.optInt("sub_category_id")
                sub_categoryName = user.optString("sub_categoryName")
                location_id = user.optInt("location_id")
                post_status = user.optInt("post_status")
                price = user.optInt("price")
                address = user.optString("address")
                latitude = user.optString("latitude")
                longitude = user.optString("longitude")

                ispremium = user.optInt("is_primium")
                likecount = user.optInt("likeCount")
                viewcount = user.optInt("viewCount")
                isFavorite = user.optInt("isFavorite")

                published_date = user.optString("published_date")
                expire_date = user.optString("expire_date")
                pkg_expire_date = user.optString("packageExpDate")
                premiumtag_expire_date = user.optString("primiumTagExpDate")
                mainPostImage = user.optString("mainPostImage")

                other_information = user.optString("other_information")


                var jsonArray: JSONArray? = null
                if (user.has("postImages") && user.get("postImages") is JSONArray) {
                    jsonArray = user.getJSONArray("postImages")
                }
                if (jsonArray != null) {
                    for (i in 0 until jsonArray.length()) {
                        val dataModel = ProductListHouseBean.PostImageX(jsonArray.getJSONObject(i))
                        postImages.add(dataModel)

                    }
                }
            }
        }
    }
}
