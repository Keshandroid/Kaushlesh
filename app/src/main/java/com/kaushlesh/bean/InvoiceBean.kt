package com.kaushlesh.bean

import com.kaushlesh.utils.CallTiming
import org.json.JSONArray
import java.io.Serializable



import org.json.JSONException
import org.json.JSONObject
import java.util.ArrayList


class InvoiceBean : Serializable {
    var userid: String? = null

    var billing_address1: String? = null
    var billing_address2: String? = null

    var billing_bussiness_name: String? = null
    var billing_city: String? = null
    var billing_district: String? = null
    var billing_email: String? = null
    var billing_gst_no: String? = null
    var billing_name: String? = null
    var billing_state: String? = null
    var is_gst: String? = null
    var locationName: String? = null

    @Throws(JSONException::class)
    fun invoiceBean(aJSONObject: JSONObject?) {
        val user = aJSONObject?.optJSONObject("result")
        if (user != null) {
            userid = user.optString("userid")

            billing_address1 = user.optString("billing_address1")
            billing_address2 = user.optString("billing_address2")
            billing_bussiness_name = user.optString("billing_bussiness_name")
            billing_city = user.optString("billing_city")
            billing_district = user.optString("billing_district")
            billing_email = user.optString("billing_email")
            billing_gst_no = user.optString("billing_gst_no")
            billing_name = user.optString("billing_name")
            billing_state = user.optString("billing_state")

            is_gst = user.optString("is_gst")
            locationName = user.optString("locationName")







        }
    }
}