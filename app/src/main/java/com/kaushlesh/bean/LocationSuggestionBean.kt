package com.kaushlesh.bean

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable

class LocationSuggestionBean : Serializable {

    public var locationsList: ArrayList<LocationSuggestionBean.Lacation> = ArrayList<LocationSuggestionBean.Lacation>()

    @Throws(JSONException::class)
    fun LocationSuggestionBean(aJSONObject: JSONObject?) {

        if (aJSONObject != null) {

            var jsonArray: JSONArray? = null
            if (aJSONObject.has("predictions") && aJSONObject.get("predictions") is JSONArray) {
                jsonArray = aJSONObject.getJSONArray("predictions")
            }
            if (jsonArray != null) {
                for (i in 0 until jsonArray.length()) {
                    val dataModel = LocationSuggestionBean.Lacation(jsonArray.getJSONObject(i))
                    locationsList.add(dataModel)

                }
            }

        }
    }

    class Lacation(jsonObject: JSONObject?) : Serializable {

        var locationName: String? = null

        init {

            if (jsonObject != null) {
                locationName = jsonObject.optString("description")
            }
        }
    }
}