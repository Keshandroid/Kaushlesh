package com.kaushlesh.bean

import android.util.Log
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable

class GetMyPackagesBean  : Serializable {

    public var packagelist: ArrayList<Packages> = ArrayList<Packages>()
    var freepkglist: ArrayList<Packages> = ArrayList<Packages>()
    var premiumpkglist: ArrayList<Packages> = ArrayList<Packages>()

    @Throws(JSONException::class)
    fun GetMyPackagesBean(aJSONObject: JSONObject?) {

        if (aJSONObject != null) {

            val user = aJSONObject.optJSONObject("result")
            if (user != null) {

                var jsonArray: JSONArray? = null
                if (user.has("freePackage") && user.get("freePackage") is JSONArray) {
                    jsonArray = user.getJSONArray("freePackage")
                }
                if (jsonArray != null) {
                    for (i in 0 until jsonArray.length()) {
                        val dataModel = Packages(jsonArray.getJSONObject(i))
                        freepkglist.add(dataModel)

                    }
                }

                var jsonArray1: JSONArray? = null
                if (user.has("premiumPackage") && user.get("premiumPackage") is JSONArray) {
                    jsonArray1= user.getJSONArray("premiumPackage")
                }
                if (jsonArray1 != null) {
                    for (i in 0 until jsonArray1.length()) {
                        val dataModel = Packages(jsonArray1.getJSONObject(i))
                        premiumpkglist.add(dataModel)

                    }
                }
            }

        }
    }

    class Packages(jsonObject: JSONObject?) : Serializable {

        var packageid: String? = null
        var name: String? = null
        var categoryid: String? = null
        var pkgvalidity: String? = null
        var packagetype: String? = null
        var duration: String? = null
        var noofAd: String? = null
        var remaiingpost: String? = null
        var price: String? = null
        var status: String? = null
        var selected:Boolean? = false
        var expdate: String? = null
        var userpostpackageid: String? = null

        init {

            if (jsonObject != null) {

                packageid = jsonObject.optString("package_id")
                name = jsonObject.optString("name")
                categoryid = jsonObject.optString("category_id")
                pkgvalidity = jsonObject.optString("package_validity")
                packagetype = jsonObject.optString("package_type")
                duration = jsonObject.optString("duration")
                noofAd = jsonObject.optString("noOfAd")
                remaiingpost = jsonObject.optString("remainingPost")
                price = jsonObject.optString("price")
                status = jsonObject.optString("status")
                expdate = jsonObject.optString("expDate")
                userpostpackageid = jsonObject.optString("userpostpurchasepackage_id")
            }
        }

    }

}