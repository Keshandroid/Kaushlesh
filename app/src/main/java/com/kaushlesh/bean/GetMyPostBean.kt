package com.kaushlesh.bean

import android.util.Log
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable
import java.util.ArrayList

class GetMyPostBean : Serializable {
    public var getMyPostBeanlist: ArrayList<GetMyPostBean.getMyPostList> = ArrayList<GetMyPostBean.getMyPostList>()

    @Throws(JSONException::class)
    fun GetMyPostBean(aJSONObject: JSONObject?) {

        if (aJSONObject != null) {

            var jsonArray: JSONArray? = null
            if (aJSONObject.has("result") && aJSONObject.get("result") is JSONArray) {
                jsonArray = aJSONObject.getJSONArray("result")
            }
            if (jsonArray != null) {
                for (i in 0 until jsonArray.length()) {
                    val dataModel =
                        GetMyPostBean.getMyPostList(jsonArray.getJSONObject(i))
                    getMyPostBeanlist.add(dataModel)

                }
            }
        }
    }


    class PostImageX(jsonObject: JSONObject?) : Serializable {

        var postImage: String? = null

        init {
            //Log.i("KAUSHLESH", "===product detail img list===" + jsonObject!!)

            postImage = jsonObject!!.optString("postImage")
        }
    }
    class PostDetails(jsonObject: JSONObject?) : Serializable {

        var property_detail_id: Int? = null
        var property_type: Int? = null
        var bedrooms: Int? = null
        var bathrooms: Int? = null
        var furnishing: Int? = null
        var construction_status: Int? = null
        var listed_by: Int? = null
        var super_builtup_area: Int? = null
        var carpet_area: Int? = null
        var bachelors_allowed: Int? = null
        var maintenance: Int? = null
        var total_floors: Int? = null
        var floor_no: Int? = null
        var car_parking: Int? = null
        var project_name: String? = null
        var other_information: String? = null

        init {

            if (jsonObject != null) {
                property_detail_id = jsonObject.optInt("property_detail_id")
                property_type = jsonObject.optInt("property_type")
                bedrooms = jsonObject.optInt("bedrooms")
                bathrooms = jsonObject.optInt("bathrooms")
                furnishing = jsonObject.optInt("furnishing")
                construction_status = jsonObject.optInt("construction_status")
                listed_by = jsonObject.optInt("listed_by")
                super_builtup_area = jsonObject.optInt("super_builtup_area")
                carpet_area = jsonObject.optInt("carpet_area")
                bachelors_allowed = jsonObject.optInt("bachelors_allowed")
                maintenance = jsonObject.optInt("maintenance")
                total_floors = jsonObject.optInt("total_floors")
                floor_no = jsonObject.optInt("floor_no")
                car_parking = jsonObject.optInt("car_parking")
                project_name = jsonObject.optString("project_name")
                other_information = jsonObject.optString("other_information")
            }
        }
    }

    class getMyPostList(user: JSONObject?) : Serializable {
        var post_id: Int? = null
        var title: String? = null
        var category_id: Int? = null
        var categoryName: String? = null
        var sub_category_id: Int? = null
        var sub_categoryName: String?=null
        var location_id: Int?=null
        var location_name: String?=null
        var post_status: Int?=null
        var price: Int?=null
        var isPrimium: Int?=null
        var likeCount: Int?=null
        var viewCount: Int?=null
        var published_date: String?=null
        var expire_date: String?=null
        var pkg_expire_date: String?=null
        var premium_expire_date: String?=null
        var address: String?=null
        var latitude: String?=null
        var longitude: String?=null
        var mainPostImg: String?=null
        var postImages: ArrayList<GetMyPostBean.PostImageX> = ArrayList<GetMyPostBean.PostImageX>()
        var PostDetails: ArrayList<GetMyPostBean.PostDetails> = ArrayList<GetMyPostBean.PostDetails>()
        init {
            if (user != null) {
                post_id = user.optInt("post_id")
                title = user.optString("title")
                category_id = user.optInt("category_id")
                categoryName = user.optString("categoryName")
                sub_category_id = user.optInt("sub_category_id")
                sub_categoryName = user.optString("sub_categoryName")
                location_id = user.optInt("location_id")
                location_name = user.optString("locationName")
                post_status = user.optInt("post_status")
                price = user.optInt("price")
                isPrimium = user.optInt("isPrimium")
                likeCount = user.optInt("likeCount")
                viewCount = user.optInt("viewCount")
                published_date = user.optString("published_date")
                expire_date = user.optString("expire_date")
                address = user.optString("address")
                latitude = user.optString("latitude")
                longitude = user.optString("longitude")
                pkg_expire_date = user.optString("packageExpDate")
                premium_expire_date = user.optString("primiumTagExpDate")
                mainPostImg = user.optString("mainPostImage")


                var jsonArray: JSONArray? = null
                if (user.has("postImages") && user.get("postImages") is JSONArray) {
                    jsonArray = user.getJSONArray("postImages")
                }
                if (jsonArray != null) {
                    for (i in 0 until jsonArray.length()) {
                        val dataModel = GetMyPostBean.PostImageX(jsonArray.getJSONObject(i))
                        postImages.add(dataModel)

                    }
                }

                if (user.has("postDetails") && user.get("postDetails") is JSONArray) {
                    jsonArray = user.getJSONArray("postDetails")
                }
                if (jsonArray != null) {
                    for (i in 0 until jsonArray.length()) {
                        val dataModel = GetMyPostBean.PostDetails(jsonArray.getJSONObject(i))
                        PostDetails.add(dataModel)

                    }
                }
            }
        }
    }
}