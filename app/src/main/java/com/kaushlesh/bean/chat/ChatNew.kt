package com.kaushlesh.bean.chat



class ChatNew(
    val date: String,
    val id: String,
    val name: String,
    val text: String,
    val time: String,
    val timestamp : String
)
{
    constructor() : this("", "", "", "", "","")
}
