package com.kaushlesh.bean.chat



class ChatNewList(
    val proId: String,
    val chattype: String,
    val counter: String,
    val date: String,
    val id: String,
    val image: String,
    val name: String,
    val text: String,
    val time: String,
    val subCatName: String,
    val userProfile: String,
    val timestamp: String,
    val subCategoryID: String,
    val categoryID: String,
    val adTitle : String,
    val price : String,
    val deleteDate : String
)
{
    constructor() : this("", "", "", "", "","","","","","","","","","","","","")
}

