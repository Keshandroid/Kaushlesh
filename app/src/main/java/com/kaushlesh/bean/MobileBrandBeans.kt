package com.kaushlesh.bean

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable
import java.util.ArrayList

class MobileBrandBeans : Serializable {
    public var mobileBrandList: ArrayList<MobileBrandBeans.MobileBrandBeansList> = ArrayList<MobileBrandBeans.MobileBrandBeansList>()

    @Throws(JSONException::class)
    fun MobileBrandBeans(aJSONObject: JSONObject?) {
        if (aJSONObject != null) {
            var jsonArray: JSONArray? = null
            if (aJSONObject.has("result") && aJSONObject.get("result") is JSONArray) {
                jsonArray = aJSONObject.getJSONArray("result")
            }
            if (jsonArray != null) {
                for (i in 0 until jsonArray.length()) {
                    val dataModel = MobileBrandBeansList(jsonArray.getJSONObject(i))
                    mobileBrandList.add(dataModel)
                }
            }

        }
    }

    class MobileBrandBeansList(user: JSONObject?) : Serializable {
        var brandId: Int? = null
        var brandImage: String? = null
        var isPopular: Int? = null
        var brandName: String? = null
        var selected: Boolean? = false
        init {
            if (user != null) {
                brandId = user.optInt("brandId")
                isPopular = user.optInt("isPopular")
                brandImage = user.optString("brandImage")
                brandName = user.optString("brandName")
                selected=false
            }
        }
    }
}