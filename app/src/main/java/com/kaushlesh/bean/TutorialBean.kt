package com.kaushlesh.bean

import com.kaushlesh.utils.CallTiming
import org.json.JSONArray
import java.io.Serializable



import org.json.JSONException
import org.json.JSONObject
import java.util.ArrayList


class TutorialBean : Serializable {

    public var tutorialList: ArrayList<Tutorial> = ArrayList<Tutorial>()


    @Throws(JSONException::class)
    fun TutorialBean(aJSONObject: JSONObject?) {

        if (aJSONObject != null) {

            var jsonArray: JSONArray? = null
            if (aJSONObject.has("result") && aJSONObject.get("result") is JSONArray) {
                jsonArray = aJSONObject.getJSONArray("result")
            }
            if (jsonArray != null) {
                for (i in 0 until jsonArray.length()) {
                    val dataModel = Tutorial(jsonArray.getJSONObject(i))
                    tutorialList.add(dataModel)

                }
            }

        }
    }

    class Tutorial(jsonObject: JSONObject?) : Serializable {

        var tutorial_id: Int?=null
        var tutorialImage: String?=null
        var tutorial_title: String?=null

        init {
            if (jsonObject != null) {

                tutorial_id = jsonObject.optInt("tutorial_id")
                tutorialImage = jsonObject.optString("tutorialImage")
                tutorial_title = jsonObject.optString("tutorial_title")
            }
        }


    }

}