package com.kaushlesh.bean.PostDetails

import android.util.Log
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable

class GrainSeedPostDetailsBean : Serializable {

    var ad_id: String?=null
    var post_id: Int?=null
    var title: String?=null
    var category_id: Int?=null
    var categoryName: String?=null
    var sub_category_id: Int?=null
    var sub_categoryName: String?=null
    var location_id: Int?=null
    var location_name: String?=null
    var post_status: Int?=null
    var price: Int?=null
    var address: String?=null
    var latitude: String?=null
    var longitude: String?=null
    var published_date: String?=null
    var expire_date: String?=null
    var grainseed_detail_id: Int?=null
    var available_stock: Int?=null
    var other_information: String?=null
    var listed_by: Int?=null

    var userId: Int?=null
    var userName: String?=null
    var contactNo: String?=null
    var email: String?=null
    var city: String?=null
    var state: String?=null
    var profilePicture: String?=null
    var signUpDate: String?=null
    var isFavourite: Int?=null
    var isphone: String?=null
    var aboutInfo: String?=null

    //whatsapp detail
    var whatsapp_inquiry_allow: Int? = null
    var same_whatsapp_no: Int? = null
    var whatsapp_no: String? = null
    var bussinessprofile_verified_status: Int? = null

    var cuserName: String?=null
    var cuserProfile: String?=null
    var postImages: ArrayList<postDetailsBean.PostImageX> = ArrayList<postDetailsBean.PostImageX>()
    var relatedArray: ArrayList<postDetailsBean.RelatedArray> = ArrayList<postDetailsBean.RelatedArray>()

    @Throws(JSONException::class)
    fun GrainSeedPostDetailsBean(aJSONObject: JSONObject?) {

        if (aJSONObject != null) {

            val user = aJSONObject.optJSONObject("result")
            if (user != null) {

                cuserName = user.optString("currentUserName")
                cuserProfile = user.optString("currentUserImg")

                ad_id = user.optString("ad_id")
                post_id = user.optInt("post_id")
                title = user.optString("title")
                category_id = user.optInt("category_id")
                categoryName = user.optString("categoryName")
                sub_category_id = user.optInt("sub_category_id")
                sub_categoryName = user.optString("sub_categoryName")
                location_id = user.optInt("location_id")
                location_name = user.optString("locationName")
                post_status = user.optInt("post_status")
                price = user.optInt("price")
                address = user.optString("address")
                latitude = user.optString("latitude")
                longitude = user.optString("longitude")
                published_date = user.optString("published_date")
                expire_date = user.optString("expire_date")
                other_information = user.optString("other_information")
                grainseed_detail_id = user.optInt("grainseed_detail_id")
                available_stock = user.optInt("available_stock")
                isFavourite = user.optInt("isFavorite")
                listed_by = user.optInt("listed_by")
                val userdetails = user.optJSONObject("userDetails")
                if(userdetails != null) {
                    userId = userdetails.optInt("userId")
                    userName = userdetails.optString("userName")
                    contactNo = userdetails.optString("contactNo")
                    email = userdetails.optString("email")
                    city = userdetails.optString("city")
                    state = userdetails.optString("state")
                    profilePicture = userdetails.optString("profilePicture")
                    signUpDate = userdetails.optString("signUpDate")
                    isphone = userdetails.optString("isPhone")
                    aboutInfo = userdetails.optString("about")

                    //whatsapp detail
                    whatsapp_inquiry_allow = userdetails.optInt("whatsapp_inquiry_allow")
                    same_whatsapp_no = userdetails.optInt("same_whatsapp_no")
                    whatsapp_no = userdetails.optString("whatsapp_no")
                    bussinessprofile_verified_status = userdetails.optInt("bussinessprofile_verified_status")


                }

                var jsonArray: JSONArray? = null
                if (user.has("postImages") && user.get("postImages") is JSONArray) {
                    jsonArray = user.getJSONArray("postImages")
                }
                if (jsonArray != null) {
                    for (i in 0 until jsonArray.length()) {
                        val dataModel = postDetailsBean.PostImageX(
                            jsonArray.getJSONObject(i)
                        )
                        postImages.add(dataModel)

                    }
                }

                var jsonArray_udetails: JSONArray? = null
                if (user.has("relatedArray") && user.get("relatedArray") is JSONArray) {
                    jsonArray_udetails = user.getJSONArray("relatedArray")
                }
                if (jsonArray_udetails != null) {
                    for (i in 0 until jsonArray_udetails.length()) {
                        val dataModel = postDetailsBean.RelatedArray(
                            jsonArray_udetails.getJSONObject(i)
                        )
                        relatedArray.add(dataModel)

                    }
                }
            }
        }
    }

}