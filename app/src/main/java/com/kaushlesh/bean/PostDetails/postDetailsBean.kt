package com.kaushlesh.bean.PostDetails

import android.util.Log
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable

class postDetailsBean : Serializable {

    var ad_id: String?=null
    var post_id: Int?=null
    var title: String?=null
    var category_id: Int?=null
    var categoryName: String?=null
    var sub_category_id: Int?=null
    var sub_categoryName: String?=null
    var location_id: Int?=null
    var post_status: Int?=null
    var price: Int?=null
    var address: String?=null
    var latitude: String?=null
    var longitude: String?=null
    var published_date: String?=null
    var expire_date: String?=null
    var property_detail_id: Int?=null
    var property_type: String?=null
    var bedrooms: Int?=null
    var bathrooms: Int?=null
    var furnishing: Int?=null
    var construction_status: Int?=null
    var listed_by: String?=null
    var super_builtup_area: Int?=null
    var carpet_area: Int?=null
    var bachelors_allowed: Int?=null
    var maintenance: Int?=null
    var total_floors: Int?=null
    var floor_no: Int?=null
    var car_parking: Int?=null
    var facing: Int?=null
    var project_name: String?=null
    var other_information: String?=null
    var wash_room: Any?=null
    var purpose: Any?=null
    var plot_area: Any?=null
    var length: Any?=null
    var width: Any?=null
    var party_plot_name: Any?=null
    var guest_capacity: Any?=null
    var guest_room_available: Any?=null
    var no_of_guest_room: Any?=null
    var kitchen_available: Any?=null
    var wash_room_available: Any?=null
    var property_name: Any?=null
    var total_rooms: Any?=null
    var type_of_rooms: Any?=null
    var hotel_service_facility: Any?=null
    var meals_included: Any?=null
    var ac: Any?=null

    var userId: Int?=null
    var userName: String?=null
    var contactNo: String?=null
    var email: String?=null
    var city: String?=null
    var state: String?=null
    var profilePicture: String?=null
    var signUpDate: String?=null
    var isFavourite: Int?=null
    var postImages: ArrayList<PostImageX> = ArrayList<PostImageX>()
    var relatedArray: ArrayList<RelatedArray> = ArrayList<RelatedArray>()
    var isphone: String?=null
    var aboutInfo: String?=null

    //whatsapp detail
    var whatsapp_inquiry_allow: Int? = null
    var same_whatsapp_no: Int? = null
    var whatsapp_no: String? = null
    var bussinessprofile_verified_status: Int? = null

    var cuserName: String?=null
    var cuserProfile: String?=null

    @Throws(JSONException::class)
    fun postDetailsBean(aJSONObject: JSONObject?) {

        if (aJSONObject != null) {

            val user = aJSONObject.optJSONObject("result")
            if (user != null) {

                cuserName = user.optString("currentUserName")
                cuserProfile = user.optString("currentUserImg")

                ad_id = user.optString("ad_id")
                post_id = user.optInt("post_id")
                title = user.optString("title")
                category_id = user.optInt("category_id")
                categoryName = user.optString("categoryName")
                sub_category_id = user.optInt("sub_category_id")
                sub_categoryName = user.optString("sub_categoryName")
                location_id = user.optInt("location_id")
                post_status = user.optInt("post_status")
                price = user.optInt("price")
                address = user.optString("address")
                latitude = user.optString("latitude")
                longitude = user.optString("longitude")
                published_date = user.optString("published_date")
                expire_date = user.optString("expire_date")
                property_detail_id = user.optInt("property_detail_id")
                property_type = user.optString("property_type")
                bedrooms = user.optInt("bedrooms")
                bathrooms = user.optInt("bathrooms")
                furnishing = user.optInt("furnishing")
                construction_status = user.optInt("construction_status")
                listed_by = user.optString("listed_by")
                super_builtup_area = user.optInt("super_builtup_area")
                carpet_area = user.optInt("carpet_area")
                bachelors_allowed = user.optInt("bachelors_allowed")
                maintenance = user.optInt("maintenance")
                total_floors = user.optInt("total_floors")
                floor_no = user.optInt("floor_no")
                car_parking = user.optInt("car_parking")
                facing = user.optInt("facing")
                project_name = user.optString("project_name")
                other_information = user.optString("other_information")
                wash_room = user.optString("wash_room")
                purpose = user.optString("purpose")
                plot_area = user.optString("plot_area")
                length = user.optString("length")
                width = user.optString("width")
                party_plot_name = user.optString("party_plot_name")
                guest_capacity = user.optString("guest_capacity")
                guest_room_available = user.optString("guest_room_available")
                no_of_guest_room = user.optString("no_of_guest_room")
                kitchen_available = user.optString("kitchen_available")
                wash_room_available = user.optString("wash_room_available")
                property_name = user.optString("property_name")
                total_rooms = user.optString("total_rooms")
                type_of_rooms = user.optString("type_of_rooms")
                hotel_service_facility = user.optString("hotel_service_facility")
                meals_included = user.optString("meals_included")
                ac = user.optString("ac")
                isFavourite = user.optInt("isFavorite")

                val userdetails = user.optJSONObject("userDetails")
                if(userdetails != null) {
                    userId = userdetails.optInt("userId")
                    userName = userdetails.optString("userName")
                    contactNo = userdetails.optString("contactNo")
                    email = userdetails.optString("email")
                    city = userdetails.optString("city")
                    state = userdetails.optString("state")
                    profilePicture = userdetails.optString("profilePicture")
                    signUpDate = userdetails.optString("signUpDate")
                    isphone = userdetails.optString("isPhone")
                    aboutInfo = userdetails.optString("about")

                    //whatsapp detail
                    whatsapp_inquiry_allow = userdetails.optInt("whatsapp_inquiry_allow")
                    same_whatsapp_no = userdetails.optInt("same_whatsapp_no")
                    whatsapp_no = userdetails.optString("whatsapp_no")
                    bussinessprofile_verified_status = userdetails.optInt("bussinessprofile_verified_status")


                }

                var jsonArray: JSONArray? = null
                if (user.has("postImages") && user.get("postImages") is JSONArray) {
                    jsonArray = user.getJSONArray("postImages")
                }
                if (jsonArray != null) {
                    for (i in 0 until jsonArray.length()) {
                        val dataModel = PostImageX(
                            jsonArray.getJSONObject(i)
                        )
                        postImages.add(dataModel)

                    }
                }

                var jsonArray_udetails: JSONArray? = null
                if (user.has("relatedArray") && user.get("relatedArray") is JSONArray) {
                    jsonArray_udetails = user.getJSONArray("relatedArray")
                }
                if (jsonArray_udetails != null) {
                    for (i in 0 until jsonArray_udetails.length()) {
                        val dataModel = RelatedArray(
                            jsonArray_udetails.getJSONObject(i)
                        )
                        relatedArray.add(dataModel)

                    }
                }
            }
        }
    }

    class PostImageX(jsonObject: JSONObject?) : Serializable {

        var postImage: String? = null

        init {
            Log.i("KAUSHLESH", "===product detail img list===" + jsonObject!!)

            if (jsonObject != null) {

                postImage = jsonObject.optString("postImage")
            }
        }

    }

    class RelatedArray(jsonObject: JSONObject?) : Serializable {

        var post_id: Int?=null
        var title: String?=null
        var category_id: Int?=null
        var categoryName: String?=null
        var sub_category_id: Int?=null
        var sub_categoryName: String?=null
        var location_id: Int?=null
        var locationName: String?=null
        var post_status: Int?=null
        var published_date: String?=null
        var expire_date: String?=null
        var price: Int?=null
        var address: String?=null
        var postImages: ArrayList<PostImageX> = ArrayList<PostImageX>()


        init {
            //Log.i("KAUSHLESH", "===related array list===" + jsonObject!!)
            if (jsonObject != null) {
                post_id = jsonObject.optInt("post_id")
                title = jsonObject.optString("title")
                category_id = jsonObject.optInt("category_id")
                categoryName = jsonObject.optString("categoryName")
                sub_category_id = jsonObject.optInt("sub_category_id")
                sub_categoryName = jsonObject.optString("sub_categoryName")
                location_id = jsonObject.optInt("location_id")
                locationName = jsonObject.optString("locationName")
                post_status = jsonObject.optInt("post_status")
                published_date = jsonObject.optString("published_date")
                expire_date = jsonObject.optString("expire_date")
                price = jsonObject.optInt("price")
                address = jsonObject.optString("address")

                var jsonArray: JSONArray? = null
                if (jsonObject.has("postImages") && jsonObject.get("postImages") is JSONArray) {
                    jsonArray = jsonObject.getJSONArray("postImages")
                }
                if (jsonArray != null) {
                    for (i in 0 until jsonArray.length()) {
                        val dataModel = PostImageX(
                                jsonArray.getJSONObject(i)
                        )
                        postImages.add(dataModel)

                        //Log.i("KAUSHLESH", "===product img list===" + postImages)

                    }
                }
            }
        }
    }
}