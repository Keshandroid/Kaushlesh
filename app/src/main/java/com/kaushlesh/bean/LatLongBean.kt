package com.kaushlesh.bean

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable

class LatLongBean : Serializable {

    public var latLngList: ArrayList<LatLongBean.LatLong> = ArrayList<LatLongBean.LatLong>()

    @Throws(JSONException::class)
    fun LatLongBean(aJSONObject: JSONObject?) {

        if (aJSONObject != null) {

            var jsonArray: JSONArray? = null
            if (aJSONObject.has("results") && aJSONObject.get("results") is JSONArray) {
                jsonArray = aJSONObject.getJSONArray("results")
            }
            if (jsonArray != null) {
                for (i in 0 until jsonArray.length()) {
                    val dataModel = LatLongBean.LatLong(jsonArray.getJSONObject(i))
                    latLngList.add(dataModel)

                }
            }

        }
    }

    class LatLong(jsonObject: JSONObject?) : Serializable {

        var latitude: String?=null
        var longitude: String?=null

        init {

            if (jsonObject != null) {
                val geometry = jsonObject.optJSONObject("geometry")
                if(geometry !=null)
                {
                    val locations = geometry.optJSONObject("location")
                    if(locations != null)
                    {
                        latitude = locations.optString("lat")
                        longitude = locations.optString("lng")
                    }
                }
            }
        }
    }
}