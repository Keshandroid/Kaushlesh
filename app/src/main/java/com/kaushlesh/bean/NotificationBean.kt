package com.kaushlesh.bean

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable
import java.util.*

class NotificationBean {
    public var notificationList: ArrayList<NotificationBean.NotificationList> =
        ArrayList<NotificationBean.NotificationList>()

    @Throws(JSONException::class)
    fun NotificationBean(aJSONObject: JSONObject?) {
        if (aJSONObject != null) {
            var jsonArray: JSONArray? = null
            if (aJSONObject.has("result") && aJSONObject.get("result") is JSONArray) {
                jsonArray = aJSONObject.getJSONArray("result")
            }
            if (jsonArray != null) {
                for (i in 0 until jsonArray.length()) {
                    val dataModel =
                        NotificationBean.NotificationList(jsonArray.getJSONObject(i))
                    notificationList.add(dataModel)
                }
            }

        }
    }

    class NotificationList(user: JSONObject?) : Serializable {

        var is_read: String? = null
        var message: String? = null
        var message_time: String? = null
        var notification_id: String? = null
        var sender_id: String? = null
        var sender_name: String? = null
        var type: String? = null

        var is_live: String? = null
        var category_id: String? = null
        var sub_category_id: String? = null
        var post_id: String? = null


        init {
            if (user != null) {
                is_read = user.optString("is_read")
                message = user.optString("message")
                message_time = user.optString("message_time")
                notification_id = user.optString("notification_id")
                sender_id = user.optString("sender_id")
                sender_name = user.optString("sender_name")
                type = user.optString("type")

                is_live = user.optString("is_live")
                category_id = user.optString("category_id")
                sub_category_id = user.optString("sub_category_id")
                post_id = user.optString("post_id")

            }
        }
    }
}