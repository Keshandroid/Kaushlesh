package com.kaushlesh.bean

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable
import java.util.ArrayList

class VehicleTypesBeans : Serializable {
    public var vehicleTypeList: ArrayList<VehicleTypesBeans.VehicleTypeBeansList> = ArrayList<VehicleTypesBeans.VehicleTypeBeansList>()

    @Throws(JSONException::class)
    fun VehicleTypesBeans(aJSONObject: JSONObject?) {
        if (aJSONObject != null) {
            var jsonArray: JSONArray? = null
            if (aJSONObject.has("result") && aJSONObject.get("result") is JSONArray) {
                jsonArray = aJSONObject.getJSONArray("result")
            }
            if (jsonArray != null) {
                for (i in 0 until jsonArray.length()) {
                    val dataModel = VehicleTypeBeansList(jsonArray.getJSONObject(i))
                    vehicleTypeList.add(dataModel)
                }
            }
        }
    }

    class VehicleTypeBeansList(user: JSONObject?) : Serializable {
        var typeId: Int? = null
        var typeName: String? = null
        var selected: Boolean? = false
        init {
            if (user != null) {
                typeId = user.optInt("vehicalTypeId")
                typeName = user.optString("vehicalTypeName")
                selected=false
            }
        }
    }
}