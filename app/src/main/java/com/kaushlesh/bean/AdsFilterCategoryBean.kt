package com.kaushlesh.bean

import java.io.Serializable



import org.json.JSONException
import org.json.JSONObject


class AdsFilterCategoryBean : Serializable {
    var totalAllAds: String?=null
    var totalLiveAds: String?=null
    var totalExpiredAds: String?=null
    var totalPremiumAds: String?=null

    @Throws(JSONException::class)
    fun AdsFilterCategoryBean(aJSONObject: JSONObject?) {
        val user = aJSONObject?.optJSONObject("result")
        if (user != null) {
            totalAllAds = user.optString("totalAllAds")
            totalLiveAds = user.optString("totalLiveAds")
            totalExpiredAds = user.optString("totalExpiredAds")
            totalPremiumAds = user.optString("totalPremiumAds")
        }
    }
}