package com.kaushlesh.bean

class ProductListBean(
    var image: Int,
    var price: String,
    var description: String,
    var name: String,
    var address: String
)