package com.kaushlesh.bean

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable
import java.util.ArrayList

class KhedutProductTypeBeans : Serializable {
    public var productTypedList: ArrayList<KhedutProductTypeBeans.ProductBeanList> = ArrayList<KhedutProductTypeBeans.ProductBeanList>()

    @Throws(JSONException::class)
    fun KhedutProductTypeBeans(aJSONObject: JSONObject?) {
        if (aJSONObject != null) {
            var jsonArray: JSONArray? = null
            if (aJSONObject.has("result") && aJSONObject.get("result") is JSONArray) {
                jsonArray = aJSONObject.getJSONArray("result")
            }
            if (jsonArray != null) {
                for (i in 0 until jsonArray.length()) {
                    val dataModel = ProductBeanList(jsonArray.getJSONObject(i))
                    productTypedList.add(dataModel)
                }
            }

        }
    }

    class ProductBeanList(user: JSONObject?) : Serializable {
        var productId: Int? = null
        var brandImage: String? = null
        var productName: String? = null
        var isPopular: Int? = null

        init {
            if (user != null) {
               /* productId = user.optInt("subcategoryId")
                brandImage = user.optString("subcategoryImage")
                productName = user.optString("subcategoryName")*/

                productId = user.optInt("brandId")
                isPopular = user.optInt("isPopular")
                brandImage = user.optString("brandImage")
                productName = user.optString("brandName")
            }
        }
    }
}