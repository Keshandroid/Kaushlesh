package com.kaushlesh.bean.ads

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.kaushlesh.R
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.kaushlesh.adapter.CategoryAdapterNew
import com.kaushlesh.bean.AdvertisementBean
import com.kaushlesh.bean.CategoryBean
import java.lang.Exception

class SliderAdapter(
    private val Mcontext: Context,
    theSlideItemsModelClassList: List<AdvertisementBean.Advertisement>,
    private var itemClickListener: SliderAdapter.ItemClickListener? = null
    //private var itemClickListener: SliderAdapter.ItemClickListener? = null
) :
    PagerAdapter() {
    private var theSlideItemsModelClassList: List<AdvertisementBean.Advertisement>
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater = Mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val sliderLayout: View = inflater.inflate(R.layout.item_advertisement_slider, null)
        sliderLayout.setOnClickListener {
            //Toast.makeText(Mcontext, "" + theSlideItemsModelClassList[position].appbanner_title, Toast.LENGTH_SHORT).show()

            if(itemClickListener!=null){
                itemClickListener!!.itemClickAdvertisement(theSlideItemsModelClassList[position]);
            }

        }

        val featured_image = sliderLayout.findViewById<ImageView>(R.id.my_featured_image)
        //val caption_title = sliderLayout.findViewById<TextView>(R.id.my_caption_title)
//        featured_image.setImageResource(theSlideItemsModelClassList[position].appbanner_img)

        try {
            Glide.with(Mcontext)
                    .load(theSlideItemsModelClassList[position].appbanner_img)
                    .placeholder(R.drawable.progress_animated_home_banner)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .skipMemoryCache(true)
                    .into(featured_image)
        }catch (e: Exception){
            e.printStackTrace()
        }


        //caption_title.setText(theSlideItemsModelClassList[position].appbanner_title)
        container.addView(sliderLayout)
        return sliderLayout
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return theSlideItemsModelClassList.size
    }

    override fun isViewFromObject(view: View, o: Any): Boolean {
        return view === o
    }

    fun setAdsItems(adsList: ArrayList<AdvertisementBean.Advertisement>) {
        this.theSlideItemsModelClassList = adsList
        notifyDataSetChanged()
    }

    init {
        this.theSlideItemsModelClassList = theSlideItemsModelClassList
    }

    interface ItemClickListener {
        fun itemClickAdvertisement(advertisement: AdvertisementBean.Advertisement)
    }

    fun setClicklistnerAds(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

}