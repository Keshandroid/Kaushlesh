package com.kaushlesh.bean.AdPost

import android.graphics.Bitmap

class AdGrainSeedBean {

    var userId: Int? = null
    var userToken: String=""
    var category_id: String=""
    var sub_category_id: String=""
    var listed_by: String=""
    var available_stock: String=""
    var other_information: String=""
    var title: String=""
    var location_id: String=""
    var address: String=""
    var latitude: String=""
    var longitude: String=""
    var price: String=""
    var package_id: String=""
    var post_images: ArrayList<Bitmap>? = null
}