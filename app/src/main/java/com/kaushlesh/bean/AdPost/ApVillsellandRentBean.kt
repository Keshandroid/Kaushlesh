package com.kaushlesh.bean.AdPost

import android.graphics.Bitmap
import java.io.Serializable

class ApVillsellandRentBean : Serializable {
    var userId: Int? = null
    var userToken: String=""
    var category_id: String=""
    var sub_category_id: String=""
    var package_id: String=""
    var property_type: String=""
    var bedrooms: String=""
    var bathrooms: String=""
    var furnishing: String=""
    var construction_status: String=""
    var listed_by: String=""
    var super_builtup_area: String=""
    var carpet_area: String=""
    var bachelors_allowed: String=""
    var maintenance: String=""
    var total_floors: String=""
    var floor_no: String=""
    var car_parking: String=""
    var facing: String=""
    var project_name: String=""
    var other_information: String=""
    var title: String=""
    var location_id: String=""
    var address: String=""
    var latitude: String=""
    var longitude: String=""
    var price: String=""
    var post_images: ArrayList<Bitmap>? = null
}