package com.kaushlesh.bean.AdPost

import android.util.Log
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable
import java.util.ArrayList

class AdPostResponseBean : Serializable{

    public var pkglist: ArrayList<PkgInfo> = ArrayList<PkgInfo>()

    @Throws(JSONException::class)
    fun AdPostResponseBean(aJSONObject: JSONObject?) {

        if (aJSONObject != null) {

            var jsonArray: JSONArray? = null
            if (aJSONObject.has("result") && aJSONObject.get("result") is JSONArray) {
                jsonArray = aJSONObject.getJSONArray("result")
            }
            if (jsonArray != null) {
                for (i in 0 until jsonArray.length()) {
                    val dataModel = PkgInfo(jsonArray.getJSONObject(i))
                    pkglist.add(dataModel)

                }
            }

        }
    }

    class PkgInfo(jsonObject: JSONObject?) : Serializable {

        var pkgid: String? = null
        var name: String? = null
        var catid: String? = null
        var pkgvalidity: String? = null
        var pkgtype: String? = null
        var duration: String? = null
        var noofad: String? = null
        var remainingpost: String? = null
        var price: String? = null

        var totalfreeAd: String? = null
        var availableFreeAd: String? = null
        var usedFreeAd: String? = null
        var totalextraAd: String? = null
        var availExtraAd: String? = null
        var usedExtraAd: String? = null
        var freeAdRenewAfter: String? = null
        var expdate: String? = null

        init {
            Log.i("KAUSHLESH", "===product detail img list===" + jsonObject!!)

            pkgid = jsonObject.optString("package_id")
            name = jsonObject.optString("name")
            catid = jsonObject.optString("category_id")
            pkgvalidity = jsonObject.optString("package_validity")
            pkgtype = jsonObject.optString("package_type")
            duration = jsonObject.optString("duration")
            noofad = jsonObject.optString("noOfAd")
            remainingpost = jsonObject.optString("remainingPost")
            price = jsonObject.optString("price")

            totalfreeAd = jsonObject.optString("totalFreeAd")
            availableFreeAd = jsonObject.optString("avilableFreeAd")
            usedFreeAd = jsonObject.optString("usedFreeAd")
            totalextraAd = jsonObject.optString("totalExtraAd")
            availExtraAd = jsonObject.optString("avilableExtraAd")
            usedExtraAd = jsonObject.optString("usedExtraAd")
            freeAdRenewAfter = jsonObject.optString("freeAdRenewAfter")
            expdate = jsonObject.optString("expDate")
        }
    }
}