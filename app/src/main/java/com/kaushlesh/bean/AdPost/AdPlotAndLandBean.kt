package com.kaushlesh.bean.AdPost

import android.graphics.Bitmap
import java.io.Serializable

class AdPlotAndLandBean:Serializable {
    var userId: Int? = null
    var userToken: String=""
    var category_id: String=""
    var sub_category_id: String=""
    var title: String=""
    var location_id: Int? = null
    var address: String=""
    var latitude: String=""
    var longitude: String=""
    var price: String=""
    var post_images: ArrayList<Bitmap>? = null
    var property_type: String=""
    var purpose: String=""
    var plot_area: String=""
    var length: String=""
    var width: String=""
    var facing: String=""
    var project_name: String=""
    var other_information: String=""
    var package_id: String=""
    var listed_by: String=""
}