package com.kaushlesh.bean.AdPost

import android.graphics.Bitmap

class AdKhedutBean {

    var userId: Int ?= null
    var userToken: String =""
    var category_id: String =""
    var sub_category_id: String =""
    var title: String =""
    var other_information: String=""
    var location_id: Int? = null
    var address: String =""
    var latitude: String=""
    var longitude: String=""
    var price: String=""
    var post_images: ArrayList<Bitmap>? = null
    var package_id: String =""

    var productId: String=""
    var producttype: String="0"
    var listed_by: String=""
    var available_stock: String=""
}
