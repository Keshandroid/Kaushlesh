package com.kaushlesh.bean.AdPost

import android.graphics.Bitmap
import java.io.Serializable

class AdPartyPlotBean : Serializable {
    var userId: Int? = null
    var userToken: String=""
    var category_id: String=""
    var sub_category_id: String=""
    var title: String=""
    var location_id: Int=1
    var address: String=""
    var latitude: String=""
    var longitude: String=""
    var price: String=""
    var post_images: ArrayList<Bitmap>? = null
    var party_plot_name: String=""
    var purpose: String=""
    var plot_area: String=""
    var guest_capacity: String=""
    var no_of_guest_room: String=""
    var guest_room_available: String=""
    var kitchen_available: String=""
    var wash_room_available: String=""
    var other_information: String=""
    var package_id: String=""
    var listed_by: String=""
}

