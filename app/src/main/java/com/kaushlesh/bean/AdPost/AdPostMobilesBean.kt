package com.kaushlesh.bean.AdPost

import android.graphics.Bitmap

class AdPostMobilesBean {

    var userId: Int ?= null
    var userToken: String =""
    var category_id: String =""
    var sub_category_id: String =""
    var title: String =""
    var location_id: Int? = null
    var address: String =""
    var latitude: String=""
    var longitude: String=""
    var price: String=""
    var post_images: ArrayList<Bitmap>? = null
    var other_information: String=""
    var brandid: String=""
    var mobiletype: String=""
    var package_id: String =""
}