package com.kaushlesh.bean.AdPost

import android.graphics.Bitmap

class AdPostBikesBean {

    var userId: Int ?= null
    var userToken: String =""
    var category_id: String =""
    var sub_category_id: String =""
    var title: String =""
    var location_id: Int? = null
    var address: String =""
    var latitude: String=""
    var longitude: String=""
    var price: String=""
    var post_images: ArrayList<Bitmap>? = null
    var other_information: String? = null
    var package_id: String =""

    var brandId: String=""
    var year: String=""
    var fuel: String=""
    var kmDriven: String=""
}