package com.kaushlesh.bean.AdPost

import android.graphics.Bitmap
import java.io.Serializable

class PgGuestHouseBean:Serializable {
    var userId: Int? = null
    var userToken: String=""
    var category_id: String=""
    var sub_category_id: String=""
    var title: String=""
    var location_id: Int? = null
    var address: String=""
    var latitude: String=""
    var longitude: String=""
    var price: String=""
    var post_images: ArrayList<Bitmap>? = null
    var property_type: String=""
    var furnishing: String=""
    var listed_by: String=""
    var car_parking: String=""
    var meals_included: String=""
    var ac: String=""
    var other_information: String=""
    var package_id: String=""

}
