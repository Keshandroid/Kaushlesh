package com.kaushlesh.bean

class MyOrdersBean(
    var title1: String,
    var title2: String,
    var category: String,
    var location: String,
    var activatedate: String,
    var expdate: String,
    var orderid: String,
    var purchased: String,
    var available: String,
    var used: String
)
