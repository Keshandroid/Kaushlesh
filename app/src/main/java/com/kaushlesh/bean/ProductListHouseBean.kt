package com.kaushlesh.bean

import android.util.Log
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable
import java.util.ArrayList

class ProductListHouseBean : Serializable{

    public var productlist: ArrayList<ProductListHouse> = ArrayList<ProductListHouse>()

    @Throws(JSONException::class)
    fun ProductListHouseBean(aJSONObject: JSONObject?) {

        if (aJSONObject != null) {

            var jsonArray: JSONArray? = null
            if (aJSONObject.has("result") && aJSONObject.get("result") is JSONArray) {
                jsonArray = aJSONObject.getJSONArray("result")
            }
            if (jsonArray != null) {
                for (i in 0 until jsonArray.length()) {
                    val dataModel = ProductListHouse(jsonArray.getJSONObject(i))
                    productlist.add(dataModel)

                }
            }

        }
    }

    class PostImageX(jsonObject: JSONObject?) : Serializable {

        var postImage: String? = null

        init {
            //Log.i("KAUSHLESH", "===product detail img list===" + jsonObject!!)

            postImage = jsonObject!!.optString("postImage")
        }

    }

    class ProductListHouse(user: JSONObject?) : Serializable {

        var post_id: Int?=null
        var title: String?=null
        var category_id: Int?=null
        var categoryName: String?=null
        var sub_category_id: Int?=null
        var sub_categoryName: String?=null
        var location_id: Int?=null
        var post_status: Int?=null
        var price: Int?=null
        var address: String?=null
        var latitude: String?=null
        var longitude: String?=null
        var published_date: String?=null
        var expire_date: String?=null
        var property_detail_id: Int?=null
        var property_type: String?=null
        var bedrooms: Int?=null
        var bathrooms: Int?=null
        var furnishing: Int?=null
        var construction_status: Int?=null
        var listed_by: String?=null
        var super_builtup_area: Int?=null
        var carpet_area: Int?=null
        var bachelors_allowed: Int?=null
        var maintenance: Int?=null
        var total_floors: Int?=null
        var floor_no: Int?=null
        var car_parking: Int?=null
        var facing: Int?=null
        var project_name: String?=null
        var other_information: String?=null
        var ispremium: Int?=null
        var isFavorite: Int?=null

        var postImages: ArrayList<PostImageX> = ArrayList<PostImageX>()

        init {

            if (user != null) {

                post_id = user.optInt("post_id")
                title = user.optString("title")
                category_id = user.optInt("category_id")
                categoryName = user.optString("categoryName")
                sub_category_id = user.optInt("sub_category_id")
                sub_categoryName = user.optString("sub_categoryName")
                location_id = user.optInt("location_id")
                post_status = user.optInt("post_status")
                price = user.optInt("price")
                address = user.optString("address")
                latitude = user.optString("latitude")
                longitude = user.optString("longitude")
                published_date = user.optString("published_date")
                expire_date = user.optString("expire_date")
                property_detail_id = user.optInt("property_detail_id")
                property_type = user.optString("property_type")
                bedrooms = user.optInt("bedrooms")
                bathrooms = user.optInt("bathrooms")
                furnishing = user.optInt("furnishing")
                construction_status = user.optInt("construction_status")
                listed_by = user.optString("listed_by")
                super_builtup_area = user.optInt("super_builtup_area")
                carpet_area = user.optInt("carpet_area")
                bachelors_allowed = user.optInt("bachelors_allowed")
                maintenance = user.optInt("maintenance")
                total_floors = user.optInt("total_floors")
                floor_no = user.optInt("floor_no")
                car_parking = user.optInt("car_parking")
                facing = user.optInt("facing")
                project_name = user.optString("project_name")
                other_information = user.optString("other_information")
                ispremium = user.optInt("is_primium")
                isFavorite = user.optInt("isFavorite")


                var jsonArray: JSONArray? = null
                if (user.has("postImages") && user.get("postImages") is JSONArray) {
                    jsonArray = user.getJSONArray("postImages")
                }
                if (jsonArray != null) {
                    for (i in 0 until jsonArray.length()) {
                        val dataModel = PostImageX(jsonArray.getJSONObject(i))
                        postImages.add(dataModel)

                    }
                }
            }
        }
    }
}