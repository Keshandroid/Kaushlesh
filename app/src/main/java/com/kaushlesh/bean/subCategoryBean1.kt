package com.kaushlesh.bean

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable

class subCategoryBean1 :Serializable{

    public var subcategoryList: ArrayList<SubCategory> = ArrayList<SubCategory>()

    @Throws(JSONException::class)
    fun subCategoryBean1(aJSONObject: JSONObject?) {

        if (aJSONObject != null) {

            var jsonArray: JSONArray? = null
            if (aJSONObject.has("result") && aJSONObject.get("result") is JSONArray) {
                jsonArray = aJSONObject.getJSONArray("result")
            }
            if (jsonArray != null) {
                for (i in 0 until jsonArray.length()) {
                    val dataModel = SubCategory(jsonArray.getJSONObject(i))
                    subcategoryList.add(dataModel)

                }
            }

        }
    }

    class SubCategory(jsonObject: JSONObject?) : Serializable {

        var subcategoryId: Int?=null
        var subcategoryName: String?=null
        var subcategoryImage: String?=null

        init {

            if (jsonObject != null) {

                subcategoryId = jsonObject.optInt("subcategoryId")
                subcategoryName = jsonObject.optString("subcategoryName")
                subcategoryImage = jsonObject.optString("subcategoryImage")

            }
        }

    }

}