package com.kaushlesh.chat

/**
 * Created by ansh on 28/08/18.
 */
class ChatMessage(
        val id: String,
        val text: String,
        val fromId: String,
        val toId: String,
        val timestamp: Long,
        val unread : Int,
        val profile : String,
        val sendername : String,
        val receivername : String,
        val senderProfile : String,
        val receiverProfile : String,
        val productImg : String,
        val subCatName : String
) {
    constructor() : this("", "", "", "", -1,0,"","","","","","","")
}