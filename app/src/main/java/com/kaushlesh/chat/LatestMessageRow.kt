package com.kaushlesh.chat

import android.app.Activity
import android.content.Context
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.kaushlesh.R
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.item_row_chat_all.view.*

class LatestMessageRow(val chatMessage: ChatMessage, val context: Context,viewType: Int) : Item<ViewHolder>() {
    var viewType1 = viewType
    var chatPartnerUser: User? = null

    override fun getLayout(): Int {

        if (viewType1 == 1) {
            return R.layout.item_row_chat_all
        }
        else{
            return R.layout.item_row_chat_all_selection
        }
    }

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.tv_msg.text = chatMessage.text

        val storeUserData = StoreUserData(context)

        Utils.showLog("chat","==from id==" +  chatMessage.fromId + "$$$" + "===firebase id==="+ FirebaseAuth.getInstance().uid)
        /* if (chatMessage.fromId == FirebaseAuth.getInstance().uid) {
             Utils.showLog("chat","==sender==" +  chatMessage.receivername)
             viewHolder.itemView.tvname.text = chatMessage.receivername
         }
         else{
             Utils.showLog("chat","==sender==" + chatMessage.sendername)
             viewHolder.itemView.tvname.text = chatMessage.sendername

         }*/

        Utils.showLog("chat","==from id==" +  chatMessage.fromId + "$$$" + "===login id==="+ storeUserData.getString(Constants.USER_ID))
        if (chatMessage.fromId == storeUserData.getString(Constants.USER_ID)) {
            Utils.showLog("chat","==sender==" +  chatMessage.receivername)
            viewHolder.itemView.tvname.text = chatMessage.receivername
            val requestOptions = RequestOptions().placeholder(R.drawable.ic_user_pic)
            Glide.with(context)
                .load(chatMessage.receiverProfile)
                .apply(requestOptions)
                .into(viewHolder.itemView.civ_profile)

        }
        else{
            Utils.showLog("chat","==sender==" + chatMessage.sendername)
            viewHolder.itemView.tvname.text = chatMessage.sendername
            val requestOptions = RequestOptions().placeholder(R.drawable.ic_user_pic)
            Glide.with(context)
                .load(chatMessage.senderProfile)
                .apply(requestOptions)
                .into(viewHolder.itemView.civ_profile)

        }

        val requestOptions1 = RequestOptions().placeholder(R.drawable.bg_img_placeholder)
        Glide.with(context)
            .load(chatMessage.productImg)
            .apply(requestOptions1)
            .into(viewHolder.itemView.iv_product_img)
        viewHolder.itemView.tv_category.text = chatMessage.subCatName


        Utils.showLog("chat","==data==" + FirebaseAuth.getInstance().uid)
        Utils.showLog("chat","==data==" + chatMessage.productImg)
    }

}