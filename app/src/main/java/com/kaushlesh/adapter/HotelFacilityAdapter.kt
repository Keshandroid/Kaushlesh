package com.kaushlesh.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.R
import com.kaushlesh.bean.FilterBean

class HotelFacilityAdapter(private val list: List<FilterBean>, private val context: Context) :
    RecyclerView.Adapter<HotelFacilityAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_hotel_service_facility, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel = list[position]

        holder.tvname.text=dataModel.name
        holder.iv_image.setBackgroundResource(getHotelFacility(dataModel.id))

    }

    override fun getItemCount(): Int {
        return list.size
    }
    fun getHotelFacility(id: Int) : Int{
        when(id)
        {
            1->
            {
                return R.drawable.ic_food_black
            }
            2->
            {
                return R.drawable.ic_gym_black
            }
            3->
            {
                return R.drawable.ic_card_black
            }
            4->
            {
                return R.drawable.ic_wifi_black
            }
            5->
            {
               // return "Spa"
                return R.drawable.ic_room_black

            }
            6->
            {
                return R.drawable.ic_swimming_black
            }
            7->
            {
                return R.drawable.ic_security_black
            }
            8->
            {
             //   return "Fire & Safety"
                return R.drawable.ic_fire_black
            }
            9->
            {
              //  return "Room Service"
                return R.drawable.ic_room_black

            }
            10->
            {
              //  return "Laundry"
                return R.drawable.ic_fire_black

            }
            11->
            {
              //  return "Power Backup"
                return R.drawable.ic_powerback_black

            }
        }
        return 0
    }



    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        internal var tvname: TextView
        internal var iv_image: ImageView

        init {

            tvname = view.findViewById(R.id.tv_name)
            iv_image = view.findViewById(R.id.iv_image)
        }
    }
}

