package com.kaushlesh.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.facebook.appevents.ml.Utils
import com.kaushlesh.bean.GetCategoryPackagesBean
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.view.fragment.MyAccount.PurchasePackage.ExtraAdFragment
import com.kaushlesh.view.fragment.MyAccount.PurchasePackage.PremiumAdFragment
import java.util.ArrayList

class AdPostPackagesPagerAdapter(
    private val myContext: Context,
    fm: FragmentManager,
    internal var totalTabs: Int,var ispremium : String) : FragmentPagerAdapter(fm) {

    // this is for fragment tabs
    
    override fun getItem(position: Int): Fragment {

        AppConstants.printLog("adapter===", ispremium)
        if(ispremium.equals("yes"))
        {
            when (position) {
                0 -> {
                    return PremiumAdFragment()
                }
            }
        }
        else if(ispremium.equals("no"))
        {
            when (position) {
                0 -> {
                    return ExtraAdFragment()
                }
            }
        }
        when (position) {
            0 -> {
                return ExtraAdFragment()
            }
            else -> {
                return PremiumAdFragment()
            }
        }
    }

    // this counts total number of tabs
    override fun getCount(): Int {
        return totalTabs
    }
}