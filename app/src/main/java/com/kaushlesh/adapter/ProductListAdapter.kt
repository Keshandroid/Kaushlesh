package com.kaushlesh.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.ImageView
import android.widget.TextView
import android.widget.ToggleButton
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

import com.kaushlesh.R
import com.kaushlesh.bean.GetMyPostBean
import com.kaushlesh.bean.PostImage
import com.kaushlesh.bean.ProductDataBean
import com.kaushlesh.bean.ProductListHouseBean
import com.kaushlesh.utils.PostDetail.SetPostDetailData
import com.kaushlesh.utils.Utils
import kotlinx.android.synthetic.main.toolbar_with_menu.*

class ProductListAdapter() : RecyclerView.Adapter<ProductListAdapter.ViewHolder>() {

    var list: ArrayList<ProductDataBean> = ArrayList()
    var listhouse: ArrayList<ProductListHouseBean.ProductListHouse> = ArrayList()
    var viewType1 = 1
    lateinit var context:Context
    var categoryId : Int?= null
    var subCategoryId : Int?= null
    var items: ArrayList<ProductListHouseBean.ProductListHouse> = ArrayList()

    private var itemClickListener: ItemClickListener? = null

    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

  /*  constructor(arrayList: List<ProductDataBean>?, context: Context, viewType: Int) : this() {
        this.list = arrayList as ArrayList<ProductDataBean>
        this.viewType1 = viewType
        this.context=context
    }*/

    constructor(arrayList: ArrayList<ProductListHouseBean.ProductListHouse>?, context: Context, viewType: Int, categoryId: Int?, subCategoryId: Int?) : this() {
       /* this.listhouse = arrayList as ArrayList<ProductListHouseBean.ProductListHouse>*/
        this.items = arrayList as ArrayList<ProductListHouseBean.ProductListHouse>
        this.viewType1 = viewType
        this.context=context
        this.categoryId = categoryId
        this.subCategoryId = subCategoryId
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view: View
        Log.e("test","==view type0==" + viewType1)
        if (viewType1 == 1) {
            Log.e("test","==view type1==" + viewType1)
            view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_row_product_list_type1, parent, false)
        }
        else if (viewType1 == 2) {
            Log.e("test","==view type2==" + viewType1)
            view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_row_product_list_type2, parent, false)
        }
        else
        {
            Log.e("test","==view type3==" + viewType1)
            view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_row_product_list_type3, parent, false)
        }


        return ViewHolder(view)

    }

    fun getAllItems(): ArrayList<ProductListHouseBean.ProductListHouse> {
        return this.items
    }

    fun updateLikeDislikePost(likeState: String?, postIdLikeDislike: String?) {

        if(!items.isEmpty()){
            for (i in items.indices){
                val post_id = items[i].post_id
                if(postIdLikeDislike == post_id.toString()){
                    Log.d("CLICKED_ITEM", " Matched in Post screen " + i)

                    if(likeState == "true"){
                        items[i].isFavorite = 1
                    }else if(likeState == "false"){
                        items[i].isFavorite = 0
                    }

                    notifyItemChanged(i)
                }else{
                    Log.d("CLICKED_ITEM", " NOT Matched in Post screen " + i)
                }
            }
        }
    }

    fun addItems(list: java.util.ArrayList<ProductListHouseBean.ProductListHouse>) {
        /*   items.addAll(list)
           //notifyItemInserted(items.size - 1);
           notifyDataSetChanged()*/
        val oldSize = itemCount
        val distinct = list.distinctBy { it.post_id }
        items.addAll(distinct)
        notifyItemRangeInserted(oldSize, items.size)
    }

    fun removeItems() {
        items.clear()
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

       // if(categoryId ==2 && (subCategoryId == 1 || subCategoryId == 2)) {
            val dataModel = items[position]
            Utils.showLog("adapter","==model==" + items.size)
            setdata(holder,dataModel)
       // }

        holder.ivlike.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {

                if(buttonView!!.isPressed){
                    itemClickListener!!.itemFavUnfavourite(dataModel,position)

                }


            }
        })

        /*holder.ivlike.setOnClickListener {

            itemClickListener!!.itemFavUnfavourite(dataModel,position)

            if(holder.ivlike.background.getConstantState()?.equals(context.getDrawable(R.drawable.ic_favorite_filled_black)?.getConstantState())!!)
            {
                SetPostDetailData.setFav(holder.ivlike,0)
            }
            else{
                SetPostDetailData.setFav(holder.ivlike,1)
            }
        }*/
    }

    private fun setdata(holder: ViewHolder, dataModel: ProductListHouseBean.ProductListHouse) {

        if (dataModel.ispremium == 1) {
            holder.tvfeatured.visibility = View.VISIBLE
        } else {
            holder.tvfeatured.visibility = View.GONE
        }

        if(dataModel.category_id == 12)
        {
            if(dataModel.price!! == 0)
            {
                //holder.tvprice.visibility = View.GONE
                holder.tvprice.visibility = View.VISIBLE
                holder.tvprice.text = "₹ -"
            }
            else{
                holder.tvprice.visibility = View.VISIBLE
                holder.tvprice.text = "₹ " + dataModel.price
            }
        }
        else {
            if (dataModel.price!! == 0) {
                //holder.tvprice.visibility = View.GONE
                holder.tvprice.visibility = View.VISIBLE
                holder.tvprice.text = "₹ (Price Not Applicable)"
            } else {
                holder.tvprice.visibility = View.VISIBLE
                holder.tvprice.text = "₹ " + dataModel.price
            }
        }

        holder.tvdesc.text = dataModel.title
        holder.tvaddress.text = dataModel.address
        holder.tvname.text = dataModel.other_information

        val bean2 = PostImage()

      /*  for (i in 0 until dataModel.postImages.size) {
            bean2.postImage = dataModel.postImages[i].postImage
            Glide.with(context)
                .load(bean2.postImage)
                .into(holder.ivimg)
        }*/

        if(dataModel.postImages != null && dataModel.postImages.size >0) {
            Glide.with(context)
                    .load(dataModel.postImages[0].postImage)
                    .error(R.drawable.bg_img_placeholder)
                    .override(1600, 1600) // Can be 2000, 2000
                    .placeholder(R.drawable.progress_animated)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .skipMemoryCache(true)
                    .into(holder.ivimg)
            //  Picasso.get().load(dataModel.postImages[0].postImage).into(holder.ivimg);
        }
        else
        {
            Glide.with(context)
                    .load(R.drawable.bg_img_placeholder)
                    .placeholder(R.drawable.progress_animated)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .skipMemoryCache(true)
                    .into(holder.ivimg)
            //Picasso.get().load(R.drawable.bg_img_placeholder).into(holder.ivimg);
        }

        if(dataModel.isFavorite == 1)
        {
            holder.ivlike.isChecked = true
            //holder.ivlike.setBackgroundResource(R.drawable.ic_favorite_filled_black)
        }
        else{
            holder.ivlike.isChecked = false
//            holder.ivlike.setBackgroundResource(R.drawable.ic_fav_black)
        }

        holder.itemView.setOnClickListener { itemClickListener!!.itemclickProduct(dataModel) }
    }

    override fun getItemCount(): Int {
        /*if(categoryId ==2 && (subCategoryId == 1 || subCategoryId == 2)) {
            return listhouse.size
        }*/
        return items.size
    }

    interface ItemClickListener {
        fun itemclickProduct(bean: ProductListHouseBean.ProductListHouse)
        fun itemFavUnfavourite(dataModel: ProductListHouseBean.ProductListHouse, position: Int)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var ivimg: ImageView
        internal var tvname: TextView
        internal var tvprice: TextView
        internal var tvdesc: TextView
        internal var tvaddress: TextView
        internal var tvfeatured: TextView
        //internal var ivlike: ImageView
        internal var ivlike: ToggleButton

        init {
            ivimg = view.findViewById(R.id.iv_img)
            tvname = view.findViewById(R.id.tv_pdetail)
            tvprice = view.findViewById(R.id.tv_price)
            tvdesc = view.findViewById(R.id.tv_desc)
            tvaddress = view.findViewById(R.id.tv_address)
            tvfeatured = view.findViewById(R.id.tv_title)
            ivlike = view.findViewById(R.id.iv_like)
        }
    }
}

