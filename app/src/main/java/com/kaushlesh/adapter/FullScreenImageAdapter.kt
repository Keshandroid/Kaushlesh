package com.kaushlesh.adapter

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.graphics.PointF
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.target.SimpleTarget
import com.kaushlesh.R
import com.kaushlesh.bean.PostDetails.postDetailsBean
import com.kaushlesh.reusable.ZoomableImageView
import com.kaushlesh.utils.Utils
import com.squareup.picasso.Picasso
import com.squareup.picasso.Picasso.LoadedFrom
import java.io.File
import java.net.URL
import java.util.*


class FullScreenImageAdapter     // constructor
(
        private val _activity: Activity,
        private val _imagePaths: ArrayList<postDetailsBean.PostImageX>
) : PagerAdapter() {
    private var inflater: LayoutInflater? = null

    override fun getCount(): Int {
        return _imagePaths.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as RelativeLayout
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val imgDisplay: ImageView
        val btnClose: Button
        val imgZoom: ZoomableImageView
        val tvImgCount: TextView
        val height=0
        val width=0
        inflater = _activity
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val viewLayout = LayoutInflater.from(container.context)
            .inflate(R.layout.layout_fullscreen_image, container, false)

        imgDisplay = viewLayout.findViewById<View>(R.id.imgDisplay) as ImageView
      //  imgZoom = viewLayout.findViewById<View>(R.id.iv_zoom) as ZoomableImageView
        btnClose = viewLayout.findViewById<View>(R.id.btnClose) as Button
        tvImgCount = viewLayout.findViewById<View>(R.id.tv_count) as TextView

        val currentposion= position +1
        tvImgCount.text = (currentposion).toString() +"/"+_imagePaths.size.toString()

        val options = BitmapFactory.Options()
        options.inPreferredConfig = Bitmap.Config.ARGB_8888




        Utils.showLog(TAG, "===height===" + height + "===")
        Utils.showLog(TAG, "===width===" + width + "===")

//        val layoutParams: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(width, height)
//        imgDisplay.setLayoutParams(layoutParams)

        Glide.with(_activity)
            .load(_imagePaths[position].postImage).diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(imgDisplay)



        val imageMatrixTouchHandler = ImageMatrixTouchHandler(_activity)
        imgDisplay.setOnTouchListener(imageMatrixTouchHandler)


        // close button click event
        btnClose.setOnClickListener { _activity.finish() }
        (container as ViewPager).addView(viewLayout)
        return viewLayout

    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        (container as ViewPager).removeView(`object` as RelativeLayout)
        //super.destroyItem(container, position, `object`)
    }

    private val TAG = "Touch"
    private val MIN_ZOOM = 1f
    val MAX_ZOOM = 1f

    // These matrices will be used to scale points of the image
    var matrix: Matrix = Matrix()
    var savedMatrix: Matrix = Matrix()

    // The 3 states (events) which the user is trying to perform
    val NONE = 0
    val DRAG = 1
    val ZOOM = 2
    var mode = NONE

    // these PointF objects are used to record the point(s) the user is touching
    var start = PointF()
    var mid = PointF()
    var oldDist = 1f


    /*
     * --------------------------------------------------------------------------
     * Method: spacing Parameters: MotionEvent Returns: float Description:
     * checks the spacing between the two fingers on touch
     * ----------------------------------------------------
     */

    /*
     * --------------------------------------------------------------------------
     * Method: spacing Parameters: MotionEvent Returns: float Description:
     * checks the spacing between the two fingers on touch
     * ----------------------------------------------------
     */
    private fun spacing(event: MotionEvent): Float {
        val x = event.getX(0) - event.getX(1)
        val y = event.getY(0) - event.getY(1)
        return Math.sqrt((x * x + y * y).toDouble()).toFloat()
    }

    /*
     * --------------------------------------------------------------------------
     * Method: midPoint Parameters: PointF object, MotionEvent Returns: void
     * Description: calculates the midpoint between the two fingers
     * ------------------------------------------------------------
     */

    /*
     * --------------------------------------------------------------------------
     * Method: midPoint Parameters: PointF object, MotionEvent Returns: void
     * Description: calculates the midpoint between the two fingers
     * ------------------------------------------------------------
     */
    private fun midPoint(point: PointF, event: MotionEvent) {
        val x = event.getX(0) + event.getX(1)
        val y = event.getY(0) + event.getY(1)
        point[x / 2] = y / 2
    }

    /** Show an event in the LogCat view, for debugging  */
    private fun dumpEvent(event: MotionEvent) {
        val names = arrayOf("DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE", "POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?")
        val sb = StringBuilder()
        val action = event.action
        val actionCode = action and MotionEvent.ACTION_MASK
        sb.append("event ACTION_").append(names[actionCode])
        if (actionCode == MotionEvent.ACTION_POINTER_DOWN || actionCode == MotionEvent.ACTION_POINTER_UP) {
            sb.append("(pid ").append(action shr MotionEvent.ACTION_POINTER_ID_SHIFT)
            sb.append(")")
        }
        sb.append("[")
        for (i in 0 until event.pointerCount) {
            sb.append("#").append(i)
            sb.append("(pid ").append(event.getPointerId(i))
            sb.append(")=").append(event.getX(i).toInt())
            sb.append(",").append(event.getY(i).toInt())
            if (i + 1 < event.pointerCount) sb.append(";")
        }
        sb.append("]")
        Log.d("Touch Events ---------", sb.toString())
    }

    fun getDropboxIMGHeightSize(uri: Uri): Int {
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeFile(File(uri.path).absolutePath, options)
        val imageHeight = options.outHeight
        Utils.showLog(TAG, "===imageHeight===" + imageHeight + "===")

        return imageHeight;

    }
    fun getDropboxIMGWidthSize(uri: Uri): Int {
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeFile(File(uri.path).absolutePath, options)
        val imageWidth = options.outWidth
        Utils.showLog(TAG, "===imageWidth===" + imageWidth + "===")
        return imageWidth;

    }
    fun getBitmap(uri: String?): Bitmap {
        val url = URL(uri)
        val bmp =
            BitmapFactory.decodeStream(url.openConnection().getInputStream())

        Utils.showLog(TAG, "===bmp===" + bmp + "===")

        return bmp;

    }


}
