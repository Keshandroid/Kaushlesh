package com.kaushlesh.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.R
import com.kaushlesh.bean.BrandShowCaseBean
import com.kaushlesh.bean.FilterBean
import android.graphics.Color.parseColor
import androidx.core.content.ContextCompat


class FilterAdapter(private val list: List<FilterBean>, private val context: Context) :
    RecyclerView.Adapter<FilterAdapter.ViewHolder>() {
    private var itemClickListener: ItemClickListener? = null
    internal var temp_llmain: LinearLayout? = null
    internal var selectedpos: Int = 0
    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_row_filter, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        temp_llmain = holder.llmain

        val dataModel = list[position]

        holder.tvname.text = dataModel.name

        if (selectedpos == position) {
            holder.itemView.isSelected = true //using selector drawable
            holder.llmain.setBackgroundColor(context.resources.getColor(R.color.white))
        } else {
            holder.itemView.isSelected = false
            holder.llmain.setBackgroundColor(context.resources.getColor(R.color.greylight))
        }

        holder.itemView.setOnClickListener {

            if (selectedpos >= 0)
                notifyItemChanged(selectedpos)
            selectedpos = holder.adapterPosition
            notifyItemChanged(selectedpos)
            temp_llmain = holder.llmain

            itemClickListener!!.itemclickView(dataModel)

        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    interface ItemClickListener {
        fun itemclickView(bean: FilterBean)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var tvname: TextView
        internal var llmain: LinearLayout

        init {

            tvname = view.findViewById(R.id.tv_name)
            llmain = view.findViewById(R.id.ll_main)
        }
    }
}

