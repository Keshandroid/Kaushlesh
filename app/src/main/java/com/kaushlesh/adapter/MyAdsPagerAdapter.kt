package com.kaushlesh.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.kaushlesh.view.fragment.MyAccount.MyAds.MyPostAdFragment
import com.kaushlesh.view.fragment.MyAccount.MyAds.MyPostFavouriteFragment

class MyAdsPagerAdapter(private val myContext: Context, fm: FragmentManager, internal var totalTabs: Int) : FragmentPagerAdapter(fm) {

    // this is for fragment tabs

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return MyPostAdFragment()
            else -> return MyPostFavouriteFragment()
        }
    }

    // this counts total number of tabs
    override fun getCount(): Int {
        return totalTabs
    }
}