package com.kaushlesh.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.kaushlesh.R
import com.kaushlesh.bean.PostDetails.postDetailsBean
import java.util.ArrayList

class RelaventPostAdapter(
    private val list: ArrayList<postDetailsBean.RelatedArray>, private val context: Context) :
    RecyclerView.Adapter<RelaventPostAdapter.ViewHolder>() {
    private var itemClickListener: ItemClickListener? = null

    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_relavent_post, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val dataModel = list[position]

        if (dataModel.postImages.size > 0) {
                Glide.with(context)
                    .load(dataModel.postImages.get(0).postImage).centerCrop()
                    .placeholder(R.drawable.progress_animated)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .skipMemoryCache(true)
                    .into(holder.ivimg)
        }

        if(dataModel.price!! == 0)
        {
            holder.tvprice.visibility = View.GONE
        }
        else{
            holder.tvprice.visibility = View.VISIBLE
            holder.tvprice.text = "₹ " + dataModel.price
        }

        holder.tvdesc.text = dataModel.title
        holder.tvaddress.text = dataModel.address

        holder.itemView.setOnClickListener {
            itemClickListener!!.itemclickRecommendation(dataModel)
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    interface ItemClickListener {
        fun itemclickRecommendation(bean: postDetailsBean.RelatedArray)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var ivimg: ImageView
        internal var tvfeatured: TextView
        internal var tvprice: TextView
        internal var tvdesc: TextView
        internal var tvaddress: TextView

        init {
            ivimg = view.findViewById(R.id.iv_img)
            tvfeatured = view.findViewById(R.id.tv_title)
            tvprice = view.findViewById(R.id.tv_price)
            tvdesc = view.findViewById(R.id.tv_desc)
            tvaddress = view.findViewById(R.id.tv_address)
        }
    }
}

