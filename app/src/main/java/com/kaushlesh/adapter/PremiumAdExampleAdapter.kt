package com.kaushlesh.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.R
import android.view.animation.Animation
import android.view.animation.AlphaAnimation
import com.kaushlesh.bean.GetMyPostBean


class PremiumAdExampleAdapter(): RecyclerView.Adapter<PremiumAdExampleAdapter.ViewHolder>() {

    var viewType1 = 1
    lateinit var context: Context
    var list: ArrayList<GetMyPostBean.getMyPostList> = ArrayList()

    private var itemClickListener: ItemClickListener? = null

    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    constructor(context: Context, list: ArrayList<GetMyPostBean.getMyPostList>) : this() {
        this.list = list
        this.context=context
    }
    constructor(context: Context) : this() {
        this.context=context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        var view: View

        view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_premium_example, parent, false)

        return ViewHolder(view)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

     /*   var dataModel=list[position]
        holder.tv_desc.text = dataModel.title
        holder.tv_price.text = "₹ " + dataModel.price.toString()

        if (dataModel.isPrimium==1)
        {
            holder.tvpremium.visibility = View.VISIBLE
        }
        else{
            holder.tvpremium.visibility = View.GONE
        }


        if (dataModel.postImages.size > 0) {
            Glide.with(context)
                .load(dataModel.postImages.get(0).postImage).centerCrop()
                .into(holder.iv_img)
        }*/

        if(position == 0 || position == 1) {
            holder.tvpremium.visibility = View.VISIBLE
            val anim = AlphaAnimation(0.0f, 1.0f)
            anim.duration = 1000 //You can manage the blinking time with this parameter
            anim.startOffset = 20
            anim.repeatMode = Animation.REVERSE
            anim.repeatCount = Animation.INFINITE
            holder.tvpremium.startAnimation(anim)
        }
        else{
            holder.tvpremium.visibility = View.GONE
        }

    }

    override fun getItemCount(): Int {
        //return list.size
        return 6
    }

    interface ItemClickListener {
        fun itemclickProduct(bean: GetMyPostBean.getMyPostList)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {


        internal var tvpremium: TextView
        internal var tv_desc: TextView
        internal var tv_price: TextView
        internal var iv_img: ImageView

        init {

            tvpremium = view.findViewById(R.id.tv_premium)
            iv_img = view.findViewById(R.id.iv_img)
            tv_price = view.findViewById(R.id.tv_price)
            tv_desc = view.findViewById(R.id.tv_desc)

        }
    }
}