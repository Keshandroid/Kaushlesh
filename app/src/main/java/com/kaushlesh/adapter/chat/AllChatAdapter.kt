package com.kaushlesh.adapter.chat

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.kaushlesh.R
import com.kaushlesh.bean.chat.ChatNewList
import com.kaushlesh.chat.ChatMessage
import com.kaushlesh.utils.Utils
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.item_row_chat_all.view.*
import java.text.SimpleDateFormat
import java.util.*
import android.text.format.DateFormat.format as format1

class AllChatAdapter() : RecyclerView.Adapter<AllChatAdapter.ViewHolder>() {


    var list: ArrayList<ChatNewList> = ArrayList()
    private lateinit var chatMessage: ChatMessage
    var viewType1 = 1
    lateinit var context:Context
    private var itemClickListener: ItemClickListener? = null

    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }


    constructor(it: ArrayList<ChatNewList>, i: Int, context: Context)
    : this()
    {
        this.list = it
        this.viewType1 = i
        this.context=context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view: View
        Log.e("test", "==view type==" + viewType1)
        view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_row_chat_all_selection, parent, false)
        /*if (viewType1 == 1) {
            view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_row_chat_all, parent, false)
        }
        else
            view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_row_chat_all_selection, parent, false)*/
        return ViewHolder(view)

    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        //itemClickListener!!.checkedProductSold(viewHolder,list.get(position).proId,list.get(position).id)

        //delete chat automatic after 35 days
       /* val leftdays = Utils.getLeftDays(list.get(position).deleteDate)
        Log.e("test", "==left days==" + leftdays)
        if(leftdays < 0)
        {
            itemClickListener!!.itemdeleteAuto(list.get(position), "add")
        }
        else if(leftdays == 0)
        {
            val sdf = SimpleDateFormat("dd/MM/yyyy")
            val currentDate = sdf.format(Date())
            System.out.println("C DATE is  "+currentDate)
            if(currentDate.equals(list.get(position).deleteDate)) {
                itemClickListener!!.itemdeleteAuto(list.get(position), "add")
            }
            else{
                viewHolder.ivinfo.visibility = View.VISIBLE
            }
        }
        else if(leftdays <= 3)
        {
            viewHolder.ivinfo.visibility = View.VISIBLE
        }
        else{
            viewHolder.ivinfo.visibility = View.GONE
        }*/

        if(viewType1 == 1)
        {
            viewHolder.ivradio.visibility = View.GONE
        }
        else{
            viewHolder.ivradio.visibility = View.VISIBLE
        }

        val data = list.get(position)
        viewHolder.tvmsg.text = data.text

        viewHolder.tvname.text = data.name
            val requestOptions = RequestOptions().placeholder(R.drawable.ic_user_pic)
            Glide.with(context)
                .load(data.userProfile)
                .apply(requestOptions)
                .into(viewHolder.itemView.civ_profile)

        val requestOptions1 = RequestOptions().placeholder(R.drawable.bg_img_placeholder)
        Glide.with(context)
            .load(data.image)
            .apply(requestOptions1)
            .into(viewHolder.itemView.iv_product_img)
        viewHolder.itemView.tv_category.text = data.subCatName

        val d = Date()
        val cdate: CharSequence = format1("dd/MM/yyyy", d.getTime())
        val sdf = SimpleDateFormat("dd/MM/yyyy")
        val date1: Date = sdf.parse(cdate.toString())
        val date2: Date = sdf.parse(data.date)

        if(compareDate(date1, date2) == 1) {
            viewHolder.itemView.tv_time.text = data.time
        }
        else{
            viewHolder.itemView.tv_time.text = data.date
        }

        if(data.counter.length > 0 && data.counter.toInt() > 0)
        {
            viewHolder.rlcount.visibility = View.VISIBLE
            viewHolder.tvcount.text = data.counter
        }
        else{
            viewHolder.rlcount.visibility = View.GONE
        }

        viewHolder.itemView.setOnClickListener {
            if(viewType1 == 1)
            {
                itemClickListener!!.itemclick(list.get(position))
            }
            else{
                if(viewHolder.ivradio.background.getConstantState()?.equals(
                        viewHolder.ivradio.getContext().getDrawable(
                            R.drawable.ic_checked_orange
                        )?.getConstantState()
                    )!!)
                {
                    viewHolder.ivradio.setBackgroundResource(R.drawable.bg_rounded_circle)
                    itemClickListener!!.itemdelete(position, "remove")
                }
                else{
                    viewHolder.ivradio.setBackgroundResource(R.drawable.ic_checked_orange)
                    itemClickListener!!.itemdelete(position, "add")
              }
            }
        }

        viewHolder.ivinfo.setOnClickListener {

            itemClickListener!!.showInfoDialog()
        }
    }

    private fun compareDate(date1: Date, date2: Date): Int {
        if(date1.compareTo(date2) > 0)
        {
            return 0
        }
        else if(date1.compareTo(date2) < 0)
        {
          return 0
        }
        else if(date1.compareTo(date2) == 0)
        {
            return 1
        }
        return 0
    }


    override fun getItemCount(): Int {
        return list.size
    }

    interface ItemClickListener {
        fun itemclick(bean: ChatNewList)
        fun itemdelete(position: Int, type: String)
        fun itemdeleteAuto(position: ChatNewList, s: String)
        fun showInfoDialog()
        fun checkedProductSold(viewHolder: ViewHolder, proId: String, id: String)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var ivpimg: ImageView
        internal var ivprofile: CircleImageView
        internal var tvname: TextView
        internal var tvcategory: TextView
        internal var tvmsg: TextView
        internal var rlcount: RelativeLayout
        internal var tvcount: TextView
        internal var tvtime: TextView
        internal var ivradio: ImageView
        internal var ivinfo: ImageView


        init {
            ivpimg = view.findViewById(R.id.iv_product_img)
            ivprofile = view.findViewById(R.id.civ_profile)
            tvname = view.findViewById(R.id.tvname)
            tvcategory = view.findViewById(R.id.tv_category)
            tvmsg = view.findViewById(R.id.tv_msg)
            rlcount = view.findViewById(R.id.rl_count)
            tvcount = view.findViewById(R.id.tv_count)
            tvtime = view.findViewById(R.id.tv_time)
            ivradio = view.findViewById(R.id.iv_radio)
            ivinfo = view.findViewById(R.id.iv_info)
        }
    }
}