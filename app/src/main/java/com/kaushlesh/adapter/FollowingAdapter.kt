package com.kaushlesh.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kaushlesh.R
import com.kaushlesh.bean.FollowFollowingBean
import com.kaushlesh.bean.SubCategoryBean
import com.kaushlesh.utils.Utils
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.item_row_follower.view.*

class FollowingAdapter(private val list: List<FollowFollowingBean.FFlist>, private val context: Context, val type:Int) :
    RecyclerView.Adapter<FollowingAdapter.ViewHolder>() {
    private var itemClickListener: ItemClickListener? = null

    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View
        view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_row_following, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val dataModel = list[position]

        holder.tvname.text = dataModel.userName


        if(dataModel.userProfile != null && dataModel.userProfile.toString().length >0)
        {
            Glide.with(context)
                    .load(dataModel.userProfile).placeholder(R.drawable.ic_user_pic)
                    .into(holder.ivprofile)
        }

      /*  if(type == 1) {
            if(dataModel.isFollowing == 1)
            {
                holder.ivadd.setBackgroundResource(R.drawable.ic_checked_orange)
            }
            else{
                holder.ivadd.setBackgroundResource(R.drawable.ic_addpost)
            }
        }*/
        holder.itemView.setOnClickListener {
            itemClickListener!!.itemclick(dataModel)
        }

      /*  holder.ivadd.setOnClickListener {
                itemClickListener!!.itemclickaddToFollowing(dataModel)
        }*/

    }

    override fun getItemCount(): Int {
        return list.size
        //return  3
    }

    interface ItemClickListener {
        fun itemclick(bean: FollowFollowingBean.FFlist)
       // fun itemclickaddToFollowing(bean: FollowFollowingBean.FFlist)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var ivprofile: CircleImageView
        internal var tvname: TextView
        internal var ivadd: ImageView

        init {
            ivadd = view.findViewById(R.id.iv_add)
            ivprofile = view.findViewById(R.id.profile)
            tvname = view.findViewById(R.id.tvname)
        }
    }
}