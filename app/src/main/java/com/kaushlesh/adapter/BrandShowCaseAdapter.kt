package com.kaushlesh.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.R
import com.kaushlesh.bean.BrandShowCaseBean

class BrandShowCaseAdapter(
    private val list: List<BrandShowCaseBean>,
    private val context: Context
) :
    RecyclerView.Adapter<BrandShowCaseAdapter.ViewHolder>() {
    private var itemClickListener: ItemClickListener? = null

    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_row_home_showcase, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val dataModel = list[position]


        holder.tvbrandname.text = dataModel.brandname
        holder.ivimg.setImageResource(dataModel.brandimage)

        holder.itemView.setOnClickListener { itemClickListener!!.itemclick(dataModel) }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    interface ItemClickListener {
        fun itemclick(bean: BrandShowCaseBean)

    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var ivimg: ImageView
        internal var tvbrandname: TextView

        init {
            ivimg = view.findViewById(R.id.iv_img)
            tvbrandname = view.findViewById(R.id.tv_brandname)
        }
    }
}


