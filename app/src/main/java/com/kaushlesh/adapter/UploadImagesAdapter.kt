package com.kaushlesh.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.R
import com.kaushlesh.bean.ImagesBean
import com.kaushlesh.utils.ItemMoveCallbackListener
import java.util.*


class UploadImagesAdapter(
        private val list: List<ImagesBean>,
        private val context: Context,
        private val startDragListener: OnStartDragListener
) :
        RecyclerView.Adapter<UploadImagesAdapter.ViewHolder>(), ItemMoveCallbackListener.Listener {
    private var itemClickListener: ItemClickListener? = null
    var isMoving = false

    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_row_image, parent, false)

        return ViewHolder(view)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val dataModel = list[position]

        if (position == 0) {
            holder.tvcoverimg.visibility = View.VISIBLE
        } else {
            holder.tvcoverimg.visibility = View.GONE
        }

        if (dataModel.getImg() != null) {
            holder.ivimg.setImageBitmap(dataModel.getImg() as Bitmap?)

            /* holder.ivimg.setOnDragListener { view, dragEvent ->
                 if (dragEvent.action == MotionEvent.ACTION_MOVE) {
                     startDragListener.onStartDrag(holder)
                 }
                 return@setOnDragListener true
             }*/

            holder.ivimg.setOnTouchListener(View.OnTouchListener { v, event ->
                if (event.action == MotionEvent.ACTION_MOVE) {
                    isMoving = true
                    Log.i("isMoving:", "true")
                }
                false
            })

            holder.ivimg.setOnLongClickListener(View.OnLongClickListener {
                if (isMoving) {
                    startDragListener.onStartDrag(holder)
                }
                false
            })
            /* holder.ivimg.setOnTouchListener { _, event ->
                 if (event.action == MotionEvent.ACTION_MOVE) {
                     startDragListener.onStartDrag(holder)
                 }
                 return@setOnTouchListener true
             }
 */

            holder.ivremove.setOnClickListener {
                Log.e("removeposion", "itemclick: " + position)
                itemClickListener!!.itemremoveclick(position)

            }

        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    interface ItemClickListener {
        fun itemclick(bean: ImagesBean)
        fun itemremoveclick(position: Int)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var ivimg: ImageView
        internal var ivremove: ImageView
        internal var tvcoverimg: TextView
        var rowView: View? = view

        init {
            rowView = view;
            ivimg = view.findViewById(R.id.iv_image)
            ivremove = view.findViewById(R.id.iv_remove)
            tvcoverimg = view.findViewById(R.id.tv_coverimg)
        }


    }

    override fun onRowMoved(fromPosition: Int, toPosition: Int) {
        if (fromPosition < toPosition) {
            for (i in fromPosition until toPosition) {
                Collections.swap(list, i, i + 1)
            }
        } else {
            for (i in fromPosition downTo toPosition + 1) {
                Collections.swap(list, i, i - 1)
            }
        }
        //notifyDataSetChanged()
        notifyItemMoved(fromPosition, toPosition)

    }

    override fun onRowSelected(itemViewHolder: ViewHolder) {
        itemViewHolder.ivimg.setBackgroundColor(Color.GRAY);
    }

    override fun onRowClear(itemViewHolder: ViewHolder) {
        itemViewHolder.ivimg.setBackgroundColor(Color.WHITE);
    }

    interface OnStartDragListener {

        fun onStartDrag(viewHolder: RecyclerView.ViewHolder)

    }
}
