package com.kaushlesh.adapter.AdShowcase

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Spinner
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.R

class JewellreyFillIndividualCategoryAdapter(private val list: ArrayList<Int>, private val context: Context) :
    RecyclerView.Adapter<JewellreyFillIndividualCategoryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_jewellery_individual_category, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val a = getItemCount() - 1

        if (a == position) {
            setEnableDisable(holder, true)
        } else {
            setEnableDisable(holder, false)
        }

        holder.ll_delete.setOnClickListener {
            list.removeAt(position)
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, list.size);
            notifyDataSetChanged();
        }

        holder.tv_edit.setOnClickListener {
            setEnableDisable(holder, true)
        }

        holder.tv_update.setOnClickListener {
            setEnableDisable(holder, false)
        }
    }

    private fun setEnableDisable(
        holder: ViewHolder,
        b: Boolean)

    {
        holder.llcurd.setBackgroundColor(context.getResources().getColor(R.color.white));
        if (b) {
            holder.ll_fist.setBackgroundColor(context.getResources().getColor(R.color.white));

            holder.tv_title_about_project.setTextColor(
                context.getResources().getColor(R.color.black)
            )
            holder.tv_upload.setTextColor(context.getResources().getColor(R.color.black))
            holder.tv_title_super_build.setTextColor(context.getResources().getColor(R.color.black))
            holder.tv_title_carpet_area.setTextColor(context.getResources().getColor(R.color.black))
            holder.tv_title_washroom.setTextColor(context.getResources().getColor(R.color.black))
            holder.tv_title_price.setTextColor(context.getResources().getColor(R.color.black))
        } else {
            holder.ll_fist.setBackgroundColor(context.getResources().getColor(R.color.greylight2));

            holder.tv_title_about_project.setTextColor(
                context.getResources().getColor(R.color.gray)
            )
            holder.tv_upload.setTextColor(context.getResources().getColor(R.color.gray))
            holder.tv_title_super_build.setTextColor(context.getResources().getColor(R.color.gray))
            holder.tv_title_carpet_area.setTextColor(context.getResources().getColor(R.color.gray))
            holder.tv_title_washroom.setTextColor(context.getResources().getColor(R.color.gray))
            holder.tv_title_price.setTextColor(context.getResources().getColor(R.color.gray))

        }
        setEnable(b, holder.ll_fist)
        setEnable(b, holder.et_super_builtup_area)
        setEnable(b, holder.tv_brand_type)
        setEnable(b, holder.et_carpet_area)
        setEnable(b, holder.spinner_about_project)
        setEnable(b, holder.tv_upload)
        setEnable(b, holder.et_washroom)
        setEnable(b, holder.et_price)
    }

    private fun setEnable(b: Boolean, et: View) {
        et.isEnabled = b
    }

    override fun getItemCount(): Int {
        return list.size
    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        internal var ll_delete: LinearLayout
        internal var ll_fist: LinearLayout
        internal var llcurd: LinearLayout
        internal var tv_edit: TextView
        internal var tv_update: TextView
        internal var et_carpet_area: EditText
        internal var tv_brand_type: TextView
        internal var tv_title_about_project: TextView
        internal var et_super_builtup_area: EditText
        internal var et_washroom: EditText
        internal var et_price: EditText
        internal var tv_upload: TextView
        internal var tv_title_super_build: TextView
        internal var tv_title_carpet_area: TextView
        internal var tv_title_washroom: TextView
        internal var tv_title_price: TextView
        internal var spinner_about_project: Spinner

        init {
            ll_delete = view.findViewById(R.id.ll_delete)
            ll_fist = view.findViewById(R.id.ll_fist)
            tv_update = view.findViewById(R.id.tv_update)
            tv_edit = view.findViewById(R.id.tv_edit)
            et_carpet_area = view.findViewById(R.id.et_carpet_area)
            et_washroom = view.findViewById(R.id.et_washroom)
            et_price = view.findViewById(R.id.et_price)
            spinner_about_project = view.findViewById(R.id.spinner_about_project)
            tv_upload = view.findViewById(R.id.tv_upload)
            tv_brand_type = view.findViewById(R.id.tv_brand_type)
            et_super_builtup_area = view.findViewById(R.id.et_super_builtup_area)
            llcurd = view.findViewById(R.id.llcurd)

            tv_title_about_project = view.findViewById(R.id.tv_title_about_project)
            tv_title_super_build = view.findViewById(R.id.tv_title_super_build)
            tv_title_carpet_area = view.findViewById(R.id.tv_title_carpet_area)
            tv_title_washroom = view.findViewById(R.id.tv_title_washroom)
            tv_title_price = view.findViewById(R.id.tv_title_price)
        }
    }
}
