package com.kaushlesh.adapter.AdShowcase

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Spinner
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.R

class CarFillIndividualCategoryAdapter (private val list: ArrayList<Int>, private val context: Context) :
    RecyclerView.Adapter<CarFillIndividualCategoryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_carshowroom_individual_categoery_about_car, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val a = getItemCount() - 1

        if (a == position) {
            setEnableDisable(holder, true)
        } else {
            setEnableDisable(holder, false)
        }

        holder.ll_delete.setOnClickListener {
            list.removeAt(position)
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, list.size);
            notifyDataSetChanged();
        }

        holder.tv_edit.setOnClickListener {
            setEnableDisable(holder, true)
        }

        holder.tv_update.setOnClickListener {
            setEnableDisable(holder, false)
        }
    }

    private fun setEnableDisable(
        holder: ViewHolder,
        b: Boolean)

    {
        holder.llcurd.setBackgroundColor(context.getResources().getColor(R.color.white));
        if (b) {
            holder.ll_fist.setBackgroundColor(context.getResources().getColor(R.color.white));
            holder.tv_title_car_model.setTextColor(context.getResources().getColor(R.color.black))
            holder.tv_upload.setTextColor(context.getResources().getColor(R.color.black))
            holder.tv_upload.setTextColor(context.getResources().getColor(R.color.black))
            holder.tv_title_engine_capacity.setTextColor(context.getResources().getColor(R.color.black))
            holder.tv_title_fuel.setTextColor(context.getResources().getColor(R.color.black))
            holder.tvb1.setTextColor(context.getResources().getColor(R.color.black))
            holder.tvb2.setTextColor(context.getResources().getColor(R.color.black))
            holder.tvb3.setTextColor(context.getResources().getColor(R.color.black))
            holder.tvb4.setTextColor(context.getResources().getColor(R.color.black))
            holder.auto.setTextColor(context.getResources().getColor(R.color.black))
            holder.normal.setTextColor(context.getResources().getColor(R.color.black))

        } else {
            holder.ll_fist.setBackgroundColor(context.getResources().getColor(R.color.greylight2));
            holder.tv_title_car_model.setTextColor(context.getResources().getColor(R.color.gray))
            holder.tv_upload.setTextColor(context.getResources().getColor(R.color.gray))
            holder.tv_upload.setTextColor(context.getResources().getColor(R.color.gray))
            holder.tv_title_engine_capacity.setTextColor(context.getResources().getColor(R.color.gray))
            holder.tv_title_fuel.setTextColor(context.getResources().getColor(R.color.gray))
            holder.tvb1.setTextColor(context.getResources().getColor(R.color.gray))
            holder.tvb2.setTextColor(context.getResources().getColor(R.color.gray))
            holder.tvb3.setTextColor(context.getResources().getColor(R.color.gray))
            holder.tvb4.setTextColor(context.getResources().getColor(R.color.gray))
            holder.auto.setTextColor(context.getResources().getColor(R.color.gray))
            holder.normal.setTextColor(context.getResources().getColor(R.color.gray))
        }

        setEnable(b, holder.ll_fist)
        setEnable(b, holder.tvb1)
        setEnable(b, holder.tvb2)
        setEnable(b, holder.tvb3)
        setEnable(b, holder.tvb4)
        setEnable(b, holder.tv_upload)
        setEnable(b, holder.normal)
        setEnable(b, holder.spinner_car_model)
        setEnable(b, holder.et_milage)
        setEnable(b, holder.et_discount_rs)
        setEnable(b, holder.et_road_price)
        setEnable(b, holder.et_desc_model)
        setEnable(b, holder.et_engine)
    }

    private fun setEnable(b: Boolean, et: View) {
        et.isEnabled = b
    }

    override fun getItemCount(): Int {
        return list.size
    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        internal var ll_fist: LinearLayout
        internal var tv_title_car_model: TextView
        internal var tv_upload: TextView
        internal var tv_title_engine_capacity: TextView
        internal var tv_update: TextView
        internal var tv_edit: TextView
        internal var tv_title_fuel: TextView
        internal var tvb1: TextView
        internal var tvb2: TextView
        internal var tvb3: TextView
        internal var tvb4: TextView
        internal var auto: TextView
        internal var normal: TextView
        internal var spinner_car_model: Spinner
        internal var et_milage: EditText
        internal var et_showroom_price: EditText
        internal var et_discount_rs: EditText
        internal var et_road_price: EditText
        internal var et_desc_model: EditText
        internal var et_engine: EditText
        internal var llcurd: LinearLayout
        internal var ll_delete: LinearLayout


        init {
            tv_title_car_model = view.findViewById(R.id.tv_title_car_model)
            ll_delete = view.findViewById(R.id.ll_delete)
            llcurd = view.findViewById(R.id.ll_curd)
            ll_fist = view.findViewById(R.id.ll_fist)
            tv_update = view.findViewById(R.id.tv_update)
            tv_edit = view.findViewById(R.id.tv_edit)
            tv_upload = view.findViewById(R.id.tv_upload)
            tv_title_engine_capacity = view.findViewById(R.id.tv_title_engine_capacity)
            tv_title_fuel = view.findViewById(R.id.tv_title_fuel)
            tvb1 = view.findViewById(R.id.tvb1)
            tvb2 = view.findViewById(R.id.tvb2)
            tvb3 = view.findViewById(R.id.tvb3)
            tvb4 = view.findViewById(R.id.tvb4)
            normal = view.findViewById(R.id.normal)
            auto = view.findViewById(R.id.auto)
            spinner_car_model = view.findViewById(R.id.spinner_car_model)
            et_engine = view.findViewById(R.id.et_engine)
            et_milage = view.findViewById(R.id.et_milage)
            et_showroom_price = view.findViewById(R.id.et_showroom_price)
            et_discount_rs = view.findViewById(R.id.et_showroom_price)
            et_road_price = view.findViewById(R.id.et_showroom_price)
            et_desc_model = view.findViewById(R.id.et_showroom_price)
        }
    }
}
