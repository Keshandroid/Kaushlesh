package com.kaushlesh.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.kaushlesh.R
import com.kaushlesh.bean.BrandShowCaseBean
import com.kaushlesh.bean.CategoryBean

class CategoryAdapterNew() :
    RecyclerView.Adapter<CategoryAdapterNew.ViewHolder>() {
    private var itemClickListener: ItemClickListener? = null
    var viewType1 = 2
   lateinit var context: Context
    var list:  List<CategoryBean.Category> = ArrayList()

    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    constructor(arrayList:  List<CategoryBean.Category>, context: Context, viewType: Int) : this() {
        this.list = arrayList
        this.viewType1 = viewType
        this.context = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        var view: View
        Log.e("test","==view type==" + viewType1)
        if (viewType1 == 2) {
            view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_row_category, parent, false)
        }

        else
            view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_row_category_two, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val dataModel = list[position]


        holder.tvcategoryname.text = dataModel.categoryName
     //   holder.ivimg.setImageResource(dataModel.categoryImage)

                Glide.with(context)
                    .load(dataModel.categoryImage)
                    .placeholder(R.drawable.progress_animated)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .skipMemoryCache(true)
                .into(holder.ivimg)


        holder.itemView.setOnClickListener { itemClickListener!!.itemclickCategory(dataModel) }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setCategoryItems(categoryList: java.util.ArrayList<CategoryBean.Category>) {
        this.list = categoryList
        notifyDataSetChanged()
    }

    interface ItemClickListener {
        fun itemclickCategory(bean: CategoryBean.Category)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var ivimg: ImageView
        internal var tvcategoryname: TextView

        init {
            ivimg = view.findViewById(R.id.img_category)
            tvcategoryname = view.findViewById(R.id.tv_categoryname)
        }
    }
}

