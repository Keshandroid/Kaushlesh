package com.kaushlesh.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.R
import com.kaushlesh.bean.MyOrdersBean
import com.kaushlesh.bean.MyPackageOrderBean

class MyOrdersAdapter(private val list: List<MyPackageOrderBean.MyPackageOrderList>, private val context: Context) :
    RecyclerView.Adapter<MyOrdersAdapter.ViewHolder>() {
    private var itemClickListener: ItemClickListener? = null

    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_row_my_orders, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val dataModel = list[position]

        holder.tvtitle1.text = dataModel.name
        holder.tvcategory.text = dataModel.categoryName
        holder.tvlocation.text = dataModel.locationName
        holder.tvactivatedate.text = dataModel.activation_date
        holder.tvorderid.text = dataModel.order_id
        holder.tvpurchased.text = dataModel.purchasedPost
        holder.tvavailable.text = dataModel.avilablePost.toString()
        holder.tvused.text = dataModel.usedPost.toString()

        //old logic
        if(dataModel.isexpired == 1)
        {
            holder.tvexpdate.text = dataModel.expiry_date
            holder.ivdelete.visibility = View.VISIBLE
            holder.tvexpired.visibility = View.VISIBLE
        } else {

            if (dataModel.isSingleAd!!.toInt() == 1) {
                holder.tvexpdate.text = "-"
                holder.ivdelete.visibility = View.VISIBLE
                holder.tvexpired.visibility = View.VISIBLE
            } else {
                holder.ivdelete.visibility = View.GONE
                holder.tvexpired.visibility = View.GONE
                holder.tvexpdate.text = dataModel.expiry_date
            }
        }


        //<<<new date logic start>>>
        /*if(dataModel.isexpired == 1)
        {
            //holder.tvexpdate.text = dataModel.expiry_date
            holder.ivdelete.visibility = View.VISIBLE
            holder.tvexpired.visibility = View.VISIBLE
        } else {

            holder.ivdelete.visibility = View.GONE
            holder.tvexpired.visibility = View.GONE
            //holder.tvexpdate.text = dataModel.expiry_date
        }

        if (dataModel.isSingleAd!!.toInt() == 1) {
            holder.tvexpdate.text = "-"
            holder.ivdelete.visibility = View.VISIBLE
            holder.tvexpired.visibility = View.VISIBLE
        } else {
            holder.tvexpdate.text = dataModel.expiry_date
            holder.ivdelete.visibility = View.GONE
            holder.tvexpired.visibility = View.GONE
        }*/
        //<<<new date logic end>>>



        if(dataModel.isPremium!!.toInt() == 1)
        {
            holder.tvtitle2.visibility = View.VISIBLE
            holder.tvtitle2.text = context.getString(R.string.txt_reach_upto_ten_more)
        }
        else{
            holder.tvtitle2.visibility = View.GONE
        }

        holder.ivdelete.setOnClickListener {
            itemClickListener!!.itemclickDelete(position)
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    interface ItemClickListener {
        fun itemclick(bean: MyPackageOrderBean.MyPackageOrderList)
        fun itemclickDelete(position: Int)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var tvtitle1: TextView
        internal var tvtitle2: TextView
        internal var tvcategory: TextView
        internal var tvlocation: TextView
        internal var tvactivatedate: TextView
        internal var tvexpdate: TextView
        internal var tvorderid: TextView
        internal var tvpurchased: TextView
        internal var tvavailable: TextView
        internal var tvused: TextView
        internal var ivdelete: ImageView
        internal var tvexpired: TextView


        init {
            tvtitle1 = view.findViewById(R.id.tv_title1)
            tvtitle2 = view.findViewById(R.id.tv_title2)
            tvcategory = view.findViewById(R.id.tv_category)
            tvlocation = view.findViewById(R.id.tv_location)
            tvactivatedate = view.findViewById(R.id.tv_date_act)
            tvexpdate = view.findViewById(R.id.tv_date_exp)
            tvorderid = view.findViewById(R.id.tv_orderid)
            tvpurchased = view.findViewById(R.id.tvpuchase)
            tvavailable = view.findViewById(R.id.tvavailable)
            tvused = view.findViewById(R.id.tvused)
            ivdelete = view.findViewById(R.id.iv_delete)
            tvexpired = view.findViewById(R.id.tvexpired)
        }
    }
}




