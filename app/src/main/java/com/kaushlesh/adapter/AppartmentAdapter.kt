package com.kaushlesh.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.R
import com.kaushlesh.bean.BrandShowCaseBean

class AppartmentAdapter(private val list: List<BrandShowCaseBean>, private val context: Context) :
    RecyclerView.Adapter<AppartmentAdapter.ViewHolder>() {
    private var itemClickListener: ItemClickListener? = null

    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_appartment, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val dataModel = list[position]
       /* holder.tvcategoryname.text = dataModel.brandname
        holder.ivimg.setImageResource(dataModel.brandimage)*/
        holder.itemView.setOnClickListener { itemClickListener!!.itemclickCategory(dataModel) }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    interface ItemClickListener {
        fun itemclickCategory(bean: BrandShowCaseBean)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    }
}

