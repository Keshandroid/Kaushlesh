package com.kaushlesh.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.R
import com.kaushlesh.bean.GetMyPackagesBean
import com.kaushlesh.constant.AppConstants.printLog
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.Utils
import java.util.ArrayList

class AdPostSelectPackageAdapter(
    internal var list: ArrayList<GetMyPackagesBean.Packages> = ArrayList(),
    private val context: Context
) :
    RecyclerView.Adapter<AdPostSelectPackageAdapter.ViewHolder>() {
    private var itemClickListener: ItemClickListener? = null

    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_row_ad_post_package, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val dataModel = list[position]
        //holder.tv_details.text = dataModel.noofAd + " Ads for " + dataModel.duration + " days"
        holder.tv_details.text = dataModel.name
        holder.tvpurchased.text = dataModel.noofAd
        holder.tvused.text = dataModel.remaiingpost
        holder.tvavailable.text = (dataModel.noofAd!!.toInt() - dataModel.remaiingpost!!.toInt()).toString()

        val catname = Constants.getCategoryFromId( dataModel.categoryid!!.toInt(),context)
        holder.tvcategory.text = catname

        val date_after = Utils.formateDateFromstring("dd-MM-yyyy", "dd/MM/yyyy", dataModel.expdate.toString())
        printLog("TAG", "==FORMATED DATE===$date_after")
        //holder.tvexpdate.text = dataModel.expdate.toString()
        holder.tvexpdate.text = date_after.toString()

        if (dataModel.selected == true) {
            holder.iv_radio1.setBackgroundResource(R.drawable.ic_radio_select)

        }else{
            holder.iv_radio1.setBackgroundResource(R.drawable.bg_rounded_circle)
        }

        holder.itemView.setOnClickListener {

            dataModel.selected = !dataModel.selected!!

            Log.d("ExtraAd0"," : "+ dataModel.selected)

            itemClickListener!!.itemClicked(position, dataModel.selected!!)

        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    interface ItemClickListener {

        fun itemClicked(pos:Int,selected:Boolean)
    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var iv_radio1: ImageView
        internal var tv_details: TextView
        internal var tvpurchased: TextView
        internal var tvused: TextView
        internal var tvavailable: TextView
        internal var tvcategory: TextView
        internal var tvexpdate: TextView
        init {
            iv_radio1 = view.findViewById(R.id.iv_radio1)
            tv_details = view.findViewById(R.id.tv_details)
            tvpurchased = view.findViewById(R.id.tvpuchase)
            tvused = view.findViewById(R.id.tvused)
            tvavailable = view.findViewById(R.id.tvavailable)
            tvcategory = view.findViewById(R.id.tv_category)
            tvexpdate = view.findViewById(R.id.tv_date_exp)

        }
    }
}