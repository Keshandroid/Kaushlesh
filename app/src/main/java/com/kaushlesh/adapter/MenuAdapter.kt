package com.kaushlesh.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.R
import java.util.*

class MenuAdapter(private val menuList: ArrayList<String>?,
                  private val listener: OnMenuItemClickListener) : RecyclerView.Adapter<MenuAdapter.DataObjectHolder>() {
    class DataObjectHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var label: TextView = itemView.findViewById(R.id.label)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): DataObjectHolder {
        val view: View = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.menu_spinner, viewGroup, false)
        return DataObjectHolder(view)
    }

    override fun onBindViewHolder(holder: DataObjectHolder, position: Int) {
        holder.label.text = menuList!![position]
        holder.label.setOnClickListener { listener.onMenuItemClicked(menuList[holder.adapterPosition]) }
    }

    override fun getItemCount(): Int {
        return menuList?.size ?: 0
    }

    interface OnMenuItemClickListener {
        fun onMenuItemClicked(item: String?)
    }
}