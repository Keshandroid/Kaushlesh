package com.kaushlesh.adapter

import android.content.Context
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.kaushlesh.view.fragment.ShowcaseDetail.AppartmentsFragment

class MyAdapter(private val myContext: Context, fm: FragmentManager, internal var totalTabs: Int) : FragmentPagerAdapter(fm) {

    // this is for fragment tabs

    override fun getItem(position: Int): AppartmentsFragment {
        when (position) {
            0 -> return AppartmentsFragment()
            else -> return AppartmentsFragment()
        }
    }

    // this counts total number of tabs
    override fun getCount(): Int {
        return totalTabs
    }
}