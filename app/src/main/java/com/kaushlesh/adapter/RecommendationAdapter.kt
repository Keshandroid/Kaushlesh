package com.kaushlesh.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.widget.CompoundButton
import android.widget.ImageView
import android.widget.TextView
import android.widget.ToggleButton
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.kaushlesh.R
import com.kaushlesh.bean.PostImage
import com.kaushlesh.bean.RecommendationBean
import com.kaushlesh.utils.PostDetail.SetPostDetailData
import com.kaushlesh.utils.Utils

class RecommendationAdapter(
        var list: ArrayList<RecommendationBean.FreshProductListBean>,
        private val context: Context
) :
    RecyclerView.Adapter<RecommendationAdapter.ViewHolder>() {
    private var itemClickListener: ItemClickListener? = null

    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_row_fresh_recommendation, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val dataModel = list[position]

        holder.itemView.setTag("ŖcvChildItem : ${position}")

        //holder.ivimg.setImageResource(dataModel.image)

         if (dataModel.ispremium == 1) {
            holder.tvfeatured.visibility = View.VISIBLE
            /* val anim = AlphaAnimation(0.0f, 1.0f)
             anim.duration = 1000 //You can manage the blinking time with this parameter
             anim.startOffset = 20
             anim.repeatMode = Animation.REVERSE
             anim.repeatCount = Animation.INFINITE
             holder.tvfeatured.startAnimation(anim)*/
        } else {
            holder.tvfeatured.visibility = View.GONE
        }

        val bean2 = PostImage()
        for (i in 0 until dataModel.postImages.size) {
            bean2.postImage = dataModel.postImages[i].postImage
            Glide.with(context)
                    .load(bean2.postImage)
                    .placeholder(R.drawable.progress_animated)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .skipMemoryCache(true)
                    .into(holder.ivimg)
        }

        if(dataModel.price!! == 0)
        {
            holder.tvprice.visibility = View.VISIBLE
            holder.tvprice.text = context.getString(R.string.txt_no_price)
        }
        else{
            holder.tvprice.visibility = View.VISIBLE
            holder.tvprice.text = "₹ " + dataModel.price
        }

        holder.tvtitle.text = dataModel.title
        holder.tvdesc.text = dataModel.other_information
        holder.tvaddress.text = dataModel.address

        if(dataModel.isFavorite == 1) {
            holder.ivlike.isChecked = true
           // holder.ivlike.setBackgroundResource(R.drawable.ic_favorite_filled_black)
        } else{
            holder.ivlike.isChecked = false
            //holder.ivlike.setBackgroundResource(R.drawable.ic_fav_black)
        }

        holder.itemView.setOnClickListener {
            itemClickListener!!.itemclickRecommendation(dataModel)
        }

        //favourite toggle button
        holder.ivlike.setOnCheckedChangeListener( object: CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                itemClickListener!!.itemFavUnfavourite(dataModel,position)
                /*if (buttonView!!.isPressed()) {
                    if (isChecked) {

                    }
                }*/
            }
        })

        /*holder.ivlike.setOnClickListener {

            itemClickListener!!.itemFavUnfavourite(dataModel,position)

            if(holder.ivlike.background.getConstantState()?.equals(context.getDrawable(R.drawable.ic_favorite_filled_black)?.getConstantState())!!)
           {
               SetPostDetailData.setFav(holder.ivlike,0)
           }
           else{
               SetPostDetailData.setFav(holder.ivlike,1)
           }
        }*/

    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setItems(items: ArrayList<RecommendationBean.FreshProductListBean>) {
        this.list = items
        notifyDataSetChanged()
    }

    fun setHomeItems(freshrecomlist: java.util.ArrayList<RecommendationBean.FreshProductListBean>) {
        this.list = freshrecomlist
        notifyDataSetChanged()
    }

    interface ItemClickListener {
        fun itemclickRecommendation(bean: RecommendationBean.FreshProductListBean)
        fun itemFavUnfavourite(dataModel: RecommendationBean.FreshProductListBean, i: Int)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var ivimg: ImageView
        internal var tvfeatured: TextView
        internal var tvprice: TextView
        internal var tvdesc: TextView
        internal var tvaddress: TextView
        //internal var ivlike: ImageView
        internal var ivlike: ToggleButton
        internal var tvtitle: TextView

        init {
            ivimg = view.findViewById(R.id.iv_img)
            tvfeatured = view.findViewById(R.id.tv_title)
            tvprice = view.findViewById(R.id.tv_price)
            tvdesc = view.findViewById(R.id.tv_desc)
            tvaddress = view.findViewById(R.id.tv_address)
            ivlike = view.findViewById(R.id.iv_like)
            tvtitle = view.findViewById(R.id.tvtitle)
        }
    }
}

