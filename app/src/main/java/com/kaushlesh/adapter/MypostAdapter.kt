package com.kaushlesh.adapter

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.facebook.appevents.ml.Utils
import com.kaushlesh.R
import com.kaushlesh.bean.GetMyPostBean
import com.kaushlesh.bean.RecommendationBean
import com.kaushlesh.view.fragment.MyAccount.MyAds.KnowMoreDetailPostActivity
import com.kaushlesh.widgets.CustomImageView
import java.net.URLDecoder
import java.net.URLEncoder
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class MypostAdapter(): RecyclerView.Adapter<MypostAdapter.ViewHolder>() {


    // var list: ArrayList<ProductDataBean> = ArrayList()
    var list: ArrayList<GetMyPostBean.getMyPostList> = ArrayList()
    var viewType1 = 1
    lateinit var context: Context
    lateinit var type: String

    private var itemClickListener: ItemClickListener? = null
    //var items = arrayListOf<GetMyPostBean.getMyPostList>()
    var items: ArrayList<GetMyPostBean.getMyPostList> = ArrayList()

    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    constructor(context: Context, list: ArrayList<GetMyPostBean.getMyPostList>, type: String) : this() {

        this.context = context
        //this.list = list
        this.items = list
        this.type = type
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view: View

        view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_my_post, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if(position != RecyclerView.NO_POSITION) {
            // Do your binding here
            val dataModel = items[position]
            if (dataModel.category_id == 7 || dataModel.category_id == 10 || dataModel.category_id == 11 || dataModel.price == 0) {
                holder.tvprice.visibility = View.GONE
                holder.lledit.visibility = View.GONE
            } else {
                holder.tvprice.visibility = View.VISIBLE
                holder.lledit.visibility = View.VISIBLE
            }
            //0 pending //1 live 2//reject 3//expire
            holder.tvname.text = dataModel.title
            // holder.tvname.text = URLDecoder.decode(dataModel.title,"UTF-8")
            holder.tvprice.text = "₹ " + dataModel.price.toString()
            holder.tvlike.text = context.getString(R.string.txt_likes) + " " + dataModel.likeCount.toString()
            holder.tvview.text = context.getString(R.string.txt_view) + " " + dataModel.viewCount.toString()
            //holder.tvdate.text = list[position].published_date.toString() + " - " + list[position].expire_date.toString()


            //logic of display bigger date from expire and premimum

            if (dataModel.likeCount!!.toInt() > 0) {
                //holder.ivlike.setColorFilter(ContextCompat.getColor(context, R.color.black), android.graphics.PorterDuff.Mode.MULTIPLY);
                holder.ivlike.setBackgroundResource(R.drawable.ic_favorite_filled_black)
            }

            if (dataModel.post_status == 0) {
                holder.tvdate.text = dataModel.published_date.toString()
            } else {
                if (dataModel.premium_expire_date != null && dataModel.premium_expire_date.toString().length > 0) {

                    if (compareDate(dataModel.expire_date.toString(), dataModel.premium_expire_date.toString()) == 1) {
                        holder.tvdate.text = dataModel.published_date.toString() + " - " + dataModel.expire_date.toString()
                    } else {
                        holder.tvdate.text = dataModel.published_date.toString() + " - " + dataModel.premium_expire_date.toString()
                    }
                } else {
                    holder.tvdate.text = dataModel.published_date.toString() + " - " + dataModel.expire_date.toString()
                }
            }

            if (dataModel.mainPostImg != null && dataModel.mainPostImg.toString().length > 0) {
                Glide.with(context)
                        .load(dataModel.mainPostImg)
                        //.placeholder(R.drawable.bg_img_placeholder)
                        .placeholder(R.drawable.progress_animated_small)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .skipMemoryCache(true)
                        .into(holder.ivimage)
            }

            setDataMyPost(holder, position)

            holder.itemView.setOnClickListener {
                itemClickListener!!.itemclickProduct(dataModel, "preview", position)
            }

            holder.tvsellurgent.setOnClickListener {
                itemClickListener!!.itemclickProduct(dataModel, "sellurgent", position)
                /*   val intent = Intent(context, SellUrgentDetailActivity::class.java)
            context.startActivity(intent)*/
            }

            holder.tvknowmore.setOnClickListener {
                //itemClickListener!!.itemclickProduct(dataModel)

                val intent = Intent(context, KnowMoreDetailPostActivity::class.java)
                intent.putExtra("postId", dataModel.post_id.toString())
                context.startActivity(intent)
            }

            holder.tvliveagain.setOnClickListener {
                itemClickListener!!.itemclickLiveAgain(dataModel)
            }

            holder.ivdelete.setOnClickListener {
                itemClickListener!!.itemclickDelete(dataModel, position)
            }

            holder.lledit.setOnClickListener {
                itemClickListener!!.itemclickProduct(dataModel, "editPrice", position)
            }
        }
    }

    private fun compareDate(d11: String, d22: String): Int {

        // Create SimpleDateFormat object
        val sdfo = SimpleDateFormat("dd-MM-yyyy")

        // Get the two dates to be compared
        val d1: Date = sdfo.parse(d11)
        val d2: Date = sdfo.parse(d22)

        // Print the dates
        System.out.println("Date1 : " + sdfo.format(d1))
        System.out.println("Date2 : " + sdfo.format(d2))

        // Compare the dates
        if (d1.after(d2)) {
            System.out.println("Date1 after")
          return 1
        } else if (d1.before(d2)) {
            System.out.println("Date1 before")
         return 0
        } else if (d1.equals(d2)) {
            System.out.println("Date1 equal")
           return 1
        }
        return 0
    }

    private fun setDataMyPost(holder: ViewHolder, position: Int) {
        if (items[position].post_status==1) {
            if(items[position].isPrimium == 1)
            {
                setdata(holder, View.VISIBLE, View.GONE, View.GONE, View.GONE, View.GONE, View.VISIBLE, View.GONE, View.GONE)
            }
            else{
                setdata(holder, View.VISIBLE, View.GONE, View.GONE, View.VISIBLE, View.GONE, View.GONE, View.GONE, View.GONE)
            }
            holder.tvdesc.setText(context.getString(R.string.txt_ad_live))
        } else if (items[position].post_status==2) {
            setdata(holder, View.GONE, View.VISIBLE, View.GONE, View.GONE, View.VISIBLE, View.GONE, View.GONE, View.GONE)
            holder.tvdesc.setText(context.getString(R.string.txt_ad_rejected))
        }
        else if (items[position].post_status==3) {
            setdata(holder, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.VISIBLE, View.VISIBLE)
            holder.tvdesc.setText(context.getString(R.string.txt_ad_expired))
        } else {
            setdata(holder, View.GONE, View.GONE, View.VISIBLE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE)
            holder.tvdesc.setText(context.getString(R.string.txt_ad_pending))
        }
    }

    private fun setdata(holder: ViewHolder, p1: Int, p2: Int, p3: Int, p4: Int, p5: Int, p6: Int, p7: Int, p8: Int) {
        holder.tvlive.visibility = p1
        holder.tvreject.visibility = p2
        holder.tvpending.visibility = p3
        holder.tvsellurgent.visibility = p4
        holder.tvknowmore.visibility = p5
        holder.tvpremium.visibility = p6
        holder.tvexpired.visibility = p7
        holder.tvliveagain.visibility = p8
    }

    override fun getItemCount(): Int {
        //return list.size
        return items.size
    }

    /* fun addItems(list: java.util.ArrayList<GetMyPostBean.getMyPostList>) {
        val oldSize = itemCount
        items.addAll(list)
        notifyItemRangeInserted(oldSize,list.size)

    }*/

    fun addItems(list: java.util.ArrayList<GetMyPostBean.getMyPostList>) {
     /*   items.addAll(list)
        //notifyItemInserted(items.size - 1);
        notifyDataSetChanged()*/
        val oldSize = itemCount
        val distinct = list.distinctBy { it.post_id }
        items.addAll(distinct)
        notifyItemRangeInserted(oldSize, list.size)
    }

    fun updateItems(pos: Int) {

    }

    fun removeItems() {
        items.clear()
        notifyDataSetChanged()
    }

    interface ItemClickListener {
        fun itemclickProduct(bean: GetMyPostBean.getMyPostList, type: String,position: Int)
        fun itemclickDelete(dataModel: GetMyPostBean.getMyPostList,position: Int)
        fun itemclickLiveAgain(dataModel: GetMyPostBean.getMyPostList)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        internal var tvsellurgent: TextView
        internal var tvknowmore: TextView
        internal var tvlive: TextView
        internal var tvreject: TextView
        internal var tvpending: TextView
        internal var tvexpired: TextView
        internal var tvdesc: TextView
        internal var tvname: TextView
        internal var tvprice: TextView
        internal var tvview: TextView
        internal var tvlike: TextView
        internal var tvdate: TextView
        internal var ivdelete: ImageView
        internal var ivimage: ImageView
        internal var tvpremium: TextView
        internal var ivedit: ImageView
        internal var lledit : LinearLayout
        internal var tvliveagain: TextView
        internal var ivlike: ImageView



        init {
            tvsellurgent = view.findViewById(R.id.tv_sell_urgent_now)
            tvknowmore = view.findViewById(R.id.tv_know_more)
            tvlive = view.findViewById(R.id.tvlive)
            tvreject = view.findViewById(R.id.tvreject)
            tvpending = view.findViewById(R.id.tvpending)
            tvexpired = view.findViewById(R.id.tvexpired)
            tvdesc = view.findViewById(R.id.tv_status_desc)
            ivdelete = view.findViewById(R.id.iv_delete)
            tvname = view.findViewById(R.id.tvname)
            tvprice = view.findViewById(R.id.tvprice)
            tvview = view.findViewById(R.id.tvview)
            tvlike = view.findViewById(R.id.tvlike)
            tvdate = view.findViewById(R.id.tvdate)
            ivimage = view.findViewById(R.id.ivimage)
            tvpremium = view.findViewById(R.id.tv_premium)
            ivedit = view.findViewById(R.id.iv_edit)
            lledit = view.findViewById(R.id.ll_edit)
            tvliveagain = view.findViewById(R.id.tv_live_again)
            ivlike = view.findViewById(R.id.ivlike)
        }
    }
}