package com.kaushlesh.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

import com.kaushlesh.R
import com.kaushlesh.bean.LocationListBeans

import com.kaushlesh.bean.subCategoryBean1

class LocationListAdapter(private val list: ArrayList<LocationListBeans.LocationList>, private val context: Context) :
    RecyclerView.Adapter<LocationListAdapter.ViewHolder>() {
    private var itemClickListener: ItemClickListener? = null

    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_row_sub_category, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val dataModel = list[position]


        holder.tvname.text = dataModel.locName


        holder.itemView.setOnClickListener { itemClickListener!!.itemclickSubCategory(dataModel) }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    interface ItemClickListener {
        fun itemclickSubCategory(bean: LocationListBeans.LocationList)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var ivvback: ImageView
        internal var tvname: TextView

        init {
            ivvback = view.findViewById(R.id.iv_back)
            tvname = view.findViewById(R.id.tvname)
        }
    }
}


