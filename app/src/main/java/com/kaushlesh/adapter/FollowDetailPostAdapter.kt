package com.kaushlesh.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.kaushlesh.R
import com.kaushlesh.bean.FollowingUserDetailBean
import com.kaushlesh.bean.GetFavPostBean
import com.kaushlesh.bean.GetMyPostBean


class FollowDetailPostAdapter(): RecyclerView.Adapter<FollowDetailPostAdapter.ViewHolder>() {


    var list: ArrayList<FollowingUserDetailBean.GetFPostBean> = ArrayList()
    var viewType1 = 1
    lateinit var context: Context

    private var itemClickListener: ItemClickListener? = null

    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    constructor(context: Context, list: ArrayList<FollowingUserDetailBean.GetFPostBean>) : this() {
        this.context = context
        this.list = list
    }

    constructor(context: Context) : this() {
        this.list = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view: View
        view = LayoutInflater.from(parent.context).inflate(R.layout.item_my_post_favourite, parent, false)
        return ViewHolder(view)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val dataModel = list[position]

        //holder.iv_like.setBackgroundResource(R.drawable.ic_fav_black)

        if(dataModel.is_favourite == 1) {
            holder.iv_like.setBackgroundResource(R.drawable.ic_favorite_filled_black)
        }
        else
        {
            holder.iv_like.setBackgroundResource(R.drawable.ic_fav_black)
        }

        holder.lllocation.visibility= View.VISIBLE
        holder.tvaddress.text = list[position].address

        holder.tv_desc.text = list[position].title


        //holder.tv_price.text = "₹ " + list[position].price

        if(dataModel.price!!.toInt() == 0)
        {
            holder.tv_price.visibility = View.VISIBLE
            holder.tv_price.text = "₹ (Price Not Applicable)"
        }
        else{
            holder.tv_price.visibility = View.VISIBLE
            holder.tv_price.text = "₹ " + dataModel.price
        }

        if (dataModel.mainPostImg != null && dataModel.mainPostImg.toString().length >0 ) {
            Glide.with(context)
                    .load(dataModel.mainPostImg)
                    .error(R.drawable.bg_img_placeholder)
                    .placeholder(R.drawable.progress_animated)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .skipMemoryCache(true)
                    .into(holder.iv_img)
        }

        holder.iv_like.setOnClickListener {
            itemClickListener!!.itemRemove(position)
        }

        holder.itemView.setOnClickListener {
            itemClickListener!!.itemclickProduct(list.get(position))
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun addItems(plist: java.util.ArrayList<FollowingUserDetailBean.GetFPostBean>) {

        val oldSize = itemCount
        val distinct = plist.distinctBy { it.post_id }
        list.addAll(distinct)
        notifyItemRangeInserted(oldSize, list.size)

    }

    fun removeItems() {
        list.clear()
        notifyDataSetChanged()
    }

    interface ItemClickListener {
        fun itemclickProduct(bean: FollowingUserDetailBean.GetFPostBean)
        fun itemRemove(bean: Int)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var tv_price: TextView
        internal var tv_desc: TextView
        internal var iv_img: ImageView
        internal var iv_like: ImageView
        internal var lllocation : LinearLayout
        internal var tvaddress : TextView

        init {

            tv_price = view.findViewById(R.id.tv_price)
            tv_desc = view.findViewById(R.id.tv_desc)
            iv_img = view.findViewById(R.id.iv_img)
            iv_like = view.findViewById(R.id.iv_like)
            lllocation = view.findViewById(R.id.lllocation)
            tvaddress = view.findViewById(R.id.tv_address)

        }
    }
}