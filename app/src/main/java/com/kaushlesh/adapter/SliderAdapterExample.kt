package com.kaushlesh.adapter

import android.content.Context
import android.content.Intent
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.kaushlesh.R
import com.kaushlesh.bean.PostDetails.postDetailsBean
import com.kaushlesh.bean.PostImage
import com.kaushlesh.view.activity.productDetails.FullScreenImageViewActivity
import com.smarteist.autoimageslider.SliderViewAdapter
import java.util.*


class SliderAdapterExample(private val context: Context, private val postHeaderList: ArrayList<postDetailsBean.PostImageX>) :  SliderViewAdapter<SliderAdapterExample.ViewHolder>()  {


    private var mSliderItems: MutableList<postDetailsBean.PostImageX> = postHeaderList


    fun renewItems(sliderItems: MutableList<postDetailsBean.PostImageX>) {
        mSliderItems = sliderItems
        notifyDataSetChanged()
    }

    fun deleteItem(position: Int) {
        mSliderItems.removeAt(position)
        notifyDataSetChanged()
    }

    fun addItem(sliderItem: postDetailsBean.PostImageX) {
        var pv:PostImage =PostImage()
        pv.postImage= R.drawable.bg_border_black.toString()
        mSliderItems.add(sliderItem)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup?): SliderAdapterExample.ViewHolder? {
        val inflate: View = LayoutInflater.from(parent!!.context)
            .inflate(R.layout.item_slide_image, null)
        return ViewHolder(inflate)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder?, position: Int) {

        //Log.e("TAG", "postimagelist: "+postHeaderList[position].postImage)
        /*for(i in postHeaderList.indices)
        {
            Utils.showLog("slider","images===" + postHeaderList.get(i).postImage.toString())
        }
        Utils.showLog("slider","image ===" + postHeaderList[position].postImage)*/


        Glide.with(context)
            .load(postHeaderList[position].postImage)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
            .into(viewHolder!!.imageViewBackground!!)

        viewHolder.imageViewBackground!!.setOnClickListener {
            val i = Intent(context, FullScreenImageViewActivity::class.java)
            i.putExtra("position", position)
            i.putParcelableArrayListExtra("list", mSliderItems as ArrayList<out Parcelable>)
            context.startActivity(i)
        }


    }

    override fun getCount(): Int {
        return postHeaderList.size

    }

    class ViewHolder(itemView: View?) : SliderViewAdapter.ViewHolder(itemView) {

        var itemView: View? = null
        var imageViewBackground: ImageView? = null


        init {
            imageViewBackground = itemView!!.findViewById(R.id.iv_image)
        }
    }


}