package com.kaushlesh.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.R
import com.kaushlesh.bean.ProductDataBean
import com.kaushlesh.view.fragment.MyAccount.MyAds.KnowMoreDetailShowcaseActivity
import com.kaushlesh.view.fragment.MyAccount.MyAds.SellUrgentDetailActivity


class MyShowcaseAdapter(): RecyclerView.Adapter<MyShowcaseAdapter.ViewHolder>() {

   // var list: ArrayList<ProductDataBean> = ArrayList()
   var list: ArrayList<String> = ArrayList()
    var viewType1 = 1
    lateinit var context: Context
    lateinit var type : String

    private var itemClickListener: ItemClickListener? = null

    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    constructor(context: Context,list:ArrayList<String>,type:String) : this() {

        this.context=context
        this.list = list
        this.type = type
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view: View

            view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_my_post, parent, false)

        return ViewHolder(view)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        //1 live 2//reject 3//pending

            setDataMyShowcase(holder,position)

       holder.tvsellurgent.setOnClickListener {
           //itemClickListener!!.itemclickProduct(dataModel)
           val intent = Intent(context, SellUrgentDetailActivity::class.java)
           context.startActivity(intent)
       }

        holder.tvknowmore.setOnClickListener {
            //itemClickListener!!.itemclickProduct(dataModel)

                val intent = Intent(context, KnowMoreDetailShowcaseActivity::class.java)
                context.startActivity(intent)
        }

        holder.ivdelete.setOnClickListener {
            itemClickListener!!.itemclickDelete(position)
        }
    }

    private fun setDataMyShowcase(holder: ViewHolder, position: Int) {
        if(list.get(position).equals("1"))
        {
            setdata(holder,View.VISIBLE,View.GONE,View.GONE,View.GONE,View.GONE)
            holder.tvdesc.setText(context.getString(R.string.txt_showcase_live))
        }

        else if(list.get(position).equals("2"))
        {
            setdata(holder,View.GONE,View.VISIBLE,View.GONE,View.GONE,View.VISIBLE)
            holder.tvdesc.setText(context.getString(R.string.txt_showcase_rejected))
        }

        else{
            setdata(holder,View.GONE,View.GONE,View.VISIBLE,View.GONE,View.GONE)
            holder.tvdesc.setText(context.getString(R.string.txt_showcase_pending))
        }
    }


    private fun setdata(holder: ViewHolder, p1: Int, p2: Int, p3: Int, p4: Int,p5 :Int) {

        holder.tvlive.visibility = p1
        holder.tvreject.visibility = p2
        holder.tvpending.visibility = p3
        holder.tvsellurgent.visibility = p4
        holder.tvknowmore.visibility = p5
    }

    override fun getItemCount(): Int {
        return list.size
    }

    interface ItemClickListener {
        fun itemclickProduct(bean: ProductDataBean)
        fun itemclickDelete(position: Int)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {


        internal var tvsellurgent: TextView
        internal var tvknowmore: TextView
        internal var tvlive: TextView
        internal var tvreject: TextView
        internal var tvpending: TextView
        internal var tvdesc : TextView
        internal var ivdelete : ImageView

        init {

            tvsellurgent = view.findViewById(R.id.tv_sell_urgent_now)
            tvknowmore = view.findViewById(R.id.tv_know_more)
            tvlive = view.findViewById(R.id.tvlive)
            tvreject = view.findViewById(R.id.tvreject)
            tvpending = view.findViewById(R.id.tvpending)
            tvdesc = view.findViewById(R.id.tv_status_desc)
            ivdelete = view.findViewById(R.id.iv_delete)

        }
    }
}