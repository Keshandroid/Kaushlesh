package com.kaushlesh.adapter

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.kaushlesh.R
import com.kaushlesh.bean.AdvertisementBean
import com.kaushlesh.bean.CategoryBean
import com.kaushlesh.bean.RecommendationBean
import com.kaushlesh.bean.ads.SliderAdapter
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.fragment.HomeFragmentNew
import java.util.*

class RcvListViewAdapter(context: Context, itemClickListenerAds: SliderAdapter.ItemClickListener, itemClickListener: CategoryAdapterNew.ItemClickListener, itemClickListener1: RecommendationAdapter.ItemClickListener, itemClickListener11: RcvListViewAdapter.ItemClickListener, activity: Activity) : RecyclerView.Adapter<RecyclerView.ViewHolder>()
{
    private var itemClickListeneMain: RcvListViewAdapter.ItemClickListener? = null
    val context = context
    val mActivity = activity
    val itemClickListenerAds = itemClickListenerAds

    val itemClickListener = itemClickListener
    val itemClickListeneMain1 = itemClickListener1
    val itemClickListeneMain11 = itemClickListener11
    var items = arrayListOf<RecommendationBean.FreshProductListBean>()
    private var rcvCategoryAdapter = CategoryAdapterNew(arrayListOf<CategoryBean.Category>(), context, 2)

    private var itemPageAdapter = SliderAdapter(context, mutableListOf<AdvertisementBean.Advertisement>())


    private var rcvHomeAdapter = RecommendationAdapter(arrayListOf<RecommendationBean.FreshProductListBean>(), context)
    private var glide = Glide.with(context)
    var firstTime1 = false
    var firstTime2 = false

    internal var adsListItems: MutableList<AdvertisementBean.Advertisement> = java.util.ArrayList()


    interface ItemClickListener {
        fun itemclickCategoryViewAll()
        fun itemclickHomePost(bean: RecommendationBean.FreshProductListBean)
        fun itemFavUnfavourite(bean: RecommendationBean.FreshProductListBean, position: Int)

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType)
        {

            0 -> BannerAdViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.rcv_top_banner_ads_item, parent, false))


            1 -> CategoryTitleViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.rcv_category_title_item, parent, false))
            2 -> CategoryListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.rcv_category_list_item, parent, false))
            3 -> FreshTitleViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.rcv_category_title_item, parent, false))
            4 -> EmptyHolder(LayoutInflater.from(parent.context).inflate(R.layout.rcv_empty_item, parent, false))

            else->FreshListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_row_fresh_recommendation, parent, false))
        }
    }

    override fun onBindViewHolder(holder1: RecyclerView.ViewHolder, position: Int) {


        if(getItemViewType(position) == 0){
            val holder : BannerAdViewHolder = holder1 as BannerAdViewHolder

            try {
                if(adsListItems.isEmpty()){
                    Log.d("AdsView", "111")
                    holder.rootView.visibility = View.GONE
                    holder.rootView.layoutParams = LinearLayout.LayoutParams(0, 0)

                }else{
                    Log.d("AdsView", "222")
                    holder.rootView.visibility = View.VISIBLE
                    holder.itemView.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                }
                //holder.bind()
            }catch (e: Exception){
                e.printStackTrace()
            }
        }

        if(getItemViewType(position) == 1)
        {
            val holder : CategoryTitleViewHolder = holder1 as CategoryTitleViewHolder
            holder.viewViewAll.setOnClickListener {
                itemClickListeneMain11.itemclickCategoryViewAll()
            }
        }


        //PRODUCT LIST
        if(getItemViewType(position) == 5)
        {
            val dataModel = items[position - 5]
            val holder : FreshListViewHolder = holder1 as FreshListViewHolder
            //viewHolder.tvTitle.text = "Title ${position}"
           // viewHolder.tvDesc.text = "${position}. Sample Description!"
            if (dataModel.ispremium == 1) {
                holder.tvfeatured.visibility = View.VISIBLE
                /* val anim = AlphaAnimation(0.0f, 1.0f)
                 anim.duration = 1000 //You can manage the blinking time with this parameter
                 anim.startOffset = 20
                 anim.repeatMode = Animation.REVERSE
                 anim.repeatCount = Animation.INFINITE
                 holder.tvfeatured.startAnimation(anim)*/
            } else {
                holder.tvfeatured.visibility = View.GONE
            }

        /*    if(dataModel.postImages != null && dataModel.postImages.size >0) {
                Glide.with(context)
                        .load(dataModel.postImages[0].postImage)
                        .placeholder(R.drawable.bg_img_placeholder)
                        .into(holder.ivimg)
              //  Picasso.get().load(dataModel.postImages[0].postImage).into(holder.ivimg);
            }
            else
            {
                Glide.with(context)
                        .load(R.drawable.bg_img_placeholder)
                        .placeholder(R.drawable.bg_img_placeholder)
                        .into(holder.ivimg)
                //Picasso.get().load(R.drawable.bg_img_placeholder).into(holder.ivimg);
            }*/



         /*   val  i = (holder.adapterPosition - 4) + 1

                if(i%2==1)
                {
                    if(firstTime1)
                    {
                        glide
                                .load(dataModel.mainPostImage)
                                //.override(1600, 1600) // Can be 2000, 2000
                                .error(R.drawable.bg_img_placeholder)
                                .placeholder(R.drawable.bg_img_placeholder)
                                .into(holder.ivimg)
                        println("$i PlaceHolder3")
                        firstTime1 = false
                    }
                    else {
                        glide
                                .load(dataModel.mainPostImage)
                                //.override(1600, 1600) // Can be 2000, 2000
                                .error(R.drawable.bg_img_placeholder)
                                .placeholder(R.drawable.gl_main)
                                .into(holder.ivimg)
                        println("$i PlaceHolder1")
                        firstTime1 = true
                    }
                }
                else
                {
                    if(firstTime2)
                    {
                        glide
                                .load(dataModel.mainPostImage)
                                //.override(1600, 1600) // Can be 2000, 2000
                                .error(R.drawable.bg_img_placeholder)
                                .placeholder(R.drawable.loading_placeholder)
                                .into(holder.ivimg)
                        println("$i PlaceHolder4")
                        firstTime2 = false
                    }
                    else {
                        glide
                                .load(dataModel.mainPostImage)
                                //.override(1600, 1600) // Can be 2000, 2000
                                .error(R.drawable.bg_img_placeholder)
                                .placeholder(R.drawable.bg_no_img)
                                .into(holder.ivimg)
                        println("$i PlaceHolder2")
                        firstTime2 = true
                    }
                }
*/

            if(dataModel.mainPostImage != null && dataModel.mainPostImage.toString().length >0)
            {

                glide.load(dataModel.mainPostImage)
                    //.override(1600, 1600) // Can be 2000, 2000
                    .error(R.drawable.bg_img_placeholder)
//                    .placeholder(R.drawable.loading_placeholder)
                        .placeholder(R.drawable.progress_animated)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .skipMemoryCache(true)
                    .into(holder.ivimg)

            }

            if(dataModel.price!! == 0)
            {
                holder.tvprice.visibility = View.VISIBLE
                holder.tvprice.text = "₹ (Price Not Applicable)"
            }
            else{
                holder.tvprice.visibility = View.VISIBLE
                holder.tvprice.text = "₹ " + dataModel.price
            }

            holder.tvtitle.text = dataModel.title
            holder.tvdesc.text = dataModel.other_information
            holder.tvaddress.text = dataModel.address
            holder.tvlatlong.text = "Lat : "+ dataModel.latitude + "\nLong : " + dataModel.longitude
            if(dataModel.isFavorite == 1) {
                holder.ivlike.isChecked = true
                //holder.ivlike.setBackgroundResource(R.drawable.ic_favorite_filled_black)
            } else{
                holder.ivlike.isChecked = false
                //holder.ivlike.setBackgroundResource(R.drawable.ic_fav_black)
            }

            holder.itemView.setOnClickListener {
                itemClickListeneMain11.itemclickHomePost(dataModel)
            }

            /*holder.rlLikeToggle.setOnClickListener(object : View.OnClickListener{
                override fun onClick(p0: View?) {

                    if(holder.ivlike.isChecked){
                        holder.ivlike.isChecked = false
                    }else{
                        holder.ivlike.isChecked = true
                    }
                    itemClickListeneMain1.itemFavUnfavourite(dataModel, position)



                }

            })*/

            //favourite toggle button
            holder.ivlike.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
                override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                    if (buttonView!!.isPressed) {
                        itemClickListeneMain1.itemFavUnfavourite(dataModel, position)
                    }

                }
            })


            //Favourite button imageview
            /*holder.ivlike.setOnClickListener {

                itemClickListeneMain1.itemFavUnfavourite(dataModel, position)

                //original
                if(holder.ivlike.background.getConstantState()?.equals(context.getDrawable(R.drawable.ic_favorite_filled_black)?.getConstantState())!!)
                {
                    SetPostDetailData.setFav(holder.ivlike, 0)
                }
                else{
                    SetPostDetailData.setFav(holder.ivlike, 1)
                }
            }*/



            if(position%2 == 0)
            {
                val threeDp = holder.rootView.resources.getDimension(R.dimen._3sdp).toInt()
                val fiveDp = holder.rootView.resources.getDimension(R.dimen._5sdp).toInt()
                holder.rootView.setPadding(fiveDp, threeDp, threeDp, threeDp)
            }
            else
            {
                val threeDp = holder.rootView.resources.getDimension(R.dimen._3sdp).toInt()
                val fiveDp = holder.rootView.resources.getDimension(R.dimen._5sdp).toInt()
                holder.rootView.setPadding(threeDp, threeDp, threeDp, fiveDp)
            }
        }
        else
        {
            //OTHER VIEWHOLDERS IDENTIFIED WITH getItemViewType()
        }
    }

    override fun getItemCount(): Int {
        return items.size + 5
    }


    override fun getItemViewType(position: Int): Int {
        return when(position)
        {
            0 -> 0
            1 -> 1
            2 -> 2
            3 -> 3
            4 -> 4
            else->5
        }
    }

    fun setCategoryItems(categoryList: ArrayList<CategoryBean.Category>)
    {
        rcvCategoryAdapter.setCategoryItems(categoryList)

        rcvCategoryAdapter.setClicklistner(itemClickListener)
    }


    fun setSponsoredAdsItems(adsList: ArrayList<AdvertisementBean.Advertisement>) {

        adsListItems = adsList

        itemPageAdapter.setAdsItems(adsList)

        itemPageAdapter.setClicklistnerAds(itemClickListenerAds)
        notifyItemChanged(0)
    }



    fun getAllItems(): ArrayList<RecommendationBean.FreshProductListBean> {
        return this.items
    }


    fun addItems(freshItems: java.util.ArrayList<RecommendationBean.FreshProductListBean>, cleardata: Boolean) {
        /*    val oldSize = itemCount
            items.clear()
            items.addAll(freshItems)
            notifyDataSetChanged()*/

        //models.distinctBy{ it is Teacher}

        if(cleardata)
        {
            Utils.showLog(HomeFragmentNew.TAG, "if" + cleardata)
            items.clear()
            //val oldSize = itemCount
           // val distinct = freshItems.distinct().toList()
            val distinct = freshItems.distinctBy { it.post_id }
            //items.addAll(freshItems)
            items.addAll(distinct)
            notifyDataSetChanged()
        }
        else {
            Utils.showLog(HomeFragmentNew.TAG, "====else====" + cleardata)

            val oldSize = itemCount
            //val distinct = freshItems.distinct().toList()
            val distinct = freshItems.distinctBy { it.post_id }
           /* items.addAll(freshItems)
            notifyItemRangeInserted(oldSize, freshItems.size)*/

            items.addAll(distinct)
            notifyItemRangeInserted(oldSize, distinct.size)
        }
    }

    fun setHomeItems(freshrecomlist: ArrayList<RecommendationBean.FreshProductListBean>) {
        rcvHomeAdapter.setHomeItems(freshrecomlist)

        rcvHomeAdapter.setClicklistner(itemClickListeneMain1)
    }

    fun removeHomeItems() {
        items.clear()
        notifyDataSetChanged()
    }

    fun updateLikeDislikePost(likeState: String?, postIdLikeDislike: String?) {

        if(!items.isEmpty()){


            for (i in items.indices){
                val post_id = items[i].post_id
                if(postIdLikeDislike == post_id.toString()){
                    Log.d("CLICKED_ITEM", " Matched in home screen " + i)

                    if(likeState == "true"){
                        items[i].isFavorite = 1
                    }else if(likeState == "false"){
                        items[i].isFavorite = 0
                    }
                    notifyItemChanged(i + 5)
                }else{
                    Log.d("CLICKED_ITEM", " NOT Matched in home screen " + i)
                }
            }
        }
    }

    class CategoryTitleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var rootView = itemView.findViewById<View>(R.id.rootView)
        var tvTitle : TextView = itemView.findViewById(R.id.tvTitle)
        var viewViewAll : View = itemView.findViewById(R.id.viewViewAll)
        init {
            tvTitle.setText("Explore Categories")
        }
    }




    class FreshTitleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        var rootView = itemView.findViewById<View>(R.id.rootView)
        var tvTitle : TextView = itemView.findViewById(R.id.tvTitle)
        var viewViewAll : View = itemView.findViewById(R.id.viewViewAll)
        init {
            tvTitle.setText("Fresh Selling")
            viewViewAll.visibility = View.GONE
        }
    }

    inner class CategoryListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        var rootView = itemView.findViewById<View>(R.id.rootView)
        var rcvCategoryListView : RecyclerView = itemView.findViewById(R.id.rcvCategotyList)
        init {
            rcvCategoryListView.layoutManager = LinearLayoutManager(itemView.context, RecyclerView.HORIZONTAL, false)
            rcvCategoryListView.adapter = rcvCategoryAdapter
        }
    }

    inner class BannerAdViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var rootView = itemView.findViewById<View>(R.id.rootView)
        var adsPager : ViewPager = itemView.findViewById(R.id.advertisement_pager)
        val timer = Timer()

        init {
            adsPager.adapter = itemPageAdapter
            timer.scheduleAtFixedRate(SliderTimer(), 5000, 5000)
        }

        inner class SliderTimer : TimerTask() {
            override fun run() {

                try {
                    mActivity.runOnUiThread(Runnable {
                        if (adsPager.getCurrentItem() < adsListItems.size - 1) {
                            adsPager.setCurrentItem(adsPager.getCurrentItem() + 1)
                        } else adsPager.setCurrentItem(0)
                    })
                }catch (e: java.lang.Exception){
                    e.printStackTrace()
                }



            }
        }

    }



    class FreshListViewHolder(view: View) : RecyclerView.ViewHolder(view)
    {
        var rootView = itemView.findViewById<View>(R.id.rootView)
        internal var ivimg: ImageView
        internal var tvfeatured: TextView
        internal var tvprice: TextView
        internal var tvdesc: TextView
        internal var tvaddress: TextView
        //internal var ivlike: ImageView
        internal var ivlike: ToggleButton
        internal var rlLikeToggle: RelativeLayout
        internal var tvtitle: TextView
        internal var tvlatlong: TextView

        init {
            ivimg = view.findViewById(R.id.iv_img)
            tvfeatured = view.findViewById(R.id.tv_title)
            tvprice = view.findViewById(R.id.tv_price)
            tvdesc = view.findViewById(R.id.tv_desc)
            tvaddress = view.findViewById(R.id.tv_address)
            ivlike = view.findViewById(R.id.iv_like)
            rlLikeToggle = view.findViewById(R.id.rlLikeToggle)
            tvtitle = view.findViewById(R.id.tvtitle)
            tvlatlong = view.findViewById(R.id.tv_latlong)
        }
    }

    class EmptyHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {

    }
}