package com.kaushlesh.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.kaushlesh.R
import com.kaushlesh.bean.GetFavPostBean
import com.kaushlesh.bean.GetMyPostBean


class MyFavouritepostAdapter(): RecyclerView.Adapter<MyFavouritepostAdapter.ViewHolder>() {


    var list: ArrayList<GetFavPostBean.FavPostlist> = ArrayList()
    var viewType1 = 1
    lateinit var context: Context

    private var itemClickListener: ItemClickListener? = null

    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    constructor(context: Context, list: ArrayList<GetFavPostBean.FavPostlist>) : this() {
        this.context = context
        this.list = list
    }

    constructor(context: Context) : this() {
        this.list = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        var view: View

        view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_my_post_favourite, parent, false)

        return ViewHolder(view)

    }

    fun addItems(allList: java.util.ArrayList<GetFavPostBean.FavPostlist>) {

        val oldSize = itemCount
        val distinct = allList.distinctBy { it.post_id }
        list.addAll(distinct)
        notifyItemRangeInserted(oldSize, allList.size)
    }

    fun removeItems() {
        list.clear()
        notifyDataSetChanged()
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_desc.text = list[position].title

        //holder.tv_price.text = "₹ " + list[position].price


        if(list[position].price!!.toInt() == 0)
        {
            holder.tv_price.visibility = View.VISIBLE
            holder.tv_price.text = "₹ (Price Not Applicable)"
        }
        else{
            holder.tv_price.visibility = View.VISIBLE
            holder.tv_price.text = "₹ " + list[position].price
        }

        val dataModel = list[position]

        if (dataModel.mainPostImg != null && dataModel.mainPostImg.toString().length >0 ) {
            Glide.with(context)
                    .load(dataModel.mainPostImg)
                    //.error(R.drawable.bg_img_placeholder)
                    .placeholder(R.drawable.progress_animated)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .skipMemoryCache(true)
                    .into(holder.iv_img)
        }

        if(dataModel.address!=null){
            holder.tv_address.setText(""+dataModel.address)
        }

        if (dataModel.other_information!=null){
            holder.otherInfo.setText(""+dataModel.other_information)
        }

        holder.iv_like.setOnClickListener {
            itemClickListener!!.itemRemove(position)
        }

        holder.itemView.setOnClickListener {
            itemClickListener!!.itemclickProduct(list.get(position))
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    interface ItemClickListener {
        fun itemclickProduct(bean: GetFavPostBean.FavPostlist)
        fun itemRemove(bean: Int)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var tv_price: TextView
        internal var tv_desc: TextView
        internal var otherInfo: TextView
        internal var tv_address: TextView
        internal var iv_img: ImageView
        internal var iv_like: ImageView

        init {

            tv_price = view.findViewById(R.id.tv_price)
            tv_desc = view.findViewById(R.id.tv_desc)
            iv_img = view.findViewById(R.id.iv_img)
            iv_like = view.findViewById(R.id.iv_like)

            otherInfo = view.findViewById(R.id.tv_other_info)
            tv_address = view.findViewById(R.id.tv_address)



        }
    }
}