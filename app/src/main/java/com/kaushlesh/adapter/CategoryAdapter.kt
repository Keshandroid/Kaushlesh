package com.kaushlesh.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.R
import com.kaushlesh.bean.BrandShowCaseBean

class CategoryAdapter() :
    RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {
    private var itemClickListener: ItemClickListener? = null
    var viewType1 = 1

    var list: ArrayList<BrandShowCaseBean> = ArrayList()

    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    constructor(arrayList: List<BrandShowCaseBean>, context: Context, viewType: Int) : this() {
        this.list = arrayList as ArrayList<BrandShowCaseBean>
        this.viewType1 = viewType
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

       // val view = LayoutInflater.from(parent.context).inflate(R.layout.item_row_category, parent, false)

        //return ViewHolder(view)
        var view: View
        Log.e("test","==view type==" + viewType1)
        if (viewType1 == 1) {
            view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_row_category, parent, false)
        }

        else
            view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_row_category_two, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val dataModel = list[position]


        holder.tvcategoryname.text = dataModel.brandname
        holder.ivimg.setImageResource(dataModel.brandimage)

        holder.itemView.setOnClickListener { itemClickListener!!.itemclickCategory(dataModel) }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    interface ItemClickListener {
        fun itemclickCategory(bean: BrandShowCaseBean)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var ivimg: ImageView
        internal var tvcategoryname: TextView

        init {
            ivimg = view.findViewById(R.id.img_category)
            tvcategoryname = view.findViewById(R.id.tv_categoryname)
        }
    }
}

