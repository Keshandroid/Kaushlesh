package com.kaushlesh.adapter.filter.Vehicle

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.R
import com.kaushlesh.bean.BrandShowCaseBean
import com.kaushlesh.bean.SubCategoryBean

class AllBrandAdapter(private val list: List<SubCategoryBean>, private val context: Context) :
    RecyclerView.Adapter<AllBrandAdapter.ViewHolder>() {
    private var itemClickListener: ItemClickListener? = null

    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_row_filter_by_all_brand, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val dataModel = list[position]

        holder.tvname.setText(dataModel.name)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    interface ItemClickListener {
        fun itemclickAllBrand(bean: SubCategoryBean)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var ivcheckbox: CheckBox
        internal var tvname: TextView

        init {
            tvname = view.findViewById(R.id.tv_title)
            ivcheckbox = view.findViewById(R.id.checkbox)


            ivcheckbox.setOnCheckedChangeListener(
                object : CompoundButton.OnCheckedChangeListener {
                    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
                        if (isChecked) {

                            //selecteddaysid.add(diplomaslist.get(getAdapterPosition()).id)

                        } else {

                           // selecteddaysid.remove(diplomaslist.get(getAdapterPosition()).id)
                        }
                    }
                })

            view.setOnClickListener(
                object : View.OnClickListener {
                    override fun onClick(v: View) {
                        ivcheckbox.setChecked(!ivcheckbox.isChecked())
                    }
                })
        }
    }
}
