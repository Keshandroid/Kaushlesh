package com.kaushlesh.adapter.filter

import android.content.ContentValues.TAG
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.R
import com.kaushlesh.bean.FilterBean
import com.kaushlesh.utils.Utils

class FilterPremiumCommanAdapter(private val list: List<FilterBean>, private val context: Context) :
    RecyclerView.Adapter<FilterPremiumCommanAdapter.ViewHolder>() {
    private var itemClickListener: ItemClickListener? = null
    private var selectedPosition = -1 // no selection by default


    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_row_filter_comman, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val dataModel = list[position]
        //Utils.showLog(TAG,"==name==" + dataModel.name)
        holder.tvname.text = dataModel.name.toString()

        holder.ivcheckbox.setChecked(selectedPosition == position);

        //single
      /*  if(selectedPosition == position){
            holder.ivcheckbox.setChecked(true);
            holder.ivcheckbox.setBackgroundResource(R.drawable.bg_edittext_black)
        }
        else{
            holder.ivcheckbox.setChecked(false);
            holder.ivcheckbox.setBackgroundResource(R.drawable.bg_edittext)
        }

        holder.tvname.setOnClickListener({ v ->
            if (position == selectedPosition) {
                holder.ivcheckbox.setChecked(false)
                holder.ivcheckbox.setBackgroundResource(R.drawable.bg_edittext)
                selectedPosition = -1
            } else {
                selectedPosition = position
                notifyDataSetChanged()
                holder.ivcheckbox.setBackgroundResource(R.drawable.bg_edittext_black)
            }
            itemClickListener!!.itemClickAllBrand(position)
        })*/

        //multi

        if (list.get(position).selected == true) {
            holder.ivcheckbox.setBackgroundResource(R.drawable.bg_edittext_black)
        } else {
            holder.ivcheckbox.setBackgroundResource(R.drawable.bg_edittext)
        }

        holder.tvname.setOnClickListener {

            list.get(position).selected = !list.get(position).selected

            if (list[position].selected == true) {
                holder.ivcheckbox.setBackgroundResource(R.drawable.bg_edittext_black)
            } else {
                holder.ivcheckbox.setBackgroundResource(R.drawable.bg_edittext)
            }
            itemClickListener!!.itemClickAllBrand(position)
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }


    interface ItemClickListener {
        fun itemClickAllBrand(position: Int)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var ivcheckbox: CheckBox
        internal var tvname: TextView

        init {
            tvname = view.findViewById(R.id.tvname)
            ivcheckbox = view.findViewById(R.id.cv_comman)
        }

    }
}


