package com.kaushlesh.adapter.filter.Mobiles

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.R
import com.kaushlesh.bean.MobileBrandBeans

class TabBrandAdapter (
    private val list: List<MobileBrandBeans.MobileBrandBeansList>,
    private val context: Context
) :
    RecyclerView.Adapter<TabBrandAdapter.ViewHolder>() {
    private var itemClickListener: ItemClickListener? = null

    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_row_filter_comman, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel = list[position]

        holder.tvname.setText(dataModel.brandName)
        if (list.get(position).selected == true) {
            holder.ivcheckbox.setBackgroundResource(R.drawable.bg_edittext_black)
        } else {
            holder.ivcheckbox.setBackgroundResource(R.drawable.bg_edittext)
        }

        holder.tvname.setOnClickListener {

            list.get(position).selected = !list.get(position).selected!!

            if (list[position].selected == true) {
                holder.ivcheckbox.setBackgroundResource(R.drawable.bg_edittext_black)
            } else {
                holder.ivcheckbox.setBackgroundResource(R.drawable.bg_edittext)
            }
            itemClickListener!!.itemClickBrand()
        }
    }
    override fun getItemCount(): Int {
        return list.size
    }

    interface ItemClickListener {
        fun itemClickBrand()
    }



    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var ivcheckbox: CheckBox
        internal var tvname: TextView

        init {
            tvname = view.findViewById(R.id.tvname)
            ivcheckbox = view.findViewById(R.id.cv_comman)
        }
    }
}
