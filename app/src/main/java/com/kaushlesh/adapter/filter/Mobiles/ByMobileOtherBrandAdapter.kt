package com.kaushlesh.adapter.filter.Mobiles

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.R
import com.kaushlesh.bean.MobileBrandBeans

class ByMobileOtherBrandAdapter(
private val list: List<MobileBrandBeans.MobileBrandBeansList>,
private val context: Context
) :
RecyclerView.Adapter<ByMobileOtherBrandAdapter.ViewHolder>() {
    private var itemClickListener: ItemClickListener? = null

    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_row_filter_by_all_brand, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel = list[position]
        holder.tvname.text = dataModel.brandName
          //  list.get(position).selected =! list.get(position).selected!!
        holder.ivcheckbox.isChecked= list.get(position).selected!!
        holder.ivcheckbox.setOnCheckedChangeListener { buttonView, isChecked ->
            list.get(position).selected = isChecked
            itemClickListener!!.itemclick1()
        }
    }
    override fun getItemCount(): Int {
        return list.size
    }

    interface ItemClickListener {
        fun itemclick1()
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var ivcheckbox: CheckBox
        internal var tvname: TextView

        init {
            tvname = view.findViewById(R.id.tv_title)
            ivcheckbox = view.findViewById(R.id.checkbox)

            view.setOnClickListener(
                object : View.OnClickListener {
                    override fun onClick(v: View) {
                        ivcheckbox.setChecked(!ivcheckbox.isChecked())
                    }
                })

        }
    }
}
