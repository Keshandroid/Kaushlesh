package com.kaushlesh.adapter.filter.Vehicle

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.R
import com.kaushlesh.bean.BrandShowCaseBean

class ByBrandAdapter(private val list: List<BrandShowCaseBean>, private val context: Context) :
    RecyclerView.Adapter<ByBrandAdapter.ViewHolder>() {
    private var itemClickListener: ItemClickListener? = null

    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_row_filter_by_brand, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val dataModel = list[position]


        holder.ivimg.setImageResource(dataModel.brandimage)

        holder.itemView.setOnClickListener {
            itemClickListener!!.itemclickBrand(dataModel)
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    interface ItemClickListener {
        fun itemclickBrand(bean: BrandShowCaseBean)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var ivcheckbox: CheckBox
        internal var ivimg: ImageView

        init {
            ivimg = view.findViewById(R.id.iv_img)
            ivcheckbox = view.findViewById(R.id.cv_brand)
        }
    }
}
