package com.kaushlesh.adapter.filter.Mobiles

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kaushlesh.R
import com.kaushlesh.bean.BrandShowCaseBean
import com.kaushlesh.bean.MobileBrandBeans
import com.kaushlesh.utils.Utils

class ByBrandMobileAdapter(
    private val list: List<MobileBrandBeans.MobileBrandBeansList>,
    private val context: Context
) : RecyclerView.Adapter<ByBrandMobileAdapter.ViewHolder>() {
    private var itemClickListener: ItemClickListener? = null

    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_row_filter_by_brand, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val dataModel = list[position]

        holder.llview.setBackgroundResource(R.drawable.bg_corner_white)

        //holder.tvbrandname.text = dataModel.brandName
        if (list[position].selected == true) {
            holder.llview.setBackgroundResource(R.drawable.bg_edittext_black)
        } else {
            holder.llview.setBackgroundResource(R.drawable.bg_corner_white)
            // holder.ivcheckbox.isChecked==list[position].selected
        }

        holder.llview.setOnClickListener {
            Utils.showLog("adapter","==clicked==")
            list.get(position).selected = !list.get(position).selected!!

            if (list[position].selected == true) {
                holder.llview.setBackgroundResource(R.drawable.bg_edittext_black)
            } else {
                holder.llview.setBackgroundResource(R.drawable.bg_corner_white)
                // holder.ivcheckbox.isChecked==list[position].selected
            }
            itemClickListener!!.itemclick()
        }
        Glide.with(context)
            .load(dataModel.brandImage)
            .fitCenter()
            .into(holder.ivimg)
    }
    override fun getItemCount(): Int {
        return list.size
    }

    interface ItemClickListener {
        fun itemclick()

    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var ivimg: ImageView
        //internal var tvbrandname: TextView
        internal var llview: RelativeLayout

        init {
            ivimg = view.findViewById(R.id.iv_img)
            //tvbrandname = view.findViewById(R.id.tv_brandname)
            llview = view.findViewById(R.id.llview)
        }
    }
}


