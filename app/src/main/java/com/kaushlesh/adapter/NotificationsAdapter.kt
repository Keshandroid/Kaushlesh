package com.kaushlesh.adapter

import android.content.Context
import android.graphics.Color
import android.view.*
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.devs.readmoreoption.ReadMoreOption
import com.kaushlesh.R
import com.kaushlesh.bean.NotificationBean
import java.lang.Exception


class NotificationsAdapter(
    private val list: List<NotificationBean.NotificationList>,
    private val context: Context
) :
    RecyclerView.Adapter<NotificationsAdapter.ViewHolder>() {
    private var itemClickListener: ItemClickListener? = null

    val readMoreOption = ReadMoreOption.Builder(context).build()


    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

      //  val view = LayoutInflater.from(parent.context).inflate(R.layout.item_row_invoices, parent, false)
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_row_notifications,
            parent,
            false
        )
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val dataModel = list[position]

        holder.tv_noti_title.text = dataModel.type
        holder.tv_noti_date.text = dataModel.message_time

        holder.tv_noti_desc.setText(dataModel.message)

        /*try {

            val readMoreOption = ReadMoreOption.Builder(context)
                .textLength(2, ReadMoreOption.TYPE_LINE) // OR
                //.textLength(300, ReadMoreOption.TYPE_CHARACTER)
                .moreLabel("Read More")
                .lessLabel("Read Less")
                .moreLabelColor(Color.RED)
                .lessLabelColor(Color.BLUE)
                .labelUnderLine(true)
                .expandAnimation(true)
                .build()

            readMoreOption.addReadMoreTo(holder.tv_noti_desc,dataModel.message)
        }catch (e: Exception){
            e.printStackTrace()
        }*/


        holder.llNotification.setOnClickListener {

            itemClickListener!!.itemclick(dataModel)

        }
        holder.textViewOptions.setOnClickListener {


            val popup = android.widget.PopupMenu(context, holder.textViewOptions)
            popup.inflate(R.menu.options_menu)

            popup.setOnMenuItemClickListener(android.widget.PopupMenu.OnMenuItemClickListener { item: MenuItem? ->

                when (item!!.itemId) {
                    R.id.menuDelete -> {
                        itemClickListener!!.itemDeleteClick(dataModel)
                    }

                }
                true
            })

            popup.show()

        }

    }


    override fun getItemCount(): Int {
        return list.size
    }

    interface ItemClickListener {
        fun itemclick(bean: NotificationBean.NotificationList)
        fun itemDeleteClick(bean: NotificationBean.NotificationList)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var tv_noti_title: TextView
        internal var tv_noti_desc: TextView
        internal var tv_noti_date: TextView
        internal var textViewOptions: TextView
        internal var llNotification: CardView

        init {

            tv_noti_title = view.findViewById(R.id.tv_noti_title)
            tv_noti_desc = view.findViewById(R.id.tv_noti_desc)
            tv_noti_date = view.findViewById(R.id.tv_noti_date)
            textViewOptions = view.findViewById(R.id.textViewOptions)

            llNotification = view.findViewById(R.id.llNotification)



        }

    }
}


