package com.kaushlesh.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.R
import com.kaushlesh.bean.GetCategoryPackagesBean
import com.kaushlesh.bean.ProductDataBean


class AdPostPackageAdapter() :RecyclerView.Adapter<AdPostPackageAdapter.ViewHolder>() {

    var list: ArrayList<ProductDataBean> = ArrayList()
    var viewType1 = 1
    lateinit var context: Context
    lateinit var type: String
    private var selectedPosition = -1// no selection by default
    internal var pkglist: ArrayList<GetCategoryPackagesBean.Packages> = ArrayList()

    private var itemClickListener: ItemClickListener? = null

    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    constructor(context: Context, type: String, pkglist: java.util.ArrayList<GetCategoryPackagesBean.Packages>) : this() {

        this.context=context
        this.type = type
        this.pkglist = pkglist
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view: View

        view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_extra_ad, parent, false)

        return ViewHolder(view)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val data = pkglist.get(position)

        holder.tvprice.setText("₹ " + data.price)
        holder.tvadpack.setText(data.noofAd + " " + context.getString(R.string.txt_ad_packs))
        holder.tvmonth.setText(data.duration  + " " + context.getString(R.string.txt_days))
        holder.tvpkgval.setText("(" +context.getString(R.string.txt_package_validity) + "\n" + data.validity + " "+context.getString(R.string.txt_days)+")")

        holder.ivcheck.setChecked(selectedPosition == position)


        holder.ivcheck.setOnClickListener {

            if( holder.ivcheck.isChecked())
            {
                // checkbox.setChecked(true);
                selectedPosition = position
                itemClickListener!!.itemclickProduct(data, type)
                notifyDataSetChanged()
            }
            else
            {
                holder.rlmain.setBackgroundResource(R.drawable.bg_edittext)
                itemClickListener!!.itemclickProduct(data, "null")
            }
        }

        holder.itemView.setOnClickListener {

                holder.ivcheck.performClick()

        }

        if (selectedPosition == position) {
            // checkbox.setChecked(true);
            holder.rlmain.setBackgroundResource(R.drawable.bg_edittext_black)
           // itemClickListener!!.itemclickProduct(data,type)
        } else {
            // checkbox.setChecked(false);
            holder.rlmain.setBackgroundResource(R.drawable.bg_edittext)
            //itemClickListener!!.itemclickProduct(data,type)
        }

    }

    override fun getItemCount(): Int {
        return pkglist.size
    }

    interface ItemClickListener {
            fun itemclickProduct(bean: GetCategoryPackagesBean.Packages, type: String)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

         internal var tvmonth: TextView
         internal var tvadpack: TextView
         internal var tvprice: TextView
        internal var tvpkgval: TextView
         internal var rlmain: RelativeLayout
        internal var llmain: LinearLayout
        internal var ivcheck: CheckBox

        init {

            tvmonth = view.findViewById(R.id.tv_month)
            tvadpack = view.findViewById(R.id.tv_ad_pack)
            tvprice = view.findViewById(R.id.tv_price)
            rlmain = view.findViewById(R.id.rlmain)
            llmain = view.findViewById(R.id.llmain)
            ivcheck = view.findViewById(R.id.iv_check)
            tvpkgval = view.findViewById(R.id.tv_pkg_val)

            /*ivcheck.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->

                if (isChecked) {
                   rlmain.setBackgroundResource(R.drawable.bg_edittext_black)

                } else {
                    rlmain.setBackgroundResource(R.drawable.bg_edittext)
                }
                //changeNextButtonBackground()
            })

            view.setOnClickListener {
                ivcheck.setChecked(!ivcheck.isChecked())
            }*/
        }
    }
}