package com.kaushlesh.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kaushlesh.R
import com.kaushlesh.bean.InvoicesBean
import com.kaushlesh.bean.MyPackageOrderBean

class InvoicesAdapter(private val list: List<MyPackageOrderBean.MyPackageOrderList>, private val context: Context) :
    RecyclerView.Adapter<InvoicesAdapter.ViewHolder>() {
    private var itemClickListener: ItemClickListener? = null

    fun setClicklistner(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

      //  val view = LayoutInflater.from(parent.context).inflate(R.layout.item_row_invoices, parent, false)
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_row_invoice, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val dataModel = list[position]


        holder.tvinvoiceno.text = context.getString(R.string.txt_invoice) + " "+ dataModel.order_id
        holder.tvdate.text = dataModel.activation_date
        holder.tvprice.text = "₹ " + dataModel.price


//        holder.ivdownload.setOnClickListener { itemClickListener!!.itemclick(dataModel) }

        holder.llInvoice.setOnClickListener { itemClickListener!!.itemclick(dataModel) }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    interface ItemClickListener {
        fun itemclick(bean: MyPackageOrderBean.MyPackageOrderList)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var ivdownload: ImageView
        internal var tvinvoiceno: TextView
        internal var tvdate: TextView
        internal var tvprice: TextView
        internal var llInvoice: LinearLayout

        init {
            ivdownload = view.findViewById(R.id.iv_file)
            tvinvoiceno = view.findViewById(R.id.tv_invoice_no)
            tvdate = view.findViewById(R.id.tv_date)
            tvprice = view.findViewById(R.id.tv_amount)
            llInvoice = view.findViewById(R.id.llInvoice)
        }
    }
}


