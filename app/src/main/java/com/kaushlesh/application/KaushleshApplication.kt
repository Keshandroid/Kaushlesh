package com.kaushlesh.application

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import androidx.lifecycle.LifecycleObserver
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.google.firebase.FirebaseApp
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.kaushlesh.Controller.GetListingController
import com.kaushlesh.R
import com.kaushlesh.constant.AppConstants
import com.kaushlesh.interfaces.ParseControllerListener


class KaushleshApplication : Application(), Application.ActivityLifecycleCallbacks,
    LifecycleObserver{

    val TAG = KaushleshApplication::class.java.getSimpleName()
    lateinit var REF_MYRIAM_APPLICATION: KaushleshApplication
    internal var isautho = false
    private var mInstance: KaushleshApplication? = null
    private var mContext: Context? = null
    private var appName: String? = null
    lateinit var getListingController: GetListingController
    lateinit var parseControllerListener: ParseControllerListener




    /**
     * The m request queue.
     */
    private var mRequestQueue: RequestQueue? = null


    /**
     * Instantiates a new smart application.
     */


    /**
     * Gets the single instance of SmartApplication.
     *
     * @return single instance of SmartApplication
     */

    init {
        instance = this
    }

    companion object {
        private var instance: KaushleshApplication? = null

        var onAppForegrounded = false


        fun applicationContext() : Context {
            return instance!!.applicationContext
        }

        fun onAppForground() : Boolean {
            return onAppForegrounded
        }

    }

    @Synchronized
    fun getInstance(): KaushleshApplication? {
        return mInstance
    }



    fun getContext(): Context {
        return getInstance()!!.getApplicationContext()
    }


    fun getAppContext(): Context? {
        return mContext
    }

    override fun onCreate() {
        super.onCreate()

        mInstance = this
        appName = resources.getString(R.string.app_name)

        REF_MYRIAM_APPLICATION = this
        mContext = this.applicationContext


        FirebaseApp.initializeApp(applicationContext)

        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true)
        FirebaseAnalytics.getInstance(this)
        FirebaseAnalytics.getInstance(this).setAnalyticsCollectionEnabled(true)

    }

    fun getAPValueByKey(sharedPreferenceKey: String): String? {
        val sharedPreferences = getSharedPreferences("Kaushlesh", Context.MODE_PRIVATE)
        return sharedPreferences.getString(sharedPreferenceKey, "")
    }


    fun isUserLoggedIn(): Boolean {
        val sharedPreferences = getSharedPreferences("Kaushlesh", Context.MODE_PRIVATE)
        return sharedPreferences.getBoolean(AppConstants.AP_IS_LOGGED_IN, false)
    }

    fun getAPValueByKeyBoolean(sharedPreferenceKey: String): Boolean {
        val sharedPreferences = getSharedPreferences("Kaushlesh", Context.MODE_PRIVATE)
        return sharedPreferences.getBoolean(sharedPreferenceKey, false)
    }

    fun setAPValueByKey(sharedPreferenceKey: String, value: Boolean) {
        val sharedPreferences = getSharedPreferences("Kaushlesh", Context.MODE_PRIVATE)
        val prefsEditor = sharedPreferences.edit()
        prefsEditor.putBoolean(sharedPreferenceKey, value)
        prefsEditor.apply()
    }


    fun setAPValueByKey(sharedPreferenceKey: String, value: String) {
        val sharedPreferences = getSharedPreferences("Kaushlesh", Context.MODE_PRIVATE)
        val prefsEditor = sharedPreferences.edit()
        prefsEditor.putString(sharedPreferenceKey, value)
        prefsEditor.apply()
    }



    /**
     * Gets the request queue.
     *
     * @return the request queue
     */
    fun getRequestQueue(): RequestQueue {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(applicationContext)
        }

        return this.mRequestQueue!!
    }

    /**
     * Adds the to request queue.
     *
     * @param <T> the generic type
     * @param req the req
     * @param tag the tag
    </T> */
    fun <T> addToRequestQueue(req: Request<T>, tag: String) {
        // set the default tag if tag is empty

        req.tag = TAG

        getRequestQueue().add(req)

        try {
            req.tag = if (TextUtils.isEmpty(tag)) TAG else tag
            getRequestQueue().add(req)
        } catch (e: NullPointerException) {

        }

    }

    /**
     * Adds the to request queue.
     *
     * @param <T> the generic type
     * @param req the req
    </T> */
    fun <T> addToRequestQueue(req: Request<T>) {
        req.tag = TAG
        getRequestQueue().add(req)
    }

    /**
     * Cancel pending requests.
     *
     * @param tag the tag
     */
    fun cancelPendingRequests(tag: Any) {
        if (mRequestQueue != null) {
            mRequestQueue!!.cancelAll(tag)
        }
    }

    override fun onActivityCreated(p0: Activity, p1: Bundle?) {
        onAppForegrounded = true
    }

    override fun onActivityStarted(p0: Activity) {
        onAppForegrounded = true
    }

    override fun onActivityResumed(p0: Activity) {
        onAppForegrounded = true
    }

    override fun onActivityPaused(p0: Activity) {
        onAppForegrounded = false
    }

    override fun onActivityStopped(p0: Activity) {
        onAppForegrounded = false
    }

    override fun onTrimMemory(level: Int) {
        if (level == TRIM_MEMORY_UI_HIDDEN) {
            onAppForegrounded = false
        }
        super.onTrimMemory(level)
    }

    override fun onActivitySaveInstanceState(p0: Activity, p1: Bundle) {
        onAppForegrounded = true
    }

    override fun onActivityDestroyed(p0: Activity) {
        onAppForegrounded = false

    }


    /*override fun attachBaseContext(base: Context) {
        super.attachBaseContext(LocaleManager.setLocale(base))
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        LocaleManager.setLocale(this)
    }*/

}