package com.kaushlesh.service

import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build
import com.kaushlesh.R
import android.provider.Settings
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.kaushlesh.application.KaushleshApplication
import org.json.JSONObject
import java.util.*

class MyFirebaseMessagingService : FirebaseMessagingService() {
    var notificationManager: NotificationManager? = null
    var notificationBuilder: NotificationCompat.Builder? = null
    var context: Context? = null

    val ANDROID_CHANNEL_ID = "com.kaushlesh"
    val ANDROID_CHANNEL_NAME = "ANDROID CHANNEL"

    lateinit var localStorage: LocalStorage

    override fun onNewToken(s: String) {
        super.onNewToken(s)
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { instanceIdResult ->
            val newToken = instanceIdResult.token
            Log.e("LLLL_firebase_Token", "Refreshed token: $newToken")

            localStorage = LocalStorage(applicationContext)
            localStorage.setToken(newToken)
        }
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.e("FIREBSE_111", "onMessageReceived Data" + remoteMessage.data)
        Log.e("FIREBSE_111", "onMessageReceived Notification" + remoteMessage.notification)
        context = this

        try {
            val notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val r = RingtoneManager.getRingtone(applicationContext, notification)
            r.play()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        if (remoteMessage.data.size > 0) {
            Log.d("FIREBSE_111", "Message data payload: " + remoteMessage.data)
        }
        notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            setupChannels()
        }

        // Check if message contains a notification payload.
        if (remoteMessage.notification != null) {
            Log.d("FIREBSE_111", "Message Notification Body: " + remoteMessage.notification!!.body)
        }


        if (!KaushleshApplication.onAppForegrounded) {
            Log.e("FIREBSE_111", java.lang.String.valueOf(KaushleshApplication.onAppForegrounded))
            if (remoteMessage.data.size > 0) {
                sendNotification(remoteMessage.data)
            }
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun setupChannels() {
        val attributes =
            AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).build()
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val adminChannelName: CharSequence = ANDROID_CHANNEL_NAME
        val adminChannel: NotificationChannel
        adminChannel = NotificationChannel(
            ANDROID_CHANNEL_ID,
            adminChannelName,
            NotificationManager.IMPORTANCE_HIGH
        )
        adminChannel.enableLights(true)
        adminChannel.lightColor = Color.RED
        adminChannel.enableVibration(true)
        adminChannel.setSound(defaultSoundUri, attributes)
        if (notificationManager != null) {
            notificationManager!!.createNotificationChannel(adminChannel)
        }
    }


    private fun sendNotification(messageBody: Map<String?, String?>) {
        try {
            Log.d("FIREBSE_111","LLLL_Mess_Body: "+ messageBody.toString() + "")
            val icon =
                BitmapFactory.decodeResource(context!!.resources, R.mipmap.ic_launcher)
            notificationBuilder =
                NotificationCompat.Builder(context!!, ANDROID_CHANNEL_ID)
            val `object` = JSONObject(messageBody)
            //   JSONObject object = new JSONObject(messageBody.toString());
            val message = `object`.optString("description")
            val title = `object`.optString("title")
            //val status = `object`.optString("status")
            //isNotification = true
            val id_notifaciton = System.currentTimeMillis().toInt()
            notificationBuilder!!
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(icon) //a resource for your custom small icon
                .setContentTitle(title)
                .setWhen(System.currentTimeMillis())
                .setShowWhen(true) //the "title" value you sent in your notification
                .setContentText(message) //ditto
                .setAutoCancel(true) //dismisses the notification on click
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
//            val jsonObject = JSONObject(`object`.optString("data"))
//            Log.e("FIREBSE_111","LLLLLL_Data: "+ jsonObject.toString())
//            Log.e("FIREBSE_111","LLLLLL_Data_Forground: "+ jsonObject.toString())


            notificationManager!!.notify(
                id_notifaciton /* ID of notification */,
                notificationBuilder!!.build()
            )
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

  /*  private fun buildContentIntentLike(
        context: Context,
        activityClass: Class<out Activity?>,
        activityClass1: Class<out Activity?>,
        postId: Int
    ): PendingIntent? {
        val intent: Intent
        if (LocalStorage.getIsUserLoggedIn()) {
            intent = Intent(context, activityClass)
            intent.putExtra("postId", postId)
        } else {
            intent = Intent(context, activityClass1)
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    private fun buildContentIntentMessage(
        context: Context,
        activityClass: Class<out Activity?>,
        activityClass1: Class<out Activity?>,
        user: User
    ): PendingIntent? {
        Log.d("className1", "" + activityClass.name)
        val intent: Intent
        if (LocalStorage.getIsUserLoggedIn()) {
            intent = Intent(context, activityClass)
            intent.putExtra("user", user)
        } else {
            intent = Intent(context, activityClass1)
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    private fun buildContentIntentFollow(
        context: Context,
        activityClass: Class<out Activity?>,
        activityClass1: Class<out Activity?>,
        userID: Int
    ): PendingIntent? {
        val intent: Intent
        if (LocalStorage.getIsUserLoggedIn()) {
            intent = Intent(context, activityClass)
            intent.putExtra("userId", userID)
        } else {
            intent = Intent(context, activityClass1)
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }
*/







}