package com.kaushlesh.widgets

import android.content.Context
import android.graphics.Rect
import android.graphics.Typeface
import android.text.InputType
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.kaushlesh.R
import kotlinx.android.synthetic.main.activity_ap_villa_sell_and_rent.view.*


class CustomEditText : EditText {

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setCustomFont(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        setCustomFont(context, attrs)
    }

    private fun setCustomFont(ctx: Context, attrs: AttributeSet) {
        val a = ctx.obtainStyledAttributes(attrs, R.styleable.CustomEditText)
        val customFont = a.getString(R.styleable.CustomEditText_customFontEditText)
        setCustomFont(ctx, customFont)
        a.recycle()
    }

    fun setCustomFont(ctx: Context, asset: String?): Boolean {
        val tf: Typeface?
        try {
            tf = Typeface.createFromAsset(ctx.assets, "fonts/" + asset!!)
        } catch (e: Exception) {
            Log.e("CustomTextView", "Could not get typeface: " + e.message)
            return false
        }

        typeface = tf
        return true
    }

    override fun setLongClickable(longClickable: Boolean) {
        if (inputType == InputType.TYPE_TEXT_VARIATION_PASSWORD) {
            super.setLongClickable(false)
        } else {
            super.setLongClickable(true)
        }
    }


    override fun onFocusChanged(focused: Boolean, direction: Int, previouslyFocusedRect: Rect?) {

        super.onFocusChanged(focused, direction, previouslyFocusedRect)
        if (focused) {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
            setBackgroundResource(R.drawable.bg_edittext_black)
            setPadding(35, 0, 35, 0)
        } else {
            //val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            //imm.hideSoftInputFromWindow(this.windowToken, 0)
            if (text.isNullOrEmpty()) {
                setBackgroundResource(R.drawable.bg_edittext)
                setPadding(35, 0, 35, 0)
            }
        }
        super.onFocusChanged(focused, direction, previouslyFocusedRect)
    }




/*    @Override fun onTouch(v: View, event : MotionEvent) : Boolean{
        super.onTouchEvent(event)
        view.parent.requestDisallowInterceptTouchEvent(true)
        if (event.getAction() and MotionEvent.ACTION_UP !== 0 && event.getActionMasked() and MotionEvent.ACTION_UP !== 0) {
            view.parent.requestDisallowInterceptTouchEvent(false)
        }
        return false
    }*/

/*    override fun onTextChanged(
        text: CharSequence,
        start: Int,
        lengthBefore: Int,
        lengthAfter: Int
    ) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter)

            if (text.toString().length == 0) {
                setBackgroundResource(R.drawable.bg_edittext)
                setPadding(20, 0, 20, 0)

            } else {
                setBackgroundResource(R.drawable.bg_edittext_black)
                setPadding(20, 0, 20, 0)
            }
    }*/

}
