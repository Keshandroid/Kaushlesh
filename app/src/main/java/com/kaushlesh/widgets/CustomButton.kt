package com.kaushlesh.widgets

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.Button
import android.widget.TextView
import com.kaushlesh.R
import java.util.*

class CustomButton : Button {

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setCustomFont(this, context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        setCustomFont(this, context, attrs)
    }

    companion object {


        /**
         * Sets a font on a textview based on the custom com.my.package:font attribute
         * If the custom font attribute isn't found in the attributes nothing happens
         *
         * @param textview
         * @param context
         * @param attrs
         */
        fun setCustomFont(textview: TextView, context: Context, attrs: AttributeSet) {
            val a = context.obtainStyledAttributes(attrs, R.styleable.CustomButton)
            val font = a.getString(R.styleable.CustomButton_customFontButton)
            setCustomFont(textview, font, context)
            a.recycle()
        }

        /**
         * Sets a font on a textview
         *
         * @param textview
         * @param font
         * @param context
         */
        fun setCustomFont(textview: TextView, font: String?, context: Context) {
            if (font == null) {
                return
            }
            val tf = getFontCache(font, context)
            if (tf != null) {
                textview.typeface = tf
            }
        }

        private val fontCache = Hashtable<String, Typeface>()

        fun getFontCache(name: String, context: Context): Typeface? {
            var tf = fontCache[name]
            if (tf == null) {
                try {
                    tf = Typeface.createFromAsset(context.assets, "fonts/$name")
                } catch (e: Exception) {
                    return null
                }

                fontCache[name] = tf
            }
            return tf
        }
    }


}
