package com.kaushlesh.widgets

import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView


class CustomImageView(context: Context, attrs: AttributeSet?) : ImageView(context, attrs) {


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        try {
            val drawable = drawable
            if (drawable == null) {
                setMeasuredDimension(0, 0)
            } else {
                val imageSideRatio = drawable.intrinsicWidth.toFloat() / drawable.intrinsicHeight.toFloat()
                val viewSideRatio = MeasureSpec.getSize(widthMeasureSpec).toFloat() / MeasureSpec.getSize(heightMeasureSpec).toFloat()
                if (imageSideRatio >= viewSideRatio) {
                    // Image is wider than the display (ratio)
                    val width = MeasureSpec.getSize(widthMeasureSpec)
                    val height = (width / imageSideRatio).toInt()
                    setMeasuredDimension(width, height)
                } else {
                    // Image is taller than the display (ratio)
                    val height = MeasureSpec.getSize(heightMeasureSpec)
                    val width = (height * imageSideRatio).toInt()
                    setMeasuredDimension(width, height)
                }
            }
        } catch (e: Exception) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        }
    }
}