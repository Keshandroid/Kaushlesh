package com.kaushlesh.utils

import android.content.Context
import android.preference.PreferenceManager
import com.google.gson.Gson
import org.json.JSONObject


object Identity {

    private val PACKAGE_PURCHASE_ERROR = "PACKAGE_PURCHASE_ERROR"
    private val CONTACT_US_NUMBER = "CONTACT_US_NUMBER"
    private val UPDATE_DIALOG = "UPDATE_DIALOG"
    private val PERMISSION_DIALOG = "PERMISSION_DIALOG"
    private val RATE_US_DIALOG = "RATE_US_DIALOG"



    fun setPackagePurchase(mContext: Context?, jsonObject: JSONObject) {
        val mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext)
        val gson = Gson()
        val json = gson.toJson(jsonObject)
        mPreferences.edit().putString(PACKAGE_PURCHASE_ERROR, json).apply()
    }

    fun getPackagePurchase(mContext: Context?): String? {
        val mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext)
        val json: String? = mPreferences.getString(PACKAGE_PURCHASE_ERROR, "")
        return json
    }



    fun setContactUsNumber(mContext: Context?, contactUsStr: String) {
        val mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext)
        mPreferences.edit().putString(CONTACT_US_NUMBER, contactUsStr).apply()
    }

    fun getContactUsNumber(mContext: Context?): String? {
        val mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext)
        return mPreferences.getString(CONTACT_US_NUMBER, "")
    }


    fun setUpdateDialog(mContext: Context?, updateDialog: String) {
        val mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext)
        mPreferences.edit().putString(UPDATE_DIALOG, updateDialog).apply()
    }

    fun getUpdateDialog(mContext: Context?): String? {
        val mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext)
        return mPreferences.getString(UPDATE_DIALOG, "")
    }


    fun setPermssionDialog(mContext: Context?, permissionDialog: String) {
        val mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext)
        mPreferences.edit().putString(PERMISSION_DIALOG, permissionDialog).apply()
    }

    fun getPermssionDialog(mContext: Context?): String? {
        val mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext)
        return mPreferences.getString(PERMISSION_DIALOG, "")
    }

    fun setRateUsDialog(mContext: Context?, rateUsDialog: String) {
        val mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext)
        mPreferences.edit().putString(RATE_US_DIALOG, rateUsDialog).apply()
    }

    fun getRateUsDialog(mContext: Context?): String? {
        val mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext)
        return mPreferences.getString(RATE_US_DIALOG, "")
    }

}