package com.kaushlesh.utils

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import org.json.JSONArray
import java.io.Serializable

class BusinessProfileData: Serializable {

    var bussinessprofile_id: String? = null

    var userId: String? = null
    var userToken: String =""
    var location_id: String? = null
    lateinit var bussiness_name: String
    lateinit var bussiness_about: String
    lateinit var service_info: String
    lateinit var mobile_no: String

    var is_calls_allow: Int? = null
    var is_whatsapp_calls_allow: Int? = null
    var whatsapp_inquiry_allow: Int? = null //
    var same_whatsapp_no: Int? = null //
    lateinit var whatsapp_no: String //
    var whatsapp_inquiry_timing_same: Int? = null
    lateinit var email: String
    lateinit var address1: String
    lateinit var address2: String
    lateinit var city: String
    lateinit var state: String
    var visitingcard_front: Drawable? = null
    var visitingcard_back: Drawable? = null

    var is_change_front_img: Int? = null
    var is_change_back_img: Int? = null


    var bussiness_call_timing: ArrayList<CallTiming>? = null
    var whatsapp_inquiry_call_timing: ArrayList<CallTiming>? = null




}