package com.kaushlesh.utils

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.ContentValues
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.provider.MediaStore
import android.text.Html
import android.util.Log
import android.util.Patterns
import android.view.*
import android.view.WindowManager.BadTokenException
import android.widget.*
import androidx.core.app.ActivityCompat
import com.kaushlesh.BuildConfig
import com.kaushlesh.Controller.MyPostShowcaseDeleteController
import com.kaushlesh.R
import com.kaushlesh.application.KaushleshApplication
import com.kaushlesh.constant.AppConstants.printLog
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.widgets.CustomButton
import com.kaushlesh.widgets.CustomEditText
import com.kaushlesh.widgets.CustomTextView
import java.io.ByteArrayOutputStream
import java.io.File
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

object Utils {
    var progressDialog: ProgressDialog? = null

    const val inputFormat = "HH:mm"
    var inputParser = SimpleDateFormat(inputFormat, Locale.US)


    fun sharePostLink(categoryId: String, subCategoryId: String, postId: String, postUrl: String,postTitle: String, context: Context) {

//        val deepLinkUrl = "www.gujaratliving.com/post/"+categoryId+"/"+subCategoryId+"/"+postId
        val deepLinkUrl = "https://gujaratliving.com/post/"+categoryId+"/"+subCategoryId+"/"+postId //(for Android to IOS & IOS to Android sharing (also enable it from Manifiest file))

        val data = "<b>*Gujarat Living*</b> <br>" +
                " I found this post on Gujarat Living <br>" +
                "============================ <br>" +
                "" + postTitle + " <br> <br> " +
                "Post Link : " + deepLinkUrl + " <br> <br> " +
                " For posting more free ads Download App Now <br>" +
                " Android App : https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID} <br> <br>" +
                " IOS App : https://apps.apple.com/us/app/gujarat-living/id1621448068 "

        val i = Intent(Intent.ACTION_SEND)
        i.type = "text/plain"
        i.putExtra(Intent.EXTRA_TITLE, "Gujarat Living")
        i.putExtra(Intent.EXTRA_SUBJECT, "Hey! Check out this post from Gujarat Living")
        i.putExtra(Intent.EXTRA_TEXT, ""+ Html.fromHtml(data))
        context.startActivity(Intent.createChooser(i, "Share this POST"))

    }


    fun shareMyProfileLink(linkType: String, userId: String, context: Context) {

//        val deepLinkUrl = "www.gujaratliving.com/"+linkType+"/"+userId
        val deepLinkUrl = "https://gujaratliving.com/"+linkType+"/"+userId

        val data = "<b>*Gujarat Living*</b> <br>" +
                " I have my profile on Gujarat Living. View my profile and Follow My Network <br>" +
                "============================ <br>" +
                "Profile Link : " + deepLinkUrl + " <br>" +
                " For create your profile Download App Now <br>" +
                " Android App : https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID} <br>" +
                " IOS App : https://apps.apple.com/us/app/gujarat-living/id1621448068 "

        val i = Intent(Intent.ACTION_SEND)
        i.type = "text/plain"
        i.putExtra(Intent.EXTRA_TITLE, "")
        i.putExtra(Intent.EXTRA_SUBJECT, "")
        i.putExtra(Intent.EXTRA_TEXT, ""+ Html.fromHtml(data))
        context.startActivity(Intent.createChooser(i, "Share Profile"))

    }

    fun shareOtherUserProfileLink(linkType: String, userId: String, context: Context) {

//        val deepLinkUrl = "www.gujaratliving.com/"+linkType+"/"+userId
        val deepLinkUrl = "https://gujaratliving.com/"+linkType+"/"+userId

        val data = "<b>*Gujarat Living*</b> <br>" +
                " I have found this profile on Gujarat Living. View this profile and Follow that Network  <br>" +
                "============================ <br>" +
                "Profile Link : " + deepLinkUrl + " <br>" +
                " For create your profile Download App Now <br>" +
                " Android App : https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID} <br>" +
                " IOS App : https://apps.apple.com/us/app/gujarat-living/id1621448068 "

        val i = Intent(Intent.ACTION_SEND)
        i.type = "text/plain"
        i.putExtra(Intent.EXTRA_TITLE, "")
        i.putExtra(Intent.EXTRA_SUBJECT, "")
        i.putExtra(Intent.EXTRA_TEXT, ""+ Html.fromHtml(data))
        context.startActivity(Intent.createChooser(i, "Share Profile"))

    }

    fun shareMyBusinessLink(linkType: String, userId: String, context: Context) {

//        val deepLinkUrl = "www.gujaratliving.com/"+linkType+"/"+userId
        val deepLinkUrl = "https://gujaratliving.com/"+linkType+"/"+userId

        val data = "<b>*Gujarat Living*</b> <br>" +
                " I have my business profile on Gujarat Living. View my business profile and Follow my Network  <br>" +
                "============================ <br>" +
                "Business Profile Link : " + deepLinkUrl + " <br>" +
                " For create your own business profile Download App Now <br>" +
                " Android App : https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID} <br>" +
                " IOS App : https://apps.apple.com/us/app/gujarat-living/id1621448068 "

        val i = Intent(Intent.ACTION_SEND)
        i.type = "text/plain"
        i.putExtra(Intent.EXTRA_TITLE, "")
        i.putExtra(Intent.EXTRA_SUBJECT, "")
        i.putExtra(Intent.EXTRA_TEXT, ""+ Html.fromHtml(data))
        context.startActivity(Intent.createChooser(i, "Share Business Profile"))

    }

    fun shareOtherUserBusinessLink(linkType: String, userId: String, context: Context) {

//        val deepLinkUrl = "www.gujaratliving.com/"+linkType+"/"+userId
        val deepLinkUrl = "https://gujaratliving.com/"+linkType+"/"+userId

        val data = "<b>*Gujarat Living*</b> <br>" +
                " I have found this business profile on Gujarat Living. View this business profile and Follow that Network  <br>" +
                "============================ <br>" +
                "Business Profile Link : " + deepLinkUrl + " <br>" +
                " For create your own business profile Download App Now <br>" +
                " Android App : https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID} <br>" +
                " IOS App : https://apps.apple.com/us/app/gujarat-living/id1621448068 "

        val i = Intent(Intent.ACTION_SEND)
        i.type = "text/plain"
        i.putExtra(Intent.EXTRA_TITLE, "")
        i.putExtra(Intent.EXTRA_SUBJECT, "")
        i.putExtra(Intent.EXTRA_TEXT, ""+ Html.fromHtml(data))
        context.startActivity(Intent.createChooser(i, "Share Business Profile"))

    }




    fun internetAlert(activity: Activity) {
        AlertDialog.Builder(activity)
                .setMessage(activity.getString(R.string.txt_check_internet))
                .setPositiveButton(activity.resources.getString(R.string.ok), null)
                .create()
                .show()
    }

    fun isOnline(context: Context): Boolean {
        val connectivity = context
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val info = connectivity.allNetworkInfo
        for (i in info.indices) if (info[i].state == NetworkInfo.State.CONNECTED) {
            return true
        }
        return false
    }

    fun getFileDataFromDrawable(context: Context, drawable: Drawable): ByteArray {
        val bitmap = (drawable as BitmapDrawable).bitmap
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream)
        return byteArrayOutputStream.toByteArray()
    }

     fun getFileDataFromDrawable(bitmap: Bitmap): ByteArray {
        val byteArrayOutputStream = ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray()
    }


    fun parseDate(date: String): Date? {
        return try {
            inputParser.parse(date)
        } catch (e: ParseException) {
            Date(0)
        }
    }



    fun isEmpty(view: View?): Boolean {
        if (view is EditText) {
            if (view.text.toString().trim { it <= ' ' }.length == 0) {
                return true
            }
        } else if (view is Button) {
            if (view.text.toString().length == 0) {
                return true
            }
        } else if (view is TextView) {
            if (view.text.toString().trim { it <= ' ' }.length == 0) {
                return true
            }
        }
        return false
    }

    fun isEmpty(str: String): Boolean {
        return if (str.trim { it <= ' ' }.length == 0) {
            true
        } else false
    }

    fun isValidEmail(email: String?): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }


    fun GujaratiToEnglish(str: String):String {
        var result = ""
        var en = '0'
        for (ch in str) {
            en = ch

            when (ch) {
                '૦' -> en = '0'
                '૧' -> en = '1'
                '૨' -> en = '2'
                '૩' -> en = '3'
                '૪' -> en = '4'
                '૫' -> en = '5'
                '૬' -> en = '6'
                '૭' -> en = '7'
                '૮' -> en = '8'
                '૯' -> en = '9'
                '०' -> en = '0'
                '१' -> en = '1'
                '२' -> en = '2'
                '३' -> en = '3'
                '४' -> en = '4'
                '५' -> en = '5'
                '६' -> en = '6'
                '७' -> en = '7'
                '८' -> en = '8'
                '९' -> en = '9'
            }
            result = "${result}$en"
        }
        return result
    }

    fun showProgressDialogWithTitle(activity: Activity?, substring: String) {
        progressDialog =  ProgressDialog(activity, R.style.AppCompatAlertDialogStyle)
        progressDialog!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        //Without this user can hide loader by tapping outside screen
        progressDialog!!.setCancelable(false)
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setMessage(substring)
        progressDialog!!.show()
    }

    // Method to hide/ dismiss Progress bar
    fun hideProgressDialogWithTitle() {
        if (progressDialog!!.isShowing()) {
            progressDialog!!.dismiss();
        }
    }

    fun showProgress(activity: Activity?, message: String?) {
  /*      progressDialog =  ProgressDialog(activity);
        progressDialog!!.setIndeterminate(false);
        progressDialog!!.setCancelable(false);
        progressDialog!!.setMessage(message);
        progressDialog!!.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        progressDialog!!.show();*/

        val llPadding = 30
        val ll = LinearLayout(activity)
        ll.setOrientation(LinearLayout.HORIZONTAL)
        ll.setPadding(llPadding, llPadding, llPadding, llPadding)
        ll.setGravity(Gravity.CENTER)
        var llParam: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT)
        llParam.gravity = Gravity.CENTER
        ll.setLayoutParams(llParam)

        val progressBar = ProgressBar(activity)
        progressBar.setIndeterminate(true)
        progressBar.setPadding(0, 0, llPadding, 0)
        progressBar.setLayoutParams(llParam)

        llParam = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT)
        llParam.gravity = Gravity.CENTER
        val tvText = TextView(activity)
        tvText.text = message
        tvText.setTextColor(Color.parseColor("#000000"))
        tvText.textSize = 12f
        tvText.layoutParams = llParam

        ll.addView(progressBar)
        ll.addView(tvText)

        val builder = AlertDialog.Builder(activity)
        builder.setCancelable(true)
        builder.setView(ll)

        val dialog = builder.create()
        dialog.show()
        val window: Window? = dialog.window
        if (window != null) {
            val layoutParams: WindowManager.LayoutParams = WindowManager.LayoutParams()
            layoutParams.copyFrom(dialog.window!!.attributes)
            layoutParams.width = LinearLayout.LayoutParams.WRAP_CONTENT
            layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT
            dialog.window!!.attributes = layoutParams
        }

    }

    fun hideDialog(activity: Activity?)
    {
        val builder = AlertDialog.Builder(activity)
        val dialog = builder.create()
        dialog.dismiss()
    }

    fun showProgress(activity: Activity?) { /*   progressDialog = new ProgressDialog(activity);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(message);
        progressDialog.getWindow()
                .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();*/
        if (!isProgressShowing()) {
            progressDialog = ProgressDialog(activity)
            try {
                progressDialog!!.show()
            } catch (e: BadTokenException) {
            }
            progressDialog!!.setCancelable(false)
            progressDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            progressDialog!!.setContentView(R.layout.progressdialog)
        }
    }

    fun isProgressShowing(): Boolean {
        if (progressDialog == null)
            return false
        else {
            return progressDialog!!.isShowing
        }
    }

    fun dismissProgress() {
        try {
            if (progressDialog != null && progressDialog!!.isShowing) progressDialog!!.dismiss() else Log.i("Dialog", "already dismissed")
        } catch (e: Exception) {
        }
    }

    fun showAlert(activity: Activity, message: String?) {
        AlertDialog.Builder(activity, R.style.AlertDialogTheme)
                .setMessage(message)
                .setPositiveButton(activity.resources.getString(R.string.ok), null)
                .create()
                .show()
    }

    fun showToast(activity: Activity?, message: String?) {
        //Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
        try {
            val toast = Toast(activity)
            val view: View = LayoutInflater.from(activity).inflate(R.layout.toast_custom, null)
            val textView =
                    view.findViewById<View>(R.id.custom_toast_text) as TextView
            textView.text = message
            toast.setView(view)
            // toast.setGravity(Gravity.BOTTOM or Gravity.CENTER, 0, 0)
            toast.setDuration(Toast.LENGTH_LONG)
            toast.show()
        }catch (e: Exception){
            e.printStackTrace()
        }

    }

    fun showLog(msg: String?, value: String?) {
        Log.e(msg, value.toString())
    }

    fun showAlert(activity: Activity, message: String?, listener: DialogInterface.OnClickListener?) {
        AlertDialog.Builder(activity, R.style.AlertDialogTheme)
                .setMessage(message)
                .setPositiveButton(activity.resources.getString(R.string.ok), listener)
                //.setNegativeButton(R.string.cancel) { dialogInterface, i -> dialogInterface.dismiss() }
                .create()
                .show()


    }

    /*public static void showLogoutAlert(final Activity activity, String message,
                                       final StoreUserData storeUserData) {
        new AlertDialog.Builder(activity)
                .setMessage(message)
                .setPositiveButton(activity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        activity.startActivity(new Intent(activity, LoginActivity.class)
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        storeUserData.clearData(activity);
                    }
                })
                .create()
                .show();
    }*/

    /*public static void showLogoutAlert(final Activity activity, String message,
                                       final StoreUserData storeUserData) {
        new AlertDialog.Builder(activity)
                .setMessage(message)
                .setPositiveButton(activity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        activity.startActivity(new Intent(activity, LoginActivity.class)
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        storeUserData.clearData(activity);
                    }
                })
                .create()
                .show();
    }*/

    fun getImageContentUri(context: Context, imageFile: File): Uri? {
        val filePath = imageFile.absolutePath
        val cursor = context.contentResolver.query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, arrayOf(MediaStore.Images.Media._ID),
                MediaStore.Images.Media.DATA + "=? ", arrayOf(filePath), null)
        return if (cursor != null && cursor.moveToFirst()) {
            val id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID))
            cursor.close()
            Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "" + id)
        } else {
            if (imageFile.exists()) {
                val values = ContentValues()
                values.put(MediaStore.Images.Media.DATA, filePath)
                context.contentResolver.insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
            } else {
                null
            }
        }
    }


    fun hasPermission(perm: String?, activity: Activity?): Boolean {
        return PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(activity!!, perm!!)
    }

    /*  public static boolean isGooglePlayServicesAvailable(Activity activity) {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, activity, 0).show();
            return false;
        }
    }*/

    /*  public static boolean isGooglePlayServicesAvailable(Activity activity) {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, activity, 0).show();
            return false;
        }
    }*/
    fun parseDate(time: String?, inputPattern: String?, outputPattern: String?): String? {
        val inputFormat = SimpleDateFormat(inputPattern, Locale.getDefault())
        val outputFormat = SimpleDateFormat(outputPattern, Locale.getDefault())
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(time)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }

    fun getDate(strDate: String?): Date? {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        return try {
            inputFormat.parse(strDate)
        } catch (e: ParseException) {
            e.printStackTrace()
            Date()
        }
    }

    fun ConvertDate(current_format: String?, date: String?, new_format: String?): String? {
        return try {
            val sdf_current = SimpleDateFormat(current_format)
            val sdf_new = SimpleDateFormat(new_format)
            val get_date = sdf_current.parse(date)
            sdf_new.format(get_date)
        } catch (e: Exception) {
            e.printStackTrace()
            "Error:" + e.message
        }
    }

    fun getLocalDate(dateString: String?): String? {
        val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        formatter.timeZone = TimeZone.getTimeZone("UTC")
        var value: Date? = null
        var dt = dateString
        try {
            value = formatter.parse(dateString)
            val dateFormatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
            dateFormatter.timeZone = TimeZone.getDefault()
            dt = dateFormatter.format(value)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return dt
    }

    fun getLocalDate(dateString: String?, new_format: String?): String? {
        val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        formatter.timeZone = TimeZone.getTimeZone("UTC")
        var value: Date? = null
        var dt = dateString
        try {
            value = formatter.parse(dateString)
            val dateFormatter = SimpleDateFormat(new_format, Locale.getDefault())
            dateFormatter.timeZone = TimeZone.getDefault()
            dt = dateFormatter.format(value)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return dt
    }

    fun formateDateFromstring(inputFormat: String, outputFormat: String, expdate: String): String {
        val parsed: Date
        var outputDate = ""

        val df_input = SimpleDateFormat(inputFormat, java.util.Locale.getDefault())
        val df_output = SimpleDateFormat(outputFormat, java.util.Locale.getDefault())

        try {
            parsed = df_input.parse(expdate)
            outputDate = df_output.format(parsed)
            printLog("TAG", "output date$outputDate")

        } catch (e: ParseException) {
            printLog("TAG", "ParseException - dateFormat")
        }


        return outputDate
    }

    fun setButtonEnabled(btn: CustomButton, value: Boolean) {
        btn.isEnabled = value
    }

    fun setTextviewEnabled(tv: CustomTextView, value: Boolean) {
        tv.isEnabled = value
    }

    fun deletePost(
            activity: Activity,
            listener: ParseControllerListener,
            postId: String
    ) {
        val myPostShowcaseDeleteController = MyPostShowcaseDeleteController(activity, listener)
        myPostShowcaseDeleteController.add("post", postId.toInt())
    }

    fun setontouch(editText: CustomEditText) {
        editText.setOnTouchListener(View.OnTouchListener { v, event ->
            if (v.id == R.id.et_other_info) {
                v.parent.requestDisallowInterceptTouchEvent(true)
                when (event.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_UP -> v.parent
                            .requestDisallowInterceptTouchEvent(false)
                }
            }
            false
        })

    }

    fun setontouch1(editText: CustomEditText) {
        editText.setOnTouchListener(View.OnTouchListener { v, event ->
            if (v.id == R.id.et_ad_title) {
                v.parent.requestDisallowInterceptTouchEvent(true)
                when (event.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_MOVE -> v.parent
                            .requestDisallowInterceptTouchEvent(false)
                }
            }
            false
        })

    }

    fun setontouchAuto(editText: AutoCompleteTextView) {
        editText.setOnTouchListener(View.OnTouchListener { v, event ->
            if (v.id == R.id.act_location) {
                v.parent.requestDisallowInterceptTouchEvent(true)
                when (event.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_UP -> v.parent
                            .requestDisallowInterceptTouchEvent(false)
                }
            }
            false
        })

    }

    fun openFollowingUserDetails(
            context: Context,
            intent: Intent,
            profileId: String
    ) {
        intent.putExtra("profileId", profileId)
        context.startActivity(intent)
    }

    fun getLeftDays(deleteDate: String): Int {
            val format = SimpleDateFormat("dd/MM/yyyy")

            val date1= format.parse(deleteDate)
            System.out.println(date1)
            val millionSeconds = date1.time - Calendar.getInstance().timeInMillis
            val leftDays = TimeUnit.MILLISECONDS.toDays(millionSeconds).toInt()
            System.out.println("======" + leftDays)

            /*val diff: Long = date1.getTime() - Calendar.getInstance().timeInMillis
            val seconds = diff / 1000
            val minutes = seconds / 60
            val hours = minutes / 60
            val leftDays = hours / 24
            System.out.println("======"+leftDays)*/

        return leftDays
    }

    fun showAlertPostError(context: Activity) {
        val dialog = Dialog(context)
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_logout)
        // Set dialog title
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        //dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_white);
        // set values for custom dialog components - text, image and button

        val tvtitle = dialog.findViewById(R.id.tv_title) as TextView
        tvtitle.setText(context.resources.getText(R.string.txt_check_internet_try_again))
        val btnyes = dialog.findViewById(R.id.btn_yes) as TextView
        btnyes.text = context.resources.getText(R.string.txt_ok)
        val btnno = dialog.findViewById(R.id.btn_no) as TextView
        btnno.visibility = View.GONE

        btnyes.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
        })

        dialog.show()
    }

    fun showAlertPackageError(context: Activity) {
        val dialog = Dialog(context)
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_logout)
        // Set dialog title
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        //dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_white);
        // set values for custom dialog components - text, image and button

        val tvtitle = dialog.findViewById(R.id.tv_title) as TextView
        tvtitle.setText(context.resources.getText(R.string.txt_check_internet_try_again_package_safe))
        val btnyes = dialog.findViewById(R.id.btn_yes) as TextView
        btnyes.text = context.resources.getText(R.string.txt_ok)
        val btnno = dialog.findViewById(R.id.btn_no) as TextView
        btnno.visibility = View.GONE

        btnyes.setOnClickListener(View.OnClickListener {
            dialog.dismiss()


        })

        dialog.show()
    }

    fun isConnected(): Boolean {
        val cm = KaushleshApplication.applicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager ?: return false
        val activeNetwork = cm.activeNetworkInfo
        return (activeNetwork != null
                && activeNetwork.isConnectedOrConnecting)
    }

    fun showAlertConnection(context: Activity) {

        try {
            val dialog = Dialog(context)
            // Include dialog.xml file
            dialog.setContentView(R.layout.dialog_logout)
            // Set dialog title
            dialog.setTitle("")
            dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            //dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_white);
            // set values for custom dialog components - text, image and button

            val tvtitle = dialog.findViewById(R.id.tv_title) as TextView
            tvtitle.setText(context.resources.getText(R.string.txt_check_internet))
            val btnyes = dialog.findViewById(R.id.btn_yes) as TextView
            btnyes.text = context.resources.getText(R.string.txt_ok)
            val btnno = dialog.findViewById(R.id.btn_no) as TextView
            btnno.visibility = View.GONE

            btnyes.setOnClickListener(View.OnClickListener {
                dialog.dismiss()

            })

            dialog.show()
        }catch (e: Exception){
            e.printStackTrace()
        }


    }

    fun showLocationAlert(context: Activity, title: String) {
        val dialog = Dialog(context)
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_logout)
        // Set dialog title
        dialog.setTitle("")
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        //dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_white);
        // set values for custom dialog components - text, image and button

        val tvtitle = dialog.findViewById(R.id.tv_title) as TextView
        tvtitle.setText(title)
        val btnyes = dialog.findViewById(R.id.btn_yes) as TextView
        btnyes.text = context.resources.getText(R.string.txt_ok)
        val btnno = dialog.findViewById(R.id.btn_no) as TextView
        btnno.visibility = View.GONE

        btnyes.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
        })

        dialog.show()
    }

    fun getViewAllId(categoryId: Int?): Int {
        when(categoryId) {
            1 -> {
                return 1001
            }
            2 -> {
                return 1002
            }
            3 -> {
                return 1003
            }
            4 -> {
                return 1004
            }
            5 -> {
                return 1005
            }
            6 -> {
                return 1006
            }
            7 -> {
                return 1007
            }
            8 -> {
                return 1008
            }
            9 -> {
                return 1009
            }
            10 -> {
                return 1010
            }
            11 -> {
                return 1011
            }
            12 -> {
                return 1012
            }
        }
        return 0
    }


    /* public static void serverError(Activity activity, int code) {
        dismissProgress();
        String message = "";
        switch (code) {
            case 400:
                message = "400 - Bad Request";
                break;
            case 401:
                message = "401 - Unauthorized";
                break;
            case 404:
                message = "404 - Not Found";
                break;
            case 500:
                message = "500 - Internal Server Error";
                break;
            default:
                message = activity.getString(R.string.server_error);
        }
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }*/


}