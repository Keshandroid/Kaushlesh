package com.kaushlesh.utils.PostDetail

import android.text.Html
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.View
import com.kaushlesh.widgets.CustomTextView
import kotlinx.android.synthetic.main.comman_product_details_bottom.*

object GetDetailsFromValue {

    fun getCategoryFromId(catid: Int) : String{
        when(catid)
        {
            2->
            {
                return "Properties"
            }
        }
        return ""
    }

    fun getPropertyType(id: Int) : String{
        when(id)
        {
            1->
            {
                return "Appartment"
            }
            2->
            {
                return "Builder Floors"
            }
            3->
            {
                return "Farm House"
            }
            4->
            {
                return "House and Villa"
            }
            5->
            {
                return "Agriculture"
            }
            6->
            {
                return "Commercial"
            }
            7->
            {
                return "Residential"
            }
            8->
            {
                return "Hotel"
            }
            9->
            {
                return "Resort"
            }
            10->
            {
                return "Ware House"
            }
            11->
            {
                return "Labour Camp"
            }
            12->
            {
                return "Guest House"
            }
            13->
            {
                return "PG"
            }
            14->
            {
                return "Roommate"
            }
        }
        return "-"
    }

    fun getBedrooms(id: Int) : String{
        when(id)
        {
            1->
            {
                return "1"
            }
            2->
            {
                return "2"
            }
            3->
            {
                return "3"
            }
            4->
            {
                return "4"
            }
            5->
            {
                return "4+"
            }
        }
        return "-"
    }

    fun getBathrooms(id: Int) : String{
        when(id)
        {
            1->
            {
                return "1"
            }
            2->
            {
                return "2"
            }
            3->
            {
                return "3"
            }
            4->
            {
                return "4"
            }
            5->
            {
                return "4+"
            }
        }
        return "-"
    }

    fun getFurnishing(id: Int) : String{
        when(id)
        {
            1->
            {
                return "Furnished"
            }
            2->
            {
                return "Semi-Furnished"
            }
            3->
            {
                return "Unfurnished"
            }
        }
        return "-"
    }

    fun getConstructionStatus(id: Int) : String{
        when(id)
        {
            1->
            {
                return "New Launch"
            }
            2->
            {
                return "Ready to Shift"
            }
            3->
            {
                return "Under Construction"
            }
        }
        return "-"
    }

    fun getListedBy(id: Int) : String{
        when(id)
        {
            1->
            {
                return "Builder"
            }
            2->
            {
                return "Dealer"
            }
            3->
            {
                return "Owner"
            }
        }
        return "-"
    }

    fun getYesNo(id: Int) : String{
        when(id)
        {
            1->
            {
                return "Yes"
            }
            2->
            {
                return "No"
            }
        }
        return "-"
    }

    fun getCarParking(id: Int) : String{
        when(id)
        {
            0->
            {
                return "-"
            }
            1->
            {
                return "0"
            }
            2->
            {
                return "1"
            }
            3->
            {
                return "2"
            }
            4->
            {
                return "3"
            }
            5->
            {
                return "3+"
            }
        }
        return "-"
    }

    fun getFacing(id: Int) : String{
        when(id)
        {
            1->
            {
                return "North"
            }
            2->
            {
                return "South"
            }
            3->
            {
                return "East"
            }
            4->
            {
                return "West"
            }
        }
        return "-"
    }

    fun getPurpose(id: Int) : String{
        when(id)
        {
            1->
            {
                return "Rent"
            }
            2->
            {
                return "Sell"
            }
            3->
            {
                return "Booking"
            }
        }
        return "-"
    }

    fun getTypeofRoom(id: Int) : String{
        when(id)
        {
            1->
            {
                return "Delux"
            }
            2->
            {
                return "Super Delux"
            }
            3->
            {
                return "Suit"
            }
            4->
            {
                return "Other"
            }
            5->
            {
                return "All"
            }
        }
        return "-"
    }

    fun getHotelFacility(id: Int) : String{
        when(id)
        {
            1->
            {
                return "Food"
            }
            2->
            {
                return "GYM"
            }
            3->
            {
                return "Card Payment"
            }
            4->
            {
                return "Wifi"
            }
            5->
            {
                return "Spa"
            }
            6->
            {
                return "Swimming Pool"
            }
            7->
            {
                return "Security"
            }
            8->
            {
                return "Fire & Safety"
            }
            9->
            {
                return "Room Service"
            }
            10->
            {
                return "Laundry"
            }
            11->
            {
                return "Power Backup"
            }
        }
        return "-"
    }

    fun checkValue(id: Int) : String{
        when(id)
        {
            0->
            {
                return "-"
            }
        }
        return id.toString()
    }

    fun getMobileType(id: Int): String {

        when(id)
        {
            1->
            {
                return "Mobile"
            }
            2->
            {
                return "Tablet"
            }
        }
        return "-"
    }

    fun getBikesFuel(id: Int): String {
        when(id)
        {
            1->
            {
                return "Petrol"
            }
            2->
            {
                return "Electric"
            }
        }
        return "-"
    }

    fun getTransmission(id: Int): String {
        when(id)
        {
            1->
            {
                return "Automatic"
            }
            2->
            {
                return "Manual"
            }
        }
        return "-"

    }

    fun getNoofOwners(id: Int): String {
        when(id)
        {
            1->
            {
                return "1st"
            }
            2->
            {
                return "2nd"
            }
            3->
            {
                return "3rd"
            }
            4->
            {
                return "4th"
            }
            5->
            {
                return "4th +"
            }
        }
        return "-"
    }

    fun getInsuranceType(id: Int): String {
        when(id)
        {
            1->
            {
                return "Comprehensive"
            }
            2->
            {
                return "Third Party"
            }
            3->
            {
                return "Zero Dep"
            }
        }
        return "-"
    }

    fun getPropertyName(name: String): String {

        when(name)
        {
            "null" ->
            {
                return "-"
            }
            "" ->
            {
                return "-"
            }
        }
        return name
    }

    fun setUnderlineOnName(
        userName: String?,
        tvOwnerName: CustomTextView
    ) {

        /*val content = SpannableString(userName)
        content.setSpan(UnderlineSpan(), 0, content.length, 0)
        tvOwnerName.setText(content)*/
        tvOwnerName.setText(Html.fromHtml("<u>"+userName+"</u>"))
    }


    fun getVehicleFuel(id: Int): String {
        when(id)
        {
            1->
            {
                return "CNG & Hybrid"
            }
            2->
            {
                return "Petrol"
            }
            3->
            {
                return "Diesel"
            }
            4->
            {
                return "LPG"
            }
            5->
            {
                return "Electric"
            }

        }
        return "-"
    }

    fun getListedByGrainSeed(id: Int) : String{
        when(id)
        {
            1->
            {
                return "Farmer - ખેડુત"
            }
            2->
            {
                return "Dealer - ડીલર"
            }
            3->
            {
                return "Whole Seller - હોલ સેલર"
            }
        }
        return "-"
    }

    fun getTypeofProducts(id: Int): String {
        when(id)
        {
            1->
            {
                return "Organic - ઓર્ગેનિક"
            }
            2->
            {
                return "Inorganic - ઈન ઓર્ગેનિક"
            }
        }
        return "-"
    }

    fun setAboutInfo(aboutInfo: String, tvAboutInfo: CustomTextView) {
        if(aboutInfo != null && aboutInfo.length > 0)
        {
            tvAboutInfo.visibility = View.VISIBLE
            tvAboutInfo.text = aboutInfo
        }
        else if(aboutInfo.equals("null"))
        {
            tvAboutInfo.visibility = View.GONE
        }
        else{
            tvAboutInfo.visibility = View.GONE
        }
    }
}