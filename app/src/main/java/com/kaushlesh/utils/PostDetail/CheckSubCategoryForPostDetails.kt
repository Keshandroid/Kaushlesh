package com.kaushlesh.utils.PostDetail

import android.content.Context
import android.content.Intent
import com.kaushlesh.view.activity.productDetails.Bikes.BicycleDetailActivity
import com.kaushlesh.view.activity.productDetails.Bikes.MotorScooterPostDetailActivity
import com.kaushlesh.view.activity.productDetails.General.GeneralProductDetailActivity
import com.kaushlesh.view.activity.productDetails.GrainSeed.GrainSeedDetailActivity
import com.kaushlesh.view.activity.productDetails.Khedut.AnimalFertilizerDetailActivity
import com.kaushlesh.view.activity.productDetails.Khedut.GrainFruitVegDetailActivity
import com.kaushlesh.view.activity.productDetails.Mobiles.MobileAccessoriesActivity
import com.kaushlesh.view.activity.productDetails.Mobiles.MobileTabsDetailActivity
import com.kaushlesh.view.activity.productDetails.Property.*
import com.kaushlesh.view.activity.productDetails.Vehicles.CarDecorAutoPartsDetailActivity
import com.kaushlesh.view.activity.productDetails.Vehicles.CarForSellDetailActivity
import com.kaushlesh.view.activity.productDetails.Vehicles.CommercialVehicleForSellDetailActivity

object CheckSubCategoryForPostDetails {
    fun openDetailProperties(context: Context, categoryId: Int, subCategoryId: Int, postId: Int) {
        when(subCategoryId)
        {
            1->
            {
                val intent = Intent(context, ApVillaFarmHouseDetailActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }
            2->
            {
                val intent = Intent(context, ApVillaFarmHouseDetailActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }
            3->
            {
                val intent = Intent(context, OfficeandShopSellDetailsActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }
            4->
            {
                val intent = Intent(context, OfficeandShopSellDetailsActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }
            5->
            {
                val intent = Intent(context, PlotAndLandDetailsActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }
            6->
            {
                val intent = Intent(context, PartyPlotDetailsActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }
            7->
            {
                val intent = Intent(context, HotelResortDetailsActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }
            8->
            {
                val intent = Intent(context, WareHouseDetailsActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }
            9->
            {
                val intent = Intent(context, PgGuestHouseDetailActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }

        }
    }

    private fun openscreen(
        context: Context,
        intent: Intent,
        postId: Int,
        subCategoryId: Int
    ) {
        intent.putExtra("postid", postId.toString())
        intent.putExtra("subCatId", subCategoryId.toString())
        context.startActivity(intent)
    }

    fun openDetailMobiles(context: Context, categoryId: Int, subCategoryId: Int, postId: Int) {
        when(subCategoryId)
        {
            11->
            {
                val intent = Intent(context, MobileTabsDetailActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }

            12->
            {
                val intent = Intent(context, MobileTabsDetailActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }

            13->
            {
                val intent = Intent(context, MobileAccessoriesActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }
        }
    }

    fun openDetailShopsInCity(context: Context, categoryId: Int, subCategoryId: Int, postId: Int) {
        val intent = Intent(context, GeneralProductDetailActivity::class.java)
        openscreen(context,intent,postId,subCategoryId)
    }

    fun openDetailBikes(context: Context, categoryId: Int, subCategoryId: Int, postId: Int) {
        when(subCategoryId)
        {
            14->
            {
                val intent = Intent(context, MotorScooterPostDetailActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }

            15->
            {
                val intent = Intent(context, MotorScooterPostDetailActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }

            16->
            {
                val intent = Intent(context, GeneralProductDetailActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }
            17->
            {
                val intent = Intent(context, BicycleDetailActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }
        }
    }

    fun openDetailVehicles(context: Context, categoryId: Int, subCategoryId: Int, postId: Int) {
        when(subCategoryId)
        {
            18->
            {
                val intent = Intent(context, CarForSellDetailActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }

            19->
            {
                val intent = Intent(context, CommercialVehicleForSellDetailActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }

            20->
            {
                val intent = Intent(context, CarDecorAutoPartsDetailActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }
        }
    }

    fun openDetailGrainSeed(context: Context, categoryId: Int, subCategoryId: Int, postId: Int) {
        val intent = Intent(context, GrainSeedDetailActivity::class.java)
        openscreen(context,intent,postId,subCategoryId)
    }

    fun openDetailKhedut(context: Context, categoryId: Int, subCategoryId: Int, postId: Int) {
        when(subCategoryId)
        {
            110->
            {
                val intent = Intent(context, GrainFruitVegDetailActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }

            111->
            {
                val intent = Intent(context, GrainFruitVegDetailActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }

            112->
            {
                val intent = Intent(context, GrainFruitVegDetailActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }
            113->
            {
                val intent = Intent(context, AnimalFertilizerDetailActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }
            114->
            {
                val intent = Intent(context, GeneralProductDetailActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }
            115->
            {
                val intent = Intent(context, AnimalFertilizerDetailActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }
            116->
            {
                val intent = Intent(context, GeneralProductDetailActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }
            117->
            {
                val intent = Intent(context, GeneralProductDetailActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }
            118->
            {
                val intent = Intent(context, GeneralProductDetailActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }
            119->
            {
                val intent = Intent(context, GeneralProductDetailActivity::class.java)
                openscreen(context,intent,postId,subCategoryId)
            }
        }
    }
}