package com.kaushlesh.utils.PostDetail

import android.content.Context

object CheckCategoryForPostDetails {
    fun OpenScreen(context: Context, categoryId: Int, subCategoryId: Int, postId: Int) {

        when(categoryId)
        {
            2->
            {
                CheckSubCategoryForPostDetails.openDetailProperties(context,categoryId,subCategoryId,postId)
            }

            3->
            {
                CheckSubCategoryForPostDetails.openDetailMobiles(context,categoryId,subCategoryId,postId)
            }

            7->
            {
                CheckSubCategoryForPostDetails.openDetailShopsInCity(context,categoryId,subCategoryId,postId)
            }

            4->
            {
                CheckSubCategoryForPostDetails.openDetailBikes(context,categoryId,subCategoryId,postId)
            }

            1->
            {
                CheckSubCategoryForPostDetails.openDetailVehicles(context,categoryId,subCategoryId,postId)
            }

            5->
            {
                CheckSubCategoryForPostDetails.openDetailShopsInCity(context,categoryId,subCategoryId,postId)
            }

            6->
            {
                CheckSubCategoryForPostDetails.openDetailShopsInCity(context,categoryId,subCategoryId,postId)
            }
            8->
            {
                CheckSubCategoryForPostDetails.openDetailShopsInCity(context,categoryId,subCategoryId,postId)
            }
            9->
            {
                CheckSubCategoryForPostDetails.openDetailShopsInCity(context,categoryId,subCategoryId,postId)
            }
            10->
            {
                CheckSubCategoryForPostDetails.openDetailShopsInCity(context,categoryId,subCategoryId,postId)
            }
            11->
            {
                CheckSubCategoryForPostDetails.openDetailShopsInCity(context,categoryId,subCategoryId,postId)
            }

            12->
            {
                //CheckSubCategoryForPostDetails.openDetailGrainSeed(context,categoryId,subCategoryId,postId)
                CheckSubCategoryForPostDetails.openDetailKhedut(context,categoryId,subCategoryId,postId)
            }
        }
    }
}