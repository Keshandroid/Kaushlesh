package com.kaushlesh.utils.PostDetail

import android.app.Activity
import android.content.Context
import android.os.Bundle
import com.kaushlesh.api.API
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.view.activity.MainActivity
import com.kaushlesh.view.fragment.MyAccount.MyAds.MyPostActivity
import com.kaushlesh.view.fragment.ProductListFragment

object CheckSubCategoryForPostDetailRedirection {
    fun openForProperties(
        context: Context,
        categoryname: String,
        categoryId: Int,
        subcategoryName: String,
        subcategoryId: Int
    ) {
        var url = ""
        when(subcategoryId)
        {
            1->
            {
                url=API.GET_HOUSE_FOR_SELL
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            2->
            {
                url=API.GET_HOUSE_FOR_RENT
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            3->
            {
                url=API.GET_OFFICE_FOR_SELL
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)

            }
            4->
            {
                url=API.GET_OFFICE_FOR_RENT
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)

            }
            5->
            {
                url=API.GET_PLOTES_AND_LAND
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)

            }
            6->
            {
                url=API.GET_PARTY_PLOATS
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)

            }
            7->
            {
                url=API.GET_HOUSE_AND_RESORT
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)

            }
            8->
            {
                url=API.GET_WARE_HOUSE
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)

            }
            9->
            {
                url=API.GET_GUEST_HOUSE
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            1002->
            {
                url=API.GET_VIEW_ALL_PROPERTY
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
        }

    }

    private fun openProductList(
        context: Context,
        categoryname: String,
        categoryId: String,
        subcategoryName: String,
        subcategoryId: String,
        url: String
    ) {

        val bundle = Bundle()
        bundle.putString("name", subcategoryName)
        bundle.putString("catName",categoryname)
        bundle.putString("cateid", categoryId)
        bundle.putString("subcateid",subcategoryId)
        bundle.putString("url", url)

            (context as MainActivity).changeFragment(
                ProductListFragment.newInstance(),
                ProductListFragment.TAG,
                bundle,
                true
            )

    }

    fun openForMobiles(
        context: Context,
        categoryname: String,
        categoryId: Int,
        subcategoryName: String,
        subcategoryId: Int
    ) {
        var url = ""
        when(subcategoryId)
        {
            11 ->
            {
                url=API.GET_MOBILES
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)

            }

            12 ->
            {
                url=API.GET_TABLETS
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)

            }

            13 ->
            {
                url=API.GET_ACCESSORIES
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)

            }
            1003->
            {
                url=API.GET_VIEW_ALL_MOBILE
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
        }
    }

    fun openForShopsInCity(
        context: Context,
        categoryname: String,
        categoryId: Int,
        subcategoryName: String,
        subcategoryId: Int
    ) {
        var url =""
        when(subcategoryId)
        {
            21->
            {
                url = API.GET_GROCERY_SUPER_MARKET
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            22 ->
            {
                url = API.GET_CROCARY_PLASTIC
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            23->
            {
                url = API.GET_SUNGLASS_WATCHSHOP
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            24->
            {
                url = API.GET_PHARMACY_SHOP
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            25->
            {
                url = API.GET_JWELLERY_SHOP
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            26->
            {
                url = API.GET_HARDWARE_SANITARY
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            27->
            {
                url = API.GET_RESTORA_FASTFOOD
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            28->
            {
                url = API.GET_BAKERY_SWEETSHOP
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            29->
            {
                url = API.GET_GIFTS_CARD_SHOP
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            30->
            {
                url = API.GET_ELECTRICAL_SHOP
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            31->
            {
                url = API.GET_STATIONARY_SHOP
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            32->
            {
                url = API.GET_OTHER_SHOPS
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            1007->
            {
                url=API.GET_VIEW_ALL_SHOPS_CITY
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
        }
    }

    fun openForBikes(
        context : Context,
        categoryname: String,
        categoryId: Int,
        subcategoryName: String,
        subcategoryId: Int
    ) {
        var url = ""
        when(subcategoryId)
        {
            14 ->
            {
                url=API.GET_MOTORCYCLE
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)

            }

            15 ->
            {
                url=API.GET_SCOOTER
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)

            }

            16 ->
            {
                url=API.GET_SPARE_PARTS
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)

            }

            17 ->
            {
                url=API.GET_BICYCLE
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)

            }
            1004 ->
            {
                url=API.GET_VIEW_ALL_BIKES
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)

            }
        }
    }

    fun openForVehicles(
        context: Context,
        categoryname: String,
        categoryId: Int,
        subcategoryName: String,
        subcategoryId: Int
    ) {
        var url = ""
        when(subcategoryId)
        {
            18 ->
            {
                url=API.GET_CAR_FOR_SELL
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)

            }

            19 ->
            {
                url=API.GET_COMMERCIAL_VEHICLE_FOR_SELL
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)

            }

            20 ->
            {
                url=API.GET_CAR_DECOR_AUTO_PARTSP
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }

            1001 ->
            {
                url=API.GET_VIEW_ALL_VEHICLE
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
        }
    }

    fun openForElectronics(
        context: Context,
        categoryname: String,
        categoryId: Int,
        subcategoryName: String,
        subcategoryId: Int
    ) {
        var url =""
        when(subcategoryId)
        {
            33->
            {
                url = API.GET_TVS
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            34 ->
            {
                url = API.GET_VIDEO_AUDIO
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            35->
            {
                url = API.GET_COMPUTER_LAPTOP
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            36->
            {
                url = API.GET_GAMES_ENTERN
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            37->
            {
                url = API.GET_FRIDGE
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            38->
            {
                url = API.GET_WASHING_MACHINE
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            39->
            {
                url = API.GET_ACS
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            40->
            {
                url = API.GET_CAMERA_LANCES
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            41->
            {
                url = API.GET_HARDDISK_PRINTER
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            42->
            {
                url = API.GET_KITCHEN
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            1005 ->
            {
                url=API.GET_VIEW_ALL_ELECTRONICS
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
        }
    }

    fun openForFurniture(
        context: Context,
        categoryname: String,
        categoryId: Int,
        subcategoryName: String,
        subcategoryId: Int
    ) {
        var url =""
        when(subcategoryId)
        {
            43->
            {
                url = API.GET_SOFA_DINING
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            44 ->
            {
                url = API.GET_BED_WARDROOMS
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            45->
            {
                url = API.GET_KIDS_FURNITURE
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            46->
            {
                url = API.GET_OTHER_ITEMS
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            1006 ->
            {
                url=API.GET_VIEW_ALL_FURNITURE
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
        }
    }

    fun openForFashionBeauty(
        context: Context,
        categoryname: String,
        categoryId: Int,
        subcategoryName: String,
        subcategoryId: Int
    ) {

        var url =""
        when(subcategoryId)
        {
            47->
            {
                url = API.GET_MEN
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            48 ->
            {
                url = API.GET_WOMEN
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            49->
            {
                url = API.GET_KIDS
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            1008 ->
            {
                url=API.GET_VIEW_ALL_FASHION_BEAUTY
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
        }
    }

    fun openForArtLifeStyle(
        context: Context,
        categoryname: String,
        categoryId: Int,
        subcategoryName: String,
        subcategoryId: Int
    ) {

        var url =""
        when(subcategoryId)
        {
            50->
            {
                url = API.GET_PAINTING
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            51 ->
            {
                url = API.GET_DESIGNER_CLOCK
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            52->
            {
                url = API.GET_POTS_VASES
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            53->
            {
                url = API.GET_DESIGNER_MIRROR
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            54->
            {
                url = API.GET_GARDEN_DECOR
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            55->
            {
                url = API.GET_DECORATIVE_LIGHTINGS
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            56->
            {
                url = API.GET_OTHER_DECORATIVE_PRODUCTS
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            1009 ->
            {
                url=API.GET_VIEW_ALL_ART_LIFESTYLE
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
        }
    }

    fun openForServices(
        context: Context,
        categoryname: String,
        categoryId: Int,
        subcategoryName: String,
        subcategoryId: Int
    ) {

        var url =""
        when(subcategoryId)
        {
            57->
            {
                url = API.GET_ELECTRONICS_HOME_APPL
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            58 ->
            {
                url = API.GET_RENT_CAR_SERVICE
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            59->
            {
                url = API.GET_RENT_COMMERCIAL_VEHICLE
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            60->
            {
                url = API.GET_HEALTH_BEAUTY
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            61->
            {
                url = API.GET_MOVERS_PACKERS
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            62->
            {
                url = API.GET_TRAVEL_AGENCY
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            63->
            {
                url = API.GET_OTHER_HOMEDELIVERY_PRODUCTS
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            64->
            {
                url = API.GET_EVENT_MANAGEMENT
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            65 ->
            {
                url = API.GET_CAR_POOL_SERVICE
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            66->
            {
                url = API.GET_FINANCE_CA
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            67->
            {
                url = API.GET_LEGAL_SERVICE
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            68->
            {
                url = API.GET_INTERIOR_CIVIL
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            69->
            {
                url = API.GET_FOOD_COOKING
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            70->
            {
                url = API.GET_TUTIONS_CLASSES
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            71->
            {
                url = API.GET_OTHER_SERVICES
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            1010 ->
            {
                url=API.GET_VIEW_ALL_SERVICES
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
        }
    }

    fun openForJobs(
        context: Context,
        categoryname: String,
        categoryId: Int,
        subcategoryName: String,
        subcategoryId: Int
    ) {

        var url =""
        when(subcategoryId)
        {
            72->
            {
                url = API.GET_DATA_ENTRY
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            73 ->
            {
                url = API.GET_SALES_MARKETING
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            74->
            {
                url = API.GET_RENT_OIL_GAS
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            75->
            {
                url = API.GET_OPERATOR
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            76->
            {
                url = API.GET_TECHNICIAN
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            77->
            {
                url = API.GET_TEACHER
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            78->
            {
                url = API.GET_CA
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            79->
            {
                url = API.GET_ENGINEER
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            80 ->
            {
                url = API.GET_INTERIOR_DESIGNOR
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            81->
            {
                url = API.GET_RECEPTIONIST
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            82->
            {
                url = API.GET_HOTEL_MANAGEMENT
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            83->
            {
                url = API.GET_COURIER_SERVICE
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            84->
            {
                url = API.GET_DELIVERY_COLLECTION
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            85->
            {
                url = API.GET_OFFICE_BOY
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            86->
            {
                url = API.GET_DRIVER
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            87->
            {
                url = API.GET_COOK
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            88->
            {
                url = API.GET_DOCTOR_NURSE
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            89->
            {
                url = API.GET_OTHER_JOBS
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            1011 ->
            {
                url=API.GET_VIEW_ALL_JOBS
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
        }
    }

    fun openForGrainSeeds(
        context: Context,
        categoryname: String,
        categoryId: Int,
        subcategoryName: String,
        subcategoryId: Int
    ) {
        var url =""
        when(subcategoryId)
        {
            90->
            {
                url = API.GET_SORGHUM
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            91 ->
            {
                url = API.GET_MILLET
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            92->
            {
                url = API.GET_RICE
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            93->
            {
                url = API.GET_GROUNDNUT
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            94->
            {
                url = API.GET_CORN
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            95->
            {
                url = API.GET_COTTON
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            96->
            {
                url = API.GET_CASTOR
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            97->
            {
                url = API.GET_CUMINSEED
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            98 ->
            {
                url = API.GET_TILL
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            99->
            {
                url = API.GET_RAI
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            100->
            {
                url = API.GET_AHI_SEED
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            101->
            {
                url = API.GET_POTATO
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            102->
            {
                url = API.GET_ONION
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            103->
            {
                url = API.GET_OTHERS
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
        }
    }

    fun openForKhedut(context: Context,categoryname: String, categoryId: Int, subcategoryName: String, subcategoryId: Int) {
        var url =""
        when(subcategoryId)
        {
            110->
            {
                url = API.GET_GRAIN_FOR_SELL
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            111 ->
            {
                url = API.GET_FRUIT_FOR_SELL
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            112->
            {
                url = API.GET_VEGETABLE_FOR_SELL
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            113->
            {
                url = API.GET_ANIMAL_FOR_SELL
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            114->
            {
                url = API.GET_NURSUERY_PLANT_FOR_SELL
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            115->
            {
                url = API.GET_FERTILIZER_SEEDS
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            116->
            {
                url = API.GET_FARMING_MACHINE
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            117->
            {
                url = API.GET_TRACTOR_TROLLEY
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            118 ->
            {
                url = API.GET_TRANSPORT_FOR_KHEDUT
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
            119->
            {
                url = API.GET_FARM_FOR_RENT
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }

            1012 ->
            {
                url=API.GET_VIEW_ALL_KHEDUT
                openProductList(context,categoryname.toString(),categoryId.toString(),subcategoryName.toString(),subcategoryId.toString(),url)
            }
        }
    }
}