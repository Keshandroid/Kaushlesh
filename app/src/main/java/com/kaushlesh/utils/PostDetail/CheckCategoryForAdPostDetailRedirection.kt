package com.kaushlesh.utils.PostDetail

import android.app.Activity
import android.content.Context
import com.kaushlesh.view.activity.MainActivity
import com.kaushlesh.view.fragment.MyAccount.MyAds.MyPostActivity

object CheckCategoryForAdPostDetailRedirection {

    fun checkCategory(
        mainActivity: Activity,
        category: String,
        categoryId: Int,
        subcategoryName: String,
        subcategoryId: Int
    ) {
        when(categoryId)
        {
            2->
            {
                CheckSubCategoryForPostDetailRedirection.openForProperties(mainActivity,category,categoryId,subcategoryName,subcategoryId)
            }

            3->
            {
                CheckSubCategoryForPostDetailRedirection.openForMobiles(mainActivity,category,categoryId,subcategoryName,subcategoryId)
            }

            7->
            {
                CheckSubCategoryForPostDetailRedirection.openForShopsInCity(mainActivity,category,categoryId,subcategoryName,subcategoryId)
            }

            4->
            {
                CheckSubCategoryForPostDetailRedirection.openForBikes(mainActivity,category,categoryId,subcategoryName,subcategoryId)
            }

            1->
            {
                CheckSubCategoryForPostDetailRedirection.openForVehicles(mainActivity,category,categoryId,subcategoryName,subcategoryId)
            }

            5->
            {
                CheckSubCategoryForPostDetailRedirection.openForElectronics(mainActivity,category,categoryId,subcategoryName,subcategoryId)
            }
            6->
            {
                CheckSubCategoryForPostDetailRedirection.openForFurniture(mainActivity,category,categoryId,subcategoryName,subcategoryId)
            }
            8->
            {
                CheckSubCategoryForPostDetailRedirection.openForFashionBeauty(mainActivity,category,categoryId,subcategoryName,subcategoryId)
            }
            9->
            {
                CheckSubCategoryForPostDetailRedirection.openForArtLifeStyle(mainActivity,category,categoryId,subcategoryName,subcategoryId)

            }
            10->
            {
                CheckSubCategoryForPostDetailRedirection.openForServices(mainActivity,category,categoryId,subcategoryName,subcategoryId)

            }
            11->
            {
                CheckSubCategoryForPostDetailRedirection.openForJobs(mainActivity,category,categoryId,subcategoryName,subcategoryId)
            }
            12->
            {
               // CheckSubCategoryForPostDetailRedirection.openForGrainSeeds(mainActivity,category,categoryId,subcategoryName,subcategoryId)
                CheckSubCategoryForPostDetailRedirection.openForKhedut(mainActivity,category,categoryId,subcategoryName,subcategoryId)
            }
        }
    }

}