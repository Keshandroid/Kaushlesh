package com.kaushlesh.utils.PostDetail

import android.content.Context
import android.content.pm.PackageManager
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.kaushlesh.R
import com.kaushlesh.adapter.RelaventPostAdapter
import com.kaushlesh.adapter.SliderAdapterExample
import com.kaushlesh.bean.PostDetails.postDetailsBean
import com.kaushlesh.utils.Constants
import com.kaushlesh.widgets.CustomTextView
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView

object SetPostDetailData : RelaventPostAdapter.ItemClickListener {

    fun setSlider(
        context: Context,
        postImages: ArrayList<postDetailsBean.PostImageX>,
        adapter: SliderAdapterExample,
        sliderView: SliderView,
        tvCount: CustomTextView
    ) {
        sliderView.setSliderAdapter(adapter);

//        sliderView!!.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH)
//        sliderView!!.setIndicatorSelectedColor(Color.WHITE)
        //  sliderView!!.setIndicatorUnselectedColor(Color.GRAY)
        sliderView.setScrollTimeInSec(3);
        sliderView.setAutoCycle(true)
        sliderView.startAutoCycle()
        adapter.notifyDataSetChanged()


        val currentposion=sliderView.currentPagePosition.toInt()+1
        tvCount.text = (currentposion).toString() +"/"+postImages.size.toString()

        sliderView.setCurrentPageListener {
            val currentposion=sliderView.currentPagePosition.toInt()+1

            tvCount.text = (currentposion).toString() +"/"+postImages.size.toString()
        }
    }

    fun setRetaedArray(
        context: Context,
        rvRecom: RecyclerView,
        relatedArray: ArrayList<postDetailsBean.RelatedArray>,
        itemClick: RelaventPostAdapter.ItemClickListener
    ) {

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rvRecom.layoutManager = layoutManager
        val adapter = RelaventPostAdapter(relatedArray,context)
        adapter.setClicklistner(itemClick)
        rvRecom.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    fun setmapdata(
        supportFragmentManager: FragmentManager,
        mapView: Fragment,
        latitude: String,
        longitude: String
    ) {
        try {

            val mapFragment = supportFragmentManager.findFragmentById(R.id.mapView) as SupportMapFragment

            val finalA = latitude.toDouble()
            val finalB = longitude.toDouble()
            mapFragment.getMapAsync(OnMapReadyCallback { googleMap ->
                val marker = MarkerOptions().position(LatLng(finalA, finalB)).title("")

                // adding marker
                googleMap.addMarker(marker)

                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(finalA, finalB), 15f))

                val cameraPosition = CameraPosition.Builder()
                        .target(LatLng(finalA, finalB))      // Sets the center of the map to location user
                        .zoom(15f)                   // Sets the zoom
                        //.bearing(90)                // Sets the orientation of the camera to east
                        //.tilt(40)                   // Sets the tilt of the camera to 30 degrees
                        .build()                   // Creates a CameraPosition from the builder
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
            })
        }
        catch (e : NullPointerException)
        {
            e.printStackTrace()
        }
        catch (e : NumberFormatException)
        {
            e.printStackTrace()
        }
    }

    fun setFav(ivFav: ImageView, favourite: Int) {
        if(favourite == 1)
        {
            ivFav.setBackgroundResource(R.drawable.ic_favorite_filled_black)
        }
        else{
            ivFav.setBackgroundResource(R.drawable.ic_fav_black)

        }
    }

    fun SetchatButton(loginid: Int, userId: Int, btnChat: CustomTextView) {

        if(loginid == userId)
        {
            btnChat.visibility = View.GONE
        }
        else{
            btnChat.visibility = View.VISIBLE
        }
    }

    fun setcallButton(loginid: Int, userId: Int,btnCall: CustomTextView, value: Int) {
        if(loginid == userId)
        {
            btnCall.visibility = View.GONE
        }
        else{
            if(value == 1)
            {
                btnCall.visibility = View.VISIBLE
            }
            else{
                btnCall.visibility = View.GONE
            }
        }
    }

    //set whatsapp button
    fun setWhatsappButton(loginid: Int, userId: Int,whatsappLayout: RelativeLayout, value: Int) {
        if(loginid == userId) {
            whatsappLayout.visibility = View.GONE
        } else{
            if(value == 1) {
                whatsappLayout.visibility = View.VISIBLE
            } else{
                whatsappLayout.visibility = View.GONE
            }
        }

    }

    fun isAppInstalled(ctx: Context, packageName: String): Boolean {
        val pm: PackageManager = ctx.getPackageManager()
        val app_installed: Boolean
        app_installed = try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES)
            true
        } catch (e: PackageManager.NameNotFoundException) {
            false
        }
        return app_installed
    }


    fun setToolBar(
        loginid: Int,
        userId: Int,
        btnChat: CustomTextView,
        previewType: String,
        ivDelete: ImageView
    ) {
        if(previewType.equals("my_post_ad"))
        {
            btnChat.visibility = View.GONE
            //ivDelete.visibility = View.VISIBLE
            ivDelete.visibility = View.GONE
        }

        if(previewType.equals("my_post_fav"))
        {
            btnChat.visibility = View.GONE
            ivDelete.visibility = View.GONE
        }

        if(previewType.equals("product_list"))
        {
            btnChat.visibility = View.VISIBLE
            ivDelete.visibility = View.GONE
        }
    }



    override fun itemclickRecommendation(bean: postDetailsBean.RelatedArray) {

    }
}