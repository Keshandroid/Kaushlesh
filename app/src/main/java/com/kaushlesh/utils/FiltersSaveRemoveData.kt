package com.kaushlesh.utils

import android.content.Context
import android.util.Log
import com.facebook.FacebookSdk
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import com.kaushlesh.bean.FilterBean
import com.kaushlesh.bean.MobileBrandBeans
import java.lang.reflect.Type

object FiltersSaveRemoveData {
    val TAG = "FILTER"

    fun savePriceFilter(Price:String, cate: String, subcate: String, cname: String) {
        val gson = Gson()
        val sharedPref = FacebookSdk.getApplicationContext().getSharedPreferences(cate+"_"+subcate, Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        Log.e(TAG, "setdata")
        editor.putString(subcate+"_"+cname, Price)
        editor.apply()
    }

    fun getPriceData(cate: String, subcate: String, cname: String): String? {
        val sharedPref = FacebookSdk.getApplicationContext().getSharedPreferences(cate+"_"+subcate, Context.MODE_PRIVATE)
        val jsonPreferences = sharedPref.getString(subcate+"_"+cname, "")
        if ( jsonPreferences == null) {
            return null
        }
        return jsonPreferences
    }

    fun saveFilterDataList(list: ArrayList<FilterBean>, cate: String?, subcate: String?, cname: String?) {
        val gson = Gson()
        val jsonCurProduct = gson.toJson(list)
        val sharedPref = FacebookSdk.getApplicationContext().getSharedPreferences(cate+"_"+subcate, Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        Log.e(TAG, "setdata")
        editor.putString(subcate+"_"+cname, jsonCurProduct)
        editor.apply()
    }

    fun getFilterData(cate: String?, subcate: String?, cname: String?): ArrayList<FilterBean?>? {
        val gson = Gson()
        var productFromShared: ArrayList<FilterBean?>? = ArrayList()
        val sharedPref = FacebookSdk.getApplicationContext().getSharedPreferences(cate+"_"+subcate, Context.MODE_PRIVATE)
        val jsonPreferences = sharedPref.getString(subcate+"_"+cname, "")
        val type: Type = object : TypeToken<ArrayList<FilterBean?>?>() {}.type
        productFromShared = gson.fromJson<ArrayList<FilterBean?>>(jsonPreferences, type)
        if ( productFromShared == null) {
            return null
        }
        return productFromShared
    }

    fun saveFilterDataListBrand(list: ArrayList<MobileBrandBeans.MobileBrandBeansList>, cate: String?, subcate: String?, cname: String?) {
        val gson = Gson()
        val jsonCurProduct = gson.toJson(list)
        val sharedPref = FacebookSdk.getApplicationContext().getSharedPreferences(cate+"_"+subcate, Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        Log.e(TAG, "setdata")
        editor.putString(subcate+"_"+cname, jsonCurProduct)
        editor.apply()
    }

    fun getFilterDataBrand(cate: String?, subcate: String?, cname: String?): ArrayList<MobileBrandBeans.MobileBrandBeansList?>? {
        val gson = Gson()
        var productFromShared: ArrayList<MobileBrandBeans.MobileBrandBeansList?>? = ArrayList()
        val sharedPref = FacebookSdk.getApplicationContext().getSharedPreferences(cate+"_"+subcate, Context.MODE_PRIVATE)
        val jsonPreferences = sharedPref.getString(subcate+"_"+cname, "")
        val type: Type = object : TypeToken<ArrayList<MobileBrandBeans.MobileBrandBeansList?>?>() {}.type
        productFromShared = gson.fromJson<ArrayList<MobileBrandBeans.MobileBrandBeansList?>>(jsonPreferences, type)
        if ( productFromShared == null) {
            return null
        }
        return productFromShared
    }

    fun cleardata(cate: String?, subcate: String?) {
        val sharedPref = FacebookSdk.getApplicationContext().getSharedPreferences(cate+"_"+subcate, Context.MODE_PRIVATE)
        sharedPref.edit().clear().apply()

    }
}