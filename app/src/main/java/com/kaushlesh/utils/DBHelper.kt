package com.kaushlesh.utils

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.kaushlesh.bean.RegisterPhoneBean


class DBHelper(
    context: Context?,
    name: String?,
    factory: SQLiteDatabase.CursorFactory?,
    version: Int
) : SQLiteOpenHelper(context, name, factory, version) {

    var dbHelper: DBHelper? = null
    var database: SQLiteDatabase? = null

    val DB_NAME = "kaushlesh.db"
    val DB_VERSION = 1

    private val REGISTER_TABLE = "register_table"
    private val ID = "id"
    private val USER_ID = "user_id"
    private val USER_NAME = "user_name"
    private val EMAIL = "email"
    private val MOBILE_NUMBER = "mobile_number"
    private val TOKEN = "token"
    //private val LANGUAGE = "language"
    private val IS_LOGGED_IN = "is_logged_in"
    private val REGISTER_TYPE = "registration_type"


    internal var CREATE_REGISTER_TABLE =
        ("CREATE TABLE " + REGISTER_TABLE + "(" + USER_ID + " TEXT,"
                + ID + " TEXT,"
                + USER_NAME + " TEXT,"
                // + EMAIL + " TEXT,"
                + MOBILE_NUMBER + " TEXT,"
                + TOKEN + " TEXT,"
                + REGISTER_TYPE + " TEXT,"
                + IS_LOGGED_IN + " TEXT)")


    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(CREATE_REGISTER_TABLE)

        Log.e("create login table", CREATE_REGISTER_TABLE)

    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS $REGISTER_TABLE")

        onCreate(db)
    }


    //add register data
    fun addRegisterData(d: RegisterPhoneBean) {

        opendb()
        val values = ContentValues()
        values.put(ID, d.userid)
        values.put(USER_ID, d.userid)
        values.put(USER_NAME, d.name)
        //values.put(EMAIL, d.)
        values.put(MOBILE_NUMBER, d.ConatctNo)
        values.put(TOKEN, d.userToken)
        values.put(REGISTER_TYPE, d.registrationType)
        values.put(IS_LOGGED_IN, true)
        Log.e("add data", "inserted$values")
        database!!.insert(REGISTER_TABLE, null, values)

        closedb()
    }


    // Get user register  data
    fun getRegisterData(): RegisterPhoneBean {

        opendb()
        val d = RegisterPhoneBean()

        val cursor = database!!.query(REGISTER_TABLE, null, null, null, null, null, null)
        cursor!!.moveToFirst()

        while (!cursor.isAfterLast) {
            d.userid = cursor.getString(cursor.getColumnIndex(USER_ID))
            d.userid = cursor.getString(cursor.getColumnIndex(ID))
            d.name = cursor.getString(cursor.getColumnIndex(USER_NAME))
            //d.email = cursor.getString(cursor.getColumnIndex(EMAIL))
            d.ConatctNo = cursor.getString(cursor.getColumnIndex(MOBILE_NUMBER))
            d.userToken = cursor.getString(cursor.getColumnIndex(TOKEN))
            d.registrationType = cursor.getString(cursor.getColumnIndex(REGISTER_TYPE))
            //d.isloggedin = cursor.getString(cursor.getColumnIndex(IS_LOGGED_IN))
            return d
        }

        //closedb()
        Log.e("register id", d.userid + "")

        cursor.close()
        return d
    }


    fun updateRegisterData(d: RegisterPhoneBean) {

        //opendb()
        val values = ContentValues()
        if (d.userid != null && d.userid!!.length > 0) {
            values.put(USER_ID, d.userid)
        }

        if (d.userid != null && d.userid!!.length > 0) {
            values.put(ID, d.userid)
        }
        if (d.name != null && d.name!!.length > 0) {
            values.put(USER_NAME, d.name)
        }

        /*    if (d.email != null && d.email!!.length > 0) {
                values.put(EMAIL, d.email)
            }*/
        if (d.ConatctNo != null && d.ConatctNo!!.length > 0) {
            values.put(MOBILE_NUMBER, d.ConatctNo)
        }

        if (d.userToken != null && d.userToken!!.length > 0) {
            values.put(TOKEN, d.userToken)
        }
        if (d.registrationType != null && d.registrationType!!.length > 0) {
            values.put(REGISTER_TYPE, d.registrationType)
        }
        /* if (d.isloggedin != null && d.isloggedin!!.length > 0) {
             values.put(IS_LOGGED_IN, d.isloggedin)
         }*/
        database!!.update(REGISTER_TABLE, values, USER_ID + "=" + d.userid, null)

        Log.e("Update data", "updated$values")

        // closedb()
    }

    fun deleteUser() {
        //opendb()
        database!!.delete(REGISTER_TABLE, null, null)
        //closedb()
    }

    companion object
    {
        var dbHelper: DBHelper? = null
        var database: SQLiteDatabase? = null

        val DB_NAME = "kaushlesh.db"
        val DB_VERSION = 1

        fun getInstance(context: Context): DBHelper {

            if (dbHelper == null) {
                dbHelper = DBHelper(context, DB_NAME, null, DB_VERSION)
            }
            return dbHelper as DBHelper
        }
    }



    fun opendb() {

        database = dbHelper!!.writableDatabase
    }

    fun closedb() {

        if (database != null) {
            database!!.close()
        }
    }
}
