package com.kaushlesh.utils

import androidx.fragment.app.FragmentManager
import com.kaushlesh.bean.FilterBean
import com.kaushlesh.view.filters.*
import com.kaushlesh.view.fragment.ProductListFragment


class  CommanPropertyTypeListFilter {

    fun typeList(list: ArrayList<FilterBean>, cname: String?, subcategoryid: Int): List<FilterBean> {

        val list = ArrayList<FilterBean>()
        if (subcategoryid == 1) {
            list.add(FilterBean("Type", 9))
            list.add(FilterBean("By Price", 100))
            list.add(FilterBean("By Bedroom", 11))
            list.add(FilterBean("By Bathroom", 12))
            list.add(FilterBean("Listed By", 13))
            list.add(FilterBean("Construction Status", 14))
            list.add(FilterBean("By Furnishing", 15))
            list.add(FilterBean("Super Builtup Area", 16))
            list.add(FilterBean("Carpet Area", 19))
            list.add(FilterBean("Premium Ad", 17))
        }
        if (subcategoryid == 2) {
            list.add(FilterBean("Type", 9))
            list.add(FilterBean("By Price", 10))
            list.add(FilterBean("By Bedroom", 11))
            list.add(FilterBean("By Bathroom", 12))
            list.add(FilterBean("Listed By", 13))
            list.add(FilterBean("Construction Statu s", 14))
            list.add(FilterBean("By Furnishing", 15))
            list.add(FilterBean("Bachelor Allowed", 18))
            list.add(FilterBean("Super Builtup Area", 16))
            list.add(FilterBean("Carpet Area", 19))
            list.add(FilterBean("Premium Ad", 17))
        }
        if (subcategoryid == 3) {

            list.add(FilterBean("By Price", 10))
            list.add(FilterBean("Listed By", 13))
            list.add(FilterBean("Construction Status", 14))
            list.add(FilterBean("By Furnishing", 15))
            list.add(FilterBean("Carpet Area", 19))
            list.add(FilterBean("Premium Ad", 17))
        }
        if (subcategoryid == 4) {
            list.add(FilterBean("By Price", 10))
            list.add(FilterBean("Listed By", 13))
            list.add(FilterBean("By Furnishing", 15))
            list.add(FilterBean("Carpet Area", 19))
            list.add(FilterBean("Premium Ad", 17))
        }
        if (subcategoryid == 5) {
            list.add(FilterBean("Type", 9))
            list.add(FilterBean("Purpose", 20))
            list.add(FilterBean("By Price", 10))
            list.add(FilterBean("Listed By", 13))
            list.add(FilterBean("Land Area", 21))
            list.add(FilterBean("Premium Ad", 17))
        }
        if (subcategoryid == 6) {
            list.add(FilterBean("Type", 9))
            list.add(FilterBean("By Price", 10))
            list.add(FilterBean("Listed By", 13))
            list.add(FilterBean("Guest Capacity", 22))
            list.add(FilterBean("Premium Ad", 17))
        }
        if (subcategoryid == 7) {
            list.add(FilterBean("Type", 9))
            list.add(FilterBean("Purpose", 20))
            list.add(FilterBean("By Price", 10))
            list.add(FilterBean("Listed By", 13))
            list.add(FilterBean("Premium Ad", 17))
        }
        if (subcategoryid == 8) {
            list.add(FilterBean("Type", 9))
            list.add(FilterBean("Purpose", 20))
            list.add(FilterBean("By Price", 10))
            list.add(FilterBean("Listed By", 13))
            list.add(FilterBean("Premium Ad", 17))
        }
        if (subcategoryid == 9) {
            list.add(FilterBean("Type", 9))
            list.add(FilterBean("By Price", 10))
            list.add(FilterBean("Listed By", 13))
            list.add(FilterBean("By Furnishing", 15))
            list.add(FilterBean("Premium Ad", 17))
        }
        if (subcategoryid == 1002) {
            list.add(FilterBean("Premium Ad", 17))
        }
        return list
    }

    fun BikelList(list: ArrayList<FilterBean>, cId: Int?): List<FilterBean> {

        if (cId==14) {
            list.add(FilterBean("By Brand", 1))
            list.add(FilterBean("By Price", 2))
            list.add(FilterBean("By KM Driven", 3))
            list.add(FilterBean("By Fuel", 4))
            list.add(FilterBean("By Year", 5))
            list.add(FilterBean("Premium Ad", 7))
        }
        if (cId==15) {
            list.add(FilterBean("By Brand", 6))
            list.add(FilterBean("By Price", 2))
            list.add(FilterBean("By KM Driven", 3))
            list.add(FilterBean("By Year", 5))
            list.add(FilterBean("By Fuel", 4))
            list.add(FilterBean("Premium Ad", 7))
        }
        if (cId==16) {
            list.add(FilterBean("By Price", 2))
            list.add(FilterBean("Premium Ad", 7))
        }

        if (cId==17) {
            list.add(FilterBean("Brand", 8))
            list.add(FilterBean("By Price", 2))
            list.add(FilterBean("Premium Ad", 7))
        }
        if (cId==1004) {
            list.add(FilterBean("Premium Ad", 7))
        }
        return list
    }

    fun VehicalList(list: ArrayList<FilterBean>, cId: Int?): List<FilterBean> {

        if (cId==18) {
            list.add(FilterBean("By Brand", 1))
            list.add(FilterBean("By Price", 2))
            list.add(FilterBean("By KM Driven", 3))
            list.add(FilterBean("By Owner", 4))
            list.add(FilterBean("By Year", 5))
            list.add(FilterBean("By Fuel", 6))
            list.add(FilterBean("Premium Ad", 7))
        }
        if (cId==19) {
            list.add(FilterBean("By Type", 8))
            list.add(FilterBean("By Price", 2))
            list.add(FilterBean("By KM/Hours Run", 3))
            list.add(FilterBean("By Year", 5))
            list.add(FilterBean("Premium Ad", 7))
        }
        if (cId==20) {
            list.add(FilterBean("By Type", 8))
            list.add(FilterBean("By Price", 2))
            list.add(FilterBean("Premium Ad", 7))
        }
        if (cId==1001) {
            list.add(FilterBean("Premium Ad", 7))
        }

        return list
    }

    fun typeKhedutList(list: java.util.ArrayList<FilterBean>, cname: String?, cId: Int): List<FilterBean> {
        if (cId==110 || cId==111 ||cId==112) {
            list.add(FilterBean("By Product", 3))
            list.add(FilterBean("By Types", 4))
            list.add(FilterBean("By Price", 1))
            list.add(FilterBean("By Stock", 2))
            list.add(FilterBean("Premium Ad", 7))
        }
        if (cId==113) {
            list.add(FilterBean("Animals", 5))
            list.add(FilterBean("By Price", 1))
            list.add(FilterBean("Premium Ad", 7))
        }
        if (cId==115) {
            list.add(FilterBean("By Product", 3))
            list.add(FilterBean("Premium Ad", 7))
        }
        if (cId == 114 || cId == 116 || cId == 117 || cId == 118 ||cId == 119 ||cId==1012) {
            list.add(FilterBean("Premium Ad", 7))
        }

        return list
    }

    fun openBottomSheet(categoryid :Int, subcategoryid :Int, category: String, cname: String, ctx: ProductListFragment, fragmentManager: FragmentManager, url : String) {
        when(categoryid)
        {
            2 ->
            {
                val myBottomSheet = FilterViewPropertyComman.newInstance(ctx, cname,subcategoryid,url,categoryid)
                myBottomSheet.show(fragmentManager, myBottomSheet.getTag())
            }

            3 ->
            {
                val myBottomSheet = FilterViewMobileComman.newInstance(ctx, cname,subcategoryid,url,categoryid)
                myBottomSheet.show(fragmentManager, myBottomSheet.getTag())
            }

            7 ->
            {
                val myBottomSheet = FilterViewComman.newInstance(ctx, cname, subcategoryid, url, categoryid)
                myBottomSheet.show(fragmentManager, myBottomSheet.getTag())
            }
            4->
            {
                val myBottomSheet = FilterViewBikesComman.newInstance(ctx, cname,subcategoryid, url, categoryid)
                myBottomSheet.show(fragmentManager, myBottomSheet.getTag())
            }

            1->
            {
                val myBottomSheet = FilterViewVehicleComman.newInstance(ctx, cname,subcategoryid, url, categoryid)
                myBottomSheet.show(fragmentManager, myBottomSheet.getTag())
            }

            5->
            {
                val myBottomSheet = FilterViewGeneral.newInstance(ctx, cname, subcategoryid, url, categoryid)
                myBottomSheet.show(fragmentManager, myBottomSheet.getTag())
            }

            6->
            {
                val myBottomSheet = FilterViewGeneral.newInstance(ctx, cname, subcategoryid, url, categoryid)
                myBottomSheet.show(fragmentManager, myBottomSheet.getTag())
            }

            8->
            {
                val myBottomSheet = FilterViewGeneral.newInstance(ctx, cname, subcategoryid, url, categoryid)
                myBottomSheet.show(fragmentManager, myBottomSheet.getTag())
            }

            9->
            {
                val myBottomSheet = FilterViewGeneral.newInstance(ctx, cname, subcategoryid, url, categoryid)
                myBottomSheet.show(fragmentManager, myBottomSheet.getTag())
            }

            10->
            {
                val myBottomSheet = FilterViewComman.newInstance(ctx, cname, subcategoryid, url, categoryid)
                myBottomSheet.show(fragmentManager, myBottomSheet.getTag())
            }

            11->
            {
                val myBottomSheet = FilterViewComman.newInstance(ctx, cname, subcategoryid, url, categoryid)
                myBottomSheet.show(fragmentManager, myBottomSheet.getTag())
            }

            12->
            {
                /*val myBottomSheet = FilterViewGrainSeedComman.newInstance(ctx, cname, subcategoryid, url, categoryid)
                myBottomSheet.show(fragmentManager, myBottomSheet.getTag())*/

                val myBottomSheet = FilterViewKhedutComman.newInstance(ctx, cname, subcategoryid, url, categoryid)
                myBottomSheet.show(fragmentManager, myBottomSheet.getTag())
            }
        }
        /*if (category.equals("Vehicles")) {
            val myBottomSheet = FilterViewVehicleComman.newInstance(ctx, cname)
            myBottomSheet.show(fragmentManager!!, myBottomSheet.getTag())
        } else if (categoryid.equals("Properties")) {
            val myBottomSheet = FilterViewPropertyComman.newInstance(ctx, cname,subcategoryid)
            myBottomSheet.show(fragmentManager!!, myBottomSheet.getTag())
        } else if (category.equals("Mobiles")) {
            val myBottomSheet = FilterViewMobileComman.newInstance(ctx, cname)
            myBottomSheet.show(fragmentManager!!, myBottomSheet.getTag())
        } else if (category.equals("Bikes")) {
            val myBottomSheet = FilterViewBikesComman.newInstance(ctx, cname)
            myBottomSheet.show(fragmentManager!!, myBottomSheet.getTag())
        } else if (category.equals("Grains & Seeds")) {
            val myBottomSheet = FilterViewGrainSeedComman.newInstance(ctx, cname)
            myBottomSheet.show(fragmentManager!!, myBottomSheet.getTag())
        } else {
            val myBottomSheet = FilterViewComman.newInstance(ctx, cname,category)
            myBottomSheet.show(fragmentManager!!, myBottomSheet.getTag())
        }*/
    }


}
