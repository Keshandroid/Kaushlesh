package com.kaushlesh.utils.validations

import android.app.Activity
import android.widget.EditText
import com.kaushlesh.R
import com.kaushlesh.bean.AdPost.*
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.AdPost.Property.*
import com.kaushlesh.view.activity.AdPost.seed.AddSeedDetailsActivity
import com.kaushlesh.widgets.CustomEditText

object AdPostPropertyValidation {

    fun checkForApVillaFarm(
        context: Activity,
        adVillsellandRentBeanList: ApVillsellandRentBean,
        etsuperbuiltarea: EditText,
        etcarpetarea: EditText,
        etadtitle: EditText,
        etotherinfo: EditText
    ) : Boolean {
        val carpetrae: Boolean
        val scarpetrae: Boolean
        val title : Boolean
        val info : Boolean
        val type : Boolean
        val bedroom : Boolean
        val bathroom : Boolean
        val furnishing : Boolean
        val construction : Boolean
        val listedby : Boolean

        if(adVillsellandRentBeanList.property_type.toString().length > 0)
        {
            type = true
        }
        else{
            type = false
            Utils.showToast(context, context.getString(R.string.txt_select_type))
        }

        if(adVillsellandRentBeanList.bedrooms.toString().length > 0)
        {
            bedroom = true
        }
        else{
            bedroom = false
            Utils.showToast(context, context.getString(R.string.txt_select_bedroom))
        }

        if(adVillsellandRentBeanList.bathrooms.toString().length > 0)
        {
            bathroom = true
        }
        else{
            bathroom = false
            Utils.showToast(context, context.getString(R.string.txt_select_bathroom))
        }

        if(adVillsellandRentBeanList.furnishing.toString().length > 0)
        {
            furnishing = true
        }
        else{
            furnishing = false
            Utils.showToast(context, context.getString(R.string.txt_select_furnishing))
        }

        if(adVillsellandRentBeanList.construction_status.toString().length > 0)
        {
            construction = true
        }
        else{
            construction = false
            Utils.showToast(context, context.getString(R.string.txt_select_constructions))
        }

        if(adVillsellandRentBeanList.listed_by.toString().length > 0)
        {
            listedby = true
        }
        else{
            listedby = false
            Utils.showToast(context, context.getString(R.string.txt_select_listedby))
        }

        if (Utils.isEmpty(etsuperbuiltarea)) {
            //setError(etsuperbuiltarea)
            etsuperbuiltarea.setError(context.getString(R.string.txt_required_filed))
            scarpetrae = false
        }
        else{
            scarpetrae = true
        }

        if (Utils.isEmpty(etcarpetarea)) {
            etcarpetarea.setError(context.getString(R.string.txt_required_filed))
            carpetrae = false
        }
        else{
            carpetrae = true
        }

        if (Utils.isEmpty(etadtitle)) {
            etadtitle.setError(context.getString(R.string.txt_required_filed))
            title = false
        }
        else{
            title = true
        }

        if (Utils.isEmpty(etotherinfo)) {
            etotherinfo.setError(context.getString(R.string.txt_required_filed))
            info = false
        }
        else{
            info = true
        }

        if(type && bathroom && bedroom && furnishing && construction && listedby && scarpetrae && carpetrae && title && info)
        {
            return true
        }
        Utils.showToast(context, context.getString(R.string.txt_check_all_reuired_fileds))
        return false
    }

    fun checkForOffuce(
        context: OfficeDetailActivity,
        adOfficeForSellRent: AdOfficeForSellRentBean,
        et_floor: CustomEditText,
        etcarpetarea: EditText,
        etadtitle: EditText,
        etotherinfo: EditText
    ): Boolean {

        val furnishing : Boolean
        val construction : Boolean
        val listedby : Boolean
        val floor : Boolean
        val carpetrae: Boolean
        val title : Boolean
        val info : Boolean

        if(adOfficeForSellRent.furnishing.toString().length > 0)
        {
            furnishing = true
        }
        else{
            furnishing = false
            Utils.showToast(context, context.getString(R.string.txt_select_furnishing))
        }

        if(adOfficeForSellRent.construction_status.toString().length > 0)
        {
            construction = true
        }
        else{
            construction = false
            Utils.showToast(context, context.getString(R.string.txt_select_constructions))
        }

        if(adOfficeForSellRent.listed_by.toString().length > 0)
        {
            listedby = true
        }
        else{
            listedby = false
            Utils.showToast(context, context.getString(R.string.txt_select_listedby))
        }

        if (Utils.isEmpty(et_floor)) {
            et_floor.setError(context.getString(R.string.txt_required_filed))
            floor = false

        }
        else{
            floor = true
        }
        if (Utils.isEmpty(etcarpetarea)) {
            etcarpetarea.setError(context.getString(R.string.txt_required_filed))
            carpetrae = false
        }
        else{
            carpetrae = true
        }
        if (Utils.isEmpty(etadtitle)) {
            etadtitle.setError(context.getString(R.string.txt_required_filed))
            title = false
        }
        else{
            title = true
        }

        if (Utils.isEmpty(etotherinfo)) {
            etotherinfo.setError(context.getString(R.string.txt_required_filed))
            info = false
        }
        else{
            info = true
        }

        if(furnishing && construction && listedby && floor && carpetrae && title && info)
        {
            return true
        }
        Utils.showToast(context, context.getString(R.string.txt_check_all_reuired_fileds))
        return false
    }

    fun checkForPlotLand(
        context: PlotLandDetailActivity,
        adPlotAndLandList: AdPlotAndLandBean,
        etplotarea: EditText,
        etadtitle: EditText,
        etotherinfo: EditText
    ): Boolean {
        val type : Boolean
        val purpose : Boolean
        val listedby : Boolean
        val plotarea : Boolean
        val title : Boolean
        val info : Boolean

        if(adPlotAndLandList.property_type.toString().length > 0)
        {
            type = true
        }
        else{
            type = false
            Utils.showToast(context, context.getString(R.string.txt_select_type))
        }

        if(adPlotAndLandList.purpose.toString().length > 0)
        {
            purpose = true
        }
        else{
            purpose = false
            Utils.showToast(context, context.getString(R.string.txt_select_purpose))
        }

        if(adPlotAndLandList.listed_by.toString().length > 0)
        {
            listedby = true
        }
        else{
            listedby = false
            Utils.showToast(context, context.getString(R.string.txt_select_listedby))
        }

        if (Utils.isEmpty(etplotarea)) {
            etplotarea.setError(context.getString(R.string.txt_required_filed))
            plotarea = false
        }
        else{
            plotarea = true
        }
        if (Utils.isEmpty(etadtitle)) {
            etadtitle.setError(context.getString(R.string.txt_required_filed))
            title = false
        }
        else{
            title = true
        }
        if (Utils.isEmpty(etotherinfo)) {
            etotherinfo.setError(context.getString(R.string.txt_required_filed))
            info = false
        }
        else{
            info = true
        }

        if(type && purpose && listedby && plotarea && title && info)
        {
           return true
        }
        Utils.showToast(context, context.getString(R.string.txt_check_all_reuired_fileds))
        return false
    }

    fun checkForPartyPlot(
        context: PartyPlotActivity,
        adPartyPlotList: AdPartyPlotBean,
        etpartyplotname: EditText,
        etguestcapacity: EditText,
        etnoofrooms: EditText,
        etadtitile: EditText,
        etotherinfo: EditText,
        guestroom :Boolean
    ): Boolean {
        val purpose : Boolean
        val listedby : Boolean
        val name : Boolean
        val guestcapacity : Boolean
        var noofrooms : Boolean = guestroom
        val title : Boolean
        val info : Boolean

        if(adPartyPlotList.purpose.toString().length > 0)
        {
            purpose = true
        }
        else{
            purpose = false
            Utils.showToast(context, context.getString(R.string.txt_select_purpose))
        }

        if(adPartyPlotList.listed_by.toString().length > 0)
        {
            listedby = true
        }
        else{
            listedby = false
            Utils.showToast(context, context.getString(R.string.txt_select_listedby))
        }

        if (Utils.isEmpty(etpartyplotname)) {
            etpartyplotname.setError(context.getString(R.string.txt_required_filed))
            name = false
        }
        else{
            name = true
        }
        if (Utils.isEmpty(etguestcapacity)) {
            etguestcapacity.setError(context.getString(R.string.txt_required_filed))
            guestcapacity = false
        }
        else{
            guestcapacity = true
        }

        if(guestroom) {
            if (Utils.isEmpty(etnoofrooms)) {
                etnoofrooms.setError(context.getString(R.string.txt_required_filed))
                noofrooms = false
            } else {
                noofrooms = true
            }
        }
        if (Utils.isEmpty(etadtitile)) {
            etadtitile.setError(context.getString(R.string.txt_required_filed))
            title = false
        }
        else{
            title = true
        }
        if (Utils.isEmpty(etotherinfo)) {
            etotherinfo.setError(context.getString(R.string.txt_required_filed))
            info = false
        }
        else{
            info = true
        }

        if(guestroom)
        {
            if(purpose && listedby && name && guestcapacity && noofrooms && title && info)
            {
                return true
            }
        }
        else{
            if(purpose && listedby && name && guestcapacity && title && info)
            {
                return true
            }
        }
        Utils.showToast(context, context.getString(R.string.txt_check_all_reuired_fileds))
      return false
    }

    fun checkForHotelResort(
        context: HotelDetailActivity,
        adHotelandResortList: HotelandResortBean,
        etadtitile: EditText,
        etotherinfo: EditText,
        etpropertyname :EditText
    ): Boolean {
        val type : Boolean
        val purpose : Boolean
        val listedby : Boolean
        val title : Boolean
        val info : Boolean
        val name : Boolean

        if(adHotelandResortList.property_type.toString().length > 0)
        {
            type = true
        }
        else{
            type = false
            Utils.showToast(context, context.getString(R.string.txt_select_type))
        }

        if(adHotelandResortList.purpose.toString().length > 0)
        {
            purpose = true
        }
        else{
            purpose = false
            Utils.showToast(context, context.getString(R.string.txt_select_purpose))
        }

        if(adHotelandResortList.listed_by.toString().length > 0)
        {
            listedby = true
        }
        else{
            listedby = false
            Utils.showToast(context, context.getString(R.string.txt_select_listedby))
        }

         if (Utils.isEmpty(etpropertyname)) {
              etpropertyname.setError(context.getString(R.string.txt_required_filed))
              name = false
         }
         else{
             name = true
         }
        /* if (Utils.isEmpty(ettotalrooms)) {
            ettotalrooms.setError(context.getString(R.string.txt_required_filed))
            return false
        }*/

        if (Utils.isEmpty(etadtitile)) {
            etadtitile.setError(context.getString(R.string.txt_required_filed))
            title = false
        }
        else{
            title = true
        }
        if (Utils.isEmpty(etotherinfo)) {
            etotherinfo.setError(context.getString(R.string.txt_required_filed))
            info = false
        }
        else{
            info = true
        }

        if(name && type && purpose && listedby && title && info)
        {
            return true
        }
        Utils.showToast(context, context.getString(R.string.txt_check_all_reuired_fileds))
       return false
    }

    fun checkForLabourWarehouse(
        context: LabourDetailActivity,
        labourWareHouseBean: LabourWareHouseBean,
        etadtitile: EditText,
        etotherinfo: EditText
    ): Boolean {
        val type : Boolean
        val purpose : Boolean
        val listedby : Boolean
        val title : Boolean
        val info : Boolean


        if(labourWareHouseBean.property_type.toString().length > 0)
        {
            type = true
        }
        else{
            type = false
            Utils.showToast(context, context.getString(R.string.txt_select_type))
        }

        if(labourWareHouseBean.purpose.toString().length > 0)
        {
            purpose = true
        }
        else{
            purpose = false
            Utils.showToast(context, context.getString(R.string.txt_select_purpose))
        }

        if(labourWareHouseBean.listed_by.toString().length > 0)
        {
            listedby = true
        }
        else{
            listedby = false
            Utils.showToast(context, context.getString(R.string.txt_select_listedby))
        }

        if (Utils.isEmpty(etadtitile)) {
            etadtitile.setError(context.getString(R.string.txt_required_filed))
            title = false
        }
        else{
            title = true
        }
        if (Utils.isEmpty(etotherinfo)) {
            etotherinfo.setError(context.getString(R.string.txt_required_filed))
            info = false
        }
        else
        {
            info = true
        }
        if(type && purpose && listedby && title && info)
        {
            return true
        }
        Utils.showToast(context, context.getString(R.string.txt_check_all_reuired_fileds))
        return false
    }

    fun checkForPgGuestHouse(
        context: PgDetailActivity,
        adGuestHouseList: PgGuestHouseBean,
        etadtitle: EditText,
        etotherinfo: EditText
    ): Boolean {
        val type : Boolean
        val furnishing : Boolean
        val listedby : Boolean
        val title : Boolean
        val info : Boolean

        if(adGuestHouseList.property_type.toString().length > 0)
        {
            type = true
        }
        else{
            type = false
            Utils.showToast(context, context.getString(R.string.txt_select_type))
        }

        if(adGuestHouseList.furnishing.toString().length > 0)
        {
            furnishing = true
        }
        else{
            furnishing = false
            Utils.showToast(context, context.getString(R.string.txt_select_furnishing))
        }

        if(adGuestHouseList.listed_by.toString().length > 0)
        {
            listedby = true
        }
        else{
            listedby = false
            Utils.showToast(context, context.getString(R.string.txt_select_listedby))
        }

        if (Utils.isEmpty(etadtitle)) {
            etadtitle.setError(context.getString(R.string.txt_required_filed))
            title = false
        }
        else{
            title = true
        }
        if (Utils.isEmpty(etotherinfo)) {
            etotherinfo.setError(context.getString(R.string.txt_required_filed))
            info = false
        }
        else{
            info = true
        }

        if(type && furnishing && listedby && title && info)
        {
            return true
        }
        Utils.showToast(context, context.getString(R.string.txt_check_all_reuired_fileds))
        return false
    }

    fun checkForGrainSeed(
        context: AddSeedDetailsActivity,
        adGrainSeedBean: AdGrainSeedBean,
        etStock: CustomEditText,
        etPrice: CustomEditText,
        etadtitle: EditText,
        etotherinfo: EditText
    ): Boolean {

        val listedby : Boolean
        val stock : Boolean
        val price: Boolean
        val title : Boolean
        val info : Boolean


        if(adGrainSeedBean.listed_by.toString().length > 0)
        {
            listedby = true
        }
        else{
            listedby = false
            Utils.showToast(context, context.getString(R.string.txt_select_listedby))
        }

        if (Utils.isEmpty(etStock)) {
            etStock.setError(context.getString(R.string.txt_required_filed))
            stock = false

        }
        else{
            stock = true
        }
        if (Utils.isEmpty(etPrice)) {
            etPrice.setError(context.getString(R.string.txt_required_filed))
            price = false
        }
        else{
            price = true
        }
        if (Utils.isEmpty(etadtitle)) {
            etadtitle.setError(context.getString(R.string.txt_required_filed))
            title = false
        }
        else{
            title = true
        }

        if (Utils.isEmpty(etotherinfo)) {
            etotherinfo.setError(context.getString(R.string.txt_required_filed))
            info = false
        }
        else{
            info = true
        }

        if(listedby && stock && price && title && info)
        {
            return true
        }
        Utils.showToast(context, context.getString(R.string.txt_check_all_reuired_fileds))
        return false
    }
}