package com.kaushlesh.utils.validations

import android.app.Activity
import android.content.Context
import android.widget.EditText
import com.kaushlesh.R
import com.kaushlesh.bean.AdPost.AdKhedutBean
import com.kaushlesh.bean.AdPost.AdPostVehiclesBean
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.AdPost.Khedut.AddAnimalActivity
import com.kaushlesh.view.activity.AdPost.Khedut.GrainFruitVegActivity
import com.kaushlesh.view.activity.AdPost.Khedut.KedutOtherCategoryActivity
import com.kaushlesh.view.activity.AdPost.Khedut.TractorTrolleyActivity
import com.kaushlesh.view.activity.AdPost.Vehicles.AddBrandDetailActivity
import com.kaushlesh.view.activity.AdPost.Vehicles.AddCommercialVehicleDetailActivity
import com.kaushlesh.view.activity.AdPost.Vehicles.CarSellDetailActivity
import com.kaushlesh.widgets.CustomEditText

object AdPostKhedutValidation {
    fun checkForGrainFruitVeg(context: GrainFruitVegActivity, adKhedutBean: AdKhedutBean, etStock: CustomEditText,
                              etPrice: CustomEditText, etadtitle: EditText, etotherinfo: EditText): Boolean {

        val listedby : Boolean
        val stock : Boolean
        val price: Boolean
        val title : Boolean
        val info : Boolean

        if(adKhedutBean.listed_by.toString().length > 0)
        {
            listedby = true
        }
        else{
            listedby = false
            Utils.showToast(context, context.getString(R.string.txt_select_listedby))
        }

        if (Utils.isEmpty(etStock)) {
            etStock.setError(context.getString(R.string.txt_required_filed))
            stock = false

        }
        else{
            stock = true
        }
        if (Utils.isEmpty(etPrice)) {
            etPrice.setError(context.getString(R.string.txt_required_filed))
            price = false
        }
        else{

            price = true

        }
        if (Utils.isEmpty(etadtitle)) {
            etadtitle.setError(context.getString(R.string.txt_required_filed))
            title = false
        }
        else{
            title = true
        }

        if (Utils.isEmpty(etotherinfo)) {
            etotherinfo.setError(context.getString(R.string.txt_required_filed))
            info = false
        }
        else{
            info = true
        }

        if(listedby && stock && price && title && info)
        {
            return true
        }
        Utils.showToast(context, context.getString(R.string.txt_check_all_reuired_fileds))
        return false
    }

    fun checkForAnimal(context: AddAnimalActivity, adKhedutBean: AdKhedutBean, etPrice: CustomEditText, etadtitle: EditText, etotherinfo: EditText): Boolean {

        val price: Boolean
        val title : Boolean
        val info : Boolean

        if (Utils.isEmpty(etPrice)) {
            etPrice.setError(context.getString(R.string.txt_required_filed))
            price = false
        }
        else{

            price = true

        }
        if (Utils.isEmpty(etadtitle)) {
            etadtitle.setError(context.getString(R.string.txt_required_filed))
            title = false
        }
        else{
            title = true
        }

        if (Utils.isEmpty(etotherinfo)) {
            etotherinfo.setError(context.getString(R.string.txt_required_filed))
            info = false
        }
        else{
            info = true
        }

        if(price && title && info)
        {
            return true
        }
        Utils.showToast(context, context.getString(R.string.txt_check_all_reuired_fileds))
        return false
    }

    fun checkForKhedutComman(context: Activity, adKhedutBean: AdKhedutBean, etadtitle: EditText, etotherinfo: EditText): Boolean {
        val title : Boolean
        val info : Boolean
        if (Utils.isEmpty(etadtitle)) {
            etadtitle.setError(context.getString(R.string.txt_required_filed))
            title = false
        }
        else{
            title = true
        }

        if (Utils.isEmpty(etotherinfo)) {
            etotherinfo.setError(context.getString(R.string.txt_required_filed))
            info = false
        }
        else{
            info = true
        }

        if(title && info)
        {
            return true
        }
        Utils.showToast(context, context.getString(R.string.txt_check_all_reuired_fileds))
        return false
    }

    fun checkForTractorTrolly(context: TractorTrolleyActivity, adKhedutBean: AdKhedutBean, etPrice: CustomEditText, etadtitle: EditText, etotherinfo: EditText): Boolean {
        val price: Boolean
        val title : Boolean
        val info : Boolean

        if (Utils.isEmpty(etPrice)) {
            etPrice.setError(context.getString(R.string.txt_required_filed))
            price = false
        }
        else{

            price = true

        }
        if (Utils.isEmpty(etadtitle)) {
            etadtitle.setError(context.getString(R.string.txt_required_filed))
            title = false
        }
        else{
            title = true
        }

        if (Utils.isEmpty(etotherinfo)) {
            etotherinfo.setError(context.getString(R.string.txt_required_filed))
            info = false
        }
        else{
            info = true
        }

        if(price && title && info)
        {
            return true
        }
        Utils.showToast(context, context.getString(R.string.txt_check_all_reuired_fileds))
        return false
    }

}