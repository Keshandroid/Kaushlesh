package com.kaushlesh.utils.validations

import android.content.Context
import android.widget.EditText
import com.kaushlesh.R
import com.kaushlesh.bean.AdPost.AdPostVehiclesBean
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.AdPost.Vehicles.AddBrandDetailActivity
import com.kaushlesh.view.activity.AdPost.Vehicles.AddCommercialVehicleDetailActivity
import com.kaushlesh.view.activity.AdPost.Vehicles.CarSellDetailActivity
import com.kaushlesh.widgets.CustomEditText
import java.text.SimpleDateFormat
import java.util.*

object AdPostVehiclesValidation {
    fun checkForCarForSell(
        context: CarSellDetailActivity,
        adPostVehiclesBean: AdPostVehiclesBean,
        etyear: EditText,
        etkm: EditText,
        etadtitle: EditText,
        etotherinfo: EditText
    ): Boolean {
        val year : Boolean
        val fuel : Boolean
        val transimission : Boolean
        val kmdriven : Boolean
        val noofowner: Boolean
        val title : Boolean
        val info : Boolean

        if(adPostVehiclesBean.transmission.toString().length > 0)
        {
            transimission = true
        }
        else{
            transimission = false
            Utils.showToast(context, context.getString(R.string.txt_select_transmission))
        }

        if(adPostVehiclesBean.noOfowners.toString().length > 0)
        {
            noofowner = true
        }
        else{
            noofowner = false
            Utils.showToast(context, context.getString(R.string.txt_select_no_of_owners))
        }

        if(adPostVehiclesBean.fuel.toString().length > 0)
        {
            fuel = true
        }
        else{
            fuel = false
            Utils.showToast(context, context.getString(R.string.txt_select_fuel))
        }

        if (Utils.isEmpty(etyear)) {
            etyear.setError(context.getString(R.string.txt_required_filed))
            year = false

        }
        else{

            if(etyear.text.toString().toString().isNotEmpty() && etyear.text.toString().toString().length != 4 && etyear.text.toString().toString().startsWith("0"))
            {
                Utils.showToast(context, context.resources.getString(R.string.txt_valid_year))
                year = false
            }
            else {
                val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                val curDate: String = sdf.format(Date())
                val startyear = curDate.split("-")[0]
                if (startyear.toInt() < etyear.text.toString().toInt()) {
                    Utils.showToast(context, context.resources.getString(R.string.txt_valid_year))
                    year = false
                } else {
                    year = true
                }
            }
        }

        if (Utils.isEmpty(etkm)) {
            etkm.setError(context.getString(R.string.txt_required_filed))
            kmdriven = false
        }
        else{
            kmdriven = true
        }
        if (Utils.isEmpty(etadtitle)) {
            etadtitle.setError(context.getString(R.string.txt_required_filed))
            title = false
        }
        else{
            title = true
        }

        if (Utils.isEmpty(etotherinfo)) {
            etotherinfo.setError(context.getString(R.string.txt_required_filed))
            info = false
        }
        else{
            info = true
        }

        if(year && transimission && fuel && kmdriven && noofowner && title && info)
        {
            return true
        }
        Utils.showToast(context, context.getString(R.string.txt_check_all_reuired_fileds))
        return false
    }

    fun checkForCommercialVehicleForSell(
        context: AddCommercialVehicleDetailActivity,
        adPostVehiclesBean: AdPostVehiclesBean,
        etYear: CustomEditText,
        etKm: CustomEditText,
        etadtitle: EditText,
        etotherinfo: EditText
    ): Boolean {
        val year : Boolean
        val kmdriven : Boolean
        val title : Boolean
        val info : Boolean

        if (Utils.isEmpty(etYear)) {
            etYear.setError(context.getString(R.string.txt_required_filed))
            year = false

        }
        else{

            if(etYear.text.toString().toString().isNotEmpty() && etYear.text.toString().toString().length != 4 && etYear.text.toString().toString().startsWith("0"))
            {
                Utils.showToast(context, context.resources.getString(R.string.txt_valid_year))
                year = false
            }
            else {
                val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                val curDate: String = sdf.format(Date())
                val startyear = curDate.split("-")[0]
                if (startyear.toInt() < etYear.text.toString().toInt()) {
                    Utils.showToast(context, context.resources.getString(R.string.txt_valid_year))
                    year = false
                } else {
                    year = true
                }
            }
        }

        if (Utils.isEmpty(etKm)) {
            etKm.setError(context.getString(R.string.txt_required_filed))
            kmdriven = false
        }
        else{
            kmdriven = true
        }
        if (Utils.isEmpty(etadtitle)) {
            etadtitle.setError(context.getString(R.string.txt_required_filed))
            title = false
        }
        else{
            title = true
        }

        if (Utils.isEmpty(etotherinfo)) {
            etotherinfo.setError(context.getString(R.string.txt_required_filed))
            info = false
        }
        else{
            info = true
        }

        if(year && kmdriven && title && info)
        {
            return true
        }
        Utils.showToast(context, context.getString(R.string.txt_check_all_reuired_fileds))
        return false
    }

    fun checkForCarDecorAutoParts(
        context: AddBrandDetailActivity,
        adPostVehiclesBean: AdPostVehiclesBean,
        etadtitle: EditText,
        etotherinfo: EditText
    ): Boolean {

        val title : Boolean
        val info : Boolean

        if (Utils.isEmpty(etadtitle)) {
            etadtitle.setError(context.getString(R.string.txt_required_filed))
            title = false
        }
        else{
            title = true
        }

        if (Utils.isEmpty(etotherinfo)) {
            etotherinfo.setError(context.getString(R.string.txt_required_filed))
            info = false
        }
        else{
            info = true
        }

        if(title && info)
        {
            return true
        }
        Utils.showToast(context, context.getString(R.string.txt_check_all_reuired_fileds))
        return false
    }
}