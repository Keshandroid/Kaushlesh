package com.kaushlesh.utils.validations

import android.widget.EditText
import com.kaushlesh.R
import com.kaushlesh.bean.AdPost.AdPostMobilesBean
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.AdPost.Mobiles.AccesoriesDetailActivity
import com.kaushlesh.view.activity.AdPost.Mobiles.AddBrandDetailMobileActivity
import com.kaushlesh.view.activity.AdPost.Mobiles.AddBrandDetailTabsActivity

object AdPostMobilesValidation {
    fun checkForMobile(
        context: AddBrandDetailMobileActivity,
        adPostMobilesBean: AdPostMobilesBean,
        etadtitle: EditText,
        etotherinfo: EditText
    ): Boolean {
        val brandid: Boolean
        val title : Boolean
        val info : Boolean

        if(adPostMobilesBean.brandid.length > 0)
        {
            brandid = true
        }
        else{
            brandid = false
            Utils.showToast(context,  context.getString(R.string.txt_select_brand))
        }

        if (Utils.isEmpty(etadtitle)) {
            etadtitle.setError(context.getString(R.string.txt_required_filed))
            title = false
        }
        else{
            title = true
        }
        if (Utils.isEmpty(etotherinfo)) {
            etotherinfo.setError(context.getString(R.string.txt_required_filed))
            info = false
        }
        else{
            info = true
        }

        if(brandid && title && info)
        {
            return true
        }
        Utils.showToast(context, context.getString(R.string.txt_check_all_reuired_fileds))
        return false
    }

    fun checkForTab(
        context: AddBrandDetailTabsActivity,
        adPostMobilesBean: AdPostMobilesBean,
        etadtitle: EditText,
        etotherinfo: EditText
    ): Boolean {
        val brandid: Boolean
        val title : Boolean
        val info : Boolean

        if(adPostMobilesBean.brandid.length > 0)
        {
            brandid = true
        }
        else{
            brandid = false
            Utils.showToast(context, context.getString(R.string.txt_select_brand))
        }

        if (Utils.isEmpty(etadtitle)) {
            etadtitle.setError(context.getString(R.string.txt_required_filed))
            title = false
        }
        else{
            title = true
        }
        if (Utils.isEmpty(etotherinfo)) {
            etotherinfo.setError(context.getString(R.string.txt_required_filed))
            info = false
        }
        else{
            info = true
        }

        if(brandid && title && info)
        {
            return true
        }
        Utils.showToast(context, context.getString(R.string.txt_check_all_reuired_fileds))
        return false
    }

    fun checkForAccessories(
        context: AccesoriesDetailActivity,
        adPostMobilesBean: AdPostMobilesBean,
        etadtitile: EditText,
        etotherinfo: EditText
    ): Boolean {
        val type: Boolean
        val title : Boolean
        val info : Boolean

        if(adPostMobilesBean.mobiletype.toString().length > 0)
        {
            type = true
        }
        else{
            type = false
            Utils.showToast(context, context.getString(R.string.txt_select_type))
        }

        if (Utils.isEmpty(etadtitile)) {
            etadtitile.setError(context.getString(R.string.txt_required_filed))
            title = false
        }
        else{
            title = true
        }
        if (Utils.isEmpty(etotherinfo)) {
            etotherinfo.setError(context.getString(R.string.txt_required_filed))
            info = false
        }
        else{
            info = true
        }

        if(type && title && info)
        {
            return true
        }
        Utils.showToast(context, context.getString(R.string.txt_check_all_reuired_fileds))
        return false
    }
}