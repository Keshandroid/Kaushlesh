package com.kaushlesh.utils.validations

import android.app.Activity
import android.content.Context
import android.widget.EditText
import com.kaushlesh.R
import com.kaushlesh.bean.AdPost.AdPostGeneralBean
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.AdPost.AddDetailsActivity
import com.kaushlesh.widgets.CustomEditText

object AdPostGeneralValidation {
    fun checkForCommon(
        context: Context,
        adPostGeneralBean: AdPostGeneralBean,
        etadtitle: EditText,
        etotherinfo: EditText
    ): Boolean {
        val title : Boolean
        val info : Boolean
        if (Utils.isEmpty(etadtitle)) {
            etadtitle.setError(context.getString(R.string.txt_required_filed))
            title = false
        }
        else{
            title = true
        }
        if (Utils.isEmpty(etotherinfo)) {
            etotherinfo.setError(context.getString(R.string.txt_required_filed))
            info = false
        }
        else{
            info = true
        }

        if(title && info)
        {
            return true
        }
        Utils.showToast(context as Activity, context.getString(R.string.txt_check_all_reuired_fileds))
        return false
    }

    fun checkForShopsInCity(context: Context, adPostGeneralBean: AdPostGeneralBean, etadtitle: EditText, etotherinfo: EditText, etShopName: EditText): Boolean {
        val title : Boolean
        val info : Boolean
        val shopname : Boolean

        if (Utils.isEmpty(etShopName)) {
            etShopName.setError(context.getString(R.string.txt_required_filed))
            shopname = false
        }
        else{
            shopname = true
        }

        if (Utils.isEmpty(etadtitle)) {
            etadtitle.setError(context.getString(R.string.txt_required_filed))
            title = false
        }
        else{
            title = true
        }
        if (Utils.isEmpty(etotherinfo)) {
            etotherinfo.setError(context.getString(R.string.txt_required_filed))
            info = false
        }
        else{
            info = true
        }

        if(title && info && shopname)
        {
            return true
        }
        Utils.showToast(context as Activity, context.getString(R.string.txt_check_all_reuired_fileds))
        return false
    }
}