package com.kaushlesh.utils.validations

import android.app.Activity
import android.content.Context
import android.widget.EditText
import com.kaushlesh.R
import com.kaushlesh.bean.AdPost.AdPostBikesBean
import com.kaushlesh.bean.AdPost.AdPostVehiclesBean
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.AdPost.Bikes.AddSparePartsActivity
import com.kaushlesh.view.activity.AdPost.Bikes.AddVehicleDetailActivity
import com.kaushlesh.view.activity.AdPost.Vehicles.CarSellDetailActivity
import com.kaushlesh.widgets.CustomEditText
import java.text.SimpleDateFormat
import java.util.*

object AdPostBikesValidation {
    fun checkForMotorScooter(
        context: AddVehicleDetailActivity,
        adPostBikesBean: AdPostBikesBean,
        etYear: CustomEditText,
        etKm: CustomEditText,
        etadtitle: EditText,
        etotherinfo: EditText
    ): Boolean {
        val year : Boolean
        val kmdriven : Boolean
        val title : Boolean
        val info : Boolean


        if (Utils.isEmpty(etYear)) {
            etYear.setError(context.getString(R.string.txt_required_filed))
            year = false

        }
        else{

            if(etYear.text.toString().toString().isNotEmpty() && etYear.text.toString().toString().length != 4 && etYear.text.toString().toString().startsWith("0"))
            {
                Utils.showToast(context, context.resources.getString(R.string.txt_valid_year))
                year = false
            }
            else {
                val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                val curDate: String = sdf.format(Date())
                val startyear = curDate.split("-")[0]
                if (startyear.toInt() < etYear.text.toString().toInt()) {
                    Utils.showToast(context, context.resources.getString(R.string.txt_valid_year))
                    year = false
                } else {
                    year = true
                }
            }
        }
        if (Utils.isEmpty(etKm)) {
            etKm.setError(context.getString(R.string.txt_required_filed))
            kmdriven = false
        }
        else{
            kmdriven = true
        }
        if (Utils.isEmpty(etadtitle)) {
            etadtitle.setError(context.getString(R.string.txt_required_filed))
            title = false
        }
        else{
            title = true
        }

        if (Utils.isEmpty(etotherinfo)) {
            etotherinfo.setError(context.getString(R.string.txt_required_filed))
            info = false
        }
        else{
            info = true
        }

        if(year && kmdriven && title && info)
        {
            return true
        }
        Utils.showToast(context, context.getString(R.string.txt_check_all_reuired_fileds))
        return false
    }

    fun checkForSparePartsBicycle(
        context: Activity,
        adPostBikesBean: AdPostBikesBean,
        etadtitle: EditText,
        etotherinfo: EditText
    ): Boolean {
        val title : Boolean
        val info : Boolean
        if (Utils.isEmpty(etadtitle)) {
            etadtitle.setError(context.getString(R.string.txt_required_filed))
            title = false
        }
        else{
            title = true
        }
        if (Utils.isEmpty(etotherinfo)) {
            etotherinfo.setError(context.getString(R.string.txt_required_filed))
            info = false
        }
        else{
            info = true
        }

        if(title && info)
        {
            return true
        }
        Utils.showToast(context,  context.getString(R.string.txt_check_all_reuired_fileds))
        return false
    }

}