package com.kaushlesh.utils

import android.content.Context
import com.kaushlesh.R

object Constants {

  public  var CATEGORY_NAME = "categoryname"
  public  var CATEGORY_ID = "categoryId"

  public  var USER_NAME = "username"
  public  var USER_ID = "userid"
  public  var TOKEN = "token"
  public  var IS_LOGGED_IN = "isloggedin"
  public  var IS_OTP_VERIFED = "isOTPVerified"
  public  var IS_ACTIVE = "isActive"
  public  var DEVICE_TOKEN = "device_token"
  public  var DEVICE_TYPE = "device_type"
  public  var REGISTREATION_TYPE = "registrationType"
  public  var BUSINESS_USER_ID = "bussinessprofile_id"

  public  var NAME = "firstName"
  public  var CONTACT_NO = "ContactNo"
  public  var EMAIL = "Email"



  public  var USER_BLOCK = "user_block"
  public  var PHONE_NUMBER = ""
  public  var PROFILE = ""
  var SUBCATEGORY_NMAE = "subcategoryname"
  var SUBCATEGORYID = "subcategoryid"
  var APVILLARENTORSELL = "apvillasellrent"
  var APVILLSELLONE = "appvillsellone"
  var ADDOFFICESELLANDRENT = "adOfficeForSell"
  var ADDPLOTANDLAND = "adPlotAndLand"
  var ADDPARTYPLOT = "addPartyPlot"
  var ADDHOTELANDRESORT = "adhotelandresort"
  var ADDLABOURWAREHOUSE = "addlabourwarehouse"
  var ADDPGGUSTHOUSE = "addpgguesthouse"


  var LATITUDE = "latitude"
  var LONGITUDE = "longitude"
  var SEARCHPRODUCT = "searchproduct"
  var SEARCHPRODUCTValue = "0"

  var ADDMOBILE = "addmobile"
  var ADDGENERAL = "addgeneral"
  var ADDVEHICLES = "addvehicles"
  var ADDBIKES = "addbikes"

  var ADDGRAINSEED = "addgrainseed"
  var ADDKHEDUT = "addkhedut"

  var POST_ID = "post_id"
  var OPEN_POST_DETAIL = "open_post_detail"
  var POST_FROM = "post_from"
  var LOCATION_ID = "location_id"
  var USER_PKG_ID = "user_pkg_id"
  var LOC_ID_PKG = "location_id_pkg"
    var LOC_NAME = "location_name"

  const val INTENT_EXTRA_LIMIT = "limit"
  const val INTENT_EXTRA_IMAGES = "images"
    const val SCROLL_START = "scroll_start"
    const val HOME_LOC_FILTER = "home_loc_filter"
  const val GET_LOCA_DATA = "get_loc_data"
    const val FROM_DETAIL = "from_detail"

  fun getCategoryFromId(catid: Int, context: Context) : String{
    when(catid)
    {
      1 -> {
        return context.getString(R.string.txt_vehicle)
      }
      2 -> {
        return context.getString(R.string.txt_properties)
      }
      3 -> {
        return context.getString(R.string.txt_mobiles)
      }
      4 -> {
        return context.getString(R.string.txt_bikes)
      }
      5 -> {
        return context.getString(R.string.txt_electronics)
      }
      6 -> {
        return context.getString(R.string.txt_furniture)
      }
      7 -> {
        return context.getString(R.string.txt_shops_in_city)
      }
      8 -> {
        return context.getString(R.string.txt_fashion_beauty)
      }
      9 -> {
        return context.getString(R.string.txt_art_lifestyle)
      }
      10 -> {
        return context.getString(R.string.txt_services)
      }
      11 -> {
        return context.getString(R.string.txt_jobs)
      }
      12 -> {
        //return context.getString(R.string.txt_grain_seed)
          return context.getString(R.string.txt_khedut)
      }

    }
    return ""
  }

}