package com.kaushlesh.utils

import android.annotation.SuppressLint
import android.os.AsyncTask
import android.util.Log

object BackgroundTask {
    @SuppressLint("StaticFieldLeak")
    fun <T> with(className: String?, listener: Listener<T>) {
        if (!listener.canStart()) {
            listener.taskCompleted(null as T)
            return
        }
        Log.e(className, "with Invoked")
        object : AsyncTask<Void?, Void?, T?>() {
            protected override fun doInBackground(vararg voids: Void?): T {
                Log.e(className, "doInBackground Invoked")
                try {
                    return listener.backgroundTask()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                return null as T
            }

            override fun onPostExecute(aVoid: T?) {
                super.onPostExecute(aVoid)
                Log.e(className, "onPostExecute Invoked")
                try {
                    listener.taskCompleted(aVoid as T)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
    }

    interface Listener <T> {
        fun canStart(): Boolean
        fun backgroundTask() : T
        fun taskCompleted(result : T)
    }
}