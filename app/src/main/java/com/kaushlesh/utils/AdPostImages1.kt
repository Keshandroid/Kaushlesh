package com.kaushlesh.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.util.Log
import android.widget.Toast
import com.google.gson.Gson
import com.kaushlesh.R
import com.kaushlesh.bean.AdPost.*
import com.kaushlesh.bean.ImagesBean
import com.kaushlesh.view.activity.AdPost.SetLocationActivity
import com.kaushlesh.view.activity.AdPost.SetPriceActivity
import java.io.FileNotFoundException
import java.io.InputStream

object AdPostImages1 {

  internal var bimglist: ArrayList<Bitmap> = ArrayList()

  fun changeScreentoNext(context: Context) {
    val intent = Intent(context, SetPriceActivity::class.java)
    context.startActivity(intent)
  }

  private fun getBitmapImagelist(context: Context,imglist:ArrayList<Bitmap>): ArrayList<Bitmap> {
    for (i in imglist.indices) {
     /* val uri = Uri.parse(imglist.get(i).getImg().toString())
      //val uri = Uri.parse(result)
      var inputStream: InputStream? = null
      try {
        inputStream = context.getContentResolver()?.openInputStream(uri)
      } catch (e: FileNotFoundException) {
        e.printStackTrace()
      }

      val bmp = BitmapFactory.decodeStream(inputStream)
      bimglist.add(bmp)*/

        bimglist.add(imglist.get(i) as Bitmap)
    }
    return bimglist
  }

  fun checkCatAnSub(context: Context, imglist:ArrayList<Bitmap>, catId: String, subCatId: String) {
    if(catId.toInt() == 2)
    {
      CheckForSubcategryProperty(subCatId,context,imglist)
    }

    if(catId.toInt() == 3)
    {
      checkAndSaveMobiles(context,imglist)
    }

    if(catId.toInt() == 7)
    {
      checkAndSaveShopsInCity(context,imglist)
    }

    if(catId.toInt() == 1)
    {
      checkAndSaveVehicles(context,imglist)
    }

    if(catId.toInt() == 4)
    {
      checkAndSaveBikes(context,imglist)
    }

    if(catId.toInt() == 5)
    {
      //electoronics
      checkAndSaveGeneral(context,imglist)
    }

    if(catId.toInt() == 6)
    {
      checkAndSaveGeneral(context,imglist)
    }

    if(catId.toInt() == 8)
    {
      checkAndSaveGeneral(context,imglist)
    }

    if(catId.toInt() == 9)
    {
      checkAndSaveGeneral(context,imglist)
    }

    if(catId.toInt() == 10)
    {
      checkAndSaveShopsInCity(context,imglist)
    }

    if(catId.toInt() == 11)
    {
      checkAndSaveShopsInCity(context,imglist)
    }

    if(catId.toInt() == 12)
    {
      checkAndSaveGrainSeed(context,imglist)
    }
  }

  private fun CheckForSubcategryProperty(
    subCatId: String,
    context: Context,
    imglist: ArrayList<Bitmap>) {

    when (subCatId.toInt()) {
      1->
      {
        checkAndSaveApVilla(context,imglist)
      }
      2 ->
      {
        checkAndSaveApVilla(context,imglist)
      }
      3 ->
      {
        checkAndSaveOffice(context,imglist)
      }
      4 ->{
        checkAndSaveOffice(context,imglist)
      }
      5 ->{
        checkAndSavePlotLand(context,imglist)
      }
      6 ->{
        checkAndSavePartyPlot(context,imglist)
      }
      7 ->{
        checkAndSaveHotelResort(context,imglist)
      }
      8 ->{
        checkAndSaveLabourWarehouse(context,imglist)
      }
      9 ->{
        checkAndSavePgGuesthouse(context,imglist)
      }
    }
  }

  fun checkAndSaveApVilla(context: Context, imglist: ArrayList<Bitmap>) {
   // bimglist.clear()
    val storeUserData = StoreUserData(context)
    val gson = Gson()
    if (imglist.size>=1) {

      val json: String = storeUserData.getString(Constants.APVILLSELLONE)
      val adVillsellandRentBeanList = gson.fromJson(json, ApVillsellandRentBean::class.java)
      Log.e("test", "listsave data: " + adVillsellandRentBeanList.toString())

     /// bimglist = getBitmapImagelist(context,imglist)
      adVillsellandRentBeanList.post_images?.clear()
      adVillsellandRentBeanList.post_images = imglist

      Log.e("test", "imagelist: " + imglist.size)

      Log.e("test", "imagelist data: " + adVillsellandRentBeanList.post_images)

      val jsonset = gson.toJson(adVillsellandRentBeanList)
      storeUserData.setString(Constants.APVILLSELLONE, jsonset)

      changeScreentoNext(context)

    }else{
      Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
    }
  }

  fun checkAndSaveOffice(context: Context, imglist:ArrayList<Bitmap>) {
    //bimglist.clear()
    val storeUserData = StoreUserData(context)
    val gson = Gson()
    if (imglist.size>=1) {

      val json: String = storeUserData.getString(Constants.ADDOFFICESELLANDRENT)
      val adOfficeForSellBeanList = gson.fromJson(json, AdOfficeForSellRentBean::class.java)
      Log.e("test", "listsave data: " + adOfficeForSellBeanList.toString())

     // bimglist = getBitmapImagelist(context,imglist)

      adOfficeForSellBeanList.post_images?.clear()

      adOfficeForSellBeanList.post_images = imglist

      Log.e("test", "imagelist: " + imglist.size)

      Log.e("test", "imagelist data: " + adOfficeForSellBeanList.post_images)

      val jsonset = gson.toJson(adOfficeForSellBeanList)
      storeUserData.setString(Constants.ADDOFFICESELLANDRENT, jsonset)

      changeScreentoNext(context)

    }else{
      Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
    }
  }

  fun checkAndSavePlotLand(context: Context, imglist:ArrayList<Bitmap>) {
   // bimglist.clear()
    val storeUserData = StoreUserData(context)
    val gson = Gson()
    if (imglist.size>=1) {

      val json: String = storeUserData.getString(Constants.ADDPLOTANDLAND)
      val adPlotAndLandBean = gson.fromJson(json, AdPlotAndLandBean::class.java)
      Log.e("test", "listsave data: " + adPlotAndLandBean.toString())

   //   bimglist = getBitmapImagelist(context,imglist)


      adPlotAndLandBean.post_images?.clear()

      adPlotAndLandBean.post_images = imglist

      Log.e("test", "imagelist: " + imglist.size)

      Log.e("test", "imagelist data: " + adPlotAndLandBean.post_images)

      val jsonset = gson.toJson(adPlotAndLandBean)
      storeUserData.setString(Constants.ADDPLOTANDLAND, jsonset)

      changeScreentoNext(context)

    }else{
      Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
    }
  }

  fun checkAndSavePartyPlot(context: Context, imglist: ArrayList<Bitmap>) {
    //bimglist.clear()
    val storeUserData = StoreUserData(context)
    val gson = Gson()
    if (imglist.size>=1) {

      val json: String = storeUserData.getString(Constants.ADDPARTYPLOT)
      val adPartyPlotBean = gson.fromJson(json, AdPartyPlotBean::class.java)
      Log.e("test", "listsave data: " + adPartyPlotBean.toString())

      //bimglist = getBitmapImagelist(context,imglist)
      adPartyPlotBean.post_images?.clear()
      adPartyPlotBean.post_images = imglist

      Log.e("test", "imagelist: " + imglist.size)

      Log.e("test", "imagelist data: " + adPartyPlotBean.post_images)

      val jsonset = gson.toJson(adPartyPlotBean)
      storeUserData.setString(Constants.ADDPARTYPLOT, jsonset)

      changeScreentoNext(context)

    }else{
      Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
    }
  }

  fun checkAndSaveHotelResort(context: Context, imglist:ArrayList<Bitmap>) {
   /// bimglist.clear()
    val storeUserData = StoreUserData(context)
    val gson = Gson()
    if (imglist.size>=1) {

      val json: String = storeUserData.getString(Constants.ADDHOTELANDRESORT)
      val hotelandResortBean = gson.fromJson(json, HotelandResortBean::class.java)
      Log.e("test", "listsave data: " + hotelandResortBean.toString())

    //  bimglist = getBitmapImagelist(context,imglist)

      hotelandResortBean.post_images?.clear()
      hotelandResortBean.post_images = imglist

      Log.e("test", "imagelist: " + imglist.size)

      Log.e("test", "imagelist data: " + hotelandResortBean.post_images)

      val jsonset = gson.toJson(hotelandResortBean)
      storeUserData.setString(Constants.ADDHOTELANDRESORT, jsonset)

      changeScreentoNext(context)

    }else{
      Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
    }
  }

  fun checkAndSaveLabourWarehouse(context: Context, imglist: ArrayList<Bitmap>) {
    //bimglist.clear()
    val storeUserData = StoreUserData(context)
    val gson = Gson()
    if (imglist.size>=1) {

      val json: String = storeUserData.getString(Constants.ADDLABOURWAREHOUSE)
      val labourWareHouseBean = gson.fromJson(json, LabourWareHouseBean::class.java)
      Log.e("test", "listsave data: " + labourWareHouseBean.toString())

     // bimglist = getBitmapImagelist(context,imglist)
      labourWareHouseBean.post_images?.clear()
      labourWareHouseBean.post_images = imglist

      Log.e("test", "imagelist: " + imglist.size)

      Log.e("test", "imagelist data: " + labourWareHouseBean.post_images)

      val jsonset = gson.toJson(labourWareHouseBean)
      storeUserData.setString(Constants.ADDLABOURWAREHOUSE, jsonset)

      changeScreentoNext(context)

    }else{
      Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
    }
  }

  fun checkAndSavePgGuesthouse(context: Context, imglist:ArrayList<Bitmap>) {
   /// bimglist.clear()
    val storeUserData = StoreUserData(context)
    val gson = Gson()
    if (imglist.size>=1) {

      val json: String = storeUserData.getString(Constants.ADDPGGUSTHOUSE)
      val pgGuestHouseBean = gson.fromJson(json, PgGuestHouseBean::class.java)
      Log.e("test", "listsave data: " + pgGuestHouseBean.toString())

     /// bimglist = getBitmapImagelist(context,imglist)

      pgGuestHouseBean.post_images?.clear()
      pgGuestHouseBean.post_images = imglist

      Log.e("test", "imagelist: " + imglist.size)

      Log.e("test", "imagelist data: " + pgGuestHouseBean.post_images)

      val jsonset = gson.toJson(pgGuestHouseBean)
      storeUserData.setString(Constants.ADDPGGUSTHOUSE, jsonset)

      changeScreentoNext(context)

    }else{
      Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
    }
  }

  private fun checkAndSaveMobiles(context: Context, imglist: ArrayList<Bitmap>) {
    ///bimglist.clear()
    val storeUserData = StoreUserData(context)
    val gson = Gson()
    if (imglist.size>=1) {

      val json: String = storeUserData.getString(Constants.ADDMOBILE)
      val adPostMobilesBean = gson.fromJson(json, AdPostMobilesBean::class.java)
      Log.e("test", "listsave data: " + adPostMobilesBean.toString())

     // bimglist = getBitmapImagelist(context,imglist)

      adPostMobilesBean.post_images?.clear()
      adPostMobilesBean.post_images = imglist

      Log.e("test", "imagelist: " + imglist.size)

      Log.e("test", "imagelist data: " + adPostMobilesBean.post_images)

      val jsonset = gson.toJson(adPostMobilesBean)
      storeUserData.setString(Constants.ADDMOBILE, jsonset)

//      changeScreentoNext(context)

    }else{
      Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
    }
  }


  private fun checkAndSaveShopsInCity(context: Context, imglist:ArrayList<Bitmap>) {
   /// bimglist.clear()
    val storeUserData = StoreUserData(context)
    val gson = Gson()
    if (imglist.size>=1) {

      val json: String = storeUserData.getString(Constants.ADDGENERAL)
      val adPostGeneralBean = gson.fromJson(json, AdPostGeneralBean::class.java)
      Log.e("test", "listsave data: " + adPostGeneralBean.toString())

     // bimglist = getBitmapImagelist(context,imglist)
      adPostGeneralBean.post_images?.clear()
      adPostGeneralBean.post_images = imglist

      Log.e("test", "imagelist: " + imglist.size)

      Log.e("test", "imagelist data: " + adPostGeneralBean.post_images)

      val jsonset = gson.toJson(adPostGeneralBean)
      storeUserData.setString(Constants.ADDGENERAL, jsonset)

      changeScreentoLocation(context)

    }else{
      Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
    }
  }

  public fun changeScreentoLocation(context: Context) {
    val intent = Intent(context, SetLocationActivity::class.java)
    intent.putExtra("price","")
    context.startActivity(intent)
  }

  private fun checkAndSaveVehicles(context: Context, imglist: ArrayList<Bitmap>) {
   /// bimglist.clear()
    val storeUserData = StoreUserData(context)
    val gson = Gson()
    if (imglist.size>=1) {

      val json: String = storeUserData.getString(Constants.ADDVEHICLES)
      val adPostVehiclesBean = gson.fromJson(json, AdPostVehiclesBean::class.java)
      Log.e("test", "listsave data: " + adPostVehiclesBean.toString())

      ///bimglist = getBitmapImagelist(context,imglist)

      adPostVehiclesBean.post_images?.clear()
      adPostVehiclesBean.post_images = imglist

      Log.e("test", "imagelist: " + imglist.size)

      Log.e("test", "imagelist data: " + adPostVehiclesBean.post_images)

      val jsonset = gson.toJson(adPostVehiclesBean)
      storeUserData.setString(Constants.ADDVEHICLES, jsonset)

      changeScreentoNext(context)

    }else{
      Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
    }
  }

  private fun checkAndSaveBikes(context: Context, imglist:ArrayList<Bitmap>) {
   /// bimglist.clear()
    Log.e("test", "size data: " + imglist.size)
    val storeUserData = StoreUserData(context)
    val gson = Gson()
    if (imglist.size>=1) {

      val json: String = storeUserData.getString(Constants.ADDBIKES)
      val adPostBikesBean = gson.fromJson(json, AdPostBikesBean::class.java)
      Log.e("test", "listsave data: " + adPostBikesBean.toString())

     /// bimglist = getBitmapImagelist(context,imglist)
      adPostBikesBean.post_images?.clear()
      adPostBikesBean.post_images = imglist

      Log.e("test", "imagelist: " + imglist.size)

      Log.e("test", "imagelist data: " + adPostBikesBean.post_images)

      val jsonset = gson.toJson(adPostBikesBean)
      storeUserData.setString(Constants.ADDBIKES, jsonset)

    ///  changeScreentoNext(context)

    }else{
      Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
    }
  }

  private fun checkAndSaveGeneral(context: Context, imglist: ArrayList<Bitmap>) {

    ///bimglist.clear()
    val storeUserData = StoreUserData(context)
    val gson = Gson()
    if (imglist.size>=1) {

      val json: String = storeUserData.getString(Constants.ADDGENERAL)
      val adPostGeneralBean = gson.fromJson(json, AdPostGeneralBean::class.java)
      Log.e("test", "listsave data: " + adPostGeneralBean.toString())

     /// bimglist = getBitmapImagelist(context,imglist)

      adPostGeneralBean.post_images?.clear()
      adPostGeneralBean.post_images = imglist

      Log.e("test", "imagelist: " + imglist.size)

      Log.e("test", "imagelist data: " + adPostGeneralBean.post_images)

      val jsonset = gson.toJson(adPostGeneralBean)
      storeUserData.setString(Constants.ADDGENERAL, jsonset)

      changeScreentoNext(context)

    }else{
      Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
    }
  }


  private fun checkAndSaveGrainSeed(context: Context, imglist:ArrayList<Bitmap>) {
   /// bimglist.clear()
    val storeUserData = StoreUserData(context)
    val gson = Gson()
    if (imglist.size>=1) {

      val json: String = storeUserData.getString(Constants.ADDGRAINSEED)
      val adGrainSeedBean = gson.fromJson(json, AdGrainSeedBean::class.java)
      Log.e("test", "listsave data: " + adGrainSeedBean.toString())

      ///bimglist = getBitmapImagelist(context,imglist)
      adGrainSeedBean.post_images?.clear()

      adGrainSeedBean.post_images = imglist

      Log.e("test", "imagelist: " + imglist.size)

      Log.e("test", "imagelist data: " + adGrainSeedBean.post_images)

      val jsonset = gson.toJson(adGrainSeedBean)
      storeUserData.setString(Constants.ADDGRAINSEED, jsonset)

      changeScreentoLocation(context)

    }else{
      Utils.showToast(context as Activity, context.getString(R.string.txt_select_image))
    }
  }

}