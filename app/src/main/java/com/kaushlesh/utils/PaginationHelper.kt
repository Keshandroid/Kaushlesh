package com.kaushlesh.utils

import android.graphics.Color
import android.util.Log
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnScrollChangedListener
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.RecyclerView

/**
 * Created By Ashvin (Keshav Infotech)
 * 17 Friday, July 2020
 */
abstract class PaginationHelper(private val scrollView: NestedScrollView) {
    private var start = 0
    private var loading = false
    public var pageSize = 10
    private var auctionContainer: RecyclerView? = null
    private  val TAG = "PaginationHelper"
    var scrollViewChangeListener = OnScrollChangedListener {
        //LINEAR LAYOUT
        val view = scrollView.getChildAt(scrollView.childCount - 1) as ViewGroup?
        if (view == null || view.childCount == 0)
        {
            Log.e(TAG,"Child Not Found!!!")
            return@OnScrollChangedListener
        }
        //RELATIVE LAYOUT
        val viewGroup = view.getChildAt(view.childCount - 1) as ViewGroup?
        if (viewGroup == null || viewGroup.childCount == 0)
        {
            Log.e(TAG,"1. Child Not Found!!!")
            return@OnScrollChangedListener
        }
        //RELATIVE LAYOUT
        val viewGroup2 = viewGroup.getChildAt(viewGroup.childCount - 1) as ViewGroup?
        if(viewGroup2 == null || viewGroup2.childCount == 0)
        {
            Log.e(TAG,"2. Child Not Found!!!")
            return@OnScrollChangedListener
        }
        //RECYCLERVIEW
        val viewGroup3 = viewGroup2.getChildAt(viewGroup2.childCount - 1) as ViewGroup?
        if(viewGroup3 == null || viewGroup3.childCount == 0 || !(viewGroup3 is RecyclerView))
        {
            Log.e(TAG,"3. Child Not Found!!! ${viewGroup2.getChildAt(viewGroup2.childCount - 1)?.javaClass?.simpleName}")
            return@OnScrollChangedListener
        }
        //ITEM
        val item = viewGroup3.getChildAt(viewGroup3.childCount - 1)
        if(item==null)
        {
            Log.e(TAG,"4. Child Not Found!!!")
            return@OnScrollChangedListener
        }
        val diff = view.bottom - (scrollView.height + scrollView.scrollY)
        Log.e(
            TAG,
            "Loading : " + loading + " previousItem :: " + previousLoadedItemsCount + " start :: " + start + " Diff : " + diff + " itemHeight : " + item.height + " itemTag : ${item.tag}"
        )

        if (diff <= (item.height * 4)) {
            if (loading) return@OnScrollChangedListener
            if (previousLoadedItemsCount != start + pageSize) return@OnScrollChangedListener
            Log.e(TAG, "Pagination :: $diff")
            start += pageSize
            loading = true
            onLoadMore(start)
        }
    }

    private fun setupPagination() {
        scrollView.viewTreeObserver.addOnScrollChangedListener(scrollViewChangeListener)
    }

    fun setLoading(loading: Boolean) {
        this.loading = loading
    }

    private var previousLoadedItemsCount = 0
    fun setPreviousLoadedItemsCount(previousLoadedItemsCount: Int) {
        if (previousLoadedItemsCount % pageSize == 0) start = previousLoadedItemsCount - pageSize
        this.previousLoadedItemsCount = previousLoadedItemsCount
    }

    fun getPreviousLoadedItemsCount(): Int {
        return previousLoadedItemsCount
    }

    fun destroy() {
        auctionContainer = null
        scrollView.viewTreeObserver.removeOnScrollChangedListener(scrollViewChangeListener)
    }

    fun setStart(start: Int) {
        this.start = start
        if (start < 0) this.start = 0
    }

    abstract fun onLoadMore(start: Int)
    fun getStart(): Int {
        return start
    }

    fun setAuctionContainer(auctionContainer: RecyclerView) {
        this.auctionContainer = auctionContainer
        auctionContainer.requestLayout()
        auctionContainer.isNestedScrollingEnabled = false
    }

    init {
        loading = true
        setupPagination()
    }
}