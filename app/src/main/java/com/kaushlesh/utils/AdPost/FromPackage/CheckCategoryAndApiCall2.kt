package com.kaushlesh.utils.AdPost.FromPackage

import android.content.Context


object CheckCategoryAndApiCall2 {

    fun checkforCategory(
        context: Context, catid: Int,
        subCatId: Int, address: String, lat: String, long: String, price: String, packageid: String
    ) {

        when (catid) {
            2 -> {
                CheckSubCategoryAndApiCall2.apiCallOfProperties(
                    context,
                    catid,
                    subCatId,
                    address,
                    lat,
                    long,
                    price,
                    packageid
                )
            }

            3 -> {
                CheckSubCategoryAndApiCall2.apiCallOfMobiles(
                    context,
                    catid,
                    subCatId,
                    address,
                    lat,
                    long,
                    price,
                    packageid
                )
            }

            7 -> {
                CheckSubCategoryAndApiCall2.apiCallOfShopsInCity(
                    context,
                    catid,
                    subCatId,
                    address,
                    lat,
                    long,
                    price,
                    packageid
                )
            }

            1 -> {
                CheckSubCategoryAndApiCall2.apiCallOfVehicles(
                    context,
                    catid,
                    subCatId,
                    address,
                    lat,
                    long,
                    price,
                    packageid
                )
            }

            4 -> {
                CheckSubCategoryAndApiCall2.apiCallOfBikes(
                    context,
                    catid,
                    subCatId,
                    address,
                    lat,
                    long,
                    price,
                    packageid
                )
            }

            5 -> {
                CheckSubCategoryAndApiCall2.apiCallOfElectronics(
                    context,
                    catid,
                    subCatId,
                    address,
                    lat,
                    long,
                    price,
                    packageid
                )
            }

            6 -> {
                CheckSubCategoryAndApiCall2.apiCallOfFurniture(
                    context,
                    catid,
                    subCatId,
                    address,
                    lat,
                    long,
                    price,
                    packageid
                )
            }

            8 -> {
                CheckSubCategoryAndApiCall2.apiCallOfFashionBeauty(
                    context,
                    catid,
                    subCatId,
                    address,
                    lat,
                    long,
                    price,
                    packageid
                )
            }

            9 -> {
                CheckSubCategoryAndApiCall2.apiCallOfArtLifeStyle(
                    context,
                    catid,
                    subCatId,
                    address,
                    lat,
                    long,
                    price,
                    packageid
                )
            }

            10 -> {
                CheckSubCategoryAndApiCall2.apiCallOfServices(
                    context,
                    catid,
                    subCatId,
                    address,
                    lat,
                    long,
                    price,
                    packageid
                )
            }

            11 -> {
                CheckSubCategoryAndApiCall2.apiCallOfJobs(
                    context,
                    catid,
                    subCatId,
                    address,
                    lat,
                    long,
                    price,
                    packageid
                )
            }

            12 -> {
               /* CheckSubCategoryAndApiCall2.apiCallOfGrainSeed(
                    context,
                    catid,
                    subCatId,
                    address,
                    lat,
                    long,
                    price,
                    packageid
                )*/
                CheckSubCategoryAndApiCall2.apiCallOfKhedut(
                    context,
                    catid,
                    subCatId,
                    address,
                    lat,
                    long,
                    price,
                    packageid
                )
            }
        }

    }
}