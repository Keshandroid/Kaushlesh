package com.kaushlesh.utils.AdPost

import android.content.Context

object CheckCategoryForAdPostRedirection {
    fun openDetailScreen(
        context: Context,
        categoryname: String,
        categoryId: Int?,
        subcategoryName: String?,
        subcategoryId: Int?
    ) {
        when(categoryId)
        {
            2->
            {
                CheckSubCategoryForAdPostRedirection.openForProperties(context, categoryname, categoryId, subcategoryName, subcategoryId)
            }

            3->
            {
                CheckSubCategoryForAdPostRedirection.openForMobiles(context, categoryname, categoryId, subcategoryName, subcategoryId)

            }

            7->
            {
                CheckSubCategoryForAdPostRedirection.openForShopsInCity(context, categoryname, categoryId, subcategoryName, subcategoryId)
            }

            1->
            {
                CheckSubCategoryForAdPostRedirection.openForVehicles(context, categoryname, categoryId, subcategoryName, subcategoryId)

            }

            4->
            {
                CheckSubCategoryForAdPostRedirection.openForBikes(context, categoryname, categoryId, subcategoryName, subcategoryId)

            }
            5->
            {
                CheckSubCategoryForAdPostRedirection.openForElectronics(context, categoryname, categoryId, subcategoryName, subcategoryId)
            }
            6->
            {
                CheckSubCategoryForAdPostRedirection.openForFurniture(context, categoryname, categoryId, subcategoryName, subcategoryId)
            }
            8->
            {
                CheckSubCategoryForAdPostRedirection.openForFashionBeauty(context, categoryname, categoryId, subcategoryName, subcategoryId)
            }
            9->
            {
                CheckSubCategoryForAdPostRedirection.openForArtLifeStyle(context, categoryname, categoryId, subcategoryName, subcategoryId)

            }
            10->
            {
                CheckSubCategoryForAdPostRedirection.openForServices(context, categoryname, categoryId, subcategoryName, subcategoryId)

            }
            11->
            {
                CheckSubCategoryForAdPostRedirection.openForJobs(context, categoryname, categoryId, subcategoryName, subcategoryId)

            }
            12->
            {
               //CheckSubCategoryForAdPostRedirection.openForGrainSeeds(context,categoryname,categoryId,subcategoryName,subcategoryId)
                CheckSubCategoryForAdPostRedirection.openForKhedut(context,categoryname,categoryId,subcategoryName,subcategoryId)
            }
        }
    }
}