package com.kaushlesh.utils.AdPost

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.kaushlesh.Controller.AdPostControllers.Bikes.BicycleAdPostController
import com.kaushlesh.Controller.AdPostControllers.Bikes.MotorScooterAdPostController
import com.kaushlesh.Controller.AdPostControllers.Bikes.SparePartsAdPostController
import com.kaushlesh.Controller.AdPostControllers.General.GeneralAdPostController
import com.kaushlesh.Controller.AdPostControllers.GrainSeed.GrainSeedController
import com.kaushlesh.Controller.AdPostControllers.Khedut.GrainFruitVegController
import com.kaushlesh.Controller.AdPostControllers.Mobiles.MobileAccessoriesController
import com.kaushlesh.Controller.AdPostControllers.Mobiles.MobileTabsController
import com.kaushlesh.Controller.AdPostControllers.Property.*
import com.kaushlesh.Controller.AdPostControllers.Vehicles.CarDecorAutoPartsAdPostController
import com.kaushlesh.Controller.AdPostControllers.Vehicles.CarForSellAdPostController
import com.kaushlesh.Controller.AdPostControllers.Vehicles.CommercialVehicleForSellAdPostController
import com.kaushlesh.bean.AdPost.*
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData

object AdPostDataSavedFunction {

    fun callForSaveAndSendapvilla(
        context: Context,
        url: String,
        address: String,
        lat: String,
        long: String,
        price: String,
        apVillaSellAndRentController: ApVillaSellAndRentController,
        packageid: String
    ) {
        lateinit var storeUserData: StoreUserData
        storeUserData = StoreUserData(context)

        val advillasellrenthouse: ApVillsellandRentBean

        val gson = Gson()
        val json: String = storeUserData.getString(Constants.APVILLSELLONE)
        advillasellrenthouse = gson.fromJson(json, ApVillsellandRentBean::class.java)

        advillasellrenthouse.price= price
        advillasellrenthouse.address= address
        advillasellrenthouse.latitude= lat
        advillasellrenthouse.longitude= long
        advillasellrenthouse.package_id= packageid

        val jsonset = gson.toJson(advillasellrenthouse)
        storeUserData.setString(Constants.APVILLSELLONE, jsonset)

        Log.e("TwsAG", "listsave data: " + advillasellrenthouse.toString())
        //  adVillsellandRentBeanList.

        Log.e("price", "price data: " + advillasellrenthouse.price)

        apVillaSellAndRentController.adApVillaForSellRent(advillasellrenthouse,url)
    }

    fun callForSaveAndSendOffice(
        context: Context,
        url: String,
        address: String,
        lat: String,
        long: String,
        price: String,
        officeSellAndRentController: OfficeSellAndRentController,
        packageid: String
    ) {
        lateinit var storeUserData: StoreUserData
        storeUserData = StoreUserData(context)

        val officeForSellRentBean: AdOfficeForSellRentBean

        val gson = Gson()
        val json: String = storeUserData.getString(Constants.ADDOFFICESELLANDRENT)
        officeForSellRentBean = gson.fromJson(json, AdOfficeForSellRentBean::class.java)

        officeForSellRentBean.price= price
        officeForSellRentBean.address= address
        officeForSellRentBean.latitude= lat
        officeForSellRentBean.longitude= long
        officeForSellRentBean.package_id= packageid

        val jsonset = gson.toJson(officeForSellRentBean)
        storeUserData.setString(Constants.ADDOFFICESELLANDRENT, jsonset)

        Log.e("TwsAG", "listsave data: " + officeForSellRentBean.toString())
        //  adVillsellandRentBeanList.

        Log.e("price", "price data: " + officeForSellRentBean.price)

        officeSellAndRentController.officeForSellRent(officeForSellRentBean,url)
    }

    fun callForSaveAndSendPlotLand(
        context: Context,
        url: String,
        address: String,
        lat: String,
        long: String,
        price: String,
        plotAndLandController: PlotAndLandController,
        packageid: String
    ) {
        lateinit var storeUserData: StoreUserData
        storeUserData = StoreUserData(context)

        val adPlotAndLandBean: AdPlotAndLandBean

        val gson = Gson()
        val json: String = storeUserData.getString(Constants.ADDPLOTANDLAND)
        adPlotAndLandBean = gson.fromJson(json, AdPlotAndLandBean::class.java)

        adPlotAndLandBean.price= price
        adPlotAndLandBean.address= address
        adPlotAndLandBean.latitude= lat
        adPlotAndLandBean.longitude= long
        adPlotAndLandBean.package_id= packageid

        val jsonset = gson.toJson(adPlotAndLandBean)
        storeUserData.setString(Constants.ADDPLOTANDLAND, jsonset)

        Log.e("TwsAG", "listsave data: " + adPlotAndLandBean.toString())
        //  adVillsellandRentBeanList.

        Log.e("price", "price data: " + adPlotAndLandBean.price)

        plotAndLandController.adPlotandLand(adPlotAndLandBean,url)
    }

    fun callForSaveAndSendPartyPlot(
        context: Context,
        url: String,
        address: String,
        lat: String,
        long: String,
        price: String,
        partyPlotController: PartyPlotController,
        packageid: String
    ) {
        lateinit var storeUserData: StoreUserData
        storeUserData = StoreUserData(context)

        val partyPlotBean: AdPartyPlotBean

        val gson = Gson()
        val json: String = storeUserData.getString(Constants.ADDPARTYPLOT)
        partyPlotBean = gson.fromJson(json, AdPartyPlotBean::class.java)

        partyPlotBean.price= price
        partyPlotBean.address= address
        partyPlotBean.latitude= lat
        partyPlotBean.longitude= long
        partyPlotBean.package_id= packageid

        val jsonset = gson.toJson(partyPlotBean)
        storeUserData.setString(Constants.ADDPARTYPLOT, jsonset)

        Log.e("TwsAG", "listsave data: " + partyPlotBean.toString())
        //  adVillsellandRentBeanList.

        Log.e("price", "price data: " + partyPlotBean.price)

        partyPlotController.adPartyPlot(partyPlotBean,url)
    }

    fun callForSaveAndSendHotelResort(
        context: Context,
        url: String,
        address: String,
        lat: String,
        long: String,
        price: String,
        hotelAndResortController: HotelAndResortController,
        packageid: String
    ) {
        lateinit var storeUserData: StoreUserData
        storeUserData = StoreUserData(context)

        val hotelandResortBean: HotelandResortBean

        val gson = Gson()
        val json: String = storeUserData.getString(Constants.ADDHOTELANDRESORT)
        hotelandResortBean = gson.fromJson(json, HotelandResortBean::class.java)

        hotelandResortBean.price= price
        hotelandResortBean.address= address
        hotelandResortBean.latitude= lat
        hotelandResortBean.longitude= long
        hotelandResortBean.package_id= packageid

        val jsonset = gson.toJson(hotelandResortBean)
        storeUserData.setString(Constants.ADDHOTELANDRESORT, jsonset)

        Log.e("TwsAG", "listsave data: " + hotelandResortBean.toString())
        //  adVillsellandRentBeanList.

        Log.e("price", "price data: " + hotelandResortBean.price)

        hotelAndResortController.adHotelandResort(hotelandResortBean,url)
    }

    fun callForSaveAndSendLabourWareHouse(
        context: Context,
        url: String,
        address: String,
        lat: String,
        long: String,
        price: String,
        labourWareHouseController: LabourWareHouseController,
        packageid: String
    ) {
        lateinit var storeUserData: StoreUserData
        storeUserData = StoreUserData(context)

        val labourWareHouseBean: LabourWareHouseBean

        val gson = Gson()
        val json: String = storeUserData.getString(Constants.ADDLABOURWAREHOUSE)
        labourWareHouseBean = gson.fromJson(json, LabourWareHouseBean::class.java)

        labourWareHouseBean.price= price
        labourWareHouseBean.address= address
        labourWareHouseBean.latitude= lat
        labourWareHouseBean.longitude= long
        labourWareHouseBean.package_id= packageid

        val jsonset = gson.toJson(labourWareHouseBean)
        storeUserData.setString(Constants.ADDLABOURWAREHOUSE, jsonset)

        Log.e("TwsAG", "listsave data: " + labourWareHouseBean.toString())
        //  adVillsellandRentBeanList.

        Log.e("price", "price data: " + labourWareHouseBean.price)

        labourWareHouseController.adLabourWarehouse(labourWareHouseBean,url)
    }

    fun callForSaveAndSendPgGuestHouse(
        context: Context,
        url: String,
        address: String,
        lat: String,
        long: String,
        price: String,
        pgGuestHouseController: PgGuestHouseController,
        packageid: String
    ) {
        lateinit var storeUserData: StoreUserData
        storeUserData = StoreUserData(context)

        val pgGuestHouseBean: PgGuestHouseBean

        val gson = Gson()
        val json: String = storeUserData.getString(Constants.ADDPGGUSTHOUSE)
        pgGuestHouseBean = gson.fromJson(json, PgGuestHouseBean::class.java)

        pgGuestHouseBean.price= price
        pgGuestHouseBean.address= address
        pgGuestHouseBean.latitude= lat
        pgGuestHouseBean.longitude= long
        pgGuestHouseBean.package_id= packageid

        val jsonset = gson.toJson(pgGuestHouseBean)
        storeUserData.setString(Constants.ADDPGGUSTHOUSE, jsonset)

        Log.e("TwsAG", "listsave data: " + pgGuestHouseBean.toString())
        //  adVillsellandRentBeanList.

        Log.e("price", "price data: " + pgGuestHouseBean.price)

        pgGuestHouseController.adPgGuesthouse(pgGuestHouseBean,url)
    }

    fun callForSaveAndSendMobileTabs(
        context: Context,
        url: String,
        address: String,
        lat: String,
        long: String,
        price: String,
        mobileTabsController: MobileTabsController,
        packageid: String
    ) {

        lateinit var storeUserData: StoreUserData
        storeUserData = StoreUserData(context)

        val adPostMobilesBean: AdPostMobilesBean

        val gson = Gson()
        val json: String = storeUserData.getString(Constants.ADDMOBILE)
        adPostMobilesBean = gson.fromJson(json, AdPostMobilesBean::class.java)

        adPostMobilesBean.price= price
        adPostMobilesBean.address= address
        adPostMobilesBean.latitude= lat
        adPostMobilesBean.longitude= long
        adPostMobilesBean.package_id= packageid

        val jsonset = gson.toJson(adPostMobilesBean)
        storeUserData.setString(Constants.ADDMOBILE, jsonset)

        Log.e("TwsAG", "listsave data: " + adPostMobilesBean.toString())
        Log.e("mobilePostRequest", "ADD_MOBILE_POST_PARAM" + GsonBuilder().setPrettyPrinting().create().toJson(adPostMobilesBean))
        //  adVillsellandRentBeanList.

        Log.e("price", "price data: " + adPostMobilesBean.price)

        mobileTabsController.adMobileTabs(adPostMobilesBean,url)
    }

    fun callForSaveAndSendMobileAccessories(
        context: Context,
        url: String,
        address: String,
        lat: String,
        long: String,
        price: String,
        mobileAccessoriesController: MobileAccessoriesController,
        packageid: String
    ) {
        lateinit var storeUserData: StoreUserData
        storeUserData = StoreUserData(context)

        val adPostMobilesBean: AdPostMobilesBean

        val gson = Gson()
        val json: String = storeUserData.getString(Constants.ADDMOBILE)
        adPostMobilesBean = gson.fromJson(json, AdPostMobilesBean::class.java)

        adPostMobilesBean.price= price
        adPostMobilesBean.address= address
        adPostMobilesBean.latitude= lat
        adPostMobilesBean.longitude= long
        adPostMobilesBean.package_id= packageid

        val jsonset = gson.toJson(adPostMobilesBean)
        storeUserData.setString(Constants.ADDMOBILE, jsonset)

        Log.e("TwsAG", "listsave data: " + adPostMobilesBean.toString())
        //  adVillsellandRentBeanList.

        Log.e("price", "price data: " + adPostMobilesBean.price)

        mobileAccessoriesController.adMobileAccessories(adPostMobilesBean,url)
    }

    fun callForSaveComman(
        context: Context,
        url: String,
        address: String,
        lat: String,
        long: String,
        price: String,
        generalAdPostController: GeneralAdPostController,
        packageid: String
    ) {
        lateinit var storeUserData: StoreUserData
        storeUserData = StoreUserData(context)

        val adPostGeneralBean: AdPostGeneralBean

        val gson = Gson()
        val json: String = storeUserData.getString(Constants.ADDGENERAL)
        adPostGeneralBean = gson.fromJson(json, AdPostGeneralBean::class.java)

        adPostGeneralBean.price= price
        adPostGeneralBean.address= address
        adPostGeneralBean.latitude= lat
        adPostGeneralBean.longitude= long
        adPostGeneralBean.package_id= packageid

        val jsonset = gson.toJson(adPostGeneralBean)
        storeUserData.setString(Constants.ADDGENERAL, jsonset)

        Log.e("TwsAG", "listsave data: " + adPostGeneralBean.toString())
        //  adVillsellandRentBeanList.

        Log.e("price", "price data: " + adPostGeneralBean.price)

        generalAdPostController.adgeneralPost(adPostGeneralBean,url)
    }

    fun callForSaveAndSendCarForSell(
        context: Context,
        url: String,
        address: String,
        lat: String,
        long: String,
        price: String,
        carForSellAdPostController: CarForSellAdPostController,
        packageid: String
    ) {
        lateinit var storeUserData: StoreUserData
        storeUserData = StoreUserData(context)

        val adPostVehiclesBean: AdPostVehiclesBean

        val gson = Gson()
        val json: String = storeUserData.getString(Constants.ADDVEHICLES)
        adPostVehiclesBean = gson.fromJson(json, AdPostVehiclesBean::class.java)

        adPostVehiclesBean.price= price
        adPostVehiclesBean.address= address
        adPostVehiclesBean.latitude= lat
        adPostVehiclesBean.longitude= long
        adPostVehiclesBean.package_id= packageid

        val jsonset = gson.toJson(adPostVehiclesBean)
        storeUserData.setString(Constants.ADDVEHICLES, jsonset)

        Log.e("TwsAG", "listsave data: " + adPostVehiclesBean.toString())
        //  adVillsellandRentBeanList.

        Log.e("price", "price data: " + adPostVehiclesBean.price)

        carForSellAdPostController.adCarForSell(adPostVehiclesBean,url)
    }

    fun callForSaveAndSendCommercialVehicleForSell(
        context: Context,
        url: String,
        address: String,
        lat: String,
        long: String,
        price: String,
        commercialVehicleForSellAdPostController: CommercialVehicleForSellAdPostController,
        packageid: String
    ) {

        lateinit var storeUserData: StoreUserData
        storeUserData = StoreUserData(context)

        val adPostVehiclesBean: AdPostVehiclesBean

        val gson = Gson()
        val json: String = storeUserData.getString(Constants.ADDVEHICLES)
        adPostVehiclesBean = gson.fromJson(json, AdPostVehiclesBean::class.java)

        adPostVehiclesBean.price= price
        adPostVehiclesBean.address= address
        adPostVehiclesBean.latitude= lat
        adPostVehiclesBean.longitude= long
        adPostVehiclesBean.package_id= packageid

        val jsonset = gson.toJson(adPostVehiclesBean)
        storeUserData.setString(Constants.ADDVEHICLES, jsonset)

        Log.e("TwsAG", "listsave data: " + adPostVehiclesBean.toString())
        //  adVillsellandRentBeanList.

        Log.e("price", "price data: " + adPostVehiclesBean.price)

        commercialVehicleForSellAdPostController.adCommercialVehicleForSell(adPostVehiclesBean,url)
    }

    fun callForSaveAndSendCarDecorAutoParts(
        context: Context,
        url: String,
        address: String,
        lat: String,
        long: String,
        price: String,
        carDecorAutoPartsAdPostController: CarDecorAutoPartsAdPostController,
        packageid: String
    ) {

        lateinit var storeUserData: StoreUserData
        storeUserData = StoreUserData(context)

        val adPostVehiclesBean: AdPostVehiclesBean

        val gson = Gson()
        val json: String = storeUserData.getString(Constants.ADDVEHICLES)
        adPostVehiclesBean = gson.fromJson(json, AdPostVehiclesBean::class.java)

        adPostVehiclesBean.price= price
        adPostVehiclesBean.address= address
        adPostVehiclesBean.latitude= lat
        adPostVehiclesBean.longitude= long
        adPostVehiclesBean.package_id= packageid

        val jsonset = gson.toJson(adPostVehiclesBean)
        storeUserData.setString(Constants.ADDVEHICLES, jsonset)

        Log.e("TwsAG", "listsave data: " + adPostVehiclesBean.toString())
        //  adVillsellandRentBeanList.

        Log.e("price", "price data: " + adPostVehiclesBean.price)

        carDecorAutoPartsAdPostController.adCarDecorAutoParts(adPostVehiclesBean,url)
    }

    fun callForSaveAndSendMotorScooter(
        context: Context,
        url: String,
        address: String,
        lat: String,
        long: String,
        price: String,
        motorScooterAdPostController: MotorScooterAdPostController,
        packageid: String
    ) {

        lateinit var storeUserData: StoreUserData
        storeUserData = StoreUserData(context)

        val adPostBikesBean: AdPostBikesBean

        val gson = Gson()
        val json: String = storeUserData.getString(Constants.ADDBIKES)
        adPostBikesBean = gson.fromJson(json, AdPostBikesBean::class.java)

        adPostBikesBean.price= price
        adPostBikesBean.address= address
        adPostBikesBean.latitude= lat
        adPostBikesBean.longitude= long
        adPostBikesBean.package_id= packageid

        val jsonset = gson.toJson(adPostBikesBean)
        storeUserData.setString(Constants.ADDBIKES, jsonset)

        Log.e("TwsAG", "listsave data: " + adPostBikesBean.toString())
        //  adVillsellandRentBeanList.

        Log.e("price", "price data: " + adPostBikesBean.price)

        motorScooterAdPostController.adMotorScooter(adPostBikesBean,url)
    }

    fun callForSaveAndSendSpareParts(
        context: Context,
        url: String,
        address: String,
        lat: String,
        long : String,
        price: String,
        sparePartsAdPostController: SparePartsAdPostController,
        packageid: String
    ) {
        lateinit var storeUserData: StoreUserData
        storeUserData = StoreUserData(context)

        val adPostBikesBean: AdPostBikesBean

        val gson = Gson()
        val json: String = storeUserData.getString(Constants.ADDBIKES)
        adPostBikesBean = gson.fromJson(json, AdPostBikesBean::class.java)

        adPostBikesBean.price= price
        adPostBikesBean.address= address
        adPostBikesBean.latitude= lat
        adPostBikesBean.longitude= long
        adPostBikesBean.package_id= packageid

        val jsonset = gson.toJson(adPostBikesBean)
        storeUserData.setString(Constants.ADDBIKES, jsonset)

        Log.e("TwsAG", "listsave data: " + adPostBikesBean.toString())
        //  adVillsellandRentBeanList.

        Log.e("price", "price data: " + adPostBikesBean.price)

        sparePartsAdPostController.adSpareParts(adPostBikesBean,url)
    }

    fun callForSaveAndSendBicycle(
        context: Context,
        url: String,
        address: String,
        lat: String,
        long: String,
        price: String,
        bicycleAdPostController: BicycleAdPostController,
        packageid: String
    ) {
        lateinit var storeUserData: StoreUserData
        storeUserData = StoreUserData(context)

        val adPostBikesBean: AdPostBikesBean

        val gson = Gson()
        val json: String = storeUserData.getString(Constants.ADDBIKES)
        adPostBikesBean = gson.fromJson(json, AdPostBikesBean::class.java)

        adPostBikesBean.price= price
        adPostBikesBean.address= address
        adPostBikesBean.latitude= lat
        adPostBikesBean.longitude= long
        adPostBikesBean.package_id= packageid

        val jsonset = gson.toJson(adPostBikesBean)
        storeUserData.setString(Constants.ADDBIKES, jsonset)

        Log.e("TwsAG", "listsave data: " + adPostBikesBean.toString())
        //  adVillsellandRentBeanList.

        Log.e("price", "price data: " + adPostBikesBean.price)

        bicycleAdPostController.adBicycle(adPostBikesBean,url)
    }

    fun callForSaveAndSendGrainSeed(
        context: Context,
        url: String,
        address: String,
        lat: String,
        long: String,
        price: String,
        grainSeedController: GrainSeedController,
        packageid: String
    ) {

        lateinit var storeUserData: StoreUserData
        storeUserData = StoreUserData(context)

        val adGrainSeedBean: AdGrainSeedBean

        val gson = Gson()
        val json: String = storeUserData.getString(Constants.ADDGRAINSEED)
        adGrainSeedBean = gson.fromJson(json, AdGrainSeedBean::class.java)

        //adGrainSeedBean.price= price
        adGrainSeedBean.address= address
        adGrainSeedBean.latitude= lat
        adGrainSeedBean.longitude= long
        adGrainSeedBean.package_id= packageid

        val jsonset = gson.toJson(adGrainSeedBean)
        storeUserData.setString(Constants.ADDGRAINSEED, jsonset)

        Log.e("TwsAG", "listsave data: " + adGrainSeedBean.toString())
        //  adVillsellandRentBeanList.

        Log.e("price", "price data: " + adGrainSeedBean.price)

        grainSeedController.adGrainSeed(adGrainSeedBean,url)
    }

    fun callForSaveAndSendKhedut(
        context: Context,
        url: String,
        address: String,
        lat: String,
        long: String,
        price: String,
        grainFruitVegController: GrainFruitVegController,
        packageid: String
    ) {

        lateinit var storeUserData: StoreUserData
        storeUserData = StoreUserData(context)

        val adKhedutBean: AdKhedutBean

        val gson = Gson()
        val json: String = storeUserData.getString(Constants.ADDKHEDUT)
        adKhedutBean = gson.fromJson(json, AdKhedutBean::class.java)

        //adGrainSeedBean.price= price
        adKhedutBean.address= address
        adKhedutBean.latitude= lat
        adKhedutBean.longitude= long
        adKhedutBean.package_id= packageid

        val jsonset = gson.toJson(adKhedutBean)
        storeUserData.setString(Constants.ADDKHEDUT, jsonset)

        Log.e("TwsAG", "listsave data: " + adKhedutBean.toString())
        //  adVillsellandRentBeanList.

        Log.e("price", "price data: " + adKhedutBean.price)

        grainFruitVegController.adGrainFruitVeg(adKhedutBean,url)
    }
}