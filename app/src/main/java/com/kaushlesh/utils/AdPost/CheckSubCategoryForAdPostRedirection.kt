package com.kaushlesh.utils.AdPost

import android.content.Context
import android.content.Intent
import com.kaushlesh.view.activity.AdPost.AddDetailsActivity
import com.kaushlesh.view.activity.AdPost.Bikes.AddBicycleActivity
import com.kaushlesh.view.activity.AdPost.Bikes.AddSparePartsActivity
import com.kaushlesh.view.activity.AdPost.Bikes.AddVehicleDetailActivity
import com.kaushlesh.view.activity.AdPost.Khedut.*
import com.kaushlesh.view.activity.AdPost.Mobiles.AccesoriesDetailActivity
import com.kaushlesh.view.activity.AdPost.Mobiles.AddBrandDetailMobileActivity
import com.kaushlesh.view.activity.AdPost.Mobiles.AddBrandDetailTabsActivity
import com.kaushlesh.view.activity.AdPost.Property.*
import com.kaushlesh.view.activity.AdPost.Vehicles.AddBrandDetailActivity
import com.kaushlesh.view.activity.AdPost.Vehicles.AddCommercialVehicleDetailActivity
import com.kaushlesh.view.activity.AdPost.Vehicles.CarSellDetailActivity
import com.kaushlesh.view.activity.AdPost.seed.AddSeedDetailsActivity

object CheckSubCategoryForAdPostRedirection {
    fun openForProperties(
        context: Context, categoryname: String, categoryId: Int, subcategoryName: String?, subcategoryId: Int?) {
        when(subcategoryId)
        {
            1->
            {
                val intent = Intent(context, ApVillaSellAndRentActivity::class.java)
                intent.putExtra("for", "sell")
                context.startActivity(intent)
            }
            2->
            {
                val intent = Intent(context, ApVillaSellAndRentActivity::class.java)
                intent.putExtra("for", "rent")
                context.startActivity(intent)
            }
            3->
            {
                val intent = Intent(context, OfficeDetailActivity::class.java)
                context.startActivity(intent)
            }
            4->
            {
                val intent = Intent(context, OfficeDetailActivity::class.java)
                context.startActivity(intent)
            }
            5->
            {
                val intent = Intent(context, PlotLandDetailActivity::class.java)
                context.startActivity(intent)
            }
            6->
            {
                val intent = Intent(context, PartyPlotActivity::class.java)
                context.startActivity(intent)
            }
            7->
            {
                val intent = Intent(context, HotelDetailActivity::class.java)
                context.startActivity(intent)
            }
            8->
            {
                val intent = Intent(context, LabourDetailActivity::class.java)
                context.startActivity(intent)
            }
            9->
            {
                val intent = Intent(context, PgDetailActivity::class.java)
                context.startActivity(intent)
            }
        }
    }

    fun openForMobiles(
        context: Context,
        categoryname: String,
        categoryId: Int,
        subcategoryName: String?,
        subcategoryId: Int?
    ) {
        when(subcategoryId) {
            11 -> {
                val intent = Intent(context, AddBrandDetailMobileActivity::class.java)
                context.startActivity(intent)
            }
            12 -> {
                val intent = Intent(context, AddBrandDetailTabsActivity::class.java)
                context.startActivity(intent)
            }
            13 -> {
                val intent = Intent(context, AccesoriesDetailActivity::class.java)
                context.startActivity(intent)
            }
        }
    }

    fun openForShopsInCity(
        context: Context,
        categoryname: String,
        categoryId: Int,
        subcategoryName: String?,
        subcategoryId: Int?
    ) {
        val intent = Intent(context, AddDetailsActivity::class.java)
        context.startActivity(intent)
    }

    fun openForVehicles(
        context: Context,
        categoryname: String,
        categoryId: Int,
        subcategoryName: String?,
        subcategoryId: Int?
    ) {
        when(subcategoryId) {
            18 -> {
                val intent = Intent(context, CarSellDetailActivity::class.java)
                context.startActivity(intent)
            }
            19 -> {
                val intent = Intent(context, AddCommercialVehicleDetailActivity::class.java)
                context.startActivity(intent)
            }
            20 -> {
                val intent = Intent(context, AddBrandDetailActivity::class.java)
                context.startActivity(intent)
            }
        }
    }

    fun openForBikes(
        context: Context,
        categoryname: String,
        categoryId: Int,
        subcategoryName: String?,
        subcategoryId: Int?
    ) {
        when(subcategoryId) {
            14 -> {
                val intent = Intent(context, AddVehicleDetailActivity::class.java)
                context.startActivity(intent)
            }
            15 -> {
                val intent = Intent(context, AddVehicleDetailActivity::class.java)
                context.startActivity(intent)
            }
            16 -> {
                val intent = Intent(context, AddSparePartsActivity::class.java)
                context.startActivity(intent)
            }
            17 -> {
                val intent = Intent(context, AddBicycleActivity::class.java)
                context.startActivity(intent)
            }
        }
    }

    fun openForElectronics(
        context: Context,
        categoryname: String,
        categoryId: Int,
        subcategoryName: String?,
        subcategoryId: Int?
    ) {
        val intent = Intent(context, AddDetailsActivity::class.java)
        context.startActivity(intent)
    }

    fun openForFashionBeauty(
        context: Context,
        categoryname: String,
        categoryId: Int,
        subcategoryName: String?,
        subcategoryId: Int?
    ) {
        val intent = Intent(context, AddDetailsActivity::class.java)
        context.startActivity(intent)
    }

    fun openForFurniture(
        context: Context,
        categoryname: String,
        categoryId: Int,
        subcategoryName: String?,
        subcategoryId: Int?
    ) {
        val intent = Intent(context, AddDetailsActivity::class.java)
        context.startActivity(intent)
    }

    fun openForArtLifeStyle(
        context: Context,
        categoryname: String,
        categoryId: Int,
        subcategoryName: String?,
        subcategoryId: Int?
    ) {

        val intent = Intent(context, AddDetailsActivity::class.java)
        context.startActivity(intent)
    }

    fun openForServices(
        context: Context,
        categoryname: String,
        categoryId: Int,
        subcategoryName: String?,
        subcategoryId: Int?
    ) {
        val intent = Intent(context, AddDetailsActivity::class.java)
        context.startActivity(intent)
    }

    fun openForJobs(
        context: Context,
        categoryname: String,
        categoryId: Int,
        subcategoryName: String?,
        subcategoryId: Int?
    ) {
        val intent = Intent(context, AddDetailsActivity::class.java)
        context.startActivity(intent)
    }

    fun openForGrainSeeds(
        context: Context,
        categoryname: String,
        categoryId: Int,
        subcategoryName: String?,
        subcategoryId: Int?
    ) {
        val intent = Intent(context, AddSeedDetailsActivity::class.java)
        context.startActivity(intent)
    }

    fun openForKhedut(
        context: Context,
        categoryname: String,
        categoryId: Int,
        subcategoryName: String?,
        subcategoryId: Int?
    ) {
        when(subcategoryId) {
            110 -> {
                val intent = Intent(context, GrainFruitVegActivity::class.java)
                context.startActivity(intent)
            }
            111 -> {
                val intent = Intent(context, GrainFruitVegActivity::class.java)
                context.startActivity(intent)
            }
            112 -> {
                val intent = Intent(context, GrainFruitVegActivity::class.java)
                context.startActivity(intent)
            }
            113 -> {
                val intent = Intent(context, AddAnimalActivity::class.java)
                context.startActivity(intent)
            }
            114 -> {
                val intent = Intent(context, KedutOtherCategoryActivity::class.java)
                context.startActivity(intent)
            }
            115 -> {
                val intent = Intent(context, FertilizerSeedsActivity::class.java)
                context.startActivity(intent)
            }
            116 -> {
                val intent = Intent(context, KedutOtherCategoryActivity::class.java)
                context.startActivity(intent)
            }
            117 -> {
                val intent = Intent(context, TractorTrolleyActivity::class.java)
                context.startActivity(intent)
            }
            118 -> {
                val intent = Intent(context, KedutOtherCategoryActivity::class.java)
                context.startActivity(intent)
            }
            119 -> {
                val intent = Intent(context, FarmForRentActivity::class.java)
                context.startActivity(intent)
            }
        }
    }

}