package com.kaushlesh.utils.AdPost.FromLocation

import android.content.Context

object CheckCategoryAndApiCall {

    fun checkforCategory(
        context: Context, catid: Int,
        subCatId:Int, address: String, lat: String, long: String, price: String, packageid: String
    ) {

        when(catid)
        {
            2->
            {
                CheckSubCategoryAndApiCall.apiCallOfProperties(
                    context,
                    catid,
                    subCatId,
                    address,
                    lat,
                    long,
                    price,
                    packageid
                )
            }

            3->
            {
                CheckSubCategoryAndApiCall.apiCallOfMobiles(
                    context,
                    catid,
                    subCatId,
                    address,
                    lat,
                    long,
                    price,
                    packageid
                )
            }

            7->
            {
                CheckSubCategoryAndApiCall.apiCallOfShopsInCity(
                    context,
                    catid,
                    subCatId,
                    address,
                    lat,
                    long,
                    price,
                    packageid
                )
            }

            1->
            {
                CheckSubCategoryAndApiCall.apiCallOfVehicles(
                    context,
                    catid,
                    subCatId,
                    address,
                    lat,
                    long,
                    price,
                    packageid
                )
            }

            4->
            {
                CheckSubCategoryAndApiCall.apiCallOfBikes(
                    context,
                    catid,
                    subCatId,
                    address,
                    lat,
                    long,
                    price,
                    packageid
                )
            }

            5->
            {
                CheckSubCategoryAndApiCall.apiCallOfElectronics(
                    context,
                    catid,
                    subCatId,
                    address,
                    lat,
                    long,
                    price,
                    packageid
                )
            }

            6->
            {
                CheckSubCategoryAndApiCall.apiCallOfFurniture(
                    context,
                    catid,
                    subCatId,
                    address,
                    lat,
                    long,
                    price,
                    packageid
                )
            }

            8->
            {
                CheckSubCategoryAndApiCall.apiCallOfFashionBeauty(
                    context,
                    catid,
                    subCatId,
                    address,
                    lat,
                    long,
                    price,
                    packageid
                )
            }

            9->
            {
                CheckSubCategoryAndApiCall.apiCallOfArtLifeStyle(
                    context,
                    catid,
                    subCatId,
                    address,
                    lat,
                    long,
                    price,
                    packageid
                )
            }

            10->
            {
                CheckSubCategoryAndApiCall.apiCallOfServices(
                    context,
                    catid,
                    subCatId,
                    address,
                    lat,
                    long,
                    price,
                    packageid
                )
            }

            11->
            {
                CheckSubCategoryAndApiCall.apiCallOfJobs(
                    context,
                    catid,
                    subCatId,
                    address,
                    lat,
                    long,
                    price,
                    packageid
                )
            }

            12->
            {
                /*CheckSubCategoryAndApiCall.apiCallOfGrainSeed(
                    context,
                    catid,
                    subCatId,
                    address,
                    lat,
                    long,
                    price,
                    packageid
                )*/

                CheckSubCategoryAndApiCall.apiCallOfKhedut(
                    context,
                    catid,
                    subCatId,
                    address,
                    lat,
                    long,
                    price,
                    packageid
                )
            }
        }

    }
}