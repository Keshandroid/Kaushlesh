package com.kaushlesh.utils.AdPost.FromLocation

import android.content.Context
import com.kaushlesh.Controller.AdPostControllers.Bikes.BicycleAdPostController
import com.kaushlesh.Controller.AdPostControllers.Bikes.MotorScooterAdPostController
import com.kaushlesh.Controller.AdPostControllers.Bikes.SparePartsAdPostController
import com.kaushlesh.Controller.AdPostControllers.General.GeneralAdPostController
import com.kaushlesh.Controller.AdPostControllers.GrainSeed.GrainSeedController
import com.kaushlesh.Controller.AdPostControllers.Khedut.GrainFruitVegController
import com.kaushlesh.Controller.AdPostControllers.Mobiles.MobileAccessoriesController
import com.kaushlesh.Controller.AdPostControllers.Mobiles.MobileTabsController
import com.kaushlesh.Controller.AdPostControllers.Property.*
import com.kaushlesh.Controller.AdPostControllers.Vehicles.CarDecorAutoPartsAdPostController
import com.kaushlesh.Controller.AdPostControllers.Vehicles.CarForSellAdPostController
import com.kaushlesh.Controller.AdPostControllers.Vehicles.CommercialVehicleForSellAdPostController
import com.kaushlesh.api.API
import com.kaushlesh.utils.AdPost.AdPostDataSavedFunction
import com.kaushlesh.view.activity.AdPost.SetLocationActivity
import com.kaushlesh.view.fragment.MyAccount.PurchasePackage.AdPostPackagesActivity

object CheckSubCategoryAndApiCall{
    fun apiCallOfProperties(context: Context, catid: Int, subCatId: Int, address: String, lat: String, long: String, price: String, packageid: String) {
        var url: String =""
        when(subCatId)
        {
            1->
            {
                url = API.ADD_POST_HOUSE_FOR_SELL
                AdPostDataSavedFunction.callForSaveAndSendapvilla(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    apVillaSellAndRentController = ApVillaSellAndRentController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            2 ->
            {
                url = API.ADD_POST_HOUSE_FOR_RENT
                AdPostDataSavedFunction.callForSaveAndSendapvilla(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    apVillaSellAndRentController = ApVillaSellAndRentController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            3->
            {
                url = API.ADD_POST_OFFICE_FOR_SELL
                AdPostDataSavedFunction.callForSaveAndSendOffice(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    officeSellAndRentController = OfficeSellAndRentController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            4->
            {
                url = API.ADD_POST_OFFICE_FOR_RENT
                AdPostDataSavedFunction.callForSaveAndSendOffice(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    officeSellAndRentController = OfficeSellAndRentController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            5->
            {
                url = API.ADD_POST_PLOTS_LAND
                AdPostDataSavedFunction.callForSaveAndSendPlotLand(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    plotAndLandController = PlotAndLandController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            6->
            {
                url = API.ADD_POST_PARTY_PLOTS
                AdPostDataSavedFunction.callForSaveAndSendPartyPlot(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    partyPlotController = PartyPlotController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            7->
            {
                url = API.ADD_HOTEL_AND_RESORT
                AdPostDataSavedFunction.callForSaveAndSendHotelResort(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    hotelAndResortController = HotelAndResortController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            8->
            {
                url = API.ADD_WARE_HOUSE
                AdPostDataSavedFunction.callForSaveAndSendLabourWareHouse(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    labourWareHouseController = LabourWareHouseController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            9->
            {
                url = API.ADD_GUEST_HOUSE
                AdPostDataSavedFunction.callForSaveAndSendPgGuestHouse(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    pgGuestHouseController = PgGuestHouseController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
        }
    }

    fun apiCallOfMobiles(
        context: Context,
        catid: Int,
        subCatId: Int,
        address: String,
        lat: String,
        long: String,
        price: String,
        packageid: String
    ) {
        var url: String =""
        when(subCatId)
        {
            11->
            {
                url = API.ADD_POST_MOBILE
                AdPostDataSavedFunction.callForSaveAndSendMobileTabs(context, url, address, lat, long, price, mobileTabsController = MobileTabsController(context as SetLocationActivity, context),packageid = packageid)
            }
            12 ->
            {
                url = API.ADD_POST_TABLET
                AdPostDataSavedFunction.callForSaveAndSendMobileTabs(context, url, address, lat, long, price, mobileTabsController = MobileTabsController(context as SetLocationActivity, context),packageid = packageid)
            }
            13->
            {
                url = API.ADD_POST_ACCESSORIES
                AdPostDataSavedFunction.callForSaveAndSendMobileAccessories(context, url, address, lat, long, price, mobileAccessoriesController = MobileAccessoriesController(context as SetLocationActivity, context),packageid = packageid)
            }
        }
    }

    fun apiCallOfShopsInCity(
        context: Context,
        catid: Int,
        subCatId: Int,
        address: String,
        lat: String,
        long: String,
        price: String,
        packageid: String
    ) {
        var url: String =""
        when(subCatId)
        {
            21->
            {
                url = API.ADD_POST_GROCERY_SUPER_MARKET
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            22 ->
            {
                url = API.ADD_POST_CROCARY_PLASTIC
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            23->
            {
                url = API.ADD_POST_SUNGLASS_WATCHSHOP
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            24->
            {
                url = API.ADD_POST_PHARMACY_SHOP
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            25->
            {
                url = API.ADD_POST_JWELLERY_SHOP
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            26->
            {
                url = API.ADD_POST_HARDWARE_SANITARY
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            27->
            {
                url = API.ADD_POST_RESTORA_FASTFOOD
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            28->
            {
                url = API.ADD_POST_BAKERY_SWEETSHOP
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            29->
            {
                url = API.ADD_POST_GIFTS_CARD_SHOP
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            30->
            {
                url = API.ADD_POST_ELECTRICAL_SHOP
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            31->
            {
                url = API.ADD_POST_STATIONARY_SHOP
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            32->
            {
                url = API.ADD_POST_OTHER_SHOPS
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
        }
    }

    private fun callCommonData(
        context: Context,
        url: String,
        address: String,
        lat: String,
        long: String,
        price: String,
        generalAdPostController: GeneralAdPostController,
        packageid: String
    ) {
        AdPostDataSavedFunction.callForSaveComman(
            context,
            url,
            address,
            lat,
            long,
            price,
            generalAdPostController,
            packageid
        )
    }

    fun apiCallOfVehicles(
        context: Context,
        catid: Int,
        subCatId: Int,
        address: String,
        lat: String,
        long: String,
        price: String,
        packageid: String
    ) {
        var url: String =""
        when(subCatId)
        {
            18->
            {
                url = API.ADD_POST_CAR_FOR_SELL
                AdPostDataSavedFunction.callForSaveAndSendCarForSell(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    carForSellAdPostController = CarForSellAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            19 ->
            {
                url = API.ADD_POST_COMMERCIAL_VEHICLE_FOR_SELL
                AdPostDataSavedFunction.callForSaveAndSendCommercialVehicleForSell(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    commercialVehicleForSellAdPostController = CommercialVehicleForSellAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            20->
            {
                url = API.ADD_POST_CAR_DECOR_AUTO_PARTS
                AdPostDataSavedFunction.callForSaveAndSendCarDecorAutoParts(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    carDecorAutoPartsAdPostController = CarDecorAutoPartsAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
        }
    }

    fun apiCallOfBikes(
        context: Context,
        catid: Int,
        subCatId: Int,
        address: String,
        lat: String,
        long: String,
        price: String,
        packageid: String
    ) {
        var url: String =""
        when(subCatId)
        {
            14->
            {
                url = API.ADD_POST_MOTORCYCLE
                AdPostDataSavedFunction.callForSaveAndSendMotorScooter(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    motorScooterAdPostController = MotorScooterAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            15 ->
            {
                url = API.ADD_POST_SCOOTER
                AdPostDataSavedFunction.callForSaveAndSendMotorScooter(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    motorScooterAdPostController = MotorScooterAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            16->
            {
                url = API.ADD_POST_SPARE_PARTS
                AdPostDataSavedFunction.callForSaveAndSendSpareParts(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    sparePartsAdPostController = SparePartsAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            17->
            {
                url = API.ADD_POST_BICYCLE
                AdPostDataSavedFunction.callForSaveAndSendBicycle(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    bicycleAdPostController = BicycleAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
        }
    }

    fun apiCallOfElectronics(
        context: Context,
        catid: Int,
        subCatId: Int,
        address: String,
        lat: String,
        long: String,
        price: String,
        packageid: String
    ) {

        var url: String =""
        when(subCatId)
        {
            33->
            {
                url = API.ADD_POST_TVS
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            34 ->
            {
                url = API.ADD_POST_VIDEO_AUDIO
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            35->
            {
                url = API.ADD_POST_COMPUTER_LAPTOP
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            36->
            {
                url = API.ADD_POST_GAMES_ENTERN
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            37->
            {
                url = API.ADD_POST_FRIDGE
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            38->
            {
                url = API.ADD_POST_WASHING_MACHINE
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            39->
            {
                url = API.ADD_POST_ACS
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            40->
            {
                url = API.ADD_POST_CAMERA_LANCES
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            41->
            {
                url = API.ADD_POST_HARDDISK_PRINTER
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            42->
            {
                url = API.ADD_POST_KITCHEN
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
        }
    }

    fun apiCallOfFurniture(
        context: Context,
        catid: Int,
        subCatId: Int,
        address: String,
        lat: String,
        long: String,
        price: String,
        packageid: String
    ) {

        var url: String =""
        when(subCatId)
        {
            43->
            {
                url = API.ADD_POST_SOFA_DINING
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            44 ->
            {
                url = API.ADD_POST_BED_WARDROOMS
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            45->
            {
                url = API.ADD_POST_KIDS_FURNITURE
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            46->
            {
                url = API.ADD_POST_OTHER_ITEMS
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
        }
    }

    fun apiCallOfFashionBeauty(
        context: Context,
        catid: Int,
        subCatId: Int,
        address: String,
        lat: String,
        long: String,
        price: String,
        packageid: String
    ) {

        var url: String =""
        when(subCatId)
        {
            47->
            {
                url = API.ADD_POST_MEN
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            48 ->
            {
                url = API.ADD_POST_WOMEN
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            49->
            {
                url = API.ADD_POST_KIDS
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
        }
    }

    fun apiCallOfArtLifeStyle(
        context: Context,
        catid: Int,
        subCatId: Int,
        address: String,
        lat: String,
        long: String,
        price: String,
        packageid: String
    ) {

        var url: String =""
        when(subCatId)
        {
            50->
            {
                url = API.ADD_POST_PAINTING
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            51 ->
            {
                url = API.ADD_POST_DESIGNER_CLOCK
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            52->
            {
                url = API.ADD_POST_POTS_VASES
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            53->
            {
                url = API.ADD_POST_DESIGNER_MIRROR
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            54->
            {
                url = API.ADD_POST_GARDEN_DECOR
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            55->
            {
                url = API.ADD_POST_DECORATIVE_LIGHTINGS
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            56->
            {
                url = API.ADD_POST_OTHER_DECORATIVE_PRODUCTS
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
        }
    }

    fun apiCallOfServices(
        context: Context,
        catid: Int,
        subCatId: Int,
        address: String,
        lat: String,
        long: String,
        price: String,
        packageid: String
    ) {

        var url: String =""
        when(subCatId)
        {
            57->
            {
                url = API.ADD_POST_ELECTRONICS_HOME_APPL
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            58 ->
            {
                url = API.ADD_POST_RENT_CAR_SERVICE
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            59->
            {
                url = API.ADD_POST_RENT_COMMERCIAL_VEHICLE
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            60->
            {
                url = API.ADD_POST_HEALTH_BEAUTY
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            61->
            {
                url = API.ADD_POST_MOVERS_PACKERS
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            62->
            {
                url = API.ADD_POST_TRAVEL_AGENCY
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            63->
            {
                url = API.ADD_POST_OTHER_HOMEDELIVERY_PRODUCTS
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            64->
            {
                url = API.ADD_POST_EVENT_MANAGEMENT
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            65 ->
            {
                url = API.ADD_POST_CAR_POOL_SERVICE
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            66->
            {
                url = API.ADD_POST_FINANCE_CA
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            67->
            {
                url = API.ADD_POST_LEGAL_SERVICE
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            68->
            {
                url = API.ADD_POST_INTERIOR_CIVIL
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            69->
            {
                url = API.ADD_POST_FOOD_COOKING
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            70->
            {
                url = API.ADD_POST_TUTIONS_CLASSES
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            71->
            {
                url = API.ADD_POST_OTHER_SERVICES
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
        }
    }

    fun apiCallOfJobs(
        context: Context,
        catid: Int,
        subCatId: Int,
        address: String,
        lat: String,
        long: String,
        price: String,
        packageid: String
    ) {

        var url: String =""
        when(subCatId)
        {
            72->
            {
                url = API.ADD_POST_DATA_ENTRY
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            73 ->
            {
                url = API.ADD_POST_SALES_MARKETING
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            74->
            {
                url = API.ADD_POST_RENT_OIL_GAS
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            75->
            {
                url = API.ADD_POST_OPERATOR
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            76->
            {
                url = API.ADD_POST_TECHNICIAN
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            77->
            {
                url = API.ADD_POST_TEACHER
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            78->
            {
                url = API.ADD_POST_CA
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            79->
            {
                url = API.ADD_POST_ENGINEER
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            80 ->
            {
                url = API.ADD_POST_INTERIOR_DESIGNOR
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            81->
            {
                url = API.ADD_POST_RECEPTIONIST
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            82->
            {
                url = API.ADD_POST_HOTEL_MANAGEMENT
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            83->
            {
                url = API.ADD_POST_COURIER_SERVICE
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            84->
            {
                url = API.ADD_POST_DELIVERY_COLLECTION
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            85->
            {
                url = API.ADD_POST_OFFICE_BOY
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            86->
            {
                url = API.ADD_POST_DRIVER
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            87->
            {
                url = API.ADD_POST_COOK
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            88->
            {
                url = API.ADD_POST_DOCTOR_NURSE
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
            89->
            {
                url = API.ADD_POST_OTHER_JOBS
                callCommonData(
                    context,
                    url,
                    address,
                    lat,
                    long,
                    price,
                    generalAdPostController = GeneralAdPostController(
                        context as SetLocationActivity,
                        context
                    ),
                    packageid = packageid
                )
            }
        }
    }

    fun apiCallOfGrainSeed(
        context: Context,
        catid: Int,
        subCatId: Int,
        address: String,
        lat: String,
        long: String,
        price: String,
        packageid: String
    ) {
        var url: String =""
        when(subCatId) {
            90 -> {
                url = API.ADD_POST_SORGHUM
                AdPostDataSavedFunction.callForSaveAndSendGrainSeed(context, url, address, lat, long, price, grainSeedController = GrainSeedController(context as SetLocationActivity, context),packageid = packageid)
            }
            91 -> {
                url = API.ADD_POST_MILLET
                AdPostDataSavedFunction.callForSaveAndSendGrainSeed(context, url, address, lat, long, price, grainSeedController = GrainSeedController(context as SetLocationActivity, context),packageid = packageid)

            }
            92 -> {
                url = API.ADD_POST_RICE
                AdPostDataSavedFunction.callForSaveAndSendGrainSeed(context, url, address, lat, long, price, grainSeedController = GrainSeedController(context as SetLocationActivity, context),packageid = packageid)
            }
            93 -> {
                url = API.ADD_POST_GROUNDNUT
                AdPostDataSavedFunction.callForSaveAndSendGrainSeed(context, url, address, lat, long, price, grainSeedController = GrainSeedController(context as SetLocationActivity, context),packageid = packageid)

            }
            94 -> {
                url = API.ADD_POST_CORN
                AdPostDataSavedFunction.callForSaveAndSendGrainSeed(context, url, address, lat, long, price, grainSeedController = GrainSeedController(context as SetLocationActivity, context),packageid = packageid)

            }
            95 -> {
                url = API.ADD_POST_COTTON
                AdPostDataSavedFunction.callForSaveAndSendGrainSeed(context, url, address, lat, long, price, grainSeedController = GrainSeedController(context as SetLocationActivity, context),packageid = packageid)

            }
            96 -> {
                url = API.ADD_POST_CASTOR
                AdPostDataSavedFunction.callForSaveAndSendGrainSeed(context, url, address, lat, long, price, grainSeedController = GrainSeedController(context as SetLocationActivity, context),packageid = packageid)

            }
            97 -> {
                url = API.ADD_POST_CUMINSEED
                AdPostDataSavedFunction.callForSaveAndSendGrainSeed(context, url, address, lat, long, price, grainSeedController = GrainSeedController(context as SetLocationActivity, context),packageid = packageid)

            }
            98 -> {
                url = API.ADD_POST_TILL
                AdPostDataSavedFunction.callForSaveAndSendGrainSeed(context, url, address, lat, long, price, grainSeedController = GrainSeedController(context as SetLocationActivity, context),packageid = packageid)

            }
            99 -> {
                url = API.ADD_POST_RAI
                AdPostDataSavedFunction.callForSaveAndSendGrainSeed(context, url, address, lat, long, price, grainSeedController = GrainSeedController(context as SetLocationActivity, context),packageid = packageid)

            }
            100 -> {
                url = API.ADD_POST_AHI_SEED
                AdPostDataSavedFunction.callForSaveAndSendGrainSeed(context, url, address, lat, long, price, grainSeedController = GrainSeedController(context as SetLocationActivity, context),packageid = packageid)

            }
            101 -> {
                url = API.ADD_POST_POTATO
                AdPostDataSavedFunction.callForSaveAndSendGrainSeed(context, url, address, lat, long, price, grainSeedController = GrainSeedController(context as SetLocationActivity, context),packageid = packageid)

            }
            102 -> {
                url = API.ADD_POST_ONION
                AdPostDataSavedFunction.callForSaveAndSendGrainSeed(context, url, address, lat, long, price, grainSeedController = GrainSeedController(context as SetLocationActivity, context),packageid = packageid)

            }
            103 -> {
                url = API.ADD_POST_OTHERS
                AdPostDataSavedFunction.callForSaveAndSendGrainSeed(context, url, address, lat, long, price, grainSeedController = GrainSeedController(context as SetLocationActivity, context),packageid = packageid)
            }
        }
    }

    fun apiCallOfKhedut(
        context: Context,
        catid: Int,
        subCatId: Int,
        address: String,
        lat: String,
        long: String,
        price: String,
        packageid: String
    ) {
        var url: String =""
        when(subCatId) {
            110 -> {
                url = API.ADD_POST_GRAIN_FOR_SELL
                AdPostDataSavedFunction.callForSaveAndSendKhedut(context, url, address, lat, long, price, grainFruitVegController = GrainFruitVegController(context as SetLocationActivity, context),packageid = packageid)
            }
            111 ->
            {
                url = API.ADD_POST_FRUIT_FOR_SELL
                AdPostDataSavedFunction.callForSaveAndSendKhedut(context, url, address, lat, long, price, grainFruitVegController = GrainFruitVegController(context as SetLocationActivity, context),packageid = packageid)
            }

            112 ->
            {
                url = API.ADD_POST_VEGETABLE_FOR_SELL
                AdPostDataSavedFunction.callForSaveAndSendKhedut(context, url, address, lat, long, price, grainFruitVegController = GrainFruitVegController(context as SetLocationActivity, context),packageid = packageid)
            }
            113 ->
            {
                url = API.ADD_POST_ANIMAL_FOR_SELL
                AdPostDataSavedFunction.callForSaveAndSendKhedut(context, url, address, lat, long, price, grainFruitVegController = GrainFruitVegController(context as SetLocationActivity, context),packageid = packageid)
            }
            114 ->
            {
                url = API.ADD_POST_NURSUERY_PLANT_FOR_SELL
                AdPostDataSavedFunction.callForSaveAndSendKhedut(context, url, address, lat, long, price, grainFruitVegController = GrainFruitVegController(context as SetLocationActivity, context),packageid = packageid)
            }
            115 ->
            {
                url = API.ADD_POST_FERTILIZER_SEEDS
                AdPostDataSavedFunction.callForSaveAndSendKhedut(context, url, address, lat, long, price, grainFruitVegController = GrainFruitVegController(context as SetLocationActivity, context),packageid = packageid)
            }
            116 ->
            {
                url = API.ADD_POST_FARMING_MACHINE
                AdPostDataSavedFunction.callForSaveAndSendKhedut(context, url, address, lat, long, price, grainFruitVegController = GrainFruitVegController(context as SetLocationActivity, context),packageid = packageid)
            }
            117 ->
            {
                url = API.ADD_POST_TRACTOR_TROLLEY
                AdPostDataSavedFunction.callForSaveAndSendKhedut(context, url, address, lat, long, price, grainFruitVegController = GrainFruitVegController(context as SetLocationActivity, context),packageid = packageid)
            }
            118 ->
            {
                url = API.ADD_POST_TRANSPORT_FOR_KHEDUT
                AdPostDataSavedFunction.callForSaveAndSendKhedut(context, url, address, lat, long, price, grainFruitVegController = GrainFruitVegController(context as SetLocationActivity, context),packageid = packageid)
            }
            119 ->
            {
                url = API.ADD_POST_FARM_FOR_RENT
                AdPostDataSavedFunction.callForSaveAndSendKhedut(context, url, address, lat, long, price, grainFruitVegController = GrainFruitVegController(context as SetLocationActivity, context),packageid = packageid)
            }
        }
    }
}