package com.kaushlesh.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.util.Log
import com.kaushlesh.constant.AppConstants
import java.io.ByteArrayOutputStream

object BitmapOptimizer {
    var encodedImage: ByteArray? = null
    private var exception: Exception? = null
    fun optimizeFile(
        filePath: String?,
        maxSize: Long,
        listener: CompleteListener
    ) {
        if (filePath == null) {
            listener.onFail("File is null!")
            return
        }
        if (filePath.isEmpty()) {
            listener.onFail("File Path is Empty!")
            return
        }
        object : AsyncTask<Void?, Void?, Bitmap?>() {
            override fun onPostExecute(aVoid: Bitmap?) {
                super.onPostExecute(aVoid)
                if (aVoid != null) {
                    listener.onComplete(aVoid, encodedImage)
                } else {
                    if (exception != null) listener.onFail(exception!!.message) else listener.onFail(
                        "Image Compressing Failed!"
                    )
                }
            }

            override fun doInBackground(vararg p0: Void?): Bitmap? {
                return try {
                    var input = BitmapFactory.decodeFile(filePath)
                    var quality = 100
                    var size = -1L
                    do {
                        val baos = ByteArrayOutputStream()
                        input.compress(Bitmap.CompressFormat.JPEG, quality, baos)
                        encodedImage = baos.toByteArray()
                        size = baos.size().toLong()
                        Log.e(
                            "Compration",
                            "Size :: " + size + " Formatted : " + AppConstants.getSize(size * 1L) + " quality : " + quality
                        )
                        quality -= 2
                        if (size / 2 >= maxSize) {
                            quality -= 20
                        }
                    } while (size > maxSize)
                    input = BitmapFactory.decodeByteArray(
                        encodedImage,
                        0,
                        encodedImage!!.size
                    )
                    //encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                    input
                } catch (E: Exception) {
                    exception = E
                    null
                }

            }
        }.execute()
    }

    interface CompleteListener {
        fun onComplete(bitmap: Bitmap?, encodedImage: ByteArray?)
        fun onFail(message: String?)
    }
}
