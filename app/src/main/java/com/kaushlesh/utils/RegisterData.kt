package com.kaushlesh.utils

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import java.io.Serializable

class RegisterData: Serializable {
    var isPhone: Int ?=null
    lateinit var aboutyou: String
    var image: Drawable? = null
    lateinit var devicetoken: String
    lateinit var deviceid: String
    lateinit var state: String
    lateinit var name: String
    lateinit var profile: Bitmap
    lateinit var socialimgFilePath: ByteArray
    lateinit var socialtype: String
    lateinit var socialtoken: String
    lateinit var socialid: String
    lateinit var email: String
    lateinit var phone: String
    lateinit var locationid: String

    var whatsapp_inquiry_allow: Int? = null
    var same_whatsapp_no: Int? = null
    lateinit var whatsapp_no: String

}