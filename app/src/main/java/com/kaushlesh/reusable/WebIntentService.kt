package com.kaushlesh.reusable

import android.app.IntentService
import android.content.Intent
import android.os.Bundle
import android.os.ResultReceiver
import android.util.Log
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.kaushlesh.application.KaushleshApplication
import java.io.UnsupportedEncodingException
import javax.net.ssl.HttpsURLConnection


// TODO: Auto-generated Javadoc


/**
 * The Class WebIntentService.
 */
/**
 * Instantiates a new web intent service.
 */
class WebIntentService : IntentService("WebIntentService") {
    var INSTANCE: KaushleshApplication? = null
    override fun onHandleIntent(intent: Intent?) {
        val params = intent?.getSerializableExtra("params") as RequestParams?

        val tag_string_req = params!!.methodName

        val url = params.url

        val rec = intent?.getParcelableExtra<ResultReceiver>("rec")

        val strReq = object : StringRequest(
            params.method, url,
            Response.Listener { response ->
                val b = Bundle()
                b.putString("response", response)
                b.putString("method", params.methodName)
                rec!!.send(200, b)
                //				Log.d("response", response.toString());
            }, Response.ErrorListener { error ->
                val b = Bundle()
                var data = ""
                var errorCode = 0
                if (error is NetworkError) {
                    b.putString("error", "NetworkError : " + error.getLocalizedMessage()!!)
                } else if (error is ServerError) {
                    b.putString("error", "ServerError : " + error.getLocalizedMessage()!!)
                } else if (error is AuthFailureError) {
                    b.putString("error", "AuthFailureError : " + error.getLocalizedMessage()!!)

                } else if (error is ParseError) {
                    b.putString("error", "ParseError : " + error.getLocalizedMessage()!!)

                } else if (error is NoConnectionError) {
                    b.putString("error", "NoConnectionError : " + error.getLocalizedMessage()!!)

                } else if (error is TimeoutError) {
                    b.putString("error", "TimeoutError : " + error.getLocalizedMessage()!!)

                }


                if (error.networkResponse != null && error.networkResponse.statusCode == HttpsURLConnection.HTTP_BAD_REQUEST) {

                    Log.e("error", "Error: BadRequest ")
                    errorCode = error.networkResponse.statusCode

                }
                if (error.networkResponse != null && error.networkResponse.data != null) {

                    data = String(error.networkResponse.data)
                    Log.e("error", "Error: Data  $data")

                }
                b.putString("method", params.methodName)
                b.putString("error", data)
                rec!!.send(errorCode, b)
                Log.e("error", "Error: " + error.message)
            }
        ) {
            override fun getBodyContentType(): String {
                // TODO Auto-generated method stub
                return params.contentType

            }

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray {
                // TODO Auto-generated method stub
                try {
                    return params.contentBody.toString().toByteArray()

                } catch (e: UnsupportedEncodingException) {

                    e.printStackTrace()

                }

                return params.contentBody

            }

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                // TODO Auto-generated method stub
                return params.headers

            }

            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                // TODO Auto-generated method stub
                return params.params

            }

            override fun getPriority(): Priority {
                // TODO Auto-generated method stub
                return params.priority

            }

        }

        val socketTimeout = 30000//30 seconds - change to what you want
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        strReq.retryPolicy = policy
        INSTANCE = this as KaushleshApplication
        INSTANCE!!.addToRequestQueue(strReq, tag_string_req)

    }

    /* (non-Javadoc)
	 * @see android.app.IntentService#onHandleIntent(android.content.Intent)
	 */


}
