package com.kaushlesh.reusable

import android.graphics.Bitmap
import android.util.Log


class BitmapUtils {
companion object
{
    fun scaleBitmap(input: Bitmap, maxBytes: Long): Bitmap? {
        val currentWidth = input.width
        val currentHeight = input.height
        val currentPixels = currentWidth * currentHeight
        // Get the amount of max pixels:
        // 1 pixel = 4 bytes (R, G, B, A)
        val maxPixels = maxBytes / 4 // Floored
        if (currentPixels <= maxPixels) {
            // Already correct size:
            return input
        }
        // Scaling factor when maintaining aspect ratio is the square root since x and y have a relation:
        val scaleFactor = Math.sqrt(maxPixels / currentPixels.toDouble())
        val newWidthPx = Math.floor(currentWidth * scaleFactor).toInt()
        val newHeightPx = Math.floor(currentHeight * scaleFactor).toInt()
        Log.e("TAG", "Scaled bitmap sizes are %1\$s x %2\$s when original sizes are %3\$s x %4\$s and currentPixels %5\$s and maxPixels %6\$s and scaled total pixels are: %7\$s" + newWidthPx + newHeightPx+
                currentWidth + currentHeight + currentPixels + maxPixels+ newWidthPx * newHeightPx)

        return Bitmap.createScaledBitmap(input, newWidthPx, newHeightPx, true)
    }
}
}
