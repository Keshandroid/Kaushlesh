package com.kaushlesh.reusable

import android.util.Log
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class EndlessRecyclerOnScrollListener(private val mLinearLayoutManager: GridLayoutManager) : RecyclerView.OnScrollListener() {
    private val previousTotal = 0 // The total number of items in the dataset after the last load
    var loading = true // True if we are still waiting for the last set of data to load.
    private val visibleThreshold = 5 // The minimum amount of items to have below your current scroll position before loading more.
    var firstVisibleItem = 0
    var lastVisibleItem = 0
    var visibleItemCount = 0
    var totalItemCount = 0
    var pastVisiblesItems = 0
    var current_page = 1
    private val itemsPerPage = 10
    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        visibleItemCount = recyclerView.childCount
        totalItemCount = mLinearLayoutManager.itemCount
        lastVisibleItem = mLinearLayoutManager.findLastVisibleItemPosition();
        firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition()
        Log.e("TAG", "inside onscroll")


      /*  if ((visibleItemCount + firstVisibleItem) >= totalItemCount) {
            current_page++
            Log.e("TAG", "Preparing to call onLoadMore totalItemCount : $totalItemCount visibleItemCount : $visibleItemCount firstVisibleItem : $firstVisibleItem")
            onLoadMore(current_page)
            loading = true
        }*/

    /*    pastVisiblesItems = mLinearLayoutManager.findFirstVisibleItemPosition()
        if (!loading && visibleItemCount + pastVisiblesItems >= totalItemCount) {
            //bottom of recyclerview
            current_page++
            Log.e("TAG", "Preparing to call onLoadMore totalItemCount : $totalItemCount visibleItemCount : $visibleItemCount firstVisibleItem : $firstVisibleItem")
            onLoadMore(current_page)
            loading = true
        }*/

        if (!loading && (totalItemCount - visibleItemCount <= firstVisibleItem + visibleThreshold) && totalItemCount > 0 && firstVisibleItem >= 0) {
            // End has been reached
            // Do something
            if (itemsPerPage * current_page + 10 != totalItemCount) {
                Log.e("TAG", "if if scroll")
                return
            }
            current_page++
            Log.e("TAG", "Preparing to call onLoadMore totalItemCount : $totalItemCount visibleItemCount : $visibleItemCount firstVisibleItem : $firstVisibleItem")
            onLoadMore(current_page)
            loading = true
        }
        else{
            Log.e("TAG", "else inside onscroll")

        }
    }

    abstract fun onLoadMore(current_page: Int)

    companion object {
        var TAG = EndlessRecyclerOnScrollListener::class.java.simpleName
    }
}