package com.kaushlesh.reusable

import com.android.volley.Request
import java.io.Serializable
import java.util.HashMap

// TODO: Auto-generated Javadoc

/**
 * The Class RequestParams.
 */
class RequestParams : Serializable {

    /** The content type.  */
    /**
     * Gets the content type.
     *
     * @return the content type
     */
    /**
     * Sets the content type.
     *
     * @param contentType the new content type
     */
    lateinit var contentType: String

    /** The content body.  */
    /**
     * Gets the content body.
     *
     * @return the content body
     */
    /**
     * Sets the content body.
     *
     * @param contentBody the new content body
     */
    lateinit var contentBody: ByteArray

    /** The headers.  */
    /**
     * Gets the headers.
     *
     * @return the headers
     */
    /**
     * Sets the headers.
     *
     * @param headers the headers
     */
    lateinit var headers: HashMap<String, String>

    /** The params.  */
    /**
     * Gets the params.
     *
     * @return the params
     */
    /**
     * Sets the params.
     *
     * @param params the params
     */
    lateinit var params: HashMap<String, String>

    /** The url.  */
    /**
     * Gets the url.
     *
     * @return the url
     */
    /**
     * Sets the url.
     *
     * @param url the new url
     */
    lateinit var url: String

    /** The method name.  */
    /**
     * Gets the method name.
     *
     * @return the method name
     */
    /**
     * Sets the method name.
     *
     * @param methodName the new method name
     */
    lateinit var methodName: String

    /** The method.  */
    /**
     * Gets the method.
     *
     * @return the method
     */
    /**
     * Sets the method.
     *
     * @param method the new method
     */
    var method: Int = 0

    /** The priority.  */
    /**
     * Gets the priority.
     *
     * @return the priority
     */
    /**
     * Sets the priority.
     *
     * @param priority the new priority
     */
    lateinit var priority: Request.Priority

}
