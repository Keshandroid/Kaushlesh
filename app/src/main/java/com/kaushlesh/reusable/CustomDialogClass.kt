package com.kaushlesh.reusable
import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.AutoCompleteTextView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.widget.AppCompatEditText
import com.google.android.material.textfield.TextInputLayout
import com.kaushlesh.R
import com.kaushlesh.interfaces.DialogClickListener


class CustomDialogClass(
    var context: Activity,
    private val dialogType: Int
)//super(a, R.style.myDialogTheme);
    : Dialog(context), View.OnClickListener {
    var mTlForgotPassword: TextInputLayout? = null
    var email: AppCompatEditText? = null
    var verifyCode: AutoCompleteTextView? = null
    private val btnOk: TextView? = null
    private val btnCancel: TextView? = null
    private var tvProgressMessage: TextView? = null
    private val tvNote: TextView? = null
    private var progressBar: ProgressBar? = null
    private val dialogClickListener: DialogClickListener? = null

    internal var message = ""

    fun setOkButtonClickListener(onClickListener: View.OnClickListener) {
        btnOk!!.setOnClickListener(onClickListener)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        context.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT)
        when (dialogType) {
            DT_PROGRESS -> {
                setContentView(R.layout.custom_dialog_progressbar)
                //getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                //getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                //getWindow().setBackgroundDrawable(null);

                if(context.window != null) {
                    context.window!!.setLayout(
                        WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.WRAP_CONTENT)
                } else {
                    Log.e("DIALOG", "Window is null");
                }
                progressBar = findViewById(R.id.customProgress)
                setContentView(R.layout.custom_dialog_progress)
                if(context.window != null) {
                    context.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.WRAP_CONTENT)
                } else {
                    Log.e("DIALOG", "Window is null");
                }
                progressBar = findViewById(R.id.customProgress)

                /*    progressBar.getIndeterminateDrawable().
                        setColorFilter(ContextCompat.getColor(AppController.getContext(),R.color.colorPrimary),
                                PorterDuff.Mode.MULTIPLY);*/

                tvProgressMessage = findViewById(R.id.tvProgressMessage)
                tvProgressMessage!!.visibility = View.GONE
                if (dialogType == DT_PROGRESS_MSG) {
                    tvProgressMessage!!.visibility = View.VISIBLE
                    tvProgressMessage!!.text = message
                }
            }

            /*    progressBar.getIndeterminateDrawable().
                        setColorFilter(ContextCompat.getColor(AppController.getContext(),R.color.colorPrimary),
                                PorterDuff.Mode.MULTIPLY);*/


            DT_PROGRESS_MSG -> {
                setContentView(R.layout.custom_dialog_progress)
                if(context.window != null) {
                    context.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.WRAP_CONTENT)
                } else {
                    Log.e("DIALOG", "Window is null");
                }
                progressBar = findViewById(R.id.customProgress)
                tvProgressMessage = findViewById(R.id.tvProgressMessage)
                tvProgressMessage!!.visibility = View.GONE
                if (dialogType == DT_PROGRESS_MSG) {
                    tvProgressMessage!!.visibility = View.VISIBLE
                    tvProgressMessage!!.text = message
                }
            }
        }/* case DT_MSG_ERROR:
                setContentView(R.layout.custom_dialog_no_internet);
                getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                tvProgressMessage = findViewById(R.id.tvProgressMessage);
                tvProgressMessage.setVisibility(View.VISIBLE);
                tvProgressMessage.setText(message);

                btnCancel = findViewById(R.id.btnCancel);
                btnOk = findViewById(R.id.btnOk);
                btnOk.setOnClickListener(this);
                btnCancel.setOnClickListener(this);

                btnOk.setVisibility(View.VISIBLE);
                btnCancel.setVisibility(View.GONE);
                break;*//*
            case DT_NO_INTERNET:
                setContentView(R.layout.custom_dialog_no_internet);
                getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                tvProgressMessage = findViewById(R.id.tvProgressMessage);
                tvProgressMessage.setVisibility(View.VISIBLE);
                tvProgressMessage.setText(context.getResources().getString(R.string.txt_no_internet));
                tvProgressMessage.setGravity(Gravity.CENTER);
                btnCancel = findViewById(R.id.btnCancel);
                btnOk = findViewById(R.id.btnOk);
                btnOk.setOnClickListener(this);
                btnCancel.setOnClickListener(this);

                btnOk.setVisibility(View.VISIBLE);
                btnCancel.setVisibility(View.GONE);
                break;*/
    }

    override fun onClick(v: View) {
        dismiss()
    }

    fun setMessage(message: String) {
        this.message = message
    }

    companion object {

        val DT_PROGRESS = 0
        val DT_PROGRESS_MSG = 1
        val DT_MSG_ERROR = 2
        val DT_NO_INTERNET = 3
        val DT_FORGOT_PASSWORD = 4
    }


}