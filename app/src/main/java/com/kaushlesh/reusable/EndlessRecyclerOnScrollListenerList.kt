/*
package com.kaushlesh.reusable

import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class EndlessRecyclerOnScrollListenerList(private val mLinearLayoutManager: LinearLayoutManager) : RecyclerView.OnScrollListener() {
    private val previousTotal = 0 // The total number of items in the dataset after the last load
    var loading = true // True if we are still waiting for the last set of data to load.
    private val visibleThreshold = 5 // The minimum amount of items to have below your current scroll position before loading more.
    var firstVisibleItem = 0
    var visibleItemCount = 0
    var totalItemCount = 0
    var current_page = 1
    private val itemsPerPage = 10
    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        visibleItemCount = recyclerView.childCount
        totalItemCount = mLinearLayoutManager.itemCount
        firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition()

        */
/*if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false;
                previousTotal = totalItemCount;
            }
        }*//*
if (!loading && (totalItemCount - visibleItemCount
                        <= firstVisibleItem + visibleThreshold) && totalItemCount > 0 && firstVisibleItem >= 0) {
            // End has been reached
            // Do something
            if (itemsPerPage * current_page + 10 != totalItemCount) return
            current_page++
            Log.e("TAG", "Preparing to call onLoadMore totalItemCount : $totalItemCount visibleItemCount : $visibleItemCount firstVisibleItem : $firstVisibleItem")
            onLoadMore(current_page)
            loading = true
        }
    }

    abstract fun onLoadMore(current_page: Int)

    companion object {
        var TAG = EndlessRecyclerOnScrollListenerList::class.java.simpleName
    }
}*/
package com.kaushlesh.reusable

import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager

abstract class EndlessRecyclerOnScrollListenerList : RecyclerView.OnScrollListener() {
    // The total number of items in the dataset after the last load
    private var previousTotalItemCount = 0
    private var loading = true
    private val visibleThreshold = 5
    var firstVisibleItem = 0
    var visibleItemCount = 0
    var totalItemCount = 0
    private val startingPageIndex = 0
    private var currentPage = -1
    override fun onScrolled(mRecyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(mRecyclerView, dx, dy)
        val mLayoutManager = mRecyclerView.layoutManager as LinearLayoutManager?
        visibleItemCount = mRecyclerView.childCount
        totalItemCount = mLayoutManager!!.itemCount
        firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition()

        onScroll(firstVisibleItem, visibleItemCount, totalItemCount)
    }

    fun onScroll(firstVisibleItem: Int, visibleItemCount: Int, totalItemCount: Int) {
        // If the total item count is zero and the previous isn't, assume the
        // list is invalidated and should be reset back to initial state
        if (totalItemCount < previousTotalItemCount) {
            currentPage = startingPageIndex
            previousTotalItemCount = totalItemCount
            if (totalItemCount == 0) {
                loading = true
            }
        }
        // If it’s still loading, we check to see if the dataset count has
        // changed, if so we conclude it has finished loading and update the current page
        // number and total item count.
        if (loading && totalItemCount > previousTotalItemCount) {
            Log.e("Pagination","Loading : $loading totalItemCount : $totalItemCount  previousTotalItemCount : $previousTotalItemCount")
            loading = false
            previousTotalItemCount = totalItemCount
            currentPage++
        }

        // If it isn’t currently loading, we check to see if we have breached
        // the visibleThreshold and need to reload more data.
        // If we do need to reload some more data, we execute onLoadMore to fetch the data.
        if (!loading && totalItemCount - visibleItemCount <= firstVisibleItem +
            visibleThreshold
        ) {
            onLoadMore(currentPage + 1, totalItemCount)
            loading = true
        }
    }

    // Defines the process for actually loading more data based on page
    abstract fun onLoadMore(page: Int, totalItemsCount: Int)
    fun reset() {
        previousTotalItemCount = 0
        loading = true
        firstVisibleItem = 0
        visibleItemCount = 0
        totalItemCount = 0
        currentPage = -1
    }
}