package com.kaushlesh.reusable

import android.content.Context
import android.graphics.Bitmap
import androidx.collection.LruCache
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.ImageLoader
import com.android.volley.toolbox.Volley

class VolleySingleTon private constructor(context: Context) {
    private var mRequestQueue: RequestQueue? = null
    /**
     * Get image loader.
     *
     * @return ImageLoader
     */
    val imageLoader: ImageLoader

    // 4 bytes per pixel
    val cacheSize: Int
        get() {
            val displayMetrics = mCtx.resources.displayMetrics
            val screenWidth = displayMetrics.widthPixels
            val screenHeight = displayMetrics.heightPixels
            val screenBytes = screenWidth * screenHeight * 4

            return screenBytes * 3
        }

    /**
     * Get current request queue.
     *
     * @return RequestQueue
     */
    // getApplicationContext() is key, it keeps you from leaking the
    // Activity or BroadcastReceiver if someone passes one in.
    val requestQueue: RequestQueue
        get() {
            if (mRequestQueue == null) {

                mRequestQueue = Volley.newRequestQueue(mCtx.applicationContext)
            }
            return this.mRequestQueue!!
        }


    init {
        mCtx = context
        mRequestQueue = requestQueue

        imageLoader = ImageLoader(mRequestQueue,
            object : ImageLoader.ImageCache {
                private val cache = LruBitmapCache(cacheSize)

                override fun getBitmap(url: String): Bitmap? {
                    return cache.get(url)
                }

                override fun putBitmap(url: String, bitmap: Bitmap) {
                    cache.put(url, bitmap)
                }
            })
    }

    /**
     * Add new request depend on type like string, json object, json array request.
     *
     * @param req new request
     * @param <T> request type
    </T> */
    fun <T> addToRequestQueue(req: Request<T>) {
        requestQueue.cache.clear()
        requestQueue.add(req)
    }

    inner class LruBitmapCache(maxSize: Int) : LruCache<String, Bitmap>(maxSize),
        ImageLoader.ImageCache {

        /*  public LruBitmapCache() {
            super();
        }*/

        override fun sizeOf(key: String, value: Bitmap): Int {
            return value.rowBytes * value.height
        }

        override fun getBitmap(url: String): Bitmap? {
            return get(url)
        }

        override fun putBitmap(url: String, bitmap: Bitmap) {
            put(url, bitmap)
        }

    }

    companion object {

        private var mInstance: VolleySingleTon? = null
        private lateinit var mCtx: Context
        @Synchronized
        fun getInstance(context: Context): VolleySingleTon {
            if (mInstance == null) {
                mInstance = VolleySingleTon(context)

            }
            return mInstance as VolleySingleTon
        }
    }
}
