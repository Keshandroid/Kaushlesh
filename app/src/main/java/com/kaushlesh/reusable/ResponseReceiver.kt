package com.kaushlesh.reusable

import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver


// TODO: Auto-generated Javadoc

/**
 * The Class ResponseReceiver.
 */
class ResponseReceiver
/**
 * Instantiates a new response receiver.
 *
 * @param handler the handler
 */
    (handler: Handler)// TODO Auto-generated constructor stub
    : ResultReceiver(handler) {

    /** The listener.  */
    private var listener: Listener? = null

    /**
     * Sets the listener.
     *
     * @param listener the new listener
     */
    fun setListener(listener: Listener) {
        this.listener = listener
    }

    /* (non-Javadoc)
	 * @see android.os.ResultReceiver#onReceiveResult(int, android.os.Bundle)
	 */
    override fun onReceiveResult(resultCode: Int, resultData: Bundle) {
        if (listener != null)
            listener!!.onReceiveResult(resultCode, resultData)
    }

    /**
     * The Interface Listener.
     */
    interface Listener {

        /**
         * On receive result.
         *
         * @param resultCode the result code
         * @param resultData the result data
         */
        fun onReceiveResult(resultCode: Int, resultData: Bundle)
    }


}
