package com.kaushlesh.interfaces

interface DialogClickListener {
    fun onDialogItemClick(position: Int)

}
