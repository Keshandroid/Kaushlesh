package com.kaushlesh.interfaces

import com.kaushlesh.widgets.CustomEditText
import org.json.JSONException
import org.json.JSONObject

interface ParseControllerListener {
    @Throws(JSONException::class)
    fun onSuccess(da: JSONObject, message: String, method: String)

    fun onFail(msg: String, method: String)

    fun addTextChangedListener(etSix: CustomEditText?) {
    }

}
