package com.kaushlesh.parser

import android.app.Activity
import android.content.Intent
import android.util.Log
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.reusable.VolleySingleTon
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.Login.LoginMainActivity
import org.json.JSONException
import org.json.JSONObject


class PostRequestParsing {

    val TAG = "PostRequestParsing"
    lateinit var data: JSONObject

    fun call(
            activity: Activity,
            url: String,
            method: String,
            pm: Map<String, String>,
            listner: ParseControllerListener
    ) {
        data = JSONObject()

        Log.e(TAG, "URL ==>$url")

        val stringRequest = object : StringRequest(Request.Method.POST, url,
                Response.Listener<String> { response ->

                    try {

                        val status: String
                        val message: String

                        Log.e(TAG, "Api Response: $response")

                        val main = JSONObject(response)

                        Log.e("Response", "" + main.toString())

                        status = main.getString("status")
                        message = main.getString("message")

                        if (status.equals("1", ignoreCase = true)) {

                            if (main.has("result")) {
                                val main1 = main.getJSONObject("result")
                                listner.onSuccess(main1, message, method)
                            } else {
                                listner.onSuccess(main, message, method)
                            }

                        } else {
                            //listner.onFail(message, method)

                            if (message.equals("You are not authorized please login again.")) {
                                val storeusedata = StoreUserData(activity)
                                storeusedata.setString(Constants.USER_ID, "")
                                storeusedata.setString(Constants.TOKEN, "")
                                storeusedata.setString(Constants.IS_LOGGED_IN, "")
                                storeusedata.setString(Constants.IS_OTP_VERIFED, "")
                                storeusedata.setString(Constants.IS_ACTIVE, "")
                                storeusedata.setString(Constants.DEVICE_TOKEN, "")
                                storeusedata.setString(Constants.DEVICE_TYPE, "")
                                storeusedata.setString(Constants.REGISTREATION_TYPE, "")

                                val intent = Intent(activity, LoginMainActivity::class.java)
                                activity.startActivity(intent)
                                activity.finishAffinity()
                            } else {
                                listner.onFail(message, method)
                            }
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }, Response.ErrorListener { error ->
            val networkResponse = error.networkResponse
            var errorMessage = "Unknown error"

            Utils.dismissProgress() //new added

            if (networkResponse == null) {
                if (error.javaClass == TimeoutError::class.java) {
                    errorMessage = "Request timeout"
                } else if (error.javaClass == NoConnectionError::class.java) {
                    errorMessage = "Failed to connect server"

                    if(method == "purchasePkg"){
                        Utils.showAlertPackageError(activity)
                    }else{
                        Utils.showAlertConnection(activity)
                    }


                }
            } else {
                val result = String(networkResponse.data)
                try {
                    val response = JSONObject(result)
                    val status = response.getString("status")
                    val message = response.getString("message")

                    Log.e("Error Status", status)
                    Log.e("Error Message", message)

                    if (networkResponse.statusCode == 404) {
                        errorMessage = "Resource not found"
                    } else if (networkResponse.statusCode == 401) {
                        errorMessage = "$message Please login again"
                    } else if (networkResponse.statusCode == 400) {
                        errorMessage = "$message Check your inputs"
                    } else if (networkResponse.statusCode == 500) {
                        errorMessage = "$message Something is getting wrong"
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            }

            Log.i("Error", errorMessage)
            Log.i("Error", error.toString())
            error.printStackTrace()
        })  {
            override fun getParams(): Map<String, String> {
                val params: Map<String, String>
                params = pm
                return params
            }

           /* @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray {
                val params: Map<String, String>
                params = pm
                return JSONObject(params).toString().toByteArray()
            }*/

          /*  override fun getHeaders(): Map<String, String> {
                val params: MutableMap<String, String> = HashMap()
                params["Content-Type"] = "multipart/form-data"
                return params
            }*/

            /*override fun getBodyContentType(): String {
               return "application/json; charset=utf-8"
           }*/

            /*@Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                var params: Map<String, String> = HashMap()
                params = pm

                Log.e(TAG, "param  : $params")

                return params
            }*/

           /* @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String>? {
                val params: MutableMap<String, String> =
                    HashMap()
                params["Content-Type"] = "application/x-www-form-urlencoded"
                return params
            }*/
            
        }



       /* stringRequest.retryPolicy = DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )*/

        stringRequest.setRetryPolicy(
                DefaultRetryPolicy(
                        60000,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                )
        )

        //stringRequest.setShouldCache(false)
        VolleySingleTon.getInstance(activity).addToRequestQueue(stringRequest)
    }

    fun callarray(
            activity: Activity,
            url: String,
            method: String,
            pm: Map<String, String>,
            listner: ParseControllerListener
    ) {
        data = JSONObject()

        Log.e(TAG, "URL ==>$url")

        val stringRequest = object : StringRequest(Request.Method.POST, url,
                Response.Listener<String> { response ->

                    try {

                        val status: String
                        val message: String

                        Log.e(TAG, "Api Response: $response")

                        val main = JSONObject(response)

                        Log.e("Response", "" + main.toString())

                        status = main.getString("status")
                        message = main.getString("message")

                        if (status.equals("1", ignoreCase = true)) {

                            listner.onSuccess(main, message, method)

                        } else {
                            //listner.onFail(message, method)

                            if (message.equals("You are not authorized please login again.")) {
                                val storeusedata = StoreUserData(activity)
                                storeusedata.setString(Constants.USER_ID, "")
                                storeusedata.setString(Constants.TOKEN, "")
                                storeusedata.setString(Constants.IS_LOGGED_IN, "")
                                storeusedata.setString(Constants.IS_OTP_VERIFED, "")
                                storeusedata.setString(Constants.IS_ACTIVE, "")
                                storeusedata.setString(Constants.DEVICE_TOKEN, "")
                                storeusedata.setString(Constants.DEVICE_TYPE, "")
                                storeusedata.setString(Constants.REGISTREATION_TYPE, "")

                                val intent = Intent(activity, LoginMainActivity::class.java)
                                activity.startActivity(intent)
                                activity.finishAffinity()
                            } else {
                                listner.onFail(message, method)
                            }
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }, Response.ErrorListener { error ->
            val networkResponse = error.networkResponse
            var errorMessage = "Unknown error"

            Utils.dismissProgress() //new added

            if (networkResponse == null) {
                if (error.javaClass == TimeoutError::class.java) {
                    errorMessage = "Request timeout"
                } else if (error.javaClass == NoConnectionError::class.java) {
                    errorMessage = "Failed to connect server"
                    Utils.showAlertConnection(activity)
                }
            } else {
                val result = String(networkResponse.data)
                try {
                    val response = JSONObject(result)
                    val status = response.getString("status")
                    val message = response.getString("message")

                    Log.e("Error Status", status)
                    Log.e("Error Message", message)

                    if (networkResponse.statusCode == 404) {
                        errorMessage = "Resource not found"
                    } else if (networkResponse.statusCode == 401) {
                        errorMessage = "$message Please login again"
                    } else if (networkResponse.statusCode == 400) {
                        errorMessage = "$message Check your inputs"
                    } else if (networkResponse.statusCode == 500) {
                        errorMessage = "$message Something is getting wrong"
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

            Log.i("Error", errorMessage)
            Log.i("Error", error.toString())
            error.printStackTrace()
        })  {
            override fun getParams(): Map<String, String> {
                val params: Map<String, String>
                params = pm
                return params
            }

            /*@Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                var params: Map<String, String> = HashMap()
                params = pm

                Log.e(TAG, "param  : $params")

                return params
            }*/

            /* @Throws(AuthFailureError::class)
             override fun getHeaders(): Map<String, String>? {
                 val params: MutableMap<String, String> =
                     HashMap()
                 params["Content-Type"] = "application/x-www-form-urlencoded"
                 return params
             }*/
        }

        stringRequest.retryPolicy = DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        //stringRequest.setShouldCache(false)
        VolleySingleTon.getInstance(activity).addToRequestQueue(stringRequest)
    }
}