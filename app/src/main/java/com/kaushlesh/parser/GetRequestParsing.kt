package com.kaushlesh.parser

import android.app.Activity
import android.content.Intent
import android.util.Log
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.kaushlesh.interfaces.ParseControllerListener
import com.kaushlesh.reusable.VolleySingleTon
import com.kaushlesh.utils.Constants
import com.kaushlesh.utils.StoreUserData
import com.kaushlesh.utils.Utils
import com.kaushlesh.view.activity.Login.LoginMainActivity
import org.json.JSONException
import org.json.JSONObject
import java.util.concurrent.TimeUnit


class GetRequestParsing {
    private var mRequestStartTime: Long = 0

    val TAG = "GetRequestParsing"
    var data: JSONObject? = null

    fun callApi(
        activity: Activity,
        url: String,
        method: String,
        listener: ParseControllerListener
    ) {

        mRequestStartTime = System.currentTimeMillis(); // set the request start time just before you send the request.

        val request = StringRequest(
            Request.Method.GET, url,
            Response.Listener<String> { response ->

                val totalRequestTime: Long = System.currentTimeMillis() - mRequestStartTime
                val seconds = TimeUnit.MILLISECONDS.toSeconds(totalRequestTime)
                Utils.showLog("API", "### Success time ###" + seconds)
                try {
                    val main = JSONObject(response)

                    Log.e(TAG, response + "")

                    val status = main.getString("status")
                    val msg = main.getString("message")

                    if (status.equals("1", ignoreCase = true)) {

                        //val main1 = main.getJSONObject("result")
                        listener.onSuccess(main, msg, method)
                    } else {
                        //listener.onFail(msg, method)
                        if (msg.equals("You are not authorized please login again.")) {
                            val storeusedata = StoreUserData(activity)
                            storeusedata.setString(Constants.USER_ID, "")
                            storeusedata.setString(Constants.TOKEN, "")
                            storeusedata.setString(Constants.IS_LOGGED_IN, "")
                            storeusedata.setString(Constants.IS_OTP_VERIFED, "")
                            storeusedata.setString(Constants.IS_ACTIVE, "")
                            storeusedata.setString(Constants.DEVICE_TOKEN, "")
                            storeusedata.setString(Constants.DEVICE_TYPE, "")
                            storeusedata.setString(Constants.REGISTREATION_TYPE, "")

                            val intent = Intent(activity, LoginMainActivity::class.java)
                            activity.startActivity(intent)
                            activity.finishAffinity()
                        } else {
                            listener.onFail(msg, method)
                        }

                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { error ->
                val networkResponse = error.networkResponse
                var errorMessage = "Unknown error"
                Utils.dismissProgress() //new added

                if (networkResponse == null) {
                    if (error.javaClass == TimeoutError::class.java) {
                        errorMessage = "Request timeout"
                    } else if (error.javaClass == NoConnectionError::class.java) {
                        errorMessage = "Failed to connect server"
                        Utils.showAlertConnection(activity)
                    }
                } else {
                    val result = String(networkResponse.data)
                    try {
                        val response = JSONObject(result)
                        val status = response.getString("status")
                        val message = response.getString("message")

                        Log.e("Error Status", status)
                        Log.e("Error Message", message)

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found"
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = "$message Please login again"
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = "$message Check your inputs"
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = "$message Something is getting wrong"
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }


                }

                Log.i("Error", errorMessage)
                error.printStackTrace()
            })
           /* Response.ErrorListener { error ->
                error.printStackTrace()
                val totalRequestTime = System.currentTimeMillis() - mRequestStartTime
                val seconds = TimeUnit.MILLISECONDS.toSeconds(totalRequestTime)
                Utils.showLog("API", "### Error time ###" + seconds)
            })*/

       /* request.retryPolicy = DefaultRetryPolicy(
            60000,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )*/

        request.setRetryPolicy(DefaultRetryPolicy(60000,0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))

        VolleySingleTon.getInstance(activity).addToRequestQueue(request)
    }
//without success message
    fun callApiWithoutSM(
        activity: Activity,
        url: String,
        method: String,
        listener: ParseControllerListener
    ) {

        mRequestStartTime = System.currentTimeMillis(); // set the request start time just before you send the request.

        val request = StringRequest(
            Request.Method.GET, url,
            Response.Listener<String> { response ->

                val totalRequestTime: Long = System.currentTimeMillis() - mRequestStartTime
                val seconds = TimeUnit.MILLISECONDS.toSeconds(totalRequestTime)
                Utils.showLog("API", "### Success time ###" + seconds)
                try {
                    val main = JSONObject(response)

                    Log.e(TAG, response + "")

                    listener.onSuccess(main, "", method)

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { error ->
                val networkResponse = error.networkResponse
                var errorMessage = "Unknown error"
                Utils.dismissProgress() //new added

                if (networkResponse == null) {
                    if (error.javaClass == TimeoutError::class.java) {
                        errorMessage = "Request timeout"
                    } else if (error.javaClass == NoConnectionError::class.java) {
                        errorMessage = "Failed to connect server"
                        Utils.showAlertConnection(activity)
                    }
                } else {
                    val result = String(networkResponse.data)
                    try {
                        val response = JSONObject(result)
                        val status = response.getString("status")
                        val message = response.getString("message")

                        Log.e("Error Status", status)
                        Log.e("Error Message", message)

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found"
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = "$message Please login again"
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = "$message Check your inputs"
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = "$message Something is getting wrong"
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }


                }

                Log.i("Error", errorMessage)
                error.printStackTrace()
            })
        /* Response.ErrorListener { error ->
             error.printStackTrace()
             val totalRequestTime = System.currentTimeMillis() - mRequestStartTime
             val seconds = TimeUnit.MILLISECONDS.toSeconds(totalRequestTime)
             Utils.showLog("API", "### Error time ###" + seconds)
         })*/

        //default retry count
       /* request.retryPolicy = DefaultRetryPolicy(
            60000,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )*/

        //0 retry count
        request.retryPolicy = DefaultRetryPolicy(
            60000,
            0,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )


        VolleySingleTon.getInstance(activity).addToRequestQueue(request)
    }
}